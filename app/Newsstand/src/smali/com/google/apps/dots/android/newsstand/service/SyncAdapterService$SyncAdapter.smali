.class Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SyncAdapterService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SyncAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;Landroid/content/Context;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;

    .line 91
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 92
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 10
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 97
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    .line 98
    .local v3, "syncIsForCurrentAccount":Z
    if-nez v3, :cond_0

    .line 99
    const-string v1, "Ignoring sync request for non-current account"

    .line 100
    .local v1, "reason":Ljava/lang/String;
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v5, "%s: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    aput-object p1, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ii(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    new-instance v4, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSkippedEvent;

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSkippedEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSkippedEvent;->track()V

    .line 172
    .end local v1    # "reason":Ljava/lang/String;
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getFirstLaunch()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 107
    const-string v1, "Ignoring sync request for inactive user"

    .line 108
    .restart local v1    # "reason":Ljava/lang/String;
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v5, "%s: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    aput-object p1, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ii(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    new-instance v4, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSkippedEvent;

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSkippedEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSkippedEvent;->track()V

    goto :goto_0

    .line 115
    .end local v1    # "reason":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->cantSync(Z)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 117
    iget-object v4, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v4, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v4, Landroid/content/SyncStats;->numIoExceptions:J

    .line 118
    const-string v4, "Ignoring sync request because background disallowed: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 119
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->cantSyncReason()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 118
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 120
    .restart local v1    # "reason":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSkippedEvent;

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSkippedEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSkippedEvent;->track()V

    .line 121
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v5, "%s: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    aput-object p1, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ii(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 127
    .end local v1    # "reason":Ljava/lang/String;
    :cond_2
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v5, "Starting background full sync for %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    new-instance v2, Ljava/util/concurrent/Semaphore;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 130
    .local v2, "semaphore":Ljava/util/concurrent/Semaphore;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    .line 131
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->fullSync()Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v4

    .line 132
    invoke-virtual {v4, p1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setAccount(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter$1;

    .line 133
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v6

    invoke-direct {v5, p0, v6, p5, v2}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter$1;-><init>(Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;Landroid/os/Handler;Landroid/content/SyncResult;Ljava/util/concurrent/Semaphore;)V

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setResultReceiver(Landroid/os/ResultReceiver;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v4

    const v5, 0x927c0

    .line 154
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->wakeful(I)Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;

    move-result-object v4

    .line 155
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;->start()V

    .line 157
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;->triggerNewsWidgetRefreshIfNeeded()V

    .line 161
    const-wide/16 v4, 0x12c

    :try_start_0
    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v6}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 162
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v5, "SyncerService timed out"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;->triggerNewsWidgetUpdateIfNeeded()V

    .line 171
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v5, "Finished"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 164
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1
.end method

.method protected triggerNewsWidgetRefreshIfNeeded()V
    .locals 4

    .prologue
    .line 179
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 180
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.google.apps.dots.android.newsstand.appwidget.REFRESH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 182
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->sendBroadcast(Landroid/content/Intent;)V

    .line 184
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method protected triggerNewsWidgetUpdateIfNeeded()V
    .locals 4

    .prologue
    .line 190
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.google.apps.dots.android.newsstand.appwidget.POKE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 193
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->sendBroadcast(Landroid/content/Intent;)V

    .line 195
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
