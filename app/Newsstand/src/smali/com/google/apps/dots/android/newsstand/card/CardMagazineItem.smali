.class public Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "CardMagazineItem.java"


# static fields
.field public static final DK_CLICK_LISTENER:I

.field public static final DK_CONTENT_DESCRIPTION:I

.field public static final DK_COVER_ATTACHMENT_ID:I

.field public static final DK_COVER_HEIGHT_TO_WIDTH_RATIO:I

.field public static final DK_ISSUE_NAME:I

.field public static final DK_SOURCE_TRANSITION_NAME:I

.field public static final DK_TRANSITION_NAME:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 15
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineItem_issueName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_ISSUE_NAME:I

    .line 17
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineItem_coverAttachmentId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_COVER_ATTACHMENT_ID:I

    .line 19
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineItem_transitionName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_TRANSITION_NAME:I

    .line 21
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineItem_coverHeightToWidthRatio:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_COVER_HEIGHT_TO_WIDTH_RATIO:I

    .line 24
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineItem_clickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_CLICK_LISTENER:I

    .line 26
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineItem_contentDescription:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_CONTENT_DESCRIPTION:I

    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineItem_sourceTransitionName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_SOURCE_TRANSITION_NAME:I

    .line 30
    new-array v0, v2, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->EQUALITY_FIELDS:[I

    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_magazine_item:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->LAYOUTS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->setTransitionGroup(Z)V

    .line 54
    :cond_0
    return-void
.end method
