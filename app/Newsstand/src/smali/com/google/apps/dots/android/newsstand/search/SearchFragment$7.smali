.class Lcom/google/apps/dots/android/newsstand/search/SearchFragment$7;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->groupForResultsList(Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;)Lcom/google/android/libraries/bind/card/CardGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

.field final synthetic val$listType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$7;->val$listType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 253
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    .line 254
    .local v1, "oldState":Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    iget-object v2, v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$7;->val$listType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    invoke-direct {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;)V

    .line 255
    .local v0, "newState":Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 256
    return-void
.end method
