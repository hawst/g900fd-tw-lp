.class public Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
.super Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;
.source "MeterDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;
    }
.end annotation


# static fields
.field private static final PURCHASE_ORDER_TYPE:I = 0x1

.field private static final REQUEST_CODE_PURCHASE:I = 0x66

.field private static final SAMPLE_ORDER_TYPE:I = 0x2

.field private static final SHOW_NUMBER_ANIMATION_MIN_Y_DP:I = 0xf


# instance fields
.field private final buttons:Landroid/view/View;

.field private final connectivityListener:Ljava/lang/Runnable;

.field private doneLayout:Z

.field private final downButtons:Landroid/widget/LinearLayout;

.field private final editionPurchaseListener:Lcom/google/android/libraries/bind/data/DataObserver;

.field private enabledInArticle:Z

.field private final eventObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

.field private final flingHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

.field private isHandlingTouchEvent:Z

.field private isOffline:Z

.field private final meterBody:Landroid/view/View;

.field private final meterHeader:Landroid/view/View;

.field private final meterHeaderInaccessible:Landroid/view/View;

.field private final meterNumber:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

.field private final meterNumberContainer:Landroid/widget/RelativeLayout;

.field private final meterNumberDecremented:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

.field private meterNumberWidth:I

.field private final meterStatusText:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

.field private final meterStatusTextPast:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

.field private meteredArticlesRemaining:I

.field private meteredPolicy:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

.field private observedUri:Landroid/net/Uri;

.field private owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private postId:Ljava/lang/String;

.field private purchasedEditionList:Lcom/google/android/libraries/bind/data/DataList;

.field private final readArticleClickListener:Landroid/view/View$OnClickListener;

.field private final readButton:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

.field private setup:Z

.field private startedSwipeToClose:Z

.field private viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 141
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 142
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 149
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 92
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredArticlesRemaining:I

    .line 100
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup:Z

    .line 102
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isOffline:Z

    .line 103
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHandlingTouchEvent:Z

    .line 104
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->enabledInArticle:Z

    .line 106
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->doneLayout:Z

    .line 125
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->editionPurchaseListener:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 150
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 151
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->meter_dialog_header:I

    invoke-virtual {v0, v2, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeader:Landroid/view/View;

    .line 152
    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->meter_dialog_header_inaccessible:I

    .line 153
    invoke-virtual {v0, v2, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeaderInaccessible:Landroid/view/View;

    .line 154
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->updateAccess()V

    .line 155
    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->meter_dialog_body:I

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->bodyView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterBody:Landroid/view/View;

    .line 156
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeader:Landroid/view/View;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->meter_status_text:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterStatusText:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 157
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeaderInaccessible:Landroid/view/View;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->meter_status_text:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterStatusTextPast:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 158
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeader:Landroid/view/View;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->meter_number_container:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberContainer:Landroid/widget/RelativeLayout;

    .line 159
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeader:Landroid/view/View;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->meter_number:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumber:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 160
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeader:Landroid/view/View;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->meter_number_decremented:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberDecremented:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 161
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeader:Landroid/view/View;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->down_buttons:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->downButtons:Landroid/widget/LinearLayout;

    .line 162
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeader:Landroid/view/View;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->read_button:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->readButton:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 163
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeader:Landroid/view/View;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->buttons:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->buttons:Landroid/view/View;

    .line 165
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    .line 166
    .local v1, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$2;

    invoke-direct {v2, p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Ljava/util/concurrent/Executor;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->eventObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    .line 176
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$3;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->readArticleClickListener:Landroid/view/View$OnClickListener;

    .line 182
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeader:Landroid/view/View;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->read_article:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->readArticleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->readButton:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->readArticleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeader:Landroid/view/View;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->subscribe:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$4;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isOffline:Z

    .line 195
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$5;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$5;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->connectivityListener:Ljava/lang/Runnable;

    .line 201
    new-instance v2, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->flingHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    .line 202
    return-void

    :cond_0
    move v2, v4

    .line 194
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->purchasedEditionList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredArticlesRemaining:I

    return v0
.end method

.method static synthetic access$1002(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredArticlesRemaining:I

    return p1
.end method

.method static synthetic access$1102(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->doneLayout:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterBody:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->refreshMyNews()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->updateMeteredCount()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->tryReadingArticle()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isOffline:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isOffline:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredPolicy:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;)Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredPolicy:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->fillInDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Landroid/accounts/Account;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->watchReadStatesUriIfNecessary(Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->postId:Ljava/lang/String;

    return-object v0
.end method

.method private articleTouchEventUnhandled()V
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    instance-of v0, v0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onArticleUnhandledTouchEvent()V

    .line 699
    :cond_0
    return-void
.end method

.method private fillInDialog(Ljava/lang/String;)V
    .locals 3
    .param p1, "appFamilyId"    # Ljava/lang/String;

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 518
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appFamilySummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V

    .line 517
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 547
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setupOffersIfSupported(Ljava/lang/String;)V

    .line 548
    return-void
.end method

.method private refreshMyNews()V
    .locals 5

    .prologue
    .line 652
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    .line 653
    .local v2, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v3

    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyNews(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 654
    .local v0, "myNewsUri":Ljava/lang/String;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v3, v0, v4}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 655
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->freshVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v1

    .line 657
    .local v1, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$11;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$11;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V

    .line 656
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 664
    return-void
.end method

.method private scrollWebViewToTop()V
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    instance-of v0, v0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    if-eqz v0, :cond_0

    .line 703
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->scrollToTop()V

    .line 705
    :cond_0
    return-void
.end method

.method private setupHeaderText()V
    .locals 4

    .prologue
    const/16 v3, 0xb

    .line 481
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredPolicy:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v0, :cond_0

    .line 492
    :goto_0
    return-void

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeaderInaccessible:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->subscribe_hint:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 485
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredPolicy:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/MeteredUtil;->getSubscribeHint(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;)Landroid/text/Spanned;

    move-result-object v1

    .line 484
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 486
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumber:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredArticlesRemaining:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setText(Ljava/lang/CharSequence;)V

    .line 487
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberDecremented:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredArticlesRemaining:I

    add-int/lit8 v1, v1, -0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setText(Ljava/lang/CharSequence;)V

    .line 488
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterStatusTextPast:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredPolicy:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/MeteredUtil;->getMeteredStatusTextPast(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setText(Ljava/lang/CharSequence;)V

    .line 489
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterStatusText:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 490
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredArticlesRemaining:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredPolicy:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/MeteredUtil;->getMeteredStatusText(Landroid/content/Context;ILcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;)Landroid/text/Spanned;

    move-result-object v1

    .line 489
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setText(Ljava/lang/CharSequence;)V

    .line 491
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumber:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberWidth:I

    goto :goto_0
.end method

.method private setupOffersIfSupported(Ljava/lang/String;)V
    .locals 4
    .param p1, "appFamilyId"    # Ljava/lang/String;

    .prologue
    .line 551
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->isInAppPurchaseSupported(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 552
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 553
    .local v0, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v1

    .line 554
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {v2, v3, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getAppOffers(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getFresh(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V

    .line 553
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 596
    .end local v0    # "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    :goto_0
    return-void

    .line 593
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterBody:Landroid/view/View;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->offers_progress:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 594
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->doneLayout:Z

    goto :goto_0
.end method

.method private showButtons(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;)V
    .locals 4
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 511
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->buttons:Landroid/view/View;

    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->ANIMATION:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 512
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->downButtons:Landroid/widget/LinearLayout;

    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->TEXT_DOWN:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    if-eq p1, v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 513
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->readButton:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->TEXT_UP:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    if-ne p1, v3, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setVisibility(I)V

    .line 514
    return-void

    :cond_0
    move v0, v2

    .line 511
    goto :goto_0

    :cond_1
    move v0, v2

    .line 512
    goto :goto_1

    :cond_2
    move v2, v1

    .line 513
    goto :goto_2
.end method

.method private startWatchingPurchases()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->purchasedEditionList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    .line 291
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->newsSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->purchasedEditionDataList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->purchasedEditionList:Lcom/google/android/libraries/bind/data/DataList;

    .line 292
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->purchasedEditionList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->editionPurchaseListener:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 294
    :cond_0
    return-void
.end method

.method private stopWatchingPurchases()V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->purchasedEditionList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->purchasedEditionList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->editionPurchaseListener:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 303
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->purchasedEditionList:Lcom/google/android/libraries/bind/data/DataList;

    .line 305
    :cond_0
    return-void
.end method

.method private tryReadingArticle()V
    .locals 3

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isOffline:Z

    if-eqz v0, :cond_1

    .line 224
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 225
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->subscription_required_to_read_offline:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 224
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredArticlesRemaining:I

    if-lez v0, :cond_0

    .line 230
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredArticlesRemaining:I

    if-lez v0, :cond_0

    .line 231
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->hide(Z)V

    goto :goto_0
.end method

.method private updateHeaderText(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;)V
    .locals 4
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 504
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberContainer:Landroid/widget/RelativeLayout;

    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->ANIMATION:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 506
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterStatusText:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->ANIMATION:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    if-ne p1, v3, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setVisibility(I)V

    .line 508
    return-void

    :cond_0
    move v0, v2

    .line 504
    goto :goto_0

    :cond_1
    move v2, v1

    .line 506
    goto :goto_1
.end method

.method private updateHeaderType(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;)V
    .locals 1
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredPolicy:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->enableAccess:Z

    if-nez v0, :cond_1

    .line 501
    :cond_0
    :goto_0
    return-void

    .line 499
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->updateHeaderText(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;)V

    .line 500
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->showButtons(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;)V

    goto :goto_0
.end method

.method private updateMeteredCount()V
    .locals 3

    .prologue
    .line 328
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredPolicy:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v1, :cond_0

    .line 358
    :goto_0
    return-void

    .line 331
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 332
    .local v0, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->NETWORK_API:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Lcom/google/apps/dots/android/newsstand/async/Queue;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 357
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private watchReadStatesUriIfNecessary(Landroid/accounts/Account;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 275
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->observedUri:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 277
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getAppReadStatesUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 278
    .local v0, "readStatesUri":Ljava/lang/String;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->observedUri:Landroid/net/Uri;

    .line 279
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->observedUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->eventObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->registerObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 281
    .end local v0    # "readStatesUri":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public allowArticleScroll()Z
    .locals 1

    .prologue
    .line 431
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isOffline:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHidden:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHandlingTouchEvent:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected doneLayout()Z
    .locals 1

    .prologue
    .line 689
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->doneLayout:Z

    return v0
.end method

.method protected getInAppClickHandler(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;I)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "offer"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p2, "offerType"    # I

    .prologue
    .line 599
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;I)V

    return-object v0
.end method

.method public handleArticleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 377
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->enabledInArticle:Z

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHidden:Z

    if-eqz v4, :cond_2

    :cond_0
    move v2, v3

    .line 427
    :cond_1
    :goto_0
    return v2

    .line 380
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->allowArticleScroll()Z

    move-result v4

    if-nez v4, :cond_3

    .line 381
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->articleTouchEventUnhandled()V

    goto :goto_0

    .line 385
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    if-gt v4, v3, :cond_1

    .line 389
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->handleArticleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 390
    .local v1, "shouldUseTouchEvent":Z
    if-eqz v1, :cond_1

    .line 393
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->flingHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {v4, p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->onStartTouchEvent(Landroid/view/MotionEvent;)V

    .line 394
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    if-ne v4, v5, :cond_4

    .line 395
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->articleTouchEventUnhandled()V

    goto :goto_0

    .line 399
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 426
    :cond_5
    :goto_1
    :pswitch_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->flingHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->onEndTouchEvent(Landroid/view/MotionEvent;)V

    move v2, v3

    .line 427
    goto :goto_0

    .line 401
    :pswitch_1
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->startedSwipeToClose:Z

    goto :goto_1

    .line 404
    :pswitch_2
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->startedSwipeToClose:Z

    if-nez v2, :cond_5

    .line 407
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->initialEventY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    sub-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v4, 0x41700000    # 15.0f

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->density:F

    mul-float/2addr v4, v5

    cmpl-float v2, v2, v4

    if-lez v2, :cond_5

    .line 408
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->ANIMATION:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->updateHeaderType(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;)V

    .line 409
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->startedSwipeToClose:Z

    goto :goto_1

    .line 416
    :pswitch_3
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_1

    .line 420
    :pswitch_4
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->flingHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->getFlingDirection()Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-result-object v0

    .line 421
    .local v0, "direction":Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->UP:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    if-ne v0, v2, :cond_5

    .line 422
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_1

    .line 399
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public isSetup()Z
    .locals 1

    .prologue
    .line 271
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 309
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->onAttachedToWindow()V

    .line 310
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->watchReadStatesUriIfNecessary(Landroid/accounts/Account;)V

    .line 311
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->startWatchingPurchases()V

    .line 312
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->observedUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 317
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->observedUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->eventObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->unregisterObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 318
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->observedUri:Landroid/net/Uri;

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->connectivityListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 321
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 323
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->stopWatchingPurchases()V

    .line 324
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->onDetachedFromWindow()V

    .line 325
    return-void
.end method

.method protected onHide(Z)V
    .locals 4
    .param p1, "userDriven"    # Z

    .prologue
    const/4 v1, 0x1

    .line 450
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 451
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setFocusableInTouchMode(Z)V

    .line 452
    if-eqz p1, :cond_0

    .line 453
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 454
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->postId:Ljava/lang/String;

    .line 453
    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsMeteredRead(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    .line 456
    :cond_0
    return-void
.end method

.method protected onResetTrigger()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 680
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->onResetTrigger()V

    .line 681
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumber:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setAlpha(F)V

    .line 682
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberDecremented:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setAlpha(F)V

    .line 683
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumber:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setScaleX(F)V

    .line 684
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberDecremented:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setScaleX(F)V

    .line 685
    return-void
.end method

.method protected onSnapComplete()V
    .locals 2

    .prologue
    .line 475
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->onSnapComplete()V

    .line 476
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->TEXT_UP:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->updateHeaderType(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;)V

    .line 477
    return-void

    .line 476
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->TEXT_DOWN:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 362
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 370
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 371
    .local v0, "superHandled":Z
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHandlingTouchEvent:Z

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    :goto_1
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHandlingTouchEvent:Z

    .line 372
    return v0

    .line 364
    .end local v0    # "superHandled":Z
    :pswitch_1
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHandlingTouchEvent:Z

    goto :goto_0

    .line 368
    :pswitch_2
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHandlingTouchEvent:Z

    goto :goto_0

    .restart local v0    # "superHandled":Z
    :cond_0
    move v1, v2

    .line 371
    goto :goto_1

    .line 362
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected reset(Z)V
    .locals 2
    .param p1, "enableAccess"    # Z

    .prologue
    .line 436
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->reset(Z)V

    .line 438
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setupHeaderText()V

    .line 439
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->TEXT_UP:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->updateHeaderType(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;)V

    .line 440
    return-void

    .line 439
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->TEXT_DOWN:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    goto :goto_0
.end method

.method public setEnabledInArticle(Z)V
    .locals 3
    .param p1, "enabledInArticle"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 206
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->enabledInArticle:Z

    .line 208
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 209
    if-eqz p1, :cond_2

    .line 210
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 211
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setFocusableInTouchMode(Z)V

    .line 217
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 218
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->hide(Z)V

    .line 220
    :cond_1
    return-void

    .line 213
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 214
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setFocusableInTouchMode(Z)V

    goto :goto_0
.end method

.method public setYToSnapPoint()V
    .locals 2

    .prologue
    .line 460
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setYToSnapPoint()V

    .line 461
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->TEXT_UP:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->updateHeaderType(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;)V

    .line 462
    return-void

    .line 461
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;->TEXT_DOWN:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$HeaderType;

    goto :goto_0
.end method

.method public setup(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Landroid/view/View;Landroid/webkit/WebView;)V
    .locals 3
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "owningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p4, "overlayView"    # Landroid/view/View;
    .param p5, "webView"    # Landroid/webkit/WebView;

    .prologue
    .line 238
    invoke-virtual {p0, p4}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setOverlayView(Landroid/view/View;)V

    .line 240
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 241
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->postId:Ljava/lang/String;

    .line 242
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    .line 244
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eq v1, p2, :cond_1

    .line 245
    :cond_0
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 246
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->doneLayout:Z

    .line 247
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 248
    .local v0, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-virtual {p2, v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;

    invoke-direct {v2, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 268
    .end local v0    # "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    :cond_1
    return-void
.end method

.method protected snapToPoint(Landroid/animation/Animator$AnimatorListener;)V
    .locals 2
    .param p1, "optAnimatorListener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 466
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapToPoint(Landroid/animation/Animator$AnimatorListener;)V

    .line 468
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->DOWN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->webView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 469
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->scrollWebViewToTop()V

    .line 471
    :cond_0
    return-void
.end method

.method protected triggerUpdate(F)V
    .locals 5
    .param p1, "update"    # F

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, 0x3f800000    # 1.0f

    .line 668
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->triggerUpdate(F)V

    .line 669
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumber:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    sub-float v2, v3, p1

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setAlpha(F)V

    .line 670
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberDecremented:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setAlpha(F)V

    .line 671
    mul-float v0, p1, p1

    .line 672
    .local v0, "scale":F
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumber:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    sub-float v2, v3, v0

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setScaleX(F)V

    .line 673
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberDecremented:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setScaleX(F)V

    .line 674
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumber:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    neg-float v2, v0

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberWidth:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v4

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setTranslationX(F)V

    .line 675
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberDecremented:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    neg-float v2, v0

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterNumberWidth:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v4

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setTranslationX(F)V

    .line 676
    return-void
.end method

.method protected updateAccess()V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->headerView:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 445
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->headerView:Landroid/widget/FrameLayout;

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->enableAccess:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeader:Landroid/view/View;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 446
    return-void

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterHeaderInaccessible:Landroid/view/View;

    goto :goto_0
.end method
