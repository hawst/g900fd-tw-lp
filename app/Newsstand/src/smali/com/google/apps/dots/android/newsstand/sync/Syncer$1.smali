.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;
.super Ljava/lang/Object;
.source "Syncer.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;->start()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

.field final synthetic val$startTime:J


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 426
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iput-wide p2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;->val$startTime:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 8
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 436
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/exception/NoSpaceLeftException;

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->notifyOutOfSpace()V

    .line 439
    :cond_0
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Failed %s with %s in %s seconds"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v4

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 440
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;->val$startTime:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    .line 439
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->li(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 441
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    const/high16 v1, 0x42c80000    # 100.0f

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->reportProgressCompleted(F)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$500(Lcom/google/apps/dots/android/newsstand/sync/Syncer;F)V

    .line 430
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Finished %s in %s seconds. Ignored failed or skipped tasks: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .line 431
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v4

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;->val$startTime:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->ignoreCounts:Lcom/google/common/collect/Multiset;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/common/collect/Multiset;

    move-result-object v4

    aput-object v4, v2, v3

    .line 430
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->li(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 432
    return-void
.end method
