.class public Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "ResultingDialogFragment.java"


# instance fields
.field private resultCode:I

.field private resultData:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->resultCode:I

    return-void
.end method


# virtual methods
.method public getRequestCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 60
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    const-string v2, "req"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->setResult(I)V

    .line 97
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 98
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->setRetainInstance(Z)V

    .line 33
    if-eqz p1, :cond_0

    .line 34
    const-string v0, "resultCode"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->resultCode:I

    .line 35
    const-string v0, "resultData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->resultData:Landroid/content/Intent;

    .line 37
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 78
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroyView()V

    .line 79
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 84
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->getRequestCode()I

    move-result v1

    .line 85
    .local v1, "requestCode":I
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 86
    if-eqz v1, :cond_0

    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 87
    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;

    .end local v0    # "activity":Landroid/app/Activity;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->resultCode:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->resultData:Landroid/content/Intent;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;->onActivityResult(IILandroid/content/Intent;)V

    .line 90
    :cond_0
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->resultCode:I

    .line 91
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->resultData:Landroid/content/Intent;

    .line 92
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 42
    const-string v0, "resultCode"

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->resultCode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 43
    const-string v0, "resultData"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->resultData:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 44
    return-void
.end method

.method public setRequestCode(I)V
    .locals 2
    .param p1, "requestCode"    # I

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 51
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 52
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "args":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 54
    .restart local v0    # "args":Landroid/os/Bundle;
    :cond_0
    const-string v1, "req"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 56
    return-void
.end method

.method protected setResult(I)V
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->setResult(ILandroid/content/Intent;)V

    .line 65
    return-void
.end method

.method protected setResult(ILandroid/content/Intent;)V
    .locals 0
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 68
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->resultCode:I

    .line 69
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->resultData:Landroid/content/Intent;

    .line 70
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 106
    return-void
.end method
