.class public final Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserDemographics"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;


# instance fields
.field private ageRange_:I

.field private bitField0_:I

.field private gender_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28350
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28351
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 28356
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->gender_:Ljava/lang/String;

    .line 28378
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->ageRange_:I

    .line 28351
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
    .locals 3

    .prologue
    .line 28408
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28412
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
    return-object v0

    .line 28409
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
    :catch_0
    move-exception v1

    .line 28410
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 28347
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->clone()Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28417
    if-ne p1, p0, :cond_1

    .line 28420
    :cond_0
    :goto_0
    return v1

    .line 28418
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 28419
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    .line 28420
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->gender_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->gender_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->ageRange_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->ageRange_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->gender_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->gender_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getAgeRange()I
    .locals 1

    .prologue
    .line 28380
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->ageRange_:I

    return v0
.end method

.method public getGender()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28358
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->gender_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 28444
    const/4 v0, 0x0

    .line 28445
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 28446
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->gender_:Ljava/lang/String;

    .line 28447
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28449
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 28450
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->ageRange_:I

    .line 28451
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 28453
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->cachedSize:I

    .line 28454
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 28425
    const/16 v0, 0x11

    .line 28426
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 28427
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->gender_:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 28428
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->ageRange_:I

    add-int v0, v1, v2

    .line 28429
    return v0

    .line 28427
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->gender_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28462
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 28463
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 28467
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 28468
    :sswitch_0
    return-object p0

    .line 28473
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->gender_:Ljava/lang/String;

    .line 28474
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->bitField0_:I

    goto :goto_0

    .line 28478
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->ageRange_:I

    .line 28479
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->bitField0_:I

    goto :goto_0

    .line 28463
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28347
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28434
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 28435
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->gender_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 28437
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 28438
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->ageRange_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 28440
    :cond_1
    return-void
.end method
