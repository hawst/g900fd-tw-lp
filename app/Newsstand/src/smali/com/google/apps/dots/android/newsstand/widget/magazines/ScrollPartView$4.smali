.class Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;
.super Ljava/lang/Object;
.source "ScrollPartView.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->initGestureDetectors()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private oldFocusX:F

.field private oldFocusY:F

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

.field final synthetic val$temp:Landroid/graphics/Matrix;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Landroid/graphics/Matrix;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 746
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 9
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v8, 0x1

    .line 751
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isInGesture:Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 771
    :goto_0
    return v8

    .line 754
    :cond_0
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransform(Landroid/graphics/Matrix;)V

    .line 756
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->getMatrixScale(Landroid/graphics/Matrix;)F

    move-result v4

    .line 757
    .local v4, "oldScale":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v5

    mul-float v2, v4, v5

    .line 758
    .local v2, "newScale":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    .line 759
    .local v0, "newFocusX":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    .line 762
    .local v1, "newFocusY":F
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->minScale:F
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$400(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)F

    move-result v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getOverZoomScale()F
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)F

    move-result v6

    invoke-static {v2, v5, v6}, Lcom/google/apps/dots/android/newsstand/util/MathUtil;->clamp(FFF)F

    move-result v2

    .line 763
    div-float v3, v2, v4

    .line 765
    .local v3, "newScaleFactor":F
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->oldFocusX:F

    neg-float v6, v6

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->oldFocusY:F

    neg-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 766
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    invoke-virtual {v5, v3, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 767
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 768
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setTransform(Landroid/graphics/Matrix;)V

    .line 769
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->oldFocusX:F

    .line 770
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->oldFocusY:F

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v1, 0x1

    .line 776
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isInGesture:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 782
    :goto_0
    return v1

    .line 779
    :cond_0
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->oldFocusX:F

    .line 780
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->oldFocusY:F

    .line 781
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->onZoomAttempt()V

    goto :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 5
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 788
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransform(Landroid/graphics/Matrix;)V

    .line 789
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->getMatrixScale(Landroid/graphics/Matrix;)F

    move-result v1

    .line 790
    .local v1, "oldScale":F
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->maxScale:F
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$600(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)F

    move-result v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    .line 791
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->maxScale:F
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$600(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)F

    move-result v2

    div-float v0, v2, v1

    .line 792
    .local v0, "newScaleFactor":F
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->oldFocusX:F

    neg-float v3, v3

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->oldFocusY:F

    neg-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 793
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 794
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->oldFocusX:F

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->oldFocusY:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 795
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;->val$temp:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateScrollToTransform(Landroid/graphics/Matrix;)V

    .line 797
    .end local v0    # "newScaleFactor":F
    :cond_0
    return-void
.end method
