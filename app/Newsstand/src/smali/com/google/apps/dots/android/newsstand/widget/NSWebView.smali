.class public Lcom/google/apps/dots/android/newsstand/widget/NSWebView;
.super Landroid/webkit/WebView;
.source "NSWebView.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field protected final asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 43
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->init()V

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    goto :goto_0
.end method

.method private initWebView()V
    .locals 3

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 89
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$1;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .end local v0    # "context":Landroid/content/Context;
    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NSWebView;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 97
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$2;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NSWebView;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 119
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NSWebView;)V

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    .line 132
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 72
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->initWebView()V

    .line 74
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->startWebView()V

    .line 76
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 77
    .local v0, "settings":Landroid/webkit/WebSettings;
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 78
    sget-object v1, Landroid/webkit/WebSettings$PluginState;->ON_DEMAND:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    .line 79
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 80
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 81
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 82
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "userAgent: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0}, Landroid/webkit/WebView;->onAttachedToWindow()V

    .line 147
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 149
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->enterWebView()V

    .line 151
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 155
    invoke-super {p0}, Landroid/webkit/WebView;->onDetachedFromWindow()V

    .line 156
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 158
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->exitWebView()V

    .line 160
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 62
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->goBack()V

    .line 65
    const/4 v0, 0x1

    .line 67
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onProgressChanged(I)V
    .locals 0
    .param p1, "newProgress"    # I

    .prologue
    .line 136
    return-void
.end method

.method public setContent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "content"    # Ljava/lang/String;
    .param p2, "contentType"    # Ljava/lang/String;

    .prologue
    .line 54
    if-nez p1, :cond_0

    const-string p1, ""

    .line 55
    :cond_0
    if-nez p2, :cond_1

    const-string p2, "text/html"

    .line 56
    :cond_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Loading content with size: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method public setUseWideViewport()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 139
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 140
    .local v0, "settings":Landroid/webkit/WebSettings;
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 141
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 142
    return-void
.end method
