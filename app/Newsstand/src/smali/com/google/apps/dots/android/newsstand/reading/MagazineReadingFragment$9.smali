.class Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$9;
.super Ljava/lang/Object;
.source "MagazineReadingFragment.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->onBackToEdition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/android/libraries/bind/data/Data;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 814
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/libraries/bind/data/Data;)Z
    .locals 3
    .param p1, "input"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v1, 0x1

    .line 817
    sget v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_SECTION_SUMMARY:I

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 818
    .local v0, "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->getTocType()I

    move-result v2

    if-ne v2, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 814
    check-cast p1, Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$9;->apply(Lcom/google/android/libraries/bind/data/Data;)Z

    move-result v0

    return v0
.end method
