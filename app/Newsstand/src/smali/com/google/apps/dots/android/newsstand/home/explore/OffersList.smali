.class public Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;
.super Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
.source "OffersList.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 33
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyOffers(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
    .locals 2
    .param p1, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "rootNode"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;->primaryKey()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;I)V

    .line 65
    .local v0, "visitor":Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;

    invoke-direct {v1, p2, p3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 66
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->getResults()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method
