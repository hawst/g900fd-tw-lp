.class public Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;
.super Ljava/lang/Object;
.source "StoreArticleLoaderPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static pool:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    const-class v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 24
    new-instance v0, Landroid/support/v4/util/LruCache;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->pool:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method

.method public static getArticleLoader(ZLjava/lang/String;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .locals 1
    .param p0, "isMagazine"    # Z
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-static {p0, p1, v0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->getArticleLoader(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v0

    return-object v0
.end method

.method public static getArticleLoader(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .locals 6
    .param p0, "isMagazine"    # Z
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "sectionId"    # Ljava/lang/String;
    .param p3, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 38
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->pool:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .line 41
    .local v0, "articleLoader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDesignerMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 42
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "StoreArticleLoader cache miss.  Creating a new article loader with postId: %s."

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    if-eqz p2, :cond_1

    .line 48
    :goto_0
    if-eqz p3, :cond_2

    .line 52
    :goto_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .end local v0    # "articleLoader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    invoke-direct {v0, p3, p2, p1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 54
    .restart local v0    # "articleLoader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->pool:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    :goto_2
    return-object v0

    .line 44
    :cond_1
    const/4 v1, 0x4

    .line 46
    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 50
    :cond_2
    invoke-static {p2, v5}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 56
    :cond_3
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "StoreArticleLoader cache hit. Found: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method
