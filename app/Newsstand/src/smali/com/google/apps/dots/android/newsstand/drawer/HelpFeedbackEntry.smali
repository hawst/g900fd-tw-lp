.class public Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackEntry;
.super Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;
.source "HelpFeedbackEntry.java"


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->HELP_FEEDBACK:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;-><init>(Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;)V

    .line 12
    return-void
.end method


# virtual methods
.method public getLabelResId()I
    .locals 1

    .prologue
    .line 16
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->help_feedback:I

    return v0
.end method

.method public onClick(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 21
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;->launchHelpFeedback(Landroid/app/Activity;)V

    .line 22
    return-void
.end method
