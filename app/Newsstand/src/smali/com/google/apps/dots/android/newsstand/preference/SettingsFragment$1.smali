.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$1;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupArticleTextSizePreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    .line 122
    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Changing article font size to: %s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setArticleFontSize(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getArticleFontSize()Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    move-result-object v0

    # invokes: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getFontSizeTitleResId(Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;)I
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$200(Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 125
    return v4
.end method
