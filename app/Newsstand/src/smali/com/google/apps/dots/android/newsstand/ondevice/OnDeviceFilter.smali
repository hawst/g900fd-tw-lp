.class public abstract Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;
.super Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;
.source "OnDeviceFilter.java"


# static fields
.field private static final PINNED_EQUALITY_FIELDS:[I

.field private static final PREFERENCE_KEYS:[Ljava/lang/String;


# instance fields
.field isInOnDeviceMode:Z

.field protected final pinnedList:Lcom/google/android/libraries/bind/data/DataList;

.field protected final pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-array v0, v3, [I

    sget v1, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_EDITION:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->PINNED_EQUALITY_FIELDS:[I

    .line 26
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "downloadedOnly"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->PREFERENCE_KEYS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V
    .locals 2
    .param p1, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;

    .prologue
    .line 36
    const/4 v0, 0x1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->PREFERENCE_KEYS:[Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;Z[Ljava/lang/String;)V

    .line 38
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->pinnedList()Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->PINNED_EQUALITY_FIELDS:[I

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->filter([I)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->pinnedList:Lcom/google/android/libraries/bind/data/DataList;

    .line 39
    new-instance v0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter$1;-><init>(Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 47
    return-void
.end method


# virtual methods
.method protected abstract includeItem(Lcom/google/android/libraries/bind/data/Data;)Z
.end method

.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->isInOnDeviceMode:Z

    if-nez v0, :cond_0

    .line 71
    const/4 v0, 0x1

    .line 73
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->includeItem(Lcom/google/android/libraries/bind/data/Data;)Z

    move-result v0

    goto :goto_0
.end method

.method public onDataListRegisteredForInvalidation()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->onDataListRegisteredForInvalidation()V

    .line 52
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->pinnedList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 53
    return-void
.end method

.method public onDataListUnregisteredForInvalidation()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->onDataListUnregisteredForInvalidation()V

    .line 58
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->pinnedList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 60
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreFilter()V
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getOnDeviceOnly()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->isInOnDeviceMode:Z

    .line 66
    return-void
.end method
