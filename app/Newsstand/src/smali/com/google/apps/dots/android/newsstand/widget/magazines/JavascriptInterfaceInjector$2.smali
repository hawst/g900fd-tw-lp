.class Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$2;
.super Ljava/lang/Object;
.source "JavascriptInterfaceInjector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->documentLoaded:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$100(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->isOnScreen:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->onAppear()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->webView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reportScriptInjected()V

    .line 63
    return-void
.end method
