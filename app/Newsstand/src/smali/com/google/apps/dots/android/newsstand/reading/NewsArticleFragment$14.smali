.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$14;
.super Ljava/lang/Object;
.source "NewsArticleFragment.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->getIAPConversionEventProvider()Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

.field final synthetic val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$postId:Ljava/lang/String;

.field final synthetic val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 593
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$14;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$14;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$14;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$14;->val$postId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;ZLjava/util/Map;Ljava/util/Map;)Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;
    .locals 9
    .param p1, "fullDocId"    # Ljava/lang/String;
    .param p2, "isHouseAd"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;)",
            "Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;"
        }
    .end annotation

    .prologue
    .line 597
    .local p3, "analyticsDimensions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p4, "analyticsMetrics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$14;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$14;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$14;->val$postId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$14;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .line 600
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$400(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculateCurrentPage()I

    move-result v4

    move-object v5, p1

    move v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;ILjava/lang/String;ZLjava/util/Map;Ljava/util/Map;)V

    return-object v0
.end method
