.class Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$5;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "HeaderEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateShareParams()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 2
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$500(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .line 335
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareUtil;->getShareParamsForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    move-result-object v1

    .line 334
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->setShareParams(Lcom/google/apps/dots/android/newsstand/share/ShareParams;)V

    .line 336
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 331
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$5;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
