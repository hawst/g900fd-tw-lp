.class public Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;
.super Ljava/lang/Object;
.source "AnalyticsDeduper.java"


# instance fields
.field private final cache:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;->cache:Ljava/util/Set;

    return-void
.end method

.method private clearExpiredElements()V
    .locals 2

    .prologue
    .line 31
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;->cache:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 32
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 33
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->hasDedupeExpired()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 37
    :cond_1
    return-void
.end method


# virtual methods
.method public shouldIgnore(Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;)Z
    .locals 2
    .param p1, "event"    # Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;

    .prologue
    .line 18
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->isDedupable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 19
    const/4 v0, 0x0

    .line 27
    :cond_0
    :goto_0
    return v0

    .line 22
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;->clearExpiredElements()V

    .line 23
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;->cache:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 24
    .local v0, "shouldIgnore":Z
    if-nez v0, :cond_0

    .line 25
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;->cache:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
