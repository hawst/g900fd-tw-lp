.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "YearMonthIssuesFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

.field private archiveFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private final magazineTileFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;

.field private final onDeviceFilter:Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x0

    const-string v1, "YearMonthIssuesFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->year_month_issues_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 58
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;-><init>(Z)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->archiveFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

    .line 59
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->magazineTileFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;

    .line 60
    new-instance v0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->onDeviceFilter:Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method private setupAdapter()V
    .locals 2

    .prologue
    .line 121
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    .line 122
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 123
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 124
    return-void
.end method

.method private setupHeaderListLayout()V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 116
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 117
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 116
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 118
    return-void
.end method

.method private updateAdapter()V
    .locals 17
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->state()Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    iget v11, v13, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->year:I

    .line 151
    .local v11, "stateYear":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->state()Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    iget v10, v13, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    .line 152
    .local v10, "stateMonth":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->state()Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    iget v9, v13, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->endMonth:I

    .line 155
    .local v9, "stateEndMonth":I
    const-string v13, ""

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->setActionBarTitle(Ljava/lang/CharSequence;)V

    .line 157
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->magazineSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->onDeviceFilter:Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;

    .line 158
    invoke-virtual {v13, v14}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->archiveFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

    .line 159
    invoke-virtual {v13, v14}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v8

    .line 161
    .local v8, "showingList":Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v8, v11, v10, v9}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->filterForMonth(Lcom/google/android/libraries/bind/data/DataList;III)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    .line 162
    .local v3, "dateList":Lcom/google/android/libraries/bind/data/DataList;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->magazineTileFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;

    invoke-virtual {v3, v13}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v4

    .line 164
    .local v4, "issueCardList":Lcom/google/android/libraries/bind/data/DataList;
    const/16 v13, 0xc

    invoke-static {v13}, Lcom/google/common/collect/Maps;->newHashMapWithExpectedSize(I)Ljava/util/HashMap;

    move-result-object v12

    .line 165
    .local v12, "yearMonthStringMapping":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v6, 0x1

    .local v6, "month":I
    :goto_0
    const/16 v13, 0xc

    if-gt v6, v13, :cond_0

    .line 167
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-static {v13, v11, v6}, Lcom/google/apps/dots/android/newsstand/util/DateFormatUtil;->getLocalizedYearAndMonth(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v13

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    .line 166
    invoke-interface {v12, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 169
    :cond_0
    new-instance v5, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v4, v12}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/Map;)V

    .line 202
    .local v5, "issueGroup":Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v14, Lcom/google/android/apps/newsstanddev/R$integer;->num_magazine_cols:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    .line 203
    .local v7, "numCols":I
    invoke-virtual {v5, v7}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->setFixedNumColumns(I)Lcom/google/android/libraries/bind/card/GridGroup;

    .line 207
    new-instance v13, Lcom/google/android/libraries/bind/data/StaticDataProvider;

    .line 208
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v14

    sget v15, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_download:I

    sget v16, Lcom/google/android/apps/newsstanddev/R$string;->empty_list_caption_offline:I

    .line 207
    invoke-static/range {v14 .. v16}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeStandardEmptyCardData(Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/google/android/libraries/bind/data/StaticDataProvider;-><init>(Lcom/google/android/libraries/bind/data/Data;)V

    invoke-virtual {v5, v13}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->setEmptyRowProvider(Lcom/google/android/libraries/bind/data/DataProvider;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 211
    new-instance v2, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    invoke-direct {v2, v13}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    .line 212
    .local v2, "cardListBuilder":Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    invoke-virtual {v2, v5}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 214
    sget-object v13, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v2, v13}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->useSpacerTopPadding(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 216
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 217
    .local v1, "cardList":Lcom/google/android/libraries/bind/data/DataList;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v13, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 219
    sget v13, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v14, 0x15

    if-lt v13, v14, :cond_1

    .line 221
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    const/4 v14, 0x2

    new-instance v15, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)V

    invoke-virtual {v13, v14, v15}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnChildrenAddedListener(ILjava/lang/Runnable;)V

    .line 228
    :cond_1
    return-void
.end method


# virtual methods
.method protected createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 1

    .prologue
    .line 249
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)V

    return-object v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 232
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "MagazinesByDate: year=%d %s%s"

    const/4 v2, 0x3

    new-array v5, v2, [Ljava/lang/Object;

    .line 233
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->year:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v10

    .line 234
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    if-lez v2, :cond_0

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "month=%d"

    new-array v8, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v10

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    aput-object v2, v5, v9

    const/4 v6, 0x2

    .line 235
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->endMonth:I

    if-lez v2, :cond_1

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "-%d"

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->endMonth:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    aput-object v2, v5, v6

    .line 232
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 236
    .local v1, "editionInfo":Ljava/lang/String;
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 237
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "help_context_key"

    const-string v3, "mobile_magazine_source"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v2, "dateEditionInfo"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    return-object v0

    .line 234
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "editionInfo":Ljava/lang/String;
    :cond_0
    const-string v2, ""

    goto :goto_0

    .line 235
    :cond_1
    const-string v2, ""

    goto :goto_1
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 73
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 74
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->setupHeaderListLayout()V

    .line 75
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 76
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->setupAdapter()V

    .line 77
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 49
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;)V
    .locals 5
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 129
    if-nez p1, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    if-eqz p2, :cond_2

    iget-boolean v3, p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->hideArchive:Z

    iget-boolean v4, p2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->hideArchive:Z

    if-eq v3, v4, :cond_6

    :cond_2
    move v1, v2

    .line 137
    .local v1, "filterChanged":Z
    :goto_1
    if-eqz v1, :cond_3

    .line 138
    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

    iget-boolean v4, p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->hideArchive:Z

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;-><init>(Z)V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->archiveFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

    .line 140
    :cond_3
    if-nez v1, :cond_4

    if-eqz p2, :cond_4

    iget v3, p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->year:I

    iget v4, p2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->year:I

    if-ne v3, v4, :cond_4

    iget v3, p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    iget v4, p2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    if-eq v3, v4, :cond_5

    :cond_4
    move v0, v2

    .line 143
    .local v0, "changed":Z
    :cond_5
    if-eqz v0, :cond_0

    .line 144
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->updateAdapter()V

    goto :goto_0

    .end local v0    # "changed":Z
    .end local v1    # "filterChanged":Z
    :cond_6
    move v1, v0

    .line 136
    goto :goto_1
.end method
