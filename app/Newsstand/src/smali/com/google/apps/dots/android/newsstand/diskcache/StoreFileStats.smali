.class public final Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
.super Ljava/lang/Object;
.source "StoreFileStats.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public blobsFileSize:J

.field public isolatableBlobsSize:J

.field public isolatedBlobsSize:J

.field public nonisolatableBlobsSize:J

.field public numBlobs:I

.field public pinnedBlobsSize:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V
    .locals 4
    .param p1, "blobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .prologue
    .line 58
    if-eqz p1, :cond_0

    .line 59
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->numBlobs:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->numBlobs:I

    .line 60
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v0

    .line 61
    .local v0, "size":J
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatedBlobsSize:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatedBlobsSize:J

    .line 69
    :goto_0
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isPinned(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->pinnedBlobsSize:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->pinnedBlobsSize:J

    .line 73
    .end local v0    # "size":J
    :cond_0
    return-void

    .line 63
    .restart local v0    # "size":J
    :cond_1
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->shouldIsolate(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 64
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatableBlobsSize:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatableBlobsSize:J

    goto :goto_0

    .line 66
    :cond_2
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->nonisolatableBlobsSize:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->nonisolatableBlobsSize:J

    goto :goto_0
.end method

.method public blobsFileBlobsSize()J
    .locals 4

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatableBlobsSize:J

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->nonisolatableBlobsSize:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method protected clone()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    .locals 2

    .prologue
    .line 94
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 95
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->clone()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    move-result-object v0

    return-object v0
.end method

.method public removeBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V
    .locals 4
    .param p1, "blobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .prologue
    .line 40
    if-eqz p1, :cond_0

    .line 41
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->numBlobs:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->numBlobs:I

    .line 42
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v0

    .line 43
    .local v0, "size":J
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 44
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatedBlobsSize:J

    sub-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatedBlobsSize:J

    .line 51
    :goto_0
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isPinned(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->pinnedBlobsSize:J

    sub-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->pinnedBlobsSize:J

    .line 55
    .end local v0    # "size":J
    :cond_0
    return-void

    .line 45
    .restart local v0    # "size":J
    :cond_1
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->shouldIsolate(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 46
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatableBlobsSize:J

    sub-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatableBlobsSize:J

    goto :goto_0

    .line 48
    :cond_2
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->nonisolatableBlobsSize:J

    sub-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->nonisolatableBlobsSize:J

    goto :goto_0
.end method

.method public replace(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V
    .locals 0
    .param p1, "oldBlobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .param p2, "newBlobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->removeBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 77
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->addBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 78
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 82
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "numBlobs"

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->numBlobs:I

    .line 83
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "blobsFileSize"

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->blobsFileSize:J

    .line 84
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "isolatedBlobsSize"

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatedBlobsSize:J

    .line 85
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "isolatableBlobsSize"

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatableBlobsSize:J

    .line 86
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "nonisolatableBlobsSize"

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->nonisolatableBlobsSize:J

    .line 87
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public totalBlobsSize()J
    .locals 4

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatableBlobsSize:J

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatedBlobsSize:J

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->nonisolatableBlobsSize:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public totalCacheOnDiskSize()J
    .locals 4

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->totalOnDiskSize()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->pinnedBlobsSize:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public totalOnDiskSize()J
    .locals 4

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->blobsFileSize:J

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->isolatedBlobsSize:J

    add-long/2addr v0, v2

    return-wide v0
.end method
