.class public Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;
.super Lcom/google/android/play/layout/PlayPopupMenu;
.source "NSPopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupListAdapter;,
        Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchor"    # Landroid/view/View;

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayPopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 102
    return-void
.end method


# virtual methods
.method public show()V
    .locals 4

    .prologue
    .line 112
    new-instance v2, Landroid/support/v7/widget/ListPopupWindow;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/support/v7/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    .line 114
    new-instance v0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupListAdapter;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mPopupActions:Ljava/util/List;

    invoke-direct {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 115
    .local v0, "popupListAdapter":Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 117
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    invoke-virtual {v2, p0}, Landroid/support/v7/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 118
    invoke-virtual {p0, p0}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->setOnPopupDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 119
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$1;

    invoke-direct {v3, p0, v0}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$1;-><init>(Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 134
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mAnchor:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 136
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 137
    .local v1, "screenWidth":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mContext:Landroid/content/Context;

    .line 138
    invoke-static {v0, v3}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->measureContentWidth(Landroid/widget/ListAdapter;Landroid/content/Context;)I

    move-result v3

    .line 137
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ListPopupWindow;->setContentWidth(I)V

    .line 140
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ListPopupWindow;->setModal(Z)V

    .line 142
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/support/v7/widget/ListPopupWindow;->show()V

    .line 143
    return-void
.end method
