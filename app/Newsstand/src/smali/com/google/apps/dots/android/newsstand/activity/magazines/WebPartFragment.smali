.class public Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "WebPartFragment.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;",
        ">;",
        "Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private appId:Ljava/lang/String;

.field private eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

.field private webPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x0

    const-string v1, "WebPartFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->web_part_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 41
    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->appId:Ljava/lang/String;

    return-object v0
.end method

.method public getAsyncToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    return-object v0
.end method

.method public getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    return-object v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 139
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 140
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_magazine_source"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v2, "editionInfo"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->appId:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v2, "sectionInfo"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->sectionId:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v2, "editionPostId"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->postId:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v2, "shareUrl"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->localUrl:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    return-object v0
.end method

.method public getLetterboxScale()F
    .locals 1

    .prologue
    .line 51
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 121
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onDestroy()V

    .line 122
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->stop()V

    .line 123
    return-void
.end method

.method public onDestroyed(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 71
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->webPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->onPause()V

    .line 128
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onPause()V

    .line 129
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onResume()V

    .line 134
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->webPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->onResume()V

    .line 135
    return-void
.end method

.method public onViewCreated(Landroid/view/View;)V
    .locals 0
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 81
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;)V
    .locals 13
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;

    .prologue
    .line 85
    if-nez p1, :cond_0

    .line 117
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->appId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->appId:Ljava/lang/String;

    .line 90
    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->sectionId:Ljava/lang/String;

    .line 91
    .local v5, "sectionId":Ljava/lang/String;
    iget-object v6, p1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->postId:Ljava/lang/String;

    .line 92
    .local v6, "postId":Ljava/lang/String;
    iget-object v7, p1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->fieldId:Ljava/lang/String;

    .line 93
    .local v7, "fieldId":Ljava/lang/String;
    iget-object v12, p1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->localUrl:Ljava/lang/String;

    .line 94
    .local v12, "path":Ljava/lang/String;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    const-string v1, "nativebody"

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    .line 96
    new-instance v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->appId:Ljava/lang/String;

    new-instance v1, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;-><init>()V

    new-instance v3, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    invoke-direct {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;-><init>()V

    new-instance v8, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-direct {v8}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;-><init>()V

    .line 103
    invoke-virtual {v3, v8}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->setLocation(Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;)Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->setLayoutDetails(Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;)Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    move-result-object v1

    new-instance v3, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    invoke-direct {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;-><init>()V

    .line 104
    invoke-virtual {v1, v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->setWebDetails(Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;)Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    move-result-object v8

    const/4 v9, 0x1

    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;-><init>()V

    .line 107
    invoke-virtual {v1, v12}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->setMainResourceUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    move-result-object v10

    const/4 v11, 0x1

    move-object v1, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v11}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;ZLcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;Z)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->webPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .line 115
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->rootView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->webPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/16 v8, 0x11

    invoke-direct {v2, v3, v4, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
