.class public Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
.super Ljava/lang/Object;
.source "MagazineEditionFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field public final inLiteMode:Z

.field public final initialPageNumber:Ljava/lang/Integer;

.field public final initialPostId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "inLiteMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;ZLjava/lang/String;Ljava/lang/Integer;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;ZLjava/lang/String;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "inLiteMode"    # Z
    .param p3, "initialPostId"    # Ljava/lang/String;
    .param p4, "initialPageNumber"    # Ljava/lang/Integer;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 33
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->inLiteMode:Z

    .line 34
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPageNumber:Ljava/lang/Integer;

    .line 36
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 46
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 47
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    .line 48
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->inLiteMode:Z

    .line 49
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->inLiteMode:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    .line 50
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPageNumber:Ljava/lang/Integer;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPageNumber:Ljava/lang/Integer;

    .line 51
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 53
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 58
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->inLiteMode:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPageNumber:Ljava/lang/Integer;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 40
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v2, "{MagazineEditionFragmentState: %s %s - %s}"

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v4, v3, v0

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->inLiteMode:Z

    if-eqz v0, :cond_0

    const-string v0, "Lite mode"

    :goto_0
    aput-object v0, v3, v4

    const/4 v0, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPageNumber:Ljava/lang/Integer;

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 69
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->inLiteMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPageNumber:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPageNumber:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 72
    return-void

    .line 69
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 71
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
