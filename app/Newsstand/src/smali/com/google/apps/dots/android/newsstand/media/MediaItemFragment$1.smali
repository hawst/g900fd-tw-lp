.class Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$1;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "MediaItemFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Lcom/google/apps/dots/android/newsstand/media/MediaItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

.field final synthetic val$state:Lcom/google/apps/dots/android/newsstand/media/MediaItem;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;Lcom/google/apps/dots/android/newsstand/media/MediaItem;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$1;->val$state:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 6
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 68
    if-nez p1, :cond_1

    .line 69
    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "Failed to retrieve post"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$1;->val$state:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->fieldId:Ljava/lang/String;

    invoke-static {p1, v3}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item;

    move-result-object v0

    .line 73
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$1;->val$state:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    iget v3, v3, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->offset:I

    invoke-static {v0, v3}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;I)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v1

    .line 74
    .local v1, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    const/4 v2, 0x0

    .line 75
    .local v2, "view":Landroid/view/View;
    iget v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    packed-switch v3, :pswitch_data_0

    .line 80
    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3}, Ljava/lang/IllegalStateException;-><init>()V

    throw v3

    .line 77
    :pswitch_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->getImageView(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Landroid/view/View;

    move-result-object v2

    .line 83
    if-eqz v2, :cond_0

    .line 84
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->mediaItemContainer:Landroid/widget/FrameLayout;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->access$100(Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;)Landroid/widget/FrameLayout;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 65
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$1;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
