.class public Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
.source "AudioFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;
    }
.end annotation


# instance fields
.field private audioControlBar:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

.field private audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

.field private audioStatus:I

.field private firstScrollDownY:F

.field private firstScrollUpY:F

.field private lastScrollY:F

.field private minScrollToToggleControlsDp:F

.field private resetScrollDirection:Z

.field private scrollDirection:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

.field private scrollListener:Landroid/view/View$OnTouchListener;

.field private setupScroll:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;-><init>()V

    .line 23
    sget-object v0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;->NEUTRAL:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->scrollDirection:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioStatus:I

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioControlBar:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->showOnScrollEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->scrollDirection:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->scrollDirection:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->setupScroll:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->setupScroll:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->resetScrollDirection:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->resetScrollDirection:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 18
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->lastScrollY:F

    return v0
.end method

.method static synthetic access$502(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    .param p1, "x1"    # F

    .prologue
    .line 18
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->lastScrollY:F

    return p1
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 18
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->firstScrollUpY:F

    return v0
.end method

.method static synthetic access$602(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    .param p1, "x1"    # F

    .prologue
    .line 18
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->firstScrollUpY:F

    return p1
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 18
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->firstScrollDownY:F

    return v0
.end method

.method static synthetic access$702(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    .param p1, "x1"    # F

    .prologue
    .line 18
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->firstScrollDownY:F

    return p1
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 18
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->minScrollToToggleControlsDp:F

    return v0
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 18
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioStatus:I

    return v0
.end method

.method static synthetic access$902(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    .param p1, "x1"    # I

    .prologue
    .line 18
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioStatus:I

    return p1
.end method

.method private setupAudio()V
    .locals 2

    .prologue
    .line 123
    new-instance v0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    .line 149
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->register(Landroid/content/Context;)V

    .line 150
    return-void
.end method

.method private showOnScrollEnabled()Z
    .locals 2

    .prologue
    .line 112
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioStatus:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioStatus:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public doOnCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    sget v3, Lcom/google/android/apps/newsstanddev/R$layout;->audio_fragment:I

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 44
    .local v2, "rootView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 45
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 46
    .local v0, "density":F
    const/high16 v3, 0x41200000    # 10.0f

    mul-float/2addr v3, v0

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->minScrollToToggleControlsDp:F

    .line 48
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->audio_control_bar:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioControlBar:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .line 49
    new-instance v3, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->scrollListener:Landroid/view/View$OnTouchListener;

    .line 103
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->setupAudio()V

    .line 104
    return-object v2
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->onDestroyView()V

    .line 119
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->unregister(Landroid/content/Context;)V

    .line 120
    return-void
.end method

.method public spyOnTouchEvent(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->scrollListener:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p1, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 109
    return-void
.end method
