.class public Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
.super Ljava/lang/Object;
.source "EditionSubscriptionInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final defaultSubscribed:Z

.field public final editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

.field public final subscribe:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V
    .locals 0
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p2, "defaultSubscribed"    # Z
    .param p3, "subscribe"    # Z

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 23
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->defaultSubscribed:Z

    .line 24
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->subscribe:Z

    .line 25
    return-void
.end method

.method private static editionFromSummary(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 46
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 34
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 35
    check-cast v0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;

    .line 37
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->editionFromSummary(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 38
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->editionFromSummary(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    .line 37
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->defaultSubscribed:Z

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->defaultSubscribed:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->subscribe:Z

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->subscribe:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 42
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->editionFromSummary(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->defaultSubscribed:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->subscribe:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 58
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->defaultSubscribed:Z

    invoke-static {p1, v0}, Lcom/google/android/play/onboard/OnboardUtils;->writeBoolean(Landroid/os/Parcel;Z)V

    .line 59
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->subscribe:Z

    invoke-static {p1, v0}, Lcom/google/android/play/onboard/OnboardUtils;->writeBoolean(Landroid/os/Parcel;Z)V

    .line 60
    return-void
.end method
