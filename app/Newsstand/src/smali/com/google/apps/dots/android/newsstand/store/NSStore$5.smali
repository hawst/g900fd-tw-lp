.class Lcom/google/apps/dots/android/newsstand/store/NSStore$5;
.super Ljava/lang/Object;
.source "NSStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerAttachmentDownloadWithFallbackToClientTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field final synthetic val$combinedResponseFuture:Lcom/google/common/util/concurrent/SettableFuture;

.field final synthetic val$delayedOriginalTransformFuture:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/common/util/concurrent/SettableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 361
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$5;->val$combinedResponseFuture:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$5;->val$delayedOriginalTransformFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$5;->val$combinedResponseFuture:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 371
    return-void
.end method

.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V
    .locals 2
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$5;->val$combinedResponseFuture:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    .line 365
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$5;->val$delayedOriginalTransformFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 366
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 361
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/NSStore$5;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V

    return-void
.end method
