.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;
.super Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;
.source "MagazinesSubscriptionsList.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;-><init>()V

    return-void
.end method


# virtual methods
.method protected makeEdition(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p2, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 18
    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->magazineEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v0

    return-object v0
.end method

.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 23
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
