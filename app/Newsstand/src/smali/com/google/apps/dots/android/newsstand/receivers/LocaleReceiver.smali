.class public Lcom/google/apps/dots/android/newsstand/receivers/LocaleReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocaleReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 21
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 22
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 23
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 24
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    .line 25
    invoke-static {v0, v3}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->makeNotificationExtras(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/Version;)Ljava/util/Map;

    move-result-object v3

    .line 23
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->notify(Landroid/net/Uri;Ljava/util/Map;)V

    .line 29
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->libraryPageList()Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->invalidateData()V

    .line 32
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->viewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/view/ViewHeap;->clearHeap()V

    .line 33
    return-void
.end method
