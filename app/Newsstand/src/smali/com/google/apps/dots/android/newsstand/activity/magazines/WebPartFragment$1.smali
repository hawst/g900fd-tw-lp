.class Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment$1;
.super Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
.source "WebPartFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;ZLcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;Z)V
    .locals 11
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;
    .param p4, "appId"    # Ljava/lang/String;
    .param p5, "sectionId"    # Ljava/lang/String;
    .param p6, "postId"    # Ljava/lang/String;
    .param p7, "fieldId"    # Ljava/lang/String;
    .param p8, "webPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .param p9, "isOnlyChildOfRoot"    # Z
    .param p10, "inlineFrame"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    .param p11, "handleLocalUrls"    # Z

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move/from16 v8, p9

    move-object/from16 v9, p10

    move/from16 v10, p11

    invoke-direct/range {v0 .. v10}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;ZLcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;Z)V

    return-void
.end method


# virtual methods
.method public initWebSettings()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->initWebSettings()V

    .line 112
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment$1;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    sget-object v1, Landroid/webkit/WebSettings$PluginState;->ON_DEMAND:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    .line 113
    return-void
.end method
