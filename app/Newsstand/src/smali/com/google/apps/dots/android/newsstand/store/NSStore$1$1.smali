.class Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;
.super Ljava/lang/Object;
.source "NSStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureFallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->call()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureFallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/store/NSStore$1;

.field final synthetic val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

.field final synthetic val$blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore$1;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/store/NSStore$1;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/NSStore$1;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;->val$blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/NSStore$1;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: download failed (%s). Returning cached version."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/NSStore$1;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/NSStore$1;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->decayCacheFallbackTimeout()J
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$800(Lcom/google/apps/dots/android/newsstand/store/NSStore;)J

    .line 221
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;->val$blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/NSStore$1;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;->val$blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    .line 222
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getBlobFileVersion(Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/apps/dots/android/newsstand/store/Version;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;-><init>(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;Lcom/google/apps/dots/android/newsstand/store/Version;)V

    .line 221
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
