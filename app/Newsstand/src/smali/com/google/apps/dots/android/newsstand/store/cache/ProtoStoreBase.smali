.class public abstract Lcom/google/apps/dots/android/newsstand/store/cache/ProtoStoreBase;
.super Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;
.source "ProtoStoreBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">",
        "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private supplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/common/base/Supplier;)V
    .locals 0
    .param p1, "nsStore"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/NSStore;",
            "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;",
            "Lcom/google/common/base/Supplier",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/ProtoStoreBase;, "Lcom/google/apps/dots/android/newsstand/store/cache/ProtoStoreBase<TT;>;"
    .local p3, "supplier":Lcom/google/common/base/Supplier;, "Lcom/google/common/base/Supplier<TT;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 24
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/cache/ProtoStoreBase;->supplier:Lcom/google/common/base/Supplier;

    .line 25
    return-void
.end method


# virtual methods
.method protected parse(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;
    .locals 6
    .param p1, "storeResponse"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/ProtoStoreBase;, "Lcom/google/apps/dots/android/newsstand/store/cache/ProtoStoreBase<TT;>;"
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeAssetFileDescriptor()Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 30
    .local v0, "blobAfd":Landroid/content/res/AssetFileDescriptor;
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v1

    .line 32
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    long-to-int v3, v4

    .line 33
    .local v3, "length":I
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/cache/ProtoStoreBase;->supplier:Lcom/google/common/base/Supplier;

    invoke-interface {v4}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/nano/MessageNano;

    invoke-static {v4, v1, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->readFromStream(Lcom/google/protobuf/nano/MessageNano;Ljava/io/InputStream;I)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    .line 34
    .local v2, "item":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    new-instance v4, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    div-int/lit16 v5, v3, 0x400

    invoke-direct {v4, p1, v2, v5}, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;-><init>(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v4

    .end local v2    # "item":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    .end local v3    # "length":I
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v4
.end method
