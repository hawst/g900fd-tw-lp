.class public Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
.super Ljava/lang/Object;
.source "AuthUiHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$NoGoogleAccountDialog;,
        Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;,
        Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;,
        Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$FirstTimeNoNetworkDialog;,
        Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private authErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;

.field private authFuture:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private configFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
            ">;"
        }
    .end annotation
.end field

.field private firstTimeNoNetworkDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$FirstTimeNoNetworkDialog;

.field private final fragment:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;

.field private googlePlayServicesErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;

.field private lastRequestCode:I

.field private noGoogleAccountDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$NoGoogleAccountDialog;

.field private showedAuthPromptWithoutDialog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method protected constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;Lcom/google/apps/dots/android/newsstand/async/AsyncScope;)V
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;
    .param p2, "asyncScope"    # Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->lastRequestCode:I

    .line 99
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->fragment:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;

    .line 101
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 102
    return-void
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
    .param p1, "x1"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->lastRequestCode:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;)Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    return-object v0
.end method

.method static synthetic access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showedAuthPromptWithoutDialog:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showedAuthPromptWithoutDialog:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->restoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->saveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method private accountFlow()Z
    .locals 5

    .prologue
    .line 284
    const/4 v0, 0x0

    .line 286
    .local v0, "accountOverride":Landroid/accounts/Account;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "authAccount"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 287
    .local v2, "requestedAccount":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 288
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->accountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 291
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->initAccountIfNeeded(Landroid/accounts/Account;)Landroid/accounts/Account;
    :try_end_0
    .catch Lcom/google/apps/dots/android/newsstand/auth/NoGoogleAccountException; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    const/4 v3, 0x1

    .line 295
    :goto_0
    return v3

    .line 293
    :catch_0
    move-exception v1

    .line 294
    .local v1, "e":Lcom/google/apps/dots/android/newsstand/auth/NoGoogleAccountException;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showNoGoogleAccountDialog()V

    .line 295
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static checkAllowOnboardingFlow()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304
    const-string v1, "newsstand:onboarding_flow_enabled"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil;->getBoolean(Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 306
    .local v0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/Boolean;>;"
    new-instance v1, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$2;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$2;-><init>()V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 318
    return-object v0
.end method

.method private checkAsyncScopeIsValid()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->isStarted()Z

    move-result v0

    const-string v1, "Async scope must be started."

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 129
    return-void
.end method

.method private checkForGooglePlayServices()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 268
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 269
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 277
    :goto_0
    return v1

    .line 272
    :cond_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "GooglePlayServices error: %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 273
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showGooglePlayServicesErrorDialog(I)V

    .line 275
    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GMSVerificationErrorEvent;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GMSVerificationErrorEvent;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GMSVerificationErrorEvent;->track(Z)V

    .line 276
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->flushAnalyticsEvents()V

    move v1, v2

    .line 277
    goto :goto_0
.end method

.method protected static getAuthPromptIntent(Ljava/lang/Throwable;)Landroid/content/Intent;
    .locals 4
    .param p0, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 457
    move-object v0, p0

    .line 458
    .local v0, "current":Ljava/lang/Throwable;
    const/4 v1, 0x0

    .line 459
    .local v1, "intent":Landroid/content/Intent;
    :goto_0
    if-eqz v0, :cond_0

    .line 460
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;

    if-eqz v2, :cond_2

    .line 461
    check-cast v0, Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;

    .end local v0    # "current":Ljava/lang/Throwable;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;->getPromptIntent()Landroid/content/Intent;

    move-result-object v1

    .line 466
    :cond_0
    if-eqz v1, :cond_1

    .line 469
    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    move-result v2

    const v3, -0x10000001

    and-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 471
    :cond_1
    return-object v1

    .line 464
    .restart local v0    # "current":Ljava/lang/Throwable;
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0
.end method

.method private initializeFuture()V
    .locals 3

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 244
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->authFuture:Lcom/google/common/util/concurrent/SettableFuture;

    .line 245
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->authFuture:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->track(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 246
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->authFuture:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;)V

    .line 253
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->mainThreadExecutor()Ljava/util/concurrent/Executor;

    move-result-object v2

    .line 246
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 254
    return-void
.end method

.method public static instanceForActivity(Landroid/support/v4/app/FragmentActivity;)Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
    .locals 4
    .param p0, "fragmentActivity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 482
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    instance-of v2, p0, Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 484
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 486
    .local v1, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->access$400()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;

    .line 487
    .local v0, "fragment":Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;
    if-nez v0, :cond_0

    .line 488
    new-instance v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;

    .end local v0    # "fragment":Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;
    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;-><init>()V

    .line 489
    .restart local v0    # "fragment":Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->access$400()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 490
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 492
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 494
    :cond_0
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-ne v2, p0, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 495
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->getAuthUiHelper()Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    move-result-object v2

    return-object v2

    .line 494
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private restoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 120
    const-string v0, "AuthUiHelper_resuming"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->initializeFuture()V

    .line 123
    :cond_0
    const-string v0, "AuthUiHelper_lastRequestCode"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->lastRequestCode:I

    .line 124
    const-string v0, "AuthUiHelper_showedAuthPromptWithoutDialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showedAuthPromptWithoutDialog:Z

    .line 125
    return-void
.end method

.method private saveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 114
    const-string v0, "AuthUiHelper_lastRequestCode"

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->lastRequestCode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 115
    const-string v0, "AuthUiHelper_showedAuthPromptWithoutDialog"

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showedAuthPromptWithoutDialog:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 116
    const-string v0, "AuthUiHelper_resuming"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 117
    return-void
.end method

.method private startOrResumeAuthFlow()V
    .locals 1

    .prologue
    .line 257
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->checkAsyncScopeIsValid()V

    .line 258
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->checkForGooglePlayServices()Z

    move-result v0

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->accountFlow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->getConfig()V

    goto :goto_0
.end method


# virtual methods
.method public existingAuthFlowFuture()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 211
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->authFuture:Lcom/google/common/util/concurrent/SettableFuture;

    return-object v0
.end method

.method protected getActivity()Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->fragment:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method getConfig()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 348
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 350
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->hasPendingConfigRequest()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 407
    :goto_0
    return-void

    .line 354
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 356
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->hasCachedConfig(Landroid/accounts/Account;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 357
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->hasCachedAuthToken(Landroid/accounts/Account;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v1, v3

    .line 360
    .local v1, "hasConfigAndAuthToken":Z
    :goto_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    move-result-object v4

    invoke-virtual {v4, v0, v3}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->getAuthTokenFuture(Landroid/accounts/Account;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 362
    .local v2, "tokenFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/String;>;"
    new-instance v3, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$3;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$3;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;)V

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->configFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 370
    if-eqz v1, :cond_2

    .line 371
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->onAuthSuccess()V

    goto :goto_0

    .line 357
    .end local v1    # "hasConfigAndAuthToken":Z
    .end local v2    # "tokenFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/String;>;"
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 376
    .restart local v1    # "hasConfigAndAuthToken":Z
    .restart local v2    # "tokenFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/String;>;"
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->configFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v5, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;)V

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method protected hasPendingConfigRequest()Z
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->configFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->configFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newAuthFlowFuture()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 235
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->initializeFuture()V

    .line 236
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->startOrResumeAuthFlow()V

    .line 237
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->authFuture:Lcom/google/common/util/concurrent/SettableFuture;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    const/4 v7, -0x1

    const/4 v0, 0x1

    .line 160
    sget-object v2, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "onActivityResult: lastRequestCode=%d, requestCode=%d, resultCode=%d, asyncScope.isStarted()=%b"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->lastRequestCode:I

    .line 161
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 162
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->isStarted()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    .line 160
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->lastRequestCode:I

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->isStarted()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 202
    :cond_1
    :goto_0
    return v0

    .line 168
    :cond_2
    iput v7, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->lastRequestCode:I

    .line 170
    packed-switch p1, :pswitch_data_0

    .line 205
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown request code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->onAuthFailure(I)V

    goto :goto_0

    .line 177
    :pswitch_1
    if-nez p2, :cond_3

    .line 178
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->onAuthFailure(I)V

    goto :goto_0

    .line 179
    :cond_3
    if-ne p2, v7, :cond_1

    .line 180
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/AddGoogleAccountIntentBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/AddGoogleAccountIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/AddGoogleAccountIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x2c5

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 186
    :pswitch_2
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->startOrResumeAuthFlow()V

    goto :goto_0

    .line 189
    :pswitch_3
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->onAuthFailure(I)V

    goto :goto_0

    .line 192
    :pswitch_4
    if-ne p2, v7, :cond_4

    .line 194
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->startOrResumeAuthFlow()V

    goto :goto_0

    .line 195
    :cond_4
    if-nez p2, :cond_1

    .line 196
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->onAuthFailure(I)V

    goto :goto_0

    .line 201
    :pswitch_5
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->startOrResumeAuthFlow()V

    goto :goto_0

    .line 170
    :pswitch_data_0
    .packed-switch 0x2c4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method

.method protected onAuthFailure(I)V
    .locals 2
    .param p1, "requestCode"    # I

    .prologue
    .line 143
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 144
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;-><init>(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;->track(Z)V

    .line 145
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->flushAnalyticsEvents()V

    .line 146
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->authFuture:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 147
    return-void
.end method

.method protected onAuthSuccess()V
    .locals 2

    .prologue
    .line 150
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 151
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->authFuture:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    .line 152
    return-void
.end method

.method public shouldShowOnboardingQuiz()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 326
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    .line 327
    .local v3, "prefs":Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getShowOnboardQuizPreference()I

    move-result v4

    .line 328
    .local v4, "quizPreference":I
    if-nez v4, :cond_1

    .line 329
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 330
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v1

    .line 331
    .local v1, "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getWarmWelcomeNeeded()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getReportShowedOnboardQuiz(Landroid/accounts/Account;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 337
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    :goto_0
    return v5

    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    :cond_0
    move v5, v6

    .line 331
    goto :goto_0

    .line 334
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    :cond_1
    const/4 v7, 0x2

    if-ne v4, v7, :cond_2

    move v2, v5

    .line 335
    .local v2, "overrideShow":Z
    :goto_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v7

    if-eqz v2, :cond_3

    const-string v5, "Showing"

    :goto_2
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v8, " onboarding quiz based on override in internal preferences."

    invoke-virtual {v5, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 336
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    move v5, v2

    .line 337
    goto :goto_0

    .end local v2    # "overrideShow":Z
    :cond_2
    move v2, v6

    .line 334
    goto :goto_1

    .line 335
    .restart local v2    # "overrideShow":Z
    :cond_3
    const-string v5, "Skipping"

    goto :goto_2
.end method

.method protected showAuthErrorDialog()V
    .locals 2

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->authErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;

    if-nez v0, :cond_0

    .line 433
    new-instance v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->authErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;

    .line 434
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->authErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;

    const/16 v1, 0x2c7

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;->setRequestCode(I)V

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->authErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showDialogIfNotVisible(Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;)V

    .line 437
    return-void
.end method

.method protected showDialogIfNotVisible(Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;)V
    .locals 6
    .param p1, "dialogFragment"    # Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;

    .prologue
    .line 448
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->isVisible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 449
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 450
    .local v0, "tag":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;->getRequestCode()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->lastRequestCode:I

    .line 451
    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "showDialogIfNotVisible: tag = %s, requestCode = %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->lastRequestCode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 452
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1, v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->showSupportDialogCarefully(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V

    .line 454
    .end local v0    # "tag":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected showFirstTimeNoNetworkDialog()V
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->firstTimeNoNetworkDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$FirstTimeNoNetworkDialog;

    if-nez v0, :cond_0

    .line 425
    new-instance v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$FirstTimeNoNetworkDialog;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$FirstTimeNoNetworkDialog;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->firstTimeNoNetworkDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$FirstTimeNoNetworkDialog;

    .line 426
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->firstTimeNoNetworkDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$FirstTimeNoNetworkDialog;

    const/16 v1, 0x2c6

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$FirstTimeNoNetworkDialog;->setRequestCode(I)V

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->firstTimeNoNetworkDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$FirstTimeNoNetworkDialog;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showDialogIfNotVisible(Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;)V

    .line 429
    return-void
.end method

.method protected showGooglePlayServicesErrorDialog(I)V
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->googlePlayServicesErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->googlePlayServicesErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;

    .line 415
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;->getErrorCode()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 416
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->googlePlayServicesErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;

    .line 417
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->googlePlayServicesErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;

    const/16 v1, 0x2c9

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;->setRequestCode(I)V

    .line 418
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->googlePlayServicesErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;->setErrorCode(I)V

    .line 420
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->googlePlayServicesErrorDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showDialogIfNotVisible(Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;)V

    .line 421
    return-void
.end method

.method protected showNoGoogleAccountDialog()V
    .locals 2

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->noGoogleAccountDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$NoGoogleAccountDialog;

    if-nez v0, :cond_0

    .line 441
    new-instance v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$NoGoogleAccountDialog;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$NoGoogleAccountDialog;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->noGoogleAccountDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$NoGoogleAccountDialog;

    .line 442
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->noGoogleAccountDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$NoGoogleAccountDialog;

    const/16 v1, 0x2c4

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$NoGoogleAccountDialog;->setRequestCode(I)V

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->noGoogleAccountDialog:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$NoGoogleAccountDialog;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showDialogIfNotVisible(Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;)V

    .line 445
    return-void
.end method

.method protected startActivityForResult(Landroid/content/Intent;I)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 135
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 136
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->checkAsyncScopeIsValid()V

    .line 137
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->lastRequestCode:I

    .line 138
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "startActivityForResult(intent = %s, requestCode = %d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 139
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 140
    return-void
.end method
