.class Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList$2;
.super Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
.source "CollectionCardBuilderList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList$2;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onDataListRegisteredForInvalidation()V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->onDataListRegisteredForInvalidation()V

    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList$2;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList$2;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->selfObserver:Lcom/google/android/libraries/bind/data/DataObserver;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->access$000(Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;)Lcom/google/android/libraries/bind/data/DataObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 37
    return-void
.end method

.method public onDataListUnregisteredForInvalidation()V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->onDataListUnregisteredForInvalidation()V

    .line 42
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList$2;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList$2;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->selfObserver:Lcom/google/android/libraries/bind/data/DataObserver;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->access$000(Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;)Lcom/google/android/libraries/bind/data/DataObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 43
    return-void
.end method

.method protected refreshIfFailed()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList$2;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->refreshIfFailed(Z)V

    .line 48
    return-void
.end method
