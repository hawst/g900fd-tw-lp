.class Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$1;
.super Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter2;
.source "KeepEditionMenuHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->filterRow(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/android/libraries/bind/data/DataList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

.field final synthetic val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter2;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/Snapshot;)Ljava/util/List;
    .locals 4
    .param p1, "subscriptionList"    # Lcom/google/android/libraries/bind/data/Snapshot;
    .param p2, "pinnedList"    # Lcom/google/android/libraries/bind/data/Snapshot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/Snapshot;",
            "Lcom/google/android/libraries/bind/data/Snapshot;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Snapshot;->getDataForPrimaryValue(Ljava/lang/Object;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 52
    .local v0, "subscriptionData":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_1

    .line 53
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/Data;->copy()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 54
    # getter for: Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->DK_EDITION_PINNED:I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->access$000()I

    move-result v2

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 55
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/Snapshot;->findPositionForPrimaryValue(Ljava/lang/Object;)I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 56
    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    .line 58
    :goto_1
    return-object v1

    .line 55
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 58
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_1
.end method
