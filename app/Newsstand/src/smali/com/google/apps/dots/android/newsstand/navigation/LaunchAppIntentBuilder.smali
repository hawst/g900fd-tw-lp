.class public Lcom/google/apps/dots/android/newsstand/navigation/LaunchAppIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
.source "LaunchAppIntentBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder",
        "<",
        "Lcom/google/apps/dots/android/newsstand/navigation/LaunchAppIntentBuilder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected buildViewCollectionIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 20
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/LaunchAppIntentBuilder;->activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/LaunchAppIntentBuilder;->activity:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->finsky_my_apps_uri:I

    .line 21
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setPath(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected buildViewItemIntent()Landroid/content/Intent;
    .locals 7

    .prologue
    .line 27
    const/4 v0, 0x0

    .line 29
    .local v0, "intent":Landroid/content/Intent;
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/LaunchAppIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/LaunchAppIntentBuilder;->backendDocId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 33
    :goto_0
    if-nez v0, :cond_0

    .line 35
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/LaunchAppIntentBuilder;->activity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/LaunchAppIntentBuilder;->activity:Landroid/app/Activity;

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->finsky_app_store_uri:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/LaunchAppIntentBuilder;->backendDocId:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 36
    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setPath(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v1

    .line 37
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    .line 39
    :cond_0
    return-object v0

    .line 30
    :catch_0
    move-exception v1

    goto :goto_0
.end method
