.class public Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;
.super Lcom/google/apps/dots/android/newsstand/widget/NSWebView;
.source "LinkWidget.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;
.implements Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;
.implements Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;


# static fields
.field private static final LOADED_TIMEOUT_MS:I = 0x1388

.field private static final MINIMUM_LOADED_PERCENTAGE:I = 0x4b


# instance fields
.field private articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

.field private loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;)Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    return-object v0
.end method


# virtual methods
.method public getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    return-object v0
.end method

.method protected init()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 45
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->init()V

    .line 49
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 51
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 53
    .local v0, "settings":Landroid/webkit/WebSettings;
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 54
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 56
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 57
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 59
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 60
    return-void
.end method

.method public loadDelayedContents(Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "optLoadedRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->isNotLoadedOrFailed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadedRunnable(Ljava/lang/Runnable;)V

    .line 139
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADING:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;)V

    .line 140
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 148
    if-eqz p1, :cond_0

    .line 149
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 152
    :cond_0
    return-void
.end method

.method protected notifyArticleOverscrolled(Z)V
    .locals 1
    .param p1, "top"    # Z

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    invoke-interface {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticleOverscrolled(Landroid/view/View;Z)V

    .line 94
    :cond_0
    return-void
.end method

.method protected notifyArticlePageChanged(Z)V
    .locals 1
    .param p1, "userDriven"    # Z

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    .line 100
    :cond_0
    return-void
.end method

.method protected notifyArticleScrolled(ZI)V
    .locals 6
    .param p1, "userDriven"    # Z
    .param p2, "scrollDelta"    # I

    .prologue
    const/4 v1, 0x0

    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_1

    .line 81
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->getScrollY()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 82
    .local v2, "scrollOffset":I
    if-nez v2, :cond_0

    .line 83
    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->computeVerticalScrollRange()I

    move-result v3

    move-object v1, p0

    move v4, p2

    move v5, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticleScrolled(Landroid/view/View;IIIZ)V

    .line 88
    .end local v2    # "scrollOffset":I
    :cond_1
    return-void
.end method

.method protected notifyArticleUnhandledClick()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    invoke-interface {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticleUnhandledClick(Landroid/view/View;)V

    .line 109
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0, p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V

    .line 67
    :cond_0
    return-void
.end method

.method public onLoadStateChanged(Landroid/view/View;Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "loadState"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;Ljava/lang/Throwable;)V

    .line 166
    return-void
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 1
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    .line 119
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->onOverScrolled(IIZZ)V

    .line 120
    if-eqz p4, :cond_0

    .line 121
    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->notifyArticleOverscrolled(Z)V

    .line 123
    :cond_0
    return-void

    .line 121
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onProgressChanged(I)V
    .locals 2
    .param p1, "newProgress"    # I

    .prologue
    .line 156
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->onProgressChanged(I)V

    .line 157
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADING:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne v0, v1, :cond_0

    const/16 v0, 0x4b

    if-lt p1, v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;)V

    .line 161
    :cond_0
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 2
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 113
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->onScrollChanged(IIII)V

    .line 114
    const/4 v0, 0x1

    sub-int v1, p2, p4

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->notifyArticleScrolled(ZI)V

    .line 115
    return-void
.end method

.method public setArticleEventHandler(Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;)V
    .locals 0
    .param p1, "articleEventHandler"    # Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    .line 76
    return-void
.end method

.method public setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V
    .locals 1
    .param p1, "eventHandler"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadStateEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V

    .line 133
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 0
    .param p1, "isUserVisible"    # Z

    .prologue
    .line 170
    if-eqz p1, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->onResume()V

    .line 178
    :goto_0
    return-void

    .line 176
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->onPause()V

    goto :goto_0
.end method

.method public setup(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "postTitle"    # Ljava/lang/String;
    .param p2, "postUrl"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 71
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->loadUrl(Ljava/lang/String;)V

    .line 72
    return-void

    .line 70
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
