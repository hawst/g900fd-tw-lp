.class public abstract Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
.super Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;
.source "NSFragmentDataStatePagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter$HeaderAwareLoadingFragment;
    }
.end annotation


# instance fields
.field private errorMessageDataProvider:Lcom/google/android/libraries/bind/data/DataProvider;

.field protected final headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 32
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 33
    return-void
.end method

.method private showEmptyView()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->showErrorView()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showErrorView()Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 117
    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getFragment(I)Landroid/support/v4/app/Fragment;
    .locals 3
    .param p1, "visualPosition"    # I

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->isInvalidPosition(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 52
    if-nez p1, :cond_2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->showErrorView()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 53
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->errorMessageDataProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 55
    .local v0, "errorMessageData":Lcom/google/android/libraries/bind/data/Data;
    :goto_0
    if-eqz v0, :cond_0

    .line 56
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->addHeaderPadding(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/android/libraries/bind/data/Data;

    .line 58
    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;->setActionMessageData(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;

    move-result-object v1

    .line 61
    .end local v0    # "errorMessageData":Lcom/google/android/libraries/bind/data/Data;
    :goto_1
    return-object v1

    .line 53
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->errorMessageDataProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    .line 54
    invoke-interface {v1}, Lcom/google/android/libraries/bind/data/DataProvider;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    goto :goto_0

    .line 61
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->getFragment(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    goto :goto_1
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v0, -0x2

    .line 103
    const-string v1, "NSFragmentDataStatePagerAdapter_loadingKey"

    if-ne p1, v1, :cond_1

    .line 104
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->showEmptyView()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    .line 111
    :cond_0
    :goto_0
    return v0

    .line 106
    :cond_1
    const-string v1, "NSFragmentDataStatePagerAdapter_errorKey"

    if-eq p1, v1, :cond_0

    .line 111
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public getKey(I)Ljava/lang/Object;
    .locals 1
    .param p1, "visualPosition"    # I

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->isInvalidPosition(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 77
    if-nez p1, :cond_1

    .line 78
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->showErrorView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "NSFragmentDataStatePagerAdapter_errorKey"

    .line 86
    :goto_0
    return-object v0

    .line 81
    :cond_0
    const-string v0, "NSFragmentDataStatePagerAdapter_loadingKey"

    goto :goto_0

    .line 84
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 86
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->getKey(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v0

    goto :goto_0
.end method

.method protected makeLoadingFragment()Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    .line 66
    new-instance v1, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter$HeaderAwareLoadingFragment;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter$HeaderAwareLoadingFragment;-><init>()V

    .line 67
    .local v1, "fragment":Lcom/google/android/libraries/bind/fragment/LoadingFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 68
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "headerHeightPx"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 69
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v3

    .line 68
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 70
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/bind/fragment/LoadingFragment;->setArguments(Landroid/os/Bundle;)V

    .line 71
    return-object v1
.end method

.method public refreshErrorView()Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->showErrorView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->notifyDataSetChanged()V

    .line 46
    :cond_0
    return-object p0
.end method

.method public setErrorMessageDataProvider(Lcom/google/android/libraries/bind/data/DataProvider;)Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
    .locals 0
    .param p1, "provider"    # Lcom/google/android/libraries/bind/data/DataProvider;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->errorMessageDataProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    .line 38
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->refreshErrorView()Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 39
    return-object p0
.end method
