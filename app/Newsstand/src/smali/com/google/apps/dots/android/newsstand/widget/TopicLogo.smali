.class public Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;
.super Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;
.source "TopicLogo.java"


# static fields
.field public static final TINT_OPACITY:F = 0.6f


# instance fields
.field private backgroundColor:I

.field private bindBackgroundColorIdKey:Ljava/lang/Integer;

.field private bindEntityImageIdKey:Ljava/lang/Integer;

.field private bindInitialsKey:Ljava/lang/Integer;

.field private bindSubscribedKey:Ljava/lang/Integer;

.field private bindTitleKey:Ljava/lang/Integer;

.field private entityImageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

.field private entityImageViewBackground:Landroid/view/View;

.field private final fallbackColor:I

.field private initialBackgroundColor:I

.field private initialEntityImageId:Ljava/lang/String;

.field private initialInitials:Ljava/lang/String;

.field private initialSubscribed:Z

.field private initialTitle:Ljava/lang/String;

.field private initialsView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

.field private subscribed:Z

.field private titleView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lcom/google/android/apps/newsstanddev/R$color;->card_background_purple:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->fallbackColor:I

    .line 62
    sget-object v2, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 63
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_layoutId:I

    sget v5, Lcom/google/android/apps/newsstanddev/R$layout;->topic_logo:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 64
    .local v1, "layoutResourceId":I
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 66
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->entityImageViewBackground:I

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->entityImageViewBackground:Landroid/view/View;

    .line 67
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->entityImageView:I

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->entityImageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 68
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->initialsView:I

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialsView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 69
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->titleView:I

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->titleView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 71
    if-nez v0, :cond_3

    move-object v2, v3

    :goto_0
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialEntityImageId:Ljava/lang/String;

    .line 72
    if-nez v0, :cond_4

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->fallbackColor:I

    .line 73
    :goto_1
    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialBackgroundColor:I

    .line 74
    if-nez v0, :cond_5

    move-object v2, v3

    :goto_2
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialInitials:Ljava/lang/String;

    .line 75
    if-nez v0, :cond_6

    :goto_3
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialTitle:Ljava/lang/String;

    .line 78
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->isInEditMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_editModeBackgroundColor:I

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialBackgroundColor:I

    .line 80
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialBackgroundColor:I

    .line 81
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_editModeInitials:I

    .line 82
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialInitials:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ObjectUtil;->coalesce(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialInitials:Ljava/lang/String;

    .line 83
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_editModeTitle:I

    .line 84
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialTitle:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ObjectUtil;->coalesce(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialTitle:Ljava/lang/String;

    .line 87
    :cond_0
    if-eqz v0, :cond_1

    .line 88
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initializeBindings(Landroid/content/res/TypedArray;)V

    .line 89
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 92
    :cond_1
    sget-object v2, Lcom/google/android/apps/newsstanddev/R$styleable;->Subscribable:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 93
    if-nez v0, :cond_7

    move v2, v4

    .line 94
    :goto_4
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialSubscribed:Z

    .line 95
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->Subscribable_bindIsSubscribed:I

    invoke-static {v0, v2}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindSubscribedKey:Ljava/lang/Integer;

    .line 96
    if-eqz v0, :cond_2

    .line 97
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 100
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialEntityImageId:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->setEntityImageId(Ljava/lang/String;)V

    .line 101
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialBackgroundColor:I

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->setBackgroundColor(I)V

    .line 102
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialInitials:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->setInitials(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialTitle:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->setTitle(Ljava/lang/CharSequence;)V

    .line 104
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialSubscribed:Z

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->setSubscribed(Z)V

    .line 105
    return-void

    .line 71
    :cond_3
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_entityImageId:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 72
    :cond_4
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_backgroundColor:I

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->fallbackColor:I

    .line 73
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    goto/16 :goto_1

    .line 74
    :cond_5
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_initials:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 75
    :cond_6
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_title:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    .line 93
    :cond_7
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->Subscribable_isSubscribed:I

    .line 94
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    goto :goto_4
.end method


# virtual methods
.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->backgroundColor:I

    return v0
.end method

.method protected initializeBindings(Landroid/content/res/TypedArray;)V
    .locals 1
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    .line 108
    sget v0, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_bindEntityImageId:I

    invoke-static {p1, v0}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindEntityImageIdKey:Ljava/lang/Integer;

    .line 109
    sget v0, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_bindBackgroundColor:I

    invoke-static {p1, v0}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindBackgroundColorIdKey:Ljava/lang/Integer;

    .line 110
    sget v0, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_bindInitials:I

    invoke-static {p1, v0}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindInitialsKey:Ljava/lang/Integer;

    .line 111
    sget v0, Lcom/google/android/apps/newsstanddev/R$styleable;->TopicLogo_bindTitle:I

    invoke-static {p1, v0}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindTitleKey:Ljava/lang/Integer;

    .line 112
    return-void
.end method

.method public isSubscribed()Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->subscribed:Z

    return v0
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 119
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->backgroundColor:I

    if-eq v0, p1, :cond_0

    .line 120
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->backgroundColor:I

    .line 121
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->updateTint()V

    .line 122
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->updateInitialsBackground()V

    .line 124
    :cond_0
    return-void
.end method

.method public setEntityImageId(Ljava/lang/String;)V
    .locals 1
    .param p1, "imageId"    # Ljava/lang/String;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->entityImageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->entityImageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentId(Ljava/lang/String;)V

    .line 177
    :cond_0
    return-void
.end method

.method public setInitials(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "initials"    # Ljava/lang/CharSequence;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialsView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    return-void
.end method

.method public setSubscribed(Z)V
    .locals 1
    .param p1, "subscribed"    # Z

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->subscribed:Z

    if-eq v0, p1, :cond_0

    .line 132
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->subscribed:Z

    .line 133
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->updateTint()V

    .line 134
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->updateInitialsBackground()V

    .line 136
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->titleView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->titleView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    :cond_0
    return-void
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 4
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v2, 0x0

    .line 191
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 195
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindEntityImageIdKey:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 196
    if-nez p1, :cond_5

    move-object v1, v2

    .line 197
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialEntityImageId:Ljava/lang/String;

    .line 196
    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/util/ObjectUtil;->coalesce(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->setEntityImageId(Ljava/lang/String;)V

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindBackgroundColorIdKey:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 203
    if-nez p1, :cond_6

    move-object v0, v2

    .line 205
    .local v0, "backgroundColorRef":Ljava/lang/Integer;
    :goto_1
    if-eqz v0, :cond_7

    .line 206
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->setBackgroundColor(I)V

    .line 212
    .end local v0    # "backgroundColorRef":Ljava/lang/Integer;
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindInitialsKey:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 213
    if-nez p1, :cond_8

    move-object v1, v2

    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialInitials:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/util/ObjectUtil;->coalesce(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->setInitials(Ljava/lang/CharSequence;)V

    .line 217
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindTitleKey:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 218
    if-nez p1, :cond_9

    .line 219
    :goto_4
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialTitle:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/util/ObjectUtil;->coalesce(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 218
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->setTitle(Ljava/lang/CharSequence;)V

    .line 222
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindSubscribedKey:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 223
    if-nez p1, :cond_a

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialSubscribed:Z

    :goto_5
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->setSubscribed(Z)V

    .line 226
    :cond_4
    return-void

    .line 196
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindEntityImageIdKey:Ljava/lang/Integer;

    .line 197
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 203
    :cond_6
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindBackgroundColorIdKey:Ljava/lang/Integer;

    .line 204
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 208
    .restart local v0    # "backgroundColorRef":Ljava/lang/Integer;
    :cond_7
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialBackgroundColor:I

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->setBackgroundColor(I)V

    goto :goto_2

    .line 213
    .end local v0    # "backgroundColorRef":Ljava/lang/Integer;
    :cond_8
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindInitialsKey:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 218
    :cond_9
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindTitleKey:Ljava/lang/Integer;

    .line 219
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 223
    :cond_a
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->bindSubscribedKey:Ljava/lang/Integer;

    .line 224
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialSubscribed:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v1

    goto :goto_5
.end method

.method protected updateInitialsBackground()V
    .locals 3

    .prologue
    .line 143
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->subscribed:Z

    if-eqz v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialsView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->solid_color_circle_bg:I

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setBackgroundResource(I)V

    .line 145
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialsView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 146
    .local v0, "initialsBackground":Landroid/graphics/drawable/Drawable;
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->backgroundColor:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 150
    .end local v0    # "initialsBackground":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->initialsView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->white_outline_circle_bg:I

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected updateTint()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 153
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->subscribed:Z

    if-eqz v2, :cond_1

    .line 154
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$integer;->subscribed_topic_black_percentage:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 156
    .local v0, "blackPercentage":I
    :goto_0
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->backgroundColor:I

    invoke-static {v2, v0}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorPercentBlack(II)I

    move-result v1

    .line 157
    .local v1, "tintColor":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->entityImageViewBackground:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 158
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->entityImageViewBackground:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 159
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->titleView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    if-eqz v2, :cond_0

    .line 160
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->subscribed:Z

    if-eqz v2, :cond_2

    .line 162
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->titleView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 163
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$dimen;->explore_topics_title_shadow_radius:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const/high16 v4, -0x1000000

    .line 162
    invoke-virtual {v2, v3, v5, v5, v4}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setShadowLayer(FFFI)V

    .line 165
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->entityImageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->backgroundColor:I

    .line 166
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$integer;->subscribed_topic_black_percentage:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    const/4 v5, 0x1

    .line 165
    invoke-static {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorFilterPercentBlack(IIZ)Landroid/graphics/ColorFilter;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 171
    :cond_0
    :goto_1
    return-void

    .line 155
    .end local v0    # "blackPercentage":I
    .end local v1    # "tintColor":I
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$integer;->unsubscribed_topic_black_percentage:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    goto :goto_0

    .line 168
    .restart local v0    # "blackPercentage":I
    .restart local v1    # "tintColor":I
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/TopicLogo;->titleView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v2, v5, v5, v5, v4}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setShadowLayer(FFFI)V

    goto :goto_1
.end method
