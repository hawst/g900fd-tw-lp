.class final Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardNewsItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;ZZZZLjava/lang/String;Lcom/google/android/libraries/bind/data/Data;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$postId:Ljava/lang/String;

.field final synthetic val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

.field final synthetic val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;->val$postId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 298
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    const/4 v1, 0x1

    .line 299
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->track(Z)V

    .line 300
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;->val$postId:Ljava/lang/String;

    .line 301
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 302
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 303
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->setOwningEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    move-result-object v0

    .line 304
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->startIfOnline()V

    .line 305
    return-void
.end method
