.class public Lcom/google/apps/dots/android/newsstand/provider/NSStoreProvidelet;
.super Lcom/google/apps/dots/android/newsstand/provider/AbstractProvidelet;
.source "NSStoreProvidelet.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/apps/dots/android/newsstand/provider/NSStoreProvidelet;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/NSStoreProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/provider/AbstractProvidelet;-><init>()V

    return-void
.end method


# virtual methods
.method public getContentType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 22
    const-string v0, "application/octet-stream"

    return-object v0
.end method

.method public openAssetFile(ILandroid/net/Uri;Ljava/lang/String;Landroid/content/ContentProvider;)Landroid/content/res/AssetFileDescriptor;
    .locals 5
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "mode"    # Ljava/lang/String;
    .param p4, "contentProvider"    # Landroid/content/ContentProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 30
    :try_start_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v2

    .line 31
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    .line 32
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->parse(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v4

    .line 30
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 32
    invoke-interface {v2}, Lcom/google/common/util/concurrent/ListenableFuture;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    .line 33
    .local v0, "storeResponse":Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeAssetFileDescriptor()Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 34
    .end local v0    # "storeResponse":Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    :catch_0
    move-exception v1

    .line 35
    .local v1, "tr":Ljava/lang/Throwable;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/provider/NSStoreProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    .line 36
    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-direct {v2}, Ljava/io/FileNotFoundException;-><init>()V

    throw v2
.end method
