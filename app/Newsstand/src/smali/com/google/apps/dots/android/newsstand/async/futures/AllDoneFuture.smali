.class Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;
.super Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;
.source "AllDoneFuture.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final allMustSucceed:Z

.field private final remaining:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method constructor <init>(Ljava/util/Collection;Z)V
    .locals 4
    .param p2, "allMustSucceed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;Z)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "futures":Ljava/util/Collection;, "Ljava/util/Collection<+Lcom/google/common/util/concurrent/ListenableFuture<*>;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;-><init>()V

    .line 22
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->allMustSucceed:Z

    .line 23
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->remaining:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 26
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->set(Ljava/lang/Object;)Z

    .line 52
    :goto_0
    return-void

    .line 31
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 32
    .local v0, "listenable":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    new-instance v2, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture$1;

    invoke-direct {v2, p0, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture$1;-><init>(Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v2, v3}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 41
    .end local v0    # "listenable":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    :cond_1
    new-instance v1, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture$2;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture$2;-><init>(Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;Ljava/util/Collection;)V

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;Ljava/util/concurrent/Future;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;
    .param p1, "x1"    # Ljava/util/concurrent/Future;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->setOneValue(Ljava/util/concurrent/Future;)V

    return-void
.end method

.method private setExceptionAndMaybeLog(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->allMustSucceed:Z

    if-eqz v0, :cond_0

    .line 64
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->setException(Ljava/lang/Throwable;)Z

    .line 66
    :cond_0
    return-void
.end method

.method private setOneValue(Ljava/util/concurrent/Future;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .local p1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->isDone()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 77
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->allMustSucceed:Z

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    move v3, v5

    :goto_0
    const-string v6, "Future was done before all dependencies completed"

    invoke-static {v3, v6}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 82
    :cond_1
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v3

    const-string v6, "Tried to set value from future which is not done"

    invoke-static {v3, v6}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 84
    invoke-static {p1}, Lcom/google/common/util/concurrent/Uninterruptibles;->getUninterruptibly(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->remaining:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    .line 97
    .local v1, "newRemaining":I
    if-ltz v1, :cond_4

    :goto_1
    const-string v3, "Less than 0 remaining futures"

    invoke-static {v5, v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 98
    if-nez v1, :cond_2

    .line 99
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->set(Ljava/lang/Object;)Z

    .line 102
    :cond_2
    :goto_2
    return-void

    .end local v1    # "newRemaining":I
    :cond_3
    move v3, v4

    .line 77
    goto :goto_0

    .restart local v1    # "newRemaining":I
    :cond_4
    move v5, v4

    .line 97
    goto :goto_1

    .line 85
    .end local v1    # "newRemaining":I
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Ljava/util/concurrent/CancellationException;
    :try_start_1
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->allMustSucceed:Z

    if-eqz v3, :cond_5

    .line 89
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->cancel(Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->remaining:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    .line 97
    .restart local v1    # "newRemaining":I
    if-ltz v1, :cond_6

    :goto_3
    const-string v3, "Less than 0 remaining futures"

    invoke-static {v5, v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 98
    if-nez v1, :cond_2

    .line 99
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->set(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    move v5, v4

    .line 97
    goto :goto_3

    .line 91
    .end local v0    # "e":Ljava/util/concurrent/CancellationException;
    .end local v1    # "newRemaining":I
    :catch_1
    move-exception v0

    .line 92
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_2
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->setExceptionAndMaybeLog(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 96
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->remaining:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    .line 97
    .restart local v1    # "newRemaining":I
    if-ltz v1, :cond_7

    :goto_4
    const-string v3, "Less than 0 remaining futures"

    invoke-static {v5, v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 98
    if-nez v1, :cond_2

    .line 99
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->set(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    move v5, v4

    .line 97
    goto :goto_4

    .line 93
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    .end local v1    # "newRemaining":I
    :catch_2
    move-exception v2

    .line 94
    .local v2, "t":Ljava/lang/Throwable;
    :try_start_3
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->setExceptionAndMaybeLog(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 96
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->remaining:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    .line 97
    .restart local v1    # "newRemaining":I
    if-ltz v1, :cond_8

    :goto_5
    const-string v3, "Less than 0 remaining futures"

    invoke-static {v5, v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 98
    if-nez v1, :cond_2

    .line 99
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->set(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    move v5, v4

    .line 97
    goto :goto_5

    .line 96
    .end local v1    # "newRemaining":I
    .end local v2    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->remaining:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    .line 97
    .restart local v1    # "newRemaining":I
    if-ltz v1, :cond_a

    :goto_6
    const-string v4, "Less than 0 remaining futures"

    invoke-static {v5, v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 98
    if-nez v1, :cond_9

    .line 99
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->set(Ljava/lang/Object;)Z

    .line 101
    :cond_9
    throw v3

    :cond_a
    move v5, v4

    .line 97
    goto :goto_6
.end method
