.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$6;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "MagazineEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->fetchMostRecentlyReadPost()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 485
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 485
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$6;->onSuccess(Ljava/lang/String;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/String;)V
    .locals 1
    .param p1, "mostRecentPostId"    # Ljava/lang/String;

    .prologue
    .line 488
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateContinueReadingPost(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2100(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Ljava/lang/String;)V

    .line 491
    :cond_0
    return-void
.end method
