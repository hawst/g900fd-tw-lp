.class public Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;
.super Landroid/view/OrientationEventListener;
.source "UnlockOrientationListener.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final nsActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field private final targetOrientationDegrees:I

.field private final targetRequestedOrientation:I

.field private final thresholdDegrees:I

.field private watchForExit:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;II)V
    .locals 1
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "targetRequestedOrientation"    # I
    .param p3, "targetOrientationDegrees"    # I

    .prologue
    .line 28
    const/16 v0, 0x28

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;III)V

    .line 29
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;III)V
    .locals 0
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "targetRequestedOrientation"    # I
    .param p3, "targetOrientationDegrees"    # I
    .param p4, "thresholdDegrees"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    .line 34
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->nsActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 35
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->targetRequestedOrientation:I

    .line 36
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->targetOrientationDegrees:I

    .line 37
    iput p4, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->thresholdDegrees:I

    .line 38
    return-void
.end method

.method private withinThreshold(I)Z
    .locals 2
    .param p1, "orientationDegrees"    # I

    .prologue
    .line 56
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->targetOrientationDegrees:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->thresholdDegrees:I

    rsub-int v0, v0, 0x168

    if-ge p1, v0, :cond_1

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->thresholdDegrees:I

    if-le p1, v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->targetOrientationDegrees:I

    sub-int/2addr v0, p1

    .line 59
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->thresholdDegrees:I

    if-gt v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 6
    .param p1, "orientation"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 42
    sget-object v0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onOrientationChanged(%d): target=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->targetOrientationDegrees:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->watchForExit:Z

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->withinThreshold(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->nsActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->removeOrientationListener(Landroid/view/OrientationEventListener;)V

    .line 48
    sget-object v0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "setting requested orientation to %d"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->targetRequestedOrientation:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->nsActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->targetRequestedOrientation:I

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 50
    :cond_2
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->watchForExit:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->withinThreshold(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;->watchForExit:Z

    goto :goto_0
.end method
