.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;
.super Ljava/lang/Object;
.source "Syncer.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureFallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->startWithRetry(II)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureFallback",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

.field final synthetic val$currentTry:I

.field final synthetic val$numTries:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;II)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    .prologue
    .line 270
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    iput p2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->val$currentTry:I

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->val$numTries:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;"
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 273
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->val$currentTry:I

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->val$numTries:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->isFatal(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->shouldNotBeRetried(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    :cond_0
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: Try %d/%d failed. Aborting."

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    aput-object v3, v2, v4

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->val$currentTry:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->val$numTries:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->onFailure(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 279
    :goto_0
    return-object v0

    .line 278
    :cond_1
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: Try %d/%d failed. Retrying."

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    aput-object v3, v2, v4

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->val$currentTry:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->val$numTries:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 279
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->val$currentTry:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;->val$numTries:I

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->startWithRetry(II)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
