.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
.super Ljava/lang/Object;
.source "NativeBodyBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PartContext"
.end annotation


# instance fields
.field eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

.field isEventScopeOwner:Z

.field final location:Landroid/graphics/RectF;

.field model:Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;

.field view:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->location:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method public eventScope(Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .locals 1
    .param p1, "eventScope"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->isEventScopeOwner:Z

    .line 126
    return-object p0
.end method

.method public forChild()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .locals 3

    .prologue
    .line 111
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;-><init>()V

    .line 113
    .local v0, "partContext":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    iput-object v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    .line 114
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->location:Landroid/graphics/RectF;

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->location:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 115
    return-object v0
.end method

.method public location(Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .locals 3
    .param p1, "relativeLocation"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    .prologue
    .line 134
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->location:Landroid/graphics/RectF;

    iget v0, v2, Landroid/graphics/RectF;->left:F

    .line 135
    .local v0, "offsetX":F
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->location:Landroid/graphics/RectF;

    iget v1, v2, Landroid/graphics/RectF;->top:F

    .line 136
    .local v1, "offsetY":F
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->location:Landroid/graphics/RectF;

    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyUtil;->toRectF(Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    .line 137
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->location:Landroid/graphics/RectF;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 138
    return-object p0
.end method

.method public view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view:Landroid/view/View;

    .line 120
    return-object p0
.end method
