.class Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$6;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardListVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeTopicPostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

.field final synthetic val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .prologue
    .line 645
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$6;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$6;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$6;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 649
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$6;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$6;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$6;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    const/4 v1, 0x1

    .line 650
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->track(Z)V

    .line 653
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$6;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .line 654
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$6;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v1, v1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 655
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v0

    .line 656
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->start()V

    .line 657
    return-void
.end method
