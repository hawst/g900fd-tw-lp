.class public Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;
.super Ljava/lang/Object;
.source "NSLoadingViewProvider.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/ViewProvider;


# instance fields
.field protected headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 21
    return-void
.end method


# virtual methods
.method public getView(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    const/4 v3, 0x0

    .line 30
    new-instance v0, Lcom/google/android/libraries/bind/widget/LoadingView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/bind/widget/LoadingView;-><init>(Landroid/content/Context;)V

    .line 31
    .local v0, "loadingView":Landroid/view/View;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 32
    return-object v0
.end method

.method public setHeaderType(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;
    .locals 0
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 25
    return-object p0
.end method
