.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;
.super Ljava/lang/Object;
.source "MagazineEditionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->centerOnPostId(Ljava/lang/String;Ljava/lang/Integer;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

.field final synthetic val$cardData:Lcom/google/android/libraries/bind/data/Data;

.field final synthetic val$positionToScrollTo:I

.field final synthetic val$smoothScroll:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/android/libraries/bind/data/Data;IZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 1015
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->val$cardData:Lcom/google/android/libraries/bind/data/Data;

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->val$positionToScrollTo:I

    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->val$smoothScroll:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1018
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->val$cardData:Lcom/google/android/libraries/bind/data/Data;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getExpectedCardHeight(ILcom/google/android/libraries/bind/data/Data;)I
    invoke-static {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2600(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;ILcom/google/android/libraries/bind/data/Data;)I

    move-result v0

    .line 1024
    .local v0, "cardHeight":I
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->val$positionToScrollTo:I

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->currentlyCenteredPosition:I
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$500(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)I

    move-result v4

    if-ge v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 1025
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/app/ActionBar;->getHeight()I

    move-result v3

    mul-int/lit8 v1, v3, 0x2

    .line 1026
    .local v1, "headerHeight":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 1027
    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getHeight()I

    move-result v3

    sub-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v1

    div-int/lit8 v4, v0, 0x2

    sub-int v2, v3, v4

    .line 1033
    .local v2, "pixelsFromTop":I
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->val$smoothScroll:Z

    if-eqz v3, :cond_1

    .line 1034
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v3

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->val$positionToScrollTo:I

    const/16 v5, 0x12c

    invoke-virtual {v3, v4, v2, v5}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->smoothScrollToPositionFromTop(III)V

    .line 1039
    :goto_1
    return-void

    .line 1025
    .end local v1    # "headerHeight":I
    .end local v2    # "pixelsFromTop":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1037
    .restart local v1    # "headerHeight":I
    .restart local v2    # "pixelsFromTop":I
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v3

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;->val$positionToScrollTo:I

    invoke-virtual {v3, v4, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setSelectionFromTop(II)V

    goto :goto_1
.end method
