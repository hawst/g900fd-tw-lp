.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1;
.super Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
.source "ExploreTopicsList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList;->processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList;
    .param p2, "primaryKey"    # I

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;)V
    .locals 7
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "exploreGroupSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 52
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->getId()Ljava/lang/String;

    move-result-object v1

    .line 53
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "name":Ljava/lang/String;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 55
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v2, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .local v3, "topic":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsListItem;->LAYOUTS:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 58
    sget v4, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-virtual {v0, v4, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 59
    sget v4, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsListItem;->DK_TOPIC_NAME:I

    invoke-virtual {v0, v4, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 60
    sget v4, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsListItem;->DK_ON_CLICK_LISTENER:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1$1;

    invoke-direct {v5, p0, v3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;)V

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 68
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->hasIconImage()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 69
    sget v4, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsListItem;->DK_ENTITY_IMAGE_ID:I

    .line 70
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v5

    .line 69
    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 72
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 73
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;)V
    .locals 4
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "exploreLinkSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    .prologue
    .line 77
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 79
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->LAYOUTS:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 80
    sget v1, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    const-string v2, "special_offers"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 81
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_BACKGROUND:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->explore_topics_background_offers:I

    .line 82
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 81
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 83
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_ICON_DRAWABLE:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_offers:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 84
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_TITLE:I

    .line 85
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->offers_title:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 84
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 86
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_ON_CLICK_LISTENER:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 92
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 94
    .end local v0    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;)V

    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList$1;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;)V

    return-void
.end method
