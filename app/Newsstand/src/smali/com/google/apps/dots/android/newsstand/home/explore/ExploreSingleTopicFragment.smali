.class public Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "ExploreSingleTopicFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private connectivityListener:Ljava/lang/Runnable;

.field private headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x0

    const-string v1, "ExploreSingleTopicFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->explore_single_topic_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->updateErrorView()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    return-object v0
.end method

.method private getOnClickListenerForShelfType(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "shelfType"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .prologue
    .line 196
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$5;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V

    return-object v0
.end method

.method private refreshIfNeeded()V
    .locals 4

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 293
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 292
    invoke-static {v2, v3, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->getServerUriForExploreTopic(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;)Ljava/lang/String;

    move-result-object v0

    .line 294
    .local v0, "singleTopicUri":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->refreshIfNeeded(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    .line 295
    return-void
.end method

.method private sendAnalyticsEvent(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;)V
    .locals 2
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    .prologue
    .line 261
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->NONE:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    if-ne v0, v1, :cond_0

    .line 263
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ExploreSingleTopicScreen;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ExploreSingleTopicScreen;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ExploreSingleTopicScreen;->track(Z)V

    .line 265
    :cond_0
    return-void
.end method

.method private setUpAdapter()V
    .locals 3

    .prologue
    .line 120
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    .line 121
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$2;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 133
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 134
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 135
    return-void
.end method

.method private setUpErrorView()V
    .locals 2

    .prologue
    .line 212
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$6;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 218
    return-void
.end method

.method private setupHeaderListLayout()V
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 115
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 116
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 115
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 117
    return-void
.end method

.method private updateActionBar()V
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->setActionBarTitle(Ljava/lang/CharSequence;)V

    .line 258
    :cond_0
    return-void
.end method

.method private updateAdapter()V
    .locals 7

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    iget-object v6, v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 140
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    invoke-direct {v1, v5, v6, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V

    .line 141
    .local v1, "cardList":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 145
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->cardListBuilder()Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->useSpacerTopPadding(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 147
    .local v0, "builtCardList":Lcom/google/android/libraries/bind/data/DataList;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->TOPICS:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .line 148
    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->getOnClickListenerForShelfType(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 149
    .local v3, "expandTopicsShelfListener":Landroid/view/View$OnClickListener;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->NEWS:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .line 150
    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->getOnClickListenerForShelfType(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 152
    .local v2, "expandNewsShelfListener":Landroid/view/View$OnClickListener;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$3;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    invoke-direct {v4, p0, v5, v3, v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Ljava/util/concurrent/Executor;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 182
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 184
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_0

    .line 186
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    const/4 v5, 0x2

    new-instance v6, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$4;

    invoke-direct {v6, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)V

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnChildrenAddedListener(ILjava/lang/Runnable;)V

    .line 193
    :cond_0
    return-void
.end method

.method private updateErrorView()V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->refreshErrorView()Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 234
    return-void
.end method


# virtual methods
.method protected createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 1

    .prologue
    .line 299
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)V

    return-object v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 222
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 223
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_explore_category"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const-string v2, "exploreTopicInfo"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    return-object v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 269
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onDestroyView()V

    .line 270
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->connectivityListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 271
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 273
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 277
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onResume()V

    .line 278
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->getMsSincePause()J

    move-result-wide v0

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 279
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->refreshIfNeeded()V

    .line 281
    :cond_0
    return-void
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 70
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 71
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->setupHeaderListLayout()V

    .line 72
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 73
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->setUpAdapter()V

    .line 74
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->setUpErrorView()V

    .line 75
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->updateErrorView()V

    .line 76
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 0
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 285
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->setUserVisibleHint(Z)V

    .line 286
    if-eqz p1, :cond_0

    .line 287
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->refreshIfNeeded()V

    .line 289
    :cond_0
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 49
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;)V
    .locals 3
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    .prologue
    .line 244
    if-eqz p2, :cond_0

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 245
    .local v0, "editionChanged":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 246
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->updateAdapter()V

    .line 247
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->updateErrorView()V

    .line 248
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->updateActionBar()V

    .line 250
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->sendAnalyticsEvent(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;)V

    .line 252
    :cond_1
    return-void

    .line 244
    .end local v0    # "editionChanged":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
