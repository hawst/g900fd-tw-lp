.class final Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;
.super Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.source "MagazineListUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->forMonthFilter(III)Lcom/google/android/libraries/bind/data/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$calendar:Ljava/util/Calendar;

.field final synthetic val$endMonth:I

.field final synthetic val$month:I

.field final synthetic val$year:I


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/Calendar;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 106
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$calendar:Ljava/util/Calendar;

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$year:I

    iput p4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$month:I

    iput p5, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$endMonth:I

    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 3
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 109
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$calendar:Ljava/util/Calendar;

    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->getMonth(Ljava/util/Calendar;Lcom/google/android/libraries/bind/data/Data;)I

    move-result v0

    .line 110
    .local v0, "actualMonth":I
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$year:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$calendar:Ljava/util/Calendar;

    invoke-static {v2, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->getYear(Ljava/util/Calendar;Lcom/google/android/libraries/bind/data/Data;)I

    move-result v2

    if-ne v1, v2, :cond_1

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$month:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$month:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$endMonth:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$month:I

    if-gt v1, v0, :cond_1

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;->val$endMonth:I

    if-gt v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 1
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->getMagazineSortComparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 118
    return-object p1
.end method
