.class public Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;
.super Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;
.source "CurationSubscriptionsList.java"


# instance fields
.field private final curationCardFilter:Lcom/google/android/libraries/bind/data/Filter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;-><init>()V

    .line 19
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;->makeCardFilter(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Lcom/google/android/libraries/bind/data/Filter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;->curationCardFilter:Lcom/google/android/libraries/bind/data/Filter;

    .line 20
    return-void
.end method


# virtual methods
.method public curationsCardList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;->curationCardFilter:Lcom/google/android/libraries/bind/data/Filter;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 24
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyCurations(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
