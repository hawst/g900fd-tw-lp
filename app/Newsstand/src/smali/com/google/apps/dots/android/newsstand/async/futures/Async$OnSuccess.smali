.class public abstract Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnSuccess;
.super Ljava/lang/Object;
.source "Async.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/Async;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OnSuccess"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
        "<TI;TO;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnSuccess;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnSuccess<TI;TO;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public fallback(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 0
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TO;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnSuccess;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnSuccess<TI;TO;>;"
    throw p1
.end method
