.class Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;
.super Lcom/google/apps/dots/android/newsstand/http/NSClientCommand;
.source "NSClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/http/NSClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClientRequestCommand"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/http/NSClientCommand",
        "<",
        "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final clientConfig:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

.field private final isDotsBackend:Z

.field private final locale:Ljava/util/Locale;

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/http/NSClient;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lorg/apache/http/client/HttpClient;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;ZLcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
    .locals 15
    .param p2, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p4, "request"    # Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    .param p5, "isDotsBackend"    # Z
    .param p6, "optClientConfig"    # Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;
        }
    .end annotation

    .prologue
    .line 229
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    .line 230
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    move-object/from16 v0, p3

    invoke-direct {p0, v3, v0}, Lcom/google/apps/dots/android/newsstand/http/NSClientCommand;-><init>(Landroid/accounts/Account;Lorg/apache/http/client/HttpClient;)V

    .line 232
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->uri:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 234
    .local v14, "uri":Landroid/net/Uri;
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->locale:Ljava/util/Locale;

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->locale:Ljava/util/Locale;

    .line 235
    move/from16 v0, p5

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->isDotsBackend:Z

    .line 236
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->clientConfig:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .line 237
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->autoCloseEntityStream:Z

    .line 240
    move-object/from16 v0, p4

    instance-of v3, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$WebViewRequest;

    if-eqz v3, :cond_3

    const-string v13, "webview"

    .line 245
    .local v13, "requestContext":Ljava/lang/String;
    :goto_0
    :try_start_0
    new-instance v2, Ljava/net/URI;

    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    .local v2, "parsedUri":Ljava/net/URI;
    :goto_1
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->optPostData:[B

    if-eqz v3, :cond_4

    .line 258
    new-instance v10, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v10, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 259
    .local v10, "httpPost":Lorg/apache/http/client/methods/HttpPost;
    new-instance v3, Lorg/apache/http/entity/ByteArrayEntity;

    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->optPostData:[B

    invoke-direct {v3, v4}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v10, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 260
    const-string v3, "X-App-XSRF"

    const-string v4, "true"

    invoke-virtual {v10, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string v3, "Content-Type"

    const-string v4, "application/octet-stream"

    invoke-virtual {v10, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    move-object v11, v10

    .line 277
    .end local v10    # "httpPost":Lorg/apache/http/client/methods/HttpPost;
    .local v11, "httpRequest":Lorg/apache/http/client/methods/HttpRequestBase;
    :goto_2
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->priority:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    if-ne v3, v4, :cond_0

    .line 278
    const-string v3, "X-Dots-Background-Sync"

    const-string v4, "true"

    invoke-virtual {v11, v3, v4}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :cond_0
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->locale:Ljava/util/Locale;

    if-eqz v3, :cond_1

    .line 281
    const-string v3, "Accept-Language"

    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_1
    move-object/from16 v0, p4

    instance-of v3, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$WebViewRequest;

    if-eqz v3, :cond_2

    .line 284
    const-string v3, "User-Agent"

    check-cast p4, Lcom/google/apps/dots/android/newsstand/http/NSClient$WebViewRequest;

    .end local p4    # "request":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$WebViewRequest;->userAgent:Ljava/lang/String;

    invoke-virtual {v11, v3, v4}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_2
    invoke-virtual {p0, v11, v13}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->setRequest(Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;)V

    .line 288
    return-void

    .line 240
    .end local v2    # "parsedUri":Ljava/net/URI;
    .end local v11    # "httpRequest":Lorg/apache/http/client/methods/HttpRequestBase;
    .end local v13    # "requestContext":Ljava/lang/String;
    .restart local p4    # "request":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    :cond_3
    const-string v13, "client"

    goto :goto_0

    .line 246
    .restart local v13    # "requestContext":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 252
    .local v8, "e":Ljava/net/URISyntaxException;
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "Error parsing URI"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v8, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 253
    new-instance v2, Ljava/net/URI;

    invoke-virtual {v14}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v6

    .line 254
    invoke-virtual {v14}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v2    # "parsedUri":Ljava/net/URI;
    goto/16 :goto_1

    .line 264
    .end local v8    # "e":Ljava/net/URISyntaxException;
    :cond_4
    new-instance v9, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v9, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 266
    .local v9, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->lastModified:Ljava/lang/Long;

    if-eqz v3, :cond_5

    .line 267
    new-instance v3, Ljava/util/Date;

    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->lastModified:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v3}, Lorg/apache/http/impl/cookie/DateUtils;->formatDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v12

    .line 268
    .local v12, "modified":Ljava/lang/String;
    const-string v3, "If-Modified-Since"

    invoke-virtual {v9, v3, v12}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "%s: conditional if-modified-since: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p4

    iget-object v7, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->uri:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v12, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    .end local v12    # "modified":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->eTag:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 272
    const-string v3, "If-None-Match"

    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->eTag:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "%s: conditional if-none-match: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p4

    iget-object v7, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->uri:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p4

    iget-object v7, v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->eTag:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 275
    :cond_6
    move-object v11, v9

    .restart local v11    # "httpRequest":Lorg/apache/http/client/methods/HttpRequestBase;
    goto/16 :goto_2
.end method

.method private createResponse(Lorg/apache/http/HttpResponse;Ljava/io/InputStream;)Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;
    .locals 7
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .param p2, "in"    # Ljava/io/InputStream;

    .prologue
    .line 320
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;

    const-string v1, "ETag"

    .line 321
    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->getFirstHeaderValue(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 322
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->lastModified(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;

    move-result-object v3

    .line 323
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->expiration(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;

    move-result-object v4

    const-string v1, "Content-Type"

    .line 324
    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->getFirstHeaderValue(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v1, "Content-Encoding"

    .line 325
    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->getFirstHeaderValue(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;-><init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    .local v0, "clientResponse":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "%s: response %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v5}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 327
    return-object v0
.end method


# virtual methods
.method protected getHttpResponse(Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 6
    .param p1, "httpContext"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Ljava/net/URISyntaxException;
        }
    .end annotation

    .prologue
    .line 293
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->isDotsBackend:Z

    if-eqz v3, :cond_4

    .line 294
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 295
    .local v1, "requestUri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 296
    .local v2, "uriBuilder":Landroid/net/Uri$Builder;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSClient;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->access$300(Lcom/google/apps/dots/android/newsstand/http/NSClient;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDesignerMode()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "designerMode"

    .line 298
    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 299
    const-string v3, "designerMode"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 301
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->locale:Ljava/util/Locale;

    if-eqz v3, :cond_1

    const-string v3, "hl"

    .line 303
    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 304
    const-string v3, "hl"

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 306
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSClient;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->access$300(Lcom/google/apps/dots/android/newsstand/http/NSClient;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getCountryOverride()Ljava/lang/String;

    move-result-object v0

    .line 307
    .local v0, "countryParam":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 308
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->clientConfig:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    if-nez v3, :cond_5

    const-string v0, ""

    .line 310
    :cond_2
    :goto_0
    const-string v3, "country"

    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    .line 311
    const-string v3, "country"

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 313
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    new-instance v4, Ljava/net/URI;

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V

    .line 316
    .end local v0    # "countryParam":Ljava/lang/String;
    .end local v1    # "requestUri":Landroid/net/Uri;
    .end local v2    # "uriBuilder":Landroid/net/Uri$Builder;
    :cond_4
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/http/NSClientCommand;->getHttpResponse(Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    return-object v3

    .line 308
    .restart local v0    # "countryParam":Ljava/lang/String;
    .restart local v1    # "requestUri":Landroid/net/Uri;
    .restart local v2    # "uriBuilder":Landroid/net/Uri$Builder;
    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->clientConfig:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getServerCountry()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected handleNoContent(Lorg/apache/http/HttpResponse;)Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;
    .locals 2
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation

    .prologue
    .line 343
    new-instance v0, Ljava/io/ByteArrayInputStream;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->createResponse(Lorg/apache/http/HttpResponse;Ljava/io/InputStream;)Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic handleNoContent(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation

    .prologue
    .line 222
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->handleNoContent(Lorg/apache/http/HttpResponse;)Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;

    move-result-object v0

    return-object v0
.end method

.method protected handleNotModified(Lorg/apache/http/HttpResponse;)Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;
    .locals 5
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation

    .prologue
    .line 348
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: not modified"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 349
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->createResponse(Lorg/apache/http/HttpResponse;Ljava/io/InputStream;)Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic handleNotModified(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation

    .prologue
    .line 222
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->handleNotModified(Lorg/apache/http/HttpResponse;)Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;

    move-result-object v0

    return-object v0
.end method

.method protected handleOk(Lorg/apache/http/HttpEntity;Lorg/apache/http/HttpResponse;)Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;
    .locals 2
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation

    .prologue
    .line 334
    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->createResponse(Lorg/apache/http/HttpResponse;Ljava/io/InputStream;)Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "ioe":Ljava/io/IOException;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/sync/SyncException;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/sync/SyncException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected bridge synthetic handleOk(Lorg/apache/http/HttpEntity;Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation

    .prologue
    .line 222
    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;->handleOk(Lorg/apache/http/HttpEntity;Lorg/apache/http/HttpResponse;)Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;

    move-result-object v0

    return-object v0
.end method
