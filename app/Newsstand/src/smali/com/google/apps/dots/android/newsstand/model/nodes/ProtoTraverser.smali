.class public final Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;
.super Ljava/lang/Object;
.source "ProtoTraverser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final root:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V
    .locals 1
    .param p1, "root"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string v0, "Null collection root"

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->root:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .line 49
    return-void
.end method

.method private traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V
    .locals 1
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;
    .param p3, "root"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor",
            "<-",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;",
            ">;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64
    .local p2, "visitor":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor<-Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;>;"
    iget-object v0, p3, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverseList(Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v0

    iput-object v0, p3, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 65
    return-void
.end method

.method private traverseList(Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 5
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;
    .param p3, "children"    # [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor",
            "<-",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;",
            ">;[",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;",
            ")[",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;"
        }
    .end annotation

    .prologue
    .line 70
    .local p2, "visitor":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor<-Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;>;"
    const/4 v3, 0x0

    .line 71
    .local v3, "newChildren":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;->requestedFinish()Z

    move-result v4

    if-nez v4, :cond_4

    array-length v4, p3

    if-ge v2, v4, :cond_4

    .line 72
    aget-object v0, p3, v2

    .line 74
    .local v0, "child":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;->clearRequest()V

    .line 75
    invoke-interface {p2, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V

    .line 76
    iget-boolean v1, p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;->requestedDeletion:Z

    .line 79
    .local v1, "deletedOnVisit":Z
    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;->requestedFinish()Z

    move-result v4

    if-nez v4, :cond_0

    .line 80
    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {p0, p1, p2, v4}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverseList(Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v4

    iput-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 84
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;->clearRequest()V

    .line 85
    invoke-interface {p2, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;->exit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V

    .line 87
    if-nez v1, :cond_1

    iget-boolean v4, p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;->requestedDeletion:Z

    if-eqz v4, :cond_3

    .line 88
    :cond_1
    if-nez v3, :cond_2

    .line 89
    invoke-virtual {p3}, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clone()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "newChildren":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    check-cast v3, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 91
    .restart local v3    # "newChildren":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_2
    const/4 v4, 0x0

    aput-object v4, v3, v2

    .line 71
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 95
    .end local v0    # "child":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .end local v1    # "deletedOnVisit":Z
    :cond_4
    if-nez v3, :cond_5

    .end local p3    # "children":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :goto_1
    return-object p3

    .restart local p3    # "children":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_5
    const-class v4, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-static {v4, v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->filterNonNull(Ljava/lang/Class;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-object p3, v4

    goto :goto_1
.end method


# virtual methods
.method public traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor",
            "<-",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "visitor":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor<-Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;-><init>()V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->root:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {p0, v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    .line 60
    return-void
.end method
