.class Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout$1;
.super Ljava/lang/Object;
.source "NSPlayDrawerLayout.java"

# interfaces
.implements Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->initializeDrawer(ZLcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout$1;->this$0:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountListToggleButtonClicked(Z)V
    .locals 0
    .param p1, "isListExpanded"    # Z

    .prologue
    .line 105
    return-void
.end method

.method public onCurrentAccountClicked(ZLcom/google/android/finsky/protos/DocumentV2$DocV2;)Z
    .locals 2
    .param p1, "isLoaded"    # Z
    .param p2, "profileDoc"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .prologue
    const/4 v0, 0x1

    .line 60
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getAllGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v1

    array-length v1, v1

    if-le v1, v0, :cond_0

    .line 66
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDownloadToggleClicked(Z)V
    .locals 2
    .param p1, "isDownloadOnly"    # Z

    .prologue
    .line 109
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setOnDeviceOnly(Z)V

    .line 110
    if-eqz p1, :cond_0

    .line 111
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout$1;->this$0:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->access$000(Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;)Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;->showIfSyncNotEnabled(Landroid/accounts/Account;Landroid/support/v4/app/FragmentActivity;)V

    .line 113
    :cond_0
    return-void
.end method

.method public onPrimaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)Z
    .locals 1
    .param p1, "primaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    .prologue
    .line 90
    iget-boolean v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->isActive:Z

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->actionSelectedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 94
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onSecondaryAccountClicked(Ljava/lang/String;)Z
    .locals 5
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->accountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    .line 73
    .local v2, "newAccount":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 74
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->setAccount(Landroid/accounts/Account;)V

    .line 78
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage;->DEFAULT_HOME_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 79
    .local v1, "homePage":Lcom/google/apps/dots/android/newsstand/home/HomePage;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout$1;->this$0:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->access$000(Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;)Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->getActiveNavDrawerEntry()Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    move-result-object v0

    .line 80
    .local v0, "currentNavEntry":Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->entryType:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;->HOME_PAGE:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;

    if-ne v3, v4, :cond_1

    .line 81
    check-cast v0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    .end local v0    # "currentNavEntry":Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 83
    :cond_1
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout$1;->this$0:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->access$000(Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;)Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->clearBackstack()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->start()V

    .line 84
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout$1;->this$0:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->closeDrawer()V

    .line 85
    const/4 v3, 0x1

    return v3
.end method

.method public onSecondaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Z
    .locals 1
    .param p1, "secondaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    .prologue
    .line 99
    iget-object v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;->actionSelectedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 101
    const/4 v0, 0x1

    return v0
.end method
