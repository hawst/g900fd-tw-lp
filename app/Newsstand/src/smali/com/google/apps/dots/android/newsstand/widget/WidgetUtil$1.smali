.class final Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil$1;
.super Ljava/lang/Object;
.source "WidgetUtil.java"

# interfaces
.implements Lcom/google/android/play/onboard/OnboardUtils$Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->getAllDescendants(Landroid/view/ViewGroup;Lcom/google/common/base/Predicate;)Ljava/util/Collection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/play/onboard/OnboardUtils$Predicate",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$optPredicate:Lcom/google/common/base/Predicate;


# direct methods
.method constructor <init>(Lcom/google/common/base/Predicate;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil$1;->val$optPredicate:Lcom/google/common/base/Predicate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Landroid/view/View;)Z
    .locals 1
    .param p1, "object"    # Landroid/view/View;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil$1;->val$optPredicate:Lcom/google/common/base/Predicate;

    invoke-interface {v0, p1}, Lcom/google/common/base/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 148
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil$1;->apply(Landroid/view/View;)Z

    move-result v0

    return v0
.end method
