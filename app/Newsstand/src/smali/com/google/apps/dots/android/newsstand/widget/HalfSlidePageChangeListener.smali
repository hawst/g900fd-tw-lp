.class public Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;
.super Ljava/lang/Object;
.source "HalfSlidePageChangeListener.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field private currentItemAtDragStart:I

.field private final isFrontPage:[Z

.field private final listener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private final pageTranslation:[I

.field private final pager:Landroid/support/v4/view/NSViewPager;

.field private final pagerDropShadow:Landroid/view/View;

.field private scrollState:I

.field private useTransformationOnLeftDrag:Z

.field private useTransformationOnRightDrag:Z


# direct methods
.method public constructor <init>(Landroid/support/v4/view/NSViewPager;Landroid/view/View;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 2
    .param p1, "pager"    # Landroid/support/v4/view/NSViewPager;
    .param p2, "pagerDropShadow"    # Landroid/view/View;
    .param p3, "listener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .prologue
    const/4 v1, 0x3

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pageTranslation:[I

    .line 20
    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->isFrontPage:[Z

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->scrollState:I

    .line 28
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pager:Landroid/support/v4/view/NSViewPager;

    .line 29
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pagerDropShadow:Landroid/view/View;

    .line 30
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->listener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 31
    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 7
    .param p1, "state"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v2, 0x0

    .line 111
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->listener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v3, :cond_0

    .line 112
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->listener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v3, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 114
    :cond_0
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->scrollState:I

    .line 115
    packed-switch p1, :pswitch_data_0

    .line 146
    :cond_1
    :goto_0
    return-void

    .line 117
    :pswitch_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pagerDropShadow:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 118
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/NSViewPager;->getCurrentItem()I

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->currentItemAtDragStart:I

    .line 119
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->useTransformationOnLeftDrag:Z

    .line 120
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->useTransformationOnRightDrag:Z

    .line 122
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/NSViewPager;->isRtl()Z

    move-result v2

    if-nez v2, :cond_1

    .line 123
    iput-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->useTransformationOnLeftDrag:Z

    .line 124
    iput-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->useTransformationOnRightDrag:Z

    goto :goto_0

    .line 128
    :pswitch_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/NSViewPager;->getPageViews()[Landroid/view/View;

    move-result-object v1

    .line 130
    .local v1, "pageViews":[Landroid/view/View;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/NSViewPager;->isRtl()Z

    move-result v3

    if-nez v3, :cond_1

    .line 131
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pagerDropShadow:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eq v3, v4, :cond_2

    .line 132
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pagerDropShadow:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 138
    :cond_2
    array-length v3, v1

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 139
    .local v0, "pageView":Landroid/view/View;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v4

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_3

    .line 140
    invoke-virtual {v0, v5}, Landroid/view/View;->setTranslationX(F)V

    .line 138
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onPageScrolled(IFI)V
    .locals 12
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 35
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->listener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v9, :cond_0

    .line 36
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->listener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v9, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 39
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->pauseBackgroundProcessingTemporarily()V

    .line 40
    iget v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->currentItemAtDragStart:I

    if-ne p1, v9, :cond_2

    const/4 v0, 0x1

    .line 41
    .local v0, "draggingLeft":Z
    :goto_0
    if-eqz v0, :cond_3

    iget-boolean v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->useTransformationOnLeftDrag:Z

    if-nez v9, :cond_3

    .line 100
    :cond_1
    return-void

    .line 40
    .end local v0    # "draggingLeft":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 44
    .restart local v0    # "draggingLeft":Z
    :cond_3
    if-nez v0, :cond_4

    iget-boolean v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->useTransformationOnRightDrag:Z

    if-eqz v9, :cond_1

    .line 48
    :cond_4
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pagerDropShadow:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v9

    if-eqz v9, :cond_5

    .line 49
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pagerDropShadow:Landroid/view/View;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 51
    :cond_5
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v9}, Landroid/support/v4/view/NSViewPager;->getWidth()I

    move-result v9

    sub-int/2addr v9, p3

    add-int/lit8 v6, v9, -0x1

    .line 52
    .local v6, "shadowTranslation":I
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pagerDropShadow:Landroid/view/View;

    int-to-float v10, v6

    invoke-virtual {v9, v10}, Landroid/view/View;->setTranslationX(F)V

    .line 53
    int-to-float v9, v6

    const/high16 v10, 0x40a00000    # 5.0f

    mul-float/2addr v9, v10

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v10}, Landroid/support/v4/view/NSViewPager;->getWidth()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-static {v9, v10, v11}, Lcom/google/apps/dots/android/newsstand/util/MathUtil;->clamp(FFF)F

    move-result v1

    .line 54
    .local v1, "dropShadowAlpha":F
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pagerDropShadow:Landroid/view/View;

    invoke-virtual {v9, v1}, Landroid/view/View;->setAlpha(F)V

    .line 56
    if-eqz v0, :cond_1

    .line 61
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/4 v9, 0x3

    if-ge v2, v9, :cond_6

    .line 62
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pageTranslation:[I

    const/4 v10, 0x0

    aput v10, v9, v2

    .line 63
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->isFrontPage:[Z

    const/4 v10, 0x0

    aput-boolean v10, v9, v2

    .line 61
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 65
    :cond_6
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v9}, Landroid/support/v4/view/NSViewPager;->getPageViews()[Landroid/view/View;

    move-result-object v4

    .line 66
    .local v4, "pageViews":[Landroid/view/View;
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v9}, Landroid/support/v4/view/NSViewPager;->getWidth()I

    move-result v5

    .line 67
    .local v5, "pageWidth":I
    iget v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->currentItemAtDragStart:I

    if-nez v9, :cond_9

    const/4 v7, 0x1

    .line 68
    .local v7, "translatePageIndex":I
    :goto_2
    sub-int v9, v5, p3

    neg-int v9, v9

    div-int/lit8 v8, v9, 0x2

    .line 70
    .local v8, "translation":I
    if-eqz v0, :cond_7

    .line 71
    iget v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->scrollState:I

    packed-switch v9, :pswitch_data_0

    .line 91
    :cond_7
    :goto_3
    const/4 v2, 0x0

    :goto_4
    const/4 v9, 0x3

    if-ge v2, v9, :cond_1

    .line 92
    aget-object v3, v4, v2

    .line 93
    .local v3, "pageView":Landroid/view/View;
    if-eqz v3, :cond_8

    .line 94
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pageTranslation:[I

    aget v9, v9, v2

    int-to-float v9, v9

    invoke-virtual {v3, v9}, Landroid/view/View;->setTranslationX(F)V

    .line 95
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->isFrontPage:[Z

    aget-boolean v9, v9, v2

    if-eqz v9, :cond_8

    .line 96
    invoke-virtual {v3}, Landroid/view/View;->bringToFront()V

    .line 91
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 67
    .end local v3    # "pageView":Landroid/view/View;
    .end local v7    # "translatePageIndex":I
    .end local v8    # "translation":I
    :cond_9
    const/4 v7, 0x2

    goto :goto_2

    .line 73
    .restart local v7    # "translatePageIndex":I
    .restart local v8    # "translation":I
    :pswitch_0
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->isFrontPage:[Z

    const/4 v10, 0x1

    const/4 v11, 0x1

    aput-boolean v11, v9, v10

    .line 74
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pageTranslation:[I

    const/4 v10, 0x2

    aput v8, v9, v10

    goto :goto_3

    .line 78
    :pswitch_1
    iget v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->currentItemAtDragStart:I

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v10}, Landroid/support/v4/view/NSViewPager;->getCurrentItem()I

    move-result v10

    if-eq v9, v10, :cond_b

    const/4 v9, 0x1

    if-le v7, v9, :cond_b

    .line 79
    add-int/lit8 v7, v7, -0x1

    .line 83
    :cond_a
    :goto_5
    if-lez p3, :cond_7

    .line 84
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pageTranslation:[I

    aput v8, v9, v7

    .line 85
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->isFrontPage:[Z

    add-int/lit8 v10, v7, -0x1

    const/4 v11, 0x1

    aput-boolean v11, v9, v10

    goto :goto_3

    .line 80
    :cond_b
    iget v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->currentItemAtDragStart:I

    if-nez v9, :cond_a

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v9}, Landroid/support/v4/view/NSViewPager;->getCurrentItem()I

    move-result v9

    if-nez v9, :cond_a

    .line 81
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->listener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;->listener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 107
    :cond_0
    return-void
.end method
