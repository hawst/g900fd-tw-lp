.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
.source "AccountAuthFailedEvent.java"


# instance fields
.field private final optThrowable:Ljava/lang/Throwable;

.field private final originatingRequestCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "originatingRequestCode"    # I

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;-><init>(ILjava/lang/Throwable;)V

    .line 21
    return-void
.end method

.method public constructor <init>(ILjava/lang/Throwable;)V
    .locals 0
    .param p1, "originatingRequestCode"    # I
    .param p2, "optThrowable"    # Ljava/lang/Throwable;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;-><init>()V

    .line 29
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;->originatingRequestCode:I

    .line 30
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;->optThrowable:Ljava/lang/Throwable;

    .line 31
    return-void
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 4
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 47
    const-string v0, "Account Authentication Failed"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;->optThrowable:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    .line 52
    const-string v0, "ErrorDescription"

    new-instance v1, Lcom/google/android/gms/analytics/StandardExceptionParser;

    .line 55
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/analytics/StandardExceptionParser;-><init>(Landroid/content/Context;Ljava/util/Collection;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;->optThrowable:Ljava/lang/Throwable;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/analytics/StandardExceptionParser;->getDescription(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    .line 52
    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_0
    const-string v0, "ErrorCode"

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;->originatingRequestCode:I

    .line 60
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-object p1
.end method

.method protected fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "publisherEvent"    # Z

    .prologue
    .line 68
    const-string v0, "Internal"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setCategory(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    return-object v0
.end method

.method protected getPublisherTrackingId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 36
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 41
    const-string v0, "Debug Metadata"

    return-object v0
.end method
