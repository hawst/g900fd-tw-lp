.class public final enum Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;
.super Ljava/lang/Enum;
.source "WarmWelcomeHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DismissibleCardType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

.field public static final enum BOOKMARKS:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

.field public static final enum MY_MAGAZINES_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

.field public static final enum MY_MAGAZINES_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

.field public static final enum MY_NEWS_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

.field public static final enum MY_NEWS_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

.field public static final enum MY_TOPICS_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

.field public static final enum MY_TOPICS_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

.field public static final enum READ_NOW:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 324
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    const-string v1, "READ_NOW"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->READ_NOW:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 325
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    const-string v1, "MY_NEWS_ONLINE"

    invoke-direct {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_NEWS_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 326
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    const-string v1, "MY_NEWS_OFFLINE"

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_NEWS_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 327
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    const-string v1, "MY_MAGAZINES_ONLINE"

    invoke-direct {v0, v1, v6}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_MAGAZINES_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 328
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    const-string v1, "MY_MAGAZINES_OFFLINE"

    invoke-direct {v0, v1, v7}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_MAGAZINES_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 329
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    const-string v1, "MY_TOPICS_ONLINE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_TOPICS_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 330
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    const-string v1, "MY_TOPICS_OFFLINE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_TOPICS_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 331
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    const-string v1, "BOOKMARKS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->BOOKMARKS:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 323
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->READ_NOW:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_NEWS_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_NEWS_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_MAGAZINES_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_MAGAZINES_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_TOPICS_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_TOPICS_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->BOOKMARKS:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 323
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 323
    const-class v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;
    .locals 1

    .prologue
    .line 323
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    return-object v0
.end method


# virtual methods
.method public prefKey()Ljava/lang/String;
    .locals 3

    .prologue
    .line 337
    const-string v1, "warmWelcome_"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
