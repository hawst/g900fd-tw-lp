.class public Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;
.super Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;
.source "NSRelativeLayout.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;


# instance fields
.field private isRead:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method


# virtual methods
.method public isRead()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;->isRead:Z

    return v0
.end method

.method protected makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    return-object v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;->isRead()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 52
    .local v0, "drawableState":[I
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->STATE_READ:[I

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;->mergeDrawableStates([I[I)[I

    .line 55
    .end local v0    # "drawableState":[I
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method public setIsRead(Z)V
    .locals 1
    .param p1, "isRead"    # Z

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;->isRead:Z

    if-eq v0, p1, :cond_0

    .line 42
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;->isRead:Z

    .line 43
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;->refreshDrawableState()V

    .line 44
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->dispatchSetIsRead(Landroid/view/ViewGroup;Z)V

    .line 46
    :cond_0
    return-void
.end method
