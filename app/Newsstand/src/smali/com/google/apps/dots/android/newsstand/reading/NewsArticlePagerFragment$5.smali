.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;
.super Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;
.source "NewsArticlePagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->setupPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Landroid/support/v4/view/NSViewPager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;
    .param p2, "viewPager"    # Landroid/support/v4/view/NSViewPager;

    .prologue
    .line 311
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;-><init>(Landroid/support/v4/view/NSViewPager;)V

    return-void
.end method


# virtual methods
.method public onPageSelected(IZ)V
    .locals 13
    .param p1, "visualPosition"    # I
    .param p2, "userDriven"    # Z

    .prologue
    const/16 v12, 0x10

    .line 314
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$200(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 315
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$300()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v9

    const-string v10, "postReadingList is empty but pageChangeListener fired"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v9, v10, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 318
    :cond_0
    if-eqz p2, :cond_2

    .line 319
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$100(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    move-result-object v9

    invoke-static {v9, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v0

    .line 320
    .local v0, "logicalPosition":I
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$200(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 322
    .local v1, "newArticleData":Lcom/google/android/libraries/bind/data/Data;
    sget-object v10, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$11;->$SwitchMap$com$google$apps$dots$android$newsstand$reading$ReadingFragment$ViewType:[I

    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_VIEW_TYPE:I

    invoke-virtual {v1, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;->ordinal()I

    move-result v9

    aget v9, v10, v9

    packed-switch v9, :pswitch_data_0

    .line 346
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "ViewType of article not set."

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 324
    :pswitch_0
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ID:I

    invoke-virtual {v1, v9}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v4

    .line 325
    .local v4, "newPostId":Ljava/lang/String;
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ORIGINAL_EDITION:I

    .line 326
    invoke-virtual {v1, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 327
    .local v3, "newOriginalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .line 328
    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v9, v9, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-direct {v2, v4, v9, v3, v10}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Integer;)V

    .line 329
    .local v2, "newArticleState":Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v9, v12, :cond_1

    .line 330
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$400(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v10

    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    .line 331
    invoke-virtual {v1, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v9

    .line 330
    invoke-virtual {v10, v9}, Landroid/support/v4/view/NSViewPager;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 349
    .end local v3    # "newOriginalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v4    # "newPostId":Ljava/lang/String;
    :cond_1
    :goto_0
    new-instance v5, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .line 350
    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v9, v9, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v5, v9, v2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;)V

    .line 351
    .local v5, "newState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    const/4 v10, 0x1

    invoke-virtual {v9, v5, v10}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 353
    .end local v0    # "logicalPosition":I
    .end local v1    # "newArticleData":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "newArticleState":Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;
    .end local v5    # "newState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    :cond_2
    return-void

    .line 335
    .restart local v0    # "logicalPosition":I
    .restart local v1    # "newArticleData":Lcom/google/android/libraries/bind/data/Data;
    :pswitch_1
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_POST_URL:I

    invoke-virtual {v1, v9}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v7

    .line 336
    .local v7, "postUrl":Ljava/lang/String;
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_POST_TITLE:I

    .line 337
    invoke-virtual {v1, v9}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v6

    .line 338
    .local v6, "postTitle":Ljava/lang/String;
    sget v9, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_NAME:I

    invoke-virtual {v1, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 339
    .local v8, "publisher":Ljava/lang/String;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .line 340
    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v9, v9, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v2, v7, v6, v9, v8}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    .line 341
    .restart local v2    # "newArticleState":Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v9, v12, :cond_1

    .line 342
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$400(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/support/v4/view/NSViewPager;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 322
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
