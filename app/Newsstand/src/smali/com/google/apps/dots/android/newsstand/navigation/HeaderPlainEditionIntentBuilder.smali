.class public Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "HeaderPlainEditionIntentBuilder.java"


# instance fields
.field private final intent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 20
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/HeaderPlainEditionActivity;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;->intent:Landroid/content/Intent;

    .line 21
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "HeaderPlainEditionFragment_state"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 22
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;
    .locals 2
    .param p1, "sharedElement"    # Landroid/view/View;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;->activity:Landroid/app/Activity;

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->expando_hero:I

    .line 26
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 25
    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 27
    return-object p0
.end method
