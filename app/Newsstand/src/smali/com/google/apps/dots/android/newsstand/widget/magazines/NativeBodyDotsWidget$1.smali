.class Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1;
.super Ljava/lang/Object;
.source "NativeBodyDotsWidget.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadIfPossible()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/util/List",
        "<*>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

.field final synthetic val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 153
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    .line 154
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1$2;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 160
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 135
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<*>;"
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 139
    .local v2, "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .line 140
    .local v5, "form":Lcom/google/apps/dots/proto/client/DotsShared$Form;
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 142
    .local v3, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->indexPostByField(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/util/Map;

    move-result-object v4

    .line 143
    .local v4, "postIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;>;"
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1;Lcom/google/apps/dots/proto/client/DotsShared$Section;Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/util/Map;Lcom/google/apps/dots/proto/client/DotsShared$Form;)V

    invoke-virtual {v6, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 149
    return-void
.end method
