.class public Lcom/google/apps/dots/android/newsstand/widget/NSLinearLayout;
.super Lcom/google/android/libraries/bind/widget/BoundLinearLayout;
.source "NSLinearLayout.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;


# instance fields
.field private isRead:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;-><init>(Landroid/content/Context;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method


# virtual methods
.method public isRead()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSLinearLayout;->isRead:Z

    return v0
.end method

.method protected makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    return-object v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSLinearLayout;->isRead()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 54
    .local v0, "drawableState":[I
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->STATE_READ:[I

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSLinearLayout;->mergeDrawableStates([I[I)[I

    .line 57
    .end local v0    # "drawableState":[I
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 0
    .param p1, "layoutDirection"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->onRtlPropertiesChanged(I)V

    .line 66
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSLinearLayout;->requestLayout()V

    .line 67
    return-void
.end method

.method public setIsRead(Z)V
    .locals 1
    .param p1, "isRead"    # Z

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSLinearLayout;->isRead:Z

    if-eq v0, p1, :cond_0

    .line 44
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSLinearLayout;->isRead:Z

    .line 45
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSLinearLayout;->refreshDrawableState()V

    .line 46
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->dispatchSetIsRead(Landroid/view/ViewGroup;Z)V

    .line 48
    :cond_0
    return-void
.end method
