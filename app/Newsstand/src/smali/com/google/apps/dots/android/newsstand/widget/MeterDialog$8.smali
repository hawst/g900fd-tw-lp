.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "MeterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->fillInDialog(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 519
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
    .locals 6
    .param p1, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 522
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->source_name:I

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 524
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$dimen;->dialog_source_icon_size:I

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getPixelSize(ILandroid/content/Context;)I

    move-result v0

    .line 525
    .local v0, "iconSize":I
    new-instance v3, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v3}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->square(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v2

    .line 526
    .local v2, "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->source_icon:I

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 527
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v4

    .line 526
    invoke-virtual {v3, v4, v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentIdPx(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    .line 529
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasPricingLearnMoreUrl()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 530
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->learn_more:I

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 531
    .local v1, "learnMore":Landroid/widget/TextView;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 532
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasPricingLearnMoreUrl()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 533
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getPricingLearnMoreLabel()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 534
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getPricingLearnMoreLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 536
    :cond_0
    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8$1;

    invoke-direct {v3, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 545
    .end local v1    # "learnMore":Landroid/widget/TextView;
    :cond_1
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 519
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    return-void
.end method
