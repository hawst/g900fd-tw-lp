.class public final Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Form;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Field"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;


# instance fields
.field private accuracy_:I

.field private allowMultilineText_:Z

.field private allowMutlipleValues_:Z

.field private bitField0_:I

.field public choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

.field private fieldId_:Ljava/lang/String;

.field private limitEntriesToChoices_:Z

.field private name_:Ljava/lang/String;

.field private required_:Z

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9709
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9710
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 9721
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->type_:I

    .line 9740
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->fieldId_:Ljava/lang/String;

    .line 9762
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->name_:Ljava/lang/String;

    .line 9784
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->required_:Z

    .line 9803
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMultilineText_:Z

    .line 9822
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->limitEntriesToChoices_:Z

    .line 9841
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 9844
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMutlipleValues_:Z

    .line 9863
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->accuracy_:I

    .line 9710
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    .locals 5

    .prologue
    .line 9900
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 9904
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 9905
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 9906
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 9907
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 9908
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v4

    aput-object v4, v3, v2

    .line 9906
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 9901
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 9902
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 9912
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 9706
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 9917
    if-ne p1, p0, :cond_1

    .line 9926
    :cond_0
    :goto_0
    return v1

    .line 9918
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 9919
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    .line 9920
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->fieldId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->fieldId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 9921
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->name_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->name_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 9922
    :goto_2
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->required_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->required_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMultilineText_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMultilineText_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->limitEntriesToChoices_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->limitEntriesToChoices_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 9926
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMutlipleValues_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMutlipleValues_:Z

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->accuracy_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->accuracy_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 9920
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->fieldId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->fieldId_:Ljava/lang/String;

    .line 9921
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->name_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->name_:Ljava/lang/String;

    .line 9922
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9742
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->fieldId_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9764
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 9988
    const/4 v1, 0x0

    .line 9989
    .local v1, "size":I
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 9990
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->type_:I

    .line 9991
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 9993
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 9994
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->fieldId_:Ljava/lang/String;

    .line 9995
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 9997
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 9998
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->name_:Ljava/lang/String;

    .line 9999
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10001
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 10002
    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->required_:Z

    .line 10003
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 10005
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_4

    .line 10006
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMultilineText_:Z

    .line 10007
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 10009
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_5

    .line 10010
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->limitEntriesToChoices_:Z

    .line 10011
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 10013
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-eqz v2, :cond_7

    .line 10014
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_7

    aget-object v0, v3, v2

    .line 10015
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v0, :cond_6

    .line 10016
    const/4 v5, 0x7

    .line 10017
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 10014
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 10021
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_7
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_8

    .line 10022
    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMutlipleValues_:Z

    .line 10023
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 10025
    :cond_8
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_9

    .line 10026
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->accuracy_:I

    .line 10027
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 10029
    :cond_9
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->cachedSize:I

    .line 10030
    return v1
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 9932
    const/16 v1, 0x11

    .line 9933
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 9934
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->type_:I

    add-int v1, v2, v6

    .line 9935
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->fieldId_:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 9936
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->name_:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    add-int v1, v6, v2

    .line 9937
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->required_:Z

    if-eqz v2, :cond_3

    move v2, v4

    :goto_2
    add-int v1, v6, v2

    .line 9938
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMultilineText_:Z

    if-eqz v2, :cond_4

    move v2, v4

    :goto_3
    add-int v1, v6, v2

    .line 9939
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->limitEntriesToChoices_:Z

    if-eqz v2, :cond_5

    move v2, v4

    :goto_4
    add-int v1, v6, v2

    .line 9940
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-nez v2, :cond_6

    mul-int/lit8 v1, v1, 0x1f

    .line 9946
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMutlipleValues_:Z

    if-eqz v3, :cond_8

    :goto_5
    add-int v1, v2, v4

    .line 9947
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->accuracy_:I

    add-int v1, v2, v3

    .line 9948
    return v1

    .line 9935
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->fieldId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 9936
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->name_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_3
    move v2, v5

    .line 9937
    goto :goto_2

    :cond_4
    move v2, v5

    .line 9938
    goto :goto_3

    :cond_5
    move v2, v5

    .line 9939
    goto :goto_4

    .line 9942
    :cond_6
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 9943
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v3

    :goto_7
    add-int v1, v6, v2

    .line 9942
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 9943
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hashCode()I

    move-result v2

    goto :goto_7

    .end local v0    # "i":I
    :cond_8
    move v4, v5

    .line 9946
    goto :goto_5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    .locals 11
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 10038
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 10039
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 10043
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 10044
    :sswitch_0
    return-object p0

    .line 10049
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 10050
    .local v4, "temp":I
    if-eq v4, v8, :cond_1

    if-eq v4, v9, :cond_1

    if-eq v4, v10, :cond_1

    const/4 v6, 0x4

    if-eq v4, v6, :cond_1

    const/4 v6, 0x5

    if-eq v4, v6, :cond_1

    const/4 v6, 0x6

    if-eq v4, v6, :cond_1

    const/4 v6, 0x7

    if-eq v4, v6, :cond_1

    const/16 v6, 0x8

    if-eq v4, v6, :cond_1

    const/16 v6, 0x9

    if-eq v4, v6, :cond_1

    const/16 v6, 0xa

    if-eq v4, v6, :cond_1

    const/16 v6, 0xb

    if-eq v4, v6, :cond_1

    const/16 v6, 0xc

    if-eq v4, v6, :cond_1

    const/16 v6, 0xd

    if-eq v4, v6, :cond_1

    const/16 v6, 0xe

    if-eq v4, v6, :cond_1

    const/16 v6, 0xf

    if-ne v4, v6, :cond_2

    .line 10065
    :cond_1
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->type_:I

    .line 10066
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    goto :goto_0

    .line 10068
    :cond_2
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->type_:I

    goto :goto_0

    .line 10073
    .end local v4    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->fieldId_:Ljava/lang/String;

    .line 10074
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    goto :goto_0

    .line 10078
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->name_:Ljava/lang/String;

    .line 10079
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    goto :goto_0

    .line 10083
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->required_:Z

    .line 10084
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    goto :goto_0

    .line 10088
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMultilineText_:Z

    .line 10089
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    goto/16 :goto_0

    .line 10093
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->limitEntriesToChoices_:Z

    .line 10094
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    goto/16 :goto_0

    .line 10098
    :sswitch_7
    const/16 v6, 0x3a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 10099
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-nez v6, :cond_4

    move v1, v5

    .line 10100
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 10101
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-eqz v6, :cond_3

    .line 10102
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 10104
    :cond_3
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 10105
    :goto_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_5

    .line 10106
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;-><init>()V

    aput-object v7, v6, v1

    .line 10107
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 10108
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 10105
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 10099
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v1, v6

    goto :goto_1

    .line 10111
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;-><init>()V

    aput-object v7, v6, v1

    .line 10112
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 10116
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMutlipleValues_:Z

    .line 10117
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    goto/16 :goto_0

    .line 10121
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 10122
    .restart local v4    # "temp":I
    if-eq v4, v8, :cond_6

    if-eq v4, v9, :cond_6

    if-ne v4, v10, :cond_7

    .line 10125
    :cond_6
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->accuracy_:I

    .line 10126
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    goto/16 :goto_0

    .line 10128
    :cond_7
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->accuracy_:I

    goto/16 :goto_0

    .line 10039
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9706
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9953
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9954
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->type_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 9956
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 9957
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->fieldId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 9959
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 9960
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->name_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 9962
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 9963
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->required_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 9965
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 9966
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMultilineText_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 9968
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 9969
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->limitEntriesToChoices_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 9971
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-eqz v1, :cond_7

    .line 9972
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->choices:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_7

    aget-object v0, v2, v1

    .line 9973
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v0, :cond_6

    .line 9974
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 9972
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 9978
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_7
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_8

    .line 9979
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->allowMutlipleValues_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 9981
    :cond_8
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_9

    .line 9982
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->accuracy_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 9984
    :cond_9
    return-void
.end method
