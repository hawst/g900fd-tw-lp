.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
.source "AnalyticsSettingsDataEvent.java"


# instance fields
.field private numMagazinesPinned:I

.field private numNewsEditionsPinned:I

.field private final pinnedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "pinnedList":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->pinnedList:Ljava/util/List;

    .line 31
    return-void
.end method

.method private buildPinnedCounters()V
    .locals 3

    .prologue
    .line 94
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->pinnedList:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 95
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->pinnedList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 96
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->pinnedList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 97
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    if-eqz v2, :cond_1

    .line 98
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->numMagazinesPinned:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->numMagazinesPinned:I

    .line 95
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 99
    :cond_1
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    if-nez v2, :cond_2

    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    if-nez v2, :cond_2

    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    if-eqz v2, :cond_0

    .line 102
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->numNewsEditionsPinned:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->numNewsEditionsPinned:I

    goto :goto_1

    .line 106
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v1    # "i":I
    :cond_3
    return-void
.end method

.method public static shouldSendEvent()Z
    .locals 6

    .prologue
    .line 89
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLastAnalyticsSettingsEventSentTime()J

    move-result-wide v0

    .line 90
    .local v0, "lastTimeSent":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/32 v4, 0x5265c00

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 4
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 36
    const-string v1, "Settings Loaded"

    invoke-virtual {p1, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 38
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->buildPinnedCounters()V

    .line 41
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    .line 43
    .local v0, "isReadNowPinned":Z
    const-string v1, "SettingsKeepReadNow"

    .line 44
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    .line 43
    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string v1, "SettingsWiFiOnlyDownload"

    .line 47
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDownloadViaWifiOnlyPreference()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    .line 46
    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v1, "SettingsWidgetInstalled"

    .line 50
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->isWidgetInstalled(Landroid/content/Context;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    .line 49
    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v1, "MiniMode"

    .line 53
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->isCompactModeEnabled()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    .line 52
    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v1, "SettingsEditionsPinned"

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->numNewsEditionsPinned:I

    .line 57
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 55
    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v1, "SettingsMagazinesPinned"

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->numMagazinesPinned:I

    .line 61
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 59
    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-object p1
.end method

.method protected fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "publisherEvent"    # Z

    .prologue
    .line 69
    const-string v0, "Internal"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setCategory(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 70
    return-object p1
.end method

.method protected getPublisherTrackingId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getSystemAnalyticsId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 81
    const-string v0, "Settings"

    return-object v0
.end method
