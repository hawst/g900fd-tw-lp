.class public final Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;
.super Ljava/lang/Object;
.source "SharedPreferencesPrefStore.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;


# static fields
.field private static final NONE:Ljava/lang/Object;


# instance fields
.field private appContext:Landroid/content/Context;

.field private cleared:Z

.field private final prefCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private sharedPrefs:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->NONE:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->appContext:Landroid/content/Context;

    .line 35
    new-instance v0, Lcom/google/common/collect/MapMaker;

    invoke-direct {v0}, Lcom/google/common/collect/MapMaker;-><init>()V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/common/collect/MapMaker;->concurrencyLevel(I)Lcom/google/common/collect/MapMaker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/MapMaker;->makeMap()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->prefCache:Ljava/util/Map;

    .line 36
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->CACHE_WARMUP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$1;-><init>(Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->submit(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->sharedPrefs:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->appContext:Landroid/content/Context;

    return-object v0
.end method

.method public static deleteSharedPrefs(Landroid/content/SharedPreferences;)V
    .locals 1
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 90
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 91
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 92
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 93
    return-void
.end method

.method private getSharedPrefs()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->sharedPrefs:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private nullToNone(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 45
    if-nez p1, :cond_0

    sget-object p1, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->NONE:Ljava/lang/Object;

    .end local p1    # "value":Ljava/lang/String;
    :cond_0
    return-object p1
.end method


# virtual methods
.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 50
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->prefCache:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 51
    .local v0, "maybeNone":Ljava/lang/Object;
    if-nez v0, :cond_2

    .line 52
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->cleared:Z

    if-eqz v2, :cond_1

    .line 62
    .end local v0    # "maybeNone":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v1

    .line 55
    .restart local v0    # "maybeNone":Ljava/lang/Object;
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->getSharedPrefs()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 56
    .local v1, "value":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->prefCache:Ljava/util/Map;

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->nullToNone(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 59
    .end local v1    # "value":Ljava/lang/String;
    :cond_2
    sget-object v2, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->NONE:Ljava/lang/Object;

    if-eq v0, v2, :cond_0

    .line 60
    check-cast v0, Ljava/lang/String;

    .end local v0    # "maybeNone":Ljava/lang/Object;
    move-object v1, v0

    goto :goto_0
.end method

.method public putString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->prefCache:Ljava/util/Map;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->nullToNone(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->getSharedPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 73
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz p2, :cond_0

    .line 74
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 79
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 80
    return-void

    .line 76
    :cond_0
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;)Lcom/google/apps/dots/android/newsstand/util/Disposable;
    .locals 3
    .param p1, "preferenceListener"    # Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->getSharedPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 98
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$2;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$2;-><init>(Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;)V

    .line 109
    .local v0, "listener":Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
    invoke-interface {v1, v0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 110
    new-instance v2, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$3;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$3;-><init>(Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;Landroid/content/SharedPreferences;Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-object v2
.end method
