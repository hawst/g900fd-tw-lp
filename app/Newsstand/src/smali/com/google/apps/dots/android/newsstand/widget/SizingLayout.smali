.class public Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;
.super Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;
.source "SizingLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;
    }
.end annotation


# instance fields
.field private bindHeightMultiplier:Ljava/lang/Integer;

.field private bindWidthMultiplier:Ljava/lang/Integer;

.field private heightMultiplier:F

.field private heightSource:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

.field private initialHeightMultiplier:F

.field private initialWidthMultiplier:F

.field private maxHeightMultiplier:F

.field private maxWidthMultiplier:F

.field private widthMultiplier:F

.field private widthSource:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    const/high16 v4, 0x3f800000    # 1.0f

    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    sget-object v3, Lcom/google/android/apps/newsstanddev/R$styleable;->SizingLayout:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 44
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->SizingLayout_widthSource:I

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 45
    .local v2, "widthSourceStr":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->parseDimension(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->setWidthSource(Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;)V

    .line 47
    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->SizingLayout_heightSource:I

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "heightSourceStr":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->parseDimension(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->setHeightSource(Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;)V

    .line 50
    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->SizingLayout_widthMultiplier:I

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->initialWidthMultiplier:F

    .line 51
    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->SizingLayout_heightMultiplier:I

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->initialHeightMultiplier:F

    .line 52
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->initialWidthMultiplier:F

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->widthMultiplier:F

    .line 53
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->initialHeightMultiplier:F

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->heightMultiplier:F

    .line 54
    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->SizingLayout_bindWidthMultiplier:I

    invoke-static {v0, v3}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->bindWidthMultiplier:Ljava/lang/Integer;

    .line 55
    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->SizingLayout_bindHeightMultiplier:I

    invoke-static {v0, v3}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->bindHeightMultiplier:Ljava/lang/Integer;

    .line 56
    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->SizingLayout_maxWidthMultiplier:I

    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->maxWidthMultiplier:F

    .line 57
    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->SizingLayout_maxHeightMultiplier:I

    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->maxHeightMultiplier:F

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    return-void
.end method

.method private static parseDimension(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 126
    if-nez p0, :cond_0

    .line 127
    const/4 v0, 0x0

    .line 129
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;->valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    move-result-object v0

    goto :goto_0
.end method

.method private setHeightMultiplierInternal(F)V
    .locals 2
    .param p1, "heightMultiplier"    # F

    .prologue
    .line 118
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->maxHeightMultiplier:F

    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 119
    .local v0, "clampedMultiplier":F
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->heightMultiplier:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 120
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->heightMultiplier:F

    .line 121
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->requestLayout()V

    .line 123
    :cond_0
    return-void
.end method

.method private setWidthMultiplierInternal(F)V
    .locals 2
    .param p1, "widthMultiplier"    # F

    .prologue
    .line 110
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->maxWidthMultiplier:F

    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 111
    .local v0, "clampedMultiplier":F
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->widthMultiplier:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 112
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->widthMultiplier:F

    .line 113
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->requestLayout()V

    .line 115
    :cond_0
    return-void
.end method


# virtual methods
.method protected computeMeasureSpec(ZII)I
    .locals 9
    .param p1, "isWidth"    # Z
    .param p2, "widthMeasureSpec"    # I
    .param p3, "heightMeasureSpec"    # I

    .prologue
    .line 140
    if-eqz p1, :cond_2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->widthSource:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    .line 141
    .local v4, "source":Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;
    :goto_0
    if-eqz p1, :cond_3

    move v3, p2

    .line 142
    .local v3, "result":I
    :goto_1
    if-eqz v4, :cond_1

    .line 143
    sget-object v8, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;->WIDTH:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    if-ne v4, v8, :cond_4

    move v7, p2

    .line 144
    .local v7, "sourceSpec":I
    :goto_2
    invoke-static {v7}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 145
    .local v5, "sourceMode":I
    const/high16 v8, 0x40000000    # 2.0f

    if-eq v5, v8, :cond_0

    const/high16 v8, -0x80000000

    if-ne v5, v8, :cond_1

    .line 147
    :cond_0
    invoke-static {v7}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 148
    .local v6, "sourceSize":I
    if-eqz p1, :cond_5

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->widthMultiplier:F

    .line 149
    .local v1, "multiplier":F
    :goto_3
    int-to-float v8, v6

    mul-float/2addr v8, v1

    float-to-int v2, v8

    .line 152
    .local v2, "outSize":I
    if-eqz p1, :cond_6

    .line 153
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getMinimumWidth(Landroid/view/View;)I

    move-result v0

    .line 154
    .local v0, "minSizeForDimension":I
    :goto_4
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 156
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 159
    .end local v0    # "minSizeForDimension":I
    .end local v1    # "multiplier":F
    .end local v2    # "outSize":I
    .end local v5    # "sourceMode":I
    .end local v6    # "sourceSize":I
    .end local v7    # "sourceSpec":I
    :cond_1
    return v3

    .line 140
    .end local v3    # "result":I
    .end local v4    # "source":Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;
    :cond_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->heightSource:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    goto :goto_0

    .restart local v4    # "source":Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;
    :cond_3
    move v3, p3

    .line 141
    goto :goto_1

    .restart local v3    # "result":I
    :cond_4
    move v7, p3

    .line 143
    goto :goto_2

    .line 148
    .restart local v5    # "sourceMode":I
    .restart local v6    # "sourceSize":I
    .restart local v7    # "sourceSpec":I
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->heightMultiplier:F

    goto :goto_3

    .line 153
    .restart local v1    # "multiplier":F
    .restart local v2    # "outSize":I
    :cond_6
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getMinimumHeight(Landroid/view/View;)I

    move-result v0

    goto :goto_4
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 134
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->computeMeasureSpec(ZII)I

    move-result p1

    .line 135
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->computeMeasureSpec(ZII)I

    move-result p2

    .line 136
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->onMeasure(II)V

    .line 137
    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 0
    .param p1, "layoutDirection"    # I

    .prologue
    .line 164
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->onRtlPropertiesChanged(I)V

    .line 167
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->requestLayout()V

    .line 168
    return-void
.end method

.method public setHeightMultiplier(F)V
    .locals 0
    .param p1, "heightMultiplier"    # F

    .prologue
    .line 105
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->initialHeightMultiplier:F

    .line 106
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->setHeightMultiplierInternal(F)V

    .line 107
    return-void
.end method

.method public setHeightSource(Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;)V
    .locals 0
    .param p1, "source"    # Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->heightSource:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    .line 63
    return-void
.end method

.method public setWidthMultiplier(F)V
    .locals 0
    .param p1, "widthMultiplier"    # F

    .prologue
    .line 97
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->initialWidthMultiplier:F

    .line 98
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->setWidthMultiplierInternal(F)V

    .line 99
    return-void
.end method

.method public setWidthSource(Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;)V
    .locals 0
    .param p1, "source"    # Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->widthSource:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    .line 67
    return-void
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 72
    if-eqz p1, :cond_2

    .line 74
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->bindWidthMultiplier:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->bindWidthMultiplier:Ljava/lang/Integer;

    .line 75
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsFloat(I)Ljava/lang/Float;

    move-result-object v0

    .local v0, "multiplier":Ljava/lang/Float;
    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->setWidthMultiplierInternal(F)V

    .line 80
    .end local v0    # "multiplier":Ljava/lang/Float;
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->bindHeightMultiplier:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->bindHeightMultiplier:Ljava/lang/Integer;

    .line 81
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsFloat(I)Ljava/lang/Float;

    move-result-object v0

    .restart local v0    # "multiplier":Ljava/lang/Float;
    if-eqz v0, :cond_1

    .line 82
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->setHeightMultiplierInternal(F)V

    .line 91
    .end local v0    # "multiplier":Ljava/lang/Float;
    :goto_1
    return-void

    .line 78
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->initialWidthMultiplier:F

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->setWidthMultiplierInternal(F)V

    goto :goto_0

    .line 84
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->initialHeightMultiplier:F

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->setHeightMultiplierInternal(F)V

    goto :goto_1

    .line 88
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->initialWidthMultiplier:F

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->setWidthMultiplierInternal(F)V

    .line 89
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->initialHeightMultiplier:F

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->setHeightMultiplierInternal(F)V

    goto :goto_1
.end method
