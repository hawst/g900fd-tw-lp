.class Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "SyncerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/SyncerService;->handleIntent(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService;

.field final synthetic val$failureCount:[I

.field final synthetic val$syncStartTime:J

.field final synthetic val$total:I

.field final synthetic val$userWriteToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService;Ljava/util/concurrent/Executor;[IIJLcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 373
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->val$failureCount:[I

    iput p4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->val$total:I

    iput-wide p5, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->val$syncStartTime:J

    iput-object p7, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->val$userWriteToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 376
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->val$failureCount:[I

    aget v1, v0, v3

    .line 377
    .local v1, "failures":I
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->val$total:I

    sub-int v2, v0, v1

    .line 378
    .local v2, "successes":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->val$syncStartTime:J

    sub-long v4, v6, v8

    .line 379
    .local v4, "lengthOfSyncEventInMillis":J
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->val$failureCount:[I

    aget v0, v0, v3

    if-lez v0, :cond_0

    .line 380
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncFailedEvent;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->val$userWriteToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncFailedEvent;-><init>(IILcom/google/apps/dots/android/newsstand/async/AsyncToken;J)V

    .line 381
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncFailedEvent;->track()V

    .line 386
    :goto_0
    # invokes: Lcom/google/apps/dots/android/newsstand/service/SyncerService;->resultBundle(II)Landroid/os/Bundle;
    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->access$700(II)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 383
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSuccessEvent;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->val$total:I

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->val$userWriteToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {v0, v3, v6, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSuccessEvent;-><init>(ILcom/google/apps/dots/android/newsstand/async/AsyncToken;J)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSuccessEvent;->track()V

    goto :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
