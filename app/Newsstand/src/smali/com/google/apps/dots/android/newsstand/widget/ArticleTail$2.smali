.class Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "ArticleTail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->updateBackground(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

.field final synthetic val$owningEdition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

.field final synthetic val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;->val$owningEdition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 6
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 102
    const/4 v1, 0x0

    .line 103
    .local v1, "hasDarkTemplate":Z
    if-eqz p1, :cond_2

    .line 104
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getFormTemplateId()Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "formId":Ljava/lang/String;
    const-string v3, "photo_default"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "photo_gallery"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "video_default"

    .line 106
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 107
    :cond_0
    const/4 v1, 0x1

    .line 109
    :cond_1
    move v2, v1

    .line 110
    .local v2, "useDarkTheme":Z
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;

    invoke-direct {v4, p0, p1, v2}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 123
    .end local v0    # "formId":Ljava/lang/String;
    .end local v2    # "useDarkTheme":Z
    :goto_0
    return-void

    .line 120
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->setVisibility(I)V

    .line 121
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "Unable to display article tail; post is null"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 99
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
