.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineLiteOnlyDialog;
.super Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;
.source "MagazineLiteOnlyDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineLiteOnlyDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineLiteOnlyDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;-><init>()V

    .line 22
    return-void
.end method

.method public static show(Landroid/support/v4/app/FragmentActivity;)V
    .locals 2
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 25
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineLiteOnlyDialog;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineLiteOnlyDialog;-><init>()V

    .line 26
    .local v0, "dialog":Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineLiteOnlyDialog;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineLiteOnlyDialog;->TAG:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->showSupportDialogCarefully(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V

    .line 27
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 41
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineLiteOnlyDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->magazine_lite_only_title:I

    .line 32
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->magazine_lite_only_body:I

    .line 33
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->ok:I

    .line 34
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
