.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StreamingVideo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;


# instance fields
.field private attachmentId_:Ljava/lang/String;

.field private bitField0_:I

.field private height_:I

.field private originalUri_:Ljava/lang/String;

.field private thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private width_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14162
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14163
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 14168
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->attachmentId_:Ljava/lang/String;

    .line 14190
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->originalUri_:Ljava/lang/String;

    .line 14212
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->width_:I

    .line 14231
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->height_:I

    .line 14163
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    .locals 3

    .prologue
    .line 14283
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 14287
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_0

    .line 14288
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 14290
    :cond_0
    return-object v0

    .line 14284
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    :catch_0
    move-exception v1

    .line 14285
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 14159
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 14295
    if-ne p1, p0, :cond_1

    .line 14302
    :cond_0
    :goto_0
    return v1

    .line 14296
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 14297
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    .line 14298
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->attachmentId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->attachmentId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->originalUri_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->originalUri_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 14299
    :goto_2
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->width_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->width_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->height_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->height_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 14302
    goto :goto_0

    .line 14298
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->attachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->attachmentId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->originalUri_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->originalUri_:Ljava/lang/String;

    .line 14299
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 14302
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getAttachmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14170
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->attachmentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 14233
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->height_:I

    return v0
.end method

.method public getOriginalUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14192
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->originalUri_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 14337
    const/4 v0, 0x0

    .line 14338
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14339
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->attachmentId_:Ljava/lang/String;

    .line 14340
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14342
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14343
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->originalUri_:Ljava/lang/String;

    .line 14344
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14346
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 14347
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->width_:I

    .line 14348
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14350
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 14351
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->height_:I

    .line 14352
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14354
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v1, :cond_4

    .line 14355
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 14356
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14358
    :cond_4
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->cachedSize:I

    .line 14359
    return v0
.end method

.method public getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 14252
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 14214
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->width_:I

    return v0
.end method

.method public hasAttachmentId()Z
    .locals 1

    .prologue
    .line 14181
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOriginalUri()Z
    .locals 1

    .prologue
    .line 14203
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasThumbnail()Z
    .locals 1

    .prologue
    .line 14262
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 14306
    const/16 v0, 0x11

    .line 14307
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 14308
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->attachmentId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 14309
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->originalUri_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 14310
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->width_:I

    add-int v0, v1, v3

    .line 14311
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->height_:I

    add-int v0, v1, v3

    .line 14312
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 14313
    return v0

    .line 14308
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->attachmentId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 14309
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->originalUri_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 14312
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14367
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 14368
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 14372
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 14373
    :sswitch_0
    return-object p0

    .line 14378
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->attachmentId_:Ljava/lang/String;

    .line 14379
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    goto :goto_0

    .line 14383
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->originalUri_:Ljava/lang/String;

    .line 14384
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    goto :goto_0

    .line 14388
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->width_:I

    .line 14389
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    goto :goto_0

    .line 14393
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->height_:I

    .line 14394
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    goto :goto_0

    .line 14398
    :sswitch_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v1, :cond_1

    .line 14399
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 14401
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 14368
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14159
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14318
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14319
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->attachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 14321
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14322
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->originalUri_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 14324
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 14325
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->width_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 14327
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 14328
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->height_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 14330
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_4

    .line 14331
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 14333
    :cond_4
    return-void
.end method
