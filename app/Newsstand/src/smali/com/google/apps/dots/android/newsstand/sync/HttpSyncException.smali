.class public Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;
.super Lcom/google/apps/dots/android/newsstand/sync/SyncException;
.source "HttpSyncException.java"


# instance fields
.field private final responseStatus:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;
    .param p2, "responseStatus"    # Ljava/lang/Integer;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/SyncException;-><init>(Ljava/lang/String;)V

    .line 39
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;->responseStatus:Ljava/lang/Integer;

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .param p2, "responseStatus"    # Ljava/lang/Integer;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/SyncException;-><init>(Ljava/lang/Throwable;)V

    .line 34
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;->responseStatus:Ljava/lang/Integer;

    .line 35
    return-void
.end method


# virtual methods
.method public getResponseStatus()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;->responseStatus:Ljava/lang/Integer;

    return-object v0
.end method

.method public getResponseStatusOrZero()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;->responseStatus:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;->responseStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method
