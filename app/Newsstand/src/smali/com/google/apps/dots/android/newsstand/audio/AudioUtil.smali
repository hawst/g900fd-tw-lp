.class public Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;
.super Ljava/lang/Object;
.source "AudioUtil.java"


# direct methods
.method public static cancelAudio(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    const-string v0, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.CANCEL"

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->sendAudioAction(Landroid/content/Context;Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public static getAudioStatus(Landroid/content/Context;Landroid/os/ResultReceiver;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resultReceiver"    # Landroid/os/ResultReceiver;

    .prologue
    .line 25
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 27
    .local v0, "statusIntent":Landroid/content/Intent;
    const-string v1, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.STATUS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 28
    const-string v1, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.RESULT_RECEIVER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 29
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 30
    return-void
.end method

.method public static nextAudio(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const-string v0, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.NEXT"

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->sendAudioAction(Landroid/content/Context;Ljava/lang/String;)V

    .line 71
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->showOfflineToast(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static pauseAudio(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const-string v0, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.PAUSE"

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->sendAudioAction(Landroid/content/Context;Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public static playAudio(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const-string v0, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.PLAY"

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->sendAudioAction(Landroid/content/Context;Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public static playFirstAudioItem(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/content/Context;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 2
    .param p0, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "postId"    # Ljava/lang/String;
    .param p3, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p4, "owningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 83
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->showOfflineToast(Landroid/content/Context;)V

    .line 102
    :goto_0
    return-void

    .line 88
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v0

    invoke-virtual {v0, p0, p2}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil$1;

    invoke-direct {v1, p1, p3, p4}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil$1;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 87
    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public static previousAudio(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const-string v0, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.PREVIOUS"

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->sendAudioAction(Landroid/content/Context;Ljava/lang/String;)V

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->showOfflineToast(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static seekAudio(Landroid/content/Context;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "progress"    # I

    .prologue
    .line 59
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.SEEK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const-string v1, "progress"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 62
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 63
    return-void
.end method

.method private static sendAudioAction(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 109
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 112
    return-void
.end method

.method private static showOfflineToast(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->wait_until_online:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 116
    return-void
.end method

.method public static startAudio(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/media/AudioItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "audioToStart"    # Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    .param p2, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "owningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 34
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 36
    .local v0, "startAudioServiceIntent":Landroid/content/Intent;
    const-string v1, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.SET"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    const-string v1, "audio_item"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 38
    const-string v1, "reading_edition"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 39
    const-string v1, "owning_edition"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 40
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 44
    .end local v0    # "startAudioServiceIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 42
    :cond_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->showOfflineToast(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static togglePlayPauseAudio(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const-string v0, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.TOGGLE_PLAY_PAUSE"

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->sendAudioAction(Landroid/content/Context;Ljava/lang/String;)V

    .line 56
    return-void
.end method
