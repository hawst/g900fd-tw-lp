.class public Lcom/google/apps/dots/android/newsstand/toast/Toasts;
.super Ljava/lang/Object;
.source "Toasts.java"


# direct methods
.method public static notifyUserOfAccountProblem()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/google/apps/dots/android/newsstand/toast/Toasts$2;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/toast/Toasts$2;-><init>()V

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->postOnMainThread(Ljava/lang/Runnable;)V

    .line 58
    return-void
.end method

.method public static notifyUserOfArchiveEvent(Z)V
    .locals 3
    .param p0, "pinRemoved"    # Z

    .prologue
    .line 81
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 82
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->archive_magazine_toast_text:I

    .line 83
    .local v0, "messageResId":I
    const/4 v1, 0x0

    .line 84
    .local v1, "toastLength":I
    if-eqz p0, :cond_0

    .line 85
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->archive_magazine_remove_pin_toast_text:I

    .line 86
    const/4 v1, 0x1

    .line 88
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 89
    return-void
.end method

.method public static notifyUserOfBadShareLink()V
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/google/apps/dots/android/newsstand/toast/Toasts$3;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/toast/Toasts$3;-><init>()V

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->postOnMainThread(Ljava/lang/Runnable;)V

    .line 99
    return-void
.end method

.method public static notifyUserOfContentNotAvailableOffline()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/google/apps/dots/android/newsstand/toast/Toasts$4;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/toast/Toasts$4;-><init>()V

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->postOnMainThread(Ljava/lang/Runnable;)V

    .line 109
    return-void
.end method

.method public static notifyUserOfDownloadLater()V
    .locals 4

    .prologue
    .line 61
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 63
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->download_when_connected_toast_text:I

    .line 65
    .local v0, "messageResId":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->cantSyncReason()Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "reason":Ljava/lang/String;
    const-string v2, "not connected"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDownloadViaWifiOnlyPreference()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const-string v2, "metered connection"

    .line 71
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 72
    :cond_1
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->download_when_on_wifi_toast_text:I

    .line 77
    :cond_2
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 78
    return-void

    .line 73
    :cond_3
    const-string v2, "not charging"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 74
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->download_when_charging_toast_text:I

    goto :goto_0
.end method

.method public static notifyUserOfRequiredUpgrade()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/apps/dots/android/newsstand/toast/Toasts$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/toast/Toasts$1;-><init>()V

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->postOnMainThread(Ljava/lang/Runnable;)V

    .line 30
    return-void
.end method
