.class public Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;
.super Lcom/google/android/libraries/bind/data/BoundHelper;
.source "NSBoundHelper.java"


# instance fields
.field public final bindReadKey:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/data/BoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    .line 27
    sget-object v1, Lcom/google/android/apps/newsstanddev/R$styleable;->SupportsReadState:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 29
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->SupportsReadState_bindRead:I

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;->bindReadKey:Ljava/lang/Integer;

    .line 31
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 32
    return-void
.end method

.method public static bindRead(Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p0, "view"    # Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;
    .param p1, "bindReadKey"    # Ljava/lang/Integer;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 43
    if-eqz p1, :cond_0

    .line 44
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;->setIsRead(Z)V

    .line 46
    :cond_0
    return-void

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 37
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;->view:Landroid/view/View;

    instance-of v0, v0, Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;->view:Landroid/view/View;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;->bindReadKey:Ljava/lang/Integer;

    invoke-static {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;->bindRead(Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V

    .line 40
    :cond_0
    return-void
.end method
