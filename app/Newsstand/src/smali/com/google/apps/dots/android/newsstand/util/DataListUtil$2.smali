.class final Lcom/google/apps/dots/android/newsstand/util/DataListUtil$2;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "DataListUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/util/DataListUtil;->whenDataListRefreshed(Lcom/google/android/libraries/bind/data/DataList;Z)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$dataList:Lcom/google/android/libraries/bind/data/DataList;

.field final synthetic val$observer:Lcom/google/android/libraries/bind/data/DataObserver;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/DataObserver;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/util/DataListUtil$2;->val$dataList:Lcom/google/android/libraries/bind/data/DataList;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/util/DataListUtil$2;->val$observer:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1, "optionalResult"    # Ljava/lang/Object;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/DataListUtil$2;->val$dataList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/util/DataListUtil$2;->val$observer:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 59
    return-void
.end method
