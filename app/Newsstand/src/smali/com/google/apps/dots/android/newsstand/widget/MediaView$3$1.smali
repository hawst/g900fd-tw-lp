.class Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;
.super Ljava/lang/Object;
.source "MediaView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;->call()Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;

.field final synthetic val$afd:Landroid/content/res/AssetFileDescriptor;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;Landroid/content/res/AssetFileDescriptor;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;->val$afd:Landroid/content/res/AssetFileDescriptor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 263
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$400(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$500(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$500(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;->val$afd:Landroid/content/res/AssetFileDescriptor;

    .line 265
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;->val$afd:Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;->val$afd:Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    .line 264
    invoke-virtual/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 266
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$500(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;->val$afd:Landroid/content/res/AssetFileDescriptor;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V

    .line 272
    return-void

    .line 268
    :catch_0
    move-exception v0

    goto :goto_0
.end method
