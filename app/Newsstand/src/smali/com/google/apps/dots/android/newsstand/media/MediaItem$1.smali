.class final Lcom/google/apps/dots/android/newsstand/media/MediaItem$1;
.super Ljava/lang/Object;
.source "MediaItem.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/media/MediaItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/media/MediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .locals 6
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "postId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 85
    .local v2, "offset":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 87
    .local v1, "fieldId":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-direct {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;-><init>()V

    .line 89
    .local v4, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->parseFrom([B)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v4

    .line 90
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasAudio()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 91
    new-instance v5, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    invoke-direct {v5, v3, v2, v1, v4}, Lcom/google/apps/dots/android/newsstand/media/AudioItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .line 95
    :goto_0
    return-object v5

    .line 92
    :cond_0
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 93
    :cond_1
    new-instance v5, Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    invoke-direct {v5, v3, v2, v1, v4}, Lcom/google/apps/dots/android/newsstand/media/VideoItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v5, Ljava/lang/IllegalStateException;

    invoke-direct {v5, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 95
    .end local v0    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :cond_2
    :try_start_1
    new-instance v5, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    invoke-direct {v5, v3, v2, v1, v4}, Lcom/google/apps/dots/android/newsstand/media/MediaItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V
    :try_end_1
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaItem$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 104
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaItem$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v0

    return-object v0
.end method
