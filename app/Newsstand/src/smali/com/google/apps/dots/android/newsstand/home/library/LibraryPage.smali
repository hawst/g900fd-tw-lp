.class public abstract Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
.super Ljava/lang/Object;
.source "LibraryPage.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

.field public static final MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

.field public static final MY_NEWS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

.field public static final MY_TOPICS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;


# instance fields
.field public final type:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/news/MyNewsPage;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/MyNewsPage;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_NEWS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 20
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/topics/MyTopicsPage;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/topics/MyTopicsPage;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_TOPICS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 21
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesPage;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesPage;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 22
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_NEWS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->DEFAULT_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 72
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;)V
    .locals 0
    .param p1, "type"    # Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->type:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;

    .line 35
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 45
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 47
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->type:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->type:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 49
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public abstract getFragment()Landroid/support/v4/app/Fragment;
.end method

.method public abstract getTitle(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->type:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 59
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "{type: %s}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->type:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->type:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    return-void
.end method
