.class public Lcom/google/apps/dots/android/newsstand/util/ObjectUtil;
.super Ljava/lang/Object;
.source "ObjectUtil.java"


# direct methods
.method public static coalesce(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "a":Ljava/lang/Object;, "TT;"
    .local p1, "b":Ljava/lang/Object;, "TT;"
    if-eqz p0, :cond_0

    .end local p0    # "a":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object p0

    .restart local p0    # "a":Ljava/lang/Object;, "TT;"
    :cond_0
    move-object p0, p1

    goto :goto_0
.end method

.method public static coalesce(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "a":Ljava/lang/Object;, "TT;"
    .local p1, "b":Ljava/lang/Object;, "TT;"
    .local p2, "c":Ljava/lang/Object;, "TT;"
    if-eqz p0, :cond_0

    .end local p0    # "a":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object p0

    .restart local p0    # "a":Ljava/lang/Object;, "TT;"
    :cond_0
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/util/ObjectUtil;->coalesce(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_0
.end method
