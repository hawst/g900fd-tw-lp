.class public Lcom/google/apps/dots/android/newsstand/analytics/InternalProtoTracker;
.super Ljava/lang/Object;
.source "InternalProtoTracker.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;


# instance fields
.field private final mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

.field private final serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/store/MutationStore;)V
    .locals 0
    .param p1, "serverUris"    # Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .param p2, "mutationStore"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/InternalProtoTracker;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 26
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/InternalProtoTracker;->mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .line 27
    return-void
.end method

.method private toClientAction(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .locals 4
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .prologue
    .line 48
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/InternalProtoTracker;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 49
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getRelativeAnalyticsEventUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    const/4 v1, 0x0

    .line 50
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    .line 51
    invoke-static {p1}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setBody([B)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    .line 52
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public supportsAccountlessEvents()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public trackEvent(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "trackingId"    # Ljava/lang/String;
    .param p3, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .prologue
    .line 36
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/InternalProtoTracker;->mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/InternalProtoTracker;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 38
    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getAnalyticsUrl(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p3}, Lcom/google/apps/dots/android/newsstand/analytics/InternalProtoTracker;->toClientAction(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    .line 37
    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 40
    :cond_0
    return-void
.end method
