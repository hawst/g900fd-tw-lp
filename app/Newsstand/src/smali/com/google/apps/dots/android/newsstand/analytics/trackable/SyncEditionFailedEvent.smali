.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncEditionFailedEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;
.source "SyncEditionFailedEvent.java"


# instance fields
.field private final appId:Ljava/lang/String;

.field private final failedException:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "failedException"    # Ljava/lang/Throwable;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 18
    invoke-direct {p0, p3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 19
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncEditionFailedEvent;->appId:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncEditionFailedEvent;->failedException:Ljava/lang/Throwable;

    .line 23
    return-void
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 5
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 28
    const-string v0, "SyncMetadata"

    const-string v1, "Failed exception message: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncEditionFailedEvent;->failedException:Ljava/lang/Throwable;

    .line 29
    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 28
    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    const-string v0, "Sync Edition Failed"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncEditionFailedEvent;->appId:Ljava/lang/String;

    .line 31
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 32
    return-object p1
.end method
