.class Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;
.super Ljava/lang/Object;
.source "AttachmentStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->decodeFn(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/AsyncFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$decodeOptions:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;->val$decodeOptions:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "storeResponse"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 95
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 96
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;->val$decodeOptions:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 100
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    .line 101
    # invokes: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->decodeWithMutableBitmap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v1, v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$000(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Landroid/graphics/BitmapFactory$Options;)V

    .line 100
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;->apply(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
