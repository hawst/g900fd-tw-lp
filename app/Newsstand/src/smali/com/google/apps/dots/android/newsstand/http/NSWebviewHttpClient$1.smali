.class Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$1;
.super Ljava/lang/Object;
.source "NSWebviewHttpClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->get(Ljava/net/URL;Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;

.field final synthetic val$followRedirects:Z

.field final synthetic val$url:Ljava/net/URL;

.field final synthetic val$userAgent:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;Ljava/net/URL;ZLjava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$1;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$1;->val$url:Ljava/net/URL;

    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$1;->val$followRedirects:Z

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$1;->val$userAgent:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 136
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$1;->val$url:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 137
    .local v0, "connection":Ljava/net/HttpURLConnection;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 138
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$1;->val$followRedirects:Z

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 139
    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 140
    const-string v1, "User-Agent"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$1;->val$userAgent:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    new-instance v1, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;-><init>(Ljava/net/HttpURLConnection;)V

    return-object v1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$1;->call()Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;

    move-result-object v0

    return-object v0
.end method
