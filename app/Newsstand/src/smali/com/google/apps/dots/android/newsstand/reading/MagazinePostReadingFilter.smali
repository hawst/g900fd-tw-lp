.class public Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;
.super Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.source "MagazinePostReadingFilter.java"


# instance fields
.field private final allPostsList:Lcom/google/android/libraries/bind/data/DataList;

.field private final inLiteMode:Z

.field private isRepub:Z


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;Z)V
    .locals 1
    .param p1, "allPostsList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "inLiteMode"    # Z

    .prologue
    .line 39
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;-><init>(Ljava/util/concurrent/Executor;)V

    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->allPostsList:Lcom/google/android/libraries/bind/data/DataList;

    .line 41
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->inLiteMode:Z

    .line 42
    return-void
.end method


# virtual methods
.method public isRepub()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->isRepub:Z

    return v0
.end method

.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 8
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    .line 65
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->isRepub:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->inLiteMode:Z

    if-nez v3, :cond_1

    .line 68
    sget v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 69
    .local v1, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v4, v1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v0, v4, v3

    .line 70
    .local v0, "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getType()I

    move-result v6

    if-ne v6, v7, :cond_0

    .line 71
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getFormat()I

    move-result v6

    if-ne v6, v7, :cond_0

    .line 76
    .end local v0    # "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    .end local v1    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :goto_1
    return v2

    .line 69
    .restart local v0    # "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    .restart local v1    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 76
    .end local v0    # "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    .end local v1    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public onPreFilter()V
    .locals 4

    .prologue
    .line 51
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->isRepub:Z

    .line 52
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->allPostsList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 53
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->allPostsList:Lcom/google/android/libraries/bind/data/DataList;

    .line 54
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    sget v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 55
    .local v1, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v2, v1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 56
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->allPostsList:Lcom/google/android/libraries/bind/data/DataList;

    .line 55
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->hasValidAltFormats(Ljava/util/List;Lcom/google/android/libraries/bind/data/DataList;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 57
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->isRepub:Z

    .line 61
    .end local v1    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_0
    return-void

    .line 52
    .restart local v1    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 19
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->isRepub:Z

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->inLiteMode:Z

    if-nez v13, :cond_2

    :cond_0
    move-object/from16 v7, p1

    .line 148
    :cond_1
    return-object v7

    .line 87
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    invoke-static {v13}, Lcom/google/common/collect/Maps;->newHashMapWithExpectedSize(I)Ljava/util/HashMap;

    move-result-object v8

    .line 88
    .local v8, "postIdToDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/libraries/bind/data/Data;

    .line 89
    .local v5, "inputData":Lcom/google/android/libraries/bind/data/Data;
    sget v14, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_ID:I

    invoke-virtual {v5, v14}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v8, v14, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 94
    .end local v5    # "inputData":Lcom/google/android/libraries/bind/data/Data;
    :cond_3
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 95
    .local v7, "outputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    .line 96
    .local v4, "includedTextPostIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_4
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/libraries/bind/data/Data;

    .line 103
    .restart local v5    # "inputData":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    .line 104
    .local v10, "previousOutputDataListSize":I
    sget v13, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {v5, v13}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 105
    .local v9, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    const/4 v6, 0x0

    .line 106
    .local v6, "leftHasCorrespondingText":Z
    const/4 v11, 0x0

    .line 107
    .local v11, "rightHasCorrespondingText":Z
    const/4 v3, 0x0

    .line 108
    .local v3, "hasCorrespondingReplica":Z
    iget-object v15, v9, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v0, v15

    move/from16 v16, v0

    const/4 v13, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v13, v0, :cond_9

    aget-object v2, v15, v13

    .line 109
    .local v2, "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getType()I

    move-result v17

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 110
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getFormat()I

    move-result v17

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 111
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getObjectId()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v8, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 112
    const/4 v3, 0x1

    .line 108
    :cond_5
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 113
    :cond_6
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getType()I

    move-result v17

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 114
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getFormat()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 115
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getObjectId()Ljava/lang/String;

    move-result-object v12

    .line 119
    .local v12, "textPostId":Ljava/lang/String;
    invoke-interface {v8, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 120
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getIndex()I

    move-result v17

    if-nez v17, :cond_8

    .line 121
    const/4 v6, 0x1

    .line 127
    :cond_7
    :goto_4
    invoke-interface {v4, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_5

    .line 128
    invoke-interface {v8, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    invoke-interface {v4, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 122
    :cond_8
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getIndex()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    .line 123
    const/4 v11, 0x1

    goto :goto_4

    .line 134
    .end local v2    # "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    .end local v12    # "textPostId":Ljava/lang/String;
    :cond_9
    if-nez v3, :cond_4

    .line 138
    if-nez v11, :cond_a

    .line 141
    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 142
    :cond_a
    if-nez v6, :cond_4

    .line 145
    invoke-interface {v7, v10, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_1
.end method
