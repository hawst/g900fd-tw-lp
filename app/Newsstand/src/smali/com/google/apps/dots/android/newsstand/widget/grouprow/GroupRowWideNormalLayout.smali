.class public Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;
.super Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;
.source "GroupRowWideNormalLayout.java"


# static fields
.field private static final NOT_FOUND:I = -0x1


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method private findWideCard(Ljava/util/List;Ljava/util/ArrayList;II)I
    .locals 4
    .param p3, "position"    # I
    .param p4, "numCardsToFill"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;II)I"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .local p2, "cardListIndices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p4, :cond_1

    .line 92
    add-int v2, p3, v1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 93
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    .line 94
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x2

    .line 95
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 93
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getResizedLayoutId(ILjava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 96
    add-int v2, p3, v1

    .line 99
    .end local v0    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    :goto_1
    return v2

    .line 91
    .restart local v0    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 99
    .end local v0    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method private updateShouldShrinkAndEnlarge()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 106
    const/4 v1, 0x0

    .line 107
    .local v1, "childrenCanEnlarge":Z
    const/4 v2, 0x1

    .line 109
    .local v2, "childrenCanShrink":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 110
    invoke-static {p0, v3}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getContainedCardAt(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 111
    .local v0, "childView":Landroid/view/View;
    if-eqz v0, :cond_3

    instance-of v4, v0, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;

    if-eqz v4, :cond_3

    .line 113
    if-nez v1, :cond_0

    move-object v4, v0

    check-cast v4, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;->canEnlarge()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v1, v5

    .line 114
    :goto_1
    if-eqz v2, :cond_2

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;

    .end local v0    # "childView":Landroid/view/View;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;->canShrink()Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v5

    .line 109
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .restart local v0    # "childView":Landroid/view/View;
    :cond_1
    move v1, v6

    .line 113
    goto :goto_1

    .end local v0    # "childView":Landroid/view/View;
    :cond_2
    move v2, v6

    .line 114
    goto :goto_2

    .line 120
    .restart local v0    # "childView":Landroid/view/View;
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 123
    .end local v0    # "childView":Landroid/view/View;
    :cond_4
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;->shouldShrink:Z

    .line 124
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;->shouldEnlarge:Z

    .line 125
    return-void
.end method


# virtual methods
.method public arrangeLayout(Ljava/util/List;Ljava/util/ArrayList;I)[I
    .locals 7
    .param p3, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)[I"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .local p2, "cardListIndices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;->getNumCards()I

    move-result v5

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    sub-int/2addr v6, p3

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 70
    .local v1, "numCardsToFill":I
    new-array v2, v1, [I

    .line 73
    .local v2, "returnArray":[I
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;->findWideCard(Ljava/util/List;Ljava/util/ArrayList;II)I

    move-result v4

    .line 74
    .local v4, "wideCardIndex":I
    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 75
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 76
    .local v3, "wideCard":Ljava/lang/Integer;
    invoke-virtual {p2, p3, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 78
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 79
    add-int v5, p3, v0

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v2, v0

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    .end local v0    # "i":I
    .end local v3    # "wideCard":Ljava/lang/Integer;
    :cond_0
    const/4 v2, 0x0

    .end local v2    # "returnArray":[I
    :cond_1
    return-object v2
.end method

.method protected getDesiredHeightMeasureSpec(I)I
    .locals 2
    .param p1, "desiredRowHeight"    # I

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;->updateShouldShrinkAndEnlarge()V

    .line 50
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;->shouldShrink()Z

    move-result v1

    if-eqz v1, :cond_0

    const/high16 v1, -0x80000000

    .line 51
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 55
    .local v0, "returnSpec":I
    :goto_0
    return v0

    .line 52
    .end local v0    # "returnSpec":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;->shouldEnlarge()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0

    :cond_1
    const/high16 v1, 0x40000000    # 2.0f

    .line 54
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method protected getNumCards()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x2

    return v0
.end method

.method public shouldEnlarge()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;->shouldEnlarge:Z

    return v0
.end method

.method public shouldShrink()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;->shouldShrink:Z

    return v0
.end method
