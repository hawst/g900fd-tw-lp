.class final Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$4;
.super Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;
.source "AnimationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/view/ViewPropertyAnimator;Landroid/animation/Animator$AnimatorListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$cancellable:Lcom/google/android/libraries/bind/async/Cancellable;


# direct methods
.method constructor <init>(Landroid/animation/Animator$AnimatorListener;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/android/libraries/bind/async/Cancellable;)V
    .locals 0
    .param p1, "wrappedListener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 285
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$4;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$4;->val$cancellable:Lcom/google/android/libraries/bind/async/Cancellable;

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;-><init>(Landroid/animation/Animator$AnimatorListener;)V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$4;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$4;->val$cancellable:Lcom/google/android/libraries/bind/async/Cancellable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->unregister(Lcom/google/android/libraries/bind/async/Cancellable;)V

    .line 289
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 290
    return-void
.end method
