.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;
.super Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;
.source "YearMonthIssuesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->updateAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

.field final synthetic val$yearMonthStringMapping:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/Map;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;
    .param p2, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;->val$yearMonthStringMapping:Ljava/util/Map;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    return-void
.end method


# virtual methods
.method protected getComparatorForSortingClusters()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;)V

    return-object v0
.end method

.method protected getStringToClusterOn(Lcom/google/android/libraries/bind/data/Data;)Ljava/lang/String;
    .locals 3
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 185
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_PUBLICATION_DATE:I

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsLong(I)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 186
    .local v0, "millis":J
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/util/DateFormatUtil;->getLocalizedYearAndMonth(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected makeShelfHeader(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "rowData"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "stringToClusterOn"    # Ljava/lang/String;
    .param p3, "isFirstHeader"    # Z

    .prologue
    .line 193
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->makeShelfHeader(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 194
    .local v0, "shelfHeader":Lcom/google/android/libraries/bind/data/Data;
    if-eqz p3, :cond_0

    .line 195
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE_TEXT_COLOR:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->play_header_list_banner_text_color:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 196
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE_TRANSITION_NAME:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->reloado_title:I

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 198
    :cond_0
    return-object v0
.end method
