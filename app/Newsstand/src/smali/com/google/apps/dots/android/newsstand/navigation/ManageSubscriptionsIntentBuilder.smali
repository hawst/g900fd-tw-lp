.class public Lcom/google/apps/dots/android/newsstand/navigation/ManageSubscriptionsIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;
.source "ManageSubscriptionsIntentBuilder.java"


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 16
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getManageSubscriptionsUri(Landroid/accounts/Account;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    .line 15
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/ManageSubscriptionsIntentBuilder;->setUri(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;

    .line 17
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/ManageSubscriptionsIntentBuilder;->intent:Landroid/content/Intent;

    return-object v0
.end method
