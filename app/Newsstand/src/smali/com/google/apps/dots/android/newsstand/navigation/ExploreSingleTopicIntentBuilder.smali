.class public Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;
.source "ExploreSingleTopicIntentBuilder.java"


# instance fields
.field private final expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

.field private final topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "topic"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .prologue
    .line 26
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->NONE:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "topic"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    .param p3, "expandedShelfType"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 32
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 33
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .line 34
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 48
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->useExpandableHeader()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->NONE:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    if-ne v3, v4, :cond_0

    .line 49
    const-class v3, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderActivity;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 50
    .local v0, "intent":Landroid/content/Intent;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    invoke-direct {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V

    .line 52
    .local v2, "state":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;
    const-string v3, "ExploreSingleTopicHeaderFragment_state"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object v1, v0

    .line 59
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "state":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;
    .local v1, "intent":Landroid/content/Intent;
    :goto_0
    return-object v1

    .line 55
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    const-class v3, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 56
    .restart local v0    # "intent":Landroid/content/Intent;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    invoke-direct {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V

    .line 58
    .local v2, "state":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;
    const-string v3, "ExploreSingleTopicFragment_state"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object v1, v0

    .line 59
    .end local v0    # "intent":Landroid/content/Intent;
    .restart local v1    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method

.method public setTransitionView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 37
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->expando_splash:I

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getStringResource(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 38
    return-object p0
.end method
