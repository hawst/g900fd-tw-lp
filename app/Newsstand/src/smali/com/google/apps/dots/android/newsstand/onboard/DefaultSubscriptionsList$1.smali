.class Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;
.super Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
.source "DefaultSubscriptionsList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;->processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$dataFutures:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;ILcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;
    .param p2, "primaryKey"    # I

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->val$dataFutures:Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;-><init>(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
    .locals 7
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    const/4 v6, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->currentCollectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->getCollectionId()Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "collectionId":Ljava/lang/String;
    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;->SUPPORTED_COLLECTION_IDS:Ljava/util/Set;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;->access$000()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 120
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v5, p2, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    aget-object v4, v5, v6

    .line 73
    .local v4, "itemId":Ljava/lang/String;
    const-string v5, "default-curation-collection"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 76
    .local v3, "defaultOn":Z
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->currentNode()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v5

    iget-object v5, v5, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v5, v5

    if-lez v5, :cond_1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->currentNode()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v5

    iget-object v5, v5, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v5, v5, v6

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppSummary()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 77
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->currentNode()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v5

    iget-object v5, v5, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v5, v5, v6

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 84
    .local v0, "appSummaryFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;>;"
    :goto_1
    new-instance v5, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;

    invoke-direct {v5, p0, v4, p2, v3}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Z)V

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 85
    invoke-static {v0, v5, v6}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 119
    .local v2, "dataFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->val$dataFutures:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    .end local v0    # "appSummaryFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;>;"
    .end local v2    # "dataFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/android/libraries/bind/data/Data;>;"
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appSummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v5, v6, v4}, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;->getAny(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .restart local v0    # "appSummaryFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;>;"
    goto :goto_1
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
    .locals 0

    .prologue
    .line 61
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    return-void
.end method
