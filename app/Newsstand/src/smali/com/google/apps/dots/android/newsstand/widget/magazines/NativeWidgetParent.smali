.class public interface abstract Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;
.super Ljava/lang/Object;
.source "NativeWidgetParent.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/EventSupport;


# virtual methods
.method public abstract applyLayoutToViewCoordsTransform(Landroid/graphics/Matrix;)V
.end method

.method public abstract onLoadComplete()V
.end method

.method public abstract onPageChanged()V
.end method

.method public abstract onScrolled()V
.end method

.method public abstract onUnhandledClick()V
.end method

.method public abstract onUnhandledFling(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)V
.end method

.method public abstract onZoomAttempt()V
.end method
