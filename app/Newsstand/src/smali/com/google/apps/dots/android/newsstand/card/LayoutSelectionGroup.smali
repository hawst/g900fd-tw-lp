.class public Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;
.super Lcom/google/android/libraries/bind/card/CardGroup;
.source "LayoutSelectionGroup.java"


# static fields
.field private static final CARD_RES_IDS:[I


# instance fields
.field private includeOversizedLayouts:Z

.field private maxRows:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->card_0:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->card_1:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->card_2:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->card_3:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->card_4:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->CARD_RES_IDS:[I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "groupList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/card/CardGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 31
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->maxRows:I

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->includeOversizedLayouts:Z

    .line 36
    return-void
.end method

.method static synthetic access$000()[I
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->CARD_RES_IDS:[I

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->getMappedPosition(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;Lcom/google/android/libraries/bind/view/ViewHeap;ILandroid/view/ViewGroup$LayoutParams;Ljava/lang/Integer;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;
    .param p1, "x1"    # Lcom/google/android/libraries/bind/view/ViewHeap;
    .param p2, "x2"    # I
    .param p3, "x3"    # Landroid/view/ViewGroup$LayoutParams;
    .param p4, "x4"    # Ljava/lang/Integer;

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->makeCardView(Lcom/google/android/libraries/bind/view/ViewHeap;ILandroid/view/ViewGroup$LayoutParams;Ljava/lang/Integer;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;I)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->getCardId(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private makeViewGenerator(I[I)Lcom/google/android/libraries/bind/card/ViewGenerator;
    .locals 1
    .param p1, "layoutResId"    # I
    .param p2, "dataIndices"    # [I

    .prologue
    .line 140
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;-><init>(Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;I[I)V

    return-object v0
.end method


# virtual methods
.method protected makeRows(Ljava/util/List;I)Ljava/util/List;
    .locals 20
    .param p2, "primaryKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v11

    .line 66
    .local v11, "rowDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 67
    .local v3, "cardListIndices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_0

    .line 68
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 72
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v13

    .line 75
    .local v13, "skipList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 76
    .local v8, "layoutResourceIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getBasicLayoutResourceIds()[Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v8, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 77
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->includeOversizedLayouts:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 78
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getOversizedLayoutResourceIds()[Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v8, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 81
    :cond_1
    const/4 v4, 0x0

    .line 82
    .local v4, "currentCard":I
    const/4 v12, 0x0

    .line 83
    .local v12, "rowIndex":I
    new-instance v9, Ljava/util/Random;

    invoke-direct {v9}, Ljava/util/Random;-><init>()V

    .line 84
    .local v9, "random":Ljava/util/Random;
    :goto_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v4, v0, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->maxRows:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v12, v0, :cond_6

    .line 87
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 88
    .local v15, "tempLayoutResourceIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/libraries/bind/data/Data;

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->hashCode()I

    move-result v2

    .line 92
    .local v2, "cardIdSeed":I
    int-to-long v0, v2

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/Random;->setSeed(J)V

    .line 93
    const/4 v5, 0x0

    .line 94
    .local v5, "dataIndices":[I
    const/4 v7, -0x1

    .line 98
    .local v7, "layoutPicker":I
    :cond_2
    :goto_2
    if-nez v5, :cond_5

    .line 100
    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 101
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    .line 102
    .local v14, "skipped":Ljava/lang/Integer;
    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-gt v0, v4, :cond_3

    .line 105
    new-instance v17, Ljava/lang/RuntimeException;

    new-instance v18, Ljava/lang/StringBuilder;

    const/16 v19, 0x3a

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v19, "Unable to find an appropriate layout for row = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 109
    :cond_3
    new-instance v15, Ljava/util/ArrayList;

    .end local v15    # "tempLayoutResourceIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {v15, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 112
    .end local v14    # "skipped":Ljava/lang/Integer;
    .restart local v15    # "tempLayoutResourceIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_4
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v7

    .line 114
    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    .line 113
    move/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1, v3, v4}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->arrangeLayout(ILjava/util/List;Ljava/util/ArrayList;I)[I

    move-result-object v5

    .line 117
    if-nez v5, :cond_2

    .line 118
    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 122
    :cond_5
    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->makeViewGenerator(I[I)Lcom/google/android/libraries/bind/card/ViewGenerator;

    move-result-object v16

    .line 123
    .local v16, "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v12, v1}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->makeRowData(ILcom/google/android/libraries/bind/card/ViewGenerator;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v10

    .line 126
    .local v10, "rowData":Lcom/google/android/libraries/bind/data/Data;
    array-length v0, v5

    move/from16 v17, v0

    add-int v4, v4, v17

    .line 129
    invoke-virtual {v3, v4, v13}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 130
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    .line 132
    new-instance v17, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v10}, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;-><init>(Lcom/google/android/libraries/bind/card/ViewGenerator;Lcom/google/android/libraries/bind/data/Data;)V

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    add-int/lit8 v12, v12, 0x1

    .line 134
    goto/16 :goto_1

    .line 136
    .end local v2    # "cardIdSeed":I
    .end local v5    # "dataIndices":[I
    .end local v7    # "layoutPicker":I
    .end local v10    # "rowData":Lcom/google/android/libraries/bind/data/Data;
    .end local v15    # "tempLayoutResourceIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v16    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :cond_6
    return-object v11
.end method

.method public setIncludeOversizedLayouts(Z)Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;
    .locals 0
    .param p1, "includeOversizedLayouts"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->includeOversizedLayouts:Z

    .line 43
    return-object p0
.end method

.method public setMaxRows(I)Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;
    .locals 0
    .param p1, "maxRows"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->maxRows:I

    .line 48
    return-object p0
.end method
