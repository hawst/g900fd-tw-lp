.class public final Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NameValuePair"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;


# instance fields
.field public name:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 754
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 755
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 758
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    .line 761
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    .line 755
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    .locals 3

    .prologue
    .line 774
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 778
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    return-object v0

    .line 775
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    :catch_0
    move-exception v1

    .line 776
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 751
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 783
    if-ne p1, p0, :cond_1

    .line 787
    :cond_0
    :goto_0
    return v1

    .line 784
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 785
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    .line 786
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 787
    goto :goto_0

    .line 786
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    .line 787
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 806
    const/4 v0, 0x0

    .line 807
    .local v0, "size":I
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    .line 808
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 809
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    .line 810
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 811
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->cachedSize:I

    .line 812
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 791
    const/16 v0, 0x11

    .line 792
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 793
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 794
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 795
    return v0

    .line 793
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 794
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 820
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 821
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 825
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 826
    :sswitch_0
    return-object p0

    .line 831
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    goto :goto_0

    .line 835
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    goto :goto_0

    .line 821
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 751
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 800
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 801
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 802
    return-void
.end method
