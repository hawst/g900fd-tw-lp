.class public final Lcom/google/apps/dots/proto/client/DotsShared$Section;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Section"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Section;


# instance fields
.field public appId:Ljava/lang/String;

.field private bitField0_:I

.field private correspondingImageSectionId_:Ljava/lang/String;

.field private correspondingTextSectionId_:Ljava/lang/String;

.field private created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

.field private dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

.field private displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

.field private externalId_:Ljava/lang/String;

.field public features:[I

.field public flag:[I

.field private formId_:Ljava/lang/String;

.field private hidden_:Z

.field private hideTimePublishedInSummary_:Z

.field private isMetered_:Z

.field private languageCode_:Ljava/lang/String;

.field private layoutEngineVersionOverride_:I

.field private name_:Ljava/lang/String;

.field public popularPostIds:[Ljava/lang/String;

.field public sectionId:Ljava/lang/String;

.field public sectionLevelAttachmentIds:[Ljava/lang/String;

.field private showFavicon_:Z

.field private showTopicTags_:Z

.field private storeType_:I

.field private syncImageTransform_:I

.field private syncStrategy_:I

.field private translationCode_:Ljava/lang/String;

.field private type_:I

.field private untranslatedAppId_:Ljava/lang/String;

.field private untranslatedFormId_:Ljava/lang/String;

.field private untranslatedSectionId_:Ljava/lang/String;

.field private updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

.field private visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20561
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Section;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Section;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 20562
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 20982
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->appId:Ljava/lang/String;

    .line 20985
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedAppId_:Ljava/lang/String;

    .line 21007
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    .line 21010
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedSectionId_:Ljava/lang/String;

    .line 21070
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->name_:Ljava/lang/String;

    .line 21092
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->formId_:Ljava/lang/String;

    .line 21114
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedFormId_:Ljava/lang/String;

    .line 21155
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hidden_:Z

    .line 21174
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hideTimePublishedInSummary_:Z

    .line 21193
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    .line 21196
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->type_:I

    .line 21234
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->externalId_:Ljava/lang/String;

    .line 21256
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showFavicon_:Z

    .line 21275
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->languageCode_:Ljava/lang/String;

    .line 21297
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->translationCode_:Ljava/lang/String;

    .line 21338
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingImageSectionId_:Ljava/lang/String;

    .line 21360
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingTextSectionId_:Ljava/lang/String;

    .line 21382
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncStrategy_:I

    .line 21401
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncImageTransform_:I

    .line 21420
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    .line 21423
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->isMetered_:Z

    .line 21442
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    .line 21445
    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showTopicTags_:Z

    .line 21464
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    .line 21467
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->storeType_:I

    .line 21486
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->layoutEngineVersionOverride_:I

    .line 20562
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Section;
    .locals 3

    .prologue
    .line 21545
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 21549
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_0

    .line 21550
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->clone()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 21552
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_1

    .line 21553
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->clone()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 21555
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    if-eqz v2, :cond_2

    .line 21556
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    .line 21558
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    array-length v2, v2

    if-lez v2, :cond_3

    .line 21559
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    .line 21561
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    if-eqz v2, :cond_4

    .line 21562
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    .line 21564
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_5

    .line 21565
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->clone()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 21567
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    array-length v2, v2

    if-lez v2, :cond_6

    .line 21568
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    .line 21570
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 21571
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    invoke-virtual {v2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    .line 21573
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 21574
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    invoke-virtual {v2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    .line 21576
    :cond_8
    return-object v0

    .line 21546
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    :catch_0
    move-exception v1

    .line 21547
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20558
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Section;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 21581
    if-ne p1, p0, :cond_1

    .line 21612
    :cond_0
    :goto_0
    return v1

    .line 21582
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 21583
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 21584
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->appId:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->appId:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 21585
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 21586
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 21587
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_3

    .line 21588
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_3

    .line 21589
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->name_:Ljava/lang/String;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->name_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 21590
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->formId_:Ljava/lang/String;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->formId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 21591
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedFormId_:Ljava/lang/String;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedFormId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 21592
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    if-nez v3, :cond_3

    .line 21593
    :goto_a
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hidden_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hidden_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hideTimePublishedInSummary_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hideTimePublishedInSummary_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    .line 21596
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    if-nez v3, :cond_3

    .line 21598
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->externalId_:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->externalId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 21599
    :goto_c
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showFavicon_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showFavicon_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 21601
    :goto_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 21602
    :goto_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_12

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_3

    .line 21603
    :goto_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingImageSectionId_:Ljava/lang/String;

    if-nez v3, :cond_13

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingImageSectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 21604
    :goto_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingTextSectionId_:Ljava/lang/String;

    if-nez v3, :cond_14

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingTextSectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 21605
    :goto_11
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncStrategy_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncStrategy_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncImageTransform_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncImageTransform_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    .line 21608
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->isMetered_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->isMetered_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    .line 21610
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showTopicTags_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showTopicTags_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    .line 21612
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->storeType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->storeType_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->layoutEngineVersionOverride_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->layoutEngineVersionOverride_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0

    .line 21584
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->appId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->appId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedAppId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedAppId_:Ljava/lang/String;

    .line 21585
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    .line 21586
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedSectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedSectionId_:Ljava/lang/String;

    .line 21587
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 21588
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 21589
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->name_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->name_:Ljava/lang/String;

    .line 21590
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->formId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->formId_:Ljava/lang/String;

    .line 21591
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedFormId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedFormId_:Ljava/lang/String;

    .line 21592
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    .line 21593
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    .line 21596
    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    .line 21598
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->externalId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->externalId_:Ljava/lang/String;

    .line 21599
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->languageCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->languageCode_:Ljava/lang/String;

    .line 21601
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->translationCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->translationCode_:Ljava/lang/String;

    .line 21602
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 21603
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_f

    :cond_13
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingImageSectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingImageSectionId_:Ljava/lang/String;

    .line 21604
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_10

    :cond_14
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingTextSectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingTextSectionId_:Ljava/lang/String;

    .line 21605
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_11
.end method

.method public getDisplayOptions()Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;
    .locals 1

    .prologue
    .line 21217
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    return-object v0
.end method

.method public getFormId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21094
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->formId_:Ljava/lang/String;

    return-object v0
.end method

.method public getLayoutEngineVersionOverride()I
    .locals 1

    .prologue
    .line 21488
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->layoutEngineVersionOverride_:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21072
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 21777
    const/4 v2, 0x0

    .line 21778
    .local v2, "size":I
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->appId:Ljava/lang/String;

    .line 21779
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21780
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    .line 21781
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21782
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v4, :cond_0

    .line 21783
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 21784
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21786
    :cond_0
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v4, :cond_1

    .line 21787
    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 21788
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21790
    :cond_1
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_2

    .line 21791
    const/4 v4, 0x6

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->name_:Ljava/lang/String;

    .line 21792
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21794
    :cond_2
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_3

    .line 21795
    const/16 v4, 0x8

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->formId_:Ljava/lang/String;

    .line 21796
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21798
    :cond_3
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    if-eqz v4, :cond_4

    .line 21799
    const/16 v4, 0x9

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    .line 21800
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21802
    :cond_4
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_5

    .line 21803
    const/16 v4, 0xb

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->type_:I

    .line 21804
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 21806
    :cond_5
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    if-eqz v4, :cond_6

    .line 21807
    const/16 v4, 0xc

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    .line 21808
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21810
    :cond_6
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v4, v4, 0x100

    if-eqz v4, :cond_7

    .line 21811
    const/16 v4, 0xf

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->externalId_:Ljava/lang/String;

    .line 21812
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21814
    :cond_7
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_8

    .line 21815
    const/16 v4, 0x19

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hidden_:Z

    .line 21816
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 21818
    :cond_8
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    array-length v4, v4

    if-lez v4, :cond_a

    .line 21819
    const/4 v0, 0x0

    .line 21820
    .local v0, "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_9

    aget v1, v5, v4

    .line 21822
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v7

    add-int/2addr v0, v7

    .line 21820
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 21824
    .end local v1    # "element":I
    :cond_9
    add-int/2addr v2, v0

    .line 21825
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    .line 21827
    .end local v0    # "dataSize":I
    :cond_a
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_b

    .line 21828
    const/16 v4, 0x1d

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showFavicon_:Z

    .line 21829
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 21831
    :cond_b
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v4, v4, 0x400

    if-eqz v4, :cond_c

    .line 21832
    const/16 v4, 0x1e

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->languageCode_:Ljava/lang/String;

    .line 21833
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21835
    :cond_c
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v4, v4, 0x800

    if-eqz v4, :cond_d

    .line 21836
    const/16 v4, 0x1f

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->translationCode_:Ljava/lang/String;

    .line 21837
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21839
    :cond_d
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v4, :cond_e

    .line 21840
    const/16 v4, 0x20

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 21841
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21843
    :cond_e
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v4, v4, 0x4000

    if-eqz v4, :cond_f

    .line 21844
    const/16 v4, 0x21

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncStrategy_:I

    .line 21845
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 21847
    :cond_f
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v4, v4, 0x1000

    if-eqz v4, :cond_10

    .line 21848
    const/16 v4, 0x22

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingImageSectionId_:Ljava/lang/String;

    .line 21849
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21851
    :cond_10
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v4, v4, 0x2000

    if-eqz v4, :cond_11

    .line 21852
    const/16 v4, 0x23

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingTextSectionId_:Ljava/lang/String;

    .line 21853
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 21855
    :cond_11
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    if-eqz v4, :cond_13

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    array-length v4, v4

    if-lez v4, :cond_13

    .line 21856
    const/4 v0, 0x0

    .line 21857
    .restart local v0    # "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_12

    aget v1, v5, v4

    .line 21859
    .restart local v1    # "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v7

    add-int/2addr v0, v7

    .line 21857
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 21861
    .end local v1    # "element":I
    :cond_12
    add-int/2addr v2, v0

    .line 21862
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    .line 21864
    .end local v0    # "dataSize":I
    :cond_13
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_14

    .line 21865
    const/16 v4, 0x25

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hideTimePublishedInSummary_:Z

    .line 21866
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 21868
    :cond_14
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const v5, 0x8000

    and-int/2addr v4, v5

    if-eqz v4, :cond_15

    .line 21869
    const/16 v4, 0x2a

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncImageTransform_:I

    .line 21870
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 21872
    :cond_15
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v5, 0x10000

    and-int/2addr v4, v5

    if-eqz v4, :cond_16

    .line 21873
    const/16 v4, 0x2e

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->isMetered_:Z

    .line 21874
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 21876
    :cond_16
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    if-eqz v4, :cond_18

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_18

    .line 21877
    const/4 v0, 0x0

    .line 21878
    .restart local v0    # "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_17

    aget-object v1, v5, v4

    .line 21880
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 21878
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 21882
    .end local v1    # "element":Ljava/lang/String;
    :cond_17
    add-int/2addr v2, v0

    .line 21883
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    .line 21885
    .end local v0    # "dataSize":I
    :cond_18
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v5, 0x20000

    and-int/2addr v4, v5

    if-eqz v4, :cond_19

    .line 21886
    const/16 v4, 0x30

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showTopicTags_:Z

    .line 21887
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 21889
    :cond_19
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    if-eqz v4, :cond_1b

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_1b

    .line 21890
    const/4 v0, 0x0

    .line 21891
    .restart local v0    # "dataSize":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    array-length v5, v4

    :goto_3
    if-ge v3, v5, :cond_1a

    aget-object v1, v4, v3

    .line 21893
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 21891
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 21895
    .end local v1    # "element":Ljava/lang/String;
    :cond_1a
    add-int/2addr v2, v0

    .line 21896
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 21898
    .end local v0    # "dataSize":I
    :cond_1b
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1c

    .line 21899
    const/16 v3, 0x32

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedAppId_:Ljava/lang/String;

    .line 21900
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 21902
    :cond_1c
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_1d

    .line 21903
    const/16 v3, 0x33

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedSectionId_:Ljava/lang/String;

    .line 21904
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 21906
    :cond_1d
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    if-eqz v3, :cond_1e

    .line 21907
    const/16 v3, 0x34

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedFormId_:Ljava/lang/String;

    .line 21908
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 21910
    :cond_1e
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v4, 0x40000

    and-int/2addr v3, v4

    if-eqz v3, :cond_1f

    .line 21911
    const/16 v3, 0x3a

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->storeType_:I

    .line 21912
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 21914
    :cond_1f
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v4, 0x80000

    and-int/2addr v3, v4

    if-eqz v3, :cond_20

    .line 21915
    const/16 v3, 0x3b

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->layoutEngineVersionOverride_:I

    .line 21916
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 21918
    :cond_20
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->cachedSize:I

    .line 21919
    return v2
.end method

.method public getSyncImageTransform()I
    .locals 1

    .prologue
    .line 21403
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncImageTransform_:I

    return v0
.end method

.method public hasCorrespondingImageSectionId()Z
    .locals 1

    .prologue
    .line 21351
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayOptions()Z
    .locals 1

    .prologue
    .line 21227
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFormId()Z
    .locals 1

    .prologue
    .line 21105
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLayoutEngineVersionOverride()Z
    .locals 2

    .prologue
    .line 21496
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21618
    const/16 v1, 0x11

    .line 21619
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 21620
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->appId:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 21621
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedAppId_:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_1
    add-int v1, v6, v2

    .line 21622
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_2
    add-int v1, v6, v2

    .line 21623
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    :goto_3
    add-int v1, v6, v2

    .line 21624
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v2, :cond_8

    move v2, v3

    :goto_4
    add-int v1, v6, v2

    .line 21625
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v2, :cond_9

    move v2, v3

    :goto_5
    add-int v1, v6, v2

    .line 21626
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->name_:Ljava/lang/String;

    if-nez v2, :cond_a

    move v2, v3

    :goto_6
    add-int v1, v6, v2

    .line 21627
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->formId_:Ljava/lang/String;

    if-nez v2, :cond_b

    move v2, v3

    :goto_7
    add-int v1, v6, v2

    .line 21628
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedFormId_:Ljava/lang/String;

    if-nez v2, :cond_c

    move v2, v3

    :goto_8
    add-int v1, v6, v2

    .line 21629
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    if-nez v2, :cond_d

    move v2, v3

    :goto_9
    add-int v1, v6, v2

    .line 21630
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hidden_:Z

    if-eqz v2, :cond_e

    move v2, v4

    :goto_a
    add-int v1, v6, v2

    .line 21631
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hideTimePublishedInSummary_:Z

    if-eqz v2, :cond_f

    move v2, v4

    :goto_b
    add-int v1, v6, v2

    .line 21632
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    if-nez v2, :cond_10

    mul-int/lit8 v1, v1, 0x1f

    .line 21638
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->type_:I

    add-int v1, v2, v6

    .line 21639
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    if-nez v2, :cond_11

    move v2, v3

    :goto_c
    add-int v1, v6, v2

    .line 21640
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->externalId_:Ljava/lang/String;

    if-nez v2, :cond_12

    move v2, v3

    :goto_d
    add-int v1, v6, v2

    .line 21641
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showFavicon_:Z

    if-eqz v2, :cond_13

    move v2, v4

    :goto_e
    add-int v1, v6, v2

    .line 21642
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->languageCode_:Ljava/lang/String;

    if-nez v2, :cond_14

    move v2, v3

    :goto_f
    add-int v1, v6, v2

    .line 21643
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->translationCode_:Ljava/lang/String;

    if-nez v2, :cond_15

    move v2, v3

    :goto_10
    add-int v1, v6, v2

    .line 21644
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v2, :cond_16

    move v2, v3

    :goto_11
    add-int v1, v6, v2

    .line 21645
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingImageSectionId_:Ljava/lang/String;

    if-nez v2, :cond_17

    move v2, v3

    :goto_12
    add-int v1, v6, v2

    .line 21646
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingTextSectionId_:Ljava/lang/String;

    if-nez v2, :cond_18

    move v2, v3

    :goto_13
    add-int v1, v6, v2

    .line 21647
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncStrategy_:I

    add-int v1, v2, v6

    .line 21648
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncImageTransform_:I

    add-int v1, v2, v6

    .line 21649
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    if-nez v2, :cond_19

    mul-int/lit8 v1, v1, 0x1f

    .line 21655
    :cond_1
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->isMetered_:Z

    if-eqz v2, :cond_1a

    move v2, v4

    :goto_14
    add-int v1, v6, v2

    .line 21656
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    if-nez v2, :cond_1b

    mul-int/lit8 v1, v1, 0x1f

    .line 21662
    :cond_2
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showTopicTags_:Z

    if-eqz v6, :cond_1d

    :goto_15
    add-int v1, v2, v4

    .line 21663
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    if-nez v2, :cond_1e

    mul-int/lit8 v1, v1, 0x1f

    .line 21669
    :cond_3
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->storeType_:I

    add-int v1, v2, v3

    .line 21670
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->layoutEngineVersionOverride_:I

    add-int v1, v2, v3

    .line 21671
    return v1

    .line 21620
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->appId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 21621
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 21622
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 21623
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedSectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 21624
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 21625
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 21626
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->name_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 21627
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->formId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 21628
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedFormId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 21629
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->hashCode()I

    move-result v2

    goto/16 :goto_9

    :cond_e
    move v2, v5

    .line 21630
    goto/16 :goto_a

    :cond_f
    move v2, v5

    .line 21631
    goto/16 :goto_b

    .line 21634
    :cond_10
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_16
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 21635
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    aget v6, v6, v0

    add-int v1, v2, v6

    .line 21634
    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    .line 21639
    .end local v0    # "i":I
    :cond_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->hashCode()I

    move-result v2

    goto/16 :goto_c

    .line 21640
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->externalId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_d

    :cond_13
    move v2, v5

    .line 21641
    goto/16 :goto_e

    .line 21642
    :cond_14
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->languageCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_f

    .line 21643
    :cond_15
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->translationCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_10

    .line 21644
    :cond_16
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->hashCode()I

    move-result v2

    goto/16 :goto_11

    .line 21645
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingImageSectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_12

    .line 21646
    :cond_18
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingTextSectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_13

    .line 21651
    :cond_19
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 21652
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    aget v6, v6, v0

    add-int v1, v2, v6

    .line 21651
    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    .end local v0    # "i":I
    :cond_1a
    move v2, v5

    .line 21655
    goto/16 :goto_14

    .line 21658
    :cond_1b
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_18
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 21659
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_1c

    move v2, v3

    :goto_19
    add-int v1, v6, v2

    .line 21658
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 21659
    :cond_1c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_19

    .end local v0    # "i":I
    :cond_1d
    move v4, v5

    .line 21662
    goto/16 :goto_15

    .line 21665
    :cond_1e
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 21666
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_1f

    move v2, v3

    :goto_1b
    add-int v1, v4, v2

    .line 21665
    add-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 21666
    :cond_1f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1b
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Section;
    .locals 10
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 21927
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 21928
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 21932
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 21933
    :sswitch_0
    return-object p0

    .line 21938
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->appId:Ljava/lang/String;

    goto :goto_0

    .line 21942
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    goto :goto_0

    .line 21946
    :sswitch_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v5, :cond_1

    .line 21947
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 21949
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 21953
    :sswitch_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v5, :cond_2

    .line 21954
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 21956
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 21960
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->name_:Ljava/lang/String;

    .line 21961
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto :goto_0

    .line 21965
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->formId_:Ljava/lang/String;

    .line 21966
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto :goto_0

    .line 21970
    :sswitch_7
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    if-nez v5, :cond_3

    .line 21971
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    .line 21973
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 21977
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 21978
    .local v4, "temp":I
    if-eqz v4, :cond_4

    if-eq v4, v8, :cond_4

    if-eq v4, v9, :cond_4

    const/4 v5, 0x5

    if-eq v4, v5, :cond_4

    const/4 v5, 0x6

    if-eq v4, v5, :cond_4

    const/4 v5, 0x7

    if-eq v4, v5, :cond_4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_4

    const/16 v5, 0x9

    if-eq v4, v5, :cond_4

    const/16 v5, 0xa

    if-eq v4, v5, :cond_4

    const/16 v5, 0xb

    if-eq v4, v5, :cond_4

    const/16 v5, 0xc

    if-ne v4, v5, :cond_5

    .line 21989
    :cond_4
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->type_:I

    .line 21990
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 21992
    :cond_5
    iput v7, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->type_:I

    goto/16 :goto_0

    .line 21997
    .end local v4    # "temp":I
    :sswitch_9
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    if-nez v5, :cond_6

    .line 21998
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    .line 22000
    :cond_6
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 22004
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->externalId_:Ljava/lang/String;

    .line 22005
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22009
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hidden_:Z

    .line 22010
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22014
    :sswitch_c
    const/16 v5, 0xd8

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 22015
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    array-length v1, v5

    .line 22016
    .local v1, "i":I
    add-int v5, v1, v0

    new-array v2, v5, [I

    .line 22017
    .local v2, "newArray":[I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    invoke-static {v5, v7, v2, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 22018
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    .line 22019
    :goto_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 22020
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    .line 22021
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 22019
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 22024
    :cond_7
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    goto/16 :goto_0

    .line 22028
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[I
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showFavicon_:Z

    .line 22029
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit16 v5, v5, 0x200

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22033
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->languageCode_:Ljava/lang/String;

    .line 22034
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit16 v5, v5, 0x400

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22038
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->translationCode_:Ljava/lang/String;

    .line 22039
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit16 v5, v5, 0x800

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22043
    :sswitch_10
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v5, :cond_8

    .line 22044
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 22046
    :cond_8
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 22050
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 22051
    .restart local v4    # "temp":I
    if-eqz v4, :cond_9

    if-ne v4, v8, :cond_a

    .line 22053
    :cond_9
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncStrategy_:I

    .line 22054
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit16 v5, v5, 0x4000

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22056
    :cond_a
    iput v7, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncStrategy_:I

    goto/16 :goto_0

    .line 22061
    .end local v4    # "temp":I
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingImageSectionId_:Ljava/lang/String;

    .line 22062
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit16 v5, v5, 0x1000

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22066
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingTextSectionId_:Ljava/lang/String;

    .line 22067
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit16 v5, v5, 0x2000

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22071
    :sswitch_14
    const/16 v5, 0x120

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 22072
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    array-length v1, v5

    .line 22073
    .restart local v1    # "i":I
    add-int v5, v1, v0

    new-array v2, v5, [I

    .line 22074
    .restart local v2    # "newArray":[I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    invoke-static {v5, v7, v2, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 22075
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    .line 22076
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_b

    .line 22077
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    .line 22078
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 22076
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 22081
    :cond_b
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    goto/16 :goto_0

    .line 22085
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[I
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hideTimePublishedInSummary_:Z

    .line 22086
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22090
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 22091
    .restart local v4    # "temp":I
    if-eqz v4, :cond_c

    if-eq v4, v8, :cond_c

    if-eq v4, v9, :cond_c

    const/4 v5, 0x3

    if-ne v4, v5, :cond_d

    .line 22095
    :cond_c
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncImageTransform_:I

    .line 22096
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const v6, 0x8000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22098
    :cond_d
    iput v7, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncImageTransform_:I

    goto/16 :goto_0

    .line 22103
    .end local v4    # "temp":I
    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->isMetered_:Z

    .line 22104
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v6, 0x10000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22108
    :sswitch_18
    const/16 v5, 0x17a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 22109
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    array-length v1, v5

    .line 22110
    .restart local v1    # "i":I
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 22111
    .local v2, "newArray":[Ljava/lang/String;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    invoke-static {v5, v7, v2, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 22112
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    .line 22113
    :goto_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_e

    .line 22114
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 22115
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 22113
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 22118
    :cond_e
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    goto/16 :goto_0

    .line 22122
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showTopicTags_:Z

    .line 22123
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v6, 0x20000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22127
    :sswitch_1a
    const/16 v5, 0x18a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 22128
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    array-length v1, v5

    .line 22129
    .restart local v1    # "i":I
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 22130
    .restart local v2    # "newArray":[Ljava/lang/String;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    invoke-static {v5, v7, v2, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 22131
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    .line 22132
    :goto_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_f

    .line 22133
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 22134
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 22132
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 22137
    :cond_f
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    goto/16 :goto_0

    .line 22141
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedAppId_:Ljava/lang/String;

    .line 22142
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22146
    :sswitch_1c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedSectionId_:Ljava/lang/String;

    .line 22147
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22151
    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedFormId_:Ljava/lang/String;

    .line 22152
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22156
    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 22157
    .restart local v4    # "temp":I
    if-eqz v4, :cond_10

    if-eq v4, v8, :cond_10

    if-ne v4, v9, :cond_11

    .line 22160
    :cond_10
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->storeType_:I

    .line 22161
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v6, 0x40000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 22163
    :cond_11
    iput v7, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->storeType_:I

    goto/16 :goto_0

    .line 22168
    .end local v4    # "temp":I
    :sswitch_1f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->layoutEngineVersionOverride_:I

    .line 22169
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v6, 0x80000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    goto/16 :goto_0

    .line 21928
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x58 -> :sswitch_8
        0x62 -> :sswitch_9
        0x7a -> :sswitch_a
        0xc8 -> :sswitch_b
        0xd8 -> :sswitch_c
        0xe8 -> :sswitch_d
        0xf2 -> :sswitch_e
        0xfa -> :sswitch_f
        0x102 -> :sswitch_10
        0x108 -> :sswitch_11
        0x112 -> :sswitch_12
        0x11a -> :sswitch_13
        0x120 -> :sswitch_14
        0x128 -> :sswitch_15
        0x150 -> :sswitch_16
        0x170 -> :sswitch_17
        0x17a -> :sswitch_18
        0x180 -> :sswitch_19
        0x18a -> :sswitch_1a
        0x192 -> :sswitch_1b
        0x19a -> :sswitch_1c
        0x1a2 -> :sswitch_1d
        0x1d0 -> :sswitch_1e
        0x1d8 -> :sswitch_1f
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20558
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Section;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 21676
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->appId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21677
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21678
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_0

    .line 21679
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 21681
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_1

    .line 21682
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 21684
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 21685
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->name_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21687
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 21688
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->formId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21690
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    if-eqz v2, :cond_4

    .line 21691
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->dataSource_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 21693
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_5

    .line 21694
    const/16 v2, 0xb

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->type_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 21696
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    if-eqz v2, :cond_6

    .line 21697
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->displayOptions_:Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 21699
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_7

    .line 21700
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->externalId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21702
    :cond_7
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_8

    .line 21703
    const/16 v2, 0x19

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hidden_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 21705
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    array-length v2, v2

    if-lez v2, :cond_9

    .line 21706
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->flag:[I

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_9

    aget v0, v3, v2

    .line 21707
    .local v0, "element":I
    const/16 v5, 0x1b

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 21706
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 21710
    .end local v0    # "element":I
    :cond_9
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_a

    .line 21711
    const/16 v2, 0x1d

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showFavicon_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 21713
    :cond_a
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_b

    .line 21714
    const/16 v2, 0x1e

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->languageCode_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21716
    :cond_b
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v2, v2, 0x800

    if-eqz v2, :cond_c

    .line 21717
    const/16 v2, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->translationCode_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21719
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_d

    .line 21720
    const/16 v2, 0x20

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->visibilityUpdate_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 21722
    :cond_d
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v2, v2, 0x4000

    if-eqz v2, :cond_e

    .line 21723
    const/16 v2, 0x21

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncStrategy_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 21725
    :cond_e
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v2, v2, 0x1000

    if-eqz v2, :cond_f

    .line 21726
    const/16 v2, 0x22

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingImageSectionId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21728
    :cond_f
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit16 v2, v2, 0x2000

    if-eqz v2, :cond_10

    .line 21729
    const/16 v2, 0x23

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->correspondingTextSectionId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21731
    :cond_10
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    array-length v2, v2

    if-lez v2, :cond_11

    .line 21732
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_11

    aget v0, v3, v2

    .line 21733
    .restart local v0    # "element":I
    const/16 v5, 0x24

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 21732
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 21736
    .end local v0    # "element":I
    :cond_11
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_12

    .line 21737
    const/16 v2, 0x25

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hideTimePublishedInSummary_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 21739
    :cond_12
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const v3, 0x8000

    and-int/2addr v2, v3

    if-eqz v2, :cond_13

    .line 21740
    const/16 v2, 0x2a

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->syncImageTransform_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 21742
    :cond_13
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    if-eqz v2, :cond_14

    .line 21743
    const/16 v2, 0x2e

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->isMetered_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 21745
    :cond_14
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    if-eqz v2, :cond_15

    .line 21746
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_15

    aget-object v0, v3, v2

    .line 21747
    .local v0, "element":Ljava/lang/String;
    const/16 v5, 0x2f

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21746
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 21750
    .end local v0    # "element":Ljava/lang/String;
    :cond_15
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    if-eqz v2, :cond_16

    .line 21751
    const/16 v2, 0x30

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->showTopicTags_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 21753
    :cond_16
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    if-eqz v2, :cond_17

    .line 21754
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->popularPostIds:[Ljava/lang/String;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_17

    aget-object v0, v2, v1

    .line 21755
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v4, 0x31

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21754
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 21758
    .end local v0    # "element":Ljava/lang/String;
    :cond_17
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_18

    .line 21759
    const/16 v1, 0x32

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21761
    :cond_18
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_19

    .line 21762
    const/16 v1, 0x33

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedSectionId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21764
    :cond_19
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_1a

    .line 21765
    const/16 v1, 0x34

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->untranslatedFormId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 21767
    :cond_1a
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v2, 0x40000

    and-int/2addr v1, v2

    if-eqz v1, :cond_1b

    .line 21768
    const/16 v1, 0x3a

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->storeType_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 21770
    :cond_1b
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->bitField0_:I

    const/high16 v2, 0x80000

    and-int/2addr v1, v2

    if-eqz v1, :cond_1c

    .line 21771
    const/16 v1, 0x3b

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->layoutEngineVersionOverride_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 21773
    :cond_1c
    return-void
.end method
