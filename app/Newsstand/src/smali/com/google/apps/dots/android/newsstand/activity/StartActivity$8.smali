.class Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "StartActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->startEditionHelper(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

.field final synthetic val$optPostId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    .prologue
    .line 524
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;->val$optPostId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 5
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 527
    move-object v0, p1

    .line 528
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-nez v0, :cond_0

    .line 529
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    .line 530
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/toast/Toasts;->notifyUserOfBadShareLink()V

    .line 533
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;->val$optPostId:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 534
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 535
    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v3

    .line 536
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->noTransition()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v3

    .line 537
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->clearBackstack()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v3

    .line 538
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->start()V

    .line 551
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->finish()V

    .line 552
    return-void

    .line 540
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;->val$optPostId:Ljava/lang/String;

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 541
    .local v2, "sectionId":Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->sectionEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    move-result-object v1

    .line 544
    .local v1, "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 545
    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;->val$optPostId:Ljava/lang/String;

    .line 546
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v3

    .line 547
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->noTransition()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v3

    .line 548
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->clearBackstack()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v3

    .line 549
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->start()V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 524
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    return-void
.end method
