.class Lcom/google/apps/dots/android/newsstand/widget/NSWebView$3;
.super Ljava/lang/Object;
.source "NSWebView.java"

# interfaces
.implements Landroid/webkit/DownloadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->initWebView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NSWebView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p1, "downloadUrl"    # Ljava/lang/String;
    .param p2, "userAgent"    # Ljava/lang/String;
    .param p3, "contentDisposition"    # Ljava/lang/String;
    .param p4, "mimetype"    # Ljava/lang/String;
    .param p5, "contentLength"    # J

    .prologue
    .line 124
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 125
    .local v0, "downloadUri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 126
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 127
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->getUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 130
    :cond_0
    return-void
.end method
