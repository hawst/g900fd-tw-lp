.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PdfDetails"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;


# instance fields
.field private backgroundColor_:Ljava/lang/String;

.field private bitField0_:I

.field private page_:I

.field private pdfFieldId_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3003
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3004
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3009
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->pdfFieldId_:Ljava/lang/String;

    .line 3031
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->page_:I

    .line 3051
    const-string v0, "#FFFFFF"

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->backgroundColor_:Ljava/lang/String;

    .line 3004
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;
    .locals 3

    .prologue
    .line 3085
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3089
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;
    return-object v0

    .line 3086
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;
    :catch_0
    move-exception v1

    .line 3087
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3000
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3094
    if-ne p1, p0, :cond_1

    .line 3099
    :cond_0
    :goto_0
    return v1

    .line 3095
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 3096
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    .line 3097
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->pdfFieldId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->pdfFieldId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->page_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->page_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->backgroundColor_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->backgroundColor_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 3099
    goto :goto_0

    .line 3097
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->pdfFieldId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->pdfFieldId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->backgroundColor_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->backgroundColor_:Ljava/lang/String;

    .line 3099
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getBackgroundColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3053
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->backgroundColor_:Ljava/lang/String;

    return-object v0
.end method

.method public getPage()I
    .locals 1

    .prologue
    .line 3033
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->page_:I

    return v0
.end method

.method public getPdfFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3011
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->pdfFieldId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3126
    const/4 v0, 0x0

    .line 3127
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3128
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->pdfFieldId_:Ljava/lang/String;

    .line 3129
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3131
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3132
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->page_:I

    .line 3133
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3135
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 3136
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->backgroundColor_:Ljava/lang/String;

    .line 3137
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3139
    :cond_2
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->cachedSize:I

    .line 3140
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 3103
    const/16 v0, 0x11

    .line 3104
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 3105
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->pdfFieldId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 3106
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->page_:I

    add-int v0, v1, v3

    .line 3107
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->backgroundColor_:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 3108
    return v0

    .line 3105
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->pdfFieldId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 3107
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->backgroundColor_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3148
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3149
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3153
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3154
    :sswitch_0
    return-object p0

    .line 3159
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->pdfFieldId_:Ljava/lang/String;

    .line 3160
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    goto :goto_0

    .line 3164
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->page_:I

    .line 3165
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    goto :goto_0

    .line 3169
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->backgroundColor_:Ljava/lang/String;

    .line 3170
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    goto :goto_0

    .line 3149
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3000
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3113
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3114
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->pdfFieldId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3116
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3117
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->page_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 3119
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 3120
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->backgroundColor_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3122
    :cond_2
    return-void
.end method
