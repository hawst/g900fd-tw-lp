.class public Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;
.super Ljava/lang/Object;
.source "SearchFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final query:Ljava/lang/String;

.field public final searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "searchType"    # Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->whiteSpaceToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    .line 33
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 37
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 38
    check-cast v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    .line 39
    .local v0, "otherState":Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    .line 40
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 42
    .end local v0    # "otherState":Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->ordinal()I

    move-result v0

    goto :goto_0
.end method
