.class public Lcom/google/apps/dots/android/newsstand/store/AsyncLock;
.super Ljava/lang/Object;
.source "AsyncLock.java"


# instance fields
.field private final lock:Ljava/lang/Object;

.field private passing:Z

.field private final pending:Lcom/google/android/libraries/bind/collections/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/bind/collections/RingBuffer",
            "<",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/AsyncLock;",
            ">;>;"
        }
    .end annotation
.end field

.field private permits:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;-><init>(I)V

    .line 34
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "permits"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->lock:Ljava/lang/Object;

    .line 28
    invoke-static {}, Lcom/google/android/libraries/bind/collections/RingBuffer;->create()Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->pending:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 37
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->permits:I

    .line 38
    return-void
.end method

.method private tryPassPermit()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 77
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->passing:Z

    if-nez v2, :cond_4

    .line 78
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->passing:Z

    .line 81
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->permits:I

    if-le v2, v0, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->pending:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/collections/RingBuffer;->size()I

    move-result v2

    if-gt v2, v0, :cond_2

    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 82
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->permits:I

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->pending:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 83
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->permits:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->permits:I

    .line 84
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->pending:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p0}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->permits:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->permits:I

    goto :goto_1

    :cond_2
    move v0, v1

    .line 81
    goto :goto_0

    .line 92
    :cond_3
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->passing:Z

    .line 94
    :cond_4
    return-void
.end method


# virtual methods
.method public acquire()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/AsyncLock;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 60
    .local v0, "future":Lcom/google/common/util/concurrent/SettableFuture;, "Lcom/google/common/util/concurrent/SettableFuture<Lcom/google/apps/dots/android/newsstand/store/AsyncLock;>;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 61
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->pending:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->addLast(Ljava/lang/Object;)V

    .line 62
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->tryPassPermit()V

    .line 63
    monitor-exit v2

    .line 64
    return-object v0

    .line 63
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public release()V
    .locals 2

    .prologue
    .line 68
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 69
    :try_start_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->permits:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->permits:I

    .line 70
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->tryPassPermit()V

    .line 71
    monitor-exit v1

    .line 72
    return-void

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public with(Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/apps/dots/android/newsstand/async/Task",
            "<TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "task":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->acquire()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 47
    .local v0, "permitFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/AsyncLock;>;"
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->doAfter(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/AsyncLock$1;

    invoke-direct {v2, p0, v0}, Lcom/google/apps/dots/android/newsstand/store/AsyncLock$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/AsyncLock;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 46
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addListener(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method
