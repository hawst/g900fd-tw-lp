.class public Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "MediaDrawerIntentBuilder.java"


# instance fields
.field private edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

.field private postId:Ljava/lang/String;

.field private restrictToSingleField:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 24
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 58
    const-class v2, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 59
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->postId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->restrictToSingleField:Z

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Ljava/lang/String;ZLcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 61
    .local v1, "state":Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;
    const-string v2, "MediaDrawerPagerFragment_state"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 62
    return-object v0
.end method

.method public restrictToSingleField(Z)Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;
    .locals 0
    .param p1, "restrictToSingleField"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->restrictToSingleField:Z

    .line 53
    return-object p0
.end method

.method public setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 44
    return-object p0
.end method

.method public setMediaItem(Lcom/google/apps/dots/android/newsstand/media/MediaItem;)Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;
    .locals 0
    .param p1, "mediaItem"    # Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 31
    return-object p0
.end method

.method public setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;
    .locals 0
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->postId:Ljava/lang/String;

    .line 39
    return-object p0
.end method
