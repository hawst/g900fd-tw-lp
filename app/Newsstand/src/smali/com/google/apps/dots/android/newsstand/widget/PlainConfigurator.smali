.class public abstract Lcom/google/apps/dots/android/newsstand/widget/PlainConfigurator;
.super Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;
.source "PlainConfigurator.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected alwaysUseFloatingBackground()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 3

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/PlainConfigurator;->mContext:Landroid/content/Context;

    const/4 v1, 0x2

    .line 20
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/PlainConfigurator;->getHeaderBottomMargin()I

    move-result v2

    .line 19
    invoke-static {v0, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method protected getHeaderShadowMode()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x3

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x2

    return v0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return v0
.end method
