.class Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$2;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;
.source "MyLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->setupHeaderListLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public getOffsetMode()I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x2

    return v0
.end method

.method public onPulledToRefresh()V
    .locals 2

    .prologue
    .line 150
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->currentEditionFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$300(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    move-result-object v0

    .line 151
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    if-eqz v1, :cond_0

    .line 152
    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$2$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$2$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$2;)V

    .line 153
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->refreshSubscriptionCollectionAndStartSync(Ljava/lang/Runnable;)V

    .line 160
    :cond_0
    return-void
.end method

.method public supportsPullToRefresh(I)Z
    .locals 7
    .param p1, "pagerAdapterIndex"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 134
    sget v5, Lcom/google/android/apps/newsstanddev/R$bool;->enable_developer_options:I

    .line 135
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getBooleanResource(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 136
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v5

    const-string v6, "developerMode"

    invoke-virtual {v5, v6, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_2

    move v0, v4

    .line 137
    .local v0, "isDeveloper":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 138
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 139
    invoke-static {v5, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v2

    .line 140
    .local v2, "logicalPosition":I
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .line 141
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v5

    sget v6, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DK_LIBRARY_PAGE:I

    invoke-virtual {v5, v6}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 142
    .local v1, "libraryPage":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_NEWS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    invoke-virtual {v5, v1}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_TOPICS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 143
    invoke-virtual {v5, v1}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v3, v4

    .line 145
    .end local v1    # "libraryPage":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    .end local v2    # "logicalPosition":I
    :cond_1
    return v3

    .end local v0    # "isDeveloper":Z
    :cond_2
    move v0, v3

    .line 136
    goto :goto_0
.end method
