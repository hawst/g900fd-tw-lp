.class public Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;
.super Ljava/lang/Object;
.source "AnimationUtil.java"


# direct methods
.method static synthetic access$000(Landroid/view/View;Landroid/animation/ObjectAnimator;)V
    .locals 0
    .param p0, "x0"    # Landroid/view/View;
    .param p1, "x1"    # Landroid/animation/ObjectAnimator;

    .prologue
    .line 28
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->setAlphaAnimatorTag(Landroid/view/View;Landroid/animation/ObjectAnimator;)V

    return-void
.end method

.method public static cancelFade(Landroid/view/View;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 122
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->getAlphaAnimatorTag(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 123
    .local v0, "oldAnimator":Landroid/animation/ObjectAnimator;
    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 127
    :cond_0
    return-void
.end method

.method public static clearImageViewPropertyAnimation(Landroid/view/View;)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 258
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 259
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 261
    :cond_0
    invoke-virtual {p0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 262
    invoke-virtual {p0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 263
    invoke-virtual {p0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 264
    invoke-virtual {p0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 265
    invoke-virtual {p0, v1}, Landroid/view/View;->setRotation(F)V

    .line 266
    return-void
.end method

.method public static fade(Landroid/view/View;IILjava/lang/Runnable;)V
    .locals 12
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "fadeMode"    # I
    .param p3, "finishedRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 65
    if-nez p2, :cond_1

    const/4 v4, 0x1

    .line 68
    .local v4, "fadeIn":Z
    :goto_0
    if-eqz v4, :cond_2

    const/high16 v6, 0x3f800000    # 1.0f

    .line 69
    .local v6, "alphaEnd":F
    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v7, 0x0

    .line 71
    .local v7, "alphaStart":F
    :goto_2
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->cancelFade(Landroid/view/View;)V

    .line 73
    const/16 v0, 0xa

    if-ge p1, v0, :cond_5

    .line 75
    if-eqz v4, :cond_4

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 76
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 77
    if-eqz p3, :cond_0

    .line 78
    invoke-interface {p3}, Ljava/lang/Runnable;->run()V

    .line 119
    :cond_0
    :goto_4
    return-void

    .line 65
    .end local v4    # "fadeIn":Z
    .end local v6    # "alphaEnd":F
    .end local v7    # "alphaStart":F
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 68
    .restart local v4    # "fadeIn":Z
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 69
    .restart local v6    # "alphaEnd":F
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v7

    goto :goto_2

    .line 75
    .restart local v7    # "alphaStart":F
    :cond_4
    const/4 v0, 0x4

    goto :goto_3

    .line 84
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 87
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    sub-float v2, v7, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    mul-float/2addr v0, v2

    float-to-int v9, v0

    .line 91
    .local v9, "scaledDuration":I
    invoke-virtual {p0}, Landroid/view/View;->getLayerType()I

    move-result v3

    .line 92
    .local v3, "oldLayerType":I
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->handlesAlpha(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v1, 0x1

    .line 93
    .local v1, "useHardwareLayer":Z
    :goto_5
    if-eqz v1, :cond_6

    .line 94
    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 96
    :cond_6
    invoke-virtual {p0, v7}, Landroid/view/View;->setAlpha(F)V

    .line 98
    const-string v0, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v5, 0x0

    aput v7, v2, v5

    const/4 v5, 0x1

    aput v6, v2, v5

    .line 99
    invoke-static {p0, v0, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    int-to-long v10, v9

    invoke-virtual {v0, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 100
    .local v8, "animator":Landroid/animation/ObjectAnimator;
    cmpl-float v0, v6, v7

    if-lez v0, :cond_8

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fa00000    # 1.25f

    invoke-direct {v0, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    :goto_6
    invoke-virtual {v8, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 103
    new-instance v0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;

    move-object v2, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;-><init>(ZLandroid/view/View;IZLjava/lang/Runnable;)V

    invoke-virtual {v8, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 117
    invoke-static {p0, v8}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->setAlphaAnimatorTag(Landroid/view/View;Landroid/animation/ObjectAnimator;)V

    .line 118
    invoke-virtual {v8}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_4

    .line 92
    .end local v1    # "useHardwareLayer":Z
    .end local v8    # "animator":Landroid/animation/ObjectAnimator;
    :cond_7
    const/4 v1, 0x0

    goto :goto_5

    .line 100
    .restart local v1    # "useHardwareLayer":Z
    .restart local v8    # "animator":Landroid/animation/ObjectAnimator;
    :cond_8
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v2, 0x3fa00000    # 1.25f

    invoke-direct {v0, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    goto :goto_6
.end method

.method public static fadeIn(Landroid/view/View;ILjava/lang/Runnable;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "finishedRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 132
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fade(Landroid/view/View;IILjava/lang/Runnable;)V

    .line 133
    return-void
.end method

.method public static fadeOut(Landroid/view/View;ILjava/lang/Runnable;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "finishedRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 137
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 138
    const/4 v0, 0x1

    invoke-static {p0, p1, v0, p2}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fade(Landroid/view/View;IILjava/lang/Runnable;)V

    .line 139
    return-void
.end method

.method private static getAlphaAnimatorTag(Landroid/view/View;)Landroid/animation/ObjectAnimator;
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 39
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->tagAlphaAnimator:I

    invoke-virtual {p0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 40
    .local v0, "tag":Ljava/lang/Object;
    instance-of v1, v0, Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/animation/ObjectAnimator;

    .end local v0    # "tag":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "tag":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getCenteredCircularReveal(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Z)Landroid/animation/Animator;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "revealingView"    # Landroid/view/View;
    .param p2, "revealFromView"    # Landroid/view/View;
    .param p3, "reverse"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 216
    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0x15

    if-ge v11, v12, :cond_0

    .line 217
    const/4 v0, 0x0

    .line 248
    :goto_0
    return-object v0

    .line 220
    :cond_0
    const/4 v11, 0x2

    new-array v1, v11, [I

    .line 221
    .local v1, "centerViewOffset":[I
    const/4 v11, 0x2

    new-array v8, v11, [I

    .line 222
    .local v8, "revealingViewOffset":[I
    if-nez p2, :cond_1

    move-object v10, p1

    .line 224
    .local v10, "viewToCenterOn":Landroid/view/View;
    :goto_1
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v11

    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v12

    add-int/2addr v11, v12

    div-int/lit8 v2, v11, 0x2

    .line 225
    .local v2, "centerX":I
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v11

    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    move-result v12

    add-int/2addr v11, v12

    div-int/lit8 v3, v11, 0x2

    .line 227
    .local v3, "centerY":I
    invoke-virtual {v10, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 228
    invoke-virtual {p1, v8}, Landroid/view/View;->getLocationInWindow([I)V

    .line 230
    const/4 v11, 0x0

    aget v11, v1, v11

    const/4 v12, 0x0

    aget v12, v8, v12

    sub-int/2addr v11, v12

    add-int/2addr v2, v11

    .line 231
    const/4 v11, 0x1

    aget v11, v1, v11

    const/4 v12, 0x1

    aget v12, v8, v12

    sub-int/2addr v11, v12

    add-int/2addr v3, v11

    .line 235
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v11

    sub-int/2addr v11, v2

    invoke-static {v2, v11}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 236
    .local v7, "revealWidth":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v11

    sub-int/2addr v11, v3

    invoke-static {v3, v11}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 237
    .local v6, "revealHeight":I
    mul-int v11, v7, v7

    mul-int v12, v6, v6

    add-int/2addr v11, v12

    int-to-double v12, v11

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    double-to-float v5, v12

    .line 239
    .local v5, "fullRadius":F
    if-eqz p3, :cond_2

    move v9, v5

    .line 240
    .local v9, "startRadius":F
    :goto_2
    if-eqz p3, :cond_3

    const/4 v4, 0x0

    .line 241
    .local v4, "endRadius":F
    :goto_3
    invoke-static {p1, v2, v3, v9, v4}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    .line 243
    .local v0, "animator":Landroid/animation/Animator;
    if-eqz p3, :cond_4

    .line 244
    invoke-static {p0}, Lcom/google/android/play/animation/PlayInterpolators;->slowOutFastIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v11

    .line 243
    :goto_4
    invoke-virtual {v0, v11}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 246
    const-wide/16 v12, 0x190

    invoke-virtual {v0, v12, v13}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    goto :goto_0

    .end local v0    # "animator":Landroid/animation/Animator;
    .end local v2    # "centerX":I
    .end local v3    # "centerY":I
    .end local v4    # "endRadius":F
    .end local v5    # "fullRadius":F
    .end local v6    # "revealHeight":I
    .end local v7    # "revealWidth":I
    .end local v9    # "startRadius":F
    .end local v10    # "viewToCenterOn":Landroid/view/View;
    :cond_1
    move-object/from16 v10, p2

    .line 222
    goto :goto_1

    .line 239
    .restart local v2    # "centerX":I
    .restart local v3    # "centerY":I
    .restart local v5    # "fullRadius":F
    .restart local v6    # "revealHeight":I
    .restart local v7    # "revealWidth":I
    .restart local v10    # "viewToCenterOn":Landroid/view/View;
    :cond_2
    const/4 v9, 0x0

    goto :goto_2

    .restart local v9    # "startRadius":F
    :cond_3
    move v4, v5

    .line 240
    goto :goto_3

    .line 245
    .restart local v0    # "animator":Landroid/animation/Animator;
    .restart local v4    # "endRadius":F
    :cond_4
    invoke-static {p0}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v11

    goto :goto_4
.end method

.method public static getScaleReveal(Landroid/content/Context;Landroid/view/View;Z)Landroid/animation/Animator;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "revealingView"    # Landroid/view/View;
    .param p2, "reverse"    # Z

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 179
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 180
    .local v0, "animSet":Landroid/animation/AnimatorSet;
    const-string v6, "scaleX"

    new-array v7, v10, [F

    if-eqz p2, :cond_0

    move v3, v4

    :goto_0
    aput v3, v7, v8

    if-eqz p2, :cond_1

    move v3, v5

    :goto_1
    aput v3, v7, v9

    .line 181
    invoke-static {p1, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 182
    .local v1, "scaleX":Landroid/animation/Animator;
    const-string v6, "scaleY"

    new-array v7, v10, [F

    if-eqz p2, :cond_2

    move v3, v4

    :goto_2
    aput v3, v7, v8

    if-eqz p2, :cond_3

    :goto_3
    aput v5, v7, v9

    .line 183
    invoke-static {p1, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 184
    .local v2, "scaleY":Landroid/animation/Animator;
    new-array v3, v10, [Landroid/animation/Animator;

    aput-object v1, v3, v8

    aput-object v2, v3, v9

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 185
    if-eqz p2, :cond_4

    .line 186
    invoke-static {p0}, Lcom/google/android/play/animation/PlayInterpolators;->slowOutFastIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v3

    .line 185
    :goto_4
    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 188
    const-wide/16 v4, 0x190

    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 189
    return-object v0

    .end local v1    # "scaleX":Landroid/animation/Animator;
    .end local v2    # "scaleY":Landroid/animation/Animator;
    :cond_0
    move v3, v5

    .line 180
    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1

    .restart local v1    # "scaleX":Landroid/animation/Animator;
    :cond_2
    move v3, v5

    .line 182
    goto :goto_2

    :cond_3
    move v5, v4

    goto :goto_3

    .line 187
    .restart local v2    # "scaleY":Landroid/animation/Animator;
    :cond_4
    invoke-static {p0}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v3

    goto :goto_4
.end method

.method public static handlesAlpha(Landroid/view/View;)Z
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 51
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->tagHandlesAlpha:I

    invoke-virtual {p0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 52
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_0
    return v1

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static resetFade(Landroid/view/View;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 146
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->cancelFade(Landroid/view/View;)V

    .line 147
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 148
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 149
    return-void
.end method

.method private static setAlphaAnimatorTag(Landroid/view/View;Landroid/animation/ObjectAnimator;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "animator"    # Landroid/animation/ObjectAnimator;

    .prologue
    .line 35
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->tagAlphaAnimator:I

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 36
    return-void
.end method

.method public static startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/animation/Animator;)V
    .locals 2
    .param p0, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 298
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 299
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    new-instance v0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$5;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$5;-><init>(Landroid/animation/Animator;)V

    .line 307
    .local v0, "cancellable":Lcom/google/android/libraries/bind/async/Cancellable;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$6;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$6;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/android/libraries/bind/async/Cancellable;)V

    invoke-virtual {p1, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 313
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->register(Lcom/google/android/libraries/bind/async/Cancellable;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 314
    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    .line 316
    :cond_0
    return-void
.end method

.method public static startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/view/ViewPropertyAnimator;Landroid/animation/Animator$AnimatorListener;)V
    .locals 2
    .param p0, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "animator"    # Landroid/view/ViewPropertyAnimator;
    .param p2, "optAnimatorListener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 276
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 277
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    new-instance v0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$3;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$3;-><init>(Landroid/view/ViewPropertyAnimator;)V

    .line 285
    .local v0, "cancellable":Lcom/google/android/libraries/bind/async/Cancellable;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$4;

    invoke-direct {v1, p2, p0, v0}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$4;-><init>(Landroid/animation/Animator$AnimatorListener;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/android/libraries/bind/async/Cancellable;)V

    invoke-virtual {p1, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 292
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->register(Lcom/google/android/libraries/bind/async/Cancellable;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 295
    :cond_0
    return-void
.end method

.method public static tagHandlesAlpha(Landroid/view/View;)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 47
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->tagHandlesAlpha:I

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 48
    return-void
.end method
