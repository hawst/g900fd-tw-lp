.class public Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;
.super Lcom/google/android/play/drawer/PlayDrawerLayout;
.source "NSPlayDrawerLayout.java"


# instance fields
.field private activeNavDrawerEntry:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

.field private activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

.field private preferenceListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/drawer/PlayDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;)Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->updateDrawerEntriesInternal()V

    return-void
.end method

.method private updateDrawerEntriesInternal()V
    .locals 11

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activeNavDrawerEntry:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .line 147
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->getPrimaryActions(Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Ljava/util/List;

    move-result-object v9

    .line 148
    .local v9, "primaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->getSecondaryActions(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Ljava/util/List;

    move-result-object v10

    .line 150
    .local v10, "secondaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v7

    .line 151
    .local v7, "currentAccount":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getAllGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v6

    .line 153
    .local v6, "accounts":[Landroid/accounts/Account;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activeNavDrawerEntry:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activeNavDrawerEntry:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    .line 154
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->showDownloadedOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .line 156
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->downloaded_only:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .line 157
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material_dark:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->switch_bg:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$drawable;->switch_thumb:I

    .line 160
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getOnDeviceOnly()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;-><init>(Ljava/lang/String;IIIZ)V

    move-object v4, v0

    .line 167
    .local v4, "downloadSwitchConfig":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;
    :goto_0
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v8

    .line 168
    .local v8, "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    iget-object v1, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object v0, p0

    move-object v2, v6

    move-object v3, v9

    move-object v5, v10

    invoke-virtual/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->updateContent(Ljava/lang/String;[Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;Ljava/util/List;)V

    .line 170
    invoke-static {v8}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 171
    return-void

    .line 160
    .end local v4    # "downloadSwitchConfig":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;
    .end local v8    # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public initializeDrawer(ZLcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;)V
    .locals 9
    .param p1, "isAccountListExpanded"    # Z
    .param p2, "activeNavDrawerEntry"    # Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    .prologue
    .line 50
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->playCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;

    move-result-object v8

    .line 51
    .local v8, "playCommonNetworkStack":Lcom/google/android/play/utils/PlayCommonNetworkStack;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .line 53
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/newsstanddev/R$dimen;->action_bar_default_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const/4 v4, 0x1

    .line 55
    invoke-virtual {v8}, Lcom/google/android/play/utils/PlayCommonNetworkStack;->getPlayDfeApiProvider()Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    move-result-object v5

    .line 56
    invoke-virtual {v8}, Lcom/google/android/play/utils/PlayCommonNetworkStack;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v6

    new-instance v7, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout$1;

    invoke-direct {v7, p0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout$1;-><init>(Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;)V

    move-object v0, p0

    move v2, p1

    .line 51
    invoke-virtual/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->configure(Landroid/app/Activity;ZIZLcom/google/android/play/dfe/api/PlayDfeApiProvider;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;)V

    .line 116
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->updateDrawerEntries(Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;)V

    .line 117
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->syncDrawerIndicator()V

    .line 118
    return-void
.end method

.method public isDrawerVisible()Z
    .locals 2

    .prologue
    .line 174
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->isLocaleRtl()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x5

    .line 175
    .local v0, "targetGravity":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->isDrawerVisible(I)Z

    move-result v1

    return v1

    .line 174
    .end local v0    # "targetGravity":I
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 5

    .prologue
    .line 122
    invoke-super {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->onAttachedToWindow()V

    .line 123
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout$2;-><init>(Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "downloadedOnly"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->preferenceListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 129
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->preferenceListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->preferenceListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->preferenceListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 137
    :cond_0
    invoke-super {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->onDetachedFromWindow()V

    .line 138
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 2
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 181
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getActivityFromView(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    .line 182
    .local v0, "activity":Landroid/app/Activity;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    if-eqz v1, :cond_0

    .line 183
    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->getActiveNavDrawerEntry()Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->updateDrawerEntries(Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;)V

    .line 185
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->onDrawerOpened(Landroid/view/View;)V

    .line 186
    return-void
.end method

.method public updateDrawerEntries(Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;)V
    .locals 0
    .param p1, "activeNavDrawerEntry"    # Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->activeNavDrawerEntry:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    .line 142
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->updateDrawerEntriesInternal()V

    .line 143
    return-void
.end method
