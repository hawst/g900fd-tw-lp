.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;
.super Ljava/lang/Object;
.source "YearMonthIssuesFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final endMonth:I

.field public final hideArchive:Z

.field public final month:I

.field public final year:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIZ)V
    .locals 0
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "endMonth"    # I
    .param p4, "hideArchive"    # Z

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->year:I

    .line 21
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    .line 22
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->endMonth:I

    .line 23
    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->hideArchive:Z

    .line 24
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 36
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    .line 38
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->year:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->year:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->endMonth:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->endMonth:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->hideArchive:Z

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->hideArchive:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 41
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 46
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->year:I

    mul-int/lit16 v0, v0, 0x2710

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    mul-int/lit8 v1, v1, 0x64

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->endMonth:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 28
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "titleIssuesFragmentState: year=%d %s%s - %s"

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/Object;

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->year:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v8

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    if-lez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "month=%d"

    new-array v5, v7, [Ljava/lang/Object;

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    .line 29
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v7

    const/4 v4, 0x2

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->endMonth:I

    if-lez v0, :cond_1

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "-%d"

    new-array v6, v7, [Ljava/lang/Object;

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->endMonth:I

    .line 30
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v0, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v3, v4

    const/4 v0, 0x3

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->hideArchive:Z

    .line 31
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    .line 28
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 29
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 30
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 56
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->year:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 57
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->month:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 58
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->endMonth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;->hideArchive:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60
    return-void
.end method
