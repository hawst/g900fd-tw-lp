.class Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$2;
.super Ljava/lang/Object;
.source "CardSourceItemClickableMenuView.java"

# interfaces
.implements Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->onPreparePopupMenu(Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 4

    .prologue
    .line 213
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    .line 214
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    .line 215
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->access$100(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v3

    .line 213
    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->resetNewsSubscriptionTranslation(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    .line 216
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    .line 217
    .local v0, "context":Landroid/content/Context;
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->language_reset:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 218
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 219
    return-void
.end method
