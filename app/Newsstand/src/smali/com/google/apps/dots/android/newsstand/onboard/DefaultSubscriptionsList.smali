.class public Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;
.super Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
.source "DefaultSubscriptionsList.java"


# static fields
.field private static final SUPPORTED_COLLECTION_IDS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    const-string v0, "default-curation-collection"

    const-string v1, "optional-curation-collection"

    .line 44
    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;->SUPPORTED_COLLECTION_IDS:Ljava/util/Set;

    .line 43
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_ITEM_ID:I

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;-><init>(I)V

    .line 48
    return-void
.end method

.method static synthetic access$000()Ljava/util/Set;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;->SUPPORTED_COLLECTION_IDS:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 52
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getDefaultSubscriptions(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
    .locals 4
    .param p1, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "rootNode"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 61
    .local v1, "dataFutures":Ljava/util/List;, "Ljava/util/List<Lcom/google/common/util/concurrent/ListenableFuture<+Lcom/google/android/libraries/bind/data/Data;>;>;"
    new-instance v2, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;->primaryKey()I

    move-result v3

    invoke-direct {v2, p0, v3, p2, v1}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;ILcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/List;)V

    .line 123
    .local v2, "visitor":Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;

    invoke-direct {v3, p2, p3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 124
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList(Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 125
    .local v0, "allAsList":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    return-object v3
.end method
