.class public abstract Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;
.super Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;
.source "TranslateDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private editionLanguageCode:Ljava/lang/String;

.field private translationCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;-><init>()V

    .line 42
    return-void
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->translationCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->languageToLanguageCode(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static autoDetectLanguagePosition(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->languageCodeToPosition(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 142
    .local v0, "position":I
    if-gez v0, :cond_0

    .line 143
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->languageCodeToPosition(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 145
    :cond_0
    return v0
.end method

.method private static languageCodeToPosition(Landroid/content/Context;Ljava/lang/String;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 163
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$array;->language_codes_array:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "languageCodes":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 165
    aget-object v2, v1, v0

    invoke-static {v2, p1}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->languagesEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 169
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 164
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static languageToLanguageCode(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 149
    invoke-static {}, Ljava/util/Locale;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v3

    .line 150
    .local v3, "locales":[Ljava/util/Locale;
    aget-object v4, v3, v5

    invoke-virtual {v4}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    .line 151
    aget-object v4, v3, v5

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    .line 152
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$array;->languages_array:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 153
    .local v2, "languageNames":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$array;->language_codes_array:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "languageCodes":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 155
    aget-object v4, v2, v0

    invoke-static {v4, p1}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->languagesEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 156
    aget-object v4, v1, v0

    .line 159
    :goto_1
    return-object v4

    .line 154
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 159
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private static languagesEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p0, "codeA"    # Ljava/lang/String;
    .param p1, "codeB"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 173
    invoke-virtual {p0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 176
    :cond_1
    const-string v1, "_"

    const-string v2, "-"

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "_"

    const-string v3, "-"

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 179
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 45
    if-nez p1, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    const-string v0, "TranslateDialog_edition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49
    const-string v0, "TranslateDialog_edition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 51
    :cond_2
    const-string v0, "TranslateDialog_editionLanguageCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 52
    const-string v0, "TranslateDialog_editionLanguageCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->editionLanguageCode:Ljava/lang/String;

    .line 54
    :cond_3
    const-string v0, "TranslateDialog_translationCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    const-string v0, "TranslateDialog_translationCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->translationCode:Ljava/lang/String;

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 126
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->translationCode:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->translationCode:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->translateEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v11, 0x8

    const/4 v8, 0x0

    .line 69
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->handleExtras(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 71
    .local v2, "inflater":Landroid/view/LayoutInflater;
    sget v9, Lcom/google/android/apps/newsstanddev/R$layout;->translate_dialog:I

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 73
    .local v1, "dialogView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/apps/newsstanddev/R$array;->languages_array:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 76
    .local v4, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget v9, Lcom/google/android/apps/newsstanddev/R$id;->translation_languages_spinner:I

    .line 77
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Spinner;

    .line 78
    .local v7, "translationSpinner":Landroid/widget/Spinner;
    new-instance v9, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog$1;

    invoke-direct {v9, p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog$1;-><init>(Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;)V

    invoke-virtual {v7, v9}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 90
    const/4 v6, 0x0

    .line 91
    .local v6, "translationCodePosition":I
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->translationCode:Ljava/lang/String;

    invoke-static {v9}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 92
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->autoDetectLanguagePosition(Landroid/content/Context;)I

    move-result v6

    .line 96
    :goto_0
    invoke-static {v8, v6}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/widget/Spinner;->setSelection(I)V

    .line 99
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->editionLanguageCode:Ljava/lang/String;

    invoke-static {v9}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    .line 100
    .local v5, "showEditionLanguage":Z
    if-eqz v5, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->editionLanguageCode:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->languageCodeToPosition(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    .line 102
    .local v3, "languagePosition":I
    if-ltz v3, :cond_4

    const/4 v5, 0x1

    .line 103
    :goto_1
    if-eqz v5, :cond_0

    .line 104
    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->edition_language:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 105
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/CharSequence;

    .line 104
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    .end local v3    # "languagePosition":I
    :cond_0
    if-nez v5, :cond_1

    .line 109
    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->default_language:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    .line 110
    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->edition_language:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    .line 113
    :cond_1
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 114
    invoke-virtual {v8, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    sget v9, Lcom/google/android/apps/newsstanddev/R$string;->translate_to:I

    .line 115
    invoke-virtual {p0, v9}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    .line 116
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_launcher_translate:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    sget v9, Lcom/google/android/apps/newsstanddev/R$string;->translate:I

    .line 117
    invoke-virtual {v8, v9, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 118
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->isBlindAccessibilityEnabledFromContext(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 119
    sget v8, Lcom/google/android/apps/newsstanddev/R$string;->cancel:I

    invoke-virtual {v0, v8, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 121
    :cond_2
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    return-object v8

    .line 94
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v5    # "showEditionLanguage":Z
    :cond_3
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->translationCode:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->languageCodeToPosition(Landroid/content/Context;Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    .restart local v3    # "languagePosition":I
    .restart local v5    # "showEditionLanguage":Z
    :cond_4
    move v5, v8

    .line 102
    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    const-string v0, "TranslateDialog_edition"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 62
    const-string v0, "TranslateDialog_editionLanguageCode"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->editionLanguageCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v0, "TranslateDialog_translationCode"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->translationCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 65
    return-void
.end method

.method protected abstract translateEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V
.end method
