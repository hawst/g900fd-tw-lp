.class public Lcom/google/apps/dots/android/newsstand/async/ImmediateTask;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "ImmediateTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final result:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/ImmediateTask;, "Lcom/google/apps/dots/android/newsstand/async/ImmediateTask<TV;>;"
    .local p1, "result":Ljava/lang/Object;, "TV;"
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;)V

    .line 20
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/ImmediateTask;->result:Ljava/lang/Object;

    .line 21
    return-void
.end method

.method public static create(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/async/Task;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(TV;)",
            "Lcom/google/apps/dots/android/newsstand/async/Task",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 15
    .local p0, "result":Ljava/lang/Object;, "TV;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/ImmediateTask;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/async/ImmediateTask;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/ImmediateTask;, "Lcom/google/apps/dots/android/newsstand/async/ImmediateTask<TV;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/ImmediateTask;->result:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 11
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/ImmediateTask;, "Lcom/google/apps/dots/android/newsstand/async/ImmediateTask<TV;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/ImmediateTask;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
