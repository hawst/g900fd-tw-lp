.class Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;
.super Ljava/lang/Object;
.source "RenderSourceImpl.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->loadSerializedPostData()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;

.field final synthetic val$formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$postFuture:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;->val$postFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;->val$formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;->apply(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public apply(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3
    .param p1, "unusedResult"    # Ljava/lang/Object;

    .prologue
    .line 223
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;->val$postFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v2}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 224
    .local v1, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;->val$formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v2}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .line 225
    .local v0, "form":Lcom/google/apps/dots/proto/client/DotsShared$Form;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->itemJsonSerializer:Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->access$100(Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;)Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->getEncodedPostData(Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/proto/client/DotsShared$Form;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
