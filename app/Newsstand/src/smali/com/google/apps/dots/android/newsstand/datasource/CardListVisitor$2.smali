.class Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$2;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardListVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeMagazineSplashCardData(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

.field final synthetic val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .prologue
    .line 461
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$2;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 465
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineClickEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->getAnalyticsScreenName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$2;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineClickEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineClickEvent;->track(Z)V

    .line 466
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$2;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v0, p2, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    const-class v1, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->primary_image:I

    .line 468
    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewInParent(Landroid/view/View;Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v1

    .line 467
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->setTransitionHeroElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;

    move-result-object v0

    .line 469
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->start()V

    .line 470
    return-void
.end method
