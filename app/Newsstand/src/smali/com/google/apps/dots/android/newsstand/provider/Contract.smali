.class public Lcom/google/apps/dots/android/newsstand/provider/Contract;
.super Ljava/lang/Object;
.source "Contract.java"


# static fields
.field private static uriMatcher:Landroid/content/UriMatcher;


# direct methods
.method public static match(Landroid/net/Uri;)I
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 47
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/Contract;->uriMatcher()Landroid/content/UriMatcher;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method private static uriMatcher()Landroid/content/UriMatcher;
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 32
    sget-object v2, Lcom/google/apps/dots/android/newsstand/provider/Contract;->uriMatcher:Landroid/content/UriMatcher;

    if-nez v2, :cond_0

    .line 33
    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    .line 34
    .local v1, "matcher":Landroid/content/UriMatcher;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentAuthority()Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "authority":Ljava/lang/String;
    const-string v2, "attachment/"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 36
    const-string v2, "attachment/*"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 37
    const-string v2, "attachment/*/*"

    const/4 v3, 0x4

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 38
    const-string v2, "synced/*"

    const/4 v3, 0x6

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 39
    const-string v2, "nsstore/*/*"

    const/16 v3, 0xb

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 40
    const-string v2, "nsstore/*/*/#"

    const/16 v3, 0xc

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 41
    sput-object v1, Lcom/google/apps/dots/android/newsstand/provider/Contract;->uriMatcher:Landroid/content/UriMatcher;

    .line 43
    :cond_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/provider/Contract;->uriMatcher:Landroid/content/UriMatcher;

    return-object v2
.end method
