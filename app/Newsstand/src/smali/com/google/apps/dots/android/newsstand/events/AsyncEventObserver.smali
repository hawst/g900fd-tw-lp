.class public abstract Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver;
.super Ljava/lang/Object;
.source "AsyncEventObserver.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;


# instance fields
.field private final executor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver;->executor:Ljava/util/concurrent/Executor;

    .line 19
    return-void
.end method


# virtual methods
.method public final onEvent(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p2, "extras":Ljava/util/Map;, "Ljava/util/Map<**>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver$1;-><init>(Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver;Landroid/net/Uri;Ljava/util/Map;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 29
    return-void
.end method

.method protected abstract onEventAsync(Landroid/net/Uri;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<**>;)V"
        }
    .end annotation
.end method
