.class Lcom/google/apps/dots/android/newsstand/search/SearchFragment$4;
.super Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->setUpAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragment;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-void
.end method


# virtual methods
.method protected isLoading()Z
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$4;->showErrorView()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->access$300(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->haveAllGroupsRefreshedOnce()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;
    .locals 4

    .prologue
    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "exception":Lcom/google/android/libraries/bind/data/DataException;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->searchResultsLists:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->access$200(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    .line 124
    .local v1, "list":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v0

    .line 125
    if-eqz v0, :cond_0

    .line 129
    .end local v1    # "list":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;
    :cond_1
    return-object v0
.end method

.method protected showErrorView()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 135
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->searchResultsLists:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->access$200(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 143
    :goto_0
    return v1

    .line 138
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->searchResultsLists:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->access$200(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    .line 139
    .local v0, "list":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->didLastRefreshFail()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 143
    .end local v0    # "list":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method
