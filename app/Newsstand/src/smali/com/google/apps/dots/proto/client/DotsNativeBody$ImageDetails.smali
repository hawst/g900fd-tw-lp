.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ImageDetails"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;


# instance fields
.field private bitField0_:I

.field private imageFieldId_:Ljava/lang/String;

.field private imageValueIndex_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2068
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2069
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2074
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageFieldId_:Ljava/lang/String;

    .line 2096
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageValueIndex_:I

    .line 2069
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;
    .locals 3

    .prologue
    .line 2126
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2130
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;
    return-object v0

    .line 2127
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;
    :catch_0
    move-exception v1

    .line 2128
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2065
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2135
    if-ne p1, p0, :cond_1

    .line 2138
    :cond_0
    :goto_0
    return v1

    .line 2136
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2137
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    .line 2138
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageFieldId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageFieldId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageValueIndex_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageValueIndex_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageFieldId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageFieldId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getImageFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2076
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageFieldId_:Ljava/lang/String;

    return-object v0
.end method

.method public getImageValueIndex()I
    .locals 1

    .prologue
    .line 2098
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageValueIndex_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2162
    const/4 v0, 0x0

    .line 2163
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2164
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageFieldId_:Ljava/lang/String;

    .line 2165
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2167
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2168
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageValueIndex_:I

    .line 2169
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2171
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->cachedSize:I

    .line 2172
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 2143
    const/16 v0, 0x11

    .line 2144
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 2145
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageFieldId_:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 2146
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageValueIndex_:I

    add-int v0, v1, v2

    .line 2147
    return v0

    .line 2145
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageFieldId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2180
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2181
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2185
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2186
    :sswitch_0
    return-object p0

    .line 2191
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageFieldId_:Ljava/lang/String;

    .line 2192
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->bitField0_:I

    goto :goto_0

    .line 2196
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageValueIndex_:I

    .line 2197
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->bitField0_:I

    goto :goto_0

    .line 2181
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2065
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2152
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2153
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageFieldId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2155
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2156
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->imageValueIndex_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2158
    :cond_1
    return-void
.end method
