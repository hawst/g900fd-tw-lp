.class Lcom/google/apps/dots/android/newsstand/activity/StartActivity$4;
.super Ljava/lang/Object;
.source "StartActivity.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->navigateToTarget()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/apps/dots/android/newsstand/share/TargetInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$4;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/apps/dots/android/newsstand/share/TargetInfo;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$4;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->findTargetFromIntent(Landroid/content/Intent;Z)Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$4;->call()Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    move-result-object v0

    return-object v0
.end method
