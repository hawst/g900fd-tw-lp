.class public abstract Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;
.super Landroid/widget/ImageView;
.source "ClickableMenuView.java"


# instance fields
.field private hitRect:Landroid/graphics/Rect;

.field private popupMenu:Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->hitRect:Landroid/graphics/Rect;

    .line 36
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->setClickable(Z)V

    .line 37
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    return-void
.end method

.method private getOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;)V

    return-object v0
.end method


# virtual methods
.method protected dismissPopupMenuIfNeeded()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->popupMenu:Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->popupMenu:Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->dismiss()V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->popupMenu:Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;

    .line 93
    :cond_0
    return-void
.end method

.method protected getExtraTouchPaddingPx()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method protected onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 77
    new-instance v0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;

    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->popupMenu:Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;

    .line 78
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->popupMenu:Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->onPreparePopupMenu(Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;)V

    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->popupMenu:Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->show()V

    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->popupMenu:Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->setOnPopupDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 86
    return-void
.end method

.method protected onDismiss()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->popupMenu:Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;

    .line 97
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    .prologue
    .line 53
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 54
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->getExtraTouchPaddingPx()I

    move-result v0

    .line 59
    .local v0, "extraTouchPadding":I
    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->hitRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->getHitRect(Landroid/graphics/Rect;)V

    .line 61
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->hitRect:Landroid/graphics/Rect;

    neg-int v2, v0

    neg-int v3, v0

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->inset(II)V

    .line 62
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    new-instance v2, Landroid/view/TouchDelegate;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->hitRect:Landroid/graphics/Rect;

    invoke-direct {v2, v3, p0}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto :goto_0
.end method

.method protected abstract onPreparePopupMenu(Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;)V
.end method
