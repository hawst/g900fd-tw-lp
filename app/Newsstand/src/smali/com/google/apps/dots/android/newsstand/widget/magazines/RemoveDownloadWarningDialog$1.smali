.class Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "RemoveDownloadWarningDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;

.field final synthetic val$editionSubtitleTextView:Landroid/widget/TextView;

.field final synthetic val$editionTitleTextView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;->val$editionTitleTextView:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;->val$editionSubtitleTextView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 5
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    const/16 v4, 0x8

    .line 94
    const/4 v1, 0x0

    .line 95
    .local v1, "editionTitle":Ljava/lang/String;
    const/4 v0, 0x0

    .line 97
    .local v0, "editionSubtitle":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v2, v3, :cond_0

    .line 98
    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v1

    .line 99
    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 101
    :cond_0
    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 102
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;->val$editionTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 107
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;->val$editionSubtitleTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    :goto_1
    return-void

    .line 104
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;->val$editionTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 109
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;->val$editionSubtitleTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 91
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
