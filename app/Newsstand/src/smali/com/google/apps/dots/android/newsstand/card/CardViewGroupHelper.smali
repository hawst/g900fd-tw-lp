.class public Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;
.super Ljava/lang/Object;
.source "CardViewGroupHelper.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/card/ResizingCard;


# instance fields
.field private final cardViewGroup:Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

.field private isScrollListenerAdded:Z

.field protected onScrollListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

.field private resizeType:I

.field private suppressAnalytics:Z

.field private timeAttachedToScreen:J


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;)V
    .locals 2
    .param p1, "cardViewGroup"    # Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->timeAttachedToScreen:J

    .line 72
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onScrollListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    .line 38
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->cardViewGroup:Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;)Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->cardViewGroup:Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->suppressAnalytics:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->updateOnScrollChangedListener()V

    return-void
.end method

.method private updateOnScrollChangedListener()V
    .locals 3

    .prologue
    .line 116
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->cardViewGroup:Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->getBindingViewGroupHelper()Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    move-result-object v0

    .line 117
    .local v0, "bindingViewGroupHelper":Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->cardViewGroup:Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

    instance-of v1, v1, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 118
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->isAttached()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 119
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->isTemporarilyDetached()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->suppressAnalytics:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->isScrollListenerAdded:Z

    if-nez v1, :cond_1

    .line 122
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->cardViewGroup:Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onScrollListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 123
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->isScrollListenerAdded:Z

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->isScrollListenerAdded:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->suppressAnalytics:Z

    if-nez v1, :cond_2

    .line 125
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->isAttached()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->isTemporarilyDetached()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->cardViewGroup:Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onScrollListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    .line 129
    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 130
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->isScrollListenerAdded:Z

    goto :goto_0
.end method


# virtual methods
.method protected beenOnScreenForTimeThreshold()Z
    .locals 4

    .prologue
    .line 136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->timeAttachedToScreen:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x190

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canEnlarge()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 69
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->resizeType:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canShrink()Z
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->resizeType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    sget-object v2, Lcom/google/android/apps/newsstanddev/R$styleable;->CardViewGroup:[I

    const/4 v3, 0x0

    .line 43
    invoke-virtual {p1, p2, v2, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 44
    .local v0, "array":Landroid/content/res/TypedArray;
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->CardViewGroup_resizeType:I

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 45
    .local v1, "resizeType":I
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->setResizeType(I)V

    .line 46
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 47
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 158
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->timeAttachedToScreen:J

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->suppressAnalytics:Z

    .line 160
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->updateOnScrollChangedListener()V

    .line 161
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->updateOnScrollChangedListener()V

    .line 154
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 2

    .prologue
    .line 141
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->timeAttachedToScreen:J

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->suppressAnalytics:Z

    .line 143
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->updateOnScrollChangedListener()V

    .line 144
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->updateOnScrollChangedListener()V

    .line 149
    return-void
.end method

.method public setResizeType(I)V
    .locals 2
    .param p1, "resizeType"    # I

    .prologue
    .line 50
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid resize type specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->resizeType:I

    .line 56
    return-void
.end method
