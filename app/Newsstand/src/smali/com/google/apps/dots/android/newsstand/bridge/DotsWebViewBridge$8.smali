.class Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$8;
.super Ljava/lang/Object;
.source "DotsWebViewBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->requestPurchase(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

.field final synthetic val$uri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$8;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$8;->val$uri:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 284
    # getter for: Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "requesting purchase: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$8;->val$uri:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 285
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$8;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$8;->val$uri:Ljava/lang/String;

    .line 286
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setPath(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v0

    .line 287
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->start()V

    .line 288
    return-void
.end method
