.class Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;
.super Ljava/lang/Object;
.source "StoreCacheBase.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->getCacheItem(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;

    .prologue
    .line 120
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;, "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->this$0:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p1, "response"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
            "<TT;>;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;, "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;"
    const/4 v0, 0x0

    .line 125
    .local v0, "corruptionException":Ljava/lang/Exception;
    :try_start_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->this$0:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;

    invoke-virtual {v3, p1}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->parse(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    move-result-object v2

    .line 126
    .local v2, "parsed":Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;, "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem<TT;>;"
    # getter for: Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "Parsed %s, size %s, version %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, v2, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->sizeKb:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-object v7, p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->this$0:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->cache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-virtual {v3, v4, v5, v2}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->put(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;)V

    .line 128
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 133
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->this$0:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->access$100(Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;)Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-virtual {v4, v5, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->deleteStoreFileForCorruptResponseIfNeeded(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/lang/Exception;)V

    return-object v3

    .line 129
    .end local v2    # "parsed":Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;, "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem<TT;>;"
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    move-object v0, v1

    .line 131
    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    .end local v1    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->this$0:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->access$100(Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;)Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-virtual {v4, v5, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->deleteStoreFileForCorruptResponseIfNeeded(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/lang/Exception;)V

    throw v3
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 120
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;, "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;->apply(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
