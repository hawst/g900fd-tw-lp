.class final Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$2;
.super Ljava/lang/Object;
.source "AuthUiHelper.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->checkAllowOnboardingFlow()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 310
    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Gservices: failed call to check if onboarding flow allowed."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 311
    return-void
.end method

.method public onSuccess(Ljava/lang/Boolean;)V
    .locals 5
    .param p1, "allow"    # Ljava/lang/Boolean;

    .prologue
    .line 315
    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Gservices: onboarding %s."

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "allowed"

    :goto_0
    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    return-void

    .line 315
    :cond_0
    const-string v0, "not allowed"

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 306
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$2;->onSuccess(Ljava/lang/Boolean;)V

    return-void
.end method
