.class public final Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScoredCountry"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;


# instance fields
.field private bitField0_:I

.field private countryCode_:Ljava/lang/String;

.field private score_:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30437
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 30438
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 30443
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->countryCode_:Ljava/lang/String;

    .line 30465
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->score_:D

    .line 30438
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    .locals 3

    .prologue
    .line 30495
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30499
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    return-object v0

    .line 30496
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    :catch_0
    move-exception v1

    .line 30497
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 30434
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30504
    if-ne p1, p0, :cond_1

    .line 30507
    :cond_0
    :goto_0
    return v1

    .line 30505
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 30506
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    .line 30507
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->countryCode_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->countryCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->score_:D

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->score_:D

    cmpl-double v3, v4, v6

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->countryCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->countryCode_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 30531
    const/4 v0, 0x0

    .line 30532
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 30533
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->countryCode_:Ljava/lang/String;

    .line 30534
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30536
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 30537
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->score_:D

    .line 30538
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 30540
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->cachedSize:I

    .line 30541
    return v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 30512
    const/16 v0, 0x11

    .line 30513
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 30514
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->countryCode_:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 30515
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->score_:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->score_:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 30516
    return v0

    .line 30514
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->countryCode_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30549
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 30550
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 30554
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 30555
    :sswitch_0
    return-object p0

    .line 30560
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->countryCode_:Ljava/lang/String;

    .line 30561
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->bitField0_:I

    goto :goto_0

    .line 30565
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->score_:D

    .line 30566
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->bitField0_:I

    goto :goto_0

    .line 30550
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30434
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30521
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 30522
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->countryCode_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 30524
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 30525
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->score_:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 30527
    :cond_1
    return-void
.end method
