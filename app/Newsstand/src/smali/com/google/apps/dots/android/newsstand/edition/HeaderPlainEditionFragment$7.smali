.class Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;
.super Lcom/google/apps/dots/android/newsstand/animation/BaseAnimatorListener;
.source "HeaderPlainEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->startSubscribeAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 351
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/animation/BaseAnimatorListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->isSubscribed()Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$1200(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateImageRotatorColor()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$1300(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->coloredOverlay:Landroid/view/View;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$1100(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 377
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 355
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .line 356
    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->actionBarColor(Landroid/content/Context;Z)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 357
    .local v0, "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$integer;->header_overlay_alpha_value:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 361
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->coloredOverlay:Landroid/view/View;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$1100(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 362
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->coloredOverlay:Landroid/view/View;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$1100(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 365
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->isSubscribed()Z
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$1200(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 366
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateImageRotatorColor()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$1300(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V

    .line 368
    :cond_0
    return-void
.end method
