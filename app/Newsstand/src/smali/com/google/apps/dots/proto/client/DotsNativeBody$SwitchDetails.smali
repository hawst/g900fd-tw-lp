.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SwitchDetails"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;


# instance fields
.field private bitField0_:I

.field private initialState_:I

.field private loopAround_:Z

.field private stateCount_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2820
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2821
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2826
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->stateCount_:I

    .line 2845
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->initialState_:I

    .line 2864
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->loopAround_:Z

    .line 2821
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;
    .locals 3

    .prologue
    .line 2895
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2899
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;
    return-object v0

    .line 2896
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;
    :catch_0
    move-exception v1

    .line 2897
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2817
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2904
    if-ne p1, p0, :cond_1

    .line 2907
    :cond_0
    :goto_0
    return v1

    .line 2905
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2906
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    .line 2907
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->stateCount_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->stateCount_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->initialState_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->initialState_:I

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->loopAround_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->loopAround_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getInitialState()I
    .locals 1

    .prologue
    .line 2847
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->initialState_:I

    return v0
.end method

.method public getLoopAround()Z
    .locals 1

    .prologue
    .line 2866
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->loopAround_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2936
    const/4 v0, 0x0

    .line 2937
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2938
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->stateCount_:I

    .line 2939
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2941
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2942
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->initialState_:I

    .line 2943
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2945
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2946
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->loopAround_:Z

    .line 2947
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2949
    :cond_2
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->cachedSize:I

    .line 2950
    return v0
.end method

.method public getStateCount()I
    .locals 1

    .prologue
    .line 2828
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->stateCount_:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 2913
    const/16 v0, 0x11

    .line 2914
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 2915
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->stateCount_:I

    add-int v0, v1, v2

    .line 2916
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->initialState_:I

    add-int v0, v1, v2

    .line 2917
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->loopAround_:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    add-int v0, v2, v1

    .line 2918
    return v0

    .line 2917
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2958
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2959
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2963
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2964
    :sswitch_0
    return-object p0

    .line 2969
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->stateCount_:I

    .line 2970
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    goto :goto_0

    .line 2974
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->initialState_:I

    .line 2975
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    goto :goto_0

    .line 2979
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->loopAround_:Z

    .line 2980
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    goto :goto_0

    .line 2959
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2817
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2923
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2924
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->stateCount_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2926
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2927
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->initialState_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2929
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2930
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->loopAround_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2932
    :cond_2
    return-void
.end method
