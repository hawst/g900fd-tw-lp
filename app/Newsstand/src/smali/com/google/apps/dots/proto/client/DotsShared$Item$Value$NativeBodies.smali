.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NativeBodies"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;


# instance fields
.field private landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

.field private portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14860
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14861
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;
    .locals 3

    .prologue
    .line 14912
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 14916
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-eqz v2, :cond_0

    .line 14917
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    .line 14919
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-eqz v2, :cond_1

    .line 14920
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    .line 14922
    :cond_1
    return-object v0

    .line 14913
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;
    :catch_0
    move-exception v1

    .line 14914
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 14857
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 14927
    if-ne p1, p0, :cond_1

    .line 14931
    :cond_0
    :goto_0
    return v1

    .line 14928
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 14929
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .line 14930
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 14931
    goto :goto_0

    .line 14930
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    .line 14931
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getLandscapeNativeBody()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
    .locals 1

    .prologue
    .line 14885
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    return-object v0
.end method

.method public getPortraitNativeBody()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
    .locals 1

    .prologue
    .line 14866
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 14954
    const/4 v0, 0x0

    .line 14955
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-eqz v1, :cond_0

    .line 14956
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    .line 14957
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14959
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-eqz v1, :cond_1

    .line 14960
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    .line 14961
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14963
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->cachedSize:I

    .line 14964
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 14935
    const/16 v0, 0x11

    .line 14936
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 14937
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 14938
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 14939
    return v0

    .line 14937
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->hashCode()I

    move-result v1

    goto :goto_0

    .line 14938
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14972
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 14973
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 14977
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 14978
    :sswitch_0
    return-object p0

    .line 14983
    :sswitch_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-nez v1, :cond_1

    .line 14984
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    .line 14986
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 14990
    :sswitch_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-nez v1, :cond_2

    .line 14991
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    .line 14993
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 14973
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14857
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14944
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-eqz v0, :cond_0

    .line 14945
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->portraitNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 14947
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-eqz v0, :cond_1

    .line 14948
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->landscapeNativeBody_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 14950
    :cond_1
    return-void
.end method
