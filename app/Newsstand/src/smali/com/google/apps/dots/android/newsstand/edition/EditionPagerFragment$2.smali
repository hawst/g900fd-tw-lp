.class Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;
.source "EditionPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->setupHeaderListLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 178
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public onPullToRefreshDisplayedPageChanged(Landroid/support/v4/widget/SwipeRefreshLayout;I)V
    .locals 5
    .param p1, "swipeRefreshLayout"    # Landroid/support/v4/widget/SwipeRefreshLayout;
    .param p2, "pagerAdapterIndex"    # I

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;"
    const/4 v4, 0x1

    .line 189
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 190
    invoke-static {v3, p2}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 189
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageEdition(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 191
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-eqz v0, :cond_0

    .line 192
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2, v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->actionBarColor(Landroid/content/Context;Z)I

    move-result v1

    .line 193
    .local v1, "editionColor":I
    new-array v2, v4, [I

    const/4 v3, 0x0

    aput v1, v2, v3

    invoke-virtual {p1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    .line 195
    .end local v1    # "editionColor":I
    :cond_0
    return-void
.end method

.method public onPulledToRefresh()V
    .locals 1

    .prologue
    .line 199
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->onPulledToRefresh()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V

    .line 200
    return-void
.end method

.method public supportsPullToRefresh(I)Z
    .locals 3
    .param p1, "pagerAdapterIndex"    # I

    .prologue
    .line 181
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 182
    invoke-static {v2, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 181
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageEdition(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 183
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->supportsRefresh()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
