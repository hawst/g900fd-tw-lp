.class Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog$1;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "MyNewsTranslateDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;->translateEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;

.field final synthetic val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$languageCode:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog$1;->val$languageCode:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 4
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog$1;->val$languageCode:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->translateNewsSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;Z)V

    .line 43
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog$1;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
