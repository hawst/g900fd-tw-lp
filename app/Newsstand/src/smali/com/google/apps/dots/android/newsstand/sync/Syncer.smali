.class public Lcom/google/apps/dots/android/newsstand/sync/Syncer;
.super Ljava/lang/Object;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;,
        Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;,
        Lcom/google/apps/dots/android/newsstand/sync/Syncer$SkippedTaskException;,
        Lcom/google/apps/dots/android/newsstand/sync/Syncer$FatalSyncerException;,
        Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;,
        Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final sharedTaskQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;


# instance fields
.field private final appFamilySummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

.field private final appSummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

.field private final applicationStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

.field private final attachmentStore:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

.field private errorCounts:Lcom/google/common/collect/Multiset;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Multiset",
            "<",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;",
            ">;"
        }
    .end annotation
.end field

.field private final formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

.field private final formTemplateStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

.field private ignoreCounts:Lcom/google/common/collect/Multiset;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Multiset",
            "<",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;",
            ">;"
        }
    .end annotation
.end field

.field private final lock:Ljava/lang/Object;

.field private final mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

.field private final nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field private final postStore:Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

.field private volatile priorityBoostMs:J

.field private progressCompleted:F

.field private progressListener:Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;

.field private progressRequired:F

.field private progressRunnable:Ljava/lang/Runnable;

.field private reportedProgress:F

.field private final sectionStore:Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

.field private final syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

.field private syncingFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private final token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private final visited:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 80
    const-class v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 202
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;-><init>(I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->sharedTaskQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;)V
    .locals 1
    .param p1, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "syncerRequest"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    .prologue
    .line 391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 205
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .line 206
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->attachmentStore:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .line 207
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->postStore:Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    .line 208
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->sectionStore()Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->sectionStore:Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    .line 209
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->applicationStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

    .line 210
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appSummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->appSummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    .line 211
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appFamilySummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->appFamilySummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    .line 212
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->formStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    .line 213
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->formTemplateStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->formTemplateStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

    .line 217
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->visited:Ljava/util/Set;

    .line 218
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->lock:Ljava/lang/Object;

    .line 229
    const-class v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    invoke-static {v0}, Lcom/google/common/collect/EnumMultiset;->create(Ljava/lang/Class;)Lcom/google/common/collect/EnumMultiset;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->errorCounts:Lcom/google/common/collect/Multiset;

    .line 230
    const-class v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    invoke-static {v0}, Lcom/google/common/collect/EnumMultiset;->create(Ljava/lang/Class;)Lcom/google/common/collect/EnumMultiset;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->ignoreCounts:Lcom/google/common/collect/Multiset;

    .line 392
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 393
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    .line 394
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->priorityBoostMs:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->debugTitle(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAllDispatch(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Z

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncCollectionAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->storeRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p2, "x2"    # Ljava/lang/Object;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncBlob(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/Object;
    .param p5, "x5"    # Z
    .param p6, "x6"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .prologue
    .line 79
    invoke-direct/range {p0 .. p6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncImageOrAttachmentTaskIfPresent(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .param p3, "x3"    # Ljava/lang/Object;
    .param p4, "x4"    # Z
    .param p5, "x5"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .prologue
    .line 79
    invoke-direct/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncImageBlobTaskIfPresent(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/Object;
    .param p4, "x4"    # Z
    .param p5, "x5"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .prologue
    .line 79
    invoke-direct/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncAttachmentBlobTaskIfPresent(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Z

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncPostAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->startTasks(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/sync/Syncer;F)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # F

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->increaseProgressRequired(F)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Z

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAppAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Z

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAppSummaryBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Z

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncSectionAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAttachmentBlob(Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Z

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncFormAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Z

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncFormTemplateBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Z

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAppFamilySummaryAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    .param p3, "x3"    # Ljava/lang/Object;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addAdFormatSettingsTasks(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/MutationStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    return-object v0
.end method

.method static synthetic access$300()Lcom/google/apps/dots/android/newsstand/async/TaskQueue;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->sharedTaskQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->postStore:Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->sectionStore:Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->applicationStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->appSummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->appFamilySummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->formTemplateStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->cacheRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->attachmentStore:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    return-object v0
.end method

.method static synthetic access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->assertEnoughSpaceForEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    return-void
.end method

.method static synthetic access$4100(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressListener:Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->reportedProgress:F

    return v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/sync/Syncer;F)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p1, "x1"    # F

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->reportProgressCompleted(F)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/common/collect/Multiset;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->ignoreCounts:Lcom/google/common/collect/Multiset;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/common/collect/Multiset;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->errorCounts:Lcom/google/common/collect/Multiset;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    return-object v0
.end method

.method private addAdContentTasks(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdContent;Ljava/lang/Object;)V
    .locals 5
    .param p2, "content"    # Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    .param p3, "debugOwner"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;>;",
            "Lcom/google/apps/dots/proto/client/DotsShared$AdContent;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    const/4 v4, 0x0

    .line 842
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdTemplateId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 843
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdTemplateId()Ljava/lang/String;

    move-result-object v0

    .line 844
    .local v0, "adTemplateId":Ljava/lang/String;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Syncing ad template dependency %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 845
    invoke-direct {p0, v0, p3, v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncFormTemplateBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 847
    .end local v0    # "adTemplateId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private addAdFormatSettingsTasks(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;Ljava/lang/Object;)V
    .locals 1
    .param p2, "settings"    # Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    .param p3, "debugOwner"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;>;",
            "Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 833
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->hasGoogleSold()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 834
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getGoogleSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addAdContentTasks(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdContent;Ljava/lang/Object;)V

    .line 836
    :cond_0
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->hasPubSold()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 837
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getPubSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addAdContentTasks(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdContent;Ljava/lang/Object;)V

    .line 839
    :cond_1
    return-void
.end method

.method private addSyncAttachmentBlobTaskIfPresent(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    .locals 1
    .param p2, "attachmentId"    # Ljava/lang/String;
    .param p3, "debugOwner"    # Ljava/lang/Object;
    .param p4, "force"    # Z
    .param p5, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;>;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 808
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    invoke-static {p2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 809
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAttachmentBlob(Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 810
    const/4 v0, 0x1

    .line 812
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private addSyncImageBlobTaskIfPresent(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    .locals 1
    .param p2, "image"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .param p3, "debugOwner"    # Ljava/lang/Object;
    .param p4, "force"    # Z
    .param p5, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;>;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;",
            "Ljava/lang/Object;",
            "Z",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 799
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 800
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncImageBlob(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 801
    const/4 v0, 0x1

    .line 803
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private addSyncImageOrAttachmentTaskIfPresent(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    .locals 6
    .param p2, "image"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .param p3, "fallbackAttachmentId"    # Ljava/lang/String;
    .param p4, "debugOwner"    # Ljava/lang/Object;
    .param p5, "force"    # Z
    .param p6, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;>;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 826
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncImageBlobTaskIfPresent(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    .line 827
    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncAttachmentBlobTaskIfPresent(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private alreadyVisited(Ljava/lang/String;)Z
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 462
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->visited:Ljava/util/Set;

    monitor-enter v1

    .line 463
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->visited:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 464
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static assertEnoughSpaceForEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 12
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/32 v10, 0x100000

    .line 1139
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getFsFreeBytes()J

    move-result-wide v0

    .line 1141
    .local v0, "freeBytes":J
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v4

    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v4, v5, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getMinSyncSpaceMagazinesMb()I

    move-result v4

    .line 1142
    :goto_0
    int-to-long v4, v4

    mul-long v2, v4, v10

    .line 1143
    .local v2, "minFreeBytes":J
    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    .line 1144
    new-instance v4, Lcom/google/apps/dots/android/newsstand/exception/NoSpaceLeftException;

    const-string v5, "Not enough space to sync %s. Needed %d MB, had %d MB."

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    const/4 v7, 0x1

    div-long v8, v2, v10

    .line 1146
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    div-long v8, v0, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    .line 1144
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/apps/dots/android/newsstand/exception/NoSpaceLeftException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1142
    .end local v2    # "minFreeBytes":J
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getMinSyncSpaceNewsMb()I

    move-result v4

    goto :goto_0

    .line 1148
    .restart local v2    # "minFreeBytes":J
    :cond_1
    return-void
.end method

.method private cacheRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .prologue
    .line 877
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->storeRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;ZZ)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    return-object v0
.end method

.method private debugTitle(Ljava/lang/Object;)Ljava/lang/String;
    .locals 4
    .param p1, "debugOwner"    # Ljava/lang/Object;

    .prologue
    .line 1153
    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1154
    const-string v1, ""

    .line 1179
    .end local p1    # "debugOwner":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 1155
    .restart local p1    # "debugOwner":Ljava/lang/Object;
    :cond_0
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-eqz v1, :cond_2

    .line 1156
    const-string v2, "app family: "

    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .end local p1    # "debugOwner":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1157
    .restart local p1    # "debugOwner":Ljava/lang/Object;
    :cond_2
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$Application;

    if-eqz v1, :cond_4

    .line 1158
    const-string v2, "app: "

    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Application;

    .end local p1    # "debugOwner":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1159
    .restart local p1    # "debugOwner":Ljava/lang/Object;
    :cond_4
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v1, :cond_6

    .line 1160
    const-string v2, "app summary: "

    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .end local p1    # "debugOwner":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1161
    .restart local p1    # "debugOwner":Ljava/lang/Object;
    :cond_6
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    if-eqz v1, :cond_8

    .line 1162
    const-string v2, "section: "

    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .end local p1    # "debugOwner":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_7
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1163
    .restart local p1    # "debugOwner":Ljava/lang/Object;
    :cond_8
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-eqz v1, :cond_a

    .line 1164
    const-string v2, "post: "

    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .end local p1    # "debugOwner":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_9
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1165
    .restart local p1    # "debugOwner":Ljava/lang/Object;
    :cond_a
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-eqz v1, :cond_c

    .line 1166
    const-string v2, "post summary: "

    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .end local p1    # "debugOwner":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_b
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1167
    .restart local p1    # "debugOwner":Ljava/lang/Object;
    :cond_c
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    if-eqz v1, :cond_e

    .line 1168
    const-string v2, "form: "

    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .end local p1    # "debugOwner":Ljava/lang/Object;
    iget-object v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_d
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1169
    .restart local p1    # "debugOwner":Ljava/lang/Object;
    :cond_e
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    if-eqz v1, :cond_f

    move-object v0, p1

    .line 1170
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .line 1171
    .local v0, "root":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->hasSelf()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1172
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->getSelf()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getUri()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1174
    .end local v0    # "root":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    :cond_f
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 1175
    check-cast p1, Ljava/lang/String;

    .end local p1    # "debugOwner":Ljava/lang/Object;
    move-object v1, p1

    goto/16 :goto_0

    .line 1176
    .restart local p1    # "debugOwner":Ljava/lang/Object;
    :cond_10
    if-eqz p1, :cond_11

    .line 1177
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1179
    :cond_11
    const-string v1, "unknown"

    goto/16 :goto_0
.end method

.method private static defaultCollections(Landroid/accounts/Account;)Ljava/util/List;
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getLayoutResourcesUrl(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method private immediateSyncerTask(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(TV;)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 513
    .local p1, "result":Ljava/lang/Object;, "TV;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$3;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v3, "immediate"

    const/4 v4, 0x0

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$3;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private increaseProgressRequired(F)V
    .locals 2
    .param p1, "moreRequired"    # F

    .prologue
    .line 1129
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 1130
    :try_start_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressRequired:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressRequired:F

    .line 1131
    monitor-exit v1

    .line 1132
    return-void

    .line 1131
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private reportProgressCompleted(F)V
    .locals 5
    .param p1, "moreCompleted"    # F

    .prologue
    .line 1115
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 1116
    :try_start_0
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressCompleted:F

    add-float/2addr v1, p1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressCompleted:F

    .line 1118
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->reportedProgress:F

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressCompleted:F

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressRequired:F

    div-float/2addr v3, v4

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1119
    .local v0, "newProgress":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->reportedProgress:F

    sub-float v1, v0, v1

    const v3, 0x3c23d70a    # 0.01f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    .line 1120
    :cond_0
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->reportedProgress:F

    .line 1121
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 1122
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 1125
    :cond_1
    monitor-exit v2

    .line 1126
    return-void

    .line 1125
    .end local v0    # "newProgress":F
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private startTasks(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 523
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 524
    .local v0, "futures":Ljava/util/List;, "Ljava/util/List<Lcom/google/common/util/concurrent/ListenableFuture<*>;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    .line 525
    .local v1, "task":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;"
    if-eqz v1, :cond_0

    .line 526
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->start()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 530
    .end local v1    # "task":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;"
    :cond_1
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->whenAllDone(Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/sync/Syncer$4;

    invoke-direct {v3, p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$4;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;)V

    .line 529
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    return-object v2
.end method

.method private storageSpaceCheckTask()Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1088
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$21;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->COLLECTION:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->getAppId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$21;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method private storeRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .prologue
    .line 853
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->storeRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    return-object v0
.end method

.method private storeRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .param p3, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 861
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    iget-boolean v4, v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->anyFreshness:Z

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->storeRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;ZZ)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    return-object v0
.end method

.method private storeRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;ZZ)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .param p3, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p4, "anyFreshness"    # Z
    .param p5, "cacheOnly"    # Z

    .prologue
    .line 866
    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-direct {v1, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    invoke-virtual {v1, p3}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform(Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v2

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    iget-boolean v1, v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->userRequested:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->FOREGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    .line 868
    .local v0, "request":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    if-eqz p4, :cond_2

    .line 869
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->anyVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .line 873
    :cond_0
    :goto_1
    return-object v0

    .line 866
    .end local v0    # "request":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    :cond_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    goto :goto_0

    .line 870
    .restart local v0    # "request":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    :cond_2
    if-eqz p5, :cond_0

    .line 871
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->availableVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    goto :goto_1
.end method

.method public static sync(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "request"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 198
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->start()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private syncAll(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p1, "mutationResponse"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 556
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0
.end method

.method private syncAll(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 779
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v3, "deps"

    move-object v1, p0

    move-object v4, p1

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    return-object v0
.end method

.method private syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Application;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "app"    # Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Application;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 736
    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 737
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->immediateSyncerTask(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    .line 739
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v3, "deps"

    move-object v1, p0

    move-object v4, p1

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsShared$Application;)V

    goto :goto_0
.end method

.method private syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Form;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Form;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 715
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$8;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v3, "deps"

    move-object v1, p0

    move-object v4, p1

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$8;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsShared$Form;)V

    return-object v0
.end method

.method private syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 640
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v3, "deps"

    move-object v1, p0

    move-object v4, p1

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-object v0
.end method

.method private syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Section;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 689
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v3, "deps"

    move-object v1, p0

    move-object v4, p1

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V

    return-object v0
.end method

.method private syncAll(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "collection"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 560
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v3, "deps"

    move-object v1, p0

    move-object v4, p1

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    return-object v0
.end method

.method private syncAllDispatch(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 476
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    if-eqz v0, :cond_0

    .line 477
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    .line 509
    :goto_0
    return-object v0

    .line 478
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-eqz v0, :cond_1

    .line 479
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0

    .line 480
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_1
    instance-of v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    if-eqz v0, :cond_2

    .line 481
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0

    .line 482
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_2
    instance-of v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    if-eqz v0, :cond_3

    .line 483
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Form;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0

    .line 484
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_3
    instance-of v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$Application;

    if-eqz v0, :cond_4

    .line 485
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Application;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Application;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0

    .line 486
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_4
    instance-of v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-eqz v0, :cond_5

    .line 487
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0

    .line 488
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_5
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    if-eqz v0, :cond_6

    .line 489
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    .end local p1    # "object":Ljava/lang/Object;
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->item:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAllDispatch(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0

    .line 490
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_6
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    if-eqz v0, :cond_7

    move-object v4, p1

    .line 491
    check-cast v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    .line 494
    .local v4, "task":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$2;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v3, "unwrap"

    move-object v1, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$2;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;)V

    goto :goto_0

    .line 506
    .end local v4    # "task":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;"
    :cond_7
    if-eqz p1, :cond_8

    .line 507
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Dispatching unexpected type: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 509
    :cond_8
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->immediateSyncerTask(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0
.end method

.method private syncAppAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 4
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 728
    if-nez p3, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 729
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping app all: %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 730
    const/4 v0, 0x0

    .line 732
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAppBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAllDispatch(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0
.end method

.method private syncAppBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 936
    if-nez p3, :cond_0

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 937
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping app: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 938
    const/4 v0, 0x0

    .line 940
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$15;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BLOB:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$15;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncAppFamilySummaryAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 4
    .param p1, "appFamilyId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 771
    if-nez p3, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 772
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping app family all: %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 773
    const/4 v0, 0x0

    .line 775
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAppFamilySummaryBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAllDispatch(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0
.end method

.method private syncAppFamilySummaryBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "appFamilyId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 966
    if-nez p3, :cond_0

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 967
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping app family summary: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 968
    const/4 v0, 0x0

    .line 970
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$17;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BLOB:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$17;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncAppSummaryBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 950
    if-nez p3, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-summary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 951
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping app summary: %s summary"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 952
    const/4 v0, 0x0

    .line 954
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$16;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BLOB:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$16;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncAttachmentBlob(Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 7
    .param p1, "attachmentId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .param p4, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1019
    iget-boolean v0, p4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->isAttachment:Z

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 1020
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 1023
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s-%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-virtual {p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1024
    .local v6, "visitId":Ljava/lang/String;
    if-nez p3, :cond_1

    invoke-direct {p0, v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1025
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Skipping attachment: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v6, v1, v2

    invoke-virtual {v0, v3, v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1026
    const/4 v0, 0x0

    .line 1029
    :goto_1
    return-object v0

    .end local v6    # "visitId":Ljava/lang/String;
    :cond_0
    move v0, v2

    .line 1020
    goto :goto_0

    .line 1029
    .restart local v6    # "visitId":Ljava/lang/String;
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private syncBlob(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "storeRequest"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 898
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$12;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BLOB:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$12;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    return-object v0
.end method

.method private syncCollectionAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 5
    .param p1, "collectionId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 547
    if-nez p3, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 548
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping collection all: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 549
    const/4 v0, 0x0

    .line 552
    :goto_0
    return-object v0

    .line 551
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Syncing collection dependencies %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 552
    invoke-direct {p0, p1, p2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncCollectionBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAllDispatch(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0
.end method

.method private syncCollectionBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "collectionId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 884
    if-nez p3, :cond_0

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 885
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping collection: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 886
    const/4 v0, 0x0

    .line 888
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$11;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->COLLECTION:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$11;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncFormAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 4
    .param p1, "formId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 707
    if-nez p3, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 708
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping section all: %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 709
    const/4 v0, 0x0

    .line 711
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncFormBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAllDispatch(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0
.end method

.method private syncFormBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "formId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 981
    if-nez p3, :cond_0

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 982
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping form: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 983
    const/4 v0, 0x0

    .line 985
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$18;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BLOB:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$18;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncFormTemplateBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "formTemplateId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 995
    if-nez p3, :cond_0

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 996
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping form template: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 997
    const/4 v0, 0x0

    .line 999
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$19;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BLOB:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$19;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncImageBlob(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 1
    .param p1, "image"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .param p4, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;",
            "Ljava/lang/Object;",
            "Z",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1011
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 1012
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAttachmentBlob(Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    return-object v0

    .line 1011
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private syncPostAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 4
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 632
    if-nez p3, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping post all: %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 634
    const/4 v0, 0x0

    .line 636
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncPostBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAllDispatch(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0
.end method

.method private syncPostBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 908
    if-nez p3, :cond_0

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 909
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping post: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 910
    const/4 v0, 0x0

    .line 912
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$13;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BLOB:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$13;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncSectionAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 4
    .param p1, "sectionId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 681
    if-nez p3, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping section all: %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 683
    const/4 v0, 0x0

    .line 685
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncSectionBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAllDispatch(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    goto :goto_0
.end method

.method private syncSectionBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    .locals 6
    .param p1, "sectionId"    # Ljava/lang/String;
    .param p2, "debugOwner"    # Ljava/lang/Object;
    .param p3, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 922
    if-nez p3, :cond_0

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->alreadyVisited(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 923
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping section: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 924
    const/4 v0, 0x0

    .line 926
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$14;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BLOB:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$14;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public notifyOutOfSpace()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 449
    const-string v1, "Syncing %s failed due to lack of storage space."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    aput-object v3, v2, v4

    .line 450
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 451
    .local v0, "message":Ljava/lang/String;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 452
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->OUT_OF_STORAGE:Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    iget-boolean v3, v3, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->userRequested:Z

    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification;->showSyncFailedNotificationIfNecessary(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;Z)V

    .line 454
    return-void
.end method

.method public setProgressListener(Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;

    .prologue
    .line 1101
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressListener:Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;

    .line 1102
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$22;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$22;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressRunnable:Ljava/lang/Runnable;

    .line 1112
    return-void
.end method

.method public start()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 406
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 408
    .local v4, "startTime":J
    const/high16 v3, 0x42c80000    # 100.0f

    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->increaseProgressRequired(F)V

    .line 410
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 413
    .local v2, "futures":Ljava/util/List;, "Ljava/util/List<Lcom/google/common/util/concurrent/ListenableFuture<*>;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->storageSpaceCheckTask()Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->start()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 415
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    .line 416
    invoke-virtual {v3, v6}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;

    move-result-object v1

    .line 417
    .local v1, "collectionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 418
    .local v0, "collectionId":Ljava/lang/String;
    const-string v6, "syncer"

    invoke-direct {p0, v0, v6, v8}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncCollectionAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->start()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 420
    .end local v0    # "collectionId":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->defaultCollections(Landroid/accounts/Account;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 421
    .restart local v0    # "collectionId":Ljava/lang/String;
    const-string v6, "syncer"

    invoke-direct {p0, v0, v6, v8}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncCollectionAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->start()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 424
    .end local v0    # "collectionId":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->whenAllDone(Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    sget-object v7, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    new-array v8, v8, [Ljava/lang/Class;

    invoke-static {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->warnOnError(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/logging/Logd;[Ljava/lang/Class;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->track(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncingFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 426
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncingFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v6, Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;

    invoke-direct {v6, p0, v4, v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$1;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;J)V

    invoke-static {v3, v6}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    return-object v3
.end method
