.class public Lcom/google/apps/dots/android/newsstand/preference/SettingsActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
.source "SettingsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;-><init>(Z)V

    .line 17
    return-void
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 32
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->settings_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsActivity;->setContentView(I)V

    .line 23
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 37
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsActivity;->finish()V

    .line 39
    const/4 v0, 0x1

    .line 41
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
