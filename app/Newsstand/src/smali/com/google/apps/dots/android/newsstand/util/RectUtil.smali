.class public Lcom/google/apps/dots/android/newsstand/util/RectUtil;
.super Lcom/google/android/libraries/bind/util/RectUtil;
.source "RectUtil.java"


# direct methods
.method public static areaOf(Landroid/graphics/RectF;)F
    .locals 2
    .param p0, "r"    # Landroid/graphics/RectF;

    .prologue
    .line 18
    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public static intersectWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 1
    .param p0, "self"    # Landroid/graphics/RectF;
    .param p1, "other"    # Landroid/graphics/RectF;

    .prologue
    .line 12
    invoke-virtual {p1}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 13
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/RectF;->setEmpty()V

    .line 15
    :cond_1
    return-void
.end method

.method public static isEmpty(Landroid/graphics/RectF;)Z
    .locals 2
    .param p0, "f"    # Landroid/graphics/RectF;

    .prologue
    .line 22
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->areaOf(Landroid/graphics/RectF;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static scale(Landroid/graphics/RectF;F)V
    .locals 1
    .param p0, "rect"    # Landroid/graphics/RectF;
    .param p1, "scale"    # F

    .prologue
    .line 29
    iget v0, p0, Landroid/graphics/RectF;->left:F

    mul-float/2addr v0, p1

    iput v0, p0, Landroid/graphics/RectF;->left:F

    .line 30
    iget v0, p0, Landroid/graphics/RectF;->top:F

    mul-float/2addr v0, p1

    iput v0, p0, Landroid/graphics/RectF;->top:F

    .line 31
    iget v0, p0, Landroid/graphics/RectF;->right:F

    mul-float/2addr v0, p1

    iput v0, p0, Landroid/graphics/RectF;->right:F

    .line 32
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v0, p1

    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    .line 33
    return-void
.end method
