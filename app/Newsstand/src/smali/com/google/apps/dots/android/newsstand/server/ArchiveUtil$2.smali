.class final Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$2;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "ArchiveUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->addEditionToArchive(Landroid/app/Activity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$2;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$2;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1, "optionalResult"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x1

    .line 83
    # getter for: Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Successfully archived edition %s from archive"

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$2;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$2;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    .line 87
    .local v0, "removeDownload":Z
    if-eqz v0, :cond_0

    .line 88
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$2;->val$activity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$2;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 89
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->unpinEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v1

    .line 90
    invoke-virtual {v1, v6}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    .line 94
    :cond_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/toast/Toasts;->notifyUserOfArchiveEvent(Z)V

    .line 95
    return-void
.end method
