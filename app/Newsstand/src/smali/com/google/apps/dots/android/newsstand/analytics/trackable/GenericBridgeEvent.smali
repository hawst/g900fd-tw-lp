.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;
.source "GenericBridgeEvent.java"


# instance fields
.field private final action:Ljava/lang/String;

.field private final category:Ljava/lang/String;

.field private final customDimensions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final customMetrics:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final label:Ljava/lang/String;

.field private final page:I

.field private final postId:Ljava/lang/String;

.field private final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "postId"    # Ljava/lang/String;
    .param p4, "page"    # I
    .param p5, "category"    # Ljava/lang/String;
    .param p6, "action"    # Ljava/lang/String;
    .param p7, "label"    # Ljava/lang/String;

    .prologue
    .line 43
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "postId"    # Ljava/lang/String;
    .param p4, "page"    # I
    .param p5, "category"    # Ljava/lang/String;
    .param p6, "action"    # Ljava/lang/String;
    .param p7, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p8, "customDimensions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p9, "customMetrics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 51
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-static {p5}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-static {p6}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 56
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->postId:Ljava/lang/String;

    .line 57
    iput p4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->page:I

    .line 58
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->category:Ljava/lang/String;

    .line 59
    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->action:Ljava/lang/String;

    .line 60
    iput-object p7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->label:Ljava/lang/String;

    .line 61
    iput-object p8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->customDimensions:Ljava/util/Map;

    .line 62
    iput-object p9, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->customMetrics:Ljava/util/Map;

    .line 63
    return-void
.end method

.method private getRelatedArticlesScreen(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "postId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 143
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    invoke-virtual {v1, v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 142
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 144
    .local v0, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    if-nez v0, :cond_1

    .line 145
    new-instance v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v3, "Could not find post for postId = "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {v2, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 147
    :cond_1
    const-string v1, "%s %s - %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "[Related]"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 148
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 147
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 12
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->postId:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->getPostSummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    .line 84
    .local v3, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v2

    .line 86
    .local v2, "originalEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->action:Ljava/lang/String;

    invoke-virtual {p1, v6}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->label:Ljava/lang/String;

    .line 87
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setLabel(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->postId:Ljava/lang/String;

    .line 88
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v6

    .line 89
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v6

    iget-object v7, v3, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    .line 90
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v6

    iget-object v7, v3, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    .line 91
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v6

    .line 92
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v6

    iget-object v7, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v7, v7, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 93
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v6

    .line 94
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppFamilyName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v6

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->page:I

    .line 95
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPage(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 96
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setUserSubscriptionType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v6

    iget-object v7, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 97
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 102
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v6, v6, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    if-eqz v6, :cond_0

    .line 103
    const-string v4, "ReadNow"

    .line 120
    .local v4, "readFromString":Ljava/lang/String;
    :goto_0
    const-string v6, "ReadFrom"

    invoke-static {p1, v6, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v6, "TranslatedLanguage"

    .line 122
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->getTranslatedLanguage()Ljava/lang/String;

    move-result-object v7

    .line 121
    invoke-static {p1, v6, v7}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->customDimensions:Ljava/util/Map;

    if-eqz v6, :cond_7

    .line 125
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->customDimensions:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 126
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {p1, v6, v7}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 104
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "readFromString":Ljava/lang/String;
    :cond_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v6, v6, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    if-eqz v6, :cond_2

    .line 105
    const-string v6, "[Topic] - "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v6, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .line 106
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v7, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "readFromString":Ljava/lang/String;
    :goto_2
    goto :goto_0

    .end local v4    # "readFromString":Ljava/lang/String;
    :cond_1
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 107
    :cond_2
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v6, v6, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v6, :cond_3

    .line 108
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v6, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->getSection(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Section;

    move-result-object v5

    .line 109
    .local v5, "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    const-string v6, "[Section] "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 110
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x3

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 111
    .restart local v4    # "readFromString":Ljava/lang/String;
    goto/16 :goto_0

    .end local v4    # "readFromString":Ljava/lang/String;
    .end local v5    # "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    :cond_3
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v6, v6, Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    if-eqz v6, :cond_4

    .line 112
    const-string v4, "Bookmarks"

    .restart local v4    # "readFromString":Ljava/lang/String;
    goto/16 :goto_0

    .line 113
    .end local v4    # "readFromString":Ljava/lang/String;
    :cond_4
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v6, v6, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    if-eqz v6, :cond_5

    .line 114
    const-string v4, "Search"

    .restart local v4    # "readFromString":Ljava/lang/String;
    goto/16 :goto_0

    .line 115
    .end local v4    # "readFromString":Ljava/lang/String;
    :cond_5
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v6, v6, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    if-eqz v6, :cond_6

    .line 116
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->postId:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->getRelatedArticlesScreen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "readFromString":Ljava/lang/String;
    goto/16 :goto_0

    .line 118
    .end local v4    # "readFromString":Ljava/lang/String;
    :cond_6
    const/4 v4, 0x0

    .restart local v4    # "readFromString":Ljava/lang/String;
    goto/16 :goto_0

    .line 130
    :cond_7
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->customMetrics:Ljava/util/Map;

    if-eqz v6, :cond_9

    .line 131
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->customMetrics:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_8
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 132
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Float;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 133
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-static {p1, v6, v7}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->appendMetric(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/Float;)V

    goto :goto_3

    .line 138
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Float;>;"
    :cond_9
    return-object p1
.end method

.method protected fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "publisherEvent"    # Z

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->category:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setCategory(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    return-object v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->postId:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->getPostSummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    .line 75
    .local v0, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    const-string v1, "[Article] "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 76
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 77
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
