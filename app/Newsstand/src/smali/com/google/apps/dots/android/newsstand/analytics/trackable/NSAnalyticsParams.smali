.class public abstract Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;
.super Ljava/lang/Object;
.source "NSAnalyticsParams.java"


# static fields
.field private static final KEYVALUE_PATTERN:Ljava/util/regex/Pattern;

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final parseFloat:Lcom/google/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 25
    const-string v0, "\\((.+?)\\!(.+?)\\)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->KEYVALUE_PATTERN:Ljava/util/regex/Pattern;

    .line 30
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->parseFloat:Lcom/google/common/base/Function;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public static parseDimensions(Landroid/net/Uri;)Ljava/util/Map;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    const-string v0, "newsstand_analytics_dimensions"

    invoke-static {}, Lcom/google/common/base/Functions;->identity()Lcom/google/common/base/Function;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->parseFromUri(Landroid/net/Uri;Ljava/lang/String;Lcom/google/common/base/Function;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private static parseFromUri(Landroid/net/Uri;Ljava/lang/String;Lcom/google/common/base/Function;)Ljava/util/Map;
    .locals 10
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "paramName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "TT;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .prologue
    .local p2, "valueParser":Lcom/google/common/base/Function;, "Lcom/google/common/base/Function<Ljava/lang/String;TT;>;"
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 52
    sget-object v3, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "parseFromUri"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    .line 55
    .local v2, "returnValue":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;TT;>;"
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 56
    .local v0, "encodedParam":Ljava/lang/String;
    :goto_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "encodedParam: %s"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v0, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    if-eqz v0, :cond_1

    .line 58
    sget-object v3, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->KEYVALUE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 59
    .local v1, "matcher":Ljava/util/regex/Matcher;
    :goto_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 60
    sget-object v3, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "match found; group count: %d"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    invoke-virtual {v1, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v4}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 55
    .end local v0    # "encodedParam":Ljava/lang/String;
    .end local v1    # "matcher":Ljava/util/regex/Matcher;
    :cond_0
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 64
    .restart local v0    # "encodedParam":Ljava/lang/String;
    :cond_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "parsed param %s to %s"

    new-array v5, v9, [Ljava/lang/Object;

    aput-object p1, v5, v7

    aput-object v2, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    return-object v2
.end method

.method public static parseMetrics(Landroid/net/Uri;)Ljava/util/Map;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    const-string v0, "newsstand_analytics_metrics"

    sget-object v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->parseFloat:Lcom/google/common/base/Function;

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->parseFromUri(Landroid/net/Uri;Ljava/lang/String;Lcom/google/common/base/Function;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
