.class final Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2;
.super Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.source "MagazineListUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->filterForAppFamily(Lcom/google/android/libraries/bind/data/DataList;Ljava/lang/String;)Lcom/google/android/libraries/bind/data/DataList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$appFamilyId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Ljava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 58
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2;->val$appFamilyId:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2;->val$appFamilyId:Ljava/lang/String;

    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_FAMILY_ID:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 1
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 73
    return-object p1
.end method
