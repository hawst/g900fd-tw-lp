.class public Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;
.super Ljava/lang/Object;
.source "NSHttpClient.java"

# interfaces
.implements Lorg/apache/http/client/HttpClient;


# static fields
.field private static final ANDROID:Ljava/lang/String; = "ANDROID"

.field private static final ANDROID_PAD:Ljava/lang/String; = "ANDROID_PAD"

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static upgradeRequired:Z


# instance fields
.field private androidHexId:Ljava/lang/String;

.field private final apacheHttpClient:Lorg/apache/http/client/HttpClient;

.field private countryOverride:Ljava/lang/String;

.field private final device:Ljava/lang/String;

.field private final deviceBuildHeader:Ljava/lang/String;

.field private final deviceIdString:Ljava/lang/String;

.field private upgradeVersion:Ljava/lang/String;

.field private final userAgent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lorg/apache/http/client/HttpClient;Ljava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "util"    # Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
    .param p3, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .param p4, "apacheHttpClient"    # Lorg/apache/http/client/HttpClient;
    .param p5, "userAgent"    # Ljava/lang/String;

    .prologue
    const-wide/16 v6, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->apacheHttpClient:Lorg/apache/http/client/HttpClient;

    .line 74
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->userAgent:Ljava/lang/String;

    .line 76
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->getDeviceBuildHeader()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->deviceBuildHeader:Ljava/lang/String;

    .line 77
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/shared/DeviceCategory;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "ANDROID_PAD"

    :goto_0
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->device:Ljava/lang/String;

    .line 78
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceIdString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->deviceIdString:Ljava/lang/String;

    .line 79
    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->upgrade_version:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->upgradeVersion:Ljava/lang/String;

    .line 82
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    .line 83
    .local v0, "androidId":J
    cmp-long v2, v0, v6

    if-eqz v2, :cond_0

    .line 84
    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->androidHexId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    .end local v0    # "androidId":J
    :cond_0
    :goto_1
    invoke-virtual {p3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getCountryOverride()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->countryOverride:Ljava/lang/String;

    .line 91
    return-void

    .line 77
    :cond_1
    const-string v2, "ANDROID"

    goto :goto_0

    .line 86
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private appendCacheBustingQueryParam(Landroid/accounts/Account;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 187
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v0

    .line 189
    .local v0, "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    if-eqz v0, :cond_0

    const-string v1, "v"

    invoke-virtual {p2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 190
    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "v"

    .line 191
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getCacheVersion()Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->getUriVersion()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 192
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p2

    .line 194
    .end local p2    # "uri":Landroid/net/Uri;
    :cond_0
    return-object p2
.end method

.method private static consumeContentQuietly(Lorg/apache/http/HttpResponse;)V
    .locals 1
    .param p0, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 289
    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    :goto_0
    return-void

    .line 290
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static getDeviceBuildHeader()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 220
    const/4 v1, 0x5

    new-array v0, v1, [Lorg/apache/http/HeaderElement;

    new-instance v1, Lorg/apache/http/message/BasicHeaderElement;

    const-string v2, "dev"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeaderElement;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/http/message/BasicHeaderElement;

    const-string v3, "man"

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeaderElement;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lorg/apache/http/message/BasicHeaderElement;

    const-string v3, "mod"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeaderElement;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lorg/apache/http/message/BasicHeaderElement;

    const-string v3, "pro"

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeaderElement;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lorg/apache/http/message/BasicHeaderElement;

    const-string v3, "fin"

    sget-object v4, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeaderElement;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 227
    .local v0, "deviceBuildHeaderElements":[Lorg/apache/http/HeaderElement;
    sget-object v1, Lorg/apache/http/message/BasicHeaderValueFormatter;->DEFAULT:Lorg/apache/http/message/BasicHeaderValueFormatter;

    invoke-static {v0, v5, v1}, Lorg/apache/http/message/BasicHeaderValueFormatter;->formatElements([Lorg/apache/http/HeaderElement;ZLorg/apache/http/message/HeaderValueFormatter;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private logHeaders(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)V
    .locals 11
    .param p1, "request"    # Lorg/apache/http/HttpRequest;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    const/4 v1, 0x0

    .line 204
    sget-object v2, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 206
    const-class v3, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;

    monitor-enter v3

    .line 207
    :try_start_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Request (%s)"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 208
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v0, v4, v2

    .line 209
    .local v0, "header":Lorg/apache/http/Header;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "    %s: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 208
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 211
    .end local v0    # "header":Lorg/apache/http/Header;
    :cond_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Response (%s)"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    array-length v4, v2

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v0, v2, v1

    .line 213
    .restart local v0    # "header":Lorg/apache/http/Header;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "    %s: %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 215
    .end local v0    # "header":Lorg/apache/http/Header;
    :cond_1
    monitor-exit v3

    .line 217
    :cond_2
    return-void

    .line 215
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private throwIfUpgradeRequired()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/exception/UpgradeRequiredException;
        }
    .end annotation

    .prologue
    .line 198
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->upgradeRequired:Z

    if-eqz v0, :cond_0

    .line 199
    new-instance v0, Lcom/google/apps/dots/android/newsstand/exception/UpgradeRequiredException;

    const-string v1, "Upgrade required"

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/exception/UpgradeRequiredException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_0
    return-void
.end method


# virtual methods
.method public execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .locals 1
    .param p1, "host"    # Lorg/apache/http/HttpHost;
    .param p2, "request"    # Lorg/apache/http/HttpRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/HttpHost;",
            "Lorg/apache/http/HttpRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 263
    .local p3, "handler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<+TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;
    .locals 2
    .param p1, "host"    # Lorg/apache/http/HttpHost;
    .param p2, "request"    # Lorg/apache/http/HttpRequest;
    .param p4, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/HttpHost;",
            "Lorg/apache/http/HttpRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;",
            "Lorg/apache/http/protocol/HttpContext;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 269
    .local p3, "handler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<+TT;>;"
    invoke-virtual {p0, p1, p2, p4}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 271
    .local v0, "response":Lorg/apache/http/HttpResponse;
    :try_start_0
    invoke-interface {p3, v0}, Lorg/apache/http/client/ResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 273
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->consumeContentQuietly(Lorg/apache/http/HttpResponse;)V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->consumeContentQuietly(Lorg/apache/http/HttpResponse;)V

    throw v1
.end method

.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .locals 1
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .local p2, "handler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<+TT;>;"
    const/4 v0, 0x0

    .line 251
    invoke-virtual {p0, v0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p3, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;",
            "Lorg/apache/http/protocol/HttpContext;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 257
    .local p2, "handler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<+TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;
    .locals 1
    .param p1, "target"    # Lorg/apache/http/HttpHost;
    .param p2, "request"    # Lorg/apache/http/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 245
    const/4 v0, 0x0

    check-cast v0, Lorg/apache/http/protocol/HttpContext;

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 22
    .param p1, "host"    # Lorg/apache/http/HttpHost;
    .param p2, "request"    # Lorg/apache/http/HttpRequest;
    .param p3, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    const-string v18, "account"

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/accounts/Account;

    .line 98
    .local v4, "account":Landroid/accounts/Account;
    const-string v18, "request"

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 99
    .local v10, "requestContext":Ljava/lang/String;
    const-string v18, "client"

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 101
    .local v7, "isClientRequest":Z
    if-eqz v7, :cond_0

    .line 102
    const-string v18, "User-Agent"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->userAgent:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    const/4 v8, 0x0

    .line 106
    .local v8, "isGoogleBackend":Z
    if-eqz v7, :cond_4

    move-object/from16 v0, p2

    instance-of v0, v0, Lorg/apache/http/client/methods/HttpRequestBase;

    move/from16 v18, v0

    if-eqz v18, :cond_4

    move-object/from16 v9, p2

    .line 107
    check-cast v9, Lorg/apache/http/client/methods/HttpRequestBase;

    .line 108
    .local v9, "requestBase":Lorg/apache/http/client/methods/HttpRequestBase;
    invoke-virtual {v9}, Lorg/apache/http/client/methods/HttpRequestBase;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 110
    .local v17, "uri":Landroid/net/Uri;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v13

    .line 111
    .local v13, "serverUris":Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isGoogleBackend(Landroid/net/Uri;)Z

    move-result v8

    .line 112
    if-eqz v8, :cond_4

    .line 113
    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isGucHost(Landroid/net/Uri;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 115
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    move-result-object v18

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->getAuthToken(Landroid/accounts/Account;Z)Ljava/lang/String;

    move-result-object v5

    .line 116
    .local v5, "authToken":Ljava/lang/String;
    const-string v19, "Authorization"

    const-string v20, "GoogleLogin auth="

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v21

    if-eqz v21, :cond_7

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    :goto_0
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    .end local v5    # "authToken":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isDotsBackend(Landroid/net/Uri;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 121
    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->throwIfUpgradeRequired()V

    .line 122
    const-string v18, "X-Dots-Upgrade-Version"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->upgradeVersion:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v4, v1}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->appendCacheBustingQueryParam(Landroid/accounts/Account;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v17

    .line 126
    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V

    .line 127
    sget-object v18, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v19, "cache-busted: %s"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v17, v20, v21

    invoke-virtual/range {v18 .. v20}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    const-string v18, "X-Dots-Device-Build"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->deviceBuildHeader:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v18, "X-Dots-Device"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->device:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v18, "X-Dots-Device-ID"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->deviceIdString:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v18, "Accept-Language"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lorg/apache/http/HttpRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v18

    if-nez v18, :cond_2

    .line 136
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    .line 137
    .local v6, "countryCode":Ljava/lang/String;
    const-string v18, "Accept-Language"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-interface {v0, v1, v6}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    .end local v6    # "countryCode":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->androidHexId:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 142
    const-string v18, "X-AndroidId"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->androidHexId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->countryOverride:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 147
    const-string v18, "X-CountryOverride"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->countryOverride:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .end local v9    # "requestBase":Lorg/apache/http/client/methods/HttpRequestBase;
    .end local v13    # "serverUris":Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .end local v17    # "uri":Landroid/net/Uri;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->apacheHttpClient:Lorg/apache/http/client/HttpClient;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-interface {v0, v1, v2, v3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v11

    .line 155
    .local v11, "response":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v11}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->logHeaders(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)V

    .line 157
    if-eqz v8, :cond_8

    .line 159
    const-string v18, "X-Dots-Upgrade-Required"

    .line 160
    move-object/from16 v0, v18

    invoke-static {v11, v0}, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->getFirstHeaderValue(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 161
    .local v16, "upgradeRequiredHeader":Ljava/lang/String;
    const-string v18, "true"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 162
    const/16 v18, 0x1

    sput-boolean v18, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->upgradeRequired:Z

    .line 163
    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->throwIfUpgradeRequired()V

    .line 167
    :cond_5
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->date(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;

    move-result-object v12

    .line 168
    .local v12, "serverDate":Ljava/lang/Long;
    if-eqz v12, :cond_6

    .line 169
    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->updateClockSkew(J)V

    .line 173
    :cond_6
    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v15

    .line 174
    .local v15, "statusLine":Lorg/apache/http/StatusLine;
    if-eqz v15, :cond_8

    .line 175
    invoke-interface {v15}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v14

    .line 176
    .local v14, "statusCode":I
    const/16 v18, 0x191

    move/from16 v0, v18

    if-ne v14, v0, :cond_8

    .line 177
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->invalidateToken(Landroid/accounts/Account;)V

    .line 178
    new-instance v18, Lcom/google/apps/dots/android/newsstand/exception/HttpAuthException;

    const-string v19, "invalid token"

    invoke-direct/range {v18 .. v19}, Lcom/google/apps/dots/android/newsstand/exception/HttpAuthException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 116
    .end local v11    # "response":Lorg/apache/http/HttpResponse;
    .end local v12    # "serverDate":Ljava/lang/Long;
    .end local v14    # "statusCode":I
    .end local v15    # "statusLine":Lorg/apache/http/StatusLine;
    .end local v16    # "upgradeRequiredHeader":Ljava/lang/String;
    .restart local v5    # "authToken":Ljava/lang/String;
    .restart local v9    # "requestBase":Lorg/apache/http/client/methods/HttpRequestBase;
    .restart local v13    # "serverUris":Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .restart local v17    # "uri":Landroid/net/Uri;
    :cond_7
    new-instance v18, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 183
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v9    # "requestBase":Lorg/apache/http/client/methods/HttpRequestBase;
    .end local v13    # "serverUris":Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .end local v17    # "uri":Landroid/net/Uri;
    .restart local v11    # "response":Lorg/apache/http/HttpResponse;
    :cond_8
    return-object v11
.end method

.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 2
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 233
    move-object v0, v1

    check-cast v0, Lorg/apache/http/protocol/HttpContext;

    invoke-virtual {p0, v1, p1, v0}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 1
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 239
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->apacheHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    return-object v0
.end method

.method public getParams()Lorg/apache/http/params/HttpParams;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;->apacheHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    return-object v0
.end method
