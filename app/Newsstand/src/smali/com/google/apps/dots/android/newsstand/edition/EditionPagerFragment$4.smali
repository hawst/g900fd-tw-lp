.class Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;
.super Ljava/lang/Object;
.source "EditionPagerFragment.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->onPulledToRefresh()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 221
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 229
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 230
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 224
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 225
    return-void
.end method
