.class public interface abstract Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
.super Ljava/lang/Object;
.source "RenderSource.java"


# virtual methods
.method public abstract getOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
.end method

.method public abstract getPostId()Ljava/lang/String;
.end method

.method public abstract getReadingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
.end method

.method public abstract loadJsonStore()Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lorg/codehaus/jackson/node/ObjectNode;",
            ">;"
        }
    .end annotation
.end method

.method public abstract loadPost()Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            ">;"
        }
    .end annotation
.end method

.method public abstract loadSerializedPostData()Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract loadWebviewBaseHtml()Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
