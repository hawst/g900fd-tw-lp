.class public final Lcom/google/apps/dots/proto/client/DotsShared$Period;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Period"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Period;


# instance fields
.field private bitField0_:I

.field private count_:I

.field private unit_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27419
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Period;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Period;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27420
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 27434
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->unit_:I

    .line 27453
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->count_:I

    .line 27420
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Period;
    .locals 3

    .prologue
    .line 27483
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Period;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 27487
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Period;
    return-object v0

    .line 27484
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Period;
    :catch_0
    move-exception v1

    .line 27485
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 27416
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Period;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Period;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 27492
    if-ne p1, p0, :cond_1

    .line 27495
    :cond_0
    :goto_0
    return v1

    .line 27493
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Period;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 27494
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Period;

    .line 27495
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Period;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->unit_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->unit_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->count_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->count_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 27455
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->count_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 27519
    const/4 v0, 0x0

    .line 27520
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 27521
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->unit_:I

    .line 27522
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27524
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 27525
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->count_:I

    .line 27526
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27528
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->cachedSize:I

    .line 27529
    return v0
.end method

.method public getUnit()I
    .locals 1

    .prologue
    .line 27436
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->unit_:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 27500
    const/16 v0, 0x11

    .line 27501
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 27502
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->unit_:I

    add-int v0, v1, v2

    .line 27503
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->count_:I

    add-int v0, v1, v2

    .line 27504
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Period;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27537
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 27538
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 27542
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 27543
    :sswitch_0
    return-object p0

    .line 27548
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 27549
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 27555
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->unit_:I

    .line 27556
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->bitField0_:I

    goto :goto_0

    .line 27558
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->unit_:I

    goto :goto_0

    .line 27563
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->count_:I

    .line 27564
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->bitField0_:I

    goto :goto_0

    .line 27538
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27416
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Period;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Period;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27509
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 27510
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->unit_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 27512
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 27513
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Period;->count_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 27515
    :cond_1
    return-void
.end method
