.class public final enum Lcom/google/apps/dots/android/newsstand/translation/Translation;
.super Ljava/lang/Enum;
.source "Translation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/translation/Translation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum ALBANIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum ARABIC:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum ARMENIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum AZERBAIJANI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum BELARUSIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum BENGALI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum BULGARIAN_BULGARIA:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field private static final BY_LANGUAGE_CODE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/translation/Translation;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CATALAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum CHINESE_SIMPLIFIED:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum CHINESE_TRADITIONAL:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum CROATIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum CZECH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum DANISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum DUTCH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum ENGLISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum FINNISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum FRENCH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum GERMAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum GREEK:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum HAITIAN_CREOLE:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum HEBREW:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum HINDI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum HUNGARIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum INDONESIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum ITALIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum JAPANESE:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum KOREAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum LAO:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum LATVIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum LITHUANIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum NORWEGIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum PERSIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum POLISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum PORTUGUESE:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum ROMANIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum RUSSIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum SERBIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum SLOVAK:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum SLOVENIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum SPANISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum SWEDISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum TAGALOG:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum THAI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum TURKISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum UKRAINIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum UNDEFINED:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum URDU:Lcom/google/apps/dots/android/newsstand/translation/Translation;

.field public static final enum VIETNAMESE:Lcom/google/apps/dots/android/newsstand/translation/Translation;


# instance fields
.field private final displayNameResource:I

.field private final locale:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 21
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "ARABIC"

    const-string v2, "ar"

    const-string v3, ""

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ARABIC:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 22
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "AZERBAIJANI"

    const-string v2, "az"

    const-string v3, ""

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->AZERBAIJANI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "BELARUSIAN"

    const-string v2, "be"

    const-string v3, ""

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BELARUSIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "BULGARIAN_BULGARIA"

    const-string v2, "bg"

    const-string v3, ""

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BULGARIAN_BULGARIA:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 25
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "BENGALI"

    const-string v2, "bn"

    const-string v3, ""

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BENGALI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 26
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "CATALAN"

    const/4 v2, 0x5

    const-string v3, "ca"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->CATALAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 27
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "CZECH"

    const/4 v2, 0x6

    const-string v3, "cs"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->CZECH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 28
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "DANISH"

    const/4 v2, 0x7

    const-string v3, "da"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->DANISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 29
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "GERMAN"

    const/16 v2, 0x8

    const-string v3, "de"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->GERMAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 30
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "GREEK"

    const/16 v2, 0x9

    const-string v3, "el"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->GREEK:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 31
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "ENGLISH"

    const/16 v2, 0xa

    const-string v3, "en"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ENGLISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 32
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "SPANISH"

    const/16 v2, 0xb

    const-string v3, "es"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->SPANISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 33
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "PERSIAN"

    const/16 v2, 0xc

    const-string v3, "fa"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->PERSIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 34
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "FINNISH"

    const/16 v2, 0xd

    const-string v3, "fi"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->FINNISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 35
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "FRENCH"

    const/16 v2, 0xe

    const-string v3, "fr"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->FRENCH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 36
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "HEBREW"

    const/16 v2, 0xf

    const-string v3, "iw"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->HEBREW:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 37
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "HINDI"

    const/16 v2, 0x10

    const-string v3, "hi"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->HINDI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 38
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "CROATIAN"

    const/16 v2, 0x11

    const-string v3, "hr"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->CROATIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 39
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "HAITIAN_CREOLE"

    const/16 v2, 0x12

    const-string v3, "ht"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->HAITIAN_CREOLE:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 40
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "HUNGARIAN"

    const/16 v2, 0x13

    const-string v3, "hu"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->HUNGARIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 41
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "ARMENIAN"

    const/16 v2, 0x14

    const-string v3, "hy"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ARMENIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 42
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "INDONESIAN"

    const/16 v2, 0x15

    const-string v3, "id"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->INDONESIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 43
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "ITALIAN"

    const/16 v2, 0x16

    const-string v3, "it"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ITALIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 44
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "JAPANESE"

    const/16 v2, 0x17

    const-string v3, "ja"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->JAPANESE:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 45
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "KOREAN"

    const/16 v2, 0x18

    const-string v3, "ko"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->KOREAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 46
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "LAO"

    const/16 v2, 0x19

    const-string v3, "lo"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->LAO:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 47
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "LITHUANIAN"

    const/16 v2, 0x1a

    const-string v3, "lt"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->LITHUANIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 48
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "LATVIAN"

    const/16 v2, 0x1b

    const-string v3, "lv"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->LATVIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 49
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "NORWEGIAN"

    const/16 v2, 0x1c

    const-string v3, "no"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->NORWEGIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 50
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "DUTCH"

    const/16 v2, 0x1d

    const-string v3, "nl"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->DUTCH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 51
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "POLISH"

    const/16 v2, 0x1e

    const-string v3, "pl"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->POLISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 52
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "PORTUGUESE"

    const/16 v2, 0x1f

    const-string v3, "pt"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->PORTUGUESE:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 53
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "ROMANIAN"

    const/16 v2, 0x20

    const-string v3, "ro"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ROMANIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 54
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "RUSSIAN"

    const/16 v2, 0x21

    const-string v3, "ru"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->RUSSIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 55
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "SLOVAK"

    const/16 v2, 0x22

    const-string v3, "sk"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->SLOVAK:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 56
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "SLOVENIAN"

    const/16 v2, 0x23

    const-string v3, "sl"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->SLOVENIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 57
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "ALBANIAN"

    const/16 v2, 0x24

    const-string v3, "sq"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ALBANIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 58
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "SERBIAN"

    const/16 v2, 0x25

    const-string v3, "sr"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->SERBIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 59
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "SWEDISH"

    const/16 v2, 0x26

    const-string v3, "sv"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->SWEDISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 60
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "THAI"

    const/16 v2, 0x27

    const-string v3, "th"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->THAI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 61
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "TAGALOG"

    const/16 v2, 0x28

    const-string v3, "fil"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->TAGALOG:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 62
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "TURKISH"

    const/16 v2, 0x29

    const-string v3, "tr"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->TURKISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 63
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "UKRAINIAN"

    const/16 v2, 0x2a

    const-string v3, "uk"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->UKRAINIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 64
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "URDU"

    const/16 v2, 0x2b

    const-string v3, "ur"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->URDU:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 65
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "VIETNAMESE"

    const/16 v2, 0x2c

    const-string v3, "vi"

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->VIETNAMESE:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 66
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "CHINESE_SIMPLIFIED"

    const/16 v2, 0x2d

    const-string v3, "zh"

    const-string v4, "CN"

    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->language_chinese_simplified:I

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->CHINESE_SIMPLIFIED:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 67
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "CHINESE_TRADITIONAL"

    const/16 v2, 0x2e

    const-string v3, "zh"

    const-string v4, "TW"

    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->language_chinese_traditional:I

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->CHINESE_TRADITIONAL:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 68
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    const-string v1, "UNDEFINED"

    const/16 v2, 0x2f

    const-string v3, "und"

    const-string v4, ""

    const/4 v5, -0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->UNDEFINED:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 19
    const/16 v0, 0x30

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/translation/Translation;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ARABIC:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/translation/Translation;->AZERBAIJANI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BELARUSIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BULGARIAN_BULGARIA:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BENGALI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v1, v0, v10

    const/4 v1, 0x5

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->CATALAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->CZECH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->DANISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->GERMAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->GREEK:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ENGLISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->SPANISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->PERSIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->FINNISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->FRENCH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->HEBREW:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->HINDI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->CROATIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->HAITIAN_CREOLE:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->HUNGARIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ARMENIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->INDONESIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ITALIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->JAPANESE:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->KOREAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->LAO:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->LITHUANIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->LATVIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->NORWEGIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->DUTCH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->POLISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->PORTUGUESE:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ROMANIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->RUSSIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->SLOVAK:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->SLOVENIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ALBANIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->SERBIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->SWEDISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->THAI:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->TAGALOG:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->TURKISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->UKRAINIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->URDU:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->VIETNAMESE:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->CHINESE_SIMPLIFIED:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->CHINESE_TRADITIONAL:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->UNDEFINED:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->$VALUES:[Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 73
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BY_LANGUAGE_CODE:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p3, "language"    # Ljava/lang/String;
    .param p4, "country"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    const/4 v5, -0x1

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IZ)V

    .line 77
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p3, "language"    # Ljava/lang/String;
    .param p4, "country"    # Ljava/lang/String;
    .param p5, "displayNameResource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 80
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/translation/Translation;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IZ)V

    .line 81
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IZ)V
    .locals 1
    .param p3, "language"    # Ljava/lang/String;
    .param p4, "country"    # Ljava/lang/String;
    .param p5, "displayNameResource"    # I
    .param p6, "visible"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 84
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p3, p4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->locale:Ljava/util/Locale;

    .line 85
    iput p5, p0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->displayNameResource:I

    .line 86
    return-void
.end method

.method public static fromLanguageCode(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/translation/Translation;
    .locals 13
    .param p0, "languageCode"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 105
    sget-object v3, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BY_LANGUAGE_CODE:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 106
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/translation/Translation;->values()[Lcom/google/apps/dots/android/newsstand/translation/Translation;

    move-result-object v5

    array-length v6, v5

    move v3, v4

    :goto_0
    if-ge v3, v6, :cond_1

    aget-object v2, v5, v3

    .line 107
    .local v2, "translation":Lcom/google/apps/dots/android/newsstand/translation/Translation;
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/translation/Translation;->getLocale()Ljava/util/Locale;

    move-result-object v0

    .line 108
    .local v0, "locale":Ljava/util/Locale;
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 109
    sget-object v7, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BY_LANGUAGE_CODE:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/translation/Translation;->toLanguageCode()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    :goto_1
    sget-object v7, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BY_LANGUAGE_CODE:Ljava/util/Map;

    const-string v8, "id"

    sget-object v9, Lcom/google/apps/dots/android/newsstand/translation/Translation;->INDONESIAN:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v7, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BY_LANGUAGE_CODE:Ljava/util/Map;

    const-string v8, "he"

    sget-object v9, Lcom/google/apps/dots/android/newsstand/translation/Translation;->HEBREW:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 112
    :cond_0
    sget-object v7, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BY_LANGUAGE_CODE:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "-"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v7, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BY_LANGUAGE_CODE:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "_"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 120
    .end local v0    # "locale":Ljava/util/Locale;
    .end local v2    # "translation":Lcom/google/apps/dots/android/newsstand/translation/Translation;
    :cond_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BY_LANGUAGE_CODE:Ljava/util/Map;

    invoke-interface {v3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 121
    .restart local v2    # "translation":Lcom/google/apps/dots/android/newsstand/translation/Translation;
    if-nez v2, :cond_2

    .line 122
    const-string v3, "[-_]"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 123
    .local v1, "tokens":[Ljava/lang/String;
    array-length v3, v1

    if-lez v3, :cond_2

    .line 124
    sget-object v3, Lcom/google/apps/dots/android/newsstand/translation/Translation;->BY_LANGUAGE_CODE:Ljava/util/Map;

    aget-object v4, v1, v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "translation":Lcom/google/apps/dots/android/newsstand/translation/Translation;
    check-cast v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 127
    .end local v1    # "tokens":[Ljava/lang/String;
    .restart local v2    # "translation":Lcom/google/apps/dots/android/newsstand/translation/Translation;
    :cond_2
    if-nez v2, :cond_3

    .line 128
    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->UNDEFINED:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 130
    :cond_3
    return-object v2
.end method

.method public static getDefault()Lcom/google/apps/dots/android/newsstand/translation/Translation;
    .locals 3

    .prologue
    .line 134
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 135
    .local v0, "defaultLocale":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/translation/Translation;->fromLanguageCode(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/translation/Translation;

    move-result-object v1

    .line 136
    .local v1, "translation":Lcom/google/apps/dots/android/newsstand/translation/Translation;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->UNDEFINED:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    if-ne v1, v2, :cond_0

    .line 137
    sget-object v1, Lcom/google/apps/dots/android/newsstand/translation/Translation;->ENGLISH:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 139
    :cond_0
    return-object v1
.end method

.method public static getPreferred()Lcom/google/apps/dots/android/newsstand/translation/Translation;
    .locals 3

    .prologue
    .line 143
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    .line 144
    .local v1, "prefs":Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getPreferredLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/translation/Translation;->fromLanguageCode(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/translation/Translation;

    move-result-object v0

    .line 145
    .local v0, "fromPrefs":Lcom/google/apps/dots/android/newsstand/translation/Translation;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/translation/Translation;->UNDEFINED:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    if-ne v0, v2, :cond_0

    .line 146
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/translation/Translation;->getDefault()Lcom/google/apps/dots/android/newsstand/translation/Translation;

    move-result-object v0

    .line 148
    .end local v0    # "fromPrefs":Lcom/google/apps/dots/android/newsstand/translation/Translation;
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/translation/Translation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/translation/Translation;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->$VALUES:[Lcom/google/apps/dots/android/newsstand/translation/Translation;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/translation/Translation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/translation/Translation;

    return-object v0
.end method


# virtual methods
.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public toLanguageCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
