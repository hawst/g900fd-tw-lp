.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$17;
.super Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAppFamilySummaryBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

.field final synthetic val$appFamilyId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p2, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .param p3, "debugId"    # Ljava/lang/String;
    .param p4, "debugOwner"    # Ljava/lang/Object;

    .prologue
    .line 970
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$17;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$17;->val$appFamilyId:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public callInternal()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 973
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: syncing app family summary"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 974
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$17;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->appFamilySummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$3500(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$17;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2800(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$17;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$17;->val$appFamilyId:Ljava/lang/String;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->APPLICATION_FAMILY_SUMMARY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 975
    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->storeRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-static {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1200(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v2

    .line 974
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;->getCacheItem(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
