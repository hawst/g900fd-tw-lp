.class public final Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AnnouncementSummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;


# instance fields
.field private actionTitle_:Ljava/lang/String;

.field private action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

.field private announcementId_:Ljava/lang/String;

.field private bitField0_:I

.field private description_:Ljava/lang/String;

.field private imageAttachmentId_:Ljava/lang/String;

.field private title_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29739
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29740
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 29745
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->announcementId_:Ljava/lang/String;

    .line 29767
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->title_:Ljava/lang/String;

    .line 29789
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->description_:Ljava/lang/String;

    .line 29811
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->actionTitle_:Ljava/lang/String;

    .line 29852
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->imageAttachmentId_:Ljava/lang/String;

    .line 29740
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;
    .locals 3

    .prologue
    .line 29889
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29893
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    if-eqz v2, :cond_0

    .line 29894
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Action;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Action;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    .line 29896
    :cond_0
    return-object v0

    .line 29890
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;
    :catch_0
    move-exception v1

    .line 29891
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 29736
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 29901
    if-ne p1, p0, :cond_1

    .line 29909
    :cond_0
    :goto_0
    return v1

    .line 29902
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 29903
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    .line 29904
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->announcementId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->announcementId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 29905
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->description_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->description_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 29906
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->actionTitle_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->actionTitle_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 29907
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    if-nez v3, :cond_3

    .line 29908
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->imageAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->imageAttachmentId_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 29909
    goto :goto_0

    .line 29904
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->announcementId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->announcementId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->title_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->title_:Ljava/lang/String;

    .line 29905
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->description_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->description_:Ljava/lang/String;

    .line 29906
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->actionTitle_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->actionTitle_:Ljava/lang/String;

    .line 29907
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    .line 29908
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->imageAttachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->imageAttachmentId_:Ljava/lang/String;

    .line 29909
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 29948
    const/4 v0, 0x0

    .line 29949
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 29950
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->announcementId_:Ljava/lang/String;

    .line 29951
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29953
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 29954
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->title_:Ljava/lang/String;

    .line 29955
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29957
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 29958
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->description_:Ljava/lang/String;

    .line 29959
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29961
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 29962
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->actionTitle_:Ljava/lang/String;

    .line 29963
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29965
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    if-eqz v1, :cond_4

    .line 29966
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    .line 29967
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29969
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 29970
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->imageAttachmentId_:Ljava/lang/String;

    .line 29971
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29973
    :cond_5
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->cachedSize:I

    .line 29974
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 29913
    const/16 v0, 0x11

    .line 29914
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 29915
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->announcementId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 29916
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->title_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 29917
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->description_:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 29918
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->actionTitle_:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 29919
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 29920
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->imageAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_5

    :goto_5
    add-int v0, v1, v2

    .line 29921
    return v0

    .line 29915
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->announcementId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 29916
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->title_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 29917
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->description_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 29918
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->actionTitle_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 29919
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Action;->hashCode()I

    move-result v1

    goto :goto_4

    .line 29920
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->imageAttachmentId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29982
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 29983
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 29987
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 29988
    :sswitch_0
    return-object p0

    .line 29993
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->announcementId_:Ljava/lang/String;

    .line 29994
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    goto :goto_0

    .line 29998
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->title_:Ljava/lang/String;

    .line 29999
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    goto :goto_0

    .line 30003
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->description_:Ljava/lang/String;

    .line 30004
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    goto :goto_0

    .line 30008
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->actionTitle_:Ljava/lang/String;

    .line 30009
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    goto :goto_0

    .line 30013
    :sswitch_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    if-nez v1, :cond_1

    .line 30014
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Action;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Action;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    .line 30016
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 30020
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->imageAttachmentId_:Ljava/lang/String;

    .line 30021
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    goto :goto_0

    .line 29983
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29736
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29926
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 29927
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->announcementId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 29929
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 29930
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->title_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 29932
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 29933
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->description_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 29935
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 29936
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->actionTitle_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 29938
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    if-eqz v0, :cond_4

    .line 29939
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->action_:Lcom/google/apps/dots/proto/client/DotsShared$Action;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 29941
    :cond_4
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 29942
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->imageAttachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 29944
    :cond_5
    return-void
.end method
