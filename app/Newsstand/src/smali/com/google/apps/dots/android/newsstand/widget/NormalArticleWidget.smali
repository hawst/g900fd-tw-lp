.class public abstract Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;
.super Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;
.source "NormalArticleWidget.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/DataView;


# static fields
.field public static final DK_ARTICLE_LOADER:I

.field public static final DK_ARTICLE_VERSION:I

.field public static final DK_IS_METERED:I

.field public static final EQUALITY_FIELDS:[I

.field private static final JS_ERROR_PATTERN:Ljava/util/regex/Pattern;

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

.field private articleVersion:J

.field private audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

.field protected dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

.field private fontSizePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

.field private final jsErrorMatcher:Ljava/util/regex/Matcher;

.field protected userRoles:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 59
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 61
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->NormalArticleWidget_articleLoader:I

    sput v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_LOADER:I

    .line 62
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->NormalArticleWidget_articleVersion:I

    sput v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_VERSION:I

    .line 63
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->NormalArticleWidget_isMetered:I

    sput v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_IS_METERED:I

    .line 65
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_VERSION:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_IS_METERED:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->EQUALITY_FIELDS:[I

    .line 79
    const-string v0, ".*(Syntax|Type|Reference)Error.*"

    .line 80
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->JS_ERROR_PATTERN:Ljava/util/regex/Pattern;

    .line 79
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V
    .locals 7
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
    .param p3, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p4, "postIndex"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 86
    const-string v2, ""

    move-object v0, p0

    move-object v1, p1

    move v3, p4

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;ILcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V

    .line 71
    new-instance v0, Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;-><init>(Lcom/google/android/libraries/bind/data/DataView;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    .line 75
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleVersion:J

    .line 81
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->JS_ERROR_PATTERN:Ljava/util/regex/Pattern;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->jsErrorMatcher:Ljava/util/regex/Matcher;

    .line 87
    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->clearDataOnDetach(Z)V

    .line 89
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    .line 110
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->setupWebView(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    return-void
.end method

.method private setupWebView(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 3
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 360
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    sget v2, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_LOADER:I

    .line 361
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/DataViewHelper;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->get()Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v0

    .line 362
    .local v0, "articleLoader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    if-nez v0, :cond_0

    .line 367
    :goto_0
    return-void

    .line 366
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->setupWebView(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    goto :goto_0
.end method

.method private setupWebView(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 2
    .param p1, "articleLoader"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->whenLayoutCompletes:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    invoke-virtual {p2, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 398
    return-void
.end method


# virtual methods
.method protected articleWidgetBridge()Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

    return-object v0
.end method

.method public clearDataOnDetach(Z)V
    .locals 1
    .param p1, "clearDataOnDetach"    # Z

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataViewHelper;->clearDataOnDetach(Z)Z

    .line 293
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 219
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->unregister(Landroid/content/Context;)V

    .line 224
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    .line 226
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    .line 227
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    .line 228
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->userRoles:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .line 229
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->destroy()V

    goto :goto_0
.end method

.method public getData()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public getDataRow()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public loadDelayedContents(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "optLoadedRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->isNotLoadedOrFailed()Z

    move-result v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 154
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadedRunnable(Ljava/lang/Runnable;)V

    .line 155
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->loadIfPossible()V

    .line 156
    return-void
.end method

.method protected loadIfPossible()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 298
    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    .line 299
    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 300
    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 301
    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->form:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .line 302
    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleTemplate:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 303
    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->idToAdTemplate:Ljava/util/Map;

    .line 305
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADING:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v2, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;ZLjava/lang/Throwable;)V

    .line 306
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    sget v3, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_LOADER:I

    .line 307
    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/DataViewHelper;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->get()Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v0

    .line 308
    .local v0, "articleLoader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    if-nez v0, :cond_0

    .line 357
    :goto_0
    return-void

    .line 313
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    .line 320
    .local v1, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->setupWebView(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 323
    const/16 v2, 0x8

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    .line 325
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getUserRolesFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    .line 326
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getApplicationFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 327
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getSectionFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 328
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    .line 329
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getFormFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    .line 330
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    .line 332
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getIdToAdTemplateMapFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 333
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v5

    .line 331
    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 334
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getUseLegacyLayoutFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    aput-object v4, v2, v3

    .line 324
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$2;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;)V

    .line 323
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method protected notifyArticleOverscrolled(Z)V
    .locals 1
    .param p1, "top"    # Z

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    invoke-interface {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticleOverscrolled(Landroid/view/View;Z)V

    .line 132
    :cond_0
    return-void
.end method

.method protected notifyArticlePageChanged(Z)V
    .locals 3
    .param p1, "userDriven"    # Z

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    invoke-interface {v0, p0, v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticlePageChanged(Landroid/view/View;IIZ)V

    .line 139
    :cond_0
    return-void
.end method

.method protected notifyArticleScrolled(ZI)V
    .locals 6
    .param p1, "userDriven"    # Z
    .param p2, "scrollDelta"    # I

    .prologue
    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_1

    .line 119
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->getScrollY()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 120
    .local v2, "scrollOffset":I
    if-nez v2, :cond_0

    .line 121
    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    .line 124
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->computeVerticalScrollRange()I

    move-result v3

    move-object v1, p0

    move v4, p2

    move v5, p1

    .line 123
    invoke-interface/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticleScrolled(Landroid/view/View;IIIZ)V

    .line 126
    .end local v2    # "scrollOffset":I
    :cond_1
    return-void
.end method

.method public notifyArticleUnhandledClick()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    invoke-interface {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticleUnhandledClick(Landroid/view/View;)V

    .line 145
    :cond_0
    return-void
.end method

.method protected notifyOnLoadComplete()V
    .locals 2

    .prologue
    .line 211
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->notifyOnLoadComplete()V

    .line 212
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAudioItemCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->register(Landroid/content/Context;)V

    .line 215
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 5

    .prologue
    .line 402
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->onAttachedToWindow()V

    .line 403
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onAttachedToWindow()V

    .line 404
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    :goto_0
    return-void

    .line 408
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "articleFontSizev2"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->fontSizePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    goto :goto_0
.end method

.method protected onAudioStateChanged(Lcom/google/apps/dots/android/newsstand/media/AudioItem;I)V
    .locals 9
    .param p1, "currentItem"    # Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    .param p2, "status"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 197
    const/4 v1, 0x0

    .line 199
    .local v1, "jsonAudioItem":Lorg/codehaus/jackson/node/ObjectNode;
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->postId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 200
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-static {v3, p1}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->findValueFromMediaItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/android/newsstand/media/MediaItem;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    .line 201
    .local v0, "audioValue":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getAudio()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getOriginalUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->toJson(Ljava/lang/String;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v1

    .line 203
    .end local v0    # "audioValue":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_0
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v4, "dots.smart.updateCurrentAudio(%s, %d);"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    if-nez v1, :cond_1

    const-string v1, "undefined"

    .end local v1    # "jsonAudioItem":Lorg/codehaus/jackson/node/ObjectNode;
    :cond_1
    aput-object v1, v5, v7

    .line 205
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    .line 203
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 206
    .local v2, "updateAudioScript":Ljava/lang/String;
    new-array v3, v8, [Ljava/lang/String;

    aput-object v2, v3, v7

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->executeStatements([Ljava/lang/String;)V

    .line 207
    return-void
.end method

.method public onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 4
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 250
    if-eqz p1, :cond_1

    sget v2, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_VERSION:I

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v2

    if-eqz v2, :cond_1

    sget v2, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_VERSION:I

    .line 251
    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsLong(I)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 253
    .local v0, "newArticleVersion":J
    :goto_0
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleVersion:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 254
    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleVersion:J

    .line 259
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-eq v2, v3, :cond_0

    .line 261
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->loadIfPossible()V

    .line 264
    :cond_0
    return-void

    .line 251
    .end local v0    # "newArticleVersion":J
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 431
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->onDetachedFromWindow()V

    .line 432
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onDetachedFromWindow()V

    .line 433
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->fontSizePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->fontSizePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 435
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->fontSizePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 437
    :cond_0
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 447
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->onFinishTemporaryDetach()V

    .line 448
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onFinishTemporaryDetach()V

    .line 449
    return-void
.end method

.method protected onJsError(Landroid/webkit/ConsoleMessage;)V
    .locals 8
    .param p1, "consoleMessage"    # Landroid/webkit/ConsoleMessage;

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 268
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->onJsError(Landroid/webkit/ConsoleMessage;)V

    .line 269
    const/4 v1, 0x0

    .line 271
    .local v1, "t":Ljava/lang/Throwable;
    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->message()Ljava/lang/String;

    move-result-object v0

    .line 274
    .local v0, "messageText":Ljava/lang/String;
    const-string v2, "dots is not defined"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 275
    new-instance v1, Ljava/net/SocketException;

    .end local v1    # "t":Ljava/lang/Throwable;
    const-string v2, "Failed to load layout engine."

    invoke-direct {v1, v2}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    .line 281
    .restart local v1    # "t":Ljava/lang/Throwable;
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 282
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "JS error when displaying post %s: %s"

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-nez v2, :cond_3

    const/4 v2, 0x0

    :goto_1
    aput-object v2, v5, v6

    aput-object v1, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 284
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->canFail()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 285
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOAD_FAILED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v2, v3, v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;Ljava/lang/Throwable;)V

    .line 288
    :cond_1
    return-void

    .line 276
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->jsErrorMatcher:Ljava/util/regex/Matcher;

    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->reset(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 277
    new-instance v1, Lcom/google/apps/dots/android/newsstand/exception/LayoutEngineException;

    .end local v1    # "t":Ljava/lang/Throwable;
    const-string v2, "%s, line %d: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->sourceId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 278
    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->lineNumber()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    aput-object v0, v3, v5

    .line 277
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/exception/LayoutEngineException;-><init>(Ljava/lang/String;)V

    .restart local v1    # "t":Ljava/lang/Throwable;
    goto :goto_0

    .line 282
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onMeasure(II)V

    .line 455
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->onMeasure(II)V

    .line 456
    return-void
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 1
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    .line 166
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->onOverScrolled(IIZZ)V

    .line 167
    if-eqz p4, :cond_0

    .line 168
    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->notifyArticleOverscrolled(Z)V

    .line 170
    :cond_0
    return-void

    .line 168
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onScrollChanged(IIII)V
    .locals 2
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 160
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->onScrollChanged(IIII)V

    .line 161
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->userDrivenScroll:Z

    sub-int v1, p2, p4

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->notifyArticleScrolled(ZI)V

    .line 162
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 441
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->onStartTemporaryDetach()V

    .line 442
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onStartTemporaryDetach()V

    .line 443
    return-void
.end method

.method protected populateJsonStore()V
    .locals 4

    .prologue
    .line 180
    const-string v0, "adBlockData"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->getAdBlockData(Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Section;Lcom/google/apps/dots/proto/client/DotsShared$Post;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->putIntoJsonStore(Ljava/lang/String;Lorg/codehaus/jackson/node/ObjectNode;)V

    .line 181
    const-string v0, "sectionHeaderTemplate"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 182
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getDisplayOptions()Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->getHeaderTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->getMainTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getTemplate()Ljava/lang/String;

    move-result-object v1

    .line 181
    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->userRoles:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    if-eqz v0, :cond_0

    .line 184
    const-string v0, "userRoles"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->userRoles:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->getRolesBlockData(Lcom/google/apps/dots/proto/client/DotsShared$RoleList;)Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->putIntoJsonStore(Ljava/lang/String;Lorg/codehaus/jackson/node/ArrayNode;)V

    .line 186
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->populateJsonStore()V

    .line 187
    return-void
.end method

.method public setArticleEventHandler(Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;)V
    .locals 0
    .param p1, "articleEventHandler"    # Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    .line 114
    return-void
.end method

.method public setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataViewHelper;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 235
    return-void
.end method

.method protected setTemplateProperties()V
    .locals 3

    .prologue
    .line 191
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getName()Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "sectionName":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->setTemplateProperties(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->setTemplateProperties()V

    .line 194
    return-void
.end method

.method protected updateCurrentPage(IIZ)V
    .locals 0
    .param p1, "page"    # I
    .param p2, "pageCount"    # I
    .param p3, "userDriven"    # Z

    .prologue
    .line 174
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->updateCurrentPage(IIZ)V

    .line 175
    invoke-virtual {p0, p3}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->notifyArticlePageChanged(Z)V

    .line 176
    return-void
.end method
