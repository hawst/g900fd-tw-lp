.class final Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo$1;
.super Ljava/lang/Object;
.source "EditionSubscriptionInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 67
    const-class v3, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 68
    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 69
    .local v1, "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    invoke-static {p1}, Lcom/google/android/play/onboard/OnboardUtils;->readBoolean(Landroid/os/Parcel;)Z

    move-result v0

    .line 70
    .local v0, "defaultSubscribed":Z
    invoke-static {p1}, Lcom/google/android/play/onboard/OnboardUtils;->readBoolean(Landroid/os/Parcel;)Z

    move-result v2

    .line 71
    .local v2, "subscribe":Z
    new-instance v3, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;

    invoke-direct {v3, v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V

    return-object v3
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 76
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;

    move-result-object v0

    return-object v0
.end method
