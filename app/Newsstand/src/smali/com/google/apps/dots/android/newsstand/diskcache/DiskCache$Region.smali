.class Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
.super Ljava/lang/Object;
.source "DiskCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Region"
.end annotation


# instance fields
.field final regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

.field final storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;)V
    .locals 0
    .param p1, "storeEntry"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;
    .param p2, "regionLock"    # Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    .line 136
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    .line 137
    return-void
.end method


# virtual methods
.method public lb()Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->lb:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    return-object v0
.end method

.method public storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->storeFile:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 149
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "storeEntry"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    .line 150
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "regionLock"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    .line 151
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
