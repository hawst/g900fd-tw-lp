.class Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$3;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "HeaderEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pageList()Lcom/google/android/libraries/bind/data/DataList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;
    .param p2, "x0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 3
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 278
    sget v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->DK_PAGER_TITLE:I

    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_NAME:I

    .line 279
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 278
    invoke-virtual {p1, v1, v0}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 280
    const/4 v0, 0x1

    return v0
.end method
