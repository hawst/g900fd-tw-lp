.class final Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil$1;
.super Ljava/lang/Object;
.source "GservicesUtil.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil;->getBoolean(Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$defValue:Z

.field final synthetic val$key:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil$1;->val$key:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil$1;->val$defValue:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Boolean;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    # invokes: Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil;->contentResolver()Landroid/content/ContentResolver;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil;->access$000()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil$1;->val$key:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil$1;->val$defValue:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil$1;->call()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
