.class public Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;
.super Ljava/util/concurrent/FutureTask;
.source "NSListenableFutureTask.java"

# interfaces
.implements Lcom/google/common/util/concurrent/ListenableFuture;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/FutureTask",
        "<TV;>;",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/bind/collections/RingBuffer",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask<TV;>;"
    .local p1, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TV;>;"
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 47
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->create(I)Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 27
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 28
    return-void
.end method

.method public static create(Ljava/util/concurrent/Callable;)Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)",
            "Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 17
    .local p0, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TV;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method

.method private executionListAdd(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 3
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 53
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask<TV;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 55
    .local v0, "queue":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<Ljava/lang/Runnable;>;"
    if-eqz v0, :cond_1

    .line 56
    monitor-enter v0

    .line 57
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->runningOn(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/collections/RingBuffer;->addLast(Ljava/lang/Object;)V

    .line 59
    monitor-exit v0

    .line 69
    :goto_0
    return-void

    .line 61
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    :cond_1
    invoke-interface {p2, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private executionListExecute()V
    .locals 2

    .prologue
    .line 74
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask<TV;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 75
    .local v0, "queue":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<Ljava/lang/Runnable;>;"
    if-eqz v0, :cond_0

    .line 76
    monitor-enter v0

    .line 77
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    if-nez v1, :cond_1

    .line 78
    monitor-exit v0

    .line 89
    :cond_0
    return-void

    .line 80
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 81
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "listener"    # Ljava/lang/Runnable;
    .param p2, "exec"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 37
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask<TV;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->executionListAdd(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 38
    return-void
.end method

.method protected done()V
    .locals 0

    .prologue
    .line 42
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask<TV;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->executionListExecute()V

    .line 43
    return-void
.end method
