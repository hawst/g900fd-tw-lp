.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;
.super Ljava/lang/Object;
.source "MagazineListUtil.java"


# static fields
.field public static final DK_END_MONTH:I

.field public static final DK_MONTH:I

.field public static final DK_NUM_ISSUES:I

.field public static final DK_YEAR:I

.field public static final DK_YEAR_MONTH_KEY:I

.field private static final MAGAZINE_SORT_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MagazineListUtil_year:I

    sput v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_YEAR:I

    .line 27
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MagazineListUtil_month:I

    sput v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_MONTH:I

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MagazineListUtil_endMonth:I

    sput v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_END_MONTH:I

    .line 32
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MagazineListUtil_yearMonthKey:I

    sput v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_YEAR_MONTH_KEY:I

    .line 33
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MagazineListUtil_numIssues:I

    sput v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_NUM_ISSUES:I

    .line 37
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->MAGAZINE_SORT_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method public static filterForAppFamily(Lcom/google/android/libraries/bind/data/DataList;Ljava/lang/String;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 2
    .param p0, "subscriptionList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p1, "appFamilyId"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2;-><init>(Ljava/util/concurrent/Executor;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public static filterForMonth(Lcom/google/android/libraries/bind/data/DataList;III)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "subscriptionList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "endMonth"    # I

    .prologue
    .line 94
    invoke-static {p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->forMonthFilter(III)Lcom/google/android/libraries/bind/data/Filter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method private static forMonthFilter(III)Lcom/google/android/libraries/bind/data/Filter;
    .locals 6
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "endMonth"    # I

    .prologue
    .line 105
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 106
    .local v2, "calendar":Ljava/util/Calendar;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    move v3, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$3;-><init>(Ljava/util/concurrent/Executor;Ljava/util/Calendar;III)V

    return-object v0
.end method

.method public static getMagazineSortComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->MAGAZINE_SORT_COMPARATOR:Ljava/util/Comparator;

    return-object v0
.end method

.method public static getMonth(Ljava/util/Calendar;Lcom/google/android/libraries/bind/data/Data;)I
    .locals 3
    .param p0, "calendar"    # Ljava/util/Calendar;
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 136
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_PUBLICATION_DATE:I

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsLong(I)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 137
    .local v0, "millis":J
    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 138
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    return v2
.end method

.method public static getYear(Ljava/util/Calendar;Lcom/google/android/libraries/bind/data/Data;)I
    .locals 4
    .param p0, "calendar"    # Ljava/util/Calendar;
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 124
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_PUBLICATION_DATE:I

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsLong(I)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 125
    .local v0, "millis":J
    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 126
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 129
    .local v2, "year":I
    return v2
.end method
