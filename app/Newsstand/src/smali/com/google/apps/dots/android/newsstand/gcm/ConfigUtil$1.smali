.class Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;
.super Ljava/lang/Object;
.source "ConfigUtil.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->updateGCMAndGetFreshConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$background:Z

.field final synthetic val$gcmRegId:Ljava/lang/String;

.field final synthetic val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->val$gcmRegId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->val$account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-boolean p5, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->val$background:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 191
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->apply(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public apply(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "newGcmRegId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 195
    if-eqz p1, :cond_1

    .line 196
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->val$gcmRegId:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    # getter for: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$000(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->val$account:Landroid/accounts/Account;

    const-string v2, "gcmRegisteredForUser2"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    # getter for: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$000(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    const-string v1, "gcmRegistrationId"

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    # getter for: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$000(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    const-string v1, "gcmRegIdAppVersion"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    # getter for: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->appVersion:I
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$100(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setInt(Ljava/lang/String;I)V

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;->val$background:Z

    # invokes: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->requestConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$200(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
