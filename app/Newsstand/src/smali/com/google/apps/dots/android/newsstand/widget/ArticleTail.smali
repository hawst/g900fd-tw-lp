.class public Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;
.super Landroid/widget/FrameLayout;
.source "ArticleTail.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final adapter:Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;

.field private final cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

.field private parent:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

.field private final setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private final staticListView:Lcom/google/apps/dots/android/newsstand/widget/StaticListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x1

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 55
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 56
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->article_tail:I

    invoke-virtual {v0, v2, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 57
    .local v1, "rootView":Landroid/view/View;
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->card_list:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->staticListView:Lcom/google/apps/dots/android/newsstand/widget/StaticListView;

    .line 58
    new-instance v2, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;

    .line 59
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->staticListView:Lcom/google/apps/dots/android/newsstand/widget/StaticListView;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->setAdapter(Lcom/google/android/libraries/bind/data/DataAdapter;)V

    .line 60
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->staticListView:Lcom/google/apps/dots/android/newsstand/widget/StaticListView;

    invoke-virtual {v2, v4}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->setForceWrapHeight(Z)V

    .line 61
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->staticListView:Lcom/google/apps/dots/android/newsstand/widget/StaticListView;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$1;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->setOnSizeChangedListener(Lcom/google/apps/dots/android/newsstand/widget/StaticListView$OnSizeChangedListener;)V

    .line 76
    new-instance v2, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;)Lcom/google/apps/dots/android/newsstand/widget/StaticListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->staticListView:Lcom/google/apps/dots/android/newsstand/widget/StaticListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p3, "x3"    # Z

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->updateCardsList(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V

    return-void
.end method

.method static synthetic access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p3, "x3"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p4, "x4"    # Z

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->buildCardsList(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V

    return-void
.end method

.method private static varargs buildCardGroupList(Lcom/google/android/libraries/bind/card/CardListBuilder;[Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 4
    .param p0, "builder"    # Lcom/google/android/libraries/bind/card/CardListBuilder;
    .param p1, "groups"    # [Lcom/google/android/libraries/bind/card/CardGroup;

    .prologue
    .line 186
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, p1, v2

    .line 187
    .local v0, "group":Lcom/google/android/libraries/bind/card/CardGroup;
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 188
    .local v1, "groupData":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/card/CardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 186
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 190
    .end local v0    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    .end local v1    # "groupData":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    return-object v2
.end method

.method private buildCardsList(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V
    .locals 14
    .param p1, "owningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p3, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p4, "useDarkTheme"    # Z

    .prologue
    .line 149
    if-eqz p4, :cond_0

    sget v5, Lcom/google/android/apps/newsstanddev/R$color;->shelf_header_title_color_dark:I

    .line 154
    .local v5, "headerColor":I
    :goto_0
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 155
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/apps/newsstanddev/R$string;->article_tail_header:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    .line 154
    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeShelfHeader(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    .line 156
    .local v2, "articleTailHeader":Lcom/google/android/libraries/bind/data/Data;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getContext()Landroid/content/Context;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-direct {v3, v8, p1, v0}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    .line 159
    .local v3, "articleTailList":Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 160
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/apps/newsstanddev/R$string;->related_posts_header:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    .line 162
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getContext()Landroid/content/Context;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->title(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    .line 160
    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 162
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    .line 159
    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeShelfHeader(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v7

    .line 163
    .local v7, "relatedPostsHeader":Lcom/google/android/libraries/bind/data/Data;
    new-instance v6, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getContext()Landroid/content/Context;

    move-result-object v8

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-direct {v6, v8, v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V

    .line 165
    .local v6, "relatedArticlesList":Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->removeAll()Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 166
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    const/4 v9, 0x2

    new-array v9, v9, [Lcom/google/android/libraries/bind/card/CardGroup;

    const/4 v10, 0x0

    .line 167
    invoke-static {v3, v2}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->singleColumnGroup(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    .line 168
    invoke-static {v6, v7}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->singleColumnGroup(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;

    move-result-object v11

    aput-object v11, v9, v10

    .line 166
    invoke-static {v8, v9}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->buildCardGroupList(Lcom/google/android/libraries/bind/card/CardListBuilder;[Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v4

    .line 169
    .local v4, "cardList":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;->setSupportsErrorView(Z)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 170
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;->setSupportsEmptyView(Z)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 171
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;->setSupportsLoadingView(Z)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 172
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;

    invoke-virtual {v8, v4}, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 173
    return-void

    .line 149
    .end local v2    # "articleTailHeader":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "articleTailList":Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;
    .end local v4    # "cardList":Lcom/google/android/libraries/bind/data/DataList;
    .end local v5    # "headerColor":I
    .end local v6    # "relatedArticlesList":Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;
    .end local v7    # "relatedPostsHeader":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    sget v5, Lcom/google/android/apps/newsstanddev/R$color;->shelf_header_title_color:I

    goto/16 :goto_0
.end method

.method private static singleColumnGroup(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 2
    .param p0, "list"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p1, "header"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v1, 0x1

    .line 176
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 177
    .local v0, "cardGroup":Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setFixedNumColumns(I)Lcom/google/android/libraries/bind/card/GridGroup;

    .line 178
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setHideOnError(Z)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 179
    if-eqz p1, :cond_0

    .line 180
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->addHeader(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 182
    :cond_0
    return-object v0
.end method

.method private updateBackground(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
    .locals 3
    .param p1, "owningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .param p2, "articleLoader"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 98
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 99
    .local v0, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-virtual {p2, v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 126
    return-void
.end method

.method private updateCardsList(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V
    .locals 7
    .param p1, "owningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .param p2, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p3, "useDarkTheme"    # Z

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    .line 131
    .local v2, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-virtual {p1, v2}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V

    invoke-static {v6, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 145
    return-void
.end method


# virtual methods
.method public hasDataListRefreshedOnce()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->haveAllGroupsRefreshedOnce()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 195
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 196
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 197
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->staticListView:Lcom/google/apps/dots/android/newsstand/widget/StaticListView;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->setAdapter(Lcom/google/android/libraries/bind/data/DataAdapter;)V

    .line 198
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 199
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 81
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 82
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->parent:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->hasDataListRefreshedOnce()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->parent:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->onArticleTailLaidOut()V

    .line 85
    :cond_0
    return-void
.end method

.method public setup(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
    .locals 1
    .param p1, "owningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .param p2, "articleLoader"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->parent:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .line 89
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->updateBackground(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    .line 90
    return-void
.end method
