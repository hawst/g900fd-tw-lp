.class public Lcom/google/apps/dots/android/newsstand/exception/NoSpaceLeftException;
.super Ljava/io/IOException;
.source "NoSpaceLeftException.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 13
    return-void
.end method

.method private constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    .line 17
    return-void
.end method

.method public static create(Ljava/lang/Throwable;)Lcom/google/apps/dots/android/newsstand/exception/NoSpaceLeftException;
    .locals 1
    .param p0, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 20
    new-instance v0, Lcom/google/apps/dots/android/newsstand/exception/NoSpaceLeftException;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/exception/NoSpaceLeftException;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static detect(Ljava/io/IOException;)Z
    .locals 2
    .param p0, "e"    # Ljava/io/IOException;

    .prologue
    .line 24
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "No space left on device"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
