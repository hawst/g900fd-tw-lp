.class public abstract Lcom/google/apps/dots/android/newsstand/async/QueueTask;
.super Ljava/lang/Object;
.source "QueueTask.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field protected asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private volatile future:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final queue:Lcom/google/apps/dots/android/newsstand/async/Queue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V
    .locals 2
    .param p1, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;

    .prologue
    .line 20
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;J)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/Queue;J)V
    .locals 0
    .param p1, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;
    .param p2, "priority"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->queue:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/async/QueueTask;)Lcom/google/apps/dots/android/newsstand/async/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/async/QueueTask;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->queue:Lcom/google/apps/dots/android/newsstand/async/Queue;

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method


# virtual methods
.method public asyncToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    return-object v0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->cancel(Z)V

    .line 106
    return-void
.end method

.method public cancel(Z)V
    .locals 1
    .param p1, "mayInterrupt"    # Z

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, p1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 101
    :cond_0
    return-void
.end method

.method protected abstract doInBackground()V
.end method

.method public execute()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "optAsyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "optAsyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "runOnPostExecute"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 50
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->onPreExecute()V

    .line 52
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;-><init>(Lcom/google/apps/dots/android/newsstand/async/QueueTask;ZLcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 74
    .local v0, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Ljava/lang/Void;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->queue:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-virtual {v1, v2, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->submit(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 76
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v1

    .line 74
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->queue:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/async/Queue;->submit(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPostExecute()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method
