.class public final Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;
.super Ljava/lang/Object;
.source "AttachmentStore.java"


# static fields
.field public static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final cantUseMutableBitmap:Ljava/lang/Exception;


# instance fields
.field private final bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

.field private final bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;

.field private final coverTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;

.field private final defaultTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;

.field private final formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

.field private final nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field private final sectionStore:Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

.field private final util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

.field private final zoomTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    const-class v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 115
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Can\'t use mutable bitmap"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->cantUseMutableBitmap:Ljava/lang/Exception;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;Lcom/google/apps/dots/android/newsstand/util/BytePool;Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;)V
    .locals 1
    .param p1, "nsStore"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p2, "sectionStore"    # Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;
    .param p3, "formStore"    # Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;
    .param p4, "util"    # Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
    .param p5, "bytePool"    # Lcom/google/apps/dots/android/newsstand/util/BytePool;
    .param p6, "bitmapPool"    # Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 78
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->sectionStore:Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    .line 79
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    .line 80
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .line 81
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;

    .line 82
    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    .line 84
    invoke-virtual {p4}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDefaultTransform()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->defaultTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 85
    const/high16 v0, 0x3fc00000    # 1.5f

    invoke-virtual {p4, v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getScaledDefaultTransform(F)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->zoomTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 86
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p4, v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getScaledDefaultTransform(F)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->coverTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 87
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p3, "x3"    # Landroid/graphics/BitmapFactory$Options;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->decodeWithMutableBitmap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p3, "x3"    # Landroid/graphics/BitmapFactory$Options;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->decodeWithAllocatedBitmap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/server/Transform;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->defaultTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .param p2, "x2"    # [B
    .param p3, "x3"    # Landroid/graphics/BitmapFactory$Options;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->retryBitmapDecodeWithGC(Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p4, "x4"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->simulateBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/server/Transform;Landroid/graphics/Bitmap;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p2, "x2"    # Landroid/graphics/Bitmap;
    .param p3, "x3"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->transformDefaultBitmap(Lcom/google/apps/dots/android/newsstand/server/Transform;Landroid/graphics/Bitmap;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/server/Transform;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->zoomTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/server/Transform;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->coverTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    return-object v0
.end method

.method static computeTransformScaling(Lcom/google/apps/dots/android/newsstand/server/Transform;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 12
    .param p0, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p1, "defaultRect"    # Landroid/graphics/Rect;
    .param p2, "srcRectOut"    # Landroid/graphics/Rect;
    .param p3, "dstRectOut"    # Landroid/graphics/Rect;

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/server/Transform;->isCroppingTransform()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 407
    iget v8, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    if-lez v8, :cond_1

    iget v8, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    if-lez v8, :cond_1

    const/4 v8, 0x1

    :goto_0
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 411
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/server/Transform;->isFcrop64Transform()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 412
    iget v8, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Left:I

    int-to-float v8, v8

    const v9, 0x477fff00    # 65535.0f

    div-float v3, v8, v9

    .line 413
    .local v3, "leftFraction":F
    iget v8, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Top:I

    int-to-float v8, v8

    const v9, 0x477fff00    # 65535.0f

    div-float v6, v8, v9

    .line 414
    .local v6, "topFraction":F
    iget v8, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Right:I

    int-to-float v8, v8

    const v9, 0x477fff00    # 65535.0f

    div-float v4, v8, v9

    .line 415
    .local v4, "rightFraction":F
    iget v8, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Bottom:I

    int-to-float v8, v8

    const v9, 0x477fff00    # 65535.0f

    div-float v0, v8, v9

    .line 416
    .local v0, "bottomFraction":F
    new-instance v1, Landroid/graphics/Rect;

    .line 417
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v3

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 418
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v9, v6

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    .line 419
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v10, v4

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    .line 420
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v11, v0

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    invoke-direct {v1, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .end local p1    # "defaultRect":Landroid/graphics/Rect;
    .local v1, "defaultRect":Landroid/graphics/Rect;
    move-object p1, v1

    .line 423
    .end local v0    # "bottomFraction":F
    .end local v1    # "defaultRect":Landroid/graphics/Rect;
    .end local v3    # "leftFraction":F
    .end local v4    # "rightFraction":F
    .end local v6    # "topFraction":F
    .restart local p1    # "defaultRect":Landroid/graphics/Rect;
    :cond_0
    const/4 v8, 0x0

    const/4 v9, 0x0

    iget v10, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    iget v11, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    invoke-virtual {p3, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 424
    invoke-static {p3, p1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->fitInside(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 427
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    .line 428
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    .line 426
    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 430
    .local v5, "scale":F
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 431
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v10, v5

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v11, v5

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    .line 430
    invoke-virtual {p2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 437
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 438
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v9

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v10

    sub-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v9, v8

    iget v10, p1, Landroid/graphics/Rect;->top:I

    .line 442
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v11

    sub-int v11, v8, v11

    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->crop:Z

    if-eqz v8, :cond_2

    const/4 v8, 0x4

    :goto_1
    div-int v8, v11, v8

    add-int/2addr v8, v10

    .line 437
    invoke-virtual {p2, v9, v8}, Landroid/graphics/Rect;->offset(II)V

    .line 456
    .end local v5    # "scale":F
    :goto_2
    return-void

    .line 407
    :cond_1
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 442
    .restart local v5    # "scale":F
    :cond_2
    const/4 v8, 0x2

    goto :goto_1

    .line 446
    .end local v5    # "scale":F
    :cond_3
    iget v8, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    if-lez v8, :cond_4

    .line 447
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v8

    iget v9, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 449
    .local v7, "widthMax":I
    :goto_3
    iget v8, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    if-lez v8, :cond_5

    .line 450
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v8

    iget v9, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 452
    .local v2, "heightMax":I
    :goto_4
    invoke-virtual {p3, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 453
    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10, v7, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {p3, v8}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->fitInside(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 454
    invoke-virtual {p2, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_2

    .line 447
    .end local v2    # "heightMax":I
    .end local v7    # "widthMax":I
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    goto :goto_3

    .line 450
    .restart local v7    # "widthMax":I
    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    goto :goto_4
.end method

.method private decodeFn(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/AsyncFunction;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "decodeOptions"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;",
            ")",
            "Lcom/google/common/util/concurrent/AsyncFunction",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    return-object v0
.end method

.method private decodeWithAllocatedBitmap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "blobFile"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p3, "options"    # Landroid/graphics/BitmapFactory$Options;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;",
            "Landroid/graphics/BitmapFactory$Options;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 255
    sget-object v1, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Decode with allocated bitmap"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;

    invoke-direct {v1, p0, p3, p2, p1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;-><init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Landroid/graphics/BitmapFactory$Options;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 257
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->create(Ljava/util/concurrent/Callable;)Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;

    move-result-object v0

    .line 290
    .local v0, "decodeTask":Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask<Landroid/graphics/Bitmap;>;"
    sget-object v1, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->DECODE_BITMAP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/libraries/bind/async/JankLock;->executeOn(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;Z)V

    .line 291
    return-object v0
.end method

.method private decodeWithMutableBitmap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "blobFile"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p3, "options"    # Landroid/graphics/BitmapFactory$Options;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;",
            "Landroid/graphics/BitmapFactory$Options;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 165
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 166
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->cantUseMutableBitmap:Ljava/lang/Exception;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 206
    :goto_0
    return-object v0

    .line 171
    :cond_0
    iget-object v0, p3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne v0, v1, :cond_1

    .line 172
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->cantUseMutableBitmap:Ljava/lang/Exception;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->acquire(I)[B

    move-result-object v8

    .line 178
    .local v8, "inputStreamBuffer":[B
    invoke-interface {p2}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeDiskBlob()Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    move-result-object v7

    .line 180
    .local v7, "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    :try_start_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/io/PoolBufferedInputStream;

    .line 181
    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->createInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1, v8}, Lcom/google/apps/dots/android/newsstand/io/PoolBufferedInputStream;-><init>(Ljava/io/InputStream;[B)V

    .line 180
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;->getBitmapInfo(Ljava/io/InputStream;)Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 183
    .local v2, "imageInfo":Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;

    invoke-virtual {v0, v8}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 186
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->canDrawIntoMutableBitmap()Z

    move-result v0

    if-nez v0, :cond_2

    .line 187
    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->close()V

    .line 188
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->cantUseMutableBitmap:Ljava/lang/Exception;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 183
    .end local v2    # "imageInfo":Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;

    invoke-virtual {v1, v8}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    throw v0

    .line 191
    .restart local v2    # "imageInfo":Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    :cond_2
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Will use mutable bitmap for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v3, v5

    invoke-virtual {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    iget v0, v2, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->width:I

    iget v1, v2, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->height:I

    iget-object v3, p3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 194
    invoke-direct {p0, v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getMutableBitmap(IILandroid/graphics/Bitmap$Config;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 199
    .local v9, "mutableBitmapFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;"
    invoke-interface {v9}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->wasFailure(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 200
    :cond_3
    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->close()V

    .line 201
    const/4 v7, 0x0

    .line 204
    :cond_4
    move-object v4, v7

    .line 206
    .local v4, "finalDiskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;

    move-object v1, p0

    move-object v3, p3

    move-object v5, p2

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;-><init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;Landroid/graphics/BitmapFactory$Options;Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-static {v9, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method static fitInside(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 5
    .param p0, "inner"    # Landroid/graphics/Rect;
    .param p1, "outer"    # Landroid/graphics/Rect;

    .prologue
    const/4 v4, 0x0

    .line 463
    .line 464
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 465
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 463
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 466
    .local v0, "scale":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 467
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {p0, v4, v4, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 469
    :cond_0
    return-void
.end method

.method private getMutableBitmap(IILandroid/graphics/Bitmap$Config;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/graphics/Bitmap$Config;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, p2, p3, v3}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getPoolBitmap(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 141
    .local v0, "mutable":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 142
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 155
    :goto_0
    return-object v1

    .line 144
    :cond_0
    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;-><init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;IILandroid/graphics/Bitmap$Config;)V

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->create(Ljava/util/concurrent/Callable;)Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;

    move-result-object v1

    .line 154
    .local v1, "task":Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask<Landroid/graphics/Bitmap;>;"
    sget-object v2, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/Queues;->DECODE_BITMAP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/libraries/bind/async/JankLock;->executeOn(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;Z)V

    goto :goto_0
.end method

.method private retryBitmapDecodeWithGC(Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "diskBlob"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .param p2, "inputStreamBuffer"    # [B
    .param p3, "options"    # Landroid/graphics/BitmapFactory$Options;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;-><init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;[BLandroid/graphics/BitmapFactory$Options;)V

    .line 131
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;->run()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private scale(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "srcBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "srcRect"    # Landroid/graphics/Rect;
    .param p3, "dstRect"    # Landroid/graphics/Rect;
    .param p4, "decodeOptions"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    .prologue
    .line 472
    invoke-virtual {p2, p3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 479
    .end local p1    # "srcBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object p1

    .line 476
    .restart local p1    # "srcBitmap":Landroid/graphics/Bitmap;
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p4, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 477
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, p3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 478
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    move-object p1, v0

    .line 479
    goto :goto_0
.end method

.method private simulateBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "attachmentId"    # Ljava/lang/String;
    .param p3, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p4, "decodeOptions"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/server/Transform;",
            "Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366
    const/4 v1, 0x0

    .line 367
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 368
    invoke-direct {p0, p1, p4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->decodeFn(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/AsyncFunction;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/Queues;->DECODE_BITMAP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 366
    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 370
    .local v0, "defaultBitmapFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;"
    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;

    invoke-direct {v1, p0, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;-><init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)V

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->DECODE_BITMAP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/async/Queue;->fallbackIfMain:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method private transformDefaultBitmap(Lcom/google/apps/dots/android/newsstand/server/Transform;Landroid/graphics/Bitmap;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p2, "defaultBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "decodeOptions"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    .prologue
    const/4 v5, 0x0

    .line 389
    iget-boolean v2, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;->original:Z

    if-eqz v2, :cond_0

    .line 399
    .end local p2    # "defaultBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object p2

    .line 393
    .restart local p2    # "defaultBitmap":Landroid/graphics/Bitmap;
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 394
    .local v0, "dstRect":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 396
    .local v1, "srcRect":Landroid/graphics/Rect;
    new-instance v2, Landroid/graphics/Rect;

    .line 397
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 396
    invoke-static {p1, v2, v1, v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->computeTransformScaling(Lcom/google/apps/dots/android/newsstand/server/Transform;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 399
    invoke-direct {p0, p2, v1, v0, p3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->scale(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Landroid/graphics/Bitmap;

    move-result-object p2

    goto :goto_0
.end method


# virtual methods
.method public getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "storeRequest"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 313
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    .line 314
    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getDefaultTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$6;-><init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    .line 313
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "attachmentId"    # Ljava/lang/String;
    .param p3, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/server/Transform;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 299
    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v1, p2, v2}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 300
    invoke-virtual {v1, p3}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform(Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    .line 301
    .local v0, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method public getBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "attachmentId"    # Ljava/lang/String;
    .param p3, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/server/Transform;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 327
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->DEFAULT:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public getBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "attachmentId"    # Ljava/lang/String;
    .param p3, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p4, "decodeOptions"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/server/Transform;",
            "Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 339
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v0, p2, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 340
    invoke-virtual {v0, p3}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform(Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v7

    .line 342
    .local v7, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-virtual {p0, p1, v7}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 343
    invoke-direct {p0, p1, p4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->decodeFn(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/AsyncFunction;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->DECODE_BITMAP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 341
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 346
    .local v6, "bitmapFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;-><init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)V

    invoke-static {v6, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "attachmentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/server/Transform;",
            ">;"
        }
    .end annotation

    .prologue
    .line 484
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getDefaultTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "attachmentId"    # Ljava/lang/String;
    .param p3, "onlyIfCached"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/server/Transform;",
            ">;"
        }
    .end annotation

    .prologue
    .line 502
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getObjectIdProto(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v0

    .line 504
    .local v0, "attachmentObjectId":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    if-eqz v0, :cond_0

    const/4 v2, 0x4

    .line 505
    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;I)Ljava/lang/String;

    move-result-object v1

    .local v1, "sectionId":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 508
    .end local v1    # "sectionId":Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/server/Transform;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/server/Transform;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 514
    :goto_0
    return-object v2

    .line 509
    .restart local v1    # "sectionId":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->getPreferZoomTransformationHint()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 510
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->zoomTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_0

    .line 514
    :cond_2
    if-eqz p3, :cond_3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->sectionStore:Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    .line 516
    invoke-virtual {v2, p1, v1}, Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;->getAvailable(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 517
    :goto_1
    new-instance v3, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;

    invoke-direct {v3, p0, p3, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;-><init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;ZLcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)V

    .line 514
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_0

    .line 516
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->sectionStore:Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    .line 517
    invoke-virtual {v2, p1, v1}, Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;->getAny(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_1
.end method
