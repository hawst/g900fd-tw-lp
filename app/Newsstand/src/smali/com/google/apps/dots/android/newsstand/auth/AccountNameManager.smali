.class public abstract Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;
.super Ljava/lang/Object;
.source "AccountNameManager.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field static final SUPPORTS_RENAMES:Z


# instance fields
.field private final accountManagerDelegate:Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;

.field private nameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final nameMapLock:Ljava/lang/Object;

.field private updateListener:Landroid/accounts/OnAccountsUpdateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    const-class v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 43
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->SUPPORTS_RENAMES:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;)V
    .locals 1
    .param p1, "accountManagerDelegate"    # Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMapLock:Ljava/lang/Object;

    .line 56
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->accountManagerDelegate:Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;

    .line 57
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->updateNameMap(Z)V

    return-void
.end method

.method private cleanUpNameMap(Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 283
    .local p1, "currentNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMapLock:Ljava/lang/Object;

    monitor-enter v3

    .line 284
    :try_start_0
    new-instance v1, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 285
    .local v1, "deletedNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 286
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 287
    .local v0, "deletedName":Ljava/lang/String;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Deleting inactive name [%s]"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 290
    .end local v0    # "deletedName":Ljava/lang/String;
    .end local v1    # "deletedNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v1    # "deletedNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291
    return-void
.end method

.method private getAllGoogleAccountNames()Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getAllGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 227
    .local v1, "accounts":[Landroid/accounts/Account;
    new-instance v2, Ljava/util/HashSet;

    array-length v3, v1

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(I)V

    .line 228
    .local v2, "names":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v1, v3

    .line 229
    .local v0, "account":Landroid/accounts/Account;
    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 228
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 231
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    return-object v2
.end method

.method private initializeNameMap()V
    .locals 2

    .prologue
    .line 235
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->SUPPORTS_RENAMES:Z

    if-nez v0, :cond_0

    .line 243
    :goto_0
    return-void

    .line 238
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMapLock:Ljava/lang/Object;

    monitor-enter v1

    .line 239
    :try_start_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->loadNameMap()V

    .line 241
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->updateNameMap(Z)V

    .line 242
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private loadNameMap()V
    .locals 3

    .prologue
    .line 263
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMapLock:Ljava/lang/Object;

    monitor-enter v2

    .line 264
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->retrieveNameMap()Ljava/util/Map;

    move-result-object v0

    .line 266
    .local v0, "retrieved":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v0, :cond_0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    :goto_0
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    .line 269
    monitor-exit v2

    .line 270
    return-void

    .line 266
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_0

    .line 269
    .end local v0    # "retrieved":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private saveNameMap()V
    .locals 2

    .prologue
    .line 273
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMapLock:Ljava/lang/Object;

    monitor-enter v1

    .line 274
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->persistNameMap(Ljava/util/Map;)V

    .line 275
    monitor-exit v1

    .line 276
    return-void

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateNameMap(Z)V
    .locals 17
    .param p1, "cleanUp"    # Z

    .prologue
    .line 300
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 301
    .local v7, "renameEvents":Ljava/util/List;, "Ljava/util/List<[Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMapLock:Ljava/lang/Object;

    monitor-enter v10

    .line 302
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getAllGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 303
    .local v2, "currentAccounts":[Landroid/accounts/Account;
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 305
    .local v3, "currentNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    array-length v11, v2

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v11, :cond_2

    aget-object v1, v2, v9

    .line 306
    .local v1, "account":Landroid/accounts/Account;
    iget-object v12, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v3, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 309
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    iget-object v13, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v12, v13}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 310
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v5

    .line 311
    .local v5, "policy":Landroid/os/StrictMode$ThreadPolicy;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->accountManagerDelegate:Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;

    invoke-virtual {v12, v1}, Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;->getPreviousName(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    .line 312
    .local v6, "previousName":Ljava/lang/String;
    invoke-static {v5}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 314
    if-eqz v6, :cond_1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    invoke-interface {v12, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 318
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    invoke-interface {v12, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 319
    .local v4, "originalName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    iget-object v13, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v12, v13, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    sget-object v12, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v13, "Account rename detected from [%s] to [%s]. Original name is [%s]."

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v6, v14, v15

    const/4 v15, 0x1

    iget-object v0, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    const/4 v15, 0x2

    aput-object v4, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 322
    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    iget-object v14, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v6, v12, v13

    const/4 v13, 0x2

    aput-object v4, v12, v13

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 305
    .end local v4    # "originalName":Ljava/lang/String;
    .end local v5    # "policy":Landroid/os/StrictMode$ThreadPolicy;
    .end local v6    # "previousName":Ljava/lang/String;
    :cond_0
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 325
    .restart local v5    # "policy":Landroid/os/StrictMode$ThreadPolicy;
    .restart local v6    # "previousName":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    iget-object v13, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v14, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v12, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    sget-object v12, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v13, "New account detected: [%s]"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget-object v0, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 336
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "currentAccounts":[Landroid/accounts/Account;
    .end local v3    # "currentNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "policy":Landroid/os/StrictMode$ThreadPolicy;
    .end local v6    # "previousName":Ljava/lang/String;
    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9

    .line 331
    .restart local v2    # "currentAccounts":[Landroid/accounts/Account;
    .restart local v3    # "currentNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    if-eqz p1, :cond_3

    .line 332
    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->cleanUpNameMap(Ljava/util/Set;)V

    .line 335
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->saveNameMap()V

    .line 336
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 339
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    .line 340
    .local v8, "triple":[Ljava/lang/String;
    const/4 v10, 0x0

    aget-object v10, v8, v10

    const/4 v11, 0x1

    aget-object v11, v8, v11

    const/4 v12, 0x2

    aget-object v12, v8, v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->onAccountRenamed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 342
    .end local v8    # "triple":[Ljava/lang/String;
    :cond_4
    return-void
.end method


# virtual methods
.method public getAllGoogleAccounts()[Landroid/accounts/Account;
    .locals 10

    .prologue
    .line 96
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v7

    .line 97
    .local v7, "policy":Landroid/os/StrictMode$ThreadPolicy;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->accountManagerDelegate:Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;

    const-string v9, "com.google"

    invoke-virtual {v8, v9}, Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 98
    .local v1, "accounts":[Landroid/accounts/Account;
    invoke-static {v7}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 102
    const/4 v6, 0x0

    .line 103
    .local v6, "numNonNull":I
    array-length v9, v1

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v9, :cond_1

    aget-object v0, v1, v8

    .line 104
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 105
    add-int/lit8 v6, v6, 0x1

    .line 103
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 108
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    array-length v8, v1

    if-ne v6, v8, :cond_2

    .line 117
    .end local v1    # "accounts":[Landroid/accounts/Account;
    :goto_1
    return-object v1

    .line 111
    .restart local v1    # "accounts":[Landroid/accounts/Account;
    :cond_2
    new-array v2, v6, [Landroid/accounts/Account;

    .line 112
    .local v2, "filtered":[Landroid/accounts/Account;
    const/4 v3, 0x0

    .local v3, "i":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_2
    array-length v8, v1

    if-ge v3, v8, :cond_4

    .line 113
    aget-object v8, v1, v3

    if-eqz v8, :cond_3

    .line 114
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "j":I
    .local v5, "j":I
    aget-object v8, v1, v3

    aput-object v8, v2, v4

    move v4, v5

    .line 112
    .end local v5    # "j":I
    .restart local v4    # "j":I
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    move-object v1, v2

    .line 117
    goto :goto_1
.end method

.method public getCurrentName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "originalName"    # Ljava/lang/String;

    .prologue
    .line 185
    sget-boolean v1, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->SUPPORTS_RENAMES:Z

    if-nez v1, :cond_0

    .line 197
    .end local p1    # "originalName":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 188
    .restart local p1    # "originalName":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMapLock:Ljava/lang/Object;

    monitor-enter v2

    .line 191
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 192
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 193
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    monitor-exit v2

    move-object p1, v1

    goto :goto_0

    .line 196
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getOriginalName(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 126
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getOriginalName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOriginalName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "currentName"    # Ljava/lang/String;

    .prologue
    .line 135
    sget-boolean v1, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->SUPPORTS_RENAMES:Z

    if-nez v1, :cond_0

    .line 144
    .end local p1    # "currentName":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 138
    .restart local p1    # "currentName":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMapLock:Ljava/lang/Object;

    monitor-enter v2

    .line 139
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 140
    .local v0, "originalName":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 141
    sget-object v1, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Original name not found for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    monitor-exit v2

    goto :goto_0

    .line 145
    .end local v0    # "originalName":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 144
    .restart local v0    # "originalName":Ljava/lang/String;
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object p1, v0

    goto :goto_0
.end method

.method public getOriginalNames()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->SUPPORTS_RENAMES:Z

    if-nez v0, :cond_0

    .line 206
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getAllGoogleAccountNames()Ljava/util/Set;

    move-result-object v0

    .line 209
    :goto_0
    return-object v0

    .line 208
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMapLock:Ljava/lang/Object;

    monitor-enter v1

    .line 209
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->nameMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    monitor-exit v1

    goto :goto_0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected init()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->initializeNameMap()V

    .line 64
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->startListeningForAccountChanges()V

    .line 65
    return-void
.end method

.method protected onAccountRenamed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "currentName"    # Ljava/lang/String;
    .param p2, "previousName"    # Ljava/lang/String;
    .param p3, "originalName"    # Ljava/lang/String;

    .prologue
    .line 347
    return-void
.end method

.method protected abstract persistNameMap(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method protected abstract retrieveNameMap()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public startListeningForAccountChanges()V
    .locals 4

    .prologue
    .line 71
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->SUPPORTS_RENAMES:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->updateListener:Landroid/accounts/OnAccountsUpdateListener;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager$1;-><init>(Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->updateListener:Landroid/accounts/OnAccountsUpdateListener;

    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->accountManagerDelegate:Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->updateListener:Landroid/accounts/OnAccountsUpdateListener;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 82
    :cond_0
    return-void
.end method
