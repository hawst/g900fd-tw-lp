.class public Lcom/google/apps/dots/android/newsstand/sync/FatalSyncException;
.super Lcom/google/apps/dots/android/newsstand/sync/SyncException;
.source "FatalSyncException.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/SyncException;-><init>()V

    .line 10
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/SyncException;-><init>(Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/SyncException;-><init>(Ljava/lang/Throwable;)V

    .line 22
    return-void
.end method
