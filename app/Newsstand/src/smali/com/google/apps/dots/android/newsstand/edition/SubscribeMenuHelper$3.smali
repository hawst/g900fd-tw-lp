.class Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$3;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "SubscribeMenuHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

.field final synthetic val$addEditionMenuItem:Landroid/view/MenuItem;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;Landroid/view/MenuItem;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$3;->this$0:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$3;->val$addEditionMenuItem:Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 2
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 114
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$3;->val$addEditionMenuItem:Landroid/view/MenuItem;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 115
    return-void

    .line 114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 111
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$3;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
