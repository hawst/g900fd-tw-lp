.class public Lcom/google/apps/dots/android/newsstand/util/StringUtil;
.super Lcom/google/android/libraries/bind/util/StringUtil;
.source "StringUtil.java"


# static fields
.field public static final SUPPORTS_ZWNJ:Z

.field private static final VALID_SLUG_CHARS:Lcom/google/common/base/CharMatcher;

.field private static final filenameEncoder:Lcom/google/common/io/BaseEncoding;

.field private static final longHashFunction:Lcom/google/common/hash/HashFunction;

.field private static final sha256:Lcom/google/common/hash/HashFunction;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->SUPPORTS_ZWNJ:Z

    .line 31
    const/16 v0, 0x40

    invoke-static {v0}, Lcom/google/common/hash/Hashing;->goodFastHash(I)Lcom/google/common/hash/HashFunction;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->longHashFunction:Lcom/google/common/hash/HashFunction;

    .line 32
    invoke-static {}, Lcom/google/common/hash/Hashing;->sha256()Lcom/google/common/hash/HashFunction;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->sha256:Lcom/google/common/hash/HashFunction;

    .line 34
    invoke-static {}, Lcom/google/common/io/BaseEncoding;->base32()Lcom/google/common/io/BaseEncoding;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/io/BaseEncoding;->omitPadding()Lcom/google/common/io/BaseEncoding;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/io/BaseEncoding;->lowerCase()Lcom/google/common/io/BaseEncoding;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->filenameEncoder:Lcom/google/common/io/BaseEncoding;

    .line 36
    sget-object v0, Lcom/google/common/base/CharMatcher;->JAVA_LETTER_OR_DIGIT:Lcom/google/common/base/CharMatcher;

    const/16 v1, 0x5f

    .line 37
    invoke-static {v1}, Lcom/google/common/base/CharMatcher;->is(C)Lcom/google/common/base/CharMatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/base/CharMatcher;->or(Lcom/google/common/base/CharMatcher;)Lcom/google/common/base/CharMatcher;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->VALID_SLUG_CHARS:Lcom/google/common/base/CharMatcher;

    .line 36
    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ellipsis(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "maxLength"    # I

    .prologue
    .line 113
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 114
    const/4 v1, 0x0

    add-int/lit8 v2, p1, -0x2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 115
    .local v0, "shortenedStr":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 117
    .end local v0    # "shortenedStr":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method public static endsWithIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "suffix"    # Ljava/lang/String;

    .prologue
    .line 172
    const/4 v1, 0x1

    .line 173
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int v2, v0, v2

    const/4 v4, 0x0

    .line 175
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    move-object v0, p0

    move-object v3, p1

    .line 172
    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public static filenameSafeStringHash(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 109
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->filenameEncoder:Lcom/google/common/io/BaseEncoding;

    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->stableHash256(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/io/BaseEncoding;->encode([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getAlphanumericCharactersSingleSpaced(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-static {p0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    const-string v0, ""

    .line 84
    :goto_0
    return-object v0

    .line 83
    :cond_0
    const-string v0, "[^\\p{L}\\p{Nd}\\p{Z}]"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 84
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->getAnyCharactersSingleSpaced(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getAnyCharactersSingleSpaced(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-static {p0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const-string v0, ""

    .line 91
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/common/base/CharMatcher;->WHITESPACE:Lcom/google/common/base/CharMatcher;

    const/16 v1, 0x20

    invoke-virtual {v0, p0, v1}, Lcom/google/common/base/CharMatcher;->trimAndCollapseFrom(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getInitials(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "editionTitle"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 46
    invoke-static {p0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 47
    const-string v5, ""

    .line 75
    :cond_0
    :goto_0
    return-object v5

    .line 50
    :cond_1
    sget-object v7, Ljava/text/Normalizer$Form;->NFC:Ljava/text/Normalizer$Form;

    invoke-static {p0, v7}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object p0

    .line 51
    const-string v1, ""

    .line 53
    .local v1, "initials":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->getAlphanumericCharactersSingleSpaced(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 54
    .local v5, "wordString":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_2

    .line 55
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->getAnyCharactersSingleSpaced(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 58
    :cond_2
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_0

    .line 62
    const-string v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 63
    .local v6, "words":[Ljava/lang/String;
    array-length v7, v6

    const/4 v8, 0x2

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 64
    .local v2, "numInitials":I
    const/4 v4, 0x0

    .local v4, "wordIndex":I
    :goto_1
    if-ge v4, v2, :cond_5

    .line 66
    aget-object v3, v6, v4

    .line 67
    .local v3, "word":Ljava/lang/String;
    invoke-virtual {v3, v11}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 68
    .local v0, "codePoint":I
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v8

    invoke-virtual {v3, v11, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v7, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    :goto_2
    sget-boolean v7, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->SUPPORTS_ZWNJ:Z

    if-eqz v7, :cond_3

    add-int/lit8 v7, v2, -0x1

    if-ge v4, v7, :cond_3

    .line 71
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x200c

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 64
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 68
    :cond_4
    new-instance v1, Ljava/lang/String;

    .end local v1    # "initials":Ljava/lang/String;
    invoke-direct {v1, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .end local v0    # "codePoint":I
    .end local v3    # "word":Ljava/lang/String;
    .restart local v1    # "initials":Ljava/lang/String;
    :cond_5
    move-object v5, v1

    .line 75
    goto :goto_0
.end method

.method public static relativePastTimeString(J)Ljava/lang/String;
    .locals 6
    .param p0, "time"    # J

    .prologue
    .line 189
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 190
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    move-wide v0, p0

    .line 189
    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;

    move-result-object v0

    .line 190
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static stableHash256(Ljava/lang/String;)[B
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 105
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->sha256:Lcom/google/common/hash/HashFunction;

    invoke-interface {v0, p0}, Lcom/google/common/hash/HashFunction;->hashString(Ljava/lang/CharSequence;)Lcom/google/common/hash/HashCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/hash/HashCode;->asBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public static whiteSpaceToNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "optStr"    # Ljava/lang/String;

    .prologue
    .line 182
    if-nez p0, :cond_0

    .line 183
    const/4 v0, 0x0

    .line 185
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
