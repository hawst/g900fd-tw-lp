.class public Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;
.super Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;
.source "ReadNowList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ReadNowEditionCardListVisitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p5, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    .line 111
    invoke-direct/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    .line 112
    return-void
.end method


# virtual methods
.method protected getAnalyticsScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const-string v0, "ReadNow"

    return-object v0
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 3
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V

    .line 131
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getAppType()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->getType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 132
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    # getter for: Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->appContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->access$000(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->inContext(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    move-result-object v1

    .line 133
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->getDismissibleReadNowOnlineCardIfNeeded()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 134
    .local v0, "warmWelcomeCardData":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_0

    .line 135
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;->primaryKey:I

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_TITLE:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 136
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 139
    .end local v0    # "warmWelcomeCardData":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 1
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    invoke-virtual {p0, p2, v0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;->makeOfferCardData(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;)V

    .line 126
    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 0

    .prologue
    .line 108
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V

    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 0

    .prologue
    .line 108
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    return-void
.end method
