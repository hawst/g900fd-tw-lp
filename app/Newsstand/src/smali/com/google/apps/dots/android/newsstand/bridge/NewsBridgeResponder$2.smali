.class Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;
.super Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;
.source "NewsBridgeResponder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->openDrawer(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

.field final synthetic val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field final synthetic val$fieldId:Ljava/lang/String;

.field final synthetic val$offset:I

.field final synthetic val$restrictToSingleField:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;Ljava/lang/String;ILcom/google/apps/dots/android/newsstand/activity/NSActivity;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$fieldId:Ljava/lang/String;

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$offset:I

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iput-boolean p5, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$restrictToSingleField:Z

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 13
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 151
    if-nez p1, :cond_0

    .line 210
    :goto_0
    return-void

    .line 154
    :cond_0
    const/4 v4, 0x0

    .line 155
    .local v4, "primaryImageId":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$fieldId:Ljava/lang/String;

    const-string v9, "primaryImage"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->hasSummary()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 156
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasPrimaryImage()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 157
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v4

    .line 159
    :cond_1
    const/4 v2, 0x0

    .line 160
    .local v2, "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v9, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v10, v9

    move v7, v8

    :goto_1
    if-ge v7, v10, :cond_3

    aget-object v1, v9, v7

    .line 161
    .local v1, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget-object v11, v1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    iget-object v12, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$fieldId:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    if-eqz v4, :cond_6

    .line 162
    :cond_2
    iget-object v11, v1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iget v12, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$offset:I

    aget-object v5, v11, v12

    .line 165
    .local v5, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v4, :cond_b

    .line 166
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasImage()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 167
    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .end local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$offset:I

    iget-object v9, v1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-direct {v2, v6, v7, v9, v5}, Lcom/google/apps/dots/android/newsstand/media/MediaItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .line 198
    .end local v1    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .end local v5    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    .restart local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    :cond_3
    :goto_2
    if-eqz v2, :cond_f

    .line 199
    move-object v3, v2

    .line 200
    .local v3, "mediaItemToShow":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    new-instance v6, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 201
    invoke-virtual {v6, v3}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->setMediaItem(Lcom/google/apps/dots/android/newsstand/media/MediaItem;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    .line 202
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$restrictToSingleField:Z

    .line 203
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->restrictToSingleField(Z)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .line 204
    # getter for: Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->access$000(Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;)Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getReadingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .line 205
    # getter for: Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->access$000(Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;)Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->setOwningEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    move-result-object v6

    .line 206
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->startIfAvailable()V

    goto/16 :goto_0

    .line 169
    .end local v3    # "mediaItemToShow":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .restart local v1    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .restart local v5    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_4
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v11

    if-nez v11, :cond_5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 172
    :cond_5
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 173
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->hasThumbnail()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 174
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "attachmentId":Ljava/lang/String;
    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_a

    .line 160
    .end local v0    # "attachmentId":Ljava/lang/String;
    .end local v5    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .restart local v5    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_7
    move-object v0, v6

    .line 174
    goto :goto_3

    .line 176
    :cond_8
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getStreamingVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->hasThumbnail()Z

    move-result v11

    if-eqz v11, :cond_9

    .line 177
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getStreamingVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_9
    move-object v0, v6

    goto :goto_3

    .line 182
    .restart local v0    # "attachmentId":Ljava/lang/String;
    :cond_a
    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    .end local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$offset:I

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$fieldId:Ljava/lang/String;

    invoke-direct {v2, v6, v7, v9, v5}, Lcom/google/apps/dots/android/newsstand/media/VideoItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .line 183
    .restart local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    goto/16 :goto_2

    .line 188
    .end local v0    # "attachmentId":Ljava/lang/String;
    :cond_b
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasAudio()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 189
    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    .end local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$offset:I

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$fieldId:Ljava/lang/String;

    invoke-direct {v2, v6, v7, v9, v5}, Lcom/google/apps/dots/android/newsstand/media/AudioItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .restart local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    goto/16 :goto_2

    .line 190
    :cond_c
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v6

    if-nez v6, :cond_d

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v6

    if-eqz v6, :cond_e

    .line 191
    :cond_d
    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    .end local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$offset:I

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$fieldId:Ljava/lang/String;

    invoke-direct {v2, v6, v7, v9, v5}, Lcom/google/apps/dots/android/newsstand/media/VideoItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .restart local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    goto/16 :goto_2

    .line 193
    :cond_e
    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .end local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$offset:I

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$fieldId:Ljava/lang/String;

    invoke-direct {v2, v6, v7, v9, v5}, Lcom/google/apps/dots/android/newsstand/media/MediaItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .line 195
    .restart local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    goto/16 :goto_2

    .line 208
    .end local v1    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .end local v5    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_f
    # getter for: Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v6

    const-string v7, "openDrawer - invalid fieldId: %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->val$fieldId:Ljava/lang/String;

    aput-object v10, v9, v8

    invoke-virtual {v6, v7, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 148
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
