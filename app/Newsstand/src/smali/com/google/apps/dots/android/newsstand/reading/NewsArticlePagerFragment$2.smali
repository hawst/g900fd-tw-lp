.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$2;
.super Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
.source "NewsArticlePagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->setupAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Landroid/support/v4/app/FragmentManager;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;
    .param p2, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;
    .param p3, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-void
.end method


# virtual methods
.method public createFragment(ILcom/google/android/libraries/bind/data/Data;)Landroid/support/v4/app/Fragment;
    .locals 13
    .param p1, "logicalPosition"    # I
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 208
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_VIEW_TYPE:I

    invoke-virtual {p2, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;

    .line 209
    .local v8, "viewType":Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;
    sget-object v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;->ARTICLE:Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;

    if-ne v8, v9, :cond_0

    .line 210
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ID:I

    invoke-virtual {p2, v9}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v3

    .line 211
    .local v3, "postId":Ljava/lang/String;
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ORIGINAL_EDITION:I

    invoke-virtual {p2, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 212
    .local v2, "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_INDEX:I

    invoke-virtual {p2, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 214
    .local v4, "postIndex":Ljava/lang/Integer;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .line 215
    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v9, v9, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v1, v3, v9, v2, v4}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Integer;)V

    .line 216
    .local v1, "fragmentState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;-><init>()V

    .line 217
    .local v0, "fragment":Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .line 218
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$000(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->getArticleEventDelegate()Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v9

    .line 217
    invoke-virtual {v0, v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setHidingActionBarArticleEventDelegate(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;)V

    .line 219
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setInitialState(Landroid/os/Parcelable;)V

    .line 231
    .end local v0    # "fragment":Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;
    .end local v1    # "fragmentState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
    .end local v2    # "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v3    # "postId":Ljava/lang/String;
    .end local v4    # "postIndex":Ljava/lang/Integer;
    :goto_0
    return-object v0

    .line 221
    :cond_0
    sget-object v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;->LINK_VIEW:Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;

    if-ne v8, v9, :cond_1

    .line 222
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_POST_TITLE:I

    invoke-virtual {p2, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 223
    .local v5, "postTitle":Ljava/lang/String;
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_POST_URL:I

    invoke-virtual {p2, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 224
    .local v6, "postUrl":Ljava/lang/String;
    sget v9, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_NAME:I

    invoke-virtual {p2, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 225
    .local v7, "publisher":Ljava/lang/String;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .line 226
    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v9, v9, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v1, v6, v5, v9, v7}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    .line 227
    .local v1, "fragmentState":Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;-><init>()V

    .line 228
    .local v0, "fragment":Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .line 229
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$000(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->getArticleEventDelegate()Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v9

    .line 228
    invoke-virtual {v0, v9}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->setHidingActionBarArticleEventDelegate(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;)V

    .line 230
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->setInitialState(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 233
    .end local v0    # "fragment":Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;
    .end local v1    # "fragmentState":Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;
    .end local v5    # "postTitle":Ljava/lang/String;
    .end local v6    # "postUrl":Ljava/lang/String;
    .end local v7    # "publisher":Ljava/lang/String;
    :cond_1
    new-instance v9, Ljava/lang/IllegalStateException;

    sget-object v10, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v11, "Invalid ViewType found."

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    .line 234
    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;
    .param p2, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 240
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 246
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$2;->fragments:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 247
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    if-eqz v2, :cond_1

    .line 248
    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .line 249
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$000(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->getArticleEventDelegate()Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v2

    .line 248
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setHidingActionBarArticleEventDelegate(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;)V

    goto :goto_0

    .line 250
    .restart local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_1
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;

    if-eqz v2, :cond_0

    .line 251
    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .line 252
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$000(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->getArticleEventDelegate()Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v2

    .line 251
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->setHidingActionBarArticleEventDelegate(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;)V

    goto :goto_0

    .line 255
    :cond_2
    return-void
.end method
