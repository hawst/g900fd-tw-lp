.class public Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;
.super Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;
.source "ExploreSingleTopicList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;,
        Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;
    }
.end annotation


# static fields
.field public static final DK_SHELF_TYPE:I


# instance fields
.field private final expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

.field private final topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ExploreSingleTopicList_shelfType:I

    sput v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->DK_SHELF_TYPE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "topic"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    .param p3, "expandedShelfType"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;-><init>(Landroid/content/Context;)V

    .line 66
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 67
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->appContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->appContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->appContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    return-object v0
.end method

.method public static getServerUriForExploreTopic(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "topic"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .prologue
    .line 77
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->isInternationalCurationsTopic()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->topicId:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getExploreInternationalCurations(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    .line 79
    :cond_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->featuredTopic(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getExploreFeatured(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 82
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->topicId:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getExploreSingleTopic(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->appContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-static {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->getServerUriForExploreTopic(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected visitor(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
    .locals 6
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p3, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 89
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->appContext:Landroid/content/Context;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    return-object v0
.end method
