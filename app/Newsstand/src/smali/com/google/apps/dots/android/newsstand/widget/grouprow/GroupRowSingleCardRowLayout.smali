.class public Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;
.super Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;
.source "GroupRowSingleCardRowLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method private updateShouldShrinkAndEnlarge()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 48
    const/4 v1, 0x1

    .line 49
    .local v1, "childCanShrink":Z
    const/4 v0, 0x1

    .line 51
    .local v0, "childCanEnlarge":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 52
    invoke-static {p0, v3}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getContainedCardAt(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v2

    .line 53
    .local v2, "childView":Landroid/view/View;
    if-eqz v2, :cond_2

    instance-of v4, v2, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;

    if-eqz v4, :cond_2

    .line 54
    if-eqz v1, :cond_0

    move-object v4, v2

    check-cast v4, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;->canShrink()Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v5

    .line 55
    :goto_1
    if-eqz v0, :cond_1

    check-cast v2, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;

    .end local v2    # "childView":Landroid/view/View;
    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;->canEnlarge()Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v5

    .line 51
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .restart local v2    # "childView":Landroid/view/View;
    :cond_0
    move v1, v6

    .line 54
    goto :goto_1

    .end local v2    # "childView":Landroid/view/View;
    :cond_1
    move v0, v6

    .line 55
    goto :goto_2

    .line 58
    .restart local v2    # "childView":Landroid/view/View;
    :cond_2
    const/4 v1, 0x0

    .line 59
    const/4 v0, 0x0

    .line 63
    .end local v2    # "childView":Landroid/view/View;
    :cond_3
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;->shouldShrink:Z

    .line 64
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;->shouldEnlarge:Z

    .line 65
    return-void
.end method


# virtual methods
.method protected getDesiredHeightMeasureSpec(I)I
    .locals 3
    .param p1, "desiredRowHeight"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;->updateShouldShrinkAndEnlarge()V

    .line 38
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;->shouldShrink()Z

    move-result v1

    if-eqz v1, :cond_0

    const/high16 v1, -0x80000000

    .line 39
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 44
    .local v0, "returnSpec":I
    :goto_0
    return v0

    .line 39
    .end local v0    # "returnSpec":I
    :cond_0
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;->shouldEnlarge:Z

    if-eqz v1, :cond_1

    .line 41
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0

    :cond_1
    const/high16 v1, 0x40000000    # 2.0f

    .line 43
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method protected getNumCards()I
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    return v0
.end method

.method public shouldEnlarge()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;->shouldEnlarge:Z

    return v0
.end method

.method public shouldShrink()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;->shouldShrink:Z

    return v0
.end method
