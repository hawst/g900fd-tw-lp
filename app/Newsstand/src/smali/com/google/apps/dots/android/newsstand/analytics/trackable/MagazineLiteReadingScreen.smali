.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;
.source "MagazineLiteReadingScreen.java"


# instance fields
.field private final page:I

.field private final postId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;I)V
    .locals 0
    .param p1, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "postId"    # Ljava/lang/String;
    .param p3, "page"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 24
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->page:I

    .line 25
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->postId:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 79
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 80
    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;

    .line 81
    .local v0, "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->postId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->postId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->page:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->page:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 84
    .end local v0    # "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;
    :cond_0
    return v1
.end method

.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 4
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 44
    .local v0, "originalEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->postId:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->getPostSummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v1

    .line 46
    .local v1, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->postId:Ljava/lang/String;

    .line 47
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 48
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 49
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 50
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 51
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->page:I

    .line 52
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPage(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 53
    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setUserSubscriptionType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 54
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 56
    const-string v2, "LiteMode"

    const/4 v3, 0x1

    .line 57
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    .line 56
    invoke-static {p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-object p1
.end method

.method protected getDedupeExpiryTime()J
    .locals 2

    .prologue
    .line 69
    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 31
    .local v0, "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->postId:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->getPostSummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v1

    .line 33
    .local v1, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    const/4 v2, 0x0

    check-cast v2, Ljava/util/Locale;

    const-string v3, "%s %s - %s - %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "[Magazine - Lite]"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 35
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 36
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    .line 37
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 33
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 64
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->postId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;->page:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isDedupable()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method
