.class Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder$1;
.super Ljava/lang/Object;
.source "WatchTrailerIntentBuilder.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;

.field final synthetic val$nsActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field final synthetic val$prevOrientation:I

.field final synthetic val$prevOrientationDegrees:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;II)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder$1;->this$0:Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder$1;->val$nsActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder$1;->val$prevOrientation:I

    iput p4, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder$1;->val$prevOrientationDegrees:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 76
    # getter for: Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "onActivityResult"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder$1;->val$nsActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder$1;->val$nsActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder$1;->val$prevOrientation:I

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder$1;->val$prevOrientationDegrees:I

    invoke-direct {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/UnlockOrientationListener;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;II)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setOrientationListener(Landroid/view/OrientationEventListener;)V

    .line 81
    return-void
.end method
