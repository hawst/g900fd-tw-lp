.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;
.source "OnboardTutorialScreen.java"


# instance fields
.field private final page:I


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 19
    const-string v0, "Onboard-Tutorial"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;->page:I

    .line 21
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 48
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 49
    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;

    .line 50
    .local v0, "screen":Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;->page:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;->page:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 52
    .end local v0    # "screen":Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;
    :cond_0
    return v1
.end method

.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;->fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 27
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;->page:I

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPage(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 28
    return-object p1
.end method

.method protected getDedupeExpiryTime()J
    .locals 2

    .prologue
    .line 38
    const-wide/16 v0, 0x7530

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 43
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;->page:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isDedupable()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    return v0
.end method
