.class public final enum Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;
.super Ljava/lang/Enum;
.source "SyncFailureNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SyncFailedReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

.field public static final enum OUT_OF_STORAGE:Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;


# instance fields
.field frequencyOfNotificationInMillis:J

.field prefKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    const-string v1, "OUT_OF_STORAGE"

    const-string v3, "syncFailureNoStorageNotificationTime"

    const-wide/32 v4, 0x2932e00

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;-><init>(Ljava/lang/String;ILjava/lang/String;J)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->OUT_OF_STORAGE:Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->OUT_OF_STORAGE:Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->$VALUES:[Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;J)V
    .locals 0
    .param p3, "prefKey"    # Ljava/lang/String;
    .param p4, "frequencyOfNotificationInMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->prefKey:Ljava/lang/String;

    .line 33
    iput-wide p4, p0, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->frequencyOfNotificationInMillis:J

    .line 34
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->$VALUES:[Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    return-object v0
.end method
