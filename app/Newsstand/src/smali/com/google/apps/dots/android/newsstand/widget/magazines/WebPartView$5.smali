.class Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;
.super Landroid/webkit/WebViewClient;
.source "WebPartView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->initWebViewClient()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 304
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    const/4 v1, 0x6

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->postMethod(I)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$1000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;I)V

    .line 306
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 297
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 298
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    const/4 v1, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reportedSnapPoints:[I
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$902(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;[I)[I

    .line 299
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    const/4 v1, 0x3

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->postMethod(I)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$1000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;I)V

    .line 300
    return-void
.end method

.method public onScaleChanged(Landroid/webkit/WebView;FF)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "oldScale"    # F
    .param p3, "newScale"    # F

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->currentScale:F
    invoke-static {v0, p3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$402(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;F)F

    .line 314
    return-void
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 319
    if-eqz p2, :cond_0

    const-string v0, "file"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    new-instance v0, Landroid/webkit/WebResourceResponse;

    invoke-direct {v0, v2, v2, v2}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    .line 322
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 9
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 328
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->qualifiedMainResourceUri:Landroid/net/Uri;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$1100(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 370
    :cond_0
    :goto_0
    return v3

    .line 331
    :cond_1
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 332
    .local v2, "uri":Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getActivityFromView(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    .line 335
    .local v0, "activity":Landroid/app/Activity;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->localBaseUri:Landroid/net/Uri;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->localBaseUri:Landroid/net/Uri;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 336
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->handleLocalUrls:Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$1300(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 339
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;

    invoke-direct {v3, v0}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .line 340
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->appId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$1700(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .line 341
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->sectionId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$1600(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .line 342
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->postId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$1500(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .line 343
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->fieldId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$1400(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->setFieldId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;

    move-result-object v3

    .line 344
    invoke-virtual {v3, p2}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->setLocalUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;

    move-result-object v3

    .line 345
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->start()V

    move v3, v4

    .line 346
    goto :goto_0

    .line 350
    :cond_2
    const-string v5, "navto"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 351
    new-instance v5, Landroid/net/Uri$Builder;

    invoke-direct {v5}, Landroid/net/Uri$Builder;-><init>()V

    sget-object v6, Lcom/google/apps/dots/shared/EventCode;->SYSTEM_DO_NAV_TO_PAGE:Lcom/google/apps/dots/shared/EventCode;

    new-array v3, v3, [Ljava/lang/Object;

    .line 352
    invoke-virtual {v6, v3}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "post"

    .line 354
    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v7, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 353
    :goto_1
    invoke-virtual {v5, v6, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v5, "page"

    .line 355
    invoke-virtual {v2}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 356
    .local v1, "builder":Landroid/net/Uri$Builder;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$1800(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v3

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->dispatch(Ljava/lang/String;)V

    move v3, v4

    .line 357
    goto/16 :goto_0

    .line 354
    .end local v1    # "builder":Landroid/net/Uri$Builder;
    :cond_3
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 360
    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->recentInteraction:Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 362
    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->showInBrowser(Landroid/app/Activity;Landroid/net/Uri;)Z

    :cond_5
    move v3, v4

    .line 370
    goto/16 :goto_0

    .line 366
    :cond_6
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->isHttp(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto/16 :goto_0
.end method
