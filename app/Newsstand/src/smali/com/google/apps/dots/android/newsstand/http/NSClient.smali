.class public Lcom/google/apps/dots/android/newsstand/http/NSClient;
.super Ljava/lang/Object;
.source "NSClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;,
        Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;,
        Lcom/google/apps/dots/android/newsstand/http/NSClient$WebViewRequest;,
        Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    }
.end annotation


# static fields
.field private static final DESIGNER_MODE_PARAM:Ljava/lang/String; = "designerMode"

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final clientPool:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;",
            ">;>;"
        }
    .end annotation
.end field

.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field private final serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/apps/dots/android/newsstand/http/NSClient;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/http/NSClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V
    .locals 1
    .param p1, "dotsUris"    # Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .param p2, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 147
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient;->clientPool:Ljava/util/Map;

    .line 148
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 149
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/http/NSClient;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSClient;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "x2"    # Ljava/util/concurrent/Callable;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/http/NSClient;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSClient;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient;->clientPool:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/apps/dots/android/newsstand/http/NSClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/http/NSClient;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSClient;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    return-object v0
.end method

.method private submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 196
    .local p2, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSCLIENT_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-virtual {p1, v0, p2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->submit(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getHttpClient(Landroid/accounts/Account;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient;->clientPool:Ljava/util/Map;

    monitor-enter v2

    .line 201
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient;->clientPool:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 202
    .local v0, "clientFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;>;"
    if-nez v0, :cond_0

    .line 203
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    .line 204
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    .line 203
    invoke-static {v1, v3, v4}, Lcom/google/apps/dots/android/newsstand/http/HttpModule;->provideHttpClient(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 205
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient;->clientPool:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    new-instance v1, Lcom/google/apps/dots/android/newsstand/http/NSClient$2;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/http/NSClient$2;-><init>(Lcom/google/apps/dots/android/newsstand/http/NSClient;Landroid/accounts/Account;)V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 218
    :cond_0
    monitor-exit v2

    return-object v0

    .line 219
    .end local v0    # "clientFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public request(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "clientRequest"    # Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->request(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public request(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "clientRequest"    # Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    .param p3, "requireConfigForDotsUris"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 164
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    iget-object v5, p2, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->uri:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isDotsBackend(Ljava/lang/String;)Z

    move-result v3

    .line 165
    .local v3, "isDotsBackend":Z
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->getHttpClient(Landroid/accounts/Account;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 166
    .local v2, "httpClientFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;>;"
    if-eqz p3, :cond_0

    if-eqz v3, :cond_0

    .line 167
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v4

    invoke-virtual {v4, p1, v6}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getCachedOrFreshConfigFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 170
    .local v1, "clientConfigFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v2, v4, v6

    const/4 v5, 0x1

    aput-object v1, v4, v5

    .line 171
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 173
    .local v0, "bothFutures":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/util/List<Ljava/lang/Object;>;>;"
    new-instance v4, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;

    invoke-direct {v4, p0, p1, p2, v3}, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;-><init>(Lcom/google/apps/dots/android/newsstand/http/NSClient;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;Z)V

    invoke-static {v0, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    return-object v4

    .line 167
    .end local v0    # "bothFutures":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/util/List<Ljava/lang/Object;>;>;"
    .end local v1    # "clientConfigFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    :cond_0
    const/4 v4, 0x0

    .line 168
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method

.method public request(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;[B)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "optPostData"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            "[B)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 186
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    move-object v1, p2

    move-object v2, p3

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;-><init>(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;Ljava/util/Locale;)V

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->request(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public requestAsWebview(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "userAgent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$WebViewRequest;

    invoke-direct {v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/http/NSClient$WebViewRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->request(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
