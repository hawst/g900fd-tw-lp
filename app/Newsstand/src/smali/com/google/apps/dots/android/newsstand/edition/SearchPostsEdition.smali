.class public Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;
.super Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
.source "SearchPostsEdition.java"


# direct methods
.method constructor <init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V
    .locals 0
    .param p1, "editionMessage"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 25
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 28
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    const/16 v1, 0x8

    .line 29
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setType(I)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;-><init>()V

    .line 30
    invoke-virtual {v1, p1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;->setQuery(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setSearchPosts(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    .line 28
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 31
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    return-void
.end method


# virtual methods
.method public editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 74
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->searchPostsEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 36
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    .line 38
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 40
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition$1;

    invoke-direct {v0, p0, p1, p0}, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getSearchPosts()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;->getQuery()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 54
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getPostSearchResults(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public showKeepOnDeviceUi()Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public showOnDeviceOnlyUi()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public supportsReadStates()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public supportsSubscription()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;->readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method
