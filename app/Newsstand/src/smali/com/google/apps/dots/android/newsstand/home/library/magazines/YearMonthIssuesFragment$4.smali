.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;
.super Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;
.source "YearMonthIssuesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private originalTitleColor:I

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method protected cardClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 301
    const-class v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;

    return-object v0
.end method

.method protected bridge synthetic handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 249
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 286
    invoke-virtual {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->titleInSharedElements(Ljava/util/List;Ljava/util/List;)Landroid/widget/TextView;

    move-result-object v0

    .line 287
    .local v0, "titleView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 288
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->originalTitleColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 292
    :goto_0
    return-void

    .line 290
    :cond_0
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Unable to find title during transition"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic handleEnterSetSharedElementEnd(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 249
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected bridge synthetic handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 249
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 265
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 270
    invoke-virtual {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->titleInSharedElements(Ljava/util/List;Ljava/util/List;)Landroid/widget/TextView;

    move-result-object v1

    .line 271
    .local v1, "titleView":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 272
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->shelf_header_title_color:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 273
    .local v0, "shelfHeaderColor":I
    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->originalTitleColor:I

    .line 274
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 278
    .end local v0    # "shelfHeaderColor":I
    :goto_0
    return-void

    .line 276
    :cond_0
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Unable to find title during transition"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 249
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected bridge synthetic headerListLayout(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 249
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->headerListLayout(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    return-object v0
.end method

.method protected headerListLayout(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V
    .locals 0

    .prologue
    .line 249
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)V

    return-void
.end method

.method protected prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    .prologue
    .line 255
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    .line 257
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->postponeEnterTransition()V

    .line 258
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->access$000(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setShuffleOnFirstLoad(Z)V

    .line 259
    return-void
.end method

.method protected bridge synthetic prepareViewsForTransition(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 249
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$4;->prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;)V

    return-void
.end method
