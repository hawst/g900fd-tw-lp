.class public final enum Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;
.super Ljava/lang/Enum;
.source "ProtoEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/util/ProtoEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SubscriptionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

.field public static final enum FREE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

.field public static final enum NOT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

.field public static final enum PAID:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

.field public static final enum UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;


# instance fields
.field public final subscriptionType:Ljava/lang/String;

.field public final subscriptionTypeValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 285
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    const-string v1, "PAID"

    const-string v2, "Paid"

    invoke-direct {v0, v1, v6, v7, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->PAID:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    .line 286
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    const-string v1, "FREE"

    const-string v2, "Free"

    invoke-direct {v0, v1, v3, v5, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->FREE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    .line 287
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    const-string v1, "NOT_SUBSCRIBED"

    const-string v2, "Not Subscribed"

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->NOT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    .line 288
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    const-string v1, "UNKNOWN"

    const-string v2, "Unknown"

    invoke-direct {v0, v1, v5, v3, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    .line 283
    new-array v0, v7, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->PAID:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->FREE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->NOT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p3, "subscriptionTypeValue"    # I
    .param p4, "subscriptionType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 302
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 303
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->subscriptionType:Ljava/lang/String;

    .line 304
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->subscriptionTypeValue:I

    .line 305
    return-void
.end method

.method public static fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;
    .locals 5
    .param p0, "subscriptionTypeValue"    # I

    .prologue
    .line 294
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 295
    .local v0, "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;
    iget v4, v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->subscriptionTypeValue:I

    if-ne v4, p0, :cond_0

    .line 299
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;
    :goto_1
    return-object v0

    .line 294
    .restart local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 299
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 283
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;
    .locals 1

    .prologue
    .line 283
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    return-object v0
.end method
