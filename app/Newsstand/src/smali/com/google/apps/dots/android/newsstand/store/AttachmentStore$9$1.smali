.class Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9$1;
.super Ljava/lang/Object;
.source "AttachmentStore.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->apply(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Form;",
        "Lcom/google/apps/dots/android/newsstand/server/Transform;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;

    .prologue
    .line 537
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/proto/client/DotsShared$Form;)Lcom/google/apps/dots/android/newsstand/server/Transform;
    .locals 3
    .param p1, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .prologue
    .line 542
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->hasPostTemplate()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 543
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->hasPostTemplateId()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .line 544
    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$1100(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->getPostTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getTemplateForDevice(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    .local v0, "template":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    if-eqz v0, :cond_0

    .line 545
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getZoomable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 546
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->zoomTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$800(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v1

    .line 553
    .end local v0    # "template":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    :goto_0
    return-object v1

    .line 547
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->val$attachmentObjectId:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->getPreferOriginalTransformationHint()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 550
    sget-object v1, Lcom/google/apps/dots/android/newsstand/server/Transform;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/server/Transform;

    goto :goto_0

    .line 553
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->defaultTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$1200(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v1

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 537
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9$1;->apply(Lcom/google/apps/dots/proto/client/DotsShared$Form;)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    return-object v0
.end method
