.class public final enum Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;
.super Ljava/lang/Enum;
.source "BasePullDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SnapPoint"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

.field public static final enum DOWN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

.field public static final enum OFFSCREEN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

.field public static final enum UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 76
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    const-string v1, "UP"

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    .line 77
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->DOWN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    .line 78
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    const-string v1, "OFFSCREEN"

    invoke-direct {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->OFFSCREEN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    .line 75
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->DOWN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->OFFSCREEN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->$VALUES:[Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 75
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->$VALUES:[Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    return-object v0
.end method
