.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NativeBodyPart"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;


# instance fields
.field private activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

.field private appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

.field private bitField0_:I

.field public children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

.field public eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

.field private flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

.field private imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

.field private layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

.field private mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

.field private partId_:Ljava/lang/String;

.field private pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

.field private scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

.field private switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

.field private timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

.field private touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

.field private type_:I

.field private webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 529
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 530
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 551
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->partId_:Ljava/lang/String;

    .line 573
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->type_:I

    .line 611
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    .line 633
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 530
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .locals 5

    .prologue
    .line 851
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 855
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    if-eqz v3, :cond_0

    .line 856
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    .line 858
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 859
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    .line 860
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 861
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 862
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    move-result-object v4

    aput-object v4, v3, v2

    .line 860
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 852
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 853
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 866
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    if-eqz v3, :cond_3

    .line 867
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    .line 869
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 870
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 871
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v3, v3

    if-ge v2, v3, :cond_5

    .line 872
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    aget-object v3, v3, v2

    if-eqz v3, :cond_4

    .line 873
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    move-result-object v4

    aput-object v4, v3, v2

    .line 871
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 877
    .end local v2    # "i":I
    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    if-eqz v3, :cond_6

    .line 878
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    .line 880
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    if-eqz v3, :cond_7

    .line 881
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    .line 883
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    if-eqz v3, :cond_8

    .line 884
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    .line 886
    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    if-eqz v3, :cond_9

    .line 887
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    .line 889
    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    if-eqz v3, :cond_a

    .line 890
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    .line 892
    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    if-eqz v3, :cond_b

    .line 893
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    .line 895
    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    if-eqz v3, :cond_c

    .line 896
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    .line 898
    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    if-eqz v3, :cond_d

    .line 899
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    .line 901
    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    if-eqz v3, :cond_e

    .line 902
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    .line 904
    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    if-eqz v3, :cond_f

    .line 905
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    .line 907
    :cond_f
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 526
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 912
    if-ne p1, p0, :cond_1

    .line 930
    :cond_0
    :goto_0
    return v1

    .line 913
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 914
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 915
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->partId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->partId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    if-nez v3, :cond_3

    .line 917
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    .line 918
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    if-nez v3, :cond_3

    .line 919
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 920
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    if-nez v3, :cond_3

    .line 921
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    if-nez v3, :cond_3

    .line 922
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    if-nez v3, :cond_3

    .line 923
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    if-nez v3, :cond_3

    .line 924
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    if-nez v3, :cond_3

    .line 925
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    if-nez v3, :cond_3

    .line 926
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    if-nez v3, :cond_3

    .line 927
    :goto_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    if-nez v3, :cond_3

    .line 928
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    if-nez v3, :cond_3

    .line 929
    :goto_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 930
    goto/16 :goto_0

    .line 915
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->partId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->partId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    .line 917
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    .line 918
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    .line 919
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    .line 920
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    .line 921
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    .line 922
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    .line 923
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    .line 924
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    .line 925
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    .line 926
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    .line 927
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    .line 928
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    .line 929
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    .line 930
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0
.end method

.method public getActivatorDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;
    .locals 1

    .prologue
    .line 809
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    return-object v0
.end method

.method public getAppearance()Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    return-object v0
.end method

.method public getFlipperDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    return-object v0
.end method

.method public getImageDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;
    .locals 1

    .prologue
    .line 657
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    return-object v0
.end method

.method public getLayoutDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    return-object v0
.end method

.method public getMediaDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    return-object v0
.end method

.method public getPartId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->partId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPdfDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    return-object v0
.end method

.method public getScrollDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;
    .locals 1

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1027
    const/4 v1, 0x0

    .line 1028
    .local v1, "size":I
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 1029
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->partId_:Ljava/lang/String;

    .line 1030
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1032
    :cond_0
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_1

    .line 1033
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->type_:I

    .line 1034
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 1036
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-eqz v3, :cond_3

    .line 1037
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v0, v4, v3

    .line 1038
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    if-eqz v0, :cond_2

    .line 1039
    const/4 v6, 0x4

    .line 1040
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 1037
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1044
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    if-eqz v3, :cond_4

    .line 1045
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    .line 1046
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1048
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    if-eqz v3, :cond_5

    .line 1049
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    .line 1050
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1052
    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    if-eqz v3, :cond_6

    .line 1053
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    .line 1054
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1056
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    if-eqz v3, :cond_7

    .line 1057
    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    .line 1058
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1060
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    if-eqz v3, :cond_8

    .line 1061
    const/16 v3, 0xa

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    .line 1062
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1064
    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    if-eqz v3, :cond_a

    .line 1065
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    array-length v4, v3

    :goto_1
    if-ge v2, v4, :cond_a

    aget-object v0, v3, v2

    .line 1066
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    if-eqz v0, :cond_9

    .line 1067
    const/16 v5, 0xb

    .line 1068
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1065
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1072
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    if-eqz v2, :cond_b

    .line 1073
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    .line 1074
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1076
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    if-eqz v2, :cond_c

    .line 1077
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    .line 1078
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1080
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    if-eqz v2, :cond_d

    .line 1081
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    .line 1082
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1084
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    if-eqz v2, :cond_e

    .line 1085
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    .line 1086
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1088
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    if-eqz v2, :cond_f

    .line 1089
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    .line 1090
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1092
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    if-eqz v2, :cond_10

    .line 1093
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    .line 1094
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1096
    :cond_10
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    if-eqz v2, :cond_11

    .line 1097
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    .line 1098
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1100
    :cond_11
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->cachedSize:I

    .line 1101
    return v1
.end method

.method public getSwitchDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;
    .locals 1

    .prologue
    .line 733
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    return-object v0
.end method

.method public getTimerDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;
    .locals 1

    .prologue
    .line 771
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    return-object v0
.end method

.method public getTouchDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 575
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->type_:I

    return v0
.end method

.method public getWebDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    return-object v0
.end method

.method public hasActivatorDetails()Z
    .locals 1

    .prologue
    .line 819
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFlipperDetails()Z
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLayoutDetails()Z
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasScrollDetails()Z
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTouchDetails()Z
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 1

    .prologue
    .line 583
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 934
    const/16 v1, 0x11

    .line 935
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 936
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->partId_:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 937
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->type_:I

    add-int v1, v2, v4

    .line 938
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    if-nez v2, :cond_3

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 939
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    if-nez v2, :cond_4

    mul-int/lit8 v1, v1, 0x1f

    .line 945
    :cond_0
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    if-nez v2, :cond_6

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 946
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-nez v2, :cond_7

    mul-int/lit8 v1, v1, 0x1f

    .line 952
    :cond_1
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    if-nez v2, :cond_9

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 953
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    if-nez v2, :cond_a

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 954
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    if-nez v2, :cond_b

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 955
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    if-nez v2, :cond_c

    move v2, v3

    :goto_6
    add-int v1, v4, v2

    .line 956
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    if-nez v2, :cond_d

    move v2, v3

    :goto_7
    add-int v1, v4, v2

    .line 957
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    if-nez v2, :cond_e

    move v2, v3

    :goto_8
    add-int v1, v4, v2

    .line 958
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    if-nez v2, :cond_f

    move v2, v3

    :goto_9
    add-int v1, v4, v2

    .line 959
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    if-nez v2, :cond_10

    move v2, v3

    :goto_a
    add-int v1, v4, v2

    .line 960
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    if-nez v2, :cond_11

    move v2, v3

    :goto_b
    add-int v1, v4, v2

    .line 961
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    if-nez v4, :cond_12

    :goto_c
    add-int v1, v2, v3

    .line 962
    return v1

    .line 936
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->partId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 938
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->hashCode()I

    move-result v2

    goto :goto_1

    .line 941
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 942
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v3

    :goto_e
    add-int v1, v4, v2

    .line 941
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 942
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->hashCode()I

    move-result v2

    goto :goto_e

    .line 945
    .end local v0    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 948
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 949
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    aget-object v2, v2, v0

    if-nez v2, :cond_8

    move v2, v3

    :goto_10
    add-int v1, v4, v2

    .line 948
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 949
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->hashCode()I

    move-result v2

    goto :goto_10

    .line 952
    .end local v0    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 953
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 954
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 955
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 956
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 957
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 958
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->hashCode()I

    move-result v2

    goto/16 :goto_9

    .line 959
    :cond_10
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->hashCode()I

    move-result v2

    goto/16 :goto_a

    .line 960
    :cond_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->hashCode()I

    move-result v2

    goto/16 :goto_b

    .line 961
    :cond_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->hashCode()I

    move-result v3

    goto/16 :goto_c
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1109
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1110
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1114
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1115
    :sswitch_0
    return-object p0

    .line 1120
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->partId_:Ljava/lang/String;

    .line 1121
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->bitField0_:I

    goto :goto_0

    .line 1125
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1126
    .local v4, "temp":I
    if-eqz v4, :cond_1

    const/4 v6, 0x1

    if-eq v4, v6, :cond_1

    const/4 v6, 0x2

    if-eq v4, v6, :cond_1

    const/4 v6, 0x3

    if-eq v4, v6, :cond_1

    const/4 v6, 0x4

    if-eq v4, v6, :cond_1

    const/4 v6, 0x5

    if-eq v4, v6, :cond_1

    const/4 v6, 0x6

    if-eq v4, v6, :cond_1

    const/4 v6, 0x7

    if-eq v4, v6, :cond_1

    const/16 v6, 0x8

    if-eq v4, v6, :cond_1

    const/16 v6, 0x9

    if-eq v4, v6, :cond_1

    const/16 v6, 0xa

    if-eq v4, v6, :cond_1

    const/16 v6, 0xb

    if-eq v4, v6, :cond_1

    const/16 v6, 0xc

    if-ne v4, v6, :cond_2

    .line 1139
    :cond_1
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->type_:I

    .line 1140
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->bitField0_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->bitField0_:I

    goto :goto_0

    .line 1142
    :cond_2
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->type_:I

    goto :goto_0

    .line 1147
    .end local v4    # "temp":I
    :sswitch_3
    const/16 v6, 0x22

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1148
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-nez v6, :cond_4

    move v1, v5

    .line 1149
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 1150
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-eqz v6, :cond_3

    .line 1151
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1153
    :cond_3
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 1154
    :goto_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_5

    .line 1155
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;-><init>()V

    aput-object v7, v6, v1

    .line 1156
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1157
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1154
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1148
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    :cond_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v1, v6

    goto :goto_1

    .line 1160
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    :cond_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;-><init>()V

    aput-object v7, v6, v1

    .line 1161
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1165
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    :sswitch_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    if-nez v6, :cond_6

    .line 1166
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    .line 1168
    :cond_6
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1172
    :sswitch_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    if-nez v6, :cond_7

    .line 1173
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    .line 1175
    :cond_7
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1179
    :sswitch_6
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    if-nez v6, :cond_8

    .line 1180
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    .line 1182
    :cond_8
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1186
    :sswitch_7
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    if-nez v6, :cond_9

    .line 1187
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    .line 1189
    :cond_9
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1193
    :sswitch_8
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    if-nez v6, :cond_a

    .line 1194
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    .line 1196
    :cond_a
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1200
    :sswitch_9
    const/16 v6, 0x5a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1201
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    if-nez v6, :cond_c

    move v1, v5

    .line 1202
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    .line 1203
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    if-eqz v6, :cond_b

    .line 1204
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1206
    :cond_b
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    .line 1207
    :goto_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_d

    .line 1208
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;-><init>()V

    aput-object v7, v6, v1

    .line 1209
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1210
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1207
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1201
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    :cond_c
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    array-length v1, v6

    goto :goto_3

    .line 1213
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    :cond_d
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;-><init>()V

    aput-object v7, v6, v1

    .line 1214
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1218
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    :sswitch_a
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    if-nez v6, :cond_e

    .line 1219
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    .line 1221
    :cond_e
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1225
    :sswitch_b
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    if-nez v6, :cond_f

    .line 1226
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    .line 1228
    :cond_f
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1232
    :sswitch_c
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    if-nez v6, :cond_10

    .line 1233
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    .line 1235
    :cond_10
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1239
    :sswitch_d
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    if-nez v6, :cond_11

    .line 1240
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    .line 1242
    :cond_11
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1246
    :sswitch_e
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    if-nez v6, :cond_12

    .line 1247
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    .line 1249
    :cond_12
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1253
    :sswitch_f
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    if-nez v6, :cond_13

    .line 1254
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    .line 1256
    :cond_13
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1260
    :sswitch_10
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    if-nez v6, :cond_14

    .line 1261
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    .line 1263
    :cond_14
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1110
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x7a -> :sswitch_d
        0x82 -> :sswitch_e
        0x8a -> :sswitch_f
        0x92 -> :sswitch_10
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 526
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    move-result-object v0

    return-object v0
.end method

.method public setLayoutDetails(Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;)Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    .prologue
    .line 597
    if-nez p1, :cond_0

    .line 598
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 600
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    .line 601
    return-object p0
.end method

.method public setWebDetails(Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;)Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    .prologue
    .line 698
    if-nez p1, :cond_0

    .line 699
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 701
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    .line 702
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 967
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 968
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->partId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 970
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 971
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->type_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 973
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-eqz v2, :cond_3

    .line 974
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 975
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    if-eqz v0, :cond_2

    .line 976
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 974
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 980
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    if-eqz v2, :cond_4

    .line 981
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->scrollDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 983
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    if-eqz v2, :cond_5

    .line 984
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->imageDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 986
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    if-eqz v2, :cond_6

    .line 987
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->layoutDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 989
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    if-eqz v2, :cond_7

    .line 990
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->mediaDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 992
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    if-eqz v2, :cond_8

    .line 993
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->webDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 995
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    if-eqz v2, :cond_a

    .line 996
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_a

    aget-object v0, v2, v1

    .line 997
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    if-eqz v0, :cond_9

    .line 998
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 996
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1002
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    :cond_a
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    if-eqz v1, :cond_b

    .line 1003
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->appearance_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1005
    :cond_b
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    if-eqz v1, :cond_c

    .line 1006
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->switchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1008
    :cond_c
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    if-eqz v1, :cond_d

    .line 1009
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->pdfDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1011
    :cond_d
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    if-eqz v1, :cond_e

    .line 1012
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->timerDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1014
    :cond_e
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    if-eqz v1, :cond_f

    .line 1015
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->touchDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1017
    :cond_f
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    if-eqz v1, :cond_10

    .line 1018
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->flipperDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1020
    :cond_10
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    if-eqz v1, :cond_11

    .line 1021
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->activatorDetails_:Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1023
    :cond_11
    return-void
.end method
