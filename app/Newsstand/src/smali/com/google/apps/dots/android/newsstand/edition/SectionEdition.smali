.class public Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;
.super Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
.source "SectionEdition.java"


# instance fields
.field private edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V
    .locals 3
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "sectionId"    # Ljava/lang/String;

    .prologue
    .line 33
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    const/4 v1, 0x4

    .line 34
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setType(I)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;-><init>()V

    .line 36
    invoke-virtual {v1, p2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    move-result-object v1

    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 37
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->setEdition(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setSection(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    .line 33
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 38
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 41
    return-void
.end method

.method constructor <init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V
    .locals 0
    .param p1, "editionProto"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 30
    return-void
.end method


# virtual methods
.method public editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 4
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 111
    .local v0, "parentEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    if-nez v0, :cond_0

    .line 112
    const/4 v1, 0x0

    .line 114
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-direct {v1, p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 45
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .line 47
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 50
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;
    :cond_0
    return v1
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getSection()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->fromProto(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method protected bridge synthetic getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;

    move-result-object v0

    return-object v0
.end method

.method protected getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    invoke-static {p1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->sectionEditionList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;)Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;

    move-result-object v0

    return-object v0
.end method

.method public getSectionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getSection()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->getSectionId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTranslatedEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 2
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 150
    .line 151
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->addOrReplaceTargetTranslationLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "translatedSectionId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getTranslatedEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->sectionEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    move-result-object v1

    return-object v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public readStatesUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    instance-of v0, v0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->readStatesUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 79
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getSectionCollection(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public showKeepOnDeviceUi()Z
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public showOnDeviceOnlyUi()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public supportsReadStates()Z
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->supportsReadStates()Z

    move-result v0

    return v0
.end method

.method public supportsSubscription()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    instance-of v1, v1, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    if-eqz v1, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 87
    .local v0, "syncUris":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    .end local v0    # "syncUris":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 74
    const-string v0, "%s - edition: %s, section: %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public trackAnalytics(I)V
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 145
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SectionEditionScreen;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SectionEditionScreen;-><init>(Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;Ljava/lang/String;I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SectionEditionScreen;->track(Z)V

    .line 146
    return-void
.end method
