.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TouchDetails"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;


# instance fields
.field private allowFling_:Z

.field private bitField0_:I

.field private numTicksHorizontal_:I

.field private numTicksVertical_:I

.field private wrapAround_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2603
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2604
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2609
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksHorizontal_:I

    .line 2628
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksVertical_:I

    .line 2647
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->allowFling_:Z

    .line 2666
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->wrapAround_:Z

    .line 2604
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;
    .locals 3

    .prologue
    .line 2698
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2702
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;
    return-object v0

    .line 2699
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;
    :catch_0
    move-exception v1

    .line 2700
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2600
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2707
    if-ne p1, p0, :cond_1

    .line 2710
    :cond_0
    :goto_0
    return v1

    .line 2708
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2709
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    .line 2710
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksHorizontal_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksHorizontal_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksVertical_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksVertical_:I

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->allowFling_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->allowFling_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->wrapAround_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->wrapAround_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getAllowFling()Z
    .locals 1

    .prologue
    .line 2649
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->allowFling_:Z

    return v0
.end method

.method public getNumTicksHorizontal()I
    .locals 1

    .prologue
    .line 2611
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksHorizontal_:I

    return v0
.end method

.method public getNumTicksVertical()I
    .locals 1

    .prologue
    .line 2630
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksVertical_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2744
    const/4 v0, 0x0

    .line 2745
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2746
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksHorizontal_:I

    .line 2747
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2749
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2750
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksVertical_:I

    .line 2751
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2753
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2754
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->allowFling_:Z

    .line 2755
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2757
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 2758
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->wrapAround_:Z

    .line 2759
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2761
    :cond_3
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->cachedSize:I

    .line 2762
    return v0
.end method

.method public getWrapAround()Z
    .locals 1

    .prologue
    .line 2668
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->wrapAround_:Z

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2717
    const/16 v0, 0x11

    .line 2718
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 2719
    mul-int/lit8 v1, v0, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksHorizontal_:I

    add-int v0, v1, v4

    .line 2720
    mul-int/lit8 v1, v0, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksVertical_:I

    add-int v0, v1, v4

    .line 2721
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->allowFling_:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v4, v1

    .line 2722
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->wrapAround_:Z

    if-eqz v4, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 2723
    return v0

    :cond_0
    move v1, v3

    .line 2721
    goto :goto_0

    :cond_1
    move v2, v3

    .line 2722
    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2770
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2771
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2775
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2776
    :sswitch_0
    return-object p0

    .line 2781
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksHorizontal_:I

    .line 2782
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    goto :goto_0

    .line 2786
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksVertical_:I

    .line 2787
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    goto :goto_0

    .line 2791
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->allowFling_:Z

    .line 2792
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    goto :goto_0

    .line 2796
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->wrapAround_:Z

    .line 2797
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    goto :goto_0

    .line 2771
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2600
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2728
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2729
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksHorizontal_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2731
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2732
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->numTicksVertical_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2734
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2735
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->allowFling_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2737
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 2738
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->wrapAround_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2740
    :cond_3
    return-void
.end method
