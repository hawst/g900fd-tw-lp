.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;
.super Ljava/lang/Object;
.source "NewsArticleFragment.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NewsRenderDelegate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;


# direct methods
.method private constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V
    .locals 0

    .prologue
    .line 609
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;
    .param p2, "x1"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$1;

    .prologue
    .line 609
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    return-void
.end method


# virtual methods
.method public onArticleOverscrolled(IIZZ)V
    .locals 2
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    .line 659
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$1200(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 662
    if-nez p2, :cond_1

    if-eqz p4, :cond_1

    const/4 v0, 0x1

    .line 663
    .local v0, "top":Z
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$1200(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;->onOverscroll(Z)V

    .line 665
    .end local v0    # "top":Z
    :cond_0
    return-void

    .line 662
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onArticleScrolled(IIIZ)V
    .locals 5
    .param p1, "scrollOffset"    # I
    .param p2, "scrollRange"    # I
    .param p3, "scrollDelta"    # I
    .param p4, "userDriven"    # Z

    .prologue
    .line 648
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$1100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "onArticleScrolled - scrollOffset: %d, scrollRange: %d, scrollDelta: %d, userDriven: %b"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 650
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 648
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 651
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$1200(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 652
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$1200(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v0

    invoke-virtual {v0, p3, p1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;->onScroll(II)V

    .line 654
    :cond_0
    return-void
.end method

.method public onArticleUnhandledTouchEvent()V
    .locals 3

    .prologue
    .line 641
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$1100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "onArticleUnhandledClick"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 642
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->toggleActionBar()V

    .line 643
    return-void
.end method

.method public onLayoutChange(III)V
    .locals 0
    .param p1, "pageCount"    # I
    .param p2, "pageWidth"    # I
    .param p3, "pageHeight"    # I

    .prologue
    .line 613
    return-void
.end method

.method public onLayoutComplete()V
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->onLoadFinished()V

    .line 618
    return-void
.end method

.method public onLayoutFailed(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->onLoadFailed(Ljava/lang/Throwable;)V

    .line 630
    return-void
.end method

.method public onPageChanged(I)V
    .locals 4
    .param p1, "page"    # I

    .prologue
    .line 623
    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .line 624
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$1000(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getPostId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;I)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->track(Z)V

    .line 625
    return-void
.end method

.method public onToggleActionBar()V
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->toggleActionBar()V

    .line 670
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 634
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->audioFragment:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->audioFragment:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$400(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->spyOnTouchEvent(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 637
    :cond_0
    return-void
.end method
