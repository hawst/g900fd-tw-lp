.class Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$3;
.super Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;
.source "NewsBridgeResponder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->openAudio(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

.field final synthetic val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field final synthetic val$audioUri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$3;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$3;->val$audioUri:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$3;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 4
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 243
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$3;->val$audioUri:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->findAudioItemFromUri(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    move-result-object v0

    .line 244
    .local v0, "audio":Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    if-eqz v0, :cond_0

    .line 245
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$3;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$3;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    # getter for: Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->access$000(Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;)Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getReadingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$3;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .line 246
    # getter for: Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->access$000(Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;)Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    .line 245
    invoke-static {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->startAudio(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/media/AudioItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 248
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 239
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$3;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
