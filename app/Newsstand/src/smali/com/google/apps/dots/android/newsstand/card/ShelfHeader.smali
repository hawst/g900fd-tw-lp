.class public Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "ShelfHeader.java"


# static fields
.field public static final DK_BUTTON_TEXT:I

.field public static final DK_ON_CLICK_LISTENER:I

.field public static final DK_TITLE:I

.field public static final DK_TITLE_TEXT_COLOR:I

.field public static final DK_TITLE_TRANSITION_NAME:I

.field public static final LAYOUT:I

.field public static final LAYOUT_FIRST:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ShelfHeader_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE:I

    .line 16
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ShelfHeader_titleTextColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE_TEXT_COLOR:I

    .line 17
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ShelfHeader_titleTransitionName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE_TRANSITION_NAME:I

    .line 18
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ShelfHeader_buttonText:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_BUTTON_TEXT:I

    .line 19
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ShelfHeader_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_ON_CLICK_LISTENER:I

    .line 23
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->shelf_header_first:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->LAYOUT_FIRST:I

    .line 26
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->shelf_header:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->LAYOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method
