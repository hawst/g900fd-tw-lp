.class Lcom/google/apps/dots/android/newsstand/media/VideoActivity$2;
.super Lcom/google/apps/dots/android/newsstand/widget/MediaView;
.source "VideoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->setupPlayer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/media/VideoActivity;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 2

    .prologue
    .line 182
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onCompleted()V

    .line 183
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->streamingProgress:I
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->access$402(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;I)I

    .line 184
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->closeOnCompletion:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->access$500(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->finish()V

    .line 187
    :cond_0
    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .prologue
    .line 175
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 176
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->streamingProgress:I
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->access$400(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 177
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->access$100(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    move-result-object v1

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/media/VideoItem;->postId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->markPostAsRead(Ljava/lang/String;)V

    .line 178
    return-void
.end method
