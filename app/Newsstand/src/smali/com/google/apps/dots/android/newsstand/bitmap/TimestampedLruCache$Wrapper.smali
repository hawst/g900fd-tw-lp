.class public Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;
.super Ljava/lang/Object;
.source "TimestampedLruCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Wrapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final timestamp:J

.field public final value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<TV;>;"
    .local p1, "value":Ljava/lang/Object;, "TV;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;-><init>(Ljava/lang/Object;J)V

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;J)V
    .locals 0
    .param p2, "timestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;J)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<TV;>;"
    .local p1, "value":Ljava/lang/Object;, "TV;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;->value:Ljava/lang/Object;

    .line 32
    iput-wide p2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;->timestamp:J

    .line 33
    return-void
.end method
