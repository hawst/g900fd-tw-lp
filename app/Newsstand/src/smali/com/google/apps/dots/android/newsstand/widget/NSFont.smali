.class public final enum Lcom/google/apps/dots/android/newsstand/widget/NSFont;
.super Ljava/lang/Enum;
.source "NSFont.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/widget/NSFont;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/widget/NSFont;

.field public static final enum BOLD_CONDENSED_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

.field public static final enum BOLD_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

.field public static final enum CONDENSED_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

.field public static final enum LIGHT_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

.field public static final enum REGULAR_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

.field public static final enum ROBOTO_SLAB:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

.field public static final enum ROBOTO_SLAB_LIGHT:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

.field public static final enum ROBOTO_SLAB_LIGHT_ITALIC:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

.field public static final enum ROBOTO_SLAB_THIN:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

.field public static final enum THIN_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

.field private static typefaces:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/NSFont;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v1, "LIGHT_SANS"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->LIGHT_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    .line 17
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v1, "THIN_SANS"

    invoke-direct {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->THIN_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    .line 18
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v1, "REGULAR_SANS"

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->REGULAR_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    .line 19
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v1, "CONDENSED_SANS"

    invoke-direct {v0, v1, v6}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->CONDENSED_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    .line 20
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v1, "BOLD_CONDENSED_SANS"

    invoke-direct {v0, v1, v7}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->BOLD_CONDENSED_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    .line 21
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v1, "BOLD_SANS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->BOLD_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    .line 22
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v1, "ROBOTO_SLAB"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v1, "ROBOTO_SLAB_LIGHT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB_LIGHT:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v1, "ROBOTO_SLAB_LIGHT_ITALIC"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB_LIGHT_ITALIC:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    .line 25
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v1, "ROBOTO_SLAB_THIN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB_THIN:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    .line 14
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->LIGHT_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->THIN_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->REGULAR_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->CONDENSED_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->BOLD_CONDENSED_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->BOLD_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB_LIGHT:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB_LIGHT_ITALIC:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB_THIN:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->$VALUES:[Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static ensureLoaded(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    if-eqz v0, :cond_0

    .line 65
    :goto_0
    return-void

    .line 42
    :cond_0
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    invoke-static {v0}, Lcom/google/common/collect/Maps;->newEnumMap(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    .line 45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 46
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->LIGHT_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "sans-serif-light"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->REGULAR_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "sans-serif"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->THIN_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "sans-serif-thin"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->BOLD_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "sans-serif"

    invoke-static {v2, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->CONDENSED_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "sans-serif-condensed"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->BOLD_CONDENSED_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "sans-serif-condensed"

    invoke-static {v2, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    :goto_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "fonts/RobotoSlab-Regular"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->loadTtf(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB_LIGHT:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "fonts/RobotoSlab-Light"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->loadTtf(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB_LIGHT_ITALIC:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "fonts/RobotoSlab-LightItalic"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->loadTtf(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ROBOTO_SLAB_THIN:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "fonts/RobotoSlab-Thin"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->loadTtf(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 53
    :cond_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->LIGHT_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "fonts/Roboto-Light"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->loadTtf(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->REGULAR_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "fonts/Roboto-Regular"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->loadTtf(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->THIN_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "fonts/Roboto-Thin"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->loadTtf(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->BOLD_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "fonts/Roboto-Bold"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->loadTtf(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->CONDENSED_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "fonts/Roboto-Condensed"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->loadTtf(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->BOLD_CONDENSED_SANS:Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    const-string v2, "fonts/Roboto-BoldCondensed"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->loadTtf(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method

.method private static loadTtf(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ttfFilename"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ".ttf"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "fullName":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 70
    .local v1, "typeface":Landroid/graphics/Typeface;
    if-nez v1, :cond_1

    .line 71
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Failed to load ttf font for filename: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_1
    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/NSFont;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/widget/NSFont;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->$VALUES:[Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/widget/NSFont;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    return-object v0
.end method


# virtual methods
.method public getTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->ensureLoaded(Landroid/content/Context;)V

    .line 31
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->typefaces:Ljava/util/EnumMap;

    invoke-virtual {v1, p0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    .line 32
    .local v0, "typeface":Landroid/graphics/Typeface;
    if-nez v0, :cond_1

    .line 33
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "No typeface registered for style: "

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 35
    :cond_1
    return-object v0
.end method
