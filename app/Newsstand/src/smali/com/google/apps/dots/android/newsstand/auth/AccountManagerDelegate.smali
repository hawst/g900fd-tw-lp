.class public Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;
.super Ljava/lang/Object;
.source "AccountManagerDelegate.java"


# instance fields
.field private final manager:Landroid/accounts/AccountManager;


# direct methods
.method public constructor <init>(Landroid/accounts/AccountManager;)V
    .locals 0
    .param p1, "manager"    # Landroid/accounts/AccountManager;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;->manager:Landroid/accounts/AccountManager;

    .line 18
    return-void
.end method


# virtual methods
.method public addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V
    .locals 1
    .param p1, "listener"    # Landroid/accounts/OnAccountsUpdateListener;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "updateImmediately"    # Z

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;->manager:Landroid/accounts/AccountManager;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;->manager:Landroid/accounts/AccountManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 33
    :cond_0
    return-void
.end method

.method public getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;->manager:Landroid/accounts/AccountManager;

    invoke-virtual {v0, p1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public getPreviousName(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;->manager:Landroid/accounts/AccountManager;

    invoke-virtual {v0, p1}, Landroid/accounts/AccountManager;->getPreviousName(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
