.class public Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;
.super Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
.source "ReadNowEdition.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setType(I)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 23
    return-void
.end method


# virtual methods
.method public editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 47
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->READ_NOW:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 27
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    return v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const-string v0, "CAAiCENBa1NBQ2dBUAE"

    return-object v0
.end method

.method protected bridge synthetic getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;->getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    move-result-object v0

    return-object v0
.end method

.method protected getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->readNowList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 37
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getReadNow(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public showKeepOnDeviceUi()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public showOnDeviceOnlyUi()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method

.method protected supportsHeaderImages(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Z
    .locals 1
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method public supportsReadStates()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public supportsSubscription()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;->readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;->readStatesUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method public trackAnalytics(I)V
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 87
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;-><init>(Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;->track(Z)V

    .line 88
    return-void
.end method
