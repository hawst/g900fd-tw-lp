.class public final Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NativeBodySummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;


# instance fields
.field private bitField0_:I

.field private hasLandscapeNativeBody_:Z

.field private hasPortraitNativeBody_:Z

.field private nativeBodyVersion_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17581
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17582
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 17587
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasPortraitNativeBody_:Z

    .line 17606
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasLandscapeNativeBody_:Z

    .line 17625
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->nativeBodyVersion_:I

    .line 17582
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;
    .locals 3

    .prologue
    .line 17656
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 17660
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;
    return-object v0

    .line 17657
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;
    :catch_0
    move-exception v1

    .line 17658
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 17578
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 17665
    if-ne p1, p0, :cond_1

    .line 17668
    :cond_0
    :goto_0
    return v1

    .line 17666
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 17667
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    .line 17668
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasPortraitNativeBody_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasPortraitNativeBody_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasLandscapeNativeBody_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasLandscapeNativeBody_:Z

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->nativeBodyVersion_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->nativeBodyVersion_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getHasLandscapeNativeBody()Z
    .locals 1

    .prologue
    .line 17608
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasLandscapeNativeBody_:Z

    return v0
.end method

.method public getHasPortraitNativeBody()Z
    .locals 1

    .prologue
    .line 17589
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasPortraitNativeBody_:Z

    return v0
.end method

.method public getNativeBodyVersion()I
    .locals 1

    .prologue
    .line 17627
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->nativeBodyVersion_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 17697
    const/4 v0, 0x0

    .line 17698
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 17699
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasPortraitNativeBody_:Z

    .line 17700
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 17702
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 17703
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasLandscapeNativeBody_:Z

    .line 17704
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 17706
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 17707
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->nativeBodyVersion_:I

    .line 17708
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17710
    :cond_2
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->cachedSize:I

    .line 17711
    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 17674
    const/16 v0, 0x11

    .line 17675
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 17676
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasPortraitNativeBody_:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v4, v1

    .line 17677
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasLandscapeNativeBody_:Z

    if-eqz v4, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 17678
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->nativeBodyVersion_:I

    add-int v0, v1, v2

    .line 17679
    return v0

    :cond_0
    move v1, v3

    .line 17676
    goto :goto_0

    :cond_1
    move v2, v3

    .line 17677
    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17719
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 17720
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 17724
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 17725
    :sswitch_0
    return-object p0

    .line 17730
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasPortraitNativeBody_:Z

    .line 17731
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    goto :goto_0

    .line 17735
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasLandscapeNativeBody_:Z

    .line 17736
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    goto :goto_0

    .line 17740
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->nativeBodyVersion_:I

    .line 17741
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    goto :goto_0

    .line 17720
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17578
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17684
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 17685
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasPortraitNativeBody_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 17687
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 17688
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hasLandscapeNativeBody_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 17690
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 17691
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->nativeBodyVersion_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 17693
    :cond_2
    return-void
.end method
