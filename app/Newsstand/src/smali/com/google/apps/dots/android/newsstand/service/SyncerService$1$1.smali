.class Lcom/google/apps/dots/android/newsstand/service/SyncerService$1$1;
.super Ljava/lang/Object;
.source "SyncerService.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureFallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->call()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureFallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;

    .prologue
    .line 357
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 361
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->val$failureCount:[I

    aget v1, v0, v4

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, v4

    .line 362
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Trouble syncing %s, %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 363
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncEditionFailedEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->getAppId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->val$userWriteToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {v0, v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncEditionFailedEvent;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncEditionFailedEvent;->track()V

    .line 365
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
