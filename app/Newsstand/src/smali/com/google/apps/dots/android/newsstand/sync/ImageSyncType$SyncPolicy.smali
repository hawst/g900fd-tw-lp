.class public Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;
.super Ljava/lang/Object;
.source "ImageSyncType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SyncPolicy"
.end annotation


# instance fields
.field private final policyInt:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "policyInt"    # I

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->policyInt:I

    .line 109
    return-void
.end method

.method private hasFlag(I)Z
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 112
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->policyInt:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public allowsAnything()Z
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->policyInt:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public allowsMagazines()Z
    .locals 1

    .prologue
    .line 143
    const/high16 v0, 0x30000

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->hasFlag(I)Z

    move-result v0

    return v0
.end method

.method public allowsNewsAllImages()Z
    .locals 1

    .prologue
    .line 135
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->hasFlag(I)Z

    move-result v0

    return v0
.end method

.method public allowsNewsAnything()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 126
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->hasFlag(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->hasFlag(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x8

    .line 127
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->hasFlag(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public allowsNewsPrimaryImages()Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->hasFlag(I)Z

    move-result v0

    return v0
.end method
