.class final Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ResourceLoadFailedNotifyingCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;


# direct methods
.method private constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
    .locals 0

    .prologue
    .line 462
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p2, "x1"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$1;

    .prologue
    .line 462
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->resourceLoadListener:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedListener;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->resourceLoadListener:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedListener;

    invoke-interface {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedListener;->onResourceLoadFailed(Ljava/lang/Throwable;)V

    .line 468
    :cond_0
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 473
    return-void
.end method
