.class Lcom/google/apps/dots/android/newsstand/service/SyncerService$3$1;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;
.source "SyncerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;->apply(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor",
        "<",
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;

    .prologue
    .line 453
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;-><init>()V

    return-void
.end method


# virtual methods
.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 4
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 456
    iget-object v1, p2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 457
    .local v1, "appId":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;->val$newMagazineAppIds:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 458
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3$1;->currentAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v0

    .line 460
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    .line 462
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->magazineEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v3

    .line 461
    invoke-static {v3, p2, v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->magazineEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v3

    .line 459
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->notifyForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    .line 464
    .end local v0    # "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    :cond_0
    return-void
.end method
