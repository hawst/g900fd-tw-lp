.class public abstract Lcom/google/apps/dots/android/newsstand/home/HomePage;
.super Ljava/lang/Object;
.source "HomePage.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/home/HomePage;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_HOME_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

.field public static final EXPLORE_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

.field public static final MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

.field public static final READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

.field public static final SAVED_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;


# instance fields
.field public final type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowPage;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowPage;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryPage;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryPage;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/saved/SavedPage;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/saved/SavedPage;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->SAVED_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 25
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExplorePage;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExplorePage;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->EXPLORE_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 26
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->DEFAULT_HOME_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 84
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/HomePage$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;)V
    .locals 0
    .param p1, "type"    # Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    .line 40
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 57
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/home/HomePage;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 58
    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 59
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/home/HomePage;
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 61
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/home/HomePage;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public abstract getFragment(Landroid/content/Context;)Landroid/support/v4/app/Fragment;
.end method

.method public abstract getTitle(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->hashCode()I

    move-result v0

    return v0
.end method

.method public setupActionBar(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V
    .locals 3
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .line 50
    .local v0, "activity":Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->setActionBarTitle(Ljava/lang/CharSequence;)V

    .line 51
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 52
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->syncDrawerIndicator()V

    .line 53
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 71
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "{type: %s}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    return-void
.end method
