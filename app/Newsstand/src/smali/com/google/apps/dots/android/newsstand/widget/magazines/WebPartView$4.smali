.class Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;
.super Ljava/lang/Object;
.source "WebPartView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->postMethod(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

.field final synthetic val$methods:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 236
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    iput p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->val$methods:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 240
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->val$methods:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->resetState()V

    .line 243
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->val$methods:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->requestInjection()V

    .line 246
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->val$methods:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 250
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reloadedWithoutWideViewportCount:I
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->currentScale:F
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$400(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .line 251
    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeHorizontalScrollRange()I
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->currentScale:F
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$400(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)F

    move-result v1

    div-float/2addr v0, v1

    const/high16 v1, 0x44750000    # 980.0f

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_3

    .line 253
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # operator++ for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reloadedWithoutWideViewportCount:I
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$308(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)I

    .line 254
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 255
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->loadBaseHtml()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$600(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)V

    .line 263
    :cond_2
    :goto_0
    return-void

    .line 257
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    const/4 v1, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isLoadComplete:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$702(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;Z)Z

    .line 258
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->onLoadComplete()V

    .line 259
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->onLoadComplete()V

    goto :goto_0
.end method
