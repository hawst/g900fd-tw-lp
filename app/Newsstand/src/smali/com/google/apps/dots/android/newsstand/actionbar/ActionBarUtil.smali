.class public Lcom/google/apps/dots/android/newsstand/actionbar/ActionBarUtil;
.super Ljava/lang/Object;
.source "ActionBarUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final tempHsv:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/apps/dots/android/newsstand/actionbar/ActionBarUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/actionbar/ActionBarUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [F

    sput-object v0, Lcom/google/apps/dots/android/newsstand/actionbar/ActionBarUtil;->tempHsv:[F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method

.method public static getActionBarDrawableForColor(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "color"    # I

    .prologue
    .line 204
    .line 205
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->action_bar_white_background:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 206
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorFilter(IZ)Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 207
    return-object v0
.end method

.method public static getActionBarDrawableForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "subscribed"    # Z

    .prologue
    .line 198
    invoke-virtual {p1, p0, p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->actionBarColor(Landroid/content/Context;Z)I

    move-result v0

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/actionbar/ActionBarUtil;->getActionBarDrawableForColor(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
