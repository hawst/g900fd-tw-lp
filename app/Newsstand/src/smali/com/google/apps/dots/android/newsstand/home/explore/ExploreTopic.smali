.class public Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
.super Ljava/lang/Object;
.source "ExploreTopic.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;",
            ">;"
        }
    .end annotation
.end field

.field private static featuredTopicInstance:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;


# instance fields
.field public final imageAttachmentId:Ljava/lang/String;

.field private final internationalCurationsTopic:Z

.field public final name:Ljava/lang/String;

.field public final topicId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "topicId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "imageAttachmentId"    # Ljava/lang/String;

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "topicId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "imageAttachmentId"    # Ljava/lang/String;
    .param p4, "internationalCurationsTopic"    # Z

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->topicId:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->name:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->imageAttachmentId:Ljava/lang/String;

    .line 36
    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->internationalCurationsTopic:Z

    .line 37
    return-void
.end method

.method public static featuredTopic(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->featuredTopicInstance:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    const-string v1, "featured_topic"

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->featured:I

    .line 107
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->featuredTopicInstance:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 109
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->featuredTopicInstance:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 50
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 51
    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 52
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->topicId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->topicId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->internationalCurationsTopic:Z

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->internationalCurationsTopic:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 55
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->topicId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->internationalCurationsTopic:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->imageAttachmentId:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isInternationalCurationsTopic()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->internationalCurationsTopic:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 45
    const-string v0, "%s(%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->topicId:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public useExpandableHeader()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->imageAttachmentId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->topicId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->imageAttachmentId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->internationalCurationsTopic:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    return-void
.end method
