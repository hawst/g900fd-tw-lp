.class public final Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WebPageSummary"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;


# instance fields
.field private abstract__:Ljava/lang/String;

.field private bitField0_:I

.field public clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

.field private externalCreatedMs_:J

.field private linkId_:Ljava/lang/String;

.field private primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private publisher_:Ljava/lang/String;

.field public screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

.field private thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private title_:Ljava/lang/String;

.field private webPageUrl_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23759
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23760
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 23928
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->linkId_:Ljava/lang/String;

    .line 23950
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->title_:Ljava/lang/String;

    .line 23972
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->abstract__:Ljava/lang/String;

    .line 23994
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->publisher_:Ljava/lang/String;

    .line 24016
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->externalCreatedMs_:J

    .line 24035
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    .line 24038
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->webPageUrl_:Ljava/lang/String;

    .line 24060
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 23760
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
    .locals 5

    .prologue
    .line 24120
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24124
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 24125
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    .line 24126
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 24127
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 24128
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->clone()Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    move-result-object v4

    aput-object v4, v3, v2

    .line 24126
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 24121
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 24122
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 24132
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 24133
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 24134
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    if-ge v2, v3, :cond_3

    .line 24135
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v3, v3, v2

    if-eqz v3, :cond_2

    .line 24136
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v4

    aput-object v4, v3, v2

    .line 24134
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 24140
    .end local v2    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_4

    .line 24141
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 24143
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_5

    .line 24144
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 24146
    :cond_5
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 23756
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 24151
    if-ne p1, p0, :cond_1

    .line 24163
    :cond_0
    :goto_0
    return v1

    .line 24152
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 24153
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 24154
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->linkId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->linkId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 24155
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->abstract__:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->abstract__:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 24156
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->publisher_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->publisher_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 24157
    :goto_4
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->externalCreatedMs_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->externalCreatedMs_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    .line 24159
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->webPageUrl_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->webPageUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 24160
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 24161
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_3

    .line 24162
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 24163
    goto :goto_0

    .line 24154
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->linkId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->linkId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->title_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->title_:Ljava/lang/String;

    .line 24155
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->abstract__:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->abstract__:Ljava/lang/String;

    .line 24156
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->publisher_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->publisher_:Ljava/lang/String;

    .line 24157
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4

    .line 24159
    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->webPageUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->webPageUrl_:Ljava/lang/String;

    .line 24160
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_5

    .line 24161
    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 24162
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 24163
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0
.end method

.method public getAbstract()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23974
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->abstract__:Ljava/lang/String;

    return-object v0
.end method

.method public getExternalCreatedMs()J
    .locals 2

    .prologue
    .line 24018
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->externalCreatedMs_:J

    return-wide v0
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23996
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->publisher_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 24236
    const/4 v1, 0x0

    .line 24237
    .local v1, "size":I
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_0

    .line 24238
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->title_:Ljava/lang/String;

    .line 24239
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 24241
    :cond_0
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_1

    .line 24242
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->abstract__:Ljava/lang/String;

    .line 24243
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 24245
    :cond_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_2

    .line 24246
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->publisher_:Ljava/lang/String;

    .line 24247
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 24249
    :cond_2
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    if-eqz v3, :cond_3

    .line 24250
    const/4 v3, 0x4

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->externalCreatedMs_:J

    .line 24251
    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v3

    add-int/2addr v1, v3

    .line 24253
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    if-eqz v3, :cond_5

    .line 24254
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_5

    aget-object v0, v4, v3

    .line 24255
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    if-eqz v0, :cond_4

    .line 24256
    const/4 v6, 0x5

    .line 24257
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 24254
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 24261
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    :cond_5
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    if-eqz v3, :cond_6

    .line 24262
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->webPageUrl_:Ljava/lang/String;

    .line 24263
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 24265
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v3, :cond_8

    .line 24266
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v4, v3

    :goto_1
    if-ge v2, v4, :cond_8

    aget-object v0, v3, v2

    .line 24267
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    if-eqz v0, :cond_7

    .line 24268
    const/4 v5, 0x7

    .line 24269
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 24266
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 24273
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_9

    .line 24274
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 24275
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 24277
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_a

    .line 24278
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 24279
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 24281
    :cond_a
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_b

    .line 24282
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->linkId_:Ljava/lang/String;

    .line 24283
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 24285
    :cond_b
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->cachedSize:I

    .line 24286
    return v1
.end method

.method public getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 24065
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23952
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getWebPageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24040
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->webPageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public hasExternalCreatedMs()Z
    .locals 1

    .prologue
    .line 24026
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasThumbnail()Z
    .locals 1

    .prologue
    .line 24075
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 24167
    const/16 v1, 0x11

    .line 24168
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 24169
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->linkId_:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 24170
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->title_:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 24171
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->abstract__:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 24172
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->publisher_:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 24173
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->externalCreatedMs_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->externalCreatedMs_:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 24174
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    if-nez v2, :cond_6

    mul-int/lit8 v1, v1, 0x1f

    .line 24180
    :cond_0
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->webPageUrl_:Ljava/lang/String;

    if-nez v2, :cond_8

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 24181
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v2, :cond_9

    mul-int/lit8 v1, v1, 0x1f

    .line 24187
    :cond_1
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_b

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 24188
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v4, :cond_c

    :goto_6
    add-int v1, v2, v3

    .line 24189
    return v1

    .line 24169
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->linkId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 24170
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->title_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 24171
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->abstract__:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 24172
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->publisher_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    .line 24176
    :cond_6
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 24177
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v3

    :goto_8
    add-int v1, v4, v2

    .line 24176
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 24177
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->hashCode()I

    move-result v2

    goto :goto_8

    .line 24180
    .end local v0    # "i":I
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->webPageUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    .line 24183
    :cond_9
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 24184
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v2, v2, v0

    if-nez v2, :cond_a

    move v2, v3

    :goto_a
    add-int v1, v4, v2

    .line 24183
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 24184
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->hashCode()I

    move-result v2

    goto :goto_a

    .line 24187
    .end local v0    # "i":I
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto :goto_5

    .line 24188
    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v3

    goto :goto_6
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 24294
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 24295
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 24299
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 24300
    :sswitch_0
    return-object p0

    .line 24305
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->title_:Ljava/lang/String;

    .line 24306
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    goto :goto_0

    .line 24310
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->abstract__:Ljava/lang/String;

    .line 24311
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    goto :goto_0

    .line 24315
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->publisher_:Ljava/lang/String;

    .line 24316
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    goto :goto_0

    .line 24320
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->externalCreatedMs_:J

    .line 24321
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    goto :goto_0

    .line 24325
    :sswitch_5
    const/16 v5, 0x2a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 24326
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    if-nez v5, :cond_2

    move v1, v4

    .line 24327
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    .line 24328
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    if-eqz v5, :cond_1

    .line 24329
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 24331
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    .line 24332
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 24333
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;-><init>()V

    aput-object v6, v5, v1

    .line 24334
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 24335
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 24332
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 24326
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    array-length v1, v5

    goto :goto_1

    .line 24338
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;-><init>()V

    aput-object v6, v5, v1

    .line 24339
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 24343
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->webPageUrl_:Ljava/lang/String;

    .line 24344
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    goto/16 :goto_0

    .line 24348
    :sswitch_7
    const/16 v5, 0x3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 24349
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v5, :cond_5

    move v1, v4

    .line 24350
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 24351
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v5, :cond_4

    .line 24352
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 24354
    :cond_4
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 24355
    :goto_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 24356
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    aput-object v6, v5, v1

    .line 24357
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 24358
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 24355
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 24349
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_5
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v1, v5

    goto :goto_3

    .line 24361
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_6
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    aput-object v6, v5, v1

    .line 24362
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 24366
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :sswitch_8
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v5, :cond_7

    .line 24367
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 24369
    :cond_7
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 24373
    :sswitch_9
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v5, :cond_8

    .line 24374
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 24376
    :cond_8
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 24380
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->linkId_:Ljava/lang/String;

    .line 24381
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    goto/16 :goto_0

    .line 24295
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23756
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 24194
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    .line 24195
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->title_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 24197
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    .line 24198
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->abstract__:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 24200
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_2

    .line 24201
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->publisher_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 24203
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_3

    .line 24204
    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->externalCreatedMs_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 24206
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    if-eqz v2, :cond_5

    .line 24207
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->screenshot:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_5

    aget-object v0, v3, v2

    .line 24208
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    if-eqz v0, :cond_4

    .line 24209
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 24207
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 24213
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    :cond_5
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_6

    .line 24214
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->webPageUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 24216
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v2, :cond_8

    .line 24217
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_8

    aget-object v0, v2, v1

    .line 24218
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    if-eqz v0, :cond_7

    .line 24219
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 24217
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 24223
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_8
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v1, :cond_9

    .line 24224
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 24226
    :cond_9
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v1, :cond_a

    .line 24227
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 24229
    :cond_a
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_b

    .line 24230
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->linkId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 24232
    :cond_b
    return-void
.end method
