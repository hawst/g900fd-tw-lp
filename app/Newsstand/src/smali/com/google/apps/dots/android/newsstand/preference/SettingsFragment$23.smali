.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$23;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupInternalPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .prologue
    .line 626
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$23;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 629
    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    .line 630
    .local v0, "enabled":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->enableAll(Z)V

    .line 631
    const/4 v1, 0x1

    return v1
.end method
