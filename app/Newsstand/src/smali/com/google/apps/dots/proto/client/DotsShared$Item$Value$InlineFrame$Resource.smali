.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Resource"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;


# instance fields
.field private attachmentId_:Ljava/lang/String;

.field private bitField0_:I

.field private uri_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14430
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14431
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 14436
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->uri_:Ljava/lang/String;

    .line 14458
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->attachmentId_:Ljava/lang/String;

    .line 14431
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    .locals 3

    .prologue
    .line 14491
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 14495
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    return-object v0

    .line 14492
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    :catch_0
    move-exception v1

    .line 14493
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 14427
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 14500
    if-ne p1, p0, :cond_1

    .line 14504
    :cond_0
    :goto_0
    return v1

    .line 14501
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 14502
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    .line 14503
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->uri_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->uri_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->attachmentId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->attachmentId_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 14504
    goto :goto_0

    .line 14503
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->uri_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->uri_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->attachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->attachmentId_:Ljava/lang/String;

    .line 14504
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getAttachmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14460
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->attachmentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 14527
    const/4 v0, 0x0

    .line 14528
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14529
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->uri_:Ljava/lang/String;

    .line 14530
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14532
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14533
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->attachmentId_:Ljava/lang/String;

    .line 14534
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14536
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->cachedSize:I

    .line 14537
    return v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14438
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->uri_:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 14508
    const/16 v0, 0x11

    .line 14509
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 14510
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->uri_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 14511
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->attachmentId_:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 14512
    return v0

    .line 14510
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->uri_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 14511
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->attachmentId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14545
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 14546
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 14550
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 14551
    :sswitch_0
    return-object p0

    .line 14556
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->uri_:Ljava/lang/String;

    .line 14557
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->bitField0_:I

    goto :goto_0

    .line 14561
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->attachmentId_:Ljava/lang/String;

    .line 14562
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->bitField0_:I

    goto :goto_0

    .line 14546
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14427
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14517
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14518
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->uri_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 14520
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14521
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->attachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 14523
    :cond_1
    return-void
.end method
