.class public abstract Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.super Ljava/lang/Object;
.source "SafeOnClickListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 19
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getActivityFromView(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    .line 21
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 22
    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;->onClickSafely(Landroid/view/View;Landroid/app/Activity;)V

    .line 24
    :cond_0
    return-void
.end method

.method public abstract onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
.end method
