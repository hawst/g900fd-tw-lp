.class Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;
.super Ljava/lang/Object;
.source "LayoutSelectionGroup.java"

# interfaces
.implements Lcom/google/android/libraries/bind/card/ViewGenerator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->makeViewGenerator(I[I)Lcom/google/android/libraries/bind/card/ViewGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

.field final synthetic val$dataIndices:[I

.field final synthetic val$layoutResId:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;I[I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    iput p2, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->val$layoutResId:I

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->val$dataIndices:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCardCount()I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->val$dataIndices:[I

    array-length v0, v0

    return v0
.end method

.method public getCardIds()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->val$dataIndices:[I

    array-length v2, v2

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 188
    .local v0, "cardIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->val$dataIndices:[I

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 189
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->val$dataIndices:[I

    aget v3, v3, v1

    # invokes: Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->getCardId(I)Ljava/lang/Object;
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->access$400(Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 191
    :cond_0
    return-object v0
.end method

.method public getViewResId()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->val$layoutResId:I

    return v0
.end method

.method public makeView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;
    .locals 17
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    .line 150
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->val$layoutResId:I

    new-instance v13, Landroid/view/ViewGroup$LayoutParams;

    const/4 v14, -0x1

    const/4 v15, -0x2

    invoke-direct {v13, v14, v15}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v0, v12, v1, v13}, Lcom/google/android/libraries/bind/view/ViewHeap;->get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v10

    .line 153
    .local v10, "rowView":Landroid/view/View;
    const/4 v2, 0x0

    .local v2, "cardIndex":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->val$dataIndices:[I

    array-length v12, v12

    if-ge v2, v12, :cond_3

    .line 154
    # getter for: Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->CARD_RES_IDS:[I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->access$000()[I

    move-result-object v12

    aget v12, v12, v2

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 157
    .local v4, "cardToFill":Landroid/view/ViewGroup;
    if-nez v4, :cond_0

    .line 158
    new-instance v12, Ljava/lang/IllegalArgumentException;

    const-string v13, "Layout with resourceId = %d is trying to fill a container with cardId = %d, which does not exist."

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->val$layoutResId:I

    move/from16 v16, v0

    .line 160
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    .line 158
    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 164
    :cond_0
    new-instance v7, Landroid/view/ViewGroup$LayoutParams;

    const/4 v12, -0x1

    const/4 v13, -0x1

    invoke-direct {v7, v12, v13}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 166
    .local v7, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->val$dataIndices:[I

    aget v9, v12, v2

    .line 167
    .local v9, "position":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    # invokes: Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->getMappedPosition(I)I
    invoke-static {v12, v9}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->access$100(Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;I)I

    move-result v8

    .line 168
    .local v8, "mappedPosition":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    # invokes: Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->list()Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v12}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->access$200(Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v12

    invoke-virtual {v12, v8}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v6

    .line 169
    .local v6, "data":Lcom/google/android/libraries/bind/data/Data;
    const/4 v11, 0x0

    .line 172
    .local v11, "viewResIdOverride":Ljava/lang/Integer;
    instance-of v12, v4, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;

    if-eqz v12, :cond_1

    move-object v12, v4

    .line 173
    check-cast v12, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;

    invoke-virtual {v12}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->getCardSize()Ljava/lang/Integer;

    move-result-object v3

    .line 174
    .local v3, "cardSize":Ljava/lang/Integer;
    if-nez v3, :cond_2

    const/4 v11, 0x0

    .line 178
    .end local v3    # "cardSize":Ljava/lang/Integer;
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    move-object/from16 v0, p3

    # invokes: Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->makeCardView(Lcom/google/android/libraries/bind/view/ViewHeap;ILandroid/view/ViewGroup$LayoutParams;Ljava/lang/Integer;)Landroid/view/View;
    invoke-static {v12, v0, v9, v7, v11}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->access$300(Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;Lcom/google/android/libraries/bind/view/ViewHeap;ILandroid/view/ViewGroup$LayoutParams;Ljava/lang/Integer;)Landroid/view/View;

    move-result-object v5

    .line 179
    .local v5, "cardView":Landroid/view/View;
    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 153
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 174
    .end local v5    # "cardView":Landroid/view/View;
    .restart local v3    # "cardSize":Ljava/lang/Integer;
    :cond_2
    sget v12, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    .line 176
    invoke-virtual {v6, v12}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 175
    invoke-static {v12, v3}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getResizedLayoutId(ILjava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v11

    goto :goto_1

    .line 182
    .end local v3    # "cardSize":Ljava/lang/Integer;
    .end local v4    # "cardToFill":Landroid/view/ViewGroup;
    .end local v6    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v7    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v8    # "mappedPosition":I
    .end local v9    # "position":I
    .end local v11    # "viewResIdOverride":Ljava/lang/Integer;
    :cond_3
    return-object v10
.end method
