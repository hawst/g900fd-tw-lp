.class public Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
.super Ljava/lang/Object;
.source "AsyncToken.java"

# interfaces
.implements Ljava/util/concurrent/Executor;


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field public final account:Landroid/accounts/Account;

.field public final asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private final cancellables:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/bind/async/Cancellable;",
            ">;"
        }
    .end annotation
.end field

.field private destroyed:Z

.field private final futures:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/concurrent/Future",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final handler:Landroid/os/Handler;

.field private final lock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method private constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;Landroid/accounts/Account;)V
    .locals 2
    .param p1, "asyncScope"    # Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    .line 39
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->handler:Landroid/os/Handler;

    .line 41
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->futures:Ljava/util/Set;

    .line 42
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->cancellables:Ljava/util/Set;

    .line 56
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 57
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/concurrent/Future;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "x1"    # Ljava/util/concurrent/Future;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->unregisterInternal(Ljava/util/concurrent/Future;)V

    return-void
.end method

.method static get(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1
    .param p0, "asyncScope"    # Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 52
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;Landroid/accounts/Account;)V

    return-object v0
.end method

.method private registerInternal(Ljava/util/concurrent/Future;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .local p1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 255
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 256
    :try_start_0
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 257
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->futures:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 258
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "REGISTER Future (%d): %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->futures:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    monitor-exit v2

    .line 260
    return-void

    :cond_0
    move v0, v1

    .line 256
    goto :goto_0

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private unregisterInternal(Ljava/util/concurrent/Future;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 263
    .local p1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 264
    :try_start_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    if-nez v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->futures:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 266
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "UNREGISTER Future (%d): %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->futures:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 268
    :cond_0
    monitor-exit v1

    .line 269
    return-void

    .line 268
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "Lcom/google/common/util/concurrent/FutureCallback",
            "<-TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TT;>;"
    .local p2, "callback":Lcom/google/common/util/concurrent/FutureCallback;, "Lcom/google/common/util/concurrent/FutureCallback<-TT;>;"
    invoke-static {p1, p2, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public addInlineCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "Lcom/google/common/util/concurrent/FutureCallback",
            "<-TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TT;>;"
    .local p2, "callback":Lcom/google/common/util/concurrent/FutureCallback;, "Lcom/google/common/util/concurrent/FutureCallback<-TT;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 167
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 168
    :try_start_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    if-eqz v0, :cond_0

    .line 173
    monitor-exit v1

    .line 185
    :goto_0
    return-object p1

    .line 175
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    invoke-interface {p1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 175
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 183
    :cond_1
    invoke-static {p1, p2, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public destroy()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 62
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 63
    :try_start_0
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    if-eqz v2, :cond_0

    .line 64
    monitor-exit v3

    .line 82
    :goto_0
    return-void

    .line 66
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    .line 70
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->handler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 73
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->futures:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Future;

    .line 74
    .local v1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "CANCEL Future: %s"

    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    invoke-interface {v1, v7}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_1

    .line 70
    .end local v1    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 77
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->futures:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 78
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->cancellables:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/async/Cancellable;

    .line 79
    .local v0, "cancellable":Lcom/google/android/libraries/bind/async/Cancellable;
    invoke-interface {v0}, Lcom/google/android/libraries/bind/async/Cancellable;->cancel()V

    goto :goto_2

    .line 81
    .end local v0    # "cancellable":Lcom/google/android/libraries/bind/async/Cancellable;
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->cancellables:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    goto :goto_0
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 93
    return-void
.end method

.method public isDestroyed()Z
    .locals 2

    .prologue
    .line 85
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 86
    :try_start_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    monitor-exit v1

    return v0

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized post(Ljava/lang/Runnable;)Z
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 97
    :try_start_1
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 96
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized postDelayed(Ljava/lang/Runnable;J)Z
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;
    .param p2, "delayMillis"    # J

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 121
    :try_start_1
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    if-eqz v0, :cond_0

    .line 126
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 120
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public register(Lcom/google/android/libraries/bind/async/Cancellable;)Z
    .locals 3
    .param p1, "cancellable"    # Lcom/google/android/libraries/bind/async/Cancellable;

    .prologue
    .line 211
    const/4 v0, 0x0

    .line 212
    .local v0, "cancelImmediately":Z
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 213
    :try_start_0
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    if-eqz v1, :cond_0

    .line 214
    const/4 v0, 0x1

    .line 218
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    if-eqz v0, :cond_1

    .line 220
    invoke-interface {p1}, Lcom/google/android/libraries/bind/async/Cancellable;->cancel()V

    .line 221
    const/4 v1, 0x0

    .line 223
    :goto_1
    return v1

    .line 216
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->cancellables:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 218
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 223
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public declared-synchronized removeCallbacks(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 137
    :try_start_1
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    if-eqz v0, :cond_0

    .line 138
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    :goto_0
    monitor-exit p0

    return-void

    .line 140
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 141
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 136
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public submit(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 235
    .local p2, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    const/4 v0, 0x0

    .line 236
    .local v0, "result":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TT;>;"
    const/4 v1, 0x0

    .line 237
    .local v1, "task":Lcom/google/common/util/concurrent/ListenableFutureTask;, "Lcom/google/common/util/concurrent/ListenableFutureTask<TT;>;"
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 238
    :try_start_0
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    if-eqz v2, :cond_0

    .line 243
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateCancelledFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    monitor-exit v3

    .line 251
    :goto_0
    return-object v2

    .line 245
    :cond_0
    invoke-static {p2}, Lcom/google/common/util/concurrent/ListenableFutureTask;->create(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFutureTask;

    move-result-object v1

    .line 246
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->track(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 247
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    invoke-interface {p1, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    move-object v2, v0

    .line 251
    goto :goto_0

    .line 247
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public track(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 189
    .local p1, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TT;>;"
    const/4 v0, 0x0

    .line 190
    .local v0, "cancelImmediately":Z
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 191
    :try_start_0
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    if-eqz v1, :cond_1

    .line 193
    const/4 v0, 0x1

    .line 203
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    if-eqz v0, :cond_0

    .line 205
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 207
    :cond_0
    return-object p1

    .line 195
    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->registerInternal(Ljava/util/concurrent/Future;)V

    .line 196
    new-instance v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken$1;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addListener(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 203
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public unregister(Lcom/google/android/libraries/bind/async/Cancellable;)V
    .locals 2
    .param p1, "cancellable"    # Lcom/google/android/libraries/bind/async/Cancellable;

    .prologue
    .line 227
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 228
    :try_start_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroyed:Z

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->cancellables:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 231
    :cond_0
    monitor-exit v1

    .line 232
    return-void

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
