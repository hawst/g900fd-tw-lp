.class Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$8;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "NewsWidgetService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getBitmap(Lcom/google/common/util/concurrent/ListenableFuture;ZI)Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;

.field final synthetic val$appWidgetId:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;

    .prologue
    .line 485
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$8;->this$0:Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;

    iput p2, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$8;->val$appWidgetId:I

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 488
    if-nez p1, :cond_0

    .line 492
    :goto_0
    return-void

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$8;->this$0:Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$8;->val$appWidgetId:I

    # invokes: Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->updateRemoteViews(I)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->access$300(Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;I)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 485
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$8;->onSuccess(Landroid/graphics/Bitmap;)V

    return-void
.end method
