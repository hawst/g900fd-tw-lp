.class final Lcom/google/apps/dots/android/newsstand/edition/EditionSummary$1;
.super Ljava/lang/Object;
.source "EditionSummary.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 6
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 237
    const-class v4, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 238
    .local v3, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    const/4 v2, 0x0

    .line 240
    .local v2, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;-><init>()V

    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->decodeBase64(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-object v2, v0
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_1

    .line 244
    :goto_0
    const/4 v1, 0x0

    .line 246
    .local v1, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    :try_start_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;-><init>()V

    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->decodeBase64(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-object v1, v0
    :try_end_1
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_1 .. :try_end_1} :catch_0

    .line 250
    :goto_1
    new-instance v4, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {v4, v3, v2, v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    return-object v4

    .line 247
    :catch_0
    move-exception v4

    goto :goto_1

    .line 241
    .end local v1    # "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 233
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 255
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 233
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    return-object v0
.end method
