.class Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$2;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;
.source "ArticleTailList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->makeEditionCard(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

.field final synthetic val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$2;->this$1:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$2;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
    .locals 3

    .prologue
    .line 164
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$2;->this$1:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$2;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    # invokes: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->getAnalyticsScreenName(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->access$400(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$2;->this$1:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->access$000(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$2;->get()Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;

    move-result-object v0

    return-object v0
.end method
