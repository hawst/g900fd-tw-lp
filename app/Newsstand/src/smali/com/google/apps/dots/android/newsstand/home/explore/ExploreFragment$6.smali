.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$6;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "ExploreFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->makeFeaturedCard()Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 195
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;

    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->featuredTopic(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->start()V

    .line 196
    return-void
.end method
