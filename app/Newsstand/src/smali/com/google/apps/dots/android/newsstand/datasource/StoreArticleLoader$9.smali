.class Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$9;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getUseLegacyLayoutFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Section;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 327
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$9;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$9;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    # invokes: Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->useLegacyLayout(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Z
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->access$200(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/proto/client/DotsShared$Section;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 327
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$9;->apply(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
