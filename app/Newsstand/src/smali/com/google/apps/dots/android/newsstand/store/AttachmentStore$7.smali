.class Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;
.super Ljava/lang/Object;
.source "AttachmentStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureFallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureFallback",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$attachmentId:Ljava/lang/String;

.field final synthetic val$decodeOptions:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

.field final synthetic val$transform:Lcom/google/apps/dots/android/newsstand/server/Transform;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 349
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;->val$attachmentId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;->val$transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;->val$decodeOptions:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 352
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/sync/SyncException;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/util/concurrent/TimeoutException;

    if-eqz v0, :cond_1

    .line 354
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Falling back to client-side transform for %s due to %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;->val$attachmentId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 355
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;->val$attachmentId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;->val$transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$7;->val$decodeOptions:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->simulateBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$600(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 357
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
