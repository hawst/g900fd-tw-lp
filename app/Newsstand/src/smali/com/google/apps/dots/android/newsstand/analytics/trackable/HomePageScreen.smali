.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;
.source "HomePageScreen.java"


# instance fields
.field private final page:I

.field private final pageType:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;)V
    .locals 1
    .param p1, "pageType"    # Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen;-><init>(Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;I)V

    .line 17
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;I)V
    .locals 2
    .param p1, "pageType"    # Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;
    .param p2, "page"    # I

    .prologue
    .line 20
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen;->getScreenNameFromPageType(Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen;->pageType:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    .line 22
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen;->page:I

    .line 23
    return-void
.end method

.method private static getScreenNameFromPageType(Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;)Ljava/lang/String;
    .locals 5
    .param p0, "pageType"    # Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    .prologue
    .line 42
    sget-object v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen$1;->$SwitchMap$com$google$apps$dots$android$newsstand$home$HomePage$Type:[I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 54
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x21

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Can not recognize Homepage type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 44
    :pswitch_0
    const-string v0, "Bookmarks"

    .line 56
    :goto_0
    return-object v0

    .line 47
    :pswitch_1
    const-string v0, "Explore"

    .line 48
    .local v0, "screenName":Ljava/lang/String;
    goto :goto_0

    .line 52
    .end local v0    # "screenName":Ljava/lang/String;
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;->fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object p1

    .line 63
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen;->page:I

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPage(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 64
    return-object p1
.end method

.method protected shouldIgnore()Z
    .locals 2

    .prologue
    .line 27
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen$1;->$SwitchMap$com$google$apps$dots$android$newsstand$home$HomePage$Type:[I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen;->pageType:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 36
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 32
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 27
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
