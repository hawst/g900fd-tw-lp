.class public final Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsFinsky.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsFinsky;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FinskyDocumentSummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;


# instance fields
.field private albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

.field private appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

.field private bitField0_:I

.field private bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

.field public child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

.field private detailPagePath_:Ljava/lang/String;

.field private docidString_:Ljava/lang/String;

.field private docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

.field private imageUrl_:Ljava/lang/String;

.field public images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

.field public offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

.field private previewVideoThumbnailUrl_:Ljava/lang/String;

.field private previewVideoUrl_:Ljava/lang/String;

.field private rating_:F

.field public snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

.field private subtitle_:Ljava/lang/String;

.field private title_:Ljava/lang/String;

.field public videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1727
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1728
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1752
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->title_:Ljava/lang/String;

    .line 1774
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->subtitle_:Ljava/lang/String;

    .line 1796
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->imageUrl_:Ljava/lang/String;

    .line 1818
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoUrl_:Ljava/lang/String;

    .line 1840
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoThumbnailUrl_:Ljava/lang/String;

    .line 1900
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 1903
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docidString_:Ljava/lang/String;

    .line 1925
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    .line 1928
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->detailPagePath_:Ljava/lang/String;

    .line 1950
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->rating_:F

    .line 1969
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    .line 1972
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    .line 1994
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    .line 1728
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;
    .locals 5

    .prologue
    .line 2023
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2027
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-eqz v3, :cond_0

    .line 2028
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    .line 2030
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    if-eqz v3, :cond_1

    .line 2031
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    .line 2033
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    if-eqz v3, :cond_2

    .line 2034
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    .line 2036
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 2037
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 2038
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    array-length v3, v3

    if-ge v2, v3, :cond_4

    .line 2039
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    aget-object v3, v3, v2

    if-eqz v3, :cond_3

    .line 2040
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    move-result-object v4

    aput-object v4, v3, v2

    .line 2038
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2024
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 2025
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 2044
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 2045
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    .line 2046
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    array-length v3, v3

    if-ge v2, v3, :cond_6

    .line 2047
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    aget-object v3, v3, v2

    if-eqz v3, :cond_5

    .line 2048
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    move-result-object v4

    aput-object v4, v3, v2

    .line 2046
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2052
    .end local v2    # "i":I
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    array-length v3, v3

    if-lez v3, :cond_8

    .line 2053
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    .line 2054
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    array-length v3, v3

    if-ge v2, v3, :cond_8

    .line 2055
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    aget-object v3, v3, v2

    if-eqz v3, :cond_7

    .line 2056
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    move-result-object v4

    aput-object v4, v3, v2

    .line 2054
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2060
    .end local v2    # "i":I
    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    array-length v3, v3

    if-lez v3, :cond_a

    .line 2061
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    .line 2062
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    array-length v3, v3

    if-ge v2, v3, :cond_a

    .line 2063
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    aget-object v3, v3, v2

    if-eqz v3, :cond_9

    .line 2064
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    move-result-object v4

    aput-object v4, v3, v2

    .line 2062
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2068
    .end local v2    # "i":I
    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    if-eqz v3, :cond_b

    .line 2069
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    .line 2071
    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    array-length v3, v3

    if-lez v3, :cond_d

    .line 2072
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    .line 2073
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    array-length v3, v3

    if-ge v2, v3, :cond_d

    .line 2074
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    aget-object v3, v3, v2

    if-eqz v3, :cond_c

    .line 2075
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    move-result-object v4

    aput-object v4, v3, v2

    .line 2073
    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 2079
    .end local v2    # "i":I
    :cond_d
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1724
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2084
    if-ne p1, p0, :cond_1

    .line 2103
    :cond_0
    :goto_0
    return v1

    .line 2085
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2086
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    .line 2087
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2088
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->subtitle_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->subtitle_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2089
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->imageUrl_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->imageUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2090
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoUrl_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2091
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoThumbnailUrl_:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoThumbnailUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2092
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    if-nez v3, :cond_3

    .line 2093
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    if-nez v3, :cond_3

    .line 2094
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 2095
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docidString_:Ljava/lang/String;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docidString_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2096
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    .line 2097
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->detailPagePath_:Ljava/lang/String;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->detailPagePath_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2098
    :goto_a
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->rating_:F

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->rating_:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    .line 2100
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    .line 2101
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    if-nez v3, :cond_3

    .line 2102
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    .line 2103
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0

    .line 2087
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->title_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->title_:Ljava/lang/String;

    .line 2088
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->subtitle_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->subtitle_:Ljava/lang/String;

    .line 2089
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->imageUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->imageUrl_:Ljava/lang/String;

    .line 2090
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoUrl_:Ljava/lang/String;

    .line 2091
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoThumbnailUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoThumbnailUrl_:Ljava/lang/String;

    .line 2092
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    .line 2093
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    .line 2094
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    .line 2095
    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docidString_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docidString_:Ljava/lang/String;

    .line 2096
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    .line 2097
    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->detailPagePath_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->detailPagePath_:Ljava/lang/String;

    .line 2098
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    .line 2101
    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    .line 2102
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2231
    const/4 v1, 0x0

    .line 2232
    .local v1, "size":I
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-eqz v3, :cond_0

    .line 2233
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    .line 2234
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2236
    :cond_0
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    .line 2237
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->title_:Ljava/lang/String;

    .line 2238
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2240
    :cond_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_2

    .line 2241
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->subtitle_:Ljava/lang/String;

    .line 2242
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2244
    :cond_2
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_3

    .line 2245
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->imageUrl_:Ljava/lang/String;

    .line 2246
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2248
    :cond_3
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_4

    .line 2249
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoUrl_:Ljava/lang/String;

    .line 2250
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2252
    :cond_4
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    if-eqz v3, :cond_5

    .line 2253
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoThumbnailUrl_:Ljava/lang/String;

    .line 2254
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2256
    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    if-eqz v3, :cond_6

    .line 2257
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    .line 2258
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2260
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    if-eqz v3, :cond_7

    .line 2261
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    .line 2262
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2264
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-eqz v3, :cond_9

    .line 2265
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_9

    aget-object v0, v4, v3

    .line 2266
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    if-eqz v0, :cond_8

    .line 2267
    const/16 v6, 0x9

    .line 2268
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 2265
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2272
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    :cond_9
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    if-eqz v3, :cond_a

    .line 2273
    const/16 v3, 0xa

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docidString_:Ljava/lang/String;

    .line 2274
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2276
    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-eqz v3, :cond_c

    .line 2277
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    array-length v5, v4

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_c

    aget-object v0, v4, v3

    .line 2278
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    if-eqz v0, :cond_b

    .line 2279
    const/16 v6, 0xb

    .line 2280
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 2277
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2284
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    :cond_c
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x40

    if-eqz v3, :cond_d

    .line 2285
    const/16 v3, 0xc

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->detailPagePath_:Ljava/lang/String;

    .line 2286
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2288
    :cond_d
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit16 v3, v3, 0x80

    if-eqz v3, :cond_e

    .line 2289
    const/16 v3, 0xd

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->rating_:F

    .line 2290
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v1, v3

    .line 2292
    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-eqz v3, :cond_10

    .line 2293
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    array-length v5, v4

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_10

    aget-object v0, v4, v3

    .line 2294
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    if-eqz v0, :cond_f

    .line 2295
    const/16 v6, 0xf

    .line 2296
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 2293
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2300
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    if-eqz v3, :cond_12

    .line 2301
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    array-length v5, v4

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_12

    aget-object v0, v4, v3

    .line 2302
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;
    if-eqz v0, :cond_11

    .line 2303
    const/16 v6, 0x10

    .line 2304
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 2301
    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 2308
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;
    :cond_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    if-eqz v3, :cond_13

    .line 2309
    const/16 v3, 0x11

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    .line 2310
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2312
    :cond_13
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    if-eqz v3, :cond_15

    .line 2313
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    array-length v4, v3

    :goto_4
    if-ge v2, v4, :cond_15

    aget-object v0, v3, v2

    .line 2314
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    if-eqz v0, :cond_14

    .line 2315
    const/16 v5, 0x12

    .line 2316
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 2313
    :cond_14
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 2320
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    :cond_15
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->cachedSize:I

    .line 2321
    return v1
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2107
    const/16 v1, 0x11

    .line 2108
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 2109
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-nez v2, :cond_5

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 2110
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->title_:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 2111
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->subtitle_:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 2112
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->imageUrl_:Ljava/lang/String;

    if-nez v2, :cond_8

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 2113
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoUrl_:Ljava/lang/String;

    if-nez v2, :cond_9

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 2114
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoThumbnailUrl_:Ljava/lang/String;

    if-nez v2, :cond_a

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 2115
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    if-nez v2, :cond_b

    move v2, v3

    :goto_6
    add-int v1, v4, v2

    .line 2116
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    if-nez v2, :cond_c

    move v2, v3

    :goto_7
    add-int v1, v4, v2

    .line 2117
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-nez v2, :cond_d

    mul-int/lit8 v1, v1, 0x1f

    .line 2123
    :cond_0
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docidString_:Ljava/lang/String;

    if-nez v2, :cond_f

    move v2, v3

    :goto_8
    add-int v1, v4, v2

    .line 2124
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-nez v2, :cond_10

    mul-int/lit8 v1, v1, 0x1f

    .line 2130
    :cond_1
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->detailPagePath_:Ljava/lang/String;

    if-nez v2, :cond_12

    move v2, v3

    :goto_9
    add-int v1, v4, v2

    .line 2131
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->rating_:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 2132
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-nez v2, :cond_13

    mul-int/lit8 v1, v1, 0x1f

    .line 2138
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    if-nez v2, :cond_15

    mul-int/lit8 v1, v1, 0x1f

    .line 2144
    :cond_3
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    if-nez v2, :cond_17

    move v2, v3

    :goto_a
    add-int v1, v4, v2

    .line 2145
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    if-nez v2, :cond_18

    mul-int/lit8 v1, v1, 0x1f

    .line 2151
    :cond_4
    return v1

    .line 2109
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 2110
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->title_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 2111
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->subtitle_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 2112
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->imageUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 2113
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 2114
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoThumbnailUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 2115
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 2116
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 2119
    :cond_d
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2120
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    aget-object v2, v2, v0

    if-nez v2, :cond_e

    move v2, v3

    :goto_c
    add-int v1, v4, v2

    .line 2119
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 2120
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->hashCode()I

    move-result v2

    goto :goto_c

    .line 2123
    .end local v0    # "i":I
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docidString_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 2126
    :cond_10
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2127
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    aget-object v2, v2, v0

    if-nez v2, :cond_11

    move v2, v3

    :goto_e
    add-int v1, v4, v2

    .line 2126
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 2127
    :cond_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->hashCode()I

    move-result v2

    goto :goto_e

    .line 2130
    .end local v0    # "i":I
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->detailPagePath_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_9

    .line 2134
    :cond_13
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 2135
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    aget-object v2, v2, v0

    if-nez v2, :cond_14

    move v2, v3

    :goto_10
    add-int v1, v4, v2

    .line 2134
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 2135
    :cond_14
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->hashCode()I

    move-result v2

    goto :goto_10

    .line 2140
    .end local v0    # "i":I
    :cond_15
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2141
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    aget-object v2, v2, v0

    if-nez v2, :cond_16

    move v2, v3

    :goto_12
    add-int v1, v4, v2

    .line 2140
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 2141
    :cond_16
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;->hashCode()I

    move-result v2

    goto :goto_12

    .line 2144
    .end local v0    # "i":I
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;->hashCode()I

    move-result v2

    goto/16 :goto_a

    .line 2147
    :cond_18
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 2148
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    aget-object v2, v2, v0

    if-nez v2, :cond_19

    move v2, v3

    :goto_14
    add-int v1, v4, v2

    .line 2147
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 2148
    :cond_19
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->hashCode()I

    move-result v2

    goto :goto_14
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2329
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2330
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2334
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2335
    :sswitch_0
    return-object p0

    .line 2340
    :sswitch_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-nez v5, :cond_1

    .line 2341
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    .line 2343
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2347
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->title_:Ljava/lang/String;

    .line 2348
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    goto :goto_0

    .line 2352
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->subtitle_:Ljava/lang/String;

    .line 2353
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    goto :goto_0

    .line 2357
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->imageUrl_:Ljava/lang/String;

    .line 2358
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    goto :goto_0

    .line 2362
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoUrl_:Ljava/lang/String;

    .line 2363
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    goto :goto_0

    .line 2367
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoThumbnailUrl_:Ljava/lang/String;

    .line 2368
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    goto :goto_0

    .line 2372
    :sswitch_7
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    if-nez v5, :cond_2

    .line 2373
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    .line 2375
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2379
    :sswitch_8
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    if-nez v5, :cond_3

    .line 2380
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    .line 2382
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2386
    :sswitch_9
    const/16 v5, 0x4a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2387
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-nez v5, :cond_5

    move v1, v4

    .line 2388
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 2389
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-eqz v5, :cond_4

    .line 2390
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2392
    :cond_4
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 2393
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 2394
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;-><init>()V

    aput-object v6, v5, v1

    .line 2395
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2396
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2393
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2387
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    :cond_5
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    array-length v1, v5

    goto :goto_1

    .line 2399
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    :cond_6
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;-><init>()V

    aput-object v6, v5, v1

    .line 2400
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2404
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docidString_:Ljava/lang/String;

    .line 2405
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    goto/16 :goto_0

    .line 2409
    :sswitch_b
    const/16 v5, 0x5a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2410
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-nez v5, :cond_8

    move v1, v4

    .line 2411
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    .line 2412
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-eqz v5, :cond_7

    .line 2413
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2415
    :cond_7
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    .line 2416
    :goto_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_9

    .line 2417
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;-><init>()V

    aput-object v6, v5, v1

    .line 2418
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2419
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2416
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2410
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    :cond_8
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    array-length v1, v5

    goto :goto_3

    .line 2422
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    :cond_9
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;-><init>()V

    aput-object v6, v5, v1

    .line 2423
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2427
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->detailPagePath_:Ljava/lang/String;

    .line 2428
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    goto/16 :goto_0

    .line 2432
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->rating_:F

    .line 2433
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    goto/16 :goto_0

    .line 2437
    :sswitch_e
    const/16 v5, 0x7a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2438
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-nez v5, :cond_b

    move v1, v4

    .line 2439
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    .line 2440
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-eqz v5, :cond_a

    .line 2441
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2443
    :cond_a
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    .line 2444
    :goto_6
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_c

    .line 2445
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;-><init>()V

    aput-object v6, v5, v1

    .line 2446
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2447
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2444
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 2438
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    :cond_b
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    array-length v1, v5

    goto :goto_5

    .line 2450
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    :cond_c
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;-><init>()V

    aput-object v6, v5, v1

    .line 2451
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2455
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    :sswitch_f
    const/16 v5, 0x82

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2456
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    if-nez v5, :cond_e

    move v1, v4

    .line 2457
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    .line 2458
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    if-eqz v5, :cond_d

    .line 2459
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2461
    :cond_d
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    .line 2462
    :goto_8
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_f

    .line 2463
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;-><init>()V

    aput-object v6, v5, v1

    .line 2464
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2465
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2462
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 2456
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;
    :cond_e
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    array-length v1, v5

    goto :goto_7

    .line 2468
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;
    :cond_f
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;-><init>()V

    aput-object v6, v5, v1

    .line 2469
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2473
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;
    :sswitch_10
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    if-nez v5, :cond_10

    .line 2474
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    .line 2476
    :cond_10
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2480
    :sswitch_11
    const/16 v5, 0x92

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2481
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    if-nez v5, :cond_12

    move v1, v4

    .line 2482
    .restart local v1    # "i":I
    :goto_9
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    .line 2483
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    if-eqz v5, :cond_11

    .line 2484
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2486
    :cond_11
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    .line 2487
    :goto_a
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_13

    .line 2488
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;-><init>()V

    aput-object v6, v5, v1

    .line 2489
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2490
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2487
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 2481
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    :cond_12
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    array-length v1, v5

    goto :goto_9

    .line 2493
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    :cond_13
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;-><init>()V

    aput-object v6, v5, v1

    .line 2494
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2330
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6d -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1724
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2156
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-eqz v2, :cond_0

    .line 2157
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docid_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2159
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 2160
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->title_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2162
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_2

    .line 2163
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->subtitle_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2165
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_3

    .line 2166
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->imageUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2168
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_4

    .line 2169
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2171
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_5

    .line 2172
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->previewVideoThumbnailUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2174
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    if-eqz v2, :cond_6

    .line 2175
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->albumInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2177
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    if-eqz v2, :cond_7

    .line 2178
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bookInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2180
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-eqz v2, :cond_9

    .line 2181
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->offer:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_9

    aget-object v0, v3, v2

    .line 2182
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    if-eqz v0, :cond_8

    .line 2183
    const/16 v5, 0x9

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2181
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2187
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    :cond_9
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_a

    .line 2188
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->docidString_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2190
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-eqz v2, :cond_c

    .line 2191
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->child:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_c

    aget-object v0, v3, v2

    .line 2192
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    if-eqz v0, :cond_b

    .line 2193
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2191
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2197
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    :cond_c
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_d

    .line 2198
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->detailPagePath_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2200
    :cond_d
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_e

    .line 2201
    const/16 v2, 0xd

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->rating_:F

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 2203
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-eqz v2, :cond_10

    .line 2204
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->images:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_10

    aget-object v0, v3, v2

    .line 2205
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    if-eqz v0, :cond_f

    .line 2206
    const/16 v5, 0xf

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2204
    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2210
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    :cond_10
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    if-eqz v2, :cond_12

    .line 2211
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->snippets:[Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_12

    aget-object v0, v3, v2

    .line 2212
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;
    if-eqz v0, :cond_11

    .line 2213
    const/16 v5, 0x10

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2211
    :cond_11
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2217
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsFinsky$Snippet;
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    if-eqz v2, :cond_13

    .line 2218
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->appInfo_:Lcom/google/apps/dots/proto/client/DotsFinsky$AppInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2220
    :cond_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    if-eqz v2, :cond_15

    .line 2221
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->videos:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_15

    aget-object v0, v2, v1

    .line 2222
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    if-eqz v0, :cond_14

    .line 2223
    const/16 v4, 0x12

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2221
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2227
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    :cond_15
    return-void
.end method
