.class public final enum Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;
.super Ljava/lang/Enum;
.source "StoreRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VersionConstraint"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

.field public static final enum ANY:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

.field public static final enum AVAILABLE:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

.field public static final enum FRESH:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

.field public static final enum REALLY_FRESH:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->AVAILABLE:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    .line 22
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    const-string v1, "ANY"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->ANY:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    const-string v1, "FRESH"

    invoke-direct {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->FRESH:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    const-string v1, "REALLY_FRESH"

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->REALLY_FRESH:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->AVAILABLE:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->ANY:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->FRESH:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->REALLY_FRESH:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->$VALUES:[Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->$VALUES:[Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    return-object v0
.end method
