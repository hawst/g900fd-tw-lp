.class Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;
.super Ljava/lang/Object;
.source "HttpContentServiceImpl.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HandleImpl"
.end annotation


# instance fields
.field private final contentBaseUri:Landroid/net/Uri;

.field private final exportedContentBaseUri:Landroid/net/Uri;

.field private final requestToken:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

.field private final throwOnFinalizeIfNotUnbound:Ljava/lang/Throwable;

.field private unbound:Z

.field private final webDataBaseUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 4
    .param p2, "requestToken"    # Ljava/lang/String;
    .param p3, "contentBaseUri"    # Landroid/net/Uri;
    .param p4, "exportedContentBaseUri"    # Landroid/net/Uri;
    .param p5, "webDataBaseUri"    # Landroid/net/Uri;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->this$0:Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Missing call to HttpContentServiceImpl.HandleImpl("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ").unbind()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->throwOnFinalizeIfNotUnbound:Ljava/lang/Throwable;

    .line 79
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->requestToken:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->contentBaseUri:Landroid/net/Uri;

    .line 81
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->exportedContentBaseUri:Landroid/net/Uri;

    .line 82
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->webDataBaseUri:Landroid/net/Uri;

    .line 83
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTokens:Ljava/util/Collection;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$000(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 84
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 111
    :try_start_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->unbound:Z

    if-nez v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->throwOnFinalizeIfNotUnbound:Ljava/lang/Throwable;

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 117
    return-void
.end method

.method public getContentBaseUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->contentBaseUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getExportedContentBaseUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->exportedContentBaseUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getWebDataBaseUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->webDataBaseUri:Landroid/net/Uri;

    return-object v0
.end method

.method public unbind()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 103
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Unbinding %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->requestToken:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->this$0:Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->requestToken:Ljava/lang/String;

    # invokes: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->revokeToken(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$200(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;Ljava/lang/String;)V

    .line 105
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;->unbound:Z

    .line 106
    return-void
.end method
