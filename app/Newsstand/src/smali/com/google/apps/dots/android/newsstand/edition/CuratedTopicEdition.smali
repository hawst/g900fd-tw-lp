.class public Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;
.super Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;
.source "CuratedTopicEdition.java"


# instance fields
.field private clientEntity:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V
    .locals 0
    .param p1, "editionProto"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 36
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "appFamilyId"    # Ljava/lang/String;
    .param p2, "appId"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "optLeadCurationClientEntityId"    # Ljava/lang/String;

    .prologue
    .line 40
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setType(I)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;-><init>()V

    .line 41
    invoke-virtual {v1, p1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    move-result-object v1

    .line 42
    invoke-virtual {v1, p3}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->setDescription(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    move-result-object v1

    .line 43
    invoke-virtual {v1, p4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->setLeadCurationClientEntityId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setCuratedTopic(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 45
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    return-void
.end method

.method public static getIdToUseForColor(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Ljava/lang/String;
    .locals 1
    .param p0, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasLeadCurationClientEntityId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getLeadCurationClientEntityId()Ljava/lang/String;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public actionBarColor(Landroid/content/Context;Z)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "subscribed"    # Z

    .prologue
    .line 87
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getColorResourceId(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public getAppFamilyId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getCuratedTopic()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->getAppFamilyId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getCuratedTopic()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->getAppId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getColorResourceId(Z)I
    .locals 3
    .param p1, "subscribed"    # Z

    .prologue
    .line 91
    if-eqz p1, :cond_1

    .line 92
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getAppFamilyId()Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "idToUseForColor":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 94
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getCuratedTopic()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->getLeadCurationClientEntityId()Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "leadCurationClientEntityId":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 96
    move-object v0, v1

    .line 98
    :cond_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorResIdForEditionId(Ljava/lang/String;)I

    move-result v2

    .line 100
    .end local v0    # "idToUseForColor":Ljava/lang/String;
    .end local v1    # "leadCurationClientEntityId":Ljava/lang/String;
    :goto_0
    return v2

    :cond_1
    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->actionbar_grey:I

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getCuratedTopic()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->getDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 111
    invoke-static {p1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curatedTopicList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;)Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;

    move-result-object v0

    return-object v0
.end method

.method public getIgnoreEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->clientEntity:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    .line 66
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getAppFamilyId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->setCurationAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v0

    .line 67
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->setCurationAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->clientEntity:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->clientEntity:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    return-object v0
.end method

.method public headerImageList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    const/4 v5, 0x0

    .line 116
    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasHeroShotImage()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 117
    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getHeroShotImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v0

    .line 118
    .local v0, "heroShotImage":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    new-instance v1, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v1}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 119
    .local v1, "heroShotImageData":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->DK_IMAGE_ID:I

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 120
    sget v2, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->DK_ANIMATE_IMAGE:I

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 121
    new-instance v2, Lcom/google/android/libraries/bind/data/DataList;

    sget v3, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->DK_IMAGE_ID:I

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/android/libraries/bind/data/Data;

    aput-object v1, v4, v5

    invoke-static {v4}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/libraries/bind/data/DataList;-><init>(ILjava/util/List;)V

    .line 123
    .end local v0    # "heroShotImage":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .end local v1    # "heroShotImageData":Lcom/google/android/libraries/bind/data/Data;
    :goto_0
    return-object v2

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->headerImageList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    goto :goto_0
.end method

.method public readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 74
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getCurationEditions(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "uri":Ljava/lang/String;
    return-object v0
.end method

.method public syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    .line 81
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->readStatesUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    .line 80
    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 128
    const-string v0, "%s - curated topic: %s (appFamilyId: %s, appId: %s)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 129
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getAppFamilyId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getAppId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 128
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public trackAnalytics(I)V
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 106
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/CuratedTopicEditionScreen;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/CuratedTopicEditionScreen;-><init>(Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/CuratedTopicEditionScreen;->track(Z)V

    .line 107
    return-void
.end method
