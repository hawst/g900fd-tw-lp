.class public abstract Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;
.super Ljava/lang/Object;
.source "StoreCacheBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final extractItem:Lcom/google/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Function",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
            "<TT;>;TT;>;"
        }
    .end annotation
.end field

.field private nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field private type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method protected constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V
    .locals 1
    .param p1, "nsStore"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .prologue
    .line 40
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;, "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->extractItem:Lcom/google/common/base/Function;

    .line 41
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 42
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 43
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;)Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    return-object v0
.end method


# virtual methods
.method protected cache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    .locals 1

    .prologue
    .line 46
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;, "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase<TT;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->storeCache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v0

    return-object v0
.end method

.method public get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;, "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase<TT;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 54
    invoke-virtual {p0, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->makeDefaultStoreRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->getCacheItem(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->extractItem:Lcom/google/common/base/Function;

    .line 53
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "priority"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;, "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase<TT;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 78
    invoke-virtual {p0, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->makeDefaultStoreRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->getCacheItem(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->extractItem:Lcom/google/common/base/Function;

    .line 77
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public getAny(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;, "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase<TT;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 60
    invoke-virtual {p0, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->makeDefaultStoreRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->anyVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->getCacheItem(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->extractItem:Lcom/google/common/base/Function;

    .line 59
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public getAvailable(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 68
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;, "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase<TT;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 69
    invoke-virtual {p0, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->makeDefaultStoreRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->availableVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->getCacheItem(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->extractItem:Lcom/google/common/base/Function;

    .line 68
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public getCacheItem(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;, "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase<TT;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 95
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->cache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v1

    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {v1, v2, p2}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->get(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    move-result-object v0

    .line 96
    .local v0, "cacheItem":Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;, "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem<*>;"
    if-eqz v0, :cond_0

    .line 97
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->cachePolicy()Lcom/google/apps/dots/android/newsstand/store/CachePolicy;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    invoke-virtual {v1, v2, p2}, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;->mayUseCachedVersion(Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 113
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 100
    :pswitch_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 116
    :cond_0
    :pswitch_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "%s not in memory cache"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 118
    invoke-virtual {v1, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;

    invoke-direct {v2, p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase$2;-><init>(Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSSTORE_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/async/Queue;->fallbackIfMain:Ljava/util/concurrent/Executor;

    .line 117
    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    :goto_0
    return-object v1

    .line 106
    :cond_1
    :pswitch_2
    sget-object v1, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "%s: Found %s in memory-cache"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected makeDefaultStoreRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .prologue
    .line 142
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;, "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase<TT;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-direct {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    return-object v0
.end method

.method protected abstract parse(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
