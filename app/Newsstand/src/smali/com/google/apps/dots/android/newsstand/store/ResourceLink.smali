.class public Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
.super Ljava/lang/Object;
.source "ResourceLink.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field public final id:Ljava/lang/String;

.field public final type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

.field public final uri:Landroid/net/Uri;

.field public final uriType:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

.field public final version:Lcom/google/apps/dots/android/newsstand/store/Version;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/store/Version;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "version"    # Lcom/google/apps/dots/android/newsstand/store/Version;
    .param p5, "uriType"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->id:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 42
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->uri:Landroid/net/Uri;

    .line 43
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    .line 44
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->uriType:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    .line 45
    return-void
.end method

.method public static fromLink(Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 8
    .param p0, "link"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->hasUri()Z

    move-result v0

    if-nez v0, :cond_1

    .line 28
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Skipping link %s --> %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getUri()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    const/4 v0, 0x0

    .line 36
    :goto_0
    return-object v0

    .line 31
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    .line 32
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getId()Ljava/lang/String;

    move-result-object v1

    .line 33
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getLinkType()I

    move-result v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    move-result-object v2

    .line 34
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/store/Version;

    .line 35
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getVersion()J

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Lcom/google/apps/dots/android/newsstand/store/Version;-><init>(J)V

    .line 36
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getUriType()I

    move-result v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/store/Version;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;)V

    goto :goto_0
.end method


# virtual methods
.method public finishUri(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 7
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "serverUris"    # Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .param p3, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->uri:Landroid/net/Uri;

    invoke-virtual {p2, p1, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->qualifyRelativeSyncUri(Landroid/accounts/Account;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    .line 49
    .local v3, "qualifiedUri":Landroid/net/Uri;
    if-nez p3, :cond_0

    .line 52
    :cond_0
    if-eqz p3, :cond_2

    .line 53
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->uriType:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->LOCAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->uriType:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->FIFE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    if-ne v0, v1, :cond_2

    .line 54
    :cond_1
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v3}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 59
    :cond_2
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->uriType:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/store/Version;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 64
    const-class v0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->id:Ljava/lang/String;

    .line 65
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "type"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 66
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "uri"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->uri:Landroid/net/Uri;

    .line 67
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "version"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    .line 68
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "uriType"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->uriType:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    .line 69
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
