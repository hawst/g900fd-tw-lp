.class Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1$1;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;
.source "OnboardQuizMagazineOffersPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;->load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;

.field final synthetic val$offerEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1$1;->val$offerEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1$1;->val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
    .locals 4

    .prologue
    .line 105
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;

    const-string v1, "Onboard-Magazines-Offers"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1$1;->val$offerEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1$1;->val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1$1;->get()Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;

    move-result-object v0

    return-object v0
.end method
