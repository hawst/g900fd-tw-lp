.class Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$7;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "NewsWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->triggerPageViewInJSIfNeeded(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

.field final synthetic val$pageNumber:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .prologue
    .line 631
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    iput p2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$7;->val$pageNumber:I

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 634
    const-string v1, "dots.layout.triggerPageView && dots.layout.triggerPageView(%s);"

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$7;->val$pageNumber:I

    .line 635
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 634
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 636
    .local v0, "stmt":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    new-array v2, v5, [Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->executeStatements([Ljava/lang/String;)V

    .line 637
    return-void
.end method
