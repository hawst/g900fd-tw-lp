.class public Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
.super Ljava/lang/Object;
.source "CachingBitmapPool.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/bitmap/BitmapPool;


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache",
            "<",
            "Ljava/lang/Object;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final pool:Lcom/google/android/libraries/bind/collections/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/bind/collections/RingBuffer",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final poolLock:Ljava/lang/Object;

.field private poolMaxSizeKb:I

.field private poolSizeKb:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;)V
    .locals 3
    .param p1, "util"    # Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .prologue
    .line 53
    const v1, 0x3ecccccd    # 0.4f

    .line 54
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/newsstanddev/R$bool;->enable_large_heap:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0xc350

    .line 53
    :goto_0
    invoke-static {p1, v1, v0}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->computeScaledSizeKb(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;FI)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;-><init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;I)V

    .line 56
    return-void

    .line 54
    :cond_0
    const/16 v0, 0x7530

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;I)V
    .locals 5
    .param p1, "util"    # Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
    .param p2, "cacheMaxSizeKb"    # I

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {}, Lcom/google/android/libraries/bind/collections/RingBuffer;->create()Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolLock:Ljava/lang/Object;

    .line 59
    const v0, 0x3dcccccd    # 0.1f

    const v1, 0xc350

    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->computeScaledSizeKb(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;FI)I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolMaxSizeKb:I

    .line 60
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Creating %d kB pool, %d kB image cache"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolMaxSizeKb:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$1;

    invoke-direct {v0, p0, p2}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$1;-><init>(Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;I)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;Landroid/graphics/Bitmap;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getBitmapSizeKb(Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method

.method private static computeScaledSizeKb(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;FI)I
    .locals 2
    .param p0, "util"    # Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
    .param p1, "memClassFraction"    # F
    .param p2, "maxSizeKb"    # I

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getPerApplicationMemoryClass()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    const/high16 v1, 0x44800000    # 1024.0f

    mul-float/2addr v0, v1

    int-to-float v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method private getBitmapSizeKb(Landroid/graphics/Bitmap;)I
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 91
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;->getBitmapSizeBytes(Landroid/graphics/Bitmap;)I

    move-result v0

    div-int/lit16 v0, v0, 0x400

    return v0
.end method

.method private static isGoodForMutableBitmapRequest(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)Z
    .locals 8
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "requestWidth"    # I
    .param p2, "requestHeight"    # I
    .param p3, "requestConfig"    # Landroid/graphics/Bitmap$Config;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 106
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v5

    if-nez v5, :cond_1

    move v3, v4

    .line 118
    :cond_0
    :goto_0
    return v3

    .line 109
    :cond_1
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x13

    if-lt v5, v6, :cond_2

    .line 111
    mul-int v5, p1, p2

    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;->bytesPerPixel(Landroid/graphics/Bitmap$Config;)I

    move-result v6

    mul-int v2, v5, v6

    .line 112
    .local v2, "requiredSize":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v5

    int-to-long v0, v5

    .line 113
    .local v0, "actualSize":J
    int-to-long v6, v2

    cmp-long v5, v6, v0

    if-lez v5, :cond_0

    move v3, v4

    goto :goto_0

    .line 116
    .end local v0    # "actualSize":J
    .end local v2    # "requiredSize":I
    :cond_2
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    if-ne v5, p3, :cond_3

    .line 117
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-ne v5, p1, :cond_3

    .line 118
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-eq v5, p2, :cond_0

    :cond_3
    move v3, v4

    goto :goto_0
.end method

.method private logState()V
    .locals 5

    .prologue
    .line 292
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "%d in pool (%dK), cache size (%dK)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/collections/RingBuffer;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 293
    return-void
.end method

.method private static reconfigureBitmap(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)Z
    .locals 7
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "requestWidth"    # I
    .param p2, "requestHeight"    # I
    .param p3, "requestConfig"    # Landroid/graphics/Bitmap$Config;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 125
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v3, v4, :cond_0

    .line 127
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Landroid/graphics/Bitmap;->reconfigure(IILandroid/graphics/Bitmap$Config;)V

    .line 128
    sget-object v3, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Returning reconfigured bitmap"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_0
    :goto_0
    return v1

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "iae":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Couldn\'t reconfigure %sx%s:%s to %sx%s:%s"

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    .line 132
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x3

    .line 133
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x5

    aput-object p3, v5, v1

    .line 131
    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v2

    .line 134
    goto :goto_0
.end method


# virtual methods
.method public getCachedBitmap(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getPoolBitmap(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    .locals 26
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;
    .param p4, "allocateIfNotFound"    # Z

    .prologue
    .line 148
    const/4 v13, 0x0

    .line 149
    .local v13, "result":Landroid/graphics/Bitmap;
    mul-int v19, p1, p2

    invoke-static/range {p3 .. p3}, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;->bytesPerPixel(Landroid/graphics/Bitmap$Config;)I

    move-result v20

    mul-int v17, v19, v20

    .line 150
    .local v17, "targetSize":I
    const/4 v14, -0x1

    .line 151
    .local v14, "resultIndex":I
    const v16, 0x7fffffff

    .line 153
    .local v16, "resultSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolLock:Ljava/lang/Object;

    move-object/from16 v20, v0

    monitor-enter v20

    .line 154
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/libraries/bind/collections/RingBuffer;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v12, v0, :cond_0

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lcom/google/android/libraries/bind/collections/RingBuffer;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    .line 156
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    move/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v6, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->isGoodForMutableBitmapRequest(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 157
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;->getBitmapSizeBytes(Landroid/graphics/Bitmap;)I

    move-result v7

    .line 158
    .local v7, "bitmapSize":I
    move/from16 v0, v16

    if-ge v7, v0, :cond_7

    .line 159
    move v14, v12

    .line 160
    move/from16 v16, v7

    .line 161
    sub-int v19, v16, v17

    const v21, 0x8000

    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_7

    .line 168
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    .end local v7    # "bitmapSize":I
    :cond_0
    if-ltz v14, :cond_1

    .line 169
    sget-object v19, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v21, "Found pool bitmap for %d x %d"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Lcom/google/android/libraries/bind/collections/RingBuffer;->remove(I)Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v0, v19

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v13, v0

    .line 171
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    move/from16 v19, v0

    move/from16 v0, v16

    div-int/lit16 v0, v0, 0x400

    move/from16 v21, v0

    sub-int v19, v19, v21

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    .line 173
    :cond_1
    monitor-exit v20
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    if-nez v13, :cond_4

    .line 177
    const/4 v15, 0x0

    .line 178
    .local v15, "resultKey":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    move-object/from16 v20, v0

    monitor-enter v20

    .line 179
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->wrapperSnapshot()Ljava/util/Map;

    move-result-object v10

    .line 180
    .local v10, "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 181
    .local v8, "currentTimeMs":J
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .end local v15    # "resultKey":Ljava/lang/Object;
    :cond_2
    :goto_1
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_3

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map$Entry;

    .line 182
    .local v11, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;>;"
    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;

    .line 183
    .local v18, "wrapper":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;"
    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;->value:Ljava/lang/Object;

    check-cast v6, Landroid/graphics/Bitmap;

    .line 184
    .restart local v6    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;->timestamp:J

    move-wide/from16 v22, v0

    sub-long v22, v8, v22

    const-wide/32 v24, 0xea60

    cmp-long v19, v22, v24

    if-lez v19, :cond_8

    .line 185
    move/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v6, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->isGoodForMutableBitmapRequest(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 186
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;->getBitmapSizeBytes(Landroid/graphics/Bitmap;)I

    move-result v7

    .line 187
    .restart local v7    # "bitmapSize":I
    move/from16 v0, v16

    if-ge v7, v0, :cond_8

    .line 188
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    .line 189
    .restart local v15    # "resultKey":Ljava/lang/Object;
    move/from16 v16, v7

    .line 190
    sub-int v19, v16, v17

    const v22, 0x8000

    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_8

    .line 201
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    .end local v7    # "bitmapSize":I
    .end local v11    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;>;"
    .end local v15    # "resultKey":Ljava/lang/Object;
    .end local v18    # "wrapper":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;"
    :cond_3
    monitor-exit v20
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 204
    .end local v8    # "currentTimeMs":J
    .end local v10    # "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;>;"
    :cond_4
    if-eqz v13, :cond_5

    .line 205
    move/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v13, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->reconfigureBitmap(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)Z

    move-result v19

    if-nez v19, :cond_5

    .line 207
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 208
    const/4 v13, 0x0

    .line 212
    :cond_5
    if-nez v13, :cond_6

    if-eqz p4, :cond_6

    .line 213
    new-instance v19, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;-><init>(Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;IILandroid/graphics/Bitmap$Config;)V

    .line 224
    invoke-virtual/range {v19 .. v19}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;->run()Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "result":Landroid/graphics/Bitmap;
    check-cast v13, Landroid/graphics/Bitmap;

    .line 226
    .restart local v13    # "result":Landroid/graphics/Bitmap;
    if-nez v13, :cond_6

    .line 227
    sget-object v19, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v20, "Trouble allocating bitmap %d x %d"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-virtual/range {v19 .. v21}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->logState()V

    .line 232
    return-object v13

    .line 154
    .restart local v6    # "bitmap":Landroid/graphics/Bitmap;
    :cond_7
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 173
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v19

    :try_start_2
    monitor-exit v20
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v19

    .line 196
    .restart local v6    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v8    # "currentTimeMs":J
    .restart local v10    # "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;>;"
    .restart local v11    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;>;"
    .restart local v18    # "wrapper":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;"
    :cond_8
    if-eqz v15, :cond_2

    .line 197
    :try_start_3
    sget-object v19, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v22, "Stealing bitmap for %d x %d"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v0, v19

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v13, v0

    goto/16 :goto_1

    .line 201
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    .end local v8    # "currentTimeMs":J
    .end local v10    # "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;>;"
    .end local v11    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;>;"
    .end local v18    # "wrapper":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<Landroid/graphics/Bitmap;>;"
    :catchall_1
    move-exception v19

    monitor-exit v20
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v19
.end method

.method public isInCache(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOverLimit()Z
    .locals 2

    .prologue
    .line 236
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolMaxSizeKb:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseBitmap(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v5, 0x0

    .line 248
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 251
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Attempted to release a non-mutable bitmap into a bitmap pool"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 252
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 266
    :goto_0
    return-void

    .line 255
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Releasing %d x %d bitmap"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolLock:Ljava/lang/Object;

    monitor-enter v2

    .line 257
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->addFirst(Ljava/lang/Object;)V

    .line 258
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getBitmapSizeKb(Landroid/graphics/Bitmap;)I

    move-result v3

    add-int/2addr v1, v3

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    .line 259
    :goto_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->isOverLimit()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 260
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 261
    .local v0, "last":Landroid/graphics/Bitmap;
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getBitmapSizeKb(Landroid/graphics/Bitmap;)I

    move-result v3

    sub-int/2addr v1, v3

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    .line 262
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 264
    .end local v0    # "last":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->logState()V

    goto :goto_0
.end method

.method public releaseBitmap(Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 240
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Releasing %s, %d x %d bitmap"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    return-void
.end method

.method public trim(F)V
    .locals 7
    .param p1, "fraction"    # F

    .prologue
    .line 270
    sget-object v4, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Purging."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    monitor-enter v5

    .line 272
    :try_start_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->size()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p1

    float-to-int v3, v4

    .line 273
    .local v3, "newSizeLimit":I
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    .line 274
    .local v0, "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Landroid/graphics/Bitmap;>;"
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 275
    .local v1, "key":Ljava/lang/Object;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    invoke-virtual {v4, v1}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 276
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->cache:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->size()I

    move-result v4

    if-gt v4, v3, :cond_0

    .line 280
    .end local v1    # "key":Ljava/lang/Object;
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 281
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolLock:Ljava/lang/Object;

    monitor-enter v5

    .line 282
    :try_start_1
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    int-to-float v4, v4

    mul-float/2addr v4, p1

    float-to-int v3, v4

    .line 283
    :goto_0
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    if-le v4, v3, :cond_2

    .line 284
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/collections/RingBuffer;->removeLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 285
    .local v2, "last":Landroid/graphics/Bitmap;
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getBitmapSizeKb(Landroid/graphics/Bitmap;)I

    move-result v6

    sub-int/2addr v4, v6

    iput v4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->poolSizeKb:I

    .line 286
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 288
    .end local v2    # "last":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 280
    .end local v0    # "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Landroid/graphics/Bitmap;>;"
    .end local v3    # "newSizeLimit":I
    :catchall_1
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v4

    .line 288
    .restart local v0    # "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Landroid/graphics/Bitmap;>;"
    .restart local v3    # "newSizeLimit":I
    :cond_2
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 289
    return-void
.end method
