.class Lcom/google/apps/dots/android/newsstand/store/NSStore$8;
.super Ljava/lang/Object;
.source "NSStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/NSStore;->download(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

.field final synthetic val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 513
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .param p1, "clientResponse"    # Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v9, 0x1

    .line 518
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "%s: got response %s"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    aput-object v5, v4, v6

    aput-object p1, v4, v9

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 520
    :try_start_0
    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->data:Ljava/io/InputStream;

    if-eqz v2, :cond_0

    .line 521
    const-string v2, "NSStore-stream"

    const-string v3, "%s:%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523
    :try_start_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->data:Ljava/io/InputStream;

    invoke-interface {v2, v3}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->writeStream(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 525
    :try_start_2
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 528
    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    const-wide/16 v2, 0x0

    .line 529
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p1, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->eTag:Ljava/lang/String;

    iget-object v7, p1, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->lastModified:Ljava/lang/Long;

    iget-object v8, p1, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->expiration:Ljava/lang/Long;

    invoke-direct/range {v1 .. v8}, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;-><init>(JJLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V

    .line 531
    .local v1, "newMetadata":Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-interface {v2, v1}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->setMetadata(Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)V

    .line 533
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 534
    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getBlobFileVersion(Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/apps/dots/android/newsstand/store/Version;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;-><init>(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;Lcom/google/apps/dots/android/newsstand/store/Version;)V

    .line 536
    .local v0, "storeResponse":Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->invalidateStoreCache(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V
    invoke-static {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$1400(Lcom/google/apps/dots/android/newsstand/store/NSStore;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    .line 537
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-virtual {v2, v3, v4, v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->notifyContentProvider(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V

    .line 539
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    .line 541
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->data:Ljava/io/InputStream;

    invoke-static {v3, v9}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    return-object v2

    .line 525
    .end local v0    # "storeResponse":Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    .end local v1    # "newMetadata":Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 541
    :catchall_1
    move-exception v2

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->data:Ljava/io/InputStream;

    invoke-static {v3, v9}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    throw v2
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 513
    check-cast p1, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;->apply(Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
