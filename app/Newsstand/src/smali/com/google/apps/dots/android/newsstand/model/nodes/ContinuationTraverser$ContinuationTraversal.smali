.class public final Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/BaseTraversal;
.source "ContinuationTraverser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContinuationTraversal"
.end annotation


# instance fields
.field requestedTraverseContinuation:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/BaseTraversal;-><init>()V

    return-void
.end method


# virtual methods
.method clearRequest()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;->requestedTraverseContinuation:Z

    .line 48
    return-void
.end method
