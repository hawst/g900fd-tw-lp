.class public Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "VideoIntentBuilder.java"


# instance fields
.field private autoStart:Z

.field private closeOnCompletion:Z

.field private final intent:Landroid/content/Intent;

.field private owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private postId:Ljava/lang/String;

.field private readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 25
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->intent:Landroid/content/Intent;

    .line 26
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 60
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    if-nez v0, :cond_2

    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->postId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "postId"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->postId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    :goto_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "owningEdition"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "readingEdition"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 68
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->autoStart:Z

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "autoStart"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 71
    :cond_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->closeOnCompletion:Z

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "closeOnCompletion"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->intent:Landroid/content/Intent;

    return-object v0

    .line 64
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "videoItem"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public setAutoStart(Z)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;
    .locals 0
    .param p1, "autoStart"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->autoStart:Z

    .line 40
    return-object p0
.end method

.method public setCloseOnCompletion(Z)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;
    .locals 0
    .param p1, "closeOnCompletion"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->closeOnCompletion:Z

    .line 45
    return-object p0
.end method

.method public setOwningEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 50
    return-object p0
.end method

.method public setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;
    .locals 0
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->postId:Ljava/lang/String;

    .line 35
    return-object p0
.end method

.method public setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 55
    return-object p0
.end method

.method public setVideoItem(Lcom/google/apps/dots/android/newsstand/media/VideoItem;)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;
    .locals 0
    .param p1, "videoItem"    # Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    .line 30
    return-object p0
.end method
