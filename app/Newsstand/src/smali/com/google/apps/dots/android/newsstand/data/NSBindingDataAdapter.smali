.class public Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;
.super Lcom/google/android/libraries/bind/data/BindingDataAdapter;
.source "NSBindingDataAdapter.java"


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 1
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 13
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->viewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;-><init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V

    .line 14
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;->setEmptyViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 15
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSErrorViewProvider;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/data/NSErrorViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 16
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;->setLoadingViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 17
    return-void
.end method
