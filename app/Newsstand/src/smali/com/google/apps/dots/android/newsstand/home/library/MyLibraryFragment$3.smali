.class Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$3;
.super Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
.source "MyLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->setupPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;Landroid/support/v4/app/FragmentManager;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;
    .param p2, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;
    .param p3, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-void
.end method


# virtual methods
.method public createFragment(ILcom/google/android/libraries/bind/data/Data;)Landroid/support/v4/app/Fragment;
    .locals 3
    .param p1, "logicalPosition"    # I
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 175
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .line 176
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    sget v2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DK_LIBRARY_PAGE:I

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 177
    .local v0, "libraryPage":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    return-object v1
.end method
