.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Point"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;


# instance fields
.field private bitField0_:I

.field private x_:I

.field private y_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 15
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->x_:I

    .line 34
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->y_:I

    .line 10
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    .locals 3

    .prologue
    .line 64
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    return-object v0

    .line 65
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 73
    if-ne p1, p0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v1

    .line 74
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 75
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    .line 76
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->x_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->x_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->y_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->y_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 101
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 102
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->x_:I

    .line 103
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 106
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->y_:I

    .line 107
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->cachedSize:I

    .line 110
    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->x_:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->y_:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 81
    const/16 v0, 0x11

    .line 82
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 83
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->x_:I

    add-int v0, v1, v2

    .line 84
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->y_:I

    add-int v0, v1, v2

    .line 85
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 119
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 123
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 124
    :sswitch_0
    return-object p0

    .line 129
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->x_:I

    .line 130
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    goto :goto_0

    .line 134
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->y_:I

    .line 135
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    goto :goto_0

    .line 119
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v0

    return-object v0
.end method

.method public setX(I)Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 20
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->x_:I

    .line 21
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    .line 22
    return-object p0
.end method

.method public setY(I)Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->y_:I

    .line 40
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    .line 41
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->x_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 93
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 94
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->y_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 96
    :cond_1
    return-void
.end method
