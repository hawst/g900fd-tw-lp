.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "TitleIssuesActivity.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private titleIssuesFragment:Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>()V

    return-void
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 42
    if-nez p1, :cond_0

    .line 46
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->titleIssuesFragment:Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->handleExtras(Landroid/os/Bundle;)Z

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->titleIssuesFragment:Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->titleIssuesFragment:Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->title_issues_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->setContentView(I)V

    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->title_issues_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->titleIssuesFragment:Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    .line 31
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 32
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 36
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onNewIntent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 38
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 39
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 56
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->onOptionsItemSelectedCloseDrawerIfNeeded(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    :goto_0
    return v0

    .line 58
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 59
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;->supportFinishAfterTransition()V

    goto :goto_0

    .line 62
    :cond_1
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
