.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;
.super Ljava/lang/Object;
.source "MeterDialog.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;

.field final synthetic val$offerLine:Landroid/view/View;

.field final synthetic val$progress:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;

    .prologue
    .line 615
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;->val$offerLine:Landroid/view/View;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;->val$progress:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    .line 618
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 619
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->refreshMyNews()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$1300(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V

    .line 620
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->hide(Z)V

    .line 621
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredSubscriptionPurchasedScreen;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$000(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredSubscriptionPurchasedScreen;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredSubscriptionPurchasedScreen;->track(Z)V

    .line 623
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;->val$offerLine:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;->val$progress:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 624
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;->val$progress:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 625
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;->val$offerLine:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 627
    :cond_1
    return-void
.end method
