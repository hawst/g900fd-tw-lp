.class public Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;
.source "YearMonthIssuesIntentBuilder.java"


# instance fields
.field private endMonth:I

.field private hideArchive:Z

.field private month:I

.field private year:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 17
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->year:I

    .line 18
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->month:I

    .line 19
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->endMonth:I

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->hideArchive:Z

    .line 30
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 7

    .prologue
    .line 66
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->year:I

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 67
    const-class v1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesActivity;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 68
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "YearMonthIssuesFragment_state"

    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->year:I

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->month:I

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->endMonth:I

    iget-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->hideArchive:Z

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;-><init>(IIIZ)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 70
    return-object v0

    .line 66
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEndMonth(I)Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;
    .locals 0
    .param p1, "endMonth"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->endMonth:I

    .line 60
    return-object p0
.end method

.method public setHideArchive(Z)Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;
    .locals 0
    .param p1, "hideArchive"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->hideArchive:Z

    .line 43
    return-object p0
.end method

.method public setMonth(I)Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;
    .locals 0
    .param p1, "month"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->month:I

    .line 51
    return-object p0
.end method

.method public setYear(I)Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;
    .locals 0
    .param p1, "year"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->year:I

    .line 38
    return-object p0
.end method
