.class public interface abstract Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;
.super Ljava/lang/Object;
.source "DelayedContentWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;,
        Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    }
.end annotation


# virtual methods
.method public abstract getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
.end method

.method public abstract loadDelayedContents(Ljava/lang/Runnable;)V
.end method

.method public abstract setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V
.end method
