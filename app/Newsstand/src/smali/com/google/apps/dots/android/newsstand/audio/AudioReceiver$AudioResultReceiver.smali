.class Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;
.super Landroid/os/ResultReceiver;
.source "AudioReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AudioResultReceiver"
.end annotation


# instance fields
.field audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;)V
    .locals 1
    .param p1, "audioReceiver"    # Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    .prologue
    .line 61
    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->handler:Landroid/os/Handler;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->access$100(Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;)Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/ResultReceiver;-><init>(Landroid/os/Handler;)V

    .line 62
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    .line 63
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    .line 67
    return-void
.end method

.method protected onReceiveResult(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "resultCode"    # I
    .param p2, "resultData"    # Landroid/os/Bundle;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->onReceiveUpdate(Landroid/os/Bundle;)V

    .line 74
    :cond_0
    return-void
.end method
