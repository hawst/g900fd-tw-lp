.class public Lcom/google/apps/dots/android/newsstand/datasource/DataSources;
.super Ljava/lang/Object;
.source "DataSources.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static combinedSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

.field private static final curatedTopicLists:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;",
            "Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;",
            ">;"
        }
    .end annotation
.end field

.field private static curationSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;

.field private static defaultSubscriptionsList:Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;

.field private static libraryPageList:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;

.field private static magazineSubscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;

.field private static newsSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;

.field private static final normalEditionListCache:Lcom/google/common/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/cache/Cache",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            "Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;",
            ">;"
        }
    .end annotation
.end field

.field private static offersList:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

.field private static pinnedList:Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

.field private static rawOffersList:Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;

.field private static readNowList:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

.field private static final readStateListCache:Lcom/google/common/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/cache/Cache",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            "Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;",
            ">;"
        }
    .end annotation
.end field

.field private static recentMagazinesDataList:Lcom/google/apps/dots/android/newsstand/home/library/magazines/RecentMagazinesDataList;

.field private static recentlyReadList:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

.field private static savedList:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

.field private static final sectionEditionListCache:Lcom/google/common/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/cache/Cache",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            "Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;",
            ">;"
        }
    .end annotation
.end field

.field private static final sectionListCache:Lcom/google/common/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/cache/Cache",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            "Lcom/google/apps/dots/android/newsstand/datasource/SectionList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x5

    const/4 v1, 0x1

    .line 41
    const-class v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 57
    invoke-static {}, Lcom/google/common/cache/CacheBuilder;->newBuilder()Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/common/cache/CacheBuilder;->concurrencyLevel(I)Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    const-wide/16 v2, 0x3

    .line 58
    invoke-virtual {v0, v2, v3}, Lcom/google/common/cache/CacheBuilder;->maximumSize(J)Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/cache/CacheBuilder;->build()Lcom/google/common/cache/Cache;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->normalEditionListCache:Lcom/google/common/cache/Cache;

    .line 70
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curatedTopicLists:Ljava/util/Map;

    .line 79
    invoke-static {}, Lcom/google/common/cache/CacheBuilder;->newBuilder()Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/common/cache/CacheBuilder;->concurrencyLevel(I)Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    const-wide/16 v2, 0xa

    .line 80
    invoke-virtual {v0, v2, v3}, Lcom/google/common/cache/CacheBuilder;->maximumSize(J)Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/cache/CacheBuilder;->build()Lcom/google/common/cache/Cache;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->sectionListCache:Lcom/google/common/cache/Cache;

    .line 84
    invoke-static {}, Lcom/google/common/cache/CacheBuilder;->newBuilder()Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/common/cache/CacheBuilder;->concurrencyLevel(I)Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    .line 85
    invoke-virtual {v0, v4, v5}, Lcom/google/common/cache/CacheBuilder;->maximumSize(J)Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/cache/CacheBuilder;->build()Lcom/google/common/cache/Cache;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->sectionEditionListCache:Lcom/google/common/cache/Cache;

    .line 89
    invoke-static {}, Lcom/google/common/cache/CacheBuilder;->newBuilder()Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/common/cache/CacheBuilder;->concurrencyLevel(I)Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    .line 90
    invoke-virtual {v0, v4, v5}, Lcom/google/common/cache/CacheBuilder;->maximumSize(J)Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/cache/CacheBuilder;->build()Lcom/google/common/cache/Cache;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->readStateListCache:Lcom/google/common/cache/Cache;

    .line 111
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static combinedSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    if-nez v0, :cond_0

    .line 149
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    .line 151
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    return-object v0
.end method

.method public static curatedTopicList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;)Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .prologue
    .line 215
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curatedTopicLists:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;

    .line 216
    .local v0, "curatedTopicList":Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;
    if-nez v0, :cond_0

    .line 217
    new-instance v0, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;

    .end local v0    # "curatedTopicList":Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;)V

    .line 218
    .restart local v0    # "curatedTopicList":Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 219
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curatedTopicLists:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    :cond_0
    return-object v0
.end method

.method public static curationSubscriptionsCardList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 167
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curationSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;->curationsCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public static curationSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;
    .locals 1

    .prologue
    .line 159
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curationSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;

    if-nez v0, :cond_0

    .line 160
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curationSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;

    .line 161
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curationSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 163
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curationSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;

    return-object v0
.end method

.method public static defaultSubscriptionsList()Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;
    .locals 1

    .prologue
    .line 269
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->defaultSubscriptionsList:Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;

    if-nez v0, :cond_0

    .line 270
    new-instance v0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->defaultSubscriptionsList:Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;

    .line 271
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->defaultSubscriptionsList:Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 273
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->defaultSubscriptionsList:Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;

    return-object v0
.end method

.method public static libraryPageList()Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->libraryPageList:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->libraryPageList:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;

    .line 136
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->libraryPageList:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;

    return-object v0
.end method

.method public static magazineSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;
    .locals 1

    .prologue
    .line 171
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->magazineSubscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->magazineSubscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;

    .line 173
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->magazineSubscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 175
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->magazineSubscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;

    return-object v0
.end method

.method public static newsSubscriptionsCardList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 155
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->newsSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->newsCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public static newsSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->newsSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->newsSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;

    .line 142
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->newsSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 144
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->newsSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;

    return-object v0
.end method

.method public static normalEditionList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;)Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    .prologue
    .line 189
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->normalEditionListCache:Lcom/google/common/cache/Cache;

    invoke-interface {v1, p1}, Lcom/google/common/cache/Cache;->getIfPresent(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;

    .line 190
    .local v0, "normalEditionList":Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;
    if-nez v0, :cond_0

    .line 191
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;

    .end local v0    # "normalEditionList":Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;)V

    .line 192
    .restart local v0    # "normalEditionList":Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 193
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->normalEditionListCache:Lcom/google/common/cache/Cache;

    invoke-interface {v1, p1, v0}, Lcom/google/common/cache/Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 195
    :cond_0
    return-object v0
.end method

.method public static offersList()Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;
    .locals 1

    .prologue
    .line 285
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->offersList:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->offersList:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

    .line 287
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->offersList:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 289
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->offersList:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

    return-object v0
.end method

.method public static pinnedList()Lcom/google/apps/dots/android/newsstand/sync/PinnedList;
    .locals 1

    .prologue
    .line 255
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->pinnedList:Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    if-nez v0, :cond_0

    .line 256
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->pinnedList()Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->pinnedList:Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    .line 258
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->pinnedList:Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    return-object v0
.end method

.method public static rawOffersList()Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;
    .locals 1

    .prologue
    .line 277
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->rawOffersList:Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;

    if-nez v0, :cond_0

    .line 278
    new-instance v0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->rawOffersList:Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;

    .line 279
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->rawOffersList:Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 281
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->rawOffersList:Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;

    return-object v0
.end method

.method public static readNowList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 199
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->readNowList:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    if-nez v0, :cond_0

    .line 200
    new-instance v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->readNowList:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    .line 201
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->readNowList:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 203
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->readNowList:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    return-object v0
.end method

.method public static recentlyReadList()Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;
    .locals 1

    .prologue
    .line 262
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->recentlyReadList:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    if-nez v0, :cond_0

    .line 263
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->recentlyReadHelper()Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->recentlyReadList()Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->recentlyReadList:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    .line 265
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->recentlyReadList:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    return-object v0
.end method

.method public static reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 114
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->libraryPageList:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;

    .line 115
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->newsSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;

    .line 116
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curationSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;

    .line 117
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    .line 118
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->normalEditionListCache:Lcom/google/common/cache/Cache;

    invoke-interface {v0}, Lcom/google/common/cache/Cache;->invalidateAll()V

    .line 119
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->readNowList:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    .line 120
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->savedList:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    .line 121
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->magazineSubscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;

    .line 122
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->recentMagazinesDataList:Lcom/google/apps/dots/android/newsstand/home/library/magazines/RecentMagazinesDataList;

    .line 123
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->sectionListCache:Lcom/google/common/cache/Cache;

    invoke-interface {v0}, Lcom/google/common/cache/Cache;->invalidateAll()V

    .line 124
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->sectionEditionListCache:Lcom/google/common/cache/Cache;

    invoke-interface {v0}, Lcom/google/common/cache/Cache;->invalidateAll()V

    .line 125
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->pinnedList:Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    .line 126
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->recentlyReadList:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    .line 127
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->defaultSubscriptionsList:Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;

    .line 128
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->rawOffersList:Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;

    .line 129
    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->offersList:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

    .line 130
    return-void
.end method

.method public static savedList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/saved/SavedList;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 207
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->savedList:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/saved/SavedList;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->savedList:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    .line 209
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->savedList:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 211
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->savedList:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    return-object v0
.end method

.method public static sectionEditionList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;)Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .prologue
    .line 235
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->sectionEditionListCache:Lcom/google/common/cache/Cache;

    invoke-interface {v1, p1}, Lcom/google/common/cache/Cache;->getIfPresent(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;

    .line 236
    .local v0, "list":Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;
    if-nez v0, :cond_0

    .line 237
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;

    .end local v0    # "list":Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;)V

    .line 238
    .restart local v0    # "list":Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 239
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->sectionEditionListCache:Lcom/google/common/cache/Cache;

    invoke-interface {v1, p1, v0}, Lcom/google/common/cache/Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 241
    :cond_0
    return-object v0
.end method

.method public static sectionList(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/datasource/SectionList;
    .locals 2
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 225
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->sectionListCache:Lcom/google/common/cache/Cache;

    invoke-interface {v1, p0}, Lcom/google/common/cache/Cache;->getIfPresent(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;

    .line 226
    .local v0, "list":Lcom/google/apps/dots/android/newsstand/datasource/SectionList;
    if-nez v0, :cond_0

    .line 227
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;

    .end local v0    # "list":Lcom/google/apps/dots/android/newsstand/datasource/SectionList;
    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 228
    .restart local v0    # "list":Lcom/google/apps/dots/android/newsstand/datasource/SectionList;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 229
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->sectionListCache:Lcom/google/common/cache/Cache;

    invoke-interface {v1, p0, v0}, Lcom/google/common/cache/Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 231
    :cond_0
    return-object v0
.end method
