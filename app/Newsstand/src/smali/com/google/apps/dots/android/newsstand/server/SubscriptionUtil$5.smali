.class final Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$5;
.super Ljava/lang/Object;
.source "SubscriptionUtil.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->showCancelPurchaseDialog(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field final synthetic val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$5;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$5;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 343
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$5;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$5;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v1, v1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setNewsDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v0

    .line 344
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->start()V

    .line 345
    return-void
.end method
