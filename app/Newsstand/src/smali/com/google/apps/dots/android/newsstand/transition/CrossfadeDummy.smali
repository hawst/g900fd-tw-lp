.class public Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;
.super Landroid/transition/Transition;
.source "CrossfadeDummy.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private fadeOutView:Landroid/view/View;

.field private fadeToColor:I

.field private inset:I

.field private final isEntering:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isEntering"    # Z

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->fadeToColor:I

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->inset:I

    .line 41
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->isEntering:Z

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->fadeOutView:Landroid/view/View;

    return-object v0
.end method

.method private getAlphaAnimator(Landroid/view/View;)Landroid/animation/Animator;
    .locals 10
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const-wide/16 v8, 0x64

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 165
    const-string v4, "alpha"

    const/4 v1, 0x2

    new-array v5, v1, [F

    const/4 v6, 0x0

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->isEntering:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    aput v1, v5, v6

    const/4 v1, 0x1

    iget-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->isEntering:Z

    if-eqz v6, :cond_3

    :goto_1
    aput v3, v5, v1

    .line 166
    invoke-static {p1, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 167
    .local v0, "fade":Landroid/animation/ObjectAnimator;
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->isEntering:Z

    if-eqz v1, :cond_0

    :cond_0
    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 169
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->isEntering:Z

    if-nez v1, :cond_1

    .line 170
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 172
    :cond_1
    return-object v0

    .end local v0    # "fade":Landroid/animation/ObjectAnimator;
    :cond_2
    move v1, v3

    .line 165
    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1
.end method

.method private getDrawableAnimator(Landroid/view/View;Landroid/view/View;)Landroid/animation/Animator;
    .locals 11
    .param p1, "startView"    # Landroid/view/View;
    .param p2, "endView"    # Landroid/view/View;

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 139
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->fadeToColor:I

    invoke-direct {v1, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 140
    .local v1, "fadeToDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    if-eq v5, v8, :cond_0

    .line 141
    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 143
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 145
    .local v0, "fadeFromDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;

    .line 146
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5, v0, v1}, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 147
    .local v4, "transitionBackgroundDrawable":Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;
    const v5, 0x3e99999a    # 0.3f

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->setTransitionRatio(F)V

    .line 149
    move-object v2, v4

    .line 150
    .local v2, "newBackground":Landroid/graphics/drawable/Drawable;
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->inset:I

    if-lez v5, :cond_1

    .line 151
    new-instance v2, Landroid/graphics/drawable/InsetDrawable;

    .end local v2    # "newBackground":Landroid/graphics/drawable/Drawable;
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->inset:I

    rsub-int/lit8 v5, v5, 0x0

    invoke-direct {v2, v4, v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 153
    .restart local v2    # "newBackground":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 155
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->isEntering:Z

    if-eqz v5, :cond_2

    move v5, v6

    :goto_0
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->setTransitionFraction(F)V

    .line 156
    const-string v8, "transitionFraction"

    const/4 v5, 0x2

    new-array v9, v5, [F

    const/4 v10, 0x0

    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->isEntering:Z

    if-eqz v5, :cond_3

    move v5, v6

    :goto_1
    aput v5, v9, v10

    const/4 v5, 0x1

    iget-boolean v10, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->isEntering:Z

    if-eqz v10, :cond_4

    :goto_2
    aput v7, v9, v5

    invoke-static {v4, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 158
    .local v3, "transition":Landroid/animation/ObjectAnimator;
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->isEntering:Z

    if-eqz v5, :cond_5

    const-wide/16 v6, 0x190

    :goto_3
    invoke-virtual {v3, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 161
    return-object v3

    .end local v3    # "transition":Landroid/animation/ObjectAnimator;
    :cond_2
    move v5, v7

    .line 155
    goto :goto_0

    :cond_3
    move v5, v7

    .line 156
    goto :goto_1

    :cond_4
    move v7, v6

    goto :goto_2

    .line 158
    .restart local v3    # "transition":Landroid/animation/ObjectAnimator;
    :cond_5
    const-wide/16 v6, 0x12c

    goto :goto_3
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 73
    return-void
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 67
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 1
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 78
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 79
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->getHeroAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getHeroAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 5
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 86
    iget-object v2, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 87
    .local v2, "endView":Landroid/view/View;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->fadeOutView:Landroid/view/View;

    if-ne v2, v4, :cond_0

    .line 88
    const/4 v1, 0x0

    .line 134
    :goto_0
    return-object v1

    .line 90
    :cond_0
    iget-object v3, p2, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 91
    .local v3, "startView":Landroid/view/View;
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->getAlphaAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    .line 92
    .local v0, "alphaAnimator":Landroid/animation/Animator;
    invoke-direct {p0, v3, v2}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->getDrawableAnimator(Landroid/view/View;Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v1

    .line 94
    .local v1, "drawableAnimator":Landroid/animation/Animator;
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->isEntering:Z

    if-eqz v4, :cond_1

    .line 95
    new-instance v4, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1;

    invoke-direct {v4, p0, v0}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1;-><init>(Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;Landroid/animation/Animator;)V

    invoke-virtual {v1, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 118
    :cond_1
    new-instance v4, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$2;

    invoke-direct {v4, p0, v1}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$2;-><init>(Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;Landroid/animation/Animator;)V

    invoke-virtual {v0, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object v1, v0

    .line 134
    goto :goto_0
.end method

.method public setFadeOutView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->fadeOutView:Landroid/view/View;

    .line 60
    return-object p0
.end method

.method public setFadeToColor(I)Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->fadeToColor:I

    .line 46
    return-object p0
.end method

.method public setInset(I)Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;
    .locals 0
    .param p1, "inset"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->inset:I

    .line 55
    return-object p0
.end method
