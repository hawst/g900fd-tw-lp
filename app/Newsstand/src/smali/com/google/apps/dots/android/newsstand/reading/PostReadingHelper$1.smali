.class final Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "PostReadingHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper;->addPostReadingData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;ILcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<",
        "Lcom/google/apps/dots/android/newsstand/share/ShareParams;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 0
    .param p1, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 81
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/share/ShareParams;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 85
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appSummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;->val$account:Landroid/accounts/Account;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;)V

    .line 84
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
