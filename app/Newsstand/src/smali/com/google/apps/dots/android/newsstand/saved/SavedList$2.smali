.class Lcom/google/apps/dots/android/newsstand/saved/SavedList$2;
.super Ljava/lang/Object;
.source "SavedList.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/DataProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/saved/SavedList;->cardListBuilder()Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/saved/SavedList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/saved/SavedList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList$2;->this$0:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList$2;->this$0:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    # getter for: Lcom/google/apps/dots/android/newsstand/saved/SavedList;->appContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->access$000(Lcom/google/apps/dots/android/newsstand/saved/SavedList;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList$2;->this$0:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList$2;->this$0:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList$2;->this$0:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    .line 78
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->getRetryRunnable()Ljava/lang/Runnable;

    move-result-object v3

    .line 77
    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeSpecificErrorCardData(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method
