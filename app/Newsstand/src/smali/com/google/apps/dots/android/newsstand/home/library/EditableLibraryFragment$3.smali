.class Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$3;
.super Ljava/lang/Object;
.source "EditableLibraryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->doOnCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->updateErrorView()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->access$100(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V

    .line 126
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->updateHeaderCards()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->access$000(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V

    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getSourceCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getSourceCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData(Z)V

    goto :goto_0
.end method
