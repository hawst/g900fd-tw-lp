.class final Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "SubscriptionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->translateNewsSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field final synthetic val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

.field final synthetic val$provideToast:Z

.field final synthetic val$translationCode:Ljava/lang/String;


# direct methods
.method constructor <init>(ZLcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 259
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$provideToast:Z

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$translationCode:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1, "optionalResult"    # Ljava/lang/Object;

    .prologue
    .line 262
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$provideToast:Z

    if-eqz v2, :cond_0

    .line 263
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->translating:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/toast/UndoTranslateOperation;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$account:Landroid/accounts/Account;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$translationCode:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/toast/UndoTranslateOperation;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V

    .line 266
    .local v1, "undoOperation":Lcom/google/apps/dots/android/newsstand/toast/UndoTranslateOperation;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    const/4 v3, 0x1

    invoke-static {v2, v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->showToast(Landroid/app/Activity;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;Z)V

    .line 268
    .end local v0    # "message":Ljava/lang/String;
    .end local v1    # "undoOperation":Lcom/google/apps/dots/android/newsstand/toast/UndoTranslateOperation;
    :cond_0
    return-void
.end method
