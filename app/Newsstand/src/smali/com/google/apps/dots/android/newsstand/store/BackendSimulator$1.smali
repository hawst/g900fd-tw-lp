.class Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$1;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;
.source "BackendSimulator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->handleNewsSubscription(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor",
        "<",
        "Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

.field final synthetic val$appId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$1;->val$appId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 0

    .prologue
    .line 194
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$1;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V

    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 2
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;
    .param p2, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 197
    iget-object v0, p2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$1;->val$appId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$1;->val$appId:Ljava/lang/String;

    .line 199
    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;->delete()V

    .line 202
    :cond_1
    return-void
.end method
