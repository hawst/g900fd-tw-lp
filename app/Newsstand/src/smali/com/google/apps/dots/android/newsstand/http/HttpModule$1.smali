.class final Lcom/google/apps/dots/android/newsstand/http/HttpModule$1;
.super Ljava/lang/Object;
.source "HttpModule.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/http/HttpModule;->provideWebviewHttpClient(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$1;->val$account:Landroid/accounts/Account;

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;-><init>(Landroid/content/Context;Landroid/accounts/Account;)V

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/http/HttpModule$1;->call()Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;

    move-result-object v0

    return-object v0
.end method
