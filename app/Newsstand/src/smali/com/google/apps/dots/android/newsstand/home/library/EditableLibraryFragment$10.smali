.class Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$10;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "EditableLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->onNewsOperationDisallowed(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 406
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$10;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 1
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$10;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->showCancelPurchaseDialog(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    .line 410
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 406
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$10;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
