.class public final enum Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;
.super Ljava/lang/Enum;
.source "ProtoEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/util/ProtoEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum ALT_FORMAT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum AUDIO:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum DATETIME:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum IMAGE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum INLINE_FRAME:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum LOCATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum NATIVE_BODY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum NUMBER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum PDF:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum PRODUCT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum STREAMING_VIDEO:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum TEXT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum URL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

.field public static final enum VIDEO:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;


# instance fields
.field private itemType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 21
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "AUDIO"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->AUDIO:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 22
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "DATETIME"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->DATETIME:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "HTML"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->IMAGE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 25
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "LOCATION"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->LOCATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 26
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "NUMBER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->NUMBER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 27
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "TEXT"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->TEXT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 28
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "URL"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->URL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 29
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "VIDEO"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->VIDEO:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 30
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "PRODUCT"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->PRODUCT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 31
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "ALT_FORMAT"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->ALT_FORMAT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 32
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "STREAMING_VIDEO"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->STREAMING_VIDEO:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 33
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "INLINE_FRAME"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->INLINE_FRAME:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 34
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "NATIVE_BODY"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->NATIVE_BODY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 35
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const-string v1, "PDF"

    const/16 v2, 0xe

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->PDF:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    .line 20
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->AUDIO:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->DATETIME:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->IMAGE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->LOCATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->NUMBER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->TEXT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->URL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->VIDEO:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->PRODUCT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->ALT_FORMAT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->STREAMING_VIDEO:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->INLINE_FRAME:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->NATIVE_BODY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->PDF:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "itemType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->itemType:I

    .line 51
    return-void
.end method

.method public static fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;
    .locals 5
    .param p0, "itemType"    # I

    .prologue
    .line 41
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 42
    .local v0, "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;
    iget v4, v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->itemType:I

    if-ne v4, p0, :cond_0

    .line 46
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;
    :goto_1
    return-object v0

    .line 41
    .restart local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    return-object v0
.end method
