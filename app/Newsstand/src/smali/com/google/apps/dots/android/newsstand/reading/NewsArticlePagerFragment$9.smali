.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$9;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "NewsArticlePagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->announceCurrentPostForAccessibility()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

.field final synthetic val$articleData:Lcom/google/android/libraries/bind/data/Data;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 536
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$9;->val$articleData:Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$400(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$9;->val$articleData:Lcom/google/android/libraries/bind/data/Data;

    sget v2, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 540
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 539
    invoke-virtual {v1, v0}, Landroid/support/v4/view/NSViewPager;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 541
    return-void
.end method
