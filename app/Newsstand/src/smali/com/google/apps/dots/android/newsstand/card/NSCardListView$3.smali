.class Lcom/google/apps/dots/android/newsstand/card/NSCardListView$3;
.super Ljava/lang/Object;
.source "NSCardListView.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnChildrenAddedListener(ILjava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field final synthetic val$onChildrenAdded:Ljava/lang/Runnable;

.field final synthetic val$threshold:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;ILjava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$3;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput p2, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$3;->val$threshold:I

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$3;->val$onChildrenAdded:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$3;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getChildCount()I

    move-result v0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$3;->val$threshold:I

    if-le v0, v1, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$3;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$3;->val$onChildrenAdded:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 134
    :cond_0
    return-void
.end method
