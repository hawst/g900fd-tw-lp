.class public final Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;


# instance fields
.field private appId_:Ljava/lang/String;

.field private bitField0_:I

.field private entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5303
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5304
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5437
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->type_:I

    .line 5456
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    .line 5304
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;
    .locals 3

    .prologue
    .line 5509
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5513
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    if-eqz v2, :cond_0

    .line 5514
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    .line 5516
    :cond_0
    return-object v0

    .line 5510
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;
    :catch_0
    move-exception v1

    .line 5511
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 5300
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5521
    if-ne p1, p0, :cond_1

    .line 5526
    :cond_0
    :goto_0
    return v1

    .line 5522
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 5523
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    .line 5524
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 5525
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 5526
    goto :goto_0

    .line 5524
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    .line 5525
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    .line 5526
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5458
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 5553
    const/4 v0, 0x0

    .line 5554
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 5555
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->type_:I

    .line 5556
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5558
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 5559
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    .line 5560
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5562
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    if-eqz v1, :cond_2

    .line 5563
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    .line 5564
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5566
    :cond_2
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->cachedSize:I

    .line 5567
    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 5439
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->type_:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 5530
    const/16 v0, 0x11

    .line 5531
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 5532
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->type_:I

    add-int v0, v1, v3

    .line 5533
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 5534
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 5535
    return v0

    .line 5533
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 5534
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 5575
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 5576
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 5580
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5581
    :sswitch_0
    return-object p0

    .line 5586
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5587
    .local v1, "temp":I
    if-eq v1, v3, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_2

    .line 5594
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->type_:I

    .line 5595
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->bitField0_:I

    goto :goto_0

    .line 5597
    :cond_2
    iput v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->type_:I

    goto :goto_0

    .line 5602
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    .line 5603
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->bitField0_:I

    goto :goto_0

    .line 5607
    :sswitch_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    if-nez v2, :cond_3

    .line 5608
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    .line 5610
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5576
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5300
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v0

    return-object v0
.end method

.method public setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 5461
    if-nez p1, :cond_0

    .line 5462
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5464
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    .line 5465
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->bitField0_:I

    .line 5466
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5540
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5541
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5543
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 5544
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->appId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5546
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    if-eqz v0, :cond_2

    .line 5547
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->entityData_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType$EntityData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5549
    :cond_2
    return-void
.end method
