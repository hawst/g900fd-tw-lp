.class Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;
.super Ljava/lang/Object;
.source "PdfPartView.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 54
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->access$300()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Couldn\'t retrieve attachment."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->setImageInfo(Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;)V

    .line 56
    return-void
.end method

.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V
    .locals 6
    .param p1, "storeResponse"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->access$002(Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->width:I
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->access$100(Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;)I

    move-result v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->height:I
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;)I

    move-result v3

    const/4 v4, 0x1

    const-string v5, "application/pdf"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;-><init>(IIZLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->setImageInfo(Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;)V

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 41
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V

    return-void
.end method
