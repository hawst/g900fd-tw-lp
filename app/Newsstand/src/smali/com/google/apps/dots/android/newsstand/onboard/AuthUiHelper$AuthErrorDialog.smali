.class public Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;
.super Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;
.source "AuthUiHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AuthErrorDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 585
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;
    .param p1, "x1"    # I

    .prologue
    .line 585
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;->setResult(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;
    .param p1, "x1"    # I

    .prologue
    .line 585
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;->setResult(I)V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 589
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 591
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->try_again:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog$1;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 601
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->cancel:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog$2;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthErrorDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 608
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->authentication_failure:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 609
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->authentication_failure_error:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 610
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
