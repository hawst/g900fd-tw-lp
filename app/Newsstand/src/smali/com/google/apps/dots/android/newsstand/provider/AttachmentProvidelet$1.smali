.class Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$1;
.super Ljava/lang/Object;
.source "AttachmentProvidelet.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureFallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->openAssetFile(ILandroid/net/Uri;Ljava/lang/String;Landroid/content/ContentProvider;)Landroid/content/res/AssetFileDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureFallback",
        "<",
        "Landroid/content/res/AssetFileDescriptor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$1;->this$0:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/content/res/AssetFileDescriptor;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$1;->this$0:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    # invokes: Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->getNotFoundImageAssetDescriptor()Landroid/content/res/AssetFileDescriptor;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->access$000(Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
