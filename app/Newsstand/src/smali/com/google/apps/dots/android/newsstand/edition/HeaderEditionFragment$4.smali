.class Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$4;
.super Ljava/lang/Object;
.source "HeaderEditionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getRetryRunnable()Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 314
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->refreshIfFailed(Z)V

    .line 318
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateEditionSummary()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V

    .line 319
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateHeroView()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$200(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V

    .line 320
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateActionBarTitle()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$300(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V

    .line 321
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateShareParams()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$400(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V

    .line 322
    return-void
.end method
