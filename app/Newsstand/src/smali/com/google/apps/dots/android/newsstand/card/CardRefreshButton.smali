.class public Lcom/google/apps/dots/android/newsstand/card/CardRefreshButton;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "CardRefreshButton.java"


# static fields
.field public static final DK_ON_CLICK_LISTENER:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_refresh_button:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardRefreshButton;->LAYOUT:I

    .line 15
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardRefreshButton_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardRefreshButton;->DK_ON_CLICK_LISTENER:I

    .line 17
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardRefreshButton;->EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/card/CardRefreshButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardRefreshButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method
