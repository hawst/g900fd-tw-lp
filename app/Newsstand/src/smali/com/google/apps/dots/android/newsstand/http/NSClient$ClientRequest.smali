.class public Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
.super Ljava/lang/Object;
.source "NSClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/http/NSClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClientRequest"
.end annotation


# instance fields
.field public final eTag:Ljava/lang/String;

.field public final lastModified:Ljava/lang/Long;

.field public final locale:Ljava/util/Locale;

.field public final optPostData:[B

.field public final priority:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

.field public final uri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 7
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "optPostData"    # [B

    .prologue
    const/4 v3, 0x0

    .line 67
    sget-object v5, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;-><init>(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;Ljava/util/Locale;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;Ljava/util/Locale;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "optPostData"    # [B
    .param p3, "eTag"    # Ljava/lang/String;
    .param p4, "lastModified"    # Ljava/lang/Long;
    .param p5, "priority"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;
    .param p6, "locale"    # Ljava/util/Locale;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->uri:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->optPostData:[B

    .line 74
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->eTag:Ljava/lang/String;

    .line 75
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->lastModified:Ljava/lang/Long;

    .line 76
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->priority:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    .line 77
    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->locale:Ljava/util/Locale;

    .line 78
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 82
    const-class v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "uri"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->uri:Ljava/lang/String;

    .line 83
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "hasOptPostData"

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->optPostData:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 84
    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "eTag"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->eTag:Ljava/lang/String;

    .line 85
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "lastModified"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->lastModified:Ljava/lang/Long;

    .line 86
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->omitNullValues()Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 83
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
