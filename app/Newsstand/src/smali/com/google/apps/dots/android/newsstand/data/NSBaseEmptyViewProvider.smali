.class public abstract Lcom/google/apps/dots/android/newsstand/data/NSBaseEmptyViewProvider;
.super Ljava/lang/Object;
.source "NSBaseEmptyViewProvider.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/ViewProvider;


# instance fields
.field protected headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/NSBaseEmptyViewProvider;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 24
    return-void
.end method


# virtual methods
.method public abstract getEmptyMessageData()Lcom/google/android/libraries/bind/data/Data;
.end method

.method public getView(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    .line 33
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 35
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/NSBaseEmptyViewProvider;->getEmptyMessageData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 36
    .local v1, "emptyMessageData":Lcom/google/android/libraries/bind/data/Data;
    if-nez v1, :cond_0

    .line 37
    new-instance v2, Landroid/view/View;

    invoke-direct {v2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 38
    .local v2, "emptyView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->home_background:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 45
    :goto_0
    return-object v2

    .line 40
    .end local v2    # "emptyView":Landroid/view/View;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/data/NSBaseEmptyViewProvider;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-static {v1, v3, v4}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->addHeaderPadding(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/android/libraries/bind/data/Data;

    .line 41
    sget v3, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->LAYOUT:I

    const/4 v4, 0x0

    .line 42
    invoke-static {p1}, Lcom/google/android/libraries/bind/data/DataAdapter;->getFullScreenLayoutParams(Landroid/view/ViewGroup;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    invoke-virtual {p2, v3, v4, v5}, Lcom/google/android/libraries/bind/view/ViewHeap;->get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v2

    .restart local v2    # "emptyView":Landroid/view/View;
    move-object v3, v2

    .line 43
    check-cast v3, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0
.end method

.method public setHeaderType(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/data/NSBaseEmptyViewProvider;
    .locals 0
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/NSBaseEmptyViewProvider;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 28
    return-object p0
.end method
