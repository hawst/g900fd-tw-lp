.class public Lcom/google/apps/dots/android/newsstand/search/SearchActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "SearchActivity.java"


# instance fields
.field private searchFragment:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

.field private searchView:Landroid/support/v7/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/search/SearchActivity;)Lcom/google/apps/dots/android/newsstand/search/SearchFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/search/SearchActivity;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->searchFragment:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    return-object v0
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 69
    if-nez p1, :cond_0

    .line 81
    :goto_0
    return-void

    .line 73
    :cond_0
    const-string v2, "SearchFragment_state"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 74
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 75
    .local v0, "intentExtras":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 76
    const-string v2, "query"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "query":Ljava/lang/String;
    const-string v2, "SearchFragment_state"

    new-instance v3, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    invoke-direct {v3, v1}, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 80
    .end local v0    # "intentExtras":Landroid/os/Bundle;
    .end local v1    # "query":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->searchFragment:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->handleExtras(Landroid/os/Bundle;)Z

    goto :goto_0
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 55
    .local v0, "retVal":Z
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    .line 56
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 59
    :cond_0
    return v0
.end method

.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 166
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->searchFragment:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->searchFragment:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    return-object v0
.end method

.method public getSearchView()Landroid/support/v7/widget/SearchView;
    .locals 7

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->getContextForSearchView()Landroid/content/Context;

    move-result-object v0

    .line 136
    .local v0, "contextForSearchView":Landroid/content/Context;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    if-nez v5, :cond_2

    if-eqz v0, :cond_2

    .line 137
    new-instance v3, Landroid/support/v7/widget/SearchView;

    invoke-direct {v3, v0}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;)V

    .line 139
    .local v3, "searchView":Landroid/support/v7/widget/SearchView;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/support/v7/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 142
    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->searchview_fragment_title:I

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/support/v7/widget/SearchView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 145
    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->search_src_text:I

    .line 146
    invoke-virtual {v3, v5}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/AutoCompleteTextView;

    .line 147
    .local v4, "textView":Landroid/widget/AutoCompleteTextView;
    if-eqz v4, :cond_0

    .line 148
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$color;->action_bar_hint_text:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->setHintTextColor(I)V

    .line 152
    :cond_0
    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->search_plate:I

    invoke-virtual {v3, v5}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 153
    .local v2, "searchPlate":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 154
    sget v5, Lcom/google/android/apps/newsstanddev/R$drawable;->search_plate_background:I

    invoke-virtual {v2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 157
    :cond_1
    const-string v5, "search"

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/SearchManager;

    .line 158
    .local v1, "searchManager":Landroid/app/SearchManager;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/support/v7/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 159
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    .line 161
    .end local v1    # "searchManager":Landroid/app/SearchManager;
    .end local v2    # "searchPlate":Landroid/view/View;
    .end local v3    # "searchView":Landroid/support/v7/widget/SearchView;
    .end local v4    # "textView":Landroid/widget/AutoCompleteTextView;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    return-object v5
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->searchFragment:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->searchFragment:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 190
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onBackPressed()V

    .line 192
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->search_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->setContentView(I)V

    .line 36
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->search_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->searchFragment:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    .line 41
    if-nez p1, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 46
    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->setActionBarTitle(Ljava/lang/CharSequence;)V

    .line 47
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 86
    .local v0, "menuInflater":Landroid/view/MenuInflater;
    sget v3, Lcom/google/android/apps/newsstanddev/R$menu;->search_activity_menu:I

    invoke-virtual {v0, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 89
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_search:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 90
    .local v1, "searchItem":Landroid/view/MenuItem;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->getSearchView()Landroid/support/v7/widget/SearchView;

    move-result-object v2

    .line 92
    .local v2, "searchView":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 93
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 95
    :cond_0
    invoke-static {v1, v2}, Landroid/support/v4/view/MenuItemCompat;->setActionView(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    .line 96
    invoke-static {v1}, Landroid/support/v4/view/MenuItemCompat;->expandActionView(Landroid/view/MenuItem;)Z

    .line 97
    new-instance v3, Lcom/google/apps/dots/android/newsstand/search/SearchActivity$1;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity$1;-><init>(Lcom/google/apps/dots/android/newsstand/search/SearchActivity;)V

    invoke-static {v1, v3}, Landroid/support/v4/view/MenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 126
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    return v3
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 65
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 66
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 172
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->onOptionsItemSelectedCloseDrawerIfNeeded(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    :goto_0
    return v0

    .line 174
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 175
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->onBackPressed()V

    goto :goto_0

    .line 178
    :cond_1
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
