.class final Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat$1;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "TraceCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 54
    sget v1, Lcom/google/android/apps/newsstanddev/R$bool;->enable_systrace:I

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getBooleanResource(I)Z

    move-result v1

    # setter for: Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->enabled:Z
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->access$002(Z)Z

    .line 56
    # getter for: Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->enabled:Z
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->access$000()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 59
    :try_start_0
    const-class v1, Landroid/os/Trace;

    const-string v2, "asyncTraceBegin"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    .line 60
    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 59
    # setter for: Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->asyncTraceBegin:Ljava/lang/reflect/Method;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->access$102(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;

    .line 61
    const-class v1, Landroid/os/Trace;

    const-string v2, "asyncTraceEnd"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    .line 62
    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 61
    # setter for: Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->asyncTraceEnd:Ljava/lang/reflect/Method;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->access$202(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;

    .line 63
    const-class v1, Landroid/os/Trace;

    const-string v2, "TRACE_TAG_APP"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getLong(Ljava/lang/Object;)J

    move-result-wide v2

    # setter for: Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->traceTagApp:J
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->access$302(J)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
