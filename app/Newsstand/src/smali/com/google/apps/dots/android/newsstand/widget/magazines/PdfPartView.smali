.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;
.super Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
.source "PdfPartView.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;


# static fields
.field private static final DUMP_PDF_IMAGES:Z

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final attachmentId:Ljava/lang/String;

.field private attachmentLoaded:Z

.field private final backgroundColor:I

.field private blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

.field private final callback:Lcom/google/common/util/concurrent/FutureCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/FutureCallback",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation
.end field

.field private height:I

.field private final page:I

.field private final pdfLib:Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Ljava/lang/String;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;
    .param p3, "attachmentId"    # Ljava/lang/String;
    .param p4, "page"    # I
    .param p5, "backgroundColor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V

    .line 41
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->callback:Lcom/google/common/util/concurrent/FutureCallback;

    .line 68
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->pdfLib:Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;

    .line 69
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->attachmentId:Ljava/lang/String;

    .line 70
    iput p4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->page:I

    .line 71
    iput p5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->backgroundColor:I

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    .prologue
    .line 32
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->width:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    .prologue
    .line 32
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->height:I

    return v0
.end method

.method static synthetic access$300()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method private getCacheKey(I)Ljava/lang/String;
    .locals 4
    .param p1, "sampleSize"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->attachmentId:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->page:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private hasBitmap()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadAttachment()V
    .locals 4

    .prologue
    .line 75
    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->attachmentId:Ljava/lang/String;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    sget-object v2, Lcom/google/apps/dots/android/newsstand/server/Transform;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 76
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform(Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    .line 77
    .local v0, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v1, v2, v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->callback:Lcom/google/common/util/concurrent/FutureCallback;

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 78
    return-void
.end method


# virtual methods
.method protected getBitmap(IZ)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "sampleSize"    # I
    .param p2, "purgeable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$WouldCauseJankException;
        }
    .end annotation

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->hasBitmap()Z

    move-result v7

    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 98
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->getCacheKey(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getCachedBitmap(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 99
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    move-object v7, v0

    .line 159
    :goto_0
    return-object v7

    .line 102
    :cond_0
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v7

    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->areaOf(Landroid/graphics/RectF;)F

    move-result v4

    .line 103
    .local v4, "screenArea":F
    const/4 v7, 0x1

    if-ne p1, v7, :cond_1

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->width:I

    iget v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->height:I

    mul-int/2addr v7, v8

    int-to-float v7, v7

    const/high16 v8, 0x3e800000    # 0.25f

    mul-float/2addr v8, v4

    cmpl-float v7, v7, v8

    if-lez v7, :cond_1

    sget-object v7, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    .line 105
    invoke-virtual {v7}, Lcom/google/android/libraries/bind/async/JankLock;->isPaused()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 107
    new-instance v7, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$WouldCauseJankException;

    invoke-direct {v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$WouldCauseJankException;-><init>()V

    throw v7

    .line 110
    :cond_1
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    iget v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->width:I

    div-int/2addr v8, p1

    iget v9, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->height:I

    div-int/2addr v9, p1

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v11, 0x1

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getPoolBitmap(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 112
    if-nez v0, :cond_2

    .line 113
    const/4 v7, 0x0

    goto :goto_0

    .line 115
    :cond_2
    const/4 v1, 0x0

    .line 117
    .local v1, "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    :try_start_0
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-interface {v7}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeDiskBlob()Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    move-result-object v1

    .line 118
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->pdfLib:Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;

    iget-object v8, v1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->raf:Ljava/io/RandomAccessFile;

    iget-wide v10, v1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->offset:J

    long-to-int v9, v10

    iget-wide v10, v1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->size:J

    long-to-int v10, v10

    invoke-virtual {v7, v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;->openDocument(Ljava/io/RandomAccessFile;II)V

    .line 119
    new-instance v5, Landroid/graphics/Rect;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->pdfLib:Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;

    iget v10, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->page:I

    invoke-virtual {v9, v10}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;->getPageWidth(I)I

    move-result v9

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->pdfLib:Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;

    iget v11, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->page:I

    invoke-virtual {v10, v11}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;->getPageHeight(I)I

    move-result v10

    invoke-direct {v5, v7, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 120
    .local v5, "srcRect":Landroid/graphics/Rect;
    new-instance v2, Landroid/graphics/Rect;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-direct {v2, v7, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 122
    .local v2, "dstRect":Landroid/graphics/Rect;
    iget v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->backgroundColor:I

    invoke-virtual {v0, v7}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 125
    iget v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->backgroundColor:I

    invoke-static {v7}, Landroid/graphics/Color;->alpha(I)I

    move-result v7

    const/16 v8, 0xff

    if-ge v7, v8, :cond_3

    const/4 v7, 0x1

    :goto_1
    invoke-virtual {v0, v7}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 128
    :try_start_1
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->pdfLib:Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;

    iget v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->page:I

    invoke-virtual {v7, v8, v5, v2, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;->draw(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 152
    :goto_2
    :try_start_2
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->pdfLib:Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;->closeDocument()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    .end local v2    # "dstRect":Landroid/graphics/Rect;
    .end local v5    # "srcRect":Landroid/graphics/Rect;
    :goto_3
    move-object v7, v0

    .line 159
    goto/16 :goto_0

    .line 125
    .restart local v2    # "dstRect":Landroid/graphics/Rect;
    .restart local v5    # "srcRect":Landroid/graphics/Rect;
    :cond_3
    const/4 v7, 0x0

    goto :goto_1

    .line 129
    :catch_0
    move-exception v3

    .line 130
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    :try_start_3
    sget-object v7, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "PDFLib out of memory: purging cache and trying again."

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v7, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    const/high16 v7, 0x3f000000    # 0.5f

    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/NSDepend;->trimCaches(F)V

    .line 132
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 133
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->pdfLib:Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;

    iget v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->page:I

    invoke-virtual {v7, v8, v5, v2, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;->draw(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Bitmap;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    .line 146
    .end local v2    # "dstRect":Landroid/graphics/Rect;
    .end local v3    # "e":Ljava/lang/OutOfMemoryError;
    .end local v5    # "srcRect":Landroid/graphics/Rect;
    :catch_1
    move-exception v6

    .line 147
    .local v6, "t":Ljava/lang/Throwable;
    :try_start_4
    sget-object v7, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x1d

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Failed to open PDF document: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v7, v6, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-virtual {v7, v0}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->releaseBitmap(Landroid/graphics/Bitmap;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 149
    const/4 v0, 0x0

    .line 152
    :try_start_5
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->pdfLib:Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;->closeDocument()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 156
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_3

    .line 153
    .end local v6    # "t":Ljava/lang/Throwable;
    .restart local v2    # "dstRect":Landroid/graphics/Rect;
    .restart local v5    # "srcRect":Landroid/graphics/Rect;
    :catch_2
    move-exception v7

    .line 156
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_3

    :catchall_0
    move-exception v7

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    throw v7

    .line 153
    .end local v2    # "dstRect":Landroid/graphics/Rect;
    .end local v5    # "srcRect":Landroid/graphics/Rect;
    .restart local v6    # "t":Ljava/lang/Throwable;
    :catch_3
    move-exception v7

    .line 156
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_3

    :catchall_1
    move-exception v7

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    throw v7

    .line 151
    .end local v6    # "t":Ljava/lang/Throwable;
    :catchall_2
    move-exception v7

    .line 152
    :try_start_6
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->pdfLib:Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PDFLibWrapper;->closeDocument()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 156
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    :goto_4
    throw v7

    .line 153
    :catch_4
    move-exception v8

    .line 156
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_4

    :catchall_3
    move-exception v7

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    throw v7
.end method

.method public onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 86
    invoke-super/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->onLayout(ZIIII)V

    .line 87
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->width:I

    .line 88
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->height:I

    .line 89
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->attachmentLoaded:Z

    if-nez v0, :cond_0

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->attachmentLoaded:Z

    .line 91
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->loadAttachment()V

    .line 93
    :cond_0
    return-void
.end method

.method protected releaseBitmap(Landroid/graphics/Bitmap;I)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "sampleSize"    # I

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;->getCacheKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->releaseBitmap(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    .line 165
    return-void
.end method
