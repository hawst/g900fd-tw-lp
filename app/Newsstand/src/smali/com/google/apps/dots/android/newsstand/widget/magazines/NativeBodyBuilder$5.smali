.class Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$5;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "NativeBodyBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->jumpToExternalId(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    .prologue
    .line 264
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$5;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$5;->val$pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 264
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$5;->onSuccess(Ljava/lang/String;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/String;)V
    .locals 3
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 267
    if-nez p1, :cond_0

    .line 274
    :goto_0
    return-void

    .line 270
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$5;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->magazineEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 271
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$5;->val$pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .line 272
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v0

    const/4 v1, 0x1

    .line 273
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->start(Z)V

    goto :goto_0
.end method
