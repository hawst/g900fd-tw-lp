.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$11;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "MeterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->refreshMyNews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 657
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$11;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 660
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->newsSubscriptionsCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData()V

    .line 661
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$11;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$1400(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$11;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->readingList(Landroid/content/Context;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData()V

    .line 662
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 657
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$11;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    return-void
.end method
