.class public Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;
.super Ljava/lang/Object;
.source "OffersFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final startingOfferId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;-><init>(Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "startingOfferId"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;->startingOfferId:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 26
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 27
    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    .line 28
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;->startingOfferId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;->startingOfferId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 30
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;->startingOfferId:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 40
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "{%s}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;->startingOfferId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;->startingOfferId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    return-void
.end method
