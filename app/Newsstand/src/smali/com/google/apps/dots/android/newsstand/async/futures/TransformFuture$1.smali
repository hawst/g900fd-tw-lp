.class Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;
.super Ljava/lang/Object;
.source "TransformFuture.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

.field final synthetic val$outputFuture:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

    .prologue
    .line 97
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;, "Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;->val$outputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 100
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;, "Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

    const/4 v2, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->outputFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->access$002(Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 102
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;->val$outputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v2}, Lcom/google/common/util/concurrent/Uninterruptibles;->getUninterruptibly(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->set(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 110
    :goto_0
    return-void

    .line 103
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/util/concurrent/CancellationException;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->cancel(Z)Z

    goto :goto_0

    .line 107
    .end local v0    # "e":Ljava/util/concurrent/CancellationException;
    :catch_1
    move-exception v0

    .line 108
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
