.class final Lcom/google/apps/dots/android/newsstand/async/futures/Async$5;
.super Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnFailure;
.source "Async.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/Async;->asTransform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnFailure",
        "<TI;>;"
    }
.end annotation


# instance fields
.field final synthetic val$fallback:Lcom/google/common/util/concurrent/FutureFallback;


# direct methods
.method constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)V
    .locals 0

    .prologue
    .line 152
    .local p1, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TI;>;"
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$5;->val$fallback:Lcom/google/common/util/concurrent/FutureFallback;

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnFailure;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;)V

    return-void
.end method


# virtual methods
.method public fallback(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TI;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$5;->val$fallback:Lcom/google/common/util/concurrent/FutureFallback;

    invoke-interface {v0, p1}, Lcom/google/common/util/concurrent/FutureFallback;->create(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
