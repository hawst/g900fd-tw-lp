.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TimerDetails"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;


# instance fields
.field private bitField0_:I

.field private delayMillis_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3193
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3194
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3199
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->delayMillis_:I

    .line 3194
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;
    .locals 3

    .prologue
    .line 3228
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3232
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;
    return-object v0

    .line 3229
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;
    :catch_0
    move-exception v1

    .line 3230
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3190
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3237
    if-ne p1, p0, :cond_1

    .line 3240
    :cond_0
    :goto_0
    return v1

    .line 3238
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 3239
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    .line 3240
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->delayMillis_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->delayMillis_:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getDelayMillis()I
    .locals 1

    .prologue
    .line 3201
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->delayMillis_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3259
    const/4 v0, 0x0

    .line 3260
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3261
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->delayMillis_:I

    .line 3262
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3264
    :cond_0
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->cachedSize:I

    .line 3265
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 3244
    const/16 v0, 0x11

    .line 3245
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 3246
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->delayMillis_:I

    add-int v0, v1, v2

    .line 3247
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3273
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3274
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3278
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3279
    :sswitch_0
    return-object p0

    .line 3284
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->delayMillis_:I

    .line 3285
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->bitField0_:I

    goto :goto_0

    .line 3274
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3190
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3252
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3253
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;->delayMillis_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3255
    :cond_0
    return-void
.end method
