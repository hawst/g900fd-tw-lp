.class public Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "ExploreFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private connectivityListener:Ljava/lang/Runnable;

.field private headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 62
    const/4 v0, 0x0

    const-string v1, "ExploreFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->explore_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->makeStoreCard()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->makeFeaturedCard()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method private getTopicsListWithSpecialItems()Lcom/google/android/libraries/bind/data/DataList;
    .locals 3

    .prologue
    .line 152
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList;-><init>()V

    .line 153
    .local v0, "topicsList":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList;->freshen()V

    .line 154
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 155
    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$4;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopicsList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    return-object v1
.end method

.method private makeFeaturedCard()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->makeSpecialCard()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 187
    .local v0, "card":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    const-string v2, "special_featured"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 188
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_BACKGROUND:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->explore_topics_background_featured:I

    .line 189
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 188
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 190
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_ICON_DRAWABLE:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_feature:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 191
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_TITLE:I

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->featured:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 192
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_ON_CLICK_LISTENER:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$6;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 198
    return-object v0
.end method

.method private makeSpecialCard()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    .line 202
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 203
    .local v0, "card":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->LAYOUTS:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 204
    return-object v0
.end method

.method private makeStoreCard()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->makeSpecialCard()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 171
    .local v0, "card":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    const-string v2, "special_store"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 172
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_BACKGROUND:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->explore_topics_background_store:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 173
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_ICON_DRAWABLE:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_store:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 174
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_TITLE:I

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->store:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 175
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_ON_CLICK_LISTENER:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$5;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 182
    return-object v0
.end method

.method private refreshIfNeeded()V
    .locals 3

    .prologue
    .line 100
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 101
    .local v0, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getExploreTopics(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->refreshIfNeeded(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method private setupHeaderListLayout()V
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$2;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 94
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 95
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 94
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 96
    return-void
.end method


# virtual methods
.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 227
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 228
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_explore"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    return-object v0
.end method

.method protected getNumColumns()I
    .locals 5

    .prologue
    const/4 v2, 0x3

    .line 211
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-ne v3, v4, :cond_1

    const/4 v1, 0x1

    .line 212
    .local v1, "isPortrait":Z
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    .line 214
    .local v0, "deviceCategory":Lcom/google/apps/dots/shared/DeviceCategory;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$7;->$SwitchMap$com$google$apps$dots$shared$DeviceCategory:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/shared/DeviceCategory;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 221
    if-eqz v1, :cond_0

    const/4 v2, 0x2

    :cond_0
    :goto_1
    return v2

    .line 211
    .end local v0    # "deviceCategory":Lcom/google/apps/dots/shared/DeviceCategory;
    .end local v1    # "isPortrait":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 216
    .restart local v0    # "deviceCategory":Lcom/google/apps/dots/shared/DeviceCategory;
    .restart local v1    # "isPortrait":Z
    :pswitch_0
    if-nez v1, :cond_0

    const/4 v2, 0x5

    goto :goto_1

    .line 218
    :pswitch_1
    if-nez v1, :cond_0

    const/4 v2, 0x4

    goto :goto_1

    .line 214
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 250
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onDestroyView()V

    .line 251
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->connectivityListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 252
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 254
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 234
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onResume()V

    .line 235
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->getMsSincePause()J

    move-result-wide v0

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 236
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->refreshIfNeeded()V

    .line 238
    :cond_0
    return-void
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 72
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 73
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->setupHeaderListLayout()V

    .line 74
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->EXPLORE_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->setupActionBar(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    .line 75
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 76
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 82
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->setUpAdapter()V

    .line 83
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->updateErrorView()V

    .line 84
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->refreshIfNeeded()V

    .line 85
    return-void
.end method

.method setUpAdapter()V
    .locals 6

    .prologue
    .line 123
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    .line 124
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    .line 125
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->getTopicsListWithSpecialItems()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->getNumColumns()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setFixedNumColumns(I)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v2

    .line 126
    .local v2, "group":Lcom/google/android/libraries/bind/card/CardGroup;
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 128
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 130
    .local v1, "cardList":Lcom/google/android/libraries/bind/data/DataList;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->ACTION_BAR:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    .line 131
    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->ACTION_BAR:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->useSpacerTopPadding(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 133
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$3;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->ACTION_BAR:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v4, p0, v5}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 146
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 147
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 148
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-direct {v4}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 149
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 0
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 242
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->setUserVisibleHint(Z)V

    .line 243
    if-eqz p1, :cond_0

    .line 244
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->refreshIfNeeded()V

    .line 246
    :cond_0
    return-void
.end method

.method protected updateErrorView()V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->refreshErrorView()Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 120
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 51
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragmentState;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragmentState;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragmentState;)V
    .locals 0
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragmentState;

    .prologue
    .line 113
    return-void
.end method
