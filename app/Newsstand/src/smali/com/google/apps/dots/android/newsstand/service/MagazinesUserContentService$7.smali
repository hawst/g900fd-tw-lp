.class final Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$7;
.super Ljava/lang/Object;
.source "MagazinesUserContentService.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetchMyOffersInternal(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$7;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$7;->val$context:Landroid/content/Context;

    # invokes: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetchMyMagazinesInternal(Landroid/content/Context;)V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$900(Landroid/content/Context;)V

    .line 399
    return-void
.end method

.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 1
    .param p1, "mutationResponse"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$7;->val$context:Landroid/content/Context;

    # invokes: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->processMyOffersResponse(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$800(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    .line 394
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 390
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$7;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    return-void
.end method
