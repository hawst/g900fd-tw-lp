.class public Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;
.super Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;
.source "CardWarmWelcome.java"


# static fields
.field public static final DK_BODY:I

.field public static final DK_BUTTON_1_ICON_DRAWABLE_ID:I

.field public static final DK_BUTTON_1_ON_CLICK_LISTENER:I

.field public static final DK_BUTTON_1_TEXT:I

.field public static final DK_BUTTON_2_ICON_DRAWABLE_ID:I

.field public static final DK_BUTTON_2_ON_CLICK_LISTENER:I

.field public static final DK_BUTTON_2_TEXT:I

.field public static final DK_LOGO_DRAWABLE_ID:I

.field public static final DK_TITLE:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 24
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardWarmWelcome_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_TITLE:I

    .line 25
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardWarmWelcome_body:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BODY:I

    .line 26
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardWarmWelcome_logoDrawableId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_LOGO_DRAWABLE_ID:I

    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardWarmWelcome_button1Text:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BUTTON_1_TEXT:I

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardWarmWelcome_button1IconDrawableId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BUTTON_1_ICON_DRAWABLE_ID:I

    .line 30
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardWarmWelcome_button1OnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BUTTON_1_ON_CLICK_LISTENER:I

    .line 33
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardWarmWelcome_button2Text:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BUTTON_2_TEXT:I

    .line 34
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardWarmWelcome_button2IconDrawableId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BUTTON_2_ICON_DRAWABLE_ID:I

    .line 35
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardWarmWelcome_button2OnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BUTTON_2_ON_CLICK_LISTENER:I

    .line 41
    const/4 v0, 0x2

    new-array v0, v0, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_one_column:I

    aput v1, v0, v3

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_two_column:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->LAYOUTS:[I

    .line 44
    new-array v0, v3, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public static getEmptyBackgroundLayoutId(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    .line 126
    .local v0, "deviceCategory":Lcom/google/apps/dots/shared/DeviceCategory;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$3;->$SwitchMap$com$google$apps$dots$shared$DeviceCategory:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/shared/DeviceCategory;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 133
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-ne v1, v2, :cond_0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->empty_bg_and_card_warm_welcome_one_col:I

    :goto_0
    return v1

    .line 128
    :pswitch_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->empty_bg_and_card_warm_welcome_two_col:I

    goto :goto_0

    .line 130
    :pswitch_1
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->empty_bg_and_card_warm_welcome_one_col:I

    goto :goto_0

    .line 133
    :cond_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->empty_bg_and_card_warm_welcome_two_col:I

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getStandardLayoutId(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 106
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    .line 107
    .local v0, "deviceCategory":Lcom/google/apps/dots/shared/DeviceCategory;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$3;->$SwitchMap$com$google$apps$dots$shared$DeviceCategory:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/shared/DeviceCategory;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 114
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-ne v1, v2, :cond_0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_one_column:I

    :goto_0
    return v1

    .line 109
    :pswitch_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_two_column:I

    goto :goto_0

    .line 111
    :pswitch_1
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_one_column:I

    goto :goto_0

    .line 114
    :cond_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_two_column:I

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static makeHidingOnClickListener(Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "optBefore"    # Ljava/lang/Runnable;
    .param p1, "optAfter"    # Ljava/lang/Runnable;

    .prologue
    .line 85
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$2;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$2;-><init>(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    return-object v0
.end method


# virtual methods
.method protected fadeOutCard(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "optAfter"    # Ljava/lang/Runnable;

    .prologue
    .line 66
    const/16 v0, 0x1f4

    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$1;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;Ljava/lang/Runnable;)V

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fadeOut(Landroid/view/View;ILjava/lang/Runnable;)V

    .line 75
    return-void
.end method
