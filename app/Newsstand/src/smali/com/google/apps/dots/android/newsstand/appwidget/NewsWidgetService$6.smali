.class Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$6;
.super Landroid/support/v4/util/LruCache;
.source "NewsWidgetService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;
    .param p2, "arg0"    # I

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$6;->this$0:Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;

    invoke-direct {p0, p2}, Landroid/support/v4/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 167
    check-cast p2, Ljava/lang/String;

    check-cast p3, Lcom/google/common/util/concurrent/ListenableFuture;

    check-cast p4, Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$6;->entryRemoved(ZLjava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V

    return-void
.end method

.method protected entryRemoved(ZLjava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 1
    .param p1, "evicted"    # Z
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 171
    .local p3, "oldBitmapFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;"
    .local p4, "newBitmapFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 172
    return-void
.end method
