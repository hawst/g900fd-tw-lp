.class Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$1;
.super Ljava/lang/Object;
.source "CardListVisitor.java"

# interfaces
.implements Lcom/google/android/libraries/bind/util/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addReadNowShelfIfNeeded()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/libraries/bind/util/Provider",
        "<",
        "Lcom/google/android/libraries/bind/card/CardGroup;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

.field final synthetic val$isMagazineShelf:Z

.field final synthetic val$shelfList:Lcom/google/android/libraries/bind/data/DataList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/android/libraries/bind/data/DataList;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .prologue
    .line 415
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$1;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$1;->val$shelfList:Lcom/google/android/libraries/bind/data/DataList;

    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$1;->val$isMagazineShelf:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 3

    .prologue
    .line 418
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$1;->val$shelfList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setMaxRows(I)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v0

    .line 419
    .local v0, "shelfGroup":Lcom/google/android/libraries/bind/card/GridGroup;
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$1;->val$isMagazineShelf:Z

    if-eqz v1, :cond_0

    .line 420
    sget v1, Lcom/google/android/apps/newsstanddev/R$integer;->num_magazine_cols:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/card/GridGroup;->setFixedNumColumnsResId(I)Lcom/google/android/libraries/bind/card/GridGroup;

    .line 424
    :goto_0
    return-object v0

    .line 422
    :cond_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$integer;->num_source_shortcut_cols:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/card/GridGroup;->setFixedNumColumnsResId(I)Lcom/google/android/libraries/bind/card/GridGroup;

    goto :goto_0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$1;->get()Lcom/google/android/libraries/bind/card/CardGroup;

    move-result-object v0

    return-object v0
.end method
