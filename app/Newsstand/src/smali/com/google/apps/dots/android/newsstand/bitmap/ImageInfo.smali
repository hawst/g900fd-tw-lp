.class public Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
.super Ljava/lang/Object;
.source "ImageInfo.java"


# static fields
.field private static maxTextureSize:I


# instance fields
.field public final hasAlpha:Z

.field public final height:I

.field public final mimeType:Ljava/lang/String;

.field public final width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, -0x1

    sput v0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->maxTextureSize:I

    return-void
.end method

.method public constructor <init>(IIZLjava/lang/String;)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "hasAlpha"    # Z
    .param p4, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->width:I

    .line 27
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->height:I

    .line 28
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->hasAlpha:Z

    .line 29
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->mimeType:Ljava/lang/String;

    .line 30
    return-void
.end method

.method private getMaxTextureSize()I
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 48
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getLargerDisplayDimension()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method public canDrawIntoMutableBitmap()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->width:I

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->height:I

    if-gez v2, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    .line 57
    const-string v2, "image/webp"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->mimeType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 58
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 61
    :cond_3
    const-string v2, "image/jpeg"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->mimeType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "image/png"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->mimeType:Ljava/lang/String;

    .line 62
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method public getNumPixels()I
    .locals 2

    .prologue
    .line 33
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->width:I

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->height:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public isTooLargeForTexture()Z
    .locals 2

    .prologue
    .line 37
    sget v0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->maxTextureSize:I

    if-gtz v0, :cond_0

    .line 38
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->getMaxTextureSize()I

    move-result v0

    sput v0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->maxTextureSize:I

    .line 40
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->width:I

    sget v1, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->maxTextureSize:I

    if-gt v0, v1, :cond_1

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->height:I

    sget v1, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->maxTextureSize:I

    if-le v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 73
    const-class v0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "width"

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->width:I

    .line 74
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "height"

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->height:I

    .line 75
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "hasAlpha"

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->hasAlpha:Z

    .line 76
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mimeType"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->mimeType:Ljava/lang/String;

    .line 77
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
