.class Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;
.super Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;
.source "EditionPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 541
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method protected handleExitRemapSharedElements(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Ljava/util/List;Ljava/util/Map;)V
    .locals 2
    .param p1, "controller"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 556
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;"
    .local p2, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v1, :cond_0

    .line 557
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getCurrentListView()Landroid/view/ViewGroup;

    move-result-object v0

    .line 558
    .local v0, "currentListView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 559
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "currentListView":Landroid/view/View;
    invoke-virtual {v1, v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->handleRemapSharedElements(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/Map;)V

    .line 562
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->handleExitRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V

    .line 564
    return-void
.end method

.method protected bridge synthetic handleExitRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 541
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;->handleExitRemapSharedElements(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Ljava/util/List;Ljava/util/Map;)V

    return-void
.end method

.method protected headerListLayout(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 550
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method protected bridge synthetic headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 541
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;->headerListLayout(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    return-object v0
.end method

.method protected sharedElementView(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)Landroid/view/View;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 545
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->heroView:Landroid/view/View;

    return-object v0
.end method

.method protected bridge synthetic sharedElementView(Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 541
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;->sharedElementView(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
