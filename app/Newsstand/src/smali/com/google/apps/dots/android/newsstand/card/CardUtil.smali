.class public Lcom/google/apps/dots/android/newsstand/card/CardUtil;
.super Ljava/lang/Object;
.source "CardUtil.java"


# direct methods
.method public static isCompactModeAvailable()Z
    .locals 2

    .prologue
    .line 42
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->NORMAL_TABLET:Lcom/google/apps/dots/shared/DeviceCategory;

    if-eq v0, v1, :cond_0

    .line 43
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isImageLowResForColumn(I)Z
    .locals 1
    .param p0, "imageWidthPx"    # I

    .prologue
    .line 77
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->isImageLowResForColumnSpan(II)Z

    move-result v0

    return v0
.end method

.method public static isImageLowResForColumnSpan(II)Z
    .locals 7
    .param p0, "imageWidthPx"    # I
    .param p1, "columnSpan"    # I

    .prologue
    const/4 v4, 0x1

    .line 85
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getTypicalNumColumnsPerScreen()I

    move-result v3

    .line 86
    .local v3, "totalColumns":I
    invoke-static {p1, v4, v3}, Lcom/google/apps/dots/android/newsstand/util/MathUtil;->clamp(III)I

    move-result p1

    .line 88
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 89
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v5, v5

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getXDpi()F

    move-result v6

    div-float v1, v5, v6

    .line 90
    .local v1, "screenWidthInches":F
    int-to-float v5, v3

    div-float v5, v1, v5

    int-to-float v6, p1

    mul-float v2, v5, v6

    .line 92
    .local v2, "spanWidthInches":F
    int-to-float v5, p0

    div-float/2addr v5, v2

    const/high16 v6, 0x42c00000    # 96.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isImageTooLowResForHeader(Lcom/google/apps/dots/android/newsstand/edition/Edition;I)Z
    .locals 5
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p1, "imageWidthPx"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getXDpi()F

    move-result v4

    div-float v0, v3, v4

    .line 102
    .local v0, "widthInches":F
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->TOPIC:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-eq v3, v4, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-eq v3, v4, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->READ_NOW:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v3, v4, :cond_3

    .line 104
    :cond_0
    int-to-float v3, p1

    div-float/2addr v3, v0

    const/high16 v4, 0x43160000    # 150.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 106
    :cond_1
    :goto_0
    return v1

    :cond_2
    move v1, v2

    .line 104
    goto :goto_0

    .line 106
    :cond_3
    int-to-float v3, p1

    div-float/2addr v3, v0

    const/high16 v4, 0x42c00000    # 96.0f

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_1

    move v1, v2

    goto :goto_0
.end method

.method public static isPrimaryImageEligibleForLargeLayout(Lcom/google/android/libraries/bind/data/Data;I)Z
    .locals 2
    .param p0, "cardData"    # Lcom/google/android/libraries/bind/data/Data;
    .param p1, "columnSpan"    # I

    .prologue
    .line 96
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_WIDTH:I

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v0

    .line 97
    .local v0, "imageWidth":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->isImageLowResForColumnSpan(II)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static toCompactLayout(I)I
    .locals 1
    .param p0, "layoutResId"    # I

    .prologue
    .line 51
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item:I

    if-ne p0, v0, :cond_1

    .line 52
    sget p0, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_compact:I

    .line 68
    .end local p0    # "layoutResId":I
    :cond_0
    :goto_0
    return p0

    .line 53
    .restart local p0    # "layoutResId":I
    :cond_1
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_no_image:I

    if-ne p0, v0, :cond_2

    .line 54
    sget p0, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_no_image_compact:I

    goto :goto_0

    .line 55
    :cond_2
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_full_image:I

    if-ne p0, v0, :cond_3

    .line 56
    sget p0, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_full_image_compact:I

    goto :goto_0

    .line 57
    :cond_3
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_splash_item_magazine:I

    if-ne p0, v0, :cond_4

    .line 58
    sget p0, Lcom/google/android/apps/newsstanddev/R$layout;->card_splash_item_magazine_compact:I

    goto :goto_0

    .line 59
    :cond_4
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_item:I

    if-ne p0, v0, :cond_5

    .line 60
    sget p0, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_item_compact:I

    goto :goto_0

    .line 61
    :cond_5
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_list_item:I

    if-ne p0, v0, :cond_6

    .line 62
    sget p0, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_list_item_compact:I

    goto :goto_0

    .line 63
    :cond_6
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item:I

    if-ne p0, v0, :cond_7

    .line 64
    sget p0, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_compact:I

    goto :goto_0

    .line 65
    :cond_7
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine:I

    if-ne p0, v0, :cond_0

    .line 66
    sget p0, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine_compact:I

    goto :goto_0
.end method

.method public static useCompactMode()Z
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->isCompactModeAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->isCompactModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
