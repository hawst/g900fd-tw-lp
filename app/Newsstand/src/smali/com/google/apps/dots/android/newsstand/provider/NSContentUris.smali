.class public Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;
.super Ljava/lang/Object;
.source "NSContentUris.java"


# static fields
.field private static contentAuthority:Ljava/lang/String;

.field private static contentUri:Landroid/net/Uri;

.field private static exportedContentAuthority:Ljava/lang/String;

.field private static initialized:Z


# direct methods
.method public static contentAuthority()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->initialized:Z

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 26
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentAuthority:Ljava/lang/String;

    return-object v0
.end method

.method public static contentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 35
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->initialized:Z

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 36
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentUri:Landroid/net/Uri;

    return-object v0
.end method

.method public static exportedContentAuthority()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->initialized:Z

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 31
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->exportedContentAuthority:Ljava/lang/String;

    return-object v0
.end method

.method public static getAttachmentUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 6
    .param p0, "attachmentId"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->exportedContentAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x16

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "content://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/attachment/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "uriString":Ljava/lang/String;
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static getSyncedFileUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 44
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "synced"

    .line 45
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 46
    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static init(Ljava/lang/String;)V
    .locals 4
    .param p0, "contentAuthority"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 17
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->initialized:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 18
    sput-object p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentAuthority:Ljava/lang/String;

    .line 19
    const-string v2, "content://"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentUri:Landroid/net/Uri;

    .line 20
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ".exported"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->exportedContentAuthority:Ljava/lang/String;

    .line 21
    sput-boolean v1, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->initialized:Z

    .line 22
    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 19
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
