.class public final Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientEntity"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;


# instance fields
.field private bitField0_:I

.field private curationAppFamilyId_:Ljava/lang/String;

.field private curationAppId_:Ljava/lang/String;

.field private description_:Ljava/lang/String;

.field private id_:Ljava/lang/String;

.field private latDegrees_:D

.field private lngDegrees_:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22336
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 22337
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 22342
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->id_:Ljava/lang/String;

    .line 22364
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->description_:Ljava/lang/String;

    .line 22386
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->latDegrees_:D

    .line 22405
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->lngDegrees_:D

    .line 22424
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    .line 22446
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    .line 22337
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    .locals 3

    .prologue
    .line 22483
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22487
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    return-object v0

    .line 22484
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :catch_0
    move-exception v1

    .line 22485
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 22333
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 22492
    if-ne p1, p0, :cond_1

    .line 22500
    :cond_0
    :goto_0
    return v1

    .line 22493
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 22494
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 22495
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->id_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->id_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->description_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->description_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 22496
    :goto_2
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->latDegrees_:D

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->latDegrees_:D

    cmpl-double v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->lngDegrees_:D

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->lngDegrees_:D

    cmpl-double v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 22499
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 22500
    goto :goto_0

    .line 22495
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->id_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->id_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->description_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->description_:Ljava/lang/String;

    .line 22496
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    .line 22499
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    .line 22500
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getCurationAppFamilyId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22426
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    return-object v0
.end method

.method public getCurationAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22448
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22366
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22344
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 22539
    const/4 v0, 0x0

    .line 22540
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 22541
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->description_:Ljava/lang/String;

    .line 22542
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22544
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 22545
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->latDegrees_:D

    .line 22546
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 22548
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 22549
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->lngDegrees_:D

    .line 22550
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 22552
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    .line 22553
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->id_:Ljava/lang/String;

    .line 22554
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22556
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 22557
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    .line 22558
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22560
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 22561
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    .line 22562
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22564
    :cond_5
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->cachedSize:I

    .line 22565
    return v0
.end method

.method public hasCurationAppFamilyId()Z
    .locals 1

    .prologue
    .line 22437
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCurationAppId()Z
    .locals 1

    .prologue
    .line 22459
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v2, 0x0

    .line 22504
    const/16 v0, 0x11

    .line 22505
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 22506
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->id_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 22507
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->description_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 22508
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->latDegrees_:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->latDegrees_:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 22509
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->lngDegrees_:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->lngDegrees_:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 22510
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 22511
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_3
    add-int v0, v1, v2

    .line 22512
    return v0

    .line 22506
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->id_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 22507
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->description_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 22510
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 22511
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22573
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 22574
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 22578
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 22579
    :sswitch_0
    return-object p0

    .line 22584
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->description_:Ljava/lang/String;

    .line 22585
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    goto :goto_0

    .line 22589
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->latDegrees_:D

    .line 22590
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    goto :goto_0

    .line 22594
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->lngDegrees_:D

    .line 22595
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    goto :goto_0

    .line 22599
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->id_:Ljava/lang/String;

    .line 22600
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    goto :goto_0

    .line 22604
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    .line 22605
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    goto :goto_0

    .line 22609
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    .line 22610
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    goto :goto_0

    .line 22574
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x19 -> :sswitch_2
        0x21 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22333
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v0

    return-object v0
.end method

.method public setCurationAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 22429
    if-nez p1, :cond_0

    .line 22430
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 22432
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    .line 22433
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    .line 22434
    return-object p0
.end method

.method public setCurationAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 22451
    if-nez p1, :cond_0

    .line 22452
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 22454
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    .line 22455
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    .line 22456
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22517
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 22518
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->description_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 22520
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 22521
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->latDegrees_:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 22523
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 22524
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->lngDegrees_:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 22526
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 22527
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->id_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 22529
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 22530
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppFamilyId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 22532
    :cond_4
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 22533
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->curationAppId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 22535
    :cond_5
    return-void
.end method
