.class public Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "MagazineEditionCardListFilter.java"


# instance fields
.field private hasTextSection:Z

.field private final liteMode:Z

.field private final magazineEdition:Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

.field private final rawCardList:Lcom/google/android/libraries/bind/data/DataList;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;Lcom/google/android/libraries/bind/data/DataList;Z)V
    .locals 1
    .param p1, "magazineEdition"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;
    .param p2, "rawCardList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p3, "liteMode"    # Z

    .prologue
    .line 35
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    .line 36
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->magazineEdition:Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    .line 37
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->rawCardList:Lcom/google/android/libraries/bind/data/DataList;

    .line 38
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->liteMode:Z

    .line 39
    return-void
.end method

.method public static getCombinedPostIdAndPageNumber(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p0, "postId"    # Ljava/lang/String;
    .param p1, "pageNumber"    # I

    .prologue
    .line 158
    const-string v0, "%s %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 5
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 62
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->hasTextSection:Z

    if-eqz v3, :cond_0

    .line 63
    sget v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_SECTION_SUMMARY:I

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 64
    .local v0, "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->liteMode:Z

    if-eqz v3, :cond_2

    .line 66
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->getTocType()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    .line 73
    .end local v0    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_0
    :goto_0
    return v1

    .restart local v0    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_1
    move v1, v2

    .line 66
    goto :goto_0

    .line 69
    :cond_2
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->hasCorrespondingImageSectionId()Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public onPreFilter()V
    .locals 4

    .prologue
    .line 45
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->hasTextSection:Z

    .line 46
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->rawCardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 47
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->rawCardList:Lcom/google/android/libraries/bind/data/DataList;

    .line 48
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    sget v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_SECTION_SUMMARY:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 49
    .local v1, "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->hasCorrespondingImageSectionId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 50
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->hasTextSection:Z

    .line 54
    .end local v1    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_0
    return-void

    .line 46
    .restart local v1    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 13
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v12, 0x1

    .line 82
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Lcom/google/android/play/utils/collections/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v6

    .line 85
    .local v6, "outputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v1, 0x0

    .line 87
    .local v1, "currentSectionId":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    .line 91
    .local v2, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v9, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IMAGE_ID:I

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v9

    if-eqz v9, :cond_1

    sget v9, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IMAGE_ID:I

    .line 92
    invoke-virtual {v2, v9}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v9

    if-nez v9, :cond_1

    .line 93
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 94
    .local v7, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v9, v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 95
    const/4 v3, 0x0

    .line 100
    .local v3, "firstInCurrentSection":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 101
    sget v9, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IMAGE_ID:I

    sget v11, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IMAGE_ID:I

    invoke-virtual {v2, v9, v11}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 103
    sget v9, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_ON_CLICK_LISTENER:I

    sget v11, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_ON_CLICK_LISTENER:I

    invoke-virtual {v2, v9, v11}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 105
    sget v9, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IMAGE_ID:I

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/bind/data/Data;->remove(I)V

    .line 106
    sget v9, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_ON_CLICK_LISTENER:I

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/bind/data/Data;->remove(I)V

    .line 111
    .end local v3    # "firstInCurrentSection":Z
    .end local v7    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_1
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ID:I

    sget v11, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->DK_PRIMARY_KEY:I

    invoke-virtual {v2, v9, v11}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 112
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    sget v9, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IMAGE_ID:I

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 118
    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 119
    .restart local v7    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    const/4 v8, 0x0

    .line 120
    .local v8, "scrubberImages":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v9

    sget-object v11, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-ne v9, v11, :cond_5

    .line 121
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getNumHorizontalPortraitThumbnails()I

    move-result v9

    if-lez v9, :cond_4

    .line 122
    iget-object v8, v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 133
    :cond_2
    :goto_1
    if-eqz v8, :cond_0

    array-length v9, v8

    if-le v9, v12, :cond_0

    .line 134
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_2
    array-length v9, v8

    if-ge v4, v9, :cond_0

    .line 135
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/bind/data/Data;-><init>(Lcom/google/android/libraries/bind/data/Data;)V

    .line 136
    .local v0, "additionalData":Lcom/google/android/libraries/bind/data/Data;
    sget v9, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->DK_PRIMARY_KEY:I

    iget-object v11, v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 137
    invoke-static {v11, v4}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->getCombinedPostIdAndPageNumber(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v11

    .line 136
    invoke-virtual {v0, v9, v11}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 140
    aget-object v9, v8, v4

    invoke-static {v0, v9}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->putScrubberImageInfo(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)V

    .line 143
    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->magazineEdition:Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    sget v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    .line 145
    invoke-virtual {v2, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 144
    invoke-static {v11, v9, v4}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->makeOnClickListener(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;I)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;

    move-result-object v5

    .line 146
    .local v5, "onClickListener":Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    sget v9, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_ON_CLICK_LISTENER:I

    invoke-virtual {v0, v9, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 148
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 97
    .end local v0    # "additionalData":Lcom/google/android/libraries/bind/data/Data;
    .end local v4    # "i":I
    .end local v5    # "onClickListener":Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    .end local v8    # "scrubberImages":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_3
    iget-object v1, v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    .line 98
    const/4 v3, 0x1

    .restart local v3    # "firstInCurrentSection":Z
    goto/16 :goto_0

    .line 123
    .end local v3    # "firstInCurrentSection":Z
    .restart local v8    # "scrubberImages":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_4
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getNumHorizontalLandscapeThumbnails()I

    move-result v9

    if-le v9, v12, :cond_2

    .line 124
    iget-object v8, v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    goto :goto_1

    .line 127
    :cond_5
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getNumHorizontalLandscapeThumbnails()I

    move-result v9

    if-lez v9, :cond_6

    .line 128
    iget-object v8, v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    goto :goto_1

    .line 129
    :cond_6
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getNumHorizontalPortraitThumbnails()I

    move-result v9

    if-le v9, v12, :cond_2

    .line 130
    iget-object v8, v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    goto :goto_1

    .line 154
    .end local v2    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v7    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .end local v8    # "scrubberImages":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_7
    return-object v6
.end method
