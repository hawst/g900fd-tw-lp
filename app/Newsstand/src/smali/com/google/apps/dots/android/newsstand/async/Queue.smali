.class public Lcom/google/apps/dots/android/newsstand/async/Queue;
.super Lcom/google/android/libraries/bind/async/Queue;
.source "Queue.java"


# instance fields
.field private final listeningExecutor:Lcom/google/common/util/concurrent/ListeningExecutorService;


# direct methods
.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "poolSize"    # I
    .param p3, "jankLocked"    # Z

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/async/Queue;-><init>(Ljava/lang/String;IZ)V

    .line 20
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/Queue;->executor:Ljava/util/concurrent/Executor;

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-static {v0}, Lcom/google/common/util/concurrent/MoreExecutors;->listeningDecorator(Ljava/util/concurrent/ExecutorService;)Lcom/google/common/util/concurrent/ListeningExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/Queue;->listeningExecutor:Lcom/google/common/util/concurrent/ListeningExecutorService;

    .line 21
    return-void
.end method


# virtual methods
.method public submit(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/Queue;->listeningExecutor:Lcom/google/common/util/concurrent/ListeningExecutorService;

    invoke-interface {v0, p1}, Lcom/google/common/util/concurrent/ListeningExecutorService;->submit(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
