.class public Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;
.super Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;
.source "GoToPurchaseToastOperation.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final backendDocId:Ljava/lang/String;

.field private final docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field private final parentBackendDocId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/DocType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "docType"    # Lcom/google/apps/dots/android/newsstand/util/DocType;
    .param p4, "backendDocId"    # Ljava/lang/String;
    .param p5, "parentBackendDocId"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;->getOperationLabelForDocType(Landroid/content/res/Resources;Lcom/google/apps/dots/android/newsstand/util/DocType;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 30
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;->docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 31
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;->backendDocId:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;->parentBackendDocId:Ljava/lang/String;

    .line 33
    return-void
.end method

.method private static getOperationLabelForDocType(Landroid/content/res/Resources;Lcom/google/apps/dots/android/newsstand/util/DocType;)Ljava/lang/String;
    .locals 2
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "type"    # Lcom/google/apps/dots/android/newsstand/util/DocType;

    .prologue
    .line 54
    sget-object v0, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation$1;->$SwitchMap$com$google$apps$dots$android$newsstand$util$DocType:[I

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/util/DocType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 81
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_open_thing:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 57
    :pswitch_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_open_music:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 62
    :pswitch_1
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_open_tv_show:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 65
    :pswitch_2
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_open_movie:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 68
    :pswitch_3
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_open_book:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 72
    :pswitch_4
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_open_magazine:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 75
    :pswitch_5
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_open_edition:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 78
    :pswitch_6
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_open_app:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public onActionClicked(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 38
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;->docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->forDocType(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;->backendDocId:Ljava/lang/String;

    .line 39
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;->parentBackendDocId:Ljava/lang/String;

    .line 40
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->setParentBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->start()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "problem launching viewer: %s"

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->iap_failure_to_open:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$integer;->actionable_toast_bar_display_iap_short_ms:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 47
    invoke-static {v1, v2, v3, v6, v4}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->showToast(Landroid/app/Activity;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;ZI)V

    goto :goto_0
.end method
