.class public Lcom/google/apps/dots/android/newsstand/data/EventDataList;
.super Lcom/google/android/libraries/bind/data/DataList;
.source "EventDataList.java"


# instance fields
.field private final eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

.field private final eventObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

.field private final observedUris:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "primaryKey"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/DataList;-><init>(I)V

    .line 21
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->observedUris:Ljava/util/Set;

    .line 22
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/EventDataList$1;

    .line 23
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadExecutor()Lcom/google/common/util/concurrent/ListeningExecutorService;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/data/EventDataList$1;-><init>(Lcom/google/apps/dots/android/newsstand/data/EventDataList;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->eventObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    .line 33
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/data/EventDataList;)Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/EventDataList;

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    return-object v0
.end method

.method private registerEventObservers()V
    .locals 4

    .prologue
    .line 91
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->observedUris:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 92
    .local v0, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->eventObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->registerObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    goto :goto_0

    .line 94
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method private unregisterEventObservers()V
    .locals 4

    .prologue
    .line 97
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->observedUris:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 98
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->observedUris:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 99
    .local v0, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->eventObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->unregisterObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    goto :goto_0

    .line 102
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->setDirty(Z)V

    .line 104
    :cond_1
    return-void
.end method


# virtual methods
.method public addEventUriToWatch(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 53
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 54
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->observedUris:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->isRegisteredForInvalidation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->eventObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->registerObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 60
    :cond_0
    return-void
.end method

.method protected onEvent(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p2, "extras":Ljava/util/Map;, "Ljava/util/Map<**>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->invalidateData()V

    .line 45
    return-void
.end method

.method protected onRegisterForInvalidation()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataList;->onRegisterForInvalidation()V

    .line 81
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->registerEventObservers()V

    .line 82
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 0

    .prologue
    .line 86
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataList;->onUnregisterForInvalidation()V

    .line 87
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->unregisterEventObservers()V

    .line 88
    return-void
.end method

.method public removeEventUriToWatch(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 68
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 69
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->observedUris:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->isRegisteredForInvalidation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->eventObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->unregisterObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 76
    :cond_0
    return-void
.end method
