.class Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$16;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardListVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCardSourceListItem(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

.field final synthetic val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .prologue
    .line 1065
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$16;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$16;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1068
    new-instance v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$16;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->getAnalyticsScreenName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$16;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;->track(Z)V

    .line 1070
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-direct {v2, p2}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$16;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 1071
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v0

    .line 1074
    .local v0, "editionIntentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;
    const-class v2, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;

    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->source_icon:I

    .line 1075
    invoke-static {p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewInParent(Landroid/view/View;Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v1

    .line 1076
    .local v1, "icon":Landroid/view/View;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 1077
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    .line 1079
    :cond_0
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    .line 1080
    return-void
.end method
