.class public final Lcom/google/apps/dots/android/newsstand/reading/NewsArticleBaseHtmlLoader;
.super Ljava/lang/Object;
.source "NewsArticleBaseHtmlLoader.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;


# instance fields
.field private final articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
    .locals 0
    .param p1, "articleLoader"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleBaseHtmlLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .line 18
    return-void
.end method


# virtual methods
.method public load(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleBaseHtmlLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getNewsBaseHtmlFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
