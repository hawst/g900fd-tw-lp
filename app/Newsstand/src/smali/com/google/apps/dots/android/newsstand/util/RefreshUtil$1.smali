.class final Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "RefreshUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->refreshIfNeeded(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 9
    .param p1, "response"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 47
    if-nez p1, :cond_0

    .line 49
    # getter for: Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Autorefreshing edition: %s"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;->val$context:Landroid/content/Context;

    invoke-virtual {v2, v3, v6, v8}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->refresh(Landroid/content/Context;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 65
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    iget-wide v4, v4, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->writeTime:J

    sub-long v0, v2, v4

    .line 55
    .local v0, "writeAge":J
    const-wide/32 v2, 0x1b7740

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 56
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    # getter for: Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Autorefreshing edition: %s"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;->val$context:Landroid/content/Context;

    invoke-virtual {v2, v3, v8, v8}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->refresh(Landroid/content/Context;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 61
    :cond_1
    # getter for: Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Skipped autorefresh of edition: %s, writeAge: %d seconds"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v5, v4, v6

    const-wide/16 v6, 0x3e8

    div-long v6, v0, v6

    .line 62
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    .line 61
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    return-void
.end method
