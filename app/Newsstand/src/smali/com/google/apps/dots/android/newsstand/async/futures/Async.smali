.class public Lcom/google/apps/dots/android/newsstand/async/futures/Async;
.super Ljava/lang/Object;
.source "Async.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnFailure;,
        Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnSuccess;,
        Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;
    }
.end annotation


# static fields
.field private static final DEREFERENCER:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final IMMEDIATE_CANCELLED_FUTURE:Lcom/google/common/util/concurrent/ListenableFuture;

.field public static final sameThreadExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    .line 102
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;-><init>(Lcom/google/apps/dots/android/newsstand/async/futures/Async$1;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->IMMEDIATE_CANCELLED_FUTURE:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 228
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$7;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$7;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->DEREFERENCER:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    return-void
.end method

.method public static addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;",
            "Lcom/google/common/util/concurrent/FutureCallback",
            "<-TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 253
    .local p0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    .local p1, "callback":Lcom/google/common/util/concurrent/FutureCallback;, "Lcom/google/common/util/concurrent/FutureCallback<-TV;>;"
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-static {p0, p1, v0}, Lcom/google/common/util/concurrent/Futures;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)V

    .line 254
    return-object p0
.end method

.method public static addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 0
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;",
            "Lcom/google/common/util/concurrent/FutureCallback",
            "<-TV;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 260
    .local p0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    .local p1, "callback":Lcom/google/common/util/concurrent/FutureCallback;, "Lcom/google/common/util/concurrent/FutureCallback<-TV;>;"
    invoke-static {p0, p1, p2}, Lcom/google/common/util/concurrent/Futures;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)V

    .line 261
    return-object p0
.end method

.method public static addListener(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "listener"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;",
            "Ljava/lang/Runnable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 239
    .local p0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {p0, p1, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 240
    return-object p0
.end method

.method public static addSynchronousCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;",
            "Lcom/google/common/util/concurrent/FutureCallback",
            "<-TV;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 270
    .local p0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    .local p1, "callback":Lcom/google/common/util/concurrent/FutureCallback;, "Lcom/google/common/util/concurrent/FutureCallback<-TV;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$8;

    invoke-direct {v0, p1, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$8;-><init>(Lcom/google/common/util/concurrent/FutureCallback;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-static {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static allAsList(Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 311
    .local p0, "futures":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;>;"
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->listFuture(Ljava/util/Collection;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static varargs allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">([",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 305
    .local p0, "futures":[Lcom/google/common/util/concurrent/ListenableFuture;, "[Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;"
    invoke-static {p0}, Lcom/google/common/collect/ImmutableList;->copyOf([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->listFuture(Ljava/util/Collection;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static asTransform(Lcom/google/common/base/Function;)Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/base/Function",
            "<-TI;+TO;>;)",
            "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
            "<TI;TO;>;"
        }
    .end annotation

    .prologue
    .line 132
    .local p0, "function":Lcom/google/common/base/Function;, "Lcom/google/common/base/Function<-TI;+TO;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$3;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$3;-><init>(Lcom/google/common/base/Function;)V

    return-object v0
.end method

.method public static asTransform(Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/AsyncFunction",
            "<-TI;+TO;>;)",
            "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
            "<TI;TO;>;"
        }
    .end annotation

    .prologue
    .line 142
    .local p0, "function":Lcom/google/common/util/concurrent/AsyncFunction;, "Lcom/google/common/util/concurrent/AsyncFunction<-TI;+TO;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$4;-><init>(Lcom/google/common/util/concurrent/AsyncFunction;)V

    return-object v0
.end method

.method public static asTransform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TI;>;",
            "Lcom/google/common/util/concurrent/FutureFallback",
            "<+TI;>;)",
            "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
            "<TI;TI;>;"
        }
    .end annotation

    .prologue
    .line 152
    .local p0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TI;>;"
    .local p1, "fallback":Lcom/google/common/util/concurrent/FutureFallback;, "Lcom/google/common/util/concurrent/FutureFallback<+TI;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$5;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$5;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)V

    return-object v0
.end method

.method public static dereference(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 224
    .local p0, "nested":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;>;"
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->DEREFERENCER:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Future",
            "<TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<TV;>;"
    invoke-static {p0}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static immediateCancelledFuture()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 65
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->IMMEDIATE_CANCELLED_FUTURE:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public static immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {p0}, Lcom/google/common/util/concurrent/Futures;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(TV;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "value":Ljava/lang/Object;, "TV;"
    invoke-static {p0}, Lcom/google/common/util/concurrent/Futures;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static listFuture(Ljava/util/Collection;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "allMustSucceed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;>;Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 317
    .local p0, "futures":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;-><init>(Ljava/util/Collection;Z)V

    new-instance v1, Lcom/google/apps/dots/android/newsstand/async/futures/Async$9;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$9;-><init>(Ljava/util/Collection;)V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static makeExpiringFuture(Lcom/google/common/util/concurrent/ListenableFuture;ZJLjava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p1, "cancelOnTimeout"    # Z
    .param p2, "timeoutMs"    # J
    .param p4, "timeoutExecutor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;ZJ",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 380
    .local p0, "inputFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    new-instance v3, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$11;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$11;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-direct {v3, v0, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;-><init>(Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 387
    .local v3, "returnVal":Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture<TV;TV;>;"
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {p0, v3, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 390
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;

    move-object v2, p4

    move-wide v4, p2

    move v6, p1

    move-object v7, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;-><init>(Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;JZLcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 407
    return-object v3
.end method

.method public static makeTimerFuture(JLjava/util/concurrent/Executor;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p0, "durationMs"    # J
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .param p3, "failAtEnd"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/Executor;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 417
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 418
    .local v3, "result":Lcom/google/common/util/concurrent/SettableFuture;, "Lcom/google/common/util/concurrent/SettableFuture<Ljava/lang/Object;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v6

    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;

    move-object v1, p2

    move v2, p3

    move-wide v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;-><init>(Ljava/util/concurrent/Executor;ZLcom/google/common/util/concurrent/SettableFuture;J)V

    invoke-virtual {v6, v0, p0, p1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 434
    return-object v3
.end method

.method public static nonCancellationPropagating(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 332
    .local p0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$10;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$10;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;)V

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transformNoCancellation(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static runningOn(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "runnable"    # Ljava/lang/Runnable;
    .param p1, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 38
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    if-ne p1, v0, :cond_0

    .line 42
    .end local p0    # "runnable":Ljava/lang/Runnable;
    :goto_0
    return-object p0

    .restart local p0    # "runnable":Ljava/lang/Runnable;
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$2;

    invoke-direct {v0, p1, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$2;-><init>(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TI;>;",
            "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
            "<-TI;+TO;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 174
    .local p0, "input":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TI;>;"
    .local p1, "function":Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;, "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform<-TI;+TO;>;"
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-static {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TI;>;",
            "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
            "<-TI;+TO;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 179
    .local p0, "input":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TI;>;"
    .local p1, "function":Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;, "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform<-TI;+TO;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

    invoke-direct {v0, p1, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;-><init>(Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 180
    .local v0, "output":Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture<TI;TO;>;"
    invoke-interface {p0, v0, p2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 181
    return-object v0
.end method

.method public static transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TI;>;",
            "Lcom/google/common/base/Function",
            "<-TI;+TO;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 162
    .local p0, "input":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TI;>;"
    .local p1, "function":Lcom/google/common/base/Function;, "Lcom/google/common/base/Function<-TI;+TO;>;"
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->asTransform(Lcom/google/common/base/Function;)Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TI;>;",
            "Lcom/google/common/base/Function",
            "<-TI;+TO;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 168
    .local p0, "input":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TI;>;"
    .local p1, "function":Lcom/google/common/base/Function;, "Lcom/google/common/base/Function<-TI;+TO;>;"
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->asTransform(Lcom/google/common/base/Function;)Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TI;>;",
            "Lcom/google/common/util/concurrent/AsyncFunction",
            "<-TI;+TO;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 198
    .local p0, "input":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TI;>;"
    .local p1, "function":Lcom/google/common/util/concurrent/AsyncFunction;, "Lcom/google/common/util/concurrent/AsyncFunction<-TI;+TO;>;"
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->asTransform(Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TI;>;",
            "Lcom/google/common/util/concurrent/AsyncFunction",
            "<-TI;+TO;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 204
    .local p0, "input":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TI;>;"
    .local p1, "function":Lcom/google/common/util/concurrent/AsyncFunction;, "Lcom/google/common/util/concurrent/AsyncFunction<-TI;+TO;>;"
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->asTransform(Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static transformNoCancellation(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TI;>;",
            "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
            "<-TI;+TO;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 186
    .local p0, "input":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TI;>;"
    .local p1, "function":Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;, "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform<-TI;+TO;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$6;

    invoke-direct {v0, p1, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$6;-><init>(Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 192
    .local v0, "output":Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture<TI;TO;>;"
    invoke-interface {p0, v0, p2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 193
    return-object v0
.end method

.method public static whenAllDone(Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 287
    .local p0, "futures":Ljava/util/Collection;, "Ljava/util/Collection<+Lcom/google/common/util/concurrent/ListenableFuture<*>;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;-><init>(Ljava/util/Collection;Z)V

    return-object v0
.end method

.method public static varargs whenAllDone([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 300
    .local p0, "futures":[Lcom/google/common/util/concurrent/ListenableFuture;, "[Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;

    invoke-static {p0}, Lcom/google/common/collect/ImmutableList;->copyOf([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;-><init>(Ljava/util/Collection;Z)V

    return-object v0
.end method

.method public static withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;",
            "Lcom/google/common/util/concurrent/FutureFallback",
            "<+TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 210
    .local p0, "input":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;"
    .local p1, "fallback":Lcom/google/common/util/concurrent/FutureFallback;, "Lcom/google/common/util/concurrent/FutureFallback<+TV;>;"
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->asTransform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
