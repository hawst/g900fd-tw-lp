.class public final enum Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;
.super Ljava/lang/Enum;
.source "ProtoEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/util/ProtoEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UriType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

.field public static final enum EXTERNAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

.field public static final enum FIFE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

.field public static final enum LOCAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

.field public static final enum SCS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;


# instance fields
.field public final uriType:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 207
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->LOCAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    .line 208
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    const-string v1, "SCS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->SCS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    .line 209
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    const-string v1, "FIFE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->FIFE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    .line 210
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    const-string v1, "EXTERNAL"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->EXTERNAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    .line 206
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->LOCAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->SCS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->FIFE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->EXTERNAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "uriType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 223
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 224
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->uriType:I

    .line 225
    return-void
.end method

.method public static fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;
    .locals 5
    .param p0, "uriType"    # I

    .prologue
    .line 215
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 216
    .local v0, "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;
    iget v4, v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->uriType:I

    if-ne v4, p0, :cond_0

    .line 220
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;
    :goto_1
    return-object v0

    .line 215
    .restart local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 220
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 206
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    return-object v0
.end method
