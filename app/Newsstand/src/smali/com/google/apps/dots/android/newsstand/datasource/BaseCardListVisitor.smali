.class public Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;
.source "BaseCardListVisitor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor",
        "<",
        "Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;",
        ">;"
    }
.end annotation


# static fields
.field private static final DK_RESOURCE_BUNDLE:I

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field protected final cardIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected final primaryKey:I

.field private final resourceBundle:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;

.field private resultsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 23
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->BaseCardListVisitor_resourceBundle:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->DK_RESOURCE_BUNDLE:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "primaryKey"    # I

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;-><init>()V

    .line 26
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->globalManifest()Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->makeBundle()Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->resourceBundle:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;

    .line 27
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->cardIds:Ljava/util/Set;

    .line 30
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->resultsList:Ljava/util/List;

    .line 33
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->primaryKey:I

    .line 34
    return-void
.end method


# virtual methods
.method protected addToResults(ILcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "cardData"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->cardIds:Ljava/util/Set;

    invoke-virtual {p0, p2, v0}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->checkAndAddPrimaryKey(Lcom/google/android/libraries/bind/data/Data;Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->resultsList:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 93
    :cond_0
    return-void
.end method

.method protected addToResults(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "cardData"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 58
    if-eqz p1, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->resultsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->addToResults(ILcom/google/android/libraries/bind/data/Data;)V

    .line 61
    :cond_0
    return-void
.end method

.method protected checkAndAddPrimaryKey(Lcom/google/android/libraries/bind/data/Data;Ljava/util/Set;)Z
    .locals 7
    .param p1, "cardData"    # Lcom/google/android/libraries/bind/data/Data;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/Data;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "primaryKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Object;>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 69
    if-eqz p1, :cond_0

    .line 70
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->primaryKey:I

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 71
    .local v0, "cardId":Ljava/lang/Object;
    if-nez v0, :cond_1

    .line 72
    sget-object v3, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Null cardId at primaryKey %s (%d)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->primaryKey:I

    invoke-static {v6}, Lcom/google/android/libraries/bind/data/Data;->keyName(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->primaryKey:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    .end local v0    # "cardId":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v1

    .line 74
    .restart local v0    # "cardId":Ljava/lang/Object;
    :cond_1
    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 75
    sget-object v3, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Duplicate cardId: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v1

    invoke-virtual {v3, v4, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :cond_2
    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 79
    goto :goto_0
.end method

.method public getResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->resultsList:Ljava/util/List;

    return-object v0
.end method

.method protected makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;
    .locals 3

    .prologue
    .line 96
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 98
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->DK_RESOURCE_BUNDLE:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->resourceBundle:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 99
    return-object v0
.end method

.method public visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 5
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    .line 42
    iget-object v2, p2, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 43
    .local v0, "link":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->resourceBundle:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;

    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->registerMapping(Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;)V

    .line 42
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 45
    .end local v0    # "link":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    :cond_0
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppSummary()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 46
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->resourceBundle:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->registerAppSummary(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V

    .line 48
    :cond_1
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppFamilySummary()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 49
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->resourceBundle:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->registerAppFamilySummary(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    .line 51
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V

    .line 52
    return-void
.end method

.method public bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V

    return-void
.end method
