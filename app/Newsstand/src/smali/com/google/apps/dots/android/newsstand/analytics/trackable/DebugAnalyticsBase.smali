.class public abstract Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
.source "DebugAnalyticsBase.java"


# static fields
.field private static tracker:Lcom/google/apps/dots/android/newsstand/analytics/GATracker;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;

    .line 18
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    .line 19
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/analytics/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;-><init>(Landroid/content/Context;Lcom/google/android/gms/analytics/GoogleAnalytics;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;->tracker:Lcom/google/apps/dots/android/newsstand/analytics/GATracker;

    .line 17
    return-void
.end method

.method protected constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected abstract fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation
.end method

.method protected fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "publisherEvent"    # Z

    .prologue
    .line 44
    const-string v0, "Debug"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setCategory(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    return-object v0
.end method

.method protected getPublisherTrackingId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getSystemDebugAnalyticsId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 38
    const-string v0, "Debug"

    return-object v0
.end method

.method protected sendEventToTracker(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)V
    .locals 3
    .param p1, "analyticsEvent"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "sendToPublishers"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 51
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;->tracker:Lcom/google/apps/dots/android/newsstand/analytics/GATracker;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;->getPublisherTrackingId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->trackEvent(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V

    .line 52
    return-void
.end method

.method public track()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;->track(Z)V

    .line 56
    return-void
.end method

.method public final track(Z)V
    .locals 1
    .param p1, "sendToPublishers"    # Z

    .prologue
    .line 60
    sget v0, Lcom/google/android/apps/newsstanddev/R$bool;->enable_debug_analytics:I

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getBooleanResource(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->track(Z)V

    .line 63
    :cond_0
    return-void
.end method
