.class Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$6;
.super Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;
.source "PlainEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->setupAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-void
.end method


# virtual methods
.method public getErrorMessageData()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    .line 276
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .line 277
    # getter for: Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->access$200(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->access$200(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->getRetryRunnable()Ljava/lang/Runnable;

    move-result-object v3

    .line 276
    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public getView(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;
    .locals 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    .line 255
    const/4 v3, 0x0

    .line 256
    .local v3, "resId":Ljava/lang/Integer;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$6;->getErrorMessageData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 257
    .local v0, "errorMessageData":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_0

    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    .line 258
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "resId":Ljava/lang/Integer;
    check-cast v3, Ljava/lang/Integer;

    .restart local v3    # "resId":Ljava/lang/Integer;
    if-eqz v3, :cond_0

    .line 259
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sget v5, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->LAYOUT:I

    if-ne v4, v5, :cond_2

    .line 260
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;->getView(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;

    move-result-object v1

    .line 271
    :cond_1
    :goto_0
    return-object v1

    .line 265
    :cond_2
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v2, v4, v5}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 267
    .local v2, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->viewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v2}, Lcom/google/android/libraries/bind/view/ViewHeap;->get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v1

    .line 268
    .local v1, "errorView":Landroid/view/View;
    instance-of v4, v1, Lcom/google/android/libraries/bind/data/DataView;

    if-eqz v4, :cond_1

    move-object v4, v1

    .line 269
    check-cast v4, Lcom/google/android/libraries/bind/data/DataView;

    invoke-interface {v4, v0}, Lcom/google/android/libraries/bind/data/DataView;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0
.end method
