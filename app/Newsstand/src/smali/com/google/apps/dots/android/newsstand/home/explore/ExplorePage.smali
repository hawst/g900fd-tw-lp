.class public Lcom/google/apps/dots/android/newsstand/home/explore/ExplorePage;
.super Lcom/google/apps/dots/android/newsstand/home/HomePage;
.source "ExplorePage.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->EXPLORE:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/HomePage;-><init>(Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;)V

    .line 18
    return-void
.end method


# virtual methods
.method public getFragment(Landroid/content/Context;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;-><init>()V

    return-object v0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->explore_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
