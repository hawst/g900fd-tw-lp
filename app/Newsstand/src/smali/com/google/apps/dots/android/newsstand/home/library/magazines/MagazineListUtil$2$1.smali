.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2$1;
.super Ljava/lang/Object;
.source "MagazineListUtil.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2;->transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/libraries/bind/data/Data;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/Data;)I
    .locals 2
    .param p1, "lhs"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "rhs"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 69
    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_PUBLICATION_DATE:I

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsLong(I)Ljava/lang/Long;

    move-result-object v0

    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_PUBLICATION_DATE:I

    .line 70
    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsLong(I)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 66
    check-cast p1, Lcom/google/android/libraries/bind/data/Data;

    check-cast p2, Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$2$1;->compare(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/Data;)I

    move-result v0

    return v0
.end method
