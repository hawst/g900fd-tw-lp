.class public Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
.super Ljava/lang/Object;
.source "PlainEditionFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field public final headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 20
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 25
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 26
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 36
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    .line 38
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 41
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 30
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "{PlainEditionFragmentState: %s/%s}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    return-void
.end method
