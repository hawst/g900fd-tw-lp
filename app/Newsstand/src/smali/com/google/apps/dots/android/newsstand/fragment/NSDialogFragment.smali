.class public Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "NSDialogFragment.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field public final destroyAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 16
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->destroyAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    return-void
.end method


# virtual methods
.method public getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onCreate: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 22
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 50
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onDestroy()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroy()V

    .line 52
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->destroyAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 53
    return-void
.end method

.method public onPause()V
    .locals 5

    .prologue
    .line 38
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onPause: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onPause()V

    .line 40
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 32
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onResume: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onResume()V

    .line 34
    return-void
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 26
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onStart: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onStart()V

    .line 28
    return-void
.end method

.method public onStop()V
    .locals 5

    .prologue
    .line 44
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onStop: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onStop()V

    .line 46
    return-void
.end method
