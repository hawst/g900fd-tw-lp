.class public Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;
.super Lcom/google/android/libraries/bind/widget/BindingLinearLayout;
.source "CardLinearLayout.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/card/ResizingCard;
.implements Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;
.implements Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;


# instance fields
.field private final cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

.field private final foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

.field private isRead:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;-><init>(Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;-><init>(Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->initialize(Lcom/google/android/play/cardview/CardViewGroup;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->initialize(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->initialize(Landroid/view/ViewGroup;Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method


# virtual methods
.method public canEnlarge()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->canEnlarge()Z

    move-result v0

    return v0
.end method

.method public canShrink()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->canShrink()Z

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 189
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 190
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->draw(Landroid/view/ViewGroup;Landroid/graphics/Canvas;)V

    .line 191
    return-void
.end method

.method public drawableHotspotChanged(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 165
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->drawableHotspotChanged(FF)V

    .line 166
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->drawableHotspotChanged(FF)V

    .line 167
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->drawableStateChanged()V

    .line 158
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->drawableStateChanged(Landroid/view/ViewGroup;)V

    .line 161
    :cond_0
    return-void
.end method

.method public getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    return-object v0
.end method

.method public isRead()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->isRead:Z

    return v0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 150
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->jumpDrawablesToCurrentState()V

    .line 151
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->jumpDrawablesToCurrentState()V

    .line 152
    return-void
.end method

.method protected makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onAttachedToWindow()V

    .line 110
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onAttachedToWindow()V

    .line 111
    return-void
.end method

.method public onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->isRead()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 132
    .local v0, "drawableState":[I
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->STATE_READ:[I

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->mergeDrawableStates([I[I)[I

    .line 135
    .end local v0    # "drawableState":[I
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onDetachedFromWindow()V

    .line 104
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onDetachedFromWindow()V

    .line 105
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onFinishTemporaryDetach()V

    .line 92
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onFinishTemporaryDetach()V

    .line 93
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 177
    invoke-super/range {p0 .. p5}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onLayout(ZIIII)V

    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->onLayout()V

    .line 179
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 183
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onSizeChanged(IIII)V

    .line 184
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->onSizeChanged()V

    .line 185
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onStartTemporaryDetach()V

    .line 98
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onStartTemporaryDetach()V

    .line 99
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundColor(Lcom/google/android/play/cardview/CardViewGroup;I)V

    .line 126
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundResource(Lcom/google/android/play/cardview/CardViewGroup;I)V

    .line 121
    return-void
.end method

.method public setIsRead(Z)V
    .locals 1
    .param p1, "isRead"    # Z

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->isRead:Z

    if-eq v0, p1, :cond_0

    .line 63
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->isRead:Z

    .line 64
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->refreshDrawableState()V

    .line 65
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->dispatchSetIsRead(Landroid/view/ViewGroup;Z)V

    .line 67
    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 171
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->setVisibility(I)V

    .line 173
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
