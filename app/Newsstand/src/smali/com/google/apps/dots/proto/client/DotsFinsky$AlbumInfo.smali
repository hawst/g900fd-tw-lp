.class public final Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsFinsky.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsFinsky;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlbumInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;


# instance fields
.field private bitField0_:I

.field private displayArtistName_:Ljava/lang/String;

.field private name_:Ljava/lang/String;

.field public track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 132
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->name_:Ljava/lang/String;

    .line 154
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->displayArtistName_:Ljava/lang/String;

    .line 176
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    .line 127
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;
    .locals 5

    .prologue
    .line 191
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 196
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    .line 197
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 198
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 199
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    move-result-object v4

    aput-object v4, v3, v2

    .line 197
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 192
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 193
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 203
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 208
    if-ne p1, p0, :cond_1

    .line 213
    :cond_0
    :goto_0
    return v1

    .line 209
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 210
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    .line 211
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->name_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->name_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->displayArtistName_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->displayArtistName_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 212
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    .line 213
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 211
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->name_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->name_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->displayArtistName_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->displayArtistName_:Ljava/lang/String;

    .line 212
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 249
    const/4 v1, 0x0

    .line 250
    .local v1, "size":I
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 251
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->name_:Ljava/lang/String;

    .line 252
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 254
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 255
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->displayArtistName_:Ljava/lang/String;

    .line 256
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 258
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    if-eqz v2, :cond_3

    .line 259
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 260
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;
    if-eqz v0, :cond_2

    .line 261
    const/4 v5, 0x3

    .line 262
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 259
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 266
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;
    :cond_3
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->cachedSize:I

    .line 267
    return v1
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 217
    const/16 v1, 0x11

    .line 218
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 219
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->name_:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 220
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->displayArtistName_:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 221
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    if-nez v2, :cond_3

    mul-int/lit8 v1, v1, 0x1f

    .line 227
    :cond_0
    return v1

    .line 219
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->name_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 220
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->displayArtistName_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 223
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 224
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 224
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 275
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 276
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 280
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 281
    :sswitch_0
    return-object p0

    .line 286
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->name_:Ljava/lang/String;

    .line 287
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->bitField0_:I

    goto :goto_0

    .line 291
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->displayArtistName_:Ljava/lang/String;

    .line 292
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->bitField0_:I

    goto :goto_0

    .line 296
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 297
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    if-nez v5, :cond_2

    move v1, v4

    .line 298
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    .line 299
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    if-eqz v5, :cond_1

    .line 300
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 302
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    .line 303
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 304
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;-><init>()V

    aput-object v6, v5, v1

    .line 305
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 306
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 303
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 297
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    array-length v1, v5

    goto :goto_1

    .line 309
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;-><init>()V

    aput-object v6, v5, v1

    .line 310
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 276
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 232
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 233
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->name_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 235
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 236
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->displayArtistName_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 238
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    if-eqz v1, :cond_3

    .line 239
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$AlbumInfo;->track:[Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v0, v2, v1

    .line 240
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;
    if-eqz v0, :cond_2

    .line 241
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 239
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 245
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsFinsky$TrackInfo;
    :cond_3
    return-void
.end method
