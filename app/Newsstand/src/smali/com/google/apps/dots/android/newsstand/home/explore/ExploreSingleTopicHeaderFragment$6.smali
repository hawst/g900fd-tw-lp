.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$6;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "ExploreSingleTopicHeaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->getOnClickListenerForShelfType(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

.field final synthetic val$shelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$6;->val$shelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 232
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .line 233
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    .line 234
    .local v0, "oldState":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$6;->val$shelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    invoke-direct {v1, p2, v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V

    check-cast p1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;

    .end local p1    # "v":Landroid/view/View;
    const-class v2, Lcom/google/android/play/cardview/CardViewGroup;

    .line 235
    invoke-virtual {v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->enableReloadoTransition(Landroid/view/ViewGroup;Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .line 237
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$400(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarView()Landroid/view/View;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->reloado_actionbar:I

    .line 238
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getStringResource(I)Ljava/lang/String;

    move-result-object v3

    .line 236
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;->addSharedElement(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;

    move-result-object v1

    .line 239
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;->start()V

    .line 240
    return-void
.end method
