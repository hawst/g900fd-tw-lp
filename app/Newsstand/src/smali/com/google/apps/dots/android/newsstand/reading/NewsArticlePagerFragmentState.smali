.class public Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
.super Ljava/lang/Object;
.source "NewsArticlePagerFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

.field final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;)V
    .locals 0
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "articleState"    # Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 39
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "postId"    # Ljava/lang/String;
    .param p3, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p4, "optPostIndex"    # Ljava/lang/Integer;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    invoke-direct {v0, p2, p1, p3, p4}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "postUrl"    # Ljava/lang/String;
    .param p3, "postTitle"    # Ljava/lang/String;
    .param p4, "publisher"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 32
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    invoke-direct {v0, p2, p3, p1, p4}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    .line 34
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 44
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 45
    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    .line 47
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    .line 48
    invoke-interface {v2, v3}, Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 50
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 60
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "{%s - %s}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 74
    return-void
.end method
