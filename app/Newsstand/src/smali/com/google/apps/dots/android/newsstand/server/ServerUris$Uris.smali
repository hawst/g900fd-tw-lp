.class Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;
.super Ljava/lang/Object;
.source "ServerUris.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/server/ServerUris;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Uris"
.end annotation


# instance fields
.field public final apiUri:Landroid/net/Uri;

.field public final baseUri:Landroid/net/Uri;

.field public final gucUri:Landroid/net/Uri;

.field public final personalResourceUri:Landroid/net/Uri;

.field public final personalUri:Landroid/net/Uri;

.field public final producerUri:Landroid/net/Uri;

.field public final resourceUri:Landroid/net/Uri;

.field public final searchUri:Landroid/net/Uri;

.field public final webviewBaseUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Landroid/accounts/Account;)V
    .locals 6
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .param p3, "account"    # Landroid/accounts/Account;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getServerType()Ljava/lang/String;

    move-result-object v1

    .line 46
    .local v1, "serverType":Ljava/lang/String;
    const-string v2, "custom"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getCustomBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->baseUri:Landroid/net/Uri;

    .line 48
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getCustomGucUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->gucUri:Landroid/net/Uri;

    .line 50
    const-string v2, "prod_producer_url"

    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->getResourceString(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->producerUri:Landroid/net/Uri;

    .line 51
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->gucUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->webviewBaseUrl:Ljava/lang/String;

    .line 69
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->baseUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "api/v3"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->apiUri:Landroid/net/Uri;

    .line 70
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->gucUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "api/v3/r"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->resourceUri:Landroid/net/Uri;

    .line 71
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->apiUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "people/me"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    .line 72
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "r"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalResourceUri:Landroid/net/Uri;

    .line 73
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "search"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->searchUri:Landroid/net/Uri;

    .line 74
    return-void

    .line 55
    :cond_0
    sget v2, Lcom/google/android/apps/newsstanddev/R$bool;->force_google_accounts_to_dogfood:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "prod"

    .line 56
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p3, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v3, "@google.com"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 57
    const-string v1, "dogfood"

    .line 60
    :cond_1
    :try_start_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "_base_url"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->getResourceString(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->baseUri:Landroid/net/Uri;

    .line 61
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "_guc_url"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->getResourceString(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->gucUri:Landroid/net/Uri;

    .line 62
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "_producer_url"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->getResourceString(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->producerUri:Landroid/net/Uri;

    .line 63
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "_webview_base_url"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->getResourceString(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->webviewBaseUrl:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "unable to load urls for: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-direct {v3, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static getResourceString(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 77
    const-string v1, "string"

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 78
    .local v0, "stringId":I
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
