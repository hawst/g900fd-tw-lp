.class final Lcom/google/apps/dots/android/newsstand/reading/ReadingState$1;
.super Ljava/lang/Object;
.source "ReadingState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/reading/ReadingState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/reading/ReadingState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/reading/ReadingState;
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 74
    const-class v3, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 75
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 76
    .local v2, "postId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->fromString(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v1

    .line 77
    .local v1, "pageLocation":Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    invoke-direct {v3, v0, v2, v1}, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    return-object v3
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/ReadingState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/reading/ReadingState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 82
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/ReadingState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    move-result-object v0

    return-object v0
.end method
