.class public Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;
.super Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;
.source "CardSourceItemClickableMenuView.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/Bound;


# static fields
.field public static final APPEARANCE_DARK:I = 0x0

.field public static final APPEARANCE_LIGHT:I = 0x1

.field private static final ICONS_CLOSED:[I

.field private static final ICONS_OPEN:[I

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final appearance:I

.field private editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

.field private final extraTouchPaddingPx:I

.field private isArchived:Z

.field private isPurchased:Z

.field private final librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 62
    new-array v0, v4, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_circular_overflow_gray:I

    aput v1, v0, v2

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_circular_overflow_white_50:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->ICONS_CLOSED:[I

    .line 64
    new-array v0, v4, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_circular_overflow_dark:I

    aput v1, v0, v2

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_circular_overflow_white_100:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->ICONS_OPEN:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    const/4 v1, 0x0

    .line 85
    .local v1, "appearance":I
    sget-object v2, Lcom/google/android/apps/newsstanddev/R$styleable;->CardSourceItemClickableMenuView:[I

    .line 86
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 87
    .local v0, "a":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 88
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->CardSourceItemClickableMenuView_cardSourceItemClickableMenuViewAppearance:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 91
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 93
    :cond_0
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->appearance:I

    .line 94
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .line 95
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->ICONS_CLOSED:[I

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->appearance:I

    aget v2, v2, v3

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->setImageResource(I)V

    .line 97
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$dimen;->card_inner_content_padding:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->extraTouchPaddingPx:I

    .line 98
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->downloadMenuItemSelected(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    return-object v0
.end method

.method static synthetic access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->createShareIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->isPurchased:Z

    return v0
.end method

.method private createShareIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/share/ShareUtil;->getShareParamsForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    move-result-object v1

    .line 343
    .local v1, "params":Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    if-eqz v1, :cond_0

    .line 344
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->getShareIntentBuilder(Lcom/google/apps/dots/android/newsstand/share/ShareParams;)Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v0

    .line 347
    .local v0, "builder":Landroid/support/v4/app/ShareCompat$IntentBuilder;
    invoke-virtual {v0}, Landroid/support/v4/app/ShareCompat$IntentBuilder;->createChooserIntent()Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    .line 349
    .end local v0    # "builder":Landroid/support/v4/app/ShareCompat$IntentBuilder;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private downloadMenuItemSelected(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 6
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 142
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 143
    .local v0, "account":Landroid/accounts/Account;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v2, v3, :cond_0

    .line 145
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    .end local p1    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->isArchived:Z

    .line 144
    invoke-static {v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper;->handleMagazineKeepOnDevice(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;Z)V

    .line 182
    :goto_0
    return-void

    .line 147
    .restart local p1    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v2

    invoke-virtual {v2, v0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 149
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    .line 150
    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->unpinEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v2

    .line 151
    invoke-virtual {v2, v4}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v2

    .line 152
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    goto :goto_0

    .line 154
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 156
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasMeteredPolicy()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 158
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    .line 157
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/MeteredUtil;->showPinningMeteredContentDialog(Landroid/app/Activity;)V

    .line 161
    :cond_2
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v1

    .line 162
    .local v1, "manager":Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;
    invoke-virtual {v1, p1, v5}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->canForegroundSyncEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 164
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    .line 165
    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->pinEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v2

    .line 166
    invoke-virtual {v2, v4}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v2

    .line 167
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    .line 180
    :goto_1
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    .line 179
    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;->showIfSyncNotEnabled(Landroid/accounts/Account;Landroid/support/v4/app/FragmentActivity;)V

    goto :goto_0

    .line 168
    :cond_3
    invoke-virtual {v1, p1, v4}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->canForegroundSyncEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 172
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    .line 171
    invoke-static {v2, p1}, Lcom/google/apps/dots/android/newsstand/widget/WifiOnlyDownloadDialog;->show(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    goto :goto_0

    .line 176
    :cond_4
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v2

    invoke-virtual {v2, v0, p1, v5}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->pin(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V

    .line 177
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/toast/Toasts;->notifyUserOfDownloadLater()V

    goto :goto_1
.end method

.method private getShareIntentBuilder(Lcom/google/apps/dots/android/newsstand/share/ShareParams;)Landroid/support/v4/app/ShareCompat$IntentBuilder;
    .locals 2
    .param p1, "params"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    .prologue
    .line 353
    .line 354
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->forParams(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/share/ShareParams;)Lcom/google/apps/dots/android/newsstand/share/ShareMessage;

    move-result-object v0

    .line 355
    .local v0, "shareMessage":Lcom/google/apps/dots/android/newsstand/share/ShareMessage;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->toIntentBuilder()Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v1

    return-object v1
.end method

.method private isDownloaded(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 2
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 132
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 133
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method protected getExtraTouchPaddingPx()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->extraTouchPaddingPx:I

    return v0
.end method

.method protected onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 107
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->onClick(Landroid/view/View;)V

    .line 108
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->ICONS_OPEN:[I

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->appearance:I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->setImageResource(I)V

    .line 109
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->dismissPopupMenuIfNeeded()V

    .line 120
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->onDetachedFromWindow()V

    .line 121
    return-void
.end method

.method protected onDismiss()V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->onDismiss()V

    .line 114
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->ICONS_CLOSED:[I

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->appearance:I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->setImageResource(I)V

    .line 115
    return-void
.end method

.method protected onPreparePopupMenu(Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;)V
    .locals 6
    .param p1, "popup"    # Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;

    .prologue
    const/4 v5, 0x1

    .line 186
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 187
    .local v2, "res":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    if-nez v3, :cond_1

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v1, v3, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 191
    .local v1, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    instance-of v3, v1, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->isTranslated()Z

    move-result v3

    if-nez v3, :cond_2

    .line 194
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->isDownloaded(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 195
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->remove_content:I

    .line 199
    .local v0, "downloadMenuText":I
    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$1;

    invoke-direct {v4, p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {p1, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 207
    .end local v0    # "downloadMenuText":I
    :cond_2
    instance-of v3, v1, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    if-eqz v3, :cond_8

    .line 208
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->isTranslated()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 209
    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->undo_translation:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$2;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)V

    invoke-virtual {p1, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 266
    :cond_3
    :goto_2
    instance-of v3, v1, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    if-nez v3, :cond_4

    instance-of v3, v1, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    if-eqz v3, :cond_0

    .line 269
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->getPrecedingAppFamilyIdOfSameType(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 270
    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->move_to_top:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$6;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$6;-><init>(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)V

    invoke-virtual {p1, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 281
    :cond_5
    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->share:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$7;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$7;-><init>(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)V

    invoke-virtual {p1, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 299
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->isPurchased:Z

    if-eqz v3, :cond_a

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->unsubscribe:I

    :goto_3
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$8;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$8;-><init>(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)V

    invoke-virtual {p1, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    goto :goto_0

    .line 197
    :cond_6
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->download_content:I

    .restart local v0    # "downloadMenuText":I
    goto :goto_1

    .line 222
    .end local v0    # "downloadMenuText":I
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->supportsTranslation()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 223
    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->translate:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$3;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)V

    invoke-virtual {p1, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    goto :goto_2

    .line 233
    :cond_8
    instance-of v3, v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    if-eqz v3, :cond_3

    .line 234
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->isArchived:Z

    if-eqz v3, :cond_9

    .line 235
    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->unarchive_issue:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$4;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)V

    invoke-virtual {p1, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    goto :goto_2

    .line 250
    :cond_9
    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->archive_issue:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$5;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$5;-><init>(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)V

    invoke-virtual {p1, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    goto :goto_2

    .line 300
    :cond_a
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->getRemoveStringResId(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I

    move-result v3

    goto :goto_3
.end method

.method public onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->dismissPopupMenuIfNeeded()V

    .line 128
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/ClickableMenuView;->onStartTemporaryDetach()V

    .line 129
    return-void
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 8
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v7, 0x0

    .line 320
    if-eqz p1, :cond_2

    .line 321
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 322
    .local v2, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_SUMMARY:I

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 323
    .local v1, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    if-nez v1, :cond_0

    .line 324
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_SUMMARY:I

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 326
    .restart local v1    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    :cond_0
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_FAMILY_SUMMARY:I

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 327
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    invoke-static {v2, v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->editionSummary(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 328
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_PURCHASED:I

    invoke-virtual {p1, v3, v7}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->isPurchased:Z

    .line 330
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_ARCHIVED:I

    invoke-virtual {p1, v3, v7}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->isArchived:Z

    .line 331
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->isArchived:Z

    if-eqz v3, :cond_1

    .line 332
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Archived edition found: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 339
    .end local v0    # "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .end local v1    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .end local v2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_1
    :goto_0
    return-void

    .line 336
    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 337
    iput-boolean v7, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->isPurchased:Z

    goto :goto_0
.end method
