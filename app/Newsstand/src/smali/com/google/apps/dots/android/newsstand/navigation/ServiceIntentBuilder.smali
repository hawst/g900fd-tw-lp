.class public abstract Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/IntentBuilder;
.source "ServiceIntentBuilder.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final context:Landroid/content/Context;

.field private wakefulLockTimeoutMs:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/IntentBuilder;-><init>(Landroid/content/Context;)V

    .line 16
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;->wakefulLockTimeoutMs:I

    .line 20
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;->context:Landroid/content/Context;

    .line 21
    return-void
.end method


# virtual methods
.method public abstract build()Landroid/content/Intent;
.end method

.method public start()V
    .locals 6

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    .line 26
    .local v0, "intent":Landroid/content/Intent;
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;->wakefulLockTimeoutMs:I

    if-lez v1, :cond_0

    .line 27
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;->context:Landroid/content/Context;

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;->wakefulLockTimeoutMs:I

    invoke-static {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->makeWakeful(Landroid/content/Context;Landroid/content/Intent;I)V

    .line 29
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "%s - starting intent: %s, extras: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v4, 0x2

    .line 30
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    aput-object v5, v3, v4

    .line 29
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 32
    return-void
.end method

.method public wakeful(I)Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;
    .locals 0
    .param p1, "lockTimeoutMs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 40
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;->wakefulLockTimeoutMs:I

    .line 41
    return-object p0
.end method
