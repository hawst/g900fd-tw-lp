.class public Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;
.super Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
.source "MagazineEdition.java"


# static fields
.field public static final DK_PRIMARY_KEY:I

.field public static final EQUALITY_FIELDS:[I

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const-class v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 44
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MagazineEditionCardList_primaryKey:I

    sput v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->DK_PRIMARY_KEY:I

    .line 48
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_VERSION:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_IS_READ:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_VERSION:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_VERSION:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->EQUALITY_FIELDS:[I

    return-void
.end method

.method constructor <init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V
    .locals 0
    .param p1, "editionMessage"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 57
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 60
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    const/4 v1, 0x5

    .line 61
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setType(I)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;-><init>()V

    .line 62
    invoke-virtual {v1, p1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setMagazine(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    .line 60
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 63
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    return-void
.end method

.method public static getMostRecentPostId(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/common/base/Predicate;)Ljava/lang/String;
    .locals 4
    .param p0, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "appId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Predicate",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p2, "postIdPredicate":Lcom/google/common/base/Predicate;, "Lcom/google/common/base/Predicate<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 99
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 100
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    .line 101
    invoke-static {v3, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->readStatesUri(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 100
    invoke-static {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->getCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    move-result-object v1

    .line 102
    .local v1, "readStateCollection":Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    invoke-virtual {v1, p2}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->getMostRecentPostReadState(Lcom/google/common/base/Predicate;)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v0

    .line 103
    .local v0, "mostRecent":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getPostId()Ljava/lang/String;

    move-result-object v2

    :cond_0
    return-object v2
.end method

.method public static getMostRecentPostIdFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/common/base/Predicate;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p0, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "appId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Predicate",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    .local p2, "postIdPredicate":Lcom/google/common/base/Predicate;, "Lcom/google/common/base/Predicate<Ljava/lang/String;>;"
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$1;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/common/base/Predicate;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->submit(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static getPageFractionForPost(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)F
    .locals 4
    .param p0, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 120
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 121
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 122
    new-instance v2, Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findAncestorOfType(I)Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->getId()Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "appId":Ljava/lang/String;
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    .line 124
    invoke-static {v3, v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->readStatesUri(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 123
    invoke-static {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->getCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    move-result-object v2

    .line 124
    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->getReadState(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v1

    .line 125
    .local v1, "readState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->hasPageFraction()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 126
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getPageFraction()F

    move-result v2

    .line 128
    :goto_1
    return v2

    .end local v0    # "appId":Ljava/lang/String;
    .end local v1    # "readState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    :cond_0
    move v2, v3

    .line 121
    goto :goto_0

    .line 128
    .restart local v0    # "appId":Ljava/lang/String;
    .restart local v1    # "readState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static getPageFractionFutureForPost(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p0, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "postId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$2;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$2;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->submit(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static readStatesUri(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 94
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getAppReadStatesUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static setPageFractionForPost(Landroid/accounts/Account;Ljava/lang/String;F)V
    .locals 9
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "pageFraction"    # F

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 146
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v7

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 148
    :try_start_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findAncestorOfType(I)Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->getId()Ljava/lang/String;

    move-result-object v3

    .line 151
    .local v3, "appId":Ljava/lang/String;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    move-object v2, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;F)V

    .line 156
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;->execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .end local v3    # "appId":Ljava/lang/String;
    :goto_1
    return-void

    :cond_0
    move v0, v8

    .line 146
    goto :goto_0

    .line 157
    :catch_0
    move-exception v6

    .line 160
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Illegal PostId when trying to set pageFraction for Post: %s."

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v8

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public actionBarColor(Landroid/content/Context;Z)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "subscribed"    # Z

    .prologue
    .line 181
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public cardList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 186
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->cardList(Landroid/content/Context;Z)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public cardList(Landroid/content/Context;Z)Lcom/google/android/libraries/bind/data/DataList;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "liteMode"    # Z

    .prologue
    .line 192
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->rawCardList(Landroid/content/Context;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 193
    .local v0, "rawCardList":Lcom/google/android/libraries/bind/data/DataList;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->EQUALITY_FIELDS:[I

    sget v2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->DK_PRIMARY_KEY:I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;

    invoke-direct {v3, p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;Lcom/google/android/libraries/bind/data/DataList;Z)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/data/DataList;->filter([IILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    return-object v1
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getMagazine()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;->getAppId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readStatesUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->readStatesUri(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 68
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMagazineCollection(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public showKeepOnDeviceUi()Z
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x1

    return v0
.end method

.method public showOnDeviceOnlyUi()Z
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method public supportsReadStates()Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    return v0
.end method

.method public supportsSubscription()Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    .line 74
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->readStatesUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method public trackAnalytics(I)V
    .locals 1
    .param p1, "page"    # I

    .prologue
    .line 199
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->trackAnalytics(IZ)V

    .line 200
    return-void
.end method

.method public trackAnalytics(IZ)V
    .locals 2
    .param p1, "page"    # I
    .param p2, "inLiteMode"    # Z

    .prologue
    .line 203
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;IZ)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->track(Z)V

    .line 204
    return-void
.end method
