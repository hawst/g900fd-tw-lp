.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InlineFrame"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;


# instance fields
.field private attachmentId_:Ljava/lang/String;

.field private bitField0_:I

.field private externalResourceUri_:Ljava/lang/String;

.field private mainResourceUri_:Ljava/lang/String;

.field private originalUri_:Ljava/lang/String;

.field public resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14424
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14425
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 14585
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->attachmentId_:Ljava/lang/String;

    .line 14607
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->originalUri_:Ljava/lang/String;

    .line 14629
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->externalResourceUri_:Ljava/lang/String;

    .line 14651
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    .line 14673
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    .line 14425
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    .locals 5

    .prologue
    .line 14690
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 14694
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 14695
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    .line 14696
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 14697
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 14698
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    move-result-object v4

    aput-object v4, v3, v2

    .line 14696
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 14691
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 14692
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 14702
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 14421
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 14707
    if-ne p1, p0, :cond_1

    .line 14714
    :cond_0
    :goto_0
    return v1

    .line 14708
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 14709
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    .line 14710
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->attachmentId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->attachmentId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->originalUri_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->originalUri_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 14711
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->externalResourceUri_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->externalResourceUri_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 14712
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 14713
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    .line 14714
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 14710
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->attachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->attachmentId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->originalUri_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->originalUri_:Ljava/lang/String;

    .line 14711
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->externalResourceUri_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->externalResourceUri_:Ljava/lang/String;

    .line 14712
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    .line 14713
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4
.end method

.method public getExternalResourceUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14631
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->externalResourceUri_:Ljava/lang/String;

    return-object v0
.end method

.method public getMainResourceUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14653
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 14758
    const/4 v1, 0x0

    .line 14759
    .local v1, "size":I
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 14760
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->attachmentId_:Ljava/lang/String;

    .line 14761
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 14763
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 14764
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->originalUri_:Ljava/lang/String;

    .line 14765
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 14767
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_2

    .line 14768
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    .line 14769
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 14771
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    if-eqz v2, :cond_4

    .line 14772
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_4

    aget-object v0, v3, v2

    .line 14773
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    if-eqz v0, :cond_3

    .line 14774
    const/4 v5, 0x4

    .line 14775
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 14772
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 14779
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_5

    .line 14780
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->externalResourceUri_:Ljava/lang/String;

    .line 14781
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 14783
    :cond_5
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->cachedSize:I

    .line 14784
    return v1
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 14718
    const/16 v1, 0x11

    .line 14719
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 14720
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->attachmentId_:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 14721
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->originalUri_:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 14722
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->externalResourceUri_:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 14723
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 14724
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    if-nez v2, :cond_5

    mul-int/lit8 v1, v1, 0x1f

    .line 14730
    :cond_0
    return v1

    .line 14720
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->attachmentId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 14721
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->originalUri_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 14722
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->externalResourceUri_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 14723
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    .line 14726
    :cond_5
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 14727
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    aget-object v2, v2, v0

    if-nez v2, :cond_6

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 14726
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 14727
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->hashCode()I

    move-result v2

    goto :goto_5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 14792
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 14793
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 14797
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 14798
    :sswitch_0
    return-object p0

    .line 14803
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->attachmentId_:Ljava/lang/String;

    .line 14804
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    goto :goto_0

    .line 14808
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->originalUri_:Ljava/lang/String;

    .line 14809
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    goto :goto_0

    .line 14813
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    .line 14814
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    goto :goto_0

    .line 14818
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 14819
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    if-nez v5, :cond_2

    move v1, v4

    .line 14820
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    .line 14821
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    if-eqz v5, :cond_1

    .line 14822
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 14824
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    .line 14825
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 14826
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;-><init>()V

    aput-object v6, v5, v1

    .line 14827
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 14828
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 14825
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 14819
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    array-length v1, v5

    goto :goto_1

    .line 14831
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;-><init>()V

    aput-object v6, v5, v1

    .line 14832
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 14836
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->externalResourceUri_:Ljava/lang/String;

    .line 14837
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    goto/16 :goto_0

    .line 14793
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14421
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    move-result-object v0

    return-object v0
.end method

.method public setMainResourceUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 14656
    if-nez p1, :cond_0

    .line 14657
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14659
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    .line 14660
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    .line 14661
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14735
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14736
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->attachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 14738
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14739
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->originalUri_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 14741
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 14742
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->mainResourceUri_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 14744
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    if-eqz v1, :cond_4

    .line 14745
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v0, v2, v1

    .line 14746
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    if-eqz v0, :cond_3

    .line 14747
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 14745
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 14751
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 14752
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->externalResourceUri_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 14754
    :cond_5
    return-void
.end method
