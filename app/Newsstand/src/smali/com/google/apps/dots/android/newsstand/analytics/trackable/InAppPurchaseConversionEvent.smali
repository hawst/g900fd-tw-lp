.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;
.source "InAppPurchaseConversionEvent.java"


# instance fields
.field private final customDimensions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final customMetrics:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final fullDocId:Ljava/lang/String;

.field private final isHouseAd:Z

.field private final page:I

.field private final postId:Ljava/lang/String;

.field private final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;ILjava/lang/String;ZLjava/util/Map;Ljava/util/Map;)V
    .locals 0
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "postId"    # Ljava/lang/String;
    .param p4, "page"    # I
    .param p5, "fullDocId"    # Ljava/lang/String;
    .param p6, "isHouseAd"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p7, "customDimensions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p8, "customMetrics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 41
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 42
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->postId:Ljava/lang/String;

    .line 43
    iput p4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->page:I

    .line 44
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->fullDocId:Ljava/lang/String;

    .line 45
    iput-boolean p6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->isHouseAd:Z

    .line 46
    iput-object p7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->customDimensions:Ljava/util/Map;

    .line 47
    iput-object p8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->customMetrics:Ljava/util/Map;

    .line 48
    return-void
.end method

.method private getRelatedArticlesScreen(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "postId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 127
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    invoke-virtual {v1, v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 126
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 128
    .local v0, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    if-nez v0, :cond_1

    .line 129
    new-instance v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v3, "Could not find post for postId = "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {v2, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_1
    const-string v1, "%s %s - %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "[Related]"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 132
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 131
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 13
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->postId:Ljava/lang/String;

    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->getPostSummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    .line 69
    .local v4, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v3

    .line 71
    .local v3, "originalEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-boolean v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->isHouseAd:Z

    if-eqz v7, :cond_0

    const-string v0, "House Ad Conversion"

    .line 74
    .local v0, "action":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v7

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->fullDocId:Ljava/lang/String;

    .line 75
    invoke-virtual {v7, v8}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setLabel(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v7

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->postId:Ljava/lang/String;

    .line 76
    invoke-virtual {v7, v8}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v7

    .line 77
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v7

    iget-object v8, v4, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    .line 78
    invoke-virtual {v7, v8}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v7

    iget-object v8, v4, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    .line 79
    invoke-virtual {v7, v8}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v7

    .line 80
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v7

    iget-object v8, v3, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v8, v8, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 81
    invoke-virtual {v7, v8}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v7

    .line 82
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppFamilyName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v7

    iget v8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->page:I

    .line 83
    invoke-virtual {v7, v8}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPage(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v7

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 84
    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setUserSubscriptionType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v7

    iget-object v8, v3, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 85
    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 90
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v7, v7, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    if-eqz v7, :cond_1

    .line 91
    const-string v5, "ReadNow"

    .line 108
    .local v5, "readFromString":Ljava/lang/String;
    :goto_1
    const-string v7, "ReadFrom"

    invoke-static {p1, v7, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v7, "TranslatedLanguage"

    .line 110
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->getTranslatedLanguage()Ljava/lang/String;

    move-result-object v8

    .line 109
    invoke-static {p1, v7, v8}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->customDimensions:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 113
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {p1, v7, v8}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 71
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "readFromString":Ljava/lang/String;
    :cond_0
    const-string v0, "In-App Purchase"

    goto/16 :goto_0

    .line 92
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v7, v7, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    if-eqz v7, :cond_3

    .line 93
    const-string v7, "[Topic] - "

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v7, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .line 94
    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v8, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "readFromString":Ljava/lang/String;
    :goto_3
    goto :goto_1

    .end local v5    # "readFromString":Ljava/lang/String;
    :cond_2
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 95
    :cond_3
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v7, v7, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v7, :cond_4

    .line 96
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v7, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->getSection(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Section;

    move-result-object v6

    .line 97
    .local v6, "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    const-string v7, "[Section] "

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 98
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x3

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 99
    .restart local v5    # "readFromString":Ljava/lang/String;
    goto/16 :goto_1

    .end local v5    # "readFromString":Ljava/lang/String;
    .end local v6    # "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    :cond_4
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v7, v7, Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    if-eqz v7, :cond_5

    .line 100
    const-string v5, "Bookmarks"

    .restart local v5    # "readFromString":Ljava/lang/String;
    goto/16 :goto_1

    .line 101
    .end local v5    # "readFromString":Ljava/lang/String;
    :cond_5
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v7, v7, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    if-eqz v7, :cond_6

    .line 102
    const-string v5, "Search"

    .restart local v5    # "readFromString":Ljava/lang/String;
    goto/16 :goto_1

    .line 103
    .end local v5    # "readFromString":Ljava/lang/String;
    :cond_6
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v7, v7, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    if-eqz v7, :cond_7

    .line 104
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->postId:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->getRelatedArticlesScreen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "readFromString":Ljava/lang/String;
    goto/16 :goto_1

    .line 106
    .end local v5    # "readFromString":Ljava/lang/String;
    :cond_7
    const/4 v5, 0x0

    .restart local v5    # "readFromString":Ljava/lang/String;
    goto/16 :goto_1

    .line 116
    :cond_8
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->customMetrics:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_9
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 117
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Float;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 118
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Float;

    invoke-static {p1, v7, v8}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->appendMetric(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/Float;)V

    goto :goto_4

    .line 122
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Float;>;"
    :cond_a
    return-object p1
.end method

.method protected fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "publisherEvent"    # Z

    .prologue
    .line 53
    const-string v0, "Monetization"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setCategory(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    return-object v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->postId:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->getPostSummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    .line 60
    .local v0, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    const-string v1, "[Article] "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
