.class public Lcom/google/apps/dots/android/newsstand/toast/UndoSubscribeOperation;
.super Lcom/google/apps/dots/android/newsstand/toast/UndoToastBarOperation;
.source "UndoSubscribeOperation.java"


# instance fields
.field private editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

.field private undoAppFamilyPivotId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V
    .locals 2
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p4, "reorderPivotAppId"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->undo:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/toast/UndoToastBarOperation;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 34
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/toast/UndoSubscribeOperation;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 35
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/toast/UndoSubscribeOperation;->undoAppFamilyPivotId:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public onActionClicked(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/toast/UndoSubscribeOperation;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/toast/UndoSubscribeOperation;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/toast/UndoSubscribeOperation;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-static {v2, v3, v4, v5, v5}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->addSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V

    .line 48
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/toast/UndoSubscribeOperation;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v1, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 51
    .local v1, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    if-eqz v1, :cond_0

    .line 52
    iget-object v0, v1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 53
    .local v0, "appFamilyId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/toast/UndoSubscribeOperation;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/toast/UndoSubscribeOperation;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/toast/UndoSubscribeOperation;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 54
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/toast/UndoSubscribeOperation;->undoAppFamilyPivotId:Ljava/lang/String;

    .line 53
    invoke-static {v2, v3, v4, v0, v5}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->reorderSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .end local v0    # "appFamilyId":Ljava/lang/String;
    :cond_0
    return-void
.end method
