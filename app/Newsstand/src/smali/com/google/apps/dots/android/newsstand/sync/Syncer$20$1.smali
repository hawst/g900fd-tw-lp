.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$20$1;
.super Ljava/lang/Object;
.source "Syncer.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureFallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->callInternal()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureFallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;

.field final synthetic val$isDisallowedNewsImage:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;Z)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;

    .prologue
    .line 1059
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;

    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20$1;->val$isDisallowedNewsImage:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1062
    move-object v0, p1

    .line 1064
    .local v0, "replacedException":Ljava/lang/Throwable;
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/store/NSStore$NotAvailableException;

    if-eqz v1, :cond_0

    .line 1065
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20$1;->val$isDisallowedNewsImage:Z

    if-eqz v1, :cond_1

    .line 1067
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SkippedTaskException;

    .end local v0    # "replacedException":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->val$attachmentId:Ljava/lang/String;

    const-string v3, "skipping this type of News image"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SkippedTaskException;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    .restart local v0    # "replacedException":Ljava/lang/Throwable;
    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1

    .line 1071
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/SyncPolicyException;

    .end local v0    # "replacedException":Ljava/lang/Throwable;
    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/sync/SyncPolicyException;-><init>()V

    .restart local v0    # "replacedException":Ljava/lang/Throwable;
    goto :goto_0
.end method
