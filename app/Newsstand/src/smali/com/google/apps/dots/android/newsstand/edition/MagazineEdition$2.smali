.class final Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$2;
.super Ljava/lang/Object;
.source "MagazineEdition.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->getPageFractionFutureForPost(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$postId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$2;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$2;->val$postId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Float;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$2;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$2;->val$postId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->getPageFractionForPost(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$2;->call()Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method
