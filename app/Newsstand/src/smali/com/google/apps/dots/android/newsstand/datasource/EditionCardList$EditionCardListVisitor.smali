.class public Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;
.super Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
.source "EditionCardList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "EditionCardListVisitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p5, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;

    .line 107
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    .line 108
    return-void
.end method


# virtual methods
.method protected getAnalyticsScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    const-string v0, "[Edition]"

    return-object v0
.end method

.method public makeOfferCardData(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p2, "listToLocallyModify"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .line 126
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->getOffersStatusSnapshot()Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;->isOfferLocallyAcceptedOrDeclined(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeOfferCardData(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;)V

    .line 129
    :cond_0
    return-void
.end method

.method protected readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    return-object v0
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 1
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;

    invoke-virtual {p0, p2, v0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;->makeOfferCardData(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;)V

    .line 134
    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 0

    .prologue
    .line 104
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    return-void
.end method
