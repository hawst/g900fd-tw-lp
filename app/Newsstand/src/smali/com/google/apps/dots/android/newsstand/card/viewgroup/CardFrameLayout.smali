.class public Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "CardFrameLayout.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/card/ResizingCard;
.implements Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;
.implements Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;


# instance fields
.field private cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

.field private isRead:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;-><init>(Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->initialize(Lcom/google/android/play/cardview/CardViewGroup;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->initialize(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method


# virtual methods
.method public canEnlarge()Z
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->canEnlarge()Z

    move-result v0

    return v0
.end method

.method public canShrink()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->canShrink()Z

    move-result v0

    return v0
.end method

.method public getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    return-object v0
.end method

.method public isRead()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->isRead:Z

    return v0
.end method

.method protected makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->onAttachedToWindow()V

    .line 104
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onAttachedToWindow()V

    .line 105
    return-void
.end method

.method public onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->isRead()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 126
    .local v0, "drawableState":[I
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->STATE_READ:[I

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->mergeDrawableStates([I[I)[I

    .line 129
    .end local v0    # "drawableState":[I
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->onDetachedFromWindow()V

    .line 98
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onDetachedFromWindow()V

    .line 99
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->onFinishTemporaryDetach()V

    .line 86
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onFinishTemporaryDetach()V

    .line 87
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->onStartTemporaryDetach()V

    .line 92
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onStartTemporaryDetach()V

    .line 93
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundColor(Lcom/google/android/play/cardview/CardViewGroup;I)V

    .line 115
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundResource(Lcom/google/android/play/cardview/CardViewGroup;I)V

    .line 120
    return-void
.end method

.method public setIsRead(Z)V
    .locals 1
    .param p1, "isRead"    # Z

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->isRead:Z

    if-eq v0, p1, :cond_0

    .line 57
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->isRead:Z

    .line 58
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->refreshDrawableState()V

    .line 59
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->dispatchSetIsRead(Landroid/view/ViewGroup;Z)V

    .line 61
    :cond_0
    return-void
.end method
