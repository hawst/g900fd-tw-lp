.class final Lcom/google/apps/dots/android/newsstand/toast/Toasts$1;
.super Ljava/lang/Object;
.source "Toasts.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/toast/Toasts;->notifyUserOfRequiredUpgrade()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 23
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    .line 24
    .local v0, "appContext":Landroid/content/Context;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->upgrade_required_short_message:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 28
    :cond_0
    return-void
.end method
