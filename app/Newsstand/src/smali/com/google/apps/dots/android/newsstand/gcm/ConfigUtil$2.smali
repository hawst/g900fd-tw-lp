.class Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;
.super Ljava/lang/Object;
.source "ConfigUtil.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->requestConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
        "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$clientRequest:Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

.field final synthetic val$gcmRegisteredForUser:Z

.field final synthetic val$postGcmReg:Z

.field final synthetic val$reportOnboardQuizCompleted:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;Landroid/accounts/Account;ZZZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$clientRequest:Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$account:Landroid/accounts/Account;

    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$postGcmReg:Z

    iput-boolean p5, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$gcmRegisteredForUser:Z

    iput-boolean p6, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$reportOnboardQuizCompleted:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .param p1, "input"    # Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 253
    # getter for: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$300()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v6, "Registered %s"

    new-array v7, v5, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$clientRequest:Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    iget-object v8, v8, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;->uri:Ljava/lang/String;

    aput-object v8, v7, v4

    invoke-virtual {v3, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    # getter for: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$000(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$account:Landroid/accounts/Account;

    const-string v8, "gcmRegisteredForUser2"

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$postGcmReg:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$gcmRegisteredForUser:Z

    if-eqz v3, :cond_2

    :cond_0
    move v3, v5

    :goto_0
    invoke-virtual {v6, v7, v8, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 256
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$reportOnboardQuizCompleted:Z

    if-eqz v3, :cond_1

    .line 258
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    # getter for: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$000(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$account:Landroid/accounts/Account;

    invoke-virtual {v3, v6, v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setReportShowedOnboardQuiz(Landroid/accounts/Account;Z)V

    .line 260
    :cond_1
    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->data:Ljava/io/InputStream;

    .line 262
    .local v2, "in":Ljava/io/InputStream;
    const/4 v3, 0x1

    :try_start_0
    new-array v0, v3, [Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .line 263
    .local v0, "clientConfig":[Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    new-instance v3, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;-><init>()V

    const/16 v6, 0x400

    invoke-static {v3, v2, v6}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->readFromStream(Lcom/google/protobuf/nano/MessageNano;Ljava/io/InputStream;I)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .line 264
    .local v1, "clientRoot":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    invoke-direct {v3, v1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v6, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2$1;

    invoke-direct {v6, p0, v0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2$1;-><init>(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;[Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V

    invoke-virtual {v3, v6}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 274
    const/4 v3, 0x0

    aget-object v3, v0, v3

    if-eqz v3, :cond_3

    :goto_1
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 275
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->val$account:Landroid/accounts/Account;

    const/4 v5, 0x0

    aget-object v5, v0, v5

    # invokes: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->onNewConfig(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
    invoke-static {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$400(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V

    .line 276
    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 278
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    return-object v3

    .end local v0    # "clientConfig":[Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .end local v1    # "clientRoot":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .end local v2    # "in":Ljava/io/InputStream;
    :cond_2
    move v3, v4

    .line 254
    goto :goto_0

    .restart local v0    # "clientConfig":[Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .restart local v1    # "clientRoot":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .restart local v2    # "in":Ljava/io/InputStream;
    :cond_3
    move v5, v4

    .line 274
    goto :goto_1

    .line 278
    .end local v0    # "clientConfig":[Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .end local v1    # "clientRoot":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v3
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 250
    check-cast p1, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;->apply(Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
