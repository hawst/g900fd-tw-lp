.class Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$1;
.super Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.source "CollectionCardList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;-><init>(Landroid/content/Context;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;
    .param p2, "x0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$1;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 50
    sget v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_VIEW_TYPE:I

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_ARTICLE_ARRAY:I

    .line 51
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 7
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 57
    .local v1, "returnDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 58
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_ARTICLE_ARRAY:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 59
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_ARTICLE_ARRAY:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/android/libraries/bind/data/Data;

    check-cast v3, [Lcom/google/android/libraries/bind/data/Data;

    array-length v6, v3

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v6, :cond_0

    aget-object v2, v3, v4

    .line 60
    .local v2, "topicData":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 63
    .end local v2    # "topicData":Lcom/google/android/libraries/bind/data/Data;
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    :cond_2
    return-object v1
.end method
