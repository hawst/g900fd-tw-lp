.class public final enum Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;
.super Ljava/lang/Enum;
.source "SizingLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DimensionSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

.field public static final enum HEIGHT:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

.field public static final enum WIDTH:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    const-string v1, "WIDTH"

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;->WIDTH:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    .line 19
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    const-string v1, "HEIGHT"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;->HEIGHT:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    .line 17
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;->WIDTH:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;->HEIGHT:Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;->$VALUES:[Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;->$VALUES:[Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/widget/SizingLayout$DimensionSource;

    return-object v0
.end method
