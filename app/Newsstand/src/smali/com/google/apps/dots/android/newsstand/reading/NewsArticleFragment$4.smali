.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$4;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "NewsArticleFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setupMenus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 297
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$4;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 300
    .local p1, "results":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    .line 301
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v1

    .line 302
    .local v1, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    const/4 v3, 0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v0, v3, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 304
    .local v0, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUtil;->getShareParamsForPost(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    move-result-object v2

    .line 305
    .local v2, "shareParams":Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$300(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->setShareParams(Lcom/google/apps/dots/android/newsstand/share/ShareParams;)V

    .line 307
    .end local v0    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .end local v1    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .end local v2    # "shareParams":Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    :cond_0
    return-void
.end method
