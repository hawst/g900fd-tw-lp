.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "NewsArticleFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

.field final synthetic val$menu:Landroid/view/Menu;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;Landroid/view/Menu;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 479
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;->val$menu:Landroid/view/Menu;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 4
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 482
    if-eqz p1, :cond_0

    .line 483
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    .line 484
    .local v0, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$600(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;->val$menu:Landroid/view/Menu;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2, v3, v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    .line 485
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;->val$menu:Landroid/view/Menu;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->menu_see_original_article:I

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 486
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasExternalPostUrl()Z

    move-result v2

    .line 485
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 490
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$300(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;->val$menu:Landroid/view/Menu;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2, v3, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 492
    .end local v0    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 479
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
