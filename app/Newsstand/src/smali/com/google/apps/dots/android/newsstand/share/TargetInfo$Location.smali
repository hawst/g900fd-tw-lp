.class public final enum Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;
.super Ljava/lang/Enum;
.source "TargetInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/share/TargetInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Location"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public static final enum LOCATION_DEFAULT:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public static final enum LOCATION_HOME:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public static final enum LOCATION_MAGAZINES_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public static final enum LOCATION_MAGAZINES_HOME_ISSUES:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public static final enum LOCATION_MAGAZINES_HOME_TITLES:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public static final enum LOCATION_NEWSSTAND_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public static final enum LOCATION_NEWSSTAND_POST:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public static final enum LOCATION_NEWS_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public static final enum LOCATION_OFFERS:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public static final enum LOCATION_UNRESOLVED_REDIRECT:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public static final enum LOCATION_UNSPECIFIED:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const-string v1, "LOCATION_UNSPECIFIED"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_UNSPECIFIED:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 14
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const-string v1, "LOCATION_DEFAULT"

    invoke-direct {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_DEFAULT:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 15
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const-string v1, "LOCATION_MAGAZINES_EDITION"

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 16
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const-string v1, "LOCATION_OFFERS"

    invoke-direct {v0, v1, v6}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_OFFERS:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 17
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const-string v1, "LOCATION_HOME"

    invoke-direct {v0, v1, v7}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_HOME:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 18
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const-string v1, "LOCATION_MAGAZINES_HOME_TITLES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_HOME_TITLES:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 19
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const-string v1, "LOCATION_MAGAZINES_HOME_ISSUES"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_HOME_ISSUES:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 20
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const-string v1, "LOCATION_NEWS_EDITION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWS_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 22
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const-string v1, "LOCATION_NEWSSTAND_EDITION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWSSTAND_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const-string v1, "LOCATION_NEWSSTAND_POST"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWSSTAND_POST:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 25
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const-string v1, "LOCATION_UNRESOLVED_REDIRECT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_UNRESOLVED_REDIRECT:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 12
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_UNSPECIFIED:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_DEFAULT:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_OFFERS:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_HOME:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_HOME_TITLES:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_HOME_ISSUES:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWS_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWSSTAND_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWSSTAND_POST:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_UNRESOLVED_REDIRECT:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->$VALUES:[Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->$VALUES:[Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    return-object v0
.end method
