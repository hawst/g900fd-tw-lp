.class public Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
.super Lcom/google/android/libraries/bind/card/CardListBuilder;
.source "NSCardListBuilder.java"


# instance fields
.field private headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

.field private shelfHeaderAdded:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/card/CardListBuilder;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method private getShelfHeaderLayout()I
    .locals 2

    .prologue
    .line 105
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->shelfHeaderAdded:Z

    if-eqz v1, :cond_0

    sget v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->LAYOUT:I

    .line 106
    .local v0, "layout":I
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->shelfHeaderAdded:Z

    .line 107
    return v0

    .line 105
    .end local v0    # "layout":I
    :cond_0
    sget v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->LAYOUT_FIRST:I

    goto :goto_0
.end method


# virtual methods
.method protected makePaddingRow(Z)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "top"    # Z

    .prologue
    .line 32
    invoke-static {}, Lcom/google/android/libraries/bind/card/CardListPadding;->make()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 33
    .local v0, "paddingRow":Lcom/google/android/libraries/bind/data/Data;
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    if-eqz v1, :cond_0

    .line 34
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->appContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;->makeHeaderCard(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 38
    :goto_0
    return-object v0

    .line 36
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/card/CardListBuilder;->makePaddingRow(Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    goto :goto_0
.end method

.method public makeShelfHeader(Ljava/lang/String;)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeShelfHeader(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public makeShelfHeader(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "color"    # Ljava/lang/Integer;

    .prologue
    .line 69
    if-nez p2, :cond_0

    .line 70
    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->shelf_header_title_color:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    .line 73
    :cond_0
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 75
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->getShelfHeaderLayout()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 78
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 79
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE_TEXT_COLOR:I

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 80
    return-object v0
.end method

.method public makeShelfHeaderWithButton(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "buttonText"    # Ljava/lang/String;
    .param p3, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 92
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 94
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->getShelfHeaderLayout()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 97
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 98
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE_TEXT_COLOR:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->shelf_header_title_color:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 99
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_BUTTON_TEXT:I

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 100
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_ON_CLICK_LISTENER:I

    invoke-virtual {v0, v1, p3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 101
    return-object v0
.end method

.method public removeAll()Lcom/google/android/libraries/bind/card/CardListBuilder;
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->shelfHeaderAdded:Z

    .line 113
    invoke-super {p0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->removeAll()Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v0

    return-object v0
.end method

.method public useSpacerTopPadding(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    .locals 0
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 27
    return-object p0
.end method
