.class Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;
.super Ljava/lang/Object;
.source "AttachmentStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->decodeWithMutableBitmap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Landroid/graphics/Bitmap;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

.field final synthetic val$finalDiskBlob:Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

.field final synthetic val$imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

.field final synthetic val$options:Landroid/graphics/BitmapFactory$Options;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;Landroid/graphics/BitmapFactory$Options;Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$options:Landroid/graphics/BitmapFactory$Options;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$finalDiskBlob:Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .param p1, "mutable"    # Landroid/graphics/Bitmap;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v10, 0x4000

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 212
    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Got mutable bitmap for %s"

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213
    const/4 v2, 0x0

    .line 214
    .local v2, "result":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$options:Landroid/graphics/BitmapFactory$Options;

    iput-object p1, v3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 215
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$options:Landroid/graphics/BitmapFactory$Options;

    iput v9, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 216
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$options:Landroid/graphics/BitmapFactory$Options;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->acquire(I)[B

    move-result-object v4

    iput-object v4, v3, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 217
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->acquire(I)[B

    move-result-object v1

    .line 219
    .local v1, "inputStreamBuffer":[B
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$finalDiskBlob:Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    .line 220
    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeDiskBlob()Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    move-result-object v0

    .line 223
    .local v0, "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$options:Landroid/graphics/BitmapFactory$Options;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->retryBitmapDecodeWithGC(Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    invoke-static {v3, v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$400(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 224
    if-eq v2, p1, :cond_0

    .line 225
    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Did not use mutable bitmap."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 226
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->releaseBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 230
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$options:Landroid/graphics/BitmapFactory$Options;

    iget-object v4, v4, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 231
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$options:Landroid/graphics/BitmapFactory$Options;

    iput-object v7, v3, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 232
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$options:Landroid/graphics/BitmapFactory$Options;

    iput-object v7, v3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 233
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->close()V

    .line 236
    if-nez v2, :cond_2

    .line 237
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Couldn\'t decode bitmap"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3

    .line 220
    .end local v0    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$finalDiskBlob:Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    goto :goto_0

    .line 229
    .restart local v0    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 230
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$options:Landroid/graphics/BitmapFactory$Options;

    iget-object v5, v5, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 231
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$options:Landroid/graphics/BitmapFactory$Options;

    iput-object v7, v4, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 232
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$options:Landroid/graphics/BitmapFactory$Options;

    iput-object v7, v4, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 233
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->close()V

    throw v3

    .line 240
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->isDestroyed()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 241
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 242
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateCancelledFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 245
    :goto_1
    return-object v3

    :cond_3
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 208
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$4;->apply(Landroid/graphics/Bitmap;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
