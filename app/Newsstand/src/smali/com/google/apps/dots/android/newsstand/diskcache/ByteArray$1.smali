.class final Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray$1;
.super Ljava/lang/Object;
.source "ByteArray.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
        ">;"
    }
.end annotation


# instance fields
.field final bytesLex:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-static {}, Lcom/google/common/primitives/UnsignedBytes;->lexicographicalComparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray$1;->bytesLex:Ljava/util/Comparator;

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)I
    .locals 3
    .param p1, "lhs"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "rhs"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray$1;->bytesLex:Ljava/util/Comparator;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes:[B
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->access$000(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)[B

    move-result-object v1

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes:[B
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->access$000(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)[B

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray$1;->compare(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)I

    move-result v0

    return v0
.end method
