.class final Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState$1;
.super Ljava/lang/Object;
.source "ExploreSingleTopicFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 55
    const-class v3, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 56
    .local v2, "topic":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 57
    .local v1, "expandedShelfTypeInt":I
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    const/4 v0, 0x0

    .line 59
    .local v0, "expandedShelfType":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;
    :goto_0
    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    invoke-direct {v3, v2, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V

    return-object v3

    .line 58
    .end local v0    # "expandedShelfType":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->values()[Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    move-result-object v3

    aget-object v0, v3, v1

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 64
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragmentState;

    move-result-object v0

    return-object v0
.end method
