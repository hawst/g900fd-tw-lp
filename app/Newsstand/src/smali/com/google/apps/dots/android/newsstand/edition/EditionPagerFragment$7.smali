.class Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;
.super Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;
.source "EditionPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->setupPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Landroid/support/v4/view/NSViewPager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;
    .param p2, "viewPager"    # Landroid/support/v4/view/NSViewPager;

    .prologue
    .line 287
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;-><init>(Landroid/support/v4/view/NSViewPager;)V

    return-void
.end method


# virtual methods
.method public onPageSelected(IZ)V
    .locals 3
    .param p1, "visualPosition"    # I
    .param p2, "userDriven"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 291
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;"
    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->isChangingState()Z
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->access$200(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    .line 292
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v1

    if-nez v1, :cond_1

    .line 293
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-static {v1, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v0

    .line 294
    .local v0, "logicalPosition":I
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->stateFromPosition(I)Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 295
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 296
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/NSViewPager;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 299
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 301
    .end local v0    # "logicalPosition":I
    :cond_1
    return-void
.end method
