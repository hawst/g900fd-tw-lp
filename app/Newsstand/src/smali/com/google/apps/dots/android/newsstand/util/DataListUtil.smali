.class public Lcom/google/apps/dots/android/newsstand/util/DataListUtil;
.super Ljava/lang/Object;
.source "DataListUtil.java"


# direct methods
.method public static findClosestData(Lcom/google/android/libraries/bind/data/DataList;ILcom/google/common/base/Predicate;)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p0, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p1, "desiredPosition"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/DataList;",
            "I",
            "Lcom/google/common/base/Predicate",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;)",
            "Lcom/google/android/libraries/bind/data/Data;"
        }
    .end annotation

    .prologue
    .line 74
    .local p2, "predicate":Lcom/google/common/base/Predicate;, "Lcom/google/common/base/Predicate<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 75
    const/4 v1, 0x0

    .line 76
    .local v1, "foundData":Lcom/google/android/libraries/bind/data/Data;
    const v0, 0x7fffffff

    .line 77
    .local v0, "distanceFromDesired":I
    move v2, p1

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 78
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/google/common/base/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 79
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 80
    sub-int v0, v2, p1

    .line 84
    :cond_0
    add-int/lit8 v2, p1, -0x1

    :goto_1
    if-ltz v2, :cond_1

    sub-int v3, p1, v2

    if-ge v3, v0, :cond_1

    .line 85
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/google/common/base/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 86
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 89
    .end local v1    # "foundData":Lcom/google/android/libraries/bind/data/Data;
    :cond_1
    return-object v1

    .line 77
    .restart local v1    # "foundData":Lcom/google/android/libraries/bind/data/Data;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 84
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method public static whenDataListFirstRefreshed(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/DataList;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 27
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/util/DataListUtil;->whenDataListRefreshed(Lcom/google/android/libraries/bind/data/DataList;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static whenDataListRefreshed(Lcom/google/android/libraries/bind/data/DataList;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p0, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p1, "acceptPreviousRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/DataList;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 41
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 44
    .local v0, "future":Lcom/google/common/util/concurrent/SettableFuture;, "Lcom/google/common/util/concurrent/SettableFuture<Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 45
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    .line 63
    :goto_0
    return-object v0

    .line 48
    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/util/DataListUtil$1;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/util/DataListUtil$1;-><init>(Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 55
    .local v1, "observer":Lcom/google/android/libraries/bind/data/DataObserver;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/util/DataListUtil$2;

    invoke-direct {v2, p0, v1}, Lcom/google/apps/dots/android/newsstand/util/DataListUtil$2;-><init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/DataObserver;)V

    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 61
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    goto :goto_0
.end method
