.class public Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;
.super Lcom/google/apps/dots/android/newsstand/data/MergeList;
.source "CombinedSubscriptionsList.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;


# instance fields
.field private purchasedEditionSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation
.end field

.field private subscribedEditionSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    .line 27
    sget v1, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->DK_EDITION:I

    const/4 v2, 0x0

    check-cast v2, [I

    .line 29
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->getFilter()Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_CPU:Lcom/google/android/libraries/bind/async/Queue;

    const/4 v0, 0x2

    new-array v5, v0, [Lcom/google/android/libraries/bind/data/DataList;

    const/4 v0, 0x0

    .line 31
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->newsSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x1

    .line 32
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curationSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;

    move-result-object v6

    aput-object v6, v5, v0

    move-object v0, p0

    .line 27
    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/data/MergeList;-><init>(I[ILcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;Lcom/google/android/libraries/bind/async/Queue;[Lcom/google/android/libraries/bind/data/DataList;)V

    .line 36
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->onRegisterForInvalidation()V

    .line 37
    return-void
.end method

.method private static getFilter()Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;
    .locals 3
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 85
    if-nez p1, :cond_0

    .line 86
    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->NOT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    .line 97
    :goto_0
    return-object v2

    .line 88
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->findPositionForId(Ljava/lang/Object;)I

    move-result v1

    .line 89
    .local v1, "pos":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 90
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 91
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->DK_IS_PURCHASED:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 92
    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->PAID:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    goto :goto_0

    .line 94
    :cond_1
    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->FREE:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    goto :goto_0

    .line 97
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    :cond_2
    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->NOT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    goto :goto_0
.end method

.method protected notifyDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 2
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    const/4 v1, 0x0

    .line 52
    iget-boolean v0, p1, Lcom/google/android/libraries/bind/data/DataChange;->affectsPrimaryKey:Z

    if-eqz v0, :cond_0

    .line 54
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->subscribedEditionSet:Ljava/util/Set;

    .line 57
    :cond_0
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->purchasedEditionSet:Ljava/util/Set;

    .line 58
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/data/MergeList;->notifyDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 59
    return-void
.end method

.method public purchasedEditionSet()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->purchasedEditionSet:Ljava/util/Set;

    if-nez v2, :cond_1

    .line 73
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->purchasedEditionSet:Ljava/util/Set;

    .line 74
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 75
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 76
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->DK_IS_PURCHASED:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->purchasedEditionSet:Ljava/util/Set;

    sget v2, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->DK_EDITION:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 74
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 81
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->purchasedEditionSet:Ljava/util/Set;

    return-object v2
.end method

.method public subscribedEditionSet()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->subscribedEditionSet:Ljava/util/Set;

    if-nez v1, :cond_0

    .line 63
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->subscribedEditionSet:Ljava/util/Set;

    .line 64
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 65
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->subscribedEditionSet:Ljava/util/Set;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->getItemId(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->subscribedEditionSet:Ljava/util/Set;

    return-object v1
.end method
