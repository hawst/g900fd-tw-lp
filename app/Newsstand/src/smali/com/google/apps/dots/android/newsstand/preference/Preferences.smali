.class public Lcom/google/apps/dots/android/newsstand/preference/Preferences;
.super Ljava/lang/Object;
.source "Preferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;
    }
.end annotation


# static fields
.field private static final CLEAR_ON_DB_WRITE_PROPERTIES:[Ljava/lang/String;

.field public static final DEFAULT_NOTIFICATION_MODE_MAGAZINES:Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

.field private static final GLOBAL_PREF_KEYS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final appContext:Landroid/content/Context;

.field private final prefStore:Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 126
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->CLEAR_ON_DB_WRITE_PROPERTIES:[Ljava/lang/String;

    .line 142
    sget-object v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;->NOTIFICATIONS_DISABLED:Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    sput-object v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->DEFAULT_NOTIFICATION_MODE_MAGAZINES:Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    .line 147
    new-instance v0, Lcom/google/common/collect/ImmutableSet$Builder;

    invoke-direct {v0}, Lcom/google/common/collect/ImmutableSet$Builder;-><init>()V

    const-string v1, "serverTypeV2"

    .line 149
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "customBaseUrl"

    .line 150
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "customGucUrl"

    .line 151
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "account"

    .line 152
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "gcmRegistrationId"

    .line 153
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "gcmRegIdAppVersion"

    .line 154
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "confidentialityAcknowledgedTime"

    .line 155
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "seenUpgradeNotification"

    .line 156
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "hasClearedMagazinesData"

    .line 157
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "firstLaunch"

    .line 158
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "externalStorageDir"

    .line 159
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "pinnedAccounts"

    .line 160
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "lastGlobalUnpin"

    .line 161
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "lastRunVersion"

    .line 162
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "updatedFromVersion"

    .line 163
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "showOnboardQuiz"

    .line 164
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "showedOnboardTutorial"

    .line 165
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "accountNameMap"

    .line 166
    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet$Builder;->build()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->GLOBAL_PREF_KEYS:Ljava/util/Set;

    .line 147
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 173
    new-instance v0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;)V

    .line 174
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefStore"    # Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->appContext:Landroid/content/Context;

    .line 178
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->prefStore:Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;

    .line 179
    return-void
.end method

.method private clientConfigKey()Ljava/lang/String;
    .locals 3

    .prologue
    .line 342
    const-string v0, "clientConfig_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->appContext:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->client_config_version:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getAccountName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 334
    const/4 v0, 0x0

    check-cast v0, Landroid/accounts/Account;

    const-string v1, "account"

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getActualKey(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 309
    sget-object v1, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->GLOBAL_PREF_KEYS:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 310
    .local v0, "globalPref":Z
    if-eqz v0, :cond_0

    .end local p2    # "prefKey":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "prefKey":Ljava/lang/String;
    :cond_0
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getUserKey(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method private getStringSet(Ljava/lang/String;)Ljava/util/Set;
    .locals 6
    .param p1, "prefKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 279
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 280
    .local v1, "jsonString":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 288
    :goto_0
    return-object v4

    .line 284
    :cond_0
    :try_start_0
    new-instance v2, Lorg/codehaus/jackson/map/ObjectMapper;

    invoke-direct {v2}, Lorg/codehaus/jackson/map/ObjectMapper;-><init>()V

    .line 285
    .local v2, "mapper":Lorg/codehaus/jackson/map/ObjectMapper;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/preference/Preferences$2;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences$2;-><init>(Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    invoke-virtual {v2, v1, v5}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Lorg/codehaus/jackson/type/TypeReference;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    .line 286
    .local v3, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {v3}, Lcom/google/common/collect/ImmutableSet;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 287
    .end local v2    # "mapper":Lorg/codehaus/jackson/map/ObjectMapper;
    .end local v3    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public static getUserKey(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 314
    move-object v0, p1

    .line 315
    .local v0, "result":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 316
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getOriginalName(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 318
    :cond_0
    return-object v0
.end method

.method private showOnboardQuizPreferenceStringToInt(Ljava/lang/String;)I
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 486
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->appContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$array;->show_onboard_quiz_values:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 487
    .local v0, "values":[Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/util/ArrayUtil;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    return v1
.end method

.method protected static tryParse(Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 2
    .param p0, "prefValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Enum",
            "<TE;>;>(",
            "Ljava/lang/String;",
            "TE;)TE;"
        }
    .end annotation

    .prologue
    .line 569
    .local p1, "defaultValue":Ljava/lang/Enum;, "TE;"
    invoke-static {p0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 575
    .end local p1    # "defaultValue":Ljava/lang/Enum;, "TE;"
    :goto_0
    return-object p1

    .line 573
    .restart local p1    # "defaultValue":Ljava/lang/Enum;, "TE;"
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    .line 574
    :catch_0
    move-exception v0

    .line 575
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    goto :goto_0
.end method


# virtual methods
.method public getAccount()Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 377
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccountName()Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "accountName":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->accountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAlwaysShowGoogleSoldAds()Z
    .locals 2

    .prologue
    .line 719
    const-string v0, "alwaysShowGoogleSoldAds"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getArticleFontSize()Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    .locals 3

    .prologue
    .line 723
    const-string v2, "articleFontSizev2"

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 724
    .local v1, "fontSizeString":Ljava/lang/String;
    sget-object v2, Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;->MEDIUM:Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->tryParse(Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    .line 725
    .local v0, "fontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    return-object v0
.end method

.method public getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 393
    const-string v0, "authToken"

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBoolean(Landroid/accounts/Account;Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "prefKey"    # Ljava/lang/String;
    .param p3, "defaultValue"    # Z

    .prologue
    .line 186
    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "stringValue":Ljava/lang/String;
    if-nez v0, :cond_0

    .end local p3    # "defaultValue":Z
    :goto_0
    return p3

    .restart local p3    # "defaultValue":Z
    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result p3

    goto :goto_0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "prefKey"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Landroid/accounts/Account;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getClientConfigString(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 350
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->clientConfigKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCountryOverride()Ljava/lang/String;
    .locals 2

    .prologue
    .line 628
    const-string v0, "countryOverride"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCustomBaseUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 620
    const-string v0, "customBaseUrl"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCustomGucUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 624
    const-string v0, "customGucUrl"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultPreferredLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 409
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/translation/Translation;->getDefault()Lcom/google/apps/dots/android/newsstand/translation/Translation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/translation/Translation;->toLanguageCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDesignerMode()Z
    .locals 4

    .prologue
    .line 706
    const-string v0, "designerModeTimestamp"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/32 v2, 0xdbba00

    add-long/2addr v0, v2

    .line 707
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDontShowRemoveDownloadWarning()Z
    .locals 2

    .prologue
    .line 366
    const-string v0, "dontShowRemoveDownloadWarning"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getDownloadViaWifiOnlyPreference()Z
    .locals 2

    .prologue
    .line 453
    const-string v0, "downloadViaWifiOnly"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getDownloadWhileChargingOnlyPreference()Z
    .locals 2

    .prologue
    .line 461
    const-string v0, "syncOnlyIfCharging"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getEnabledLabIds()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 659
    const-string v1, "enabledLabIds"

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getStringSet(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 660
    .local v0, "stored":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v1

    goto :goto_0
.end method

.method public getFirstLaunch()Z
    .locals 2

    .prologue
    .line 401
    const-string v0, "firstLaunch"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getImageSyncType()Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;
    .locals 4

    .prologue
    .line 491
    const-string v2, "syncPreference"

    sget-object v3, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 493
    .local v1, "storedName":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 495
    :goto_0
    return-object v2

    .line 494
    :catch_0
    move-exception v0

    .line 495
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    goto :goto_0
.end method

.method public getInt(Ljava/lang/String;I)I
    .locals 1
    .param p1, "prefKey"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 203
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "stringValue":Ljava/lang/String;
    if-nez v0, :cond_0

    .end local p2    # "defaultValue":I
    :goto_0
    return p2

    .restart local p2    # "defaultValue":I
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    goto :goto_0
.end method

.method public getLastAnalyticsSettingsEventSentTime()J
    .locals 4

    .prologue
    .line 684
    const-string v0, "lastAnalyticsSettingsEventSentTime"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastNewFeatureCardDismissTime()J
    .locals 4

    .prologue
    .line 676
    const-string v0, "lastNewFeatureCardDismissTime"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLoadExtraJs()Ljava/lang/String;
    .locals 2

    .prologue
    .line 632
    const-string v0, "customLoadExtraJs"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLong(Ljava/lang/String;J)J
    .locals 2
    .param p1, "prefKey"    # Ljava/lang/String;
    .param p2, "defaultValue"    # J

    .prologue
    .line 212
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "stringValue":Ljava/lang/String;
    if-nez v0, :cond_0

    .end local p2    # "defaultValue":J
    :goto_0
    return-wide p2

    .restart local p2    # "defaultValue":J
    :cond_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide p2

    goto :goto_0
.end method

.method public getMinSyncSpaceMagazinesMb()I
    .locals 2

    .prologue
    .line 504
    const-string v0, "minSyncSpaceMagsMb"

    const/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getMinSyncSpaceNewsMb()I
    .locals 2

    .prologue
    .line 512
    const-string v0, "minSyncSpaceNewsMb"

    const/16 v1, 0x96

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getMyMagazinesSortByField()I
    .locals 2

    .prologue
    .line 528
    const-string v0, "myMagazinesSortField"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getNotificationMode()Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;
    .locals 3

    .prologue
    .line 545
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "google.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;->NOTIFICATIONS_ENABLED:Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    .line 548
    .local v0, "defaultMode":Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;
    :goto_0
    const-string v1, "notificationMode"

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->tryParse(Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    return-object v1

    .line 545
    .end local v0    # "defaultMode":Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->DEFAULT_NOTIFICATION_MODE_MAGAZINES:Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    goto :goto_0
.end method

.method public getOnDeviceOnly()Z
    .locals 2

    .prologue
    .line 520
    const-string v0, "downloadedOnly"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getOriginalKey(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 322
    sget-object v1, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->GLOBAL_PREF_KEYS:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 323
    .local v0, "globalPref":Z
    if-eqz v0, :cond_0

    .line 324
    .end local p2    # "prefKey":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "prefKey":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getOriginalName(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public getPreferredLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 540
    const-string v1, "preferredLanguage"

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 541
    .local v0, "value":Ljava/lang/String;
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDefaultPreferredLanguage()Ljava/lang/String;

    move-result-object v0

    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getReportShowedOnboardQuiz(Landroid/accounts/Account;)Z
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 437
    const-string v0, "reportShowedOnboardQuiz"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Landroid/accounts/Account;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getRestorableState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 744
    const-string v0, "restorableState"

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSeenUpgradeNotification()Z
    .locals 2

    .prologue
    .line 358
    const-string v0, "seenUpgradeNotification"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getServerType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 615
    const-string v0, "serverTypeV2"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->appContext:Landroid/content/Context;

    .line 616
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->default_environment:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 615
    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShowOnboardQuizPreference()I
    .locals 3

    .prologue
    .line 469
    const-string v0, "showOnboardQuiz"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->appContext:Landroid/content/Context;

    .line 470
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->default_show_onboard_quiz_preference:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 469
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->showOnboardQuizPreferenceStringToInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getShowWarmWelcome(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Z
    .locals 2
    .param p1, "cardType"    # Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .prologue
    .line 668
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->prefKey()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getShowedTutorial()Z
    .locals 2

    .prologue
    .line 429
    const-string v0, "showedOnboardTutorial"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getSortByField(I)V
    .locals 1
    .param p1, "myMagazinesSortByField"    # I

    .prologue
    .line 532
    const-string v0, "myMagazinesSortField"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setInt(Ljava/lang/String;I)V

    .line 533
    return-void
.end method

.method public getString(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->prefStore:Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;

    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getActualKey(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "prefKey"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 182
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/common/base/Objects;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getStringMap(Ljava/lang/String;)Ljava/util/Map;
    .locals 6
    .param p1, "prefKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 245
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 246
    .local v1, "jsonString":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 255
    :goto_0
    return-object v4

    .line 250
    :cond_0
    :try_start_0
    new-instance v3, Lorg/codehaus/jackson/map/ObjectMapper;

    invoke-direct {v3}, Lorg/codehaus/jackson/map/ObjectMapper;-><init>()V

    .line 251
    .local v3, "mapper":Lorg/codehaus/jackson/map/ObjectMapper;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/preference/Preferences$1;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences$1;-><init>(Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    .line 252
    invoke-virtual {v3, v1, v5}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Lorg/codehaus/jackson/type/TypeReference;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 253
    .local v2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {v2}, Lcom/google/common/collect/ImmutableMap;->copyOf(Ljava/util/Map;)Lcom/google/common/collect/ImmutableMap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 254
    .end local v2    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "mapper":Lorg/codehaus/jackson/map/ObjectMapper;
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public getWasMagazinesUser()Z
    .locals 2

    .prologue
    .line 354
    const-string v0, "wasMagazinesUser"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isCompactModeEnabled()Z
    .locals 2

    .prologue
    .line 748
    const-string v0, "useCompactLists"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public varargs registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;
    .locals 6
    .param p1, "listener"    # Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;
    .param p2, "prefKeys"    # [Ljava/lang/String;

    .prologue
    .line 583
    array-length v3, p2

    invoke-static {v3}, Lcom/google/common/collect/Sets;->newHashSetWithExpectedSize(I)Ljava/util/HashSet;

    move-result-object v1

    .line 584
    .local v1, "actualKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 585
    .local v0, "account":Landroid/accounts/Account;
    array-length v4, p2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, p2, v3

    .line 586
    .local v2, "prefKey":Ljava/lang/String;
    invoke-direct {p0, v0, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getActualKey(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 585
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 588
    .end local v2    # "prefKey":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->prefStore:Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/preference/Preferences$3;

    invoke-direct {v4, p0, v1, p1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences$3;-><init>(Lcom/google/apps/dots/android/newsstand/preference/Preferences;Ljava/util/Set;Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;Landroid/accounts/Account;)V

    invoke-interface {v3, v4}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v3

    return-object v3
.end method

.method public setAccount(Landroid/accounts/Account;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 388
    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 389
    .local v0, "accountName":Ljava/lang/String;
    :goto_0
    const-string v1, "account"

    invoke-virtual {p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    return-void

    .line 388
    .end local v0    # "accountName":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAlwaysShowGoogleSoldAds(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 715
    const-string v0, "alwaysShowGoogleSoldAds"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 716
    return-void
.end method

.method public setArticleFontSize(Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;)V
    .locals 2
    .param p1, "fontSize"    # Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    .prologue
    .line 729
    if-nez p1, :cond_0

    .line 730
    sget-object p1, Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;->MEDIUM:Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    .line 732
    :cond_0
    const-string v0, "articleFontSizev2"

    invoke-virtual {p1}, Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    return-void
.end method

.method public setArticleFontSize(Ljava/lang/String;)V
    .locals 1
    .param p1, "fontSizeString"    # Ljava/lang/String;

    .prologue
    .line 736
    sget-object v0, Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;->MEDIUM:Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->tryParse(Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setArticleFontSize(Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;)V

    .line 737
    return-void
.end method

.method public setAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "authToken"    # Ljava/lang/String;

    .prologue
    .line 397
    const-string v0, "authToken"

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method public setBoolean(Landroid/accounts/Account;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "prefKey"    # Ljava/lang/String;
    .param p3, "value"    # Z

    .prologue
    .line 191
    invoke-static {p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    return-void
.end method

.method public setBoolean(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "prefKey"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 200
    return-void
.end method

.method public setClientConfigString(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "s"    # Ljava/lang/String;

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->clientConfigKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    return-void
.end method

.method public setCountryOverride(Ljava/lang/String;)V
    .locals 1
    .param p1, "countryOverride"    # Ljava/lang/String;

    .prologue
    .line 636
    const-string v0, "countryOverride"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    return-void
.end method

.method public setCustomBaseUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "server"    # Ljava/lang/String;

    .prologue
    .line 644
    const-string v0, "customBaseUrl"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    return-void
.end method

.method public setCustomGucUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "server"    # Ljava/lang/String;

    .prologue
    .line 648
    const-string v0, "customGucUrl"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    return-void
.end method

.method public setDesignerMode(Z)V
    .locals 3
    .param p1, "designerMode"    # Z

    .prologue
    .line 711
    const-string v2, "designerModeTimestamp"

    if-eqz p1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :goto_0
    invoke-virtual {p0, v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setLong(Ljava/lang/String;J)V

    .line 712
    return-void

    .line 711
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public setDontShowRemoveDownloadWarning(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 370
    const-string v0, "dontShowRemoveDownloadWarning"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 371
    return-void
.end method

.method public setDownloadViaWifiOnlyPreference(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 457
    const-string v0, "downloadViaWifiOnly"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 458
    return-void
.end method

.method public setDownloadWhileChargingOnlyPreference(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 465
    const-string v0, "syncOnlyIfCharging"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 466
    return-void
.end method

.method public setEnabledLabIds(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 664
    .local p1, "labIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "enabledLabIds"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setStringSet(Ljava/lang/String;Ljava/util/Set;)V

    .line 665
    return-void
.end method

.method public setFirstLaunch(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 405
    const-string v0, "firstLaunch"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 406
    return-void
.end method

.method public setImageSyncType(Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;)V
    .locals 2
    .param p1, "syncType"    # Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    .prologue
    .line 500
    const-string v0, "syncPreference"

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    return-void
.end method

.method public setInt(Ljava/lang/String;I)V
    .locals 1
    .param p1, "prefKey"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 208
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    return-void
.end method

.method public setLastAnalyticsSettingsEventSentTime(J)V
    .locals 1
    .param p1, "dismissTime"    # J

    .prologue
    .line 688
    const-string v0, "lastAnalyticsSettingsEventSentTime"

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setLong(Ljava/lang/String;J)V

    .line 689
    return-void
.end method

.method public setLastNewFeatureCardDismissTime(J)V
    .locals 1
    .param p1, "dismissTime"    # J

    .prologue
    .line 680
    const-string v0, "lastNewFeatureCardDismissTime"

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setLong(Ljava/lang/String;J)V

    .line 681
    return-void
.end method

.method public setLoadExtraJs(Ljava/lang/String;)V
    .locals 1
    .param p1, "customLoadExtraJs"    # Ljava/lang/String;

    .prologue
    .line 652
    const-string v0, "customLoadExtraJs"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    return-void
.end method

.method public setLong(Ljava/lang/String;J)V
    .locals 2
    .param p1, "prefKey"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 217
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    return-void
.end method

.method public setMinSyncSpaceMagazinesMb(I)V
    .locals 1
    .param p1, "mb"    # I

    .prologue
    .line 508
    const-string v0, "minSyncSpaceMagsMb"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setInt(Ljava/lang/String;I)V

    .line 509
    return-void
.end method

.method public setMinSyncSpaceNewsMb(I)V
    .locals 1
    .param p1, "mb"    # I

    .prologue
    .line 516
    const-string v0, "minSyncSpaceNewsMb"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setInt(Ljava/lang/String;I)V

    .line 517
    return-void
.end method

.method public setNotificationMode(Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;)V
    .locals 2
    .param p1, "value"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    .prologue
    .line 552
    const-string v0, "notificationMode"

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    return-void
.end method

.method public setOnDeviceOnly(Z)V
    .locals 1
    .param p1, "onDeviceOnly"    # Z

    .prologue
    .line 524
    const-string v0, "downloadedOnly"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 525
    return-void
.end method

.method public setReadingPositionSync(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 413
    const-string v0, "readingPositionSync"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 414
    return-void
.end method

.method public setReportShowedOnboardQuiz(Landroid/accounts/Account;Z)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "showed"    # Z

    .prologue
    .line 441
    const-string v0, "reportShowedOnboardQuiz"

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 442
    return-void
.end method

.method public setRestorableState(Ljava/lang/String;)V
    .locals 1
    .param p1, "restorableState"    # Ljava/lang/String;

    .prologue
    .line 740
    const-string v0, "restorableState"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    return-void
.end method

.method public setSeenUpgradeNotification(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 362
    const-string v0, "seenUpgradeNotification"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 363
    return-void
.end method

.method public setServerType(Ljava/lang/String;)V
    .locals 1
    .param p1, "serverType"    # Ljava/lang/String;

    .prologue
    .line 640
    const-string v0, "serverTypeV2"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    return-void
.end method

.method public setShowOnboardQuizPreference(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 474
    packed-switch p1, :pswitch_data_0

    .line 481
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 478
    :pswitch_0
    const-string v0, "showOnboardQuiz"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setInt(Ljava/lang/String;I)V

    .line 483
    return-void

    .line 474
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setShowWarmWelcome(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;Z)V
    .locals 1
    .param p1, "cardType"    # Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;
    .param p2, "show"    # Z

    .prologue
    .line 672
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->prefKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 673
    return-void
.end method

.method public setShowedTutorial(Z)V
    .locals 1
    .param p1, "showed"    # Z

    .prologue
    .line 433
    const-string v0, "showedOnboardTutorial"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 434
    return-void
.end method

.method public setString(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "prefKey"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->prefStore:Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;

    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getActualKey(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/PrefStore;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    return-void
.end method

.method public setString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "prefKey"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    return-void
.end method

.method public setStringMap(Ljava/lang/String;Ljava/util/Map;)V
    .locals 6
    .param p1, "prefKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 230
    .local p2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 231
    .local v1, "jsonString":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 232
    sget-object v3, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v3}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    .line 233
    .local v2, "object":Lorg/codehaus/jackson/node/ObjectNode;
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 234
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 236
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {v2}, Lorg/codehaus/jackson/node/ObjectNode;->toString()Ljava/lang/String;

    move-result-object v1

    .line 238
    .end local v2    # "object":Lorg/codehaus/jackson/node/ObjectNode;
    :cond_1
    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    return-void
.end method

.method public setStringSet(Ljava/lang/String;Ljava/util/Set;)V
    .locals 5
    .param p1, "prefKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 293
    .local p2, "value":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 294
    .local v1, "jsonString":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 295
    sget-object v3, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v3}, Lorg/codehaus/jackson/node/JsonNodeFactory;->arrayNode()Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v2

    .line 296
    .local v2, "node":Lorg/codehaus/jackson/node/ArrayNode;
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 297
    .local v0, "entry":Ljava/lang/String;
    invoke-virtual {v2, v0}, Lorg/codehaus/jackson/node/ArrayNode;->add(Ljava/lang/String;)V

    goto :goto_0

    .line 299
    .end local v0    # "entry":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Lorg/codehaus/jackson/node/ArrayNode;->toString()Ljava/lang/String;

    move-result-object v1

    .line 301
    .end local v2    # "node":Lorg/codehaus/jackson/node/ArrayNode;
    :cond_1
    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    return-void
.end method

.method public toggleCompactMode()Z
    .locals 2

    .prologue
    .line 759
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->isCompactModeEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 760
    .local v0, "enable":Z
    :goto_0
    const-string v1, "useCompactLists"

    invoke-virtual {p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 761
    return v0

    .line 759
    .end local v0    # "enable":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public useReadingPositionSync()Z
    .locals 2

    .prologue
    .line 417
    const-string v0, "readingPositionSync"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
