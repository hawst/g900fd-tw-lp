.class final Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList$1;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/SimpleNodeVisitor;
.source "ReadStateList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->getReadStatesMap(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;J)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$dataMap:Ljava/util/Map;

.field final synthetic val$meteredDuration:J

.field final synthetic val$serverNow:J


# direct methods
.method constructor <init>(Ljava/util/Map;JJ)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList$1;->val$dataMap:Ljava/util/Map;

    iput-wide p2, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList$1;->val$serverNow:J

    iput-wide p4, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList$1;->val$meteredDuration:J

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/SimpleNodeVisitor;-><init>()V

    return-void
.end method


# virtual methods
.method getOrCreateData(Ljava/lang/String;)Lcom/google/android/libraries/bind/data/Data;
    .locals 2
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList$1;->val$dataMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 90
    .local v0, "result":Lcom/google/android/libraries/bind/data/Data;
    if-nez v0, :cond_0

    .line 91
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    .end local v0    # "result":Lcom/google/android/libraries/bind/data/Data;
    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 92
    .restart local v0    # "result":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->DK_POST_ID:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 93
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList$1;->val$dataMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    :cond_0
    return-object v0
.end method

.method public visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 6
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    .line 100
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v2

    const/4 v3, 0x7

    if-ne v2, v3, :cond_0

    .line 101
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getPostReadState()Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v1

    .line 102
    .local v1, "postReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getState()I

    move-result v2

    if-nez v2, :cond_1

    .line 113
    .end local v1    # "postReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    :cond_0
    :goto_0
    return-void

    .line 105
    .restart local v1    # "postReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    :cond_1
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getPostId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList$1;->getOrCreateData(Ljava/lang/String;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 106
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->DK_POST_READ_STATE:I

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 107
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getIsPostInMeteredSection()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getWasEditionOwnedWhenRead()Z

    move-result v2

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList$1;->val$serverNow:J

    .line 109
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getUpdateTimestamp()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList$1;->val$meteredDuration:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 110
    sget v2, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->DK_IS_METERED_READ:I

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method
