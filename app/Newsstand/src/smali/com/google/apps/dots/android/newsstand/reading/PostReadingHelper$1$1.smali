.class Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1$1;
.super Ljava/lang/Object;
.source "PostReadingHelper.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;->call()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;",
        "Lcom/google/apps/dots/android/newsstand/share/ShareParams;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    .locals 2
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 90
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 89
    invoke-static {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUtil;->getShareParamsForPost(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 86
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1$1;->apply(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    move-result-object v0

    return-object v0
.end method
