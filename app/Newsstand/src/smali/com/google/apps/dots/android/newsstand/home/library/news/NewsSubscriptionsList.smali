.class public Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;
.super Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;
.source "NewsSubscriptionsList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;
    }
.end annotation


# instance fields
.field private final newsCardFilter:Lcom/google/android/libraries/bind/data/Filter;

.field private purchasedEditionDataList:Lcom/google/android/libraries/bind/data/DataList;

.field private purchasedEditionSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation
.end field

.field private subscribedEditionSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;-><init>()V

    .line 71
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->NEWS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->makeCardFilter(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Lcom/google/android/libraries/bind/data/Filter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->newsCardFilter:Lcom/google/android/libraries/bind/data/Filter;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/libraries/bind/data/Data;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->fillInData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    return-void
.end method

.method static synthetic access$100()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->makeRecommendedCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method private static fillInData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 8
    .param p0, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    const/4 v7, 0x7

    .line 228
    sget v5, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v6, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->EQUALITY_FIELDS:[I

    invoke-virtual {p0, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 229
    sget v5, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->DK_ICON_ID:I

    invoke-virtual {p0, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v3

    .line 230
    .local v3, "iconId":Ljava/lang/String;
    sget v5, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->DK_TITLE:I

    invoke-virtual {p0, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v4

    .line 231
    .local v4, "title":Ljava/lang/String;
    sget v5, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->DK_APP_SUMMARY:I

    invoke-virtual {p0, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 232
    .local v1, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    sget v5, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->DK_APP_FAMILY_SUMMARY:I

    invoke-virtual {p0, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 233
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getAppType()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->getType()I

    move-result v2

    .line 236
    .local v2, "appType":I
    const/4 v5, 0x3

    if-ne v2, v5, :cond_0

    .line 237
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_ICON_BACKGROUND_RES_ID:I

    iget-object v6, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    .line 238
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getRssDrawableId(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 237
    invoke-virtual {p0, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 247
    :goto_0
    if-ne v2, v7, :cond_2

    .line 248
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_BACKGROUND_COLOR_RESID:I

    .line 250
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getIdToUseForColor(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Ljava/lang/String;

    move-result-object v6

    .line 249
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorResIdForEditionId(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 248
    invoke-virtual {p0, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 251
    sget v5, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->SOLID_COLOR_LAYOUT:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 256
    :goto_1
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_NAME:I

    invoke-virtual {p0, v5, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 257
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_TRANSITION_NAME:I

    iget-object v6, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-virtual {p0, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 258
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_CLICKHANDLER:I

    new-instance v6, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$4;

    invoke-direct {v6, p1}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$4;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {p0, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 274
    return-void

    .line 239
    :cond_0
    invoke-static {v3}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    if-ne v2, v7, :cond_1

    .line 241
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_ICON_NO_IMAGE:I

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 244
    :cond_1
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_ICON_ID:I

    invoke-virtual {p0, v5, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 253
    :cond_2
    sget v5, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->STANDARD_LAYOUT:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method private static getHighlightsCardAspectRatio(I)F
    .locals 7
    .param p0, "columnSpan"    # I

    .prologue
    .line 209
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$dimen;->my_sources_column_width:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 212
    .local v3, "columnWidth":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$dimen;->intrinsic_card_padding:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    mul-int/lit8 v1, v5, 0x2

    .line 215
    .local v1, "columnGapWidth":I
    int-to-float v5, v1

    int-to-float v6, v3

    div-float v0, v5, v6

    .line 217
    .local v0, "columnGapFraction":F
    const/4 v5, -0x1

    if-ne p0, v5, :cond_0

    .line 218
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v4, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 219
    .local v4, "viewWidth":I
    div-int p0, v4, v3

    .line 222
    .end local v4    # "viewWidth":I
    :cond_0
    int-to-float v5, p0

    add-int/lit8 v6, p0, -0x1

    int-to-float v6, v6

    mul-float/2addr v6, v0

    add-float v2, v5, v6

    .line 223
    .local v2, "columnSpanIncludingGaps":F
    const/high16 v5, 0x3f800000    # 1.0f

    div-float/2addr v5, v2

    return v5
.end method

.method private static getHighlightsCardColumnSpan()I
    .locals 3

    .prologue
    .line 190
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v1

    .line 191
    .local v1, "orientation":Lcom/google/apps/dots/shared/Orientation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    .line 193
    .local v0, "category":Lcom/google/apps/dots/shared/DeviceCategory;
    sget-object v2, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-ne v1, v2, :cond_0

    sget-object v2, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v0, v2, :cond_0

    .line 194
    const/4 v2, -0x1

    .line 197
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x2

    goto :goto_0
.end method

.method public static getPrecedingAppFamilyIdOfSameType(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Ljava/lang/String;
    .locals 6
    .param p0, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    const/4 v3, 0x0

    .line 287
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v4

    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v4, v5, :cond_1

    .line 288
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curationSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;

    move-result-object v2

    .line 296
    .local v2, "subscriptionsList":Lcom/google/android/libraries/bind/data/DataList;
    :goto_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    .line 297
    .local v0, "listPosition":I
    if-gtz v0, :cond_2

    .line 301
    .end local v0    # "listPosition":I
    .end local v2    # "subscriptionsList":Lcom/google/android/libraries/bind/data/DataList;
    :cond_0
    :goto_1
    return-object v3

    .line 289
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v4

    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->NEWS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v4, v5, :cond_0

    .line 290
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->newsSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;

    move-result-object v2

    .restart local v2    # "subscriptionsList":Lcom/google/android/libraries/bind/data/DataList;
    goto :goto_0

    .line 300
    .restart local v0    # "listPosition":I
    :cond_2
    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 301
    .local v1, "pivot":Lcom/google/android/libraries/bind/data/Data;
    sget v3, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->DK_APP_FAMILY_ID:I

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto :goto_1
.end method

.method protected static makeCardFilter(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Lcom/google/android/libraries/bind/data/Filter;
    .locals 2
    .param p0, "editionType"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .prologue
    .line 136
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$2;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, v1, p0}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$2;-><init>(Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)V

    return-object v0
.end method

.method private static makeRecommendedCardData()Lcom/google/android/libraries/bind/data/Data;
    .locals 5

    .prologue
    .line 160
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 161
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->highlights_edition_title:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 162
    .local v2, "title":Ljava/lang/String;
    sget v3, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->DK_EDITION:I

    sget-object v4, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 163
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;->DK_TITLE:I

    invoke-virtual {v0, v3, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 164
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_NAME:I

    invoke-virtual {v0, v3, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 165
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_CLICKHANDLER:I

    new-instance v4, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$3;

    invoke-direct {v4}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$3;-><init>()V

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 173
    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$layout;->card_highlights_item:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 174
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_BACKGROUND_COLOR_RESID:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->card_background_navy:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 176
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->getHighlightsCardColumnSpan()I

    move-result v1

    .line 177
    .local v1, "numColumns":I
    sget v3, Lcom/google/android/libraries/bind/card/GridGroup;->DK_COLUMN_SPAN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 178
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_HIGHLIGHTS_ASPECT_RATIO:I

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->getHighlightsCardAspectRatio(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 180
    return-object v0
.end method


# virtual methods
.method protected makeEdition(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p2, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 127
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromSummaries(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    return-object v0
.end method

.method public newsCardList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->newsCardFilter:Lcom/google/android/libraries/bind/data/Filter;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method protected notifyDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 2
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    const/4 v1, 0x0

    .line 116
    iget-boolean v0, p1, Lcom/google/android/libraries/bind/data/DataChange;->affectsPrimaryKey:Z

    if-eqz v0, :cond_0

    .line 118
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->subscribedEditionSet:Ljava/util/Set;

    .line 121
    :cond_0
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->purchasedEditionSet:Ljava/util/Set;

    .line 122
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;->notifyDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 123
    return-void
.end method

.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 132
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyNews(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public purchasedEditionDataList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 3

    .prologue
    .line 102
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->purchasedEditionDataList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v1, :cond_0

    .line 103
    const/4 v1, 0x1

    new-array v0, v1, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->DK_IS_PURCHASED:I

    aput v2, v0, v1

    .line 104
    .local v0, "equalityFields":[I
    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$1;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;Ljava/util/concurrent/Executor;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->purchasedEditionDataList:Lcom/google/android/libraries/bind/data/DataList;

    .line 111
    .end local v0    # "equalityFields":[I
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->purchasedEditionDataList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v1
.end method
