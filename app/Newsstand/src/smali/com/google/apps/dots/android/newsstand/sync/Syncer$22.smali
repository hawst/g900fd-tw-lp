.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$22;
.super Ljava/lang/Object;
.source "Syncer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;->setProgressListener(Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .prologue
    .line 1102
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$22;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$22;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->lock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$600(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1106
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$22;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressListener:Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$4100(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1107
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$22;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->progressListener:Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$4100(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;

    move-result-object v0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$22;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->reportedProgress:F
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$4200(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)F

    move-result v2

    invoke-interface {v0, v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;->onProgress(F)V

    .line 1109
    :cond_0
    monitor-exit v1

    .line 1110
    return-void

    .line 1109
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
