.class public abstract Lcom/google/apps/dots/android/newsstand/ondevice/DataMenuHelper;
.super Ljava/lang/Object;
.source "DataMenuHelper.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/DataView;


# instance fields
.field protected final fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

.field private final helper:Lcom/google/android/libraries/bind/data/DataViewHelper;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;-><init>(Lcom/google/android/libraries/bind/data/DataView;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/DataMenuHelper;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    .line 21
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/DataMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .line 22
    return-void
.end method


# virtual methods
.method public getData()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/DataMenuHelper;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public getDataRow()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/DataMenuHelper;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 0
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 33
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/DataMenuHelper;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onDetachedFromWindow()V

    .line 41
    return-void
.end method

.method public onViewCreated()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/DataMenuHelper;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onAttachedToWindow()V

    .line 37
    return-void
.end method

.method public setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/DataMenuHelper;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataViewHelper;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 46
    return-void
.end method
