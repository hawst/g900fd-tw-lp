.class public interface abstract Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;
.super Ljava/lang/Object;
.source "BindingCardViewGroup.java"

# interfaces
.implements Lcom/google/android/play/cardview/CardViewGroup;


# static fields
.field public static final DK_ANALYTICS_EVENT_PROVIDER:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardBase_analyticsEvent:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    return-void
.end method


# virtual methods
.method public abstract getBindingViewGroupHelper()Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;
.end method

.method public abstract getData()Lcom/google/android/libraries/bind/data/Data;
.end method
