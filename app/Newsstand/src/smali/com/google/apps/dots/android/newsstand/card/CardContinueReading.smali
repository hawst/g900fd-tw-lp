.class public Lcom/google/apps/dots/android/newsstand/card/CardContinueReading;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "CardContinueReading.java"


# static fields
.field public static final DK_LABEL_AND_TITLE:I

.field public static final DK_ON_CLICK_LISTENER:I

.field public static final LAYOUT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardContinueReading_labelAndTitle:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading;->DK_LABEL_AND_TITLE:I

    .line 26
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardContinueReading_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading;->DK_ON_CLICK_LISTENER:I

    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_continue_reading:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading;->LAYOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public static createCardData(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Z)Lcom/google/android/libraries/bind/data/Data;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .param p3, "inLiteMode"    # Z

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 46
    .local v0, "continueReadingData":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading;->LAYOUT:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 48
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "title":Ljava/lang/String;
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading;->DK_LABEL_AND_TITLE:I

    .line 50
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->continue_reading_label:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 49
    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 53
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading;->DK_ON_CLICK_LISTENER:I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading$1;

    invoke-direct {v3, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Z)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 63
    return-object v0
.end method
