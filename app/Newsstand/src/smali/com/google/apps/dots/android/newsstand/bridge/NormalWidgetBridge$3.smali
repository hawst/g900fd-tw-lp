.class Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$3;
.super Ljava/lang/Object;
.source "NormalWidgetBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->switchToApp(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

.field final synthetic val$appId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$3;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$3;->val$appId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 73
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$3;->val$appId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v0

    .line 74
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$3;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    .line 75
    return-void
.end method
