.class public final Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsFinsky.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Dimensions"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;


# instance fields
.field private bitField0_:I

.field private height_:I

.field private width_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 747
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 748
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 753
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->width_:I

    .line 772
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->height_:I

    .line 748
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;
    .locals 3

    .prologue
    .line 802
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 806
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;
    return-object v0

    .line 803
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;
    :catch_0
    move-exception v1

    .line 804
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 744
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 811
    if-ne p1, p0, :cond_1

    .line 814
    :cond_0
    :goto_0
    return v1

    .line 812
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 813
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    .line 814
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->width_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->width_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->height_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->height_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 838
    const/4 v0, 0x0

    .line 839
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 840
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->width_:I

    .line 841
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 843
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 844
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->height_:I

    .line 845
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 847
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->cachedSize:I

    .line 848
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 819
    const/16 v0, 0x11

    .line 820
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 821
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->width_:I

    add-int v0, v1, v2

    .line 822
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->height_:I

    add-int v0, v1, v2

    .line 823
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 856
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 857
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 861
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 862
    :sswitch_0
    return-object p0

    .line 867
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->width_:I

    .line 868
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->bitField0_:I

    goto :goto_0

    .line 872
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->height_:I

    .line 873
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->bitField0_:I

    goto :goto_0

    .line 857
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 744
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 828
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 829
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->width_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 831
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 832
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->height_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 834
    :cond_1
    return-void
.end method
