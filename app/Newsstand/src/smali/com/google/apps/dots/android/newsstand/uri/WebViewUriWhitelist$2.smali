.class Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist$2;
.super Ljava/lang/Object;
.source "WebViewUriWhitelist.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->load()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist$2;->this$0:Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 58
    # getter for: Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Failed to build URI whitelist for webview: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 51
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist$2;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist$2;->this$0:Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    # invokes: Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->generatePattern(Ljava/util/List;)V
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->access$000(Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;Ljava/util/List;)V

    .line 55
    return-void
.end method
