.class public Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;
.super Ljava/lang/Object;
.source "HtmlWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ArticleState"
.end annotation


# instance fields
.field public currPage:I

.field public isLaidOut:Z

.field public pageCount:I

.field public pageFraction:F

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;)V
    .locals 2
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

    .prologue
    const/4 v1, 0x0

    .line 78
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->this$0:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    .line 81
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageFraction:F

    .line 83
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    return-void
.end method


# virtual methods
.method public clone()Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->this$0:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;-><init>(Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;)V

    .line 88
    .local v0, "copy":Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    iput v1, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    .line 89
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    iput v1, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    .line 90
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageFraction:F

    iput v1, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageFraction:F

    .line 91
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    iput-boolean v1, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    .line 92
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->clone()Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 111
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 112
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    .line 113
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 116
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;
    :cond_0
    return v1
.end method

.method public isDefaultState()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 106
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    if-ne v1, v0, :cond_0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 97
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "ArticleState: page %d of %d, laidOut: %b, pf: %f"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    add-int/lit8 v4, v4, 0x1

    .line 99
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    .line 100
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    .line 101
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageFraction:F

    .line 102
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    .line 97
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
