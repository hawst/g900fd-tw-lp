.class public Lcom/google/apps/dots/android/newsstand/widget/NSTextView;
.super Lcom/google/android/libraries/bind/widget/BoundTextView;
.source "NSTextView.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;


# static fields
.field private static final TEXT_STYLE_STYLEABLE:[I


# instance fields
.field private horizontallyScrolling:Z

.field private isRead:Z

.field private maxLines:I

.field private suppressLayout:Z

.field private useTitleEllipsizing:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010097

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->TEXT_STYLE_STYLEABLE:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BoundTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    iget v9, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->maxLines:I

    if-nez v9, :cond_0

    .line 44
    const/4 v9, -0x1

    iput v9, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->maxLines:I

    .line 46
    :cond_0
    sget-object v9, Lcom/google/android/apps/newsstanddev/R$styleable;->NSTextView:[I

    .line 47
    invoke-virtual {p1, p2, v9, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 48
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->isInEditMode()Z

    move-result v9

    if-nez v9, :cond_1

    .line 50
    sget-object v9, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->TEXT_STYLE_STYLEABLE:[I

    invoke-virtual {p1, p2, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v7

    .line 51
    .local v7, "textStyleTypedArray":Landroid/content/res/TypedArray;
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    .line 52
    .local v6, "textStyle":I
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    .line 53
    sget v9, Lcom/google/android/apps/newsstanddev/R$styleable;->NSTextView_font:I

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 54
    .local v8, "typefaceStr":Ljava/lang/String;
    if-eqz v8, :cond_1

    .line 55
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/NSFont;

    move-result-object v1

    .line 56
    .local v1, "font":Lcom/google/apps/dots/android/newsstand/widget/NSFont;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/google/apps/dots/android/newsstand/widget/NSFont;->getTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v9

    invoke-virtual {p0, v9, v6}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 60
    .end local v1    # "font":Lcom/google/apps/dots/android/newsstand/widget/NSFont;
    .end local v6    # "textStyle":I
    .end local v7    # "textStyleTypedArray":Landroid/content/res/TypedArray;
    .end local v8    # "typefaceStr":Ljava/lang/String;
    :cond_1
    sget v9, Lcom/google/android/apps/newsstanddev/R$styleable;->NSTextView_shadowColor:I

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 61
    .local v2, "shadowColor":I
    if-eqz v2, :cond_2

    .line 62
    sget v9, Lcom/google/android/apps/newsstanddev/R$styleable;->NSTextView_shadowRadius:I

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v5

    .line 63
    .local v5, "shadowRadius":F
    sget v9, Lcom/google/android/apps/newsstanddev/R$styleable;->NSTextView_shadowDx:I

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    .line 64
    .local v3, "shadowDx":F
    sget v9, Lcom/google/android/apps/newsstanddev/R$styleable;->NSTextView_shadowDy:I

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    .line 65
    .local v4, "shadowDy":F
    invoke-virtual {p0, v5, v3, v4, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setShadowLayer(FFFI)V

    .line 69
    .end local v3    # "shadowDx":F
    .end local v4    # "shadowDy":F
    .end local v5    # "shadowRadius":F
    :goto_0
    sget v9, Lcom/google/android/apps/newsstanddev/R$styleable;->NSTextView_useTitleEllipsizing:I

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v9

    iput-boolean v9, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->useTitleEllipsizing:Z

    .line 70
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 71
    return-void

    .line 67
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9}, Landroid/text/TextPaint;->clearShadowLayer()V

    goto :goto_0
.end method

.method private checkTitleEllipsizing()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 128
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->useTitleEllipsizing:Z

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v0

    .line 133
    .local v0, "firstLineEnd":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 134
    .local v3, "string":Ljava/lang/String;
    const-string v4, " "

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 135
    .local v1, "firstSpace":I
    if-gez v1, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 136
    .local v2, "firstWordEnd":I
    :goto_1
    if-lt v2, v0, :cond_0

    .line 137
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->titleEllipsizingHack()V

    goto :goto_0

    .line 135
    .end local v2    # "firstWordEnd":I
    :cond_2
    add-int/lit8 v2, v1, -0x1

    goto :goto_1
.end method

.method private titleEllipsizingHack()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 154
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->maxLines:I

    .line 155
    .local v1, "oldMaxLines":I
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->horizontallyScrolling:Z

    .line 156
    .local v0, "oldHorizontallyScrolling":Z
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->suppressLayout:Z

    .line 157
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setMaxLines(I)V

    .line 158
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setHorizontallyScrolling(Z)V

    .line 159
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->suppressLayout:Z

    .line 160
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->maxLines:I

    .line 161
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->horizontallyScrolling:Z

    .line 162
    return-void
.end method


# virtual methods
.method public isRead()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->isRead:Z

    return v0
.end method

.method protected makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 75
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    return-object v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->isRead()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Lcom/google/android/libraries/bind/widget/BoundTextView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 95
    .local v0, "drawableState":[I
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->STATE_READ:[I

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->mergeDrawableStates([I[I)[I

    .line 98
    .end local v0    # "drawableState":[I
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundTextView;->onCreateDrawableState(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 109
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundTextView;->onDraw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/StackOverflowError;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-gt v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0}, Ljava/lang/StackOverflowError;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 114
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0

    .line 111
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 120
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundTextView;->onMeasure(II)V

    .line 121
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->checkTitleEllipsizing()V

    .line 122
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->suppressLayout:Z

    if-nez v0, :cond_0

    .line 184
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->requestLayout()V

    .line 186
    :cond_0
    return-void
.end method

.method public setHorizontallyScrolling(Z)V
    .locals 0
    .param p1, "whether"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->horizontallyScrolling:Z

    .line 167
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setHorizontallyScrolling(Z)V

    .line 168
    return-void
.end method

.method public setIsRead(Z)V
    .locals 1
    .param p1, "isRead"    # Z

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->isRead:Z

    if-eq v0, p1, :cond_0

    .line 86
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->isRead:Z

    .line 87
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->refreshDrawableState()V

    .line 89
    :cond_0
    return-void
.end method

.method public setLines(I)V
    .locals 0
    .param p1, "lines"    # I

    .prologue
    .line 149
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->maxLines:I

    .line 150
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setLines(I)V

    .line 151
    return-void
.end method

.method public setMaxLines(I)V
    .locals 0
    .param p1, "lines"    # I

    .prologue
    .line 143
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->maxLines:I

    .line 144
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setMaxLines(I)V

    .line 145
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "type"    # Landroid/widget/TextView$BufferType;

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->useTitleEllipsizing:Z

    if-eqz v0, :cond_1

    .line 173
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->maxLines:I

    if-lez v0, :cond_0

    .line 174
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->maxLines:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setMaxLines(I)V

    .line 176
    :cond_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->horizontallyScrolling:Z

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setHorizontallyScrolling(Z)V

    .line 178
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 179
    return-void
.end method
