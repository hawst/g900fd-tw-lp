.class public Lcom/google/apps/dots/android/newsstand/audio/AudioButton;
.super Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;
.source "AudioButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;
    }
.end annotation


# instance fields
.field private animationEnabled:Z

.field private final audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

.field private bindAudioPostIdKey:Ljava/lang/Integer;

.field private isAttachedToWindow:Z

.field private postId:Ljava/lang/String;

.field private final rootView:Landroid/view/View;

.field private state:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->animationEnabled:Z

    .line 30
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->isAttachedToWindow:Z

    .line 31
    sget-object v2, Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;->INACTIVE:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->state:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    .line 54
    sget-object v2, Lcom/google/android/apps/newsstanddev/R$styleable;->AudioButton:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 55
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->AudioButton_bindAudioPostIdKey:I

    invoke-static {v0, v2}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->bindAudioPostIdKey:Ljava/lang/Integer;

    .line 56
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 58
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 59
    .local v1, "layoutInflater":Landroid/view/LayoutInflater;
    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->audio_button:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->rootView:Landroid/view/View;

    .line 62
    sget-object v2, Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;->INACTIVE:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->setState(Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;)V

    .line 64
    new-instance v2, Lcom/google/apps/dots/android/newsstand/audio/AudioButton$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton$1;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioButton;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioButton;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioButton;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->evaluateAudioState(Landroid/os/Bundle;)V

    return-void
.end method

.method private enableAudioAnimation(Z)V
    .locals 5
    .param p1, "enable"    # Z

    .prologue
    .line 141
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->animationEnabled:Z

    if-ne v3, p1, :cond_0

    .line 161
    :goto_0
    return-void

    .line 144
    :cond_0
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->animationEnabled:Z

    .line 145
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->rootView:Landroid/view/View;

    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->audio_meter_1:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 146
    .local v0, "audioMeter1":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->rootView:Landroid/view/View;

    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->audio_meter_2:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 147
    .local v1, "audioMeter2":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->rootView:Landroid/view/View;

    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->audio_meter_3:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 149
    .local v2, "audioMeter3":Landroid/widget/ImageView;
    if-eqz p1, :cond_1

    .line 150
    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->audio_meter_animation_1:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 151
    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->audio_meter_animation_2:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 152
    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->audio_meter_animation_3:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 153
    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 154
    invoke-virtual {v1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 155
    invoke-virtual {v2}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    goto :goto_0

    .line 157
    :cond_1
    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->indicator_playing_peak_meter_1:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 158
    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->indicator_playing_peak_meter_1:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 159
    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->indicator_playing_peak_meter_1:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private evaluateAudioState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 108
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->postId:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 138
    :goto_0
    return-void

    .line 111
    :cond_0
    const-string v3, "audio_item"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    .line 112
    .local v0, "audioItem":Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    if-nez v0, :cond_1

    move-object v1, v2

    .line 113
    .local v1, "audioPostId":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->postId:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 114
    sget-object v2, Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;->INACTIVE:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->setState(Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;)V

    .line 115
    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->enableAudioAnimation(Z)V

    goto :goto_0

    .line 112
    .end local v1    # "audioPostId":Ljava/lang/String;
    :cond_1
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->postId:Ljava/lang/String;

    goto :goto_1

    .line 118
    .restart local v1    # "audioPostId":Ljava/lang/String;
    :cond_2
    const-string v3, "status"

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 134
    sget-object v3, Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;->INACTIVE:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->setState(Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;)V

    .line 135
    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->enableAudioAnimation(Z)V

    .line 136
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 121
    :pswitch_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;->ACTIVE:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->setState(Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;)V

    .line 122
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->enableAudioAnimation(Z)V

    .line 123
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->media_pause:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 128
    :pswitch_1
    sget-object v2, Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;->ACTIVE:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->setState(Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;)V

    .line 129
    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->enableAudioAnimation(Z)V

    .line 130
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->media_play:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateAudioRegistration()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->postId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->isAttachedToWindow:Z

    if-nez v0, :cond_1

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->unregister(Landroid/content/Context;)V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->register(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private updateState()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 172
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->state:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;->ACTIVE:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    if-ne v0, v1, :cond_1

    .line 173
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->rootView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->audio_inactive:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->rootView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->audio_active:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->state:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;->INACTIVE:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    if-ne v0, v1, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->rootView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->audio_inactive:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->rootView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->audio_active:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->onAttachedToWindow()V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->isAttachedToWindow:Z

    .line 89
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->updateAudioRegistration()V

    .line 90
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->onDetachedFromWindow()V

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->isAttachedToWindow:Z

    .line 96
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->updateAudioRegistration()V

    .line 97
    return-void
.end method

.method public setState(Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;)V
    .locals 1
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->state:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    if-ne v0, p1, :cond_0

    .line 169
    :goto_0
    return-void

    .line 167
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->state:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    .line 168
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->updateState()V

    goto :goto_0
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 75
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->bindAudioPostIdKey:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 76
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;->INACTIVE:Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->setState(Lcom/google/apps/dots/android/newsstand/audio/AudioButton$State;)V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->postId:Ljava/lang/String;

    .line 78
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->updateAudioRegistration()V

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->bindAudioPostIdKey:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->postId:Ljava/lang/String;

    .line 82
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioButton;->updateAudioRegistration()V

    goto :goto_0
.end method
