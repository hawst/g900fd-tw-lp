.class final Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;
.super Ljava/lang/Object;
.source "PrefixTree.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Node"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<KeySegmentT:",
        "Ljava/lang/Object;",
        "ValueT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final children:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TKeySegmentT;",
            "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node",
            "<TKeySegmentT;TValueT;>;>;"
        }
    .end annotation
.end field

.field final values:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TValueT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 22
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->values:Ljava/util/Set;

    .line 24
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->children:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$1;

    .prologue
    .line 22
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;-><init>()V

    return-void
.end method


# virtual methods
.method get(Ljava/util/Collection;Ljava/util/Iterator;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TValueT;>;",
            "Ljava/util/Iterator",
            "<TKeySegmentT;>;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    .local p1, "results":Ljava/util/Collection;, "Ljava/util/Collection<TValueT;>;"
    .local p2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<TKeySegmentT;>;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->values:Ljava/util/Set;

    invoke-interface {p1, v2}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 58
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 60
    .local v1, "nextSegment":Ljava/lang/Object;, "TKeySegmentT;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->children:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;

    .line 61
    .local v0, "child":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->get(Ljava/util/Collection;Ljava/util/Iterator;)V

    .line 65
    .end local v0    # "child":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    .end local v1    # "nextSegment":Ljava/lang/Object;, "TKeySegmentT;"
    :cond_0
    return-void
.end method

.method put(Ljava/util/Iterator;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<TKeySegmentT;>;TValueT;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    .local p1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<TKeySegmentT;>;"
    .local p2, "value":Ljava/lang/Object;, "TValueT;"
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 28
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 30
    .local v1, "nextSegment":Ljava/lang/Object;, "TKeySegmentT;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->children:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;

    .line 31
    .local v0, "child":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;

    .end local v0    # "child":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;-><init>()V

    .line 33
    .restart local v0    # "child":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->children:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->put(Ljava/util/Iterator;Ljava/lang/Object;)V

    .line 39
    .end local v0    # "child":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    .end local v1    # "nextSegment":Ljava/lang/Object;, "TKeySegmentT;"
    :goto_0
    return-void

    .line 37
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->values:Ljava/util/Set;

    invoke-interface {v2, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method remove(Ljava/util/Iterator;Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<TKeySegmentT;>;TValueT;)Z"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    .local p1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<TKeySegmentT;>;"
    .local p2, "value":Ljava/lang/Object;, "TValueT;"
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 45
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 46
    .local v1, "nextSegment":Ljava/lang/Object;, "TKeySegmentT;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->children:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;

    .line 47
    .local v0, "child":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->remove(Ljava/util/Iterator;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->children:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    .end local v0    # "child":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node<TKeySegmentT;TValueT;>;"
    .end local v1    # "nextSegment":Ljava/lang/Object;, "TKeySegmentT;"
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->values:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->children:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 51
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->values:Ljava/util/Set;

    invoke-interface {v2, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 53
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
