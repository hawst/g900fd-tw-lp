.class public Lcom/google/apps/dots/android/newsstand/card/ArticleTailEntityItem;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "ArticleTailEntityItem.java"


# static fields
.field public static final DK_SOURCE_BACKGROUND_ID:I

.field public static final DK_SOURCE_CLICKHANDLER:I

.field public static final DK_SOURCE_INITIALS:I

.field public static final DK_SOURCE_NAME:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 14
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ArticleTailEntityItem_sourceName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ArticleTailEntityItem;->DK_SOURCE_NAME:I

    .line 16
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ArticleTailEntityItem_sourceInitials:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ArticleTailEntityItem;->DK_SOURCE_INITIALS:I

    .line 18
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ArticleTailEntityItem_sourceBackgroundId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ArticleTailEntityItem;->DK_SOURCE_BACKGROUND_ID:I

    .line 20
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ArticleTailEntityItem_sourceClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ArticleTailEntityItem;->DK_SOURCE_CLICKHANDLER:I

    .line 22
    new-array v0, v2, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/ArticleTailEntityItem;->EQUALITY_FIELDS:[I

    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->article_tail_entity_item:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/ArticleTailEntityItem;->LAYOUTS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/ArticleTailEntityItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/ArticleTailEntityItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method
