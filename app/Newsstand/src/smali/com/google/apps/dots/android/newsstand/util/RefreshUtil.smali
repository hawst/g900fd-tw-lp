.class public Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;
.super Ljava/lang/Object;
.source "RefreshUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static onPauseTimeMs:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public static getMsSincePause()J
    .locals 4

    .prologue
    .line 35
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->onPauseTimeMs:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static refreshIfNeeded(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 40
    instance-of v0, p2, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    if-eqz v0, :cond_0

    .line 42
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v1

    move-object v0, p2

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    .line 43
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 42
    invoke-virtual {v1, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getAvailable(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;

    invoke-direct {v1, p2, p0}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Landroid/content/Context;)V

    .line 41
    invoke-virtual {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 68
    :cond_0
    return-void
.end method

.method public static refreshIfNeeded(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V
    .locals 2
    .param p0, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "collectionUri"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getAvailable(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 95
    return-void
.end method

.method public static updateOnPauseTime()V
    .locals 2

    .prologue
    .line 28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->onPauseTimeMs:J

    .line 29
    return-void
.end method
