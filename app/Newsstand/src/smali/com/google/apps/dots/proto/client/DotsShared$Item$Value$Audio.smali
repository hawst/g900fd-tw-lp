.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Audio"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;


# instance fields
.field private attachmentId_:Ljava/lang/String;

.field private bitField0_:I

.field private caption_:Ljava/lang/String;

.field private duration_:I

.field private originalUri_:Ljava/lang/String;

.field private thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10992
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10993
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 10998
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->attachmentId_:Ljava/lang/String;

    .line 11020
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->originalUri_:Ljava/lang/String;

    .line 11042
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->duration_:I

    .line 11080
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->caption_:Ljava/lang/String;

    .line 10993
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    .locals 3

    .prologue
    .line 11116
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11120
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_0

    .line 11121
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 11123
    :cond_0
    return-object v0

    .line 11117
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    :catch_0
    move-exception v1

    .line 11118
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 10989
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 11128
    if-ne p1, p0, :cond_1

    .line 11135
    :cond_0
    :goto_0
    return v1

    .line 11129
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 11130
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    .line 11131
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->attachmentId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->attachmentId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->originalUri_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->originalUri_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 11132
    :goto_2
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->duration_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->duration_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_3

    .line 11134
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->caption_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->caption_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 11135
    goto :goto_0

    .line 11131
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->attachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->attachmentId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->originalUri_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->originalUri_:Ljava/lang/String;

    .line 11132
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 11134
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->caption_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->caption_:Ljava/lang/String;

    .line 11135
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getAttachmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11000
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->attachmentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getCaption()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11082
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->caption_:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 11044
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->duration_:I

    return v0
.end method

.method public getOriginalUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11022
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->originalUri_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 11170
    const/4 v0, 0x0

    .line 11171
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 11172
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->attachmentId_:Ljava/lang/String;

    .line 11173
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11175
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 11176
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->originalUri_:Ljava/lang/String;

    .line 11177
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11179
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 11180
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->duration_:I

    .line 11181
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11183
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v1, :cond_3

    .line 11184
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 11185
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11187
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 11188
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->caption_:Ljava/lang/String;

    .line 11189
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11191
    :cond_4
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->cachedSize:I

    .line 11192
    return v0
.end method

.method public getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 11063
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public hasAttachmentId()Z
    .locals 1

    .prologue
    .line 11011
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOriginalUri()Z
    .locals 1

    .prologue
    .line 11033
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasThumbnail()Z
    .locals 1

    .prologue
    .line 11073
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 11139
    const/16 v0, 0x11

    .line 11140
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 11141
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->attachmentId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 11142
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->originalUri_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 11143
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->duration_:I

    add-int v0, v1, v3

    .line 11144
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 11145
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->caption_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_3
    add-int v0, v1, v2

    .line 11146
    return v0

    .line 11141
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->attachmentId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 11142
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->originalUri_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 11144
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v1

    goto :goto_2

    .line 11145
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->caption_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11200
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 11201
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 11205
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 11206
    :sswitch_0
    return-object p0

    .line 11211
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->attachmentId_:Ljava/lang/String;

    .line 11212
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    goto :goto_0

    .line 11216
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->originalUri_:Ljava/lang/String;

    .line 11217
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    goto :goto_0

    .line 11221
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->duration_:I

    .line 11222
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    goto :goto_0

    .line 11226
    :sswitch_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v1, :cond_1

    .line 11227
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 11229
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 11233
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->caption_:Ljava/lang/String;

    .line 11234
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    goto :goto_0

    .line 11201
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 10989
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11151
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 11152
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->attachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 11154
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 11155
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->originalUri_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 11157
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 11158
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->duration_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 11160
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_3

    .line 11161
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 11163
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 11164
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->caption_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 11166
    :cond_4
    return-void
.end method
