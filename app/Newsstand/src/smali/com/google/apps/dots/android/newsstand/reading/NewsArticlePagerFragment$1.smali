.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;
.super Ljava/lang/Object;
.source "NewsArticlePagerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->setupHidingActionBar(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->goUpHierarchy(Landroid/app/Activity;)V

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportFinishAfterTransition()V

    .line 125
    return-void
.end method
