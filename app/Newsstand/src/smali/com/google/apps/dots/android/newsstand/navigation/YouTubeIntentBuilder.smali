.class public Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "YouTubeIntentBuilder.java"


# instance fields
.field private closeOnCompletion:Z

.field private fullscreen:Z

.field private uri:Landroid/net/Uri;

.field private youtubeVideoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v0, 0x1

    .line 21
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 22
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->closeOnCompletion:Z

    .line 23
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->fullscreen:Z

    .line 24
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 55
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->uri:Landroid/net/Uri;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->activity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/player/YouTubeIntents;->canResolvePlayVideoIntent(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->youtubeVideoId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->youtubeVideoId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->fullscreen:Z

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->closeOnCompletion:Z

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/youtube/player/YouTubeIntents;->createPlayVideoIntentWithOptions(Landroid/content/Context;Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    .line 61
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->uri:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 62
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    const-string v1, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public setCloseOnCompletion(Z)Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;
    .locals 0
    .param p1, "closeOnCompletion"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->closeOnCompletion:Z

    .line 48
    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->uri:Landroid/net/Uri;

    .line 28
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "youtube.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->uri:Landroid/net/Uri;

    const-string v1, "v"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->youtubeVideoId:Ljava/lang/String;

    .line 33
    return-object p0

    .line 31
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot play video "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": not YouTube."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setVideoId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;
    .locals 0
    .param p1, "youtubeVideoId"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->youtubeVideoId:Ljava/lang/String;

    .line 43
    return-object p0
.end method
