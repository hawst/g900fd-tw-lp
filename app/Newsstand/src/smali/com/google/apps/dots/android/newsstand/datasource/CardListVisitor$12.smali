.class Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardListVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeWebPageCardData(Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

.field final synthetic val$postTitle:Ljava/lang/String;

.field final synthetic val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .prologue
    .line 890
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;->val$postTitle:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 893
    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getPublisher()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 894
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getTitle()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getWebPageUrl()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->track(Z)V

    .line 896
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 901
    .local v0, "card":Landroid/view/View;
    :goto_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    invoke-direct {v1, p2}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .line 902
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 903
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getWebPageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setPostUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;->val$postTitle:Ljava/lang/String;

    .line 904
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 905
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getPublisher()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setPublisher(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v1

    const/4 v2, 0x0

    .line 906
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v1

    .line 907
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->start()V

    .line 908
    return-void

    .line 896
    .end local v0    # "card":Landroid/view/View;
    :cond_0
    const-class v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->card:I

    .line 897
    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewInParent(Landroid/view/View;Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
