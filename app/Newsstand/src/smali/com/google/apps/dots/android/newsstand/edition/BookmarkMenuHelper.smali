.class public Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;
.super Ljava/lang/Object;
.source "BookmarkMenuHelper.java"


# instance fields
.field private activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field private fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

.field private savedList:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

.field private savedObserver:Lcom/google/android/libraries/bind/data/DataObserver;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 33
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->init()V

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V
    .locals 0
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .line 28
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->init()V

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->onBookmarkChanged()V

    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->savedList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->savedList:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    .line 38
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->savedObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 44
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->savedList:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->savedObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 45
    return-void

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    goto :goto_0
.end method

.method private onBookmarkChanged()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->invalidateOptionsMenu()V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->supportInvalidateOptionsMenu()V

    .line 76
    :cond_1
    return-void
.end method


# virtual methods
.method public onDestroyView()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->savedList:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->savedObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 49
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 53
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v6

    sget-object v7, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v6, v7, :cond_2

    move v0, v4

    .line 58
    .local v0, "isMagazine":Z
    :goto_1
    iget-object v6, p3, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->savedList:Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    .line 59
    invoke-static {p2, v6, v7}, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil;->postSavedState(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/saved/SavedList;)Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;

    move-result-object v1

    .line 60
    .local v1, "postSavedState":Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;
    if-eqz p3, :cond_3

    if-nez v0, :cond_3

    sget-object v6, Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;->NOT_SAVED:Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;

    if-ne v1, v6, :cond_3

    move v2, v4

    .line 62
    .local v2, "showSave":Z
    :goto_2
    if-eqz p3, :cond_4

    if-nez v0, :cond_4

    sget-object v6, Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;->SAVED:Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;

    if-ne v1, v6, :cond_4

    move v3, v4

    .line 65
    .local v3, "showUnSave":Z
    :goto_3
    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->menu_save:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 66
    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->menu_unsave:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .end local v0    # "isMagazine":Z
    .end local v1    # "postSavedState":Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;
    .end local v2    # "showSave":Z
    .end local v3    # "showUnSave":Z
    :cond_2
    move v0, v5

    .line 57
    goto :goto_1

    .restart local v0    # "isMagazine":Z
    .restart local v1    # "postSavedState":Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;
    :cond_3
    move v2, v5

    .line 60
    goto :goto_2

    .restart local v2    # "showSave":Z
    :cond_4
    move v3, v5

    .line 62
    goto :goto_3
.end method
