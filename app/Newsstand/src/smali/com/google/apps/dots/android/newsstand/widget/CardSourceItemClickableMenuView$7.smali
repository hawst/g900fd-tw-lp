.class Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$7;
.super Ljava/lang/Object;
.source "CardSourceItemClickableMenuView.java"

# interfaces
.implements Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->onPreparePopupMenu(Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 3

    .prologue
    .line 290
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    .line 291
    .local v1, "policy":Landroid/os/StrictMode$ThreadPolicy;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->createShareIntent()Landroid/content/Intent;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)Landroid/content/Intent;

    move-result-object v0

    .line 292
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 293
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 295
    :cond_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/StrictModeCompat;->setThreadPolicyDelayed(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 296
    return-void
.end method
