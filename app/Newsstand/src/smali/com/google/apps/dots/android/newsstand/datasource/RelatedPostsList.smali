.class public Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;
.super Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;
.source "RelatedPostsList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList$RelatedPostsCardListVisitor;
    }
.end annotation


# instance fields
.field private final post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

.field private final useDarkTheme:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p3, "useDarkTheme"    # Z

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 33
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->useDarkTheme:Z

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->useDarkTheme:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;)Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    return-object v0
.end method


# virtual methods
.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 45
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v1, v1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getRelatedArticles(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected visitor(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
    .locals 6
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p3, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 39
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList$RelatedPostsCardListVisitor;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->appContext:Landroid/content/Context;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList$RelatedPostsCardListVisitor;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    return-object v0
.end method
