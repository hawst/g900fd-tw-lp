.class public final enum Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;
.super Ljava/lang/Enum;
.source "ProtoEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/util/ProtoEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SyncImageTransform"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

.field public static final enum COVER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

.field public static final enum ORIGINAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

.field public static final enum UNSPECIFIED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

.field public static final enum ZOOMABLE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;


# instance fields
.field public final syncImageTransform:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 232
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    const-string v1, "UNSPECIFIED"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->UNSPECIFIED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    .line 233
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    const-string v1, "ORIGINAL"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    .line 234
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    const-string v1, "ZOOMABLE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->ZOOMABLE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    .line 235
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    const-string v1, "COVER"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->COVER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    .line 231
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->UNSPECIFIED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->ZOOMABLE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->COVER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "syncImageTransform"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 248
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 249
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->syncImageTransform:I

    .line 250
    return-void
.end method

.method public static fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;
    .locals 5
    .param p0, "syncImageTransform"    # I

    .prologue
    .line 240
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 241
    .local v0, "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;
    iget v4, v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->syncImageTransform:I

    if-ne v4, p0, :cond_0

    .line 245
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;
    :goto_1
    return-object v0

    .line 240
    .restart local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 245
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 231
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;
    .locals 1

    .prologue
    .line 231
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    return-object v0
.end method
