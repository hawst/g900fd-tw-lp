.class public Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
.super Ljava/lang/Object;
.source "StoreCache.java"


# instance fields
.field private final cache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
            "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;)V
    .locals 3
    .param p1, "util"    # Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/16 v1, 0x200

    const/16 v2, 0x14

    invoke-virtual {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->scaleForMemoryClass(II)I

    move-result v0

    .line 19
    .local v0, "cacheSizeKb":I
    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache$1;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;I)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->cache:Landroid/support/v4/util/LruCache;

    .line 25
    return-void
.end method

.method private static cacheKey(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "storeRequest"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .prologue
    .line 44
    invoke-virtual {p1, p0}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->getKey(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clear(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "storeRequest"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .prologue
    .line 39
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->cacheKey(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v0

    .line 40
    .local v0, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->cache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    return-void
.end method

.method public get(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "storeRequest"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/accounts/Account;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->cacheKey(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v0

    .line 30
    .local v0, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->cache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    return-object v1
.end method

.method public put(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "storeRequest"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest;",
            "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p3, "item":Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;, "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem<*>;"
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->cacheKey(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v0

    .line 35
    .local v0, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->cache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, v0, p3}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    return-void
.end method

.method public trim(F)V
    .locals 2
    .param p1, "fraction"    # F

    .prologue
    .line 48
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->cache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1}, Landroid/support/v4/util/LruCache;->size()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v0, v1

    .line 49
    .local v0, "newSizeLimit":I
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->cache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/LruCache;->trimToSize(I)V

    .line 50
    return-void
.end method
