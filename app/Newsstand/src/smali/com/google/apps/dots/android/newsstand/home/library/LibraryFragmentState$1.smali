.class final Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState$1;
.super Ljava/lang/Object;
.source "LibraryFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 61
    const-class v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 62
    .local v0, "libraryPage":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)V

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 67
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    move-result-object v0

    return-object v0
.end method
