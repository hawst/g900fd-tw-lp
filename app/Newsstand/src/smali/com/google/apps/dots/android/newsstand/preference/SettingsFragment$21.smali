.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupInternalPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

.field final synthetic val$enabledLabIds:Ljava/util/Set;

.field final synthetic val$labId:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

.field final synthetic val$labPreference:Landroid/preference/CheckBoxPreference;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;Ljava/util/Set;Landroid/preference/CheckBoxPreference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .prologue
    .line 585
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$labId:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$enabledLabIds:Ljava/util/Set;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$labPreference:Landroid/preference/CheckBoxPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 588
    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Toggling labId %s to: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$labId:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 589
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$enabledLabIds:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$labId:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 594
    :goto_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$enabledLabIds:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setEnabledLabIds(Ljava/util/Set;)V

    .line 595
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$labPreference:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$enabledLabIds:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$labId:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 596
    return v4

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$enabledLabIds:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;->val$labId:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
