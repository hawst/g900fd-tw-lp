.class public Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;
.super Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;
.source "ExternalStorageDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$ResultHandler;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private availabilityListener:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;

.field private clicked:Z

.field private finished:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;-><init>()V

    .line 34
    return-void
.end method

.method static synthetic access$000(Landroid/app/Activity;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/app/Activity;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->onFinished(Landroid/app/Activity;Z)V

    return-void
.end method

.method static synthetic access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->finish(Z)V

    return-void
.end method

.method private finish(Z)V
    .locals 1
    .param p1, "available"    # Z

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->finished:Z

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->finished:Z

    .line 85
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->onFinished(Landroid/app/Activity;Z)V

    .line 87
    :cond_0
    return-void
.end method

.method private static onFinished(Landroid/app/Activity;Z)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "available"    # Z

    .prologue
    .line 68
    instance-of v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$ResultHandler;

    if-eqz v0, :cond_0

    .line 69
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$2;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$2;-><init>(Landroid/app/Activity;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 76
    :cond_0
    return-void
.end method

.method public static resolveDiskCacheUnavailability(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 2
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "destroyToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 40
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSSTORE_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$1;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->execute(Ljava/lang/Runnable;)V

    .line 65
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 151
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 152
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Cancel"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 141
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 142
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Clicked"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    const-string v1, "externalStorageDir"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->clicked:Z

    .line 147
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 92
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$3;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$3;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->availabilityListener:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;

    .line 119
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->availabilityListener:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->addAvailabilityListener(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;)V

    .line 120
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 132
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->sd_card_not_available_dialog_title:I

    .line 133
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->sd_card_not_available_dialog_body:I

    .line 134
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->ok:I

    .line 135
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 124
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->onDestroy()V

    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->availabilityListener:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->removeAvailabilityListener(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;)V

    .line 127
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->clicked:Z

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->finish(Z)V

    .line 128
    return-void
.end method
