.class Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$4;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "NewsWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadArticle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 245
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$4;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 248
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->fixNavigatorOnlineInJs()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->access$200(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)V

    .line 249
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/codehaus/jackson/node/ObjectNode;

    .line 250
    .local v0, "jsonStore":Lorg/codehaus/jackson/node/ObjectNode;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v0}, Lorg/codehaus/jackson/node/ObjectNode;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->startArticleLayout(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Ljava/lang/String;)V

    .line 251
    return-void
.end method
