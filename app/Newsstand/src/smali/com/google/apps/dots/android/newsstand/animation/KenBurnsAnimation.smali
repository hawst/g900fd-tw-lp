.class public Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;
.super Ljava/lang/Object;
.source "KenBurnsAnimation.java"


# instance fields
.field private durationMs:J

.field private maxScaleFactor:F

.field private minScaleFactor:F

.field private final random:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->random:Ljava/util/Random;

    .line 12
    const v0, 0x3f99999a    # 1.2f

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->minScaleFactor:F

    .line 13
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->maxScaleFactor:F

    .line 14
    const-wide/16 v0, 0x1770

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->durationMs:J

    .line 16
    return-void
.end method

.method private pickScale()F
    .locals 4

    .prologue
    .line 63
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->minScaleFactor:F

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->random:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->maxScaleFactor:F

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->minScaleFactor:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private pickTranslation(IF)F
    .locals 3
    .param p1, "dimension"    # I
    .param p2, "scale"    # F

    .prologue
    .line 67
    int-to-float v0, p1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, p2, v1

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->random:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    const/high16 v2, 0x3f000000    # 0.5f

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    return v0
.end method

.method private static start(Landroid/view/View;JFFFFFF)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "durationMs"    # J
    .param p3, "startScale"    # F
    .param p4, "endScale"    # F
    .param p5, "startTranslationX"    # F
    .param p6, "startTranslationY"    # F
    .param p7, "endTranslationX"    # F
    .param p8, "endTranslationY"    # F

    .prologue
    .line 79
    invoke-virtual {p0, p3}, Landroid/view/View;->setScaleX(F)V

    .line 80
    invoke-virtual {p0, p3}, Landroid/view/View;->setScaleY(F)V

    .line 81
    invoke-virtual {p0, p5}, Landroid/view/View;->setTranslationX(F)V

    .line 82
    invoke-virtual {p0, p6}, Landroid/view/View;->setTranslationY(F)V

    .line 83
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 84
    invoke-virtual {v0, p7}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 85
    invoke-virtual {v0, p8}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 86
    invoke-virtual {v0, p4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 87
    invoke-virtual {v0, p4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 88
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 89
    return-void
.end method


# virtual methods
.method public animate(Landroid/view/View;)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->pickScale()F

    move-result v4

    .line 47
    .local v4, "startScale":F
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->pickScale()F

    move-result v5

    .line 48
    .local v5, "endScale":F
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-direct {p0, v0, v4}, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->pickTranslation(IF)F

    move-result v6

    .line 49
    .local v6, "startTranslationX":F
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-direct {p0, v0, v4}, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->pickTranslation(IF)F

    move-result v7

    .line 50
    .local v7, "startTranslationY":F
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-direct {p0, v0, v5}, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->pickTranslation(IF)F

    move-result v8

    .line 51
    .local v8, "endTranslationX":F
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-direct {p0, v0, v5}, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->pickTranslation(IF)F

    move-result v9

    .line 52
    .local v9, "endTranslationY":F
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->durationMs:J

    move-object v1, p1

    invoke-static/range {v1 .. v9}, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->start(Landroid/view/View;JFFFFFF)V

    .line 60
    return-void
.end method

.method public setDuration(J)Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;
    .locals 1
    .param p1, "durationMs"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->durationMs:J

    .line 42
    return-object p0
.end method
