.class public abstract Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;
.super Lcom/google/android/libraries/bind/data/DataPagerAdapter;
.source "NSDataPagerAdapter.java"


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/view/ViewHeap;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 1
    .param p1, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter;-><init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V

    .line 13
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSErrorViewProvider;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/data/NSErrorViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)V

    .line 14
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->setLoadingViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)V

    .line 15
    return-void
.end method
