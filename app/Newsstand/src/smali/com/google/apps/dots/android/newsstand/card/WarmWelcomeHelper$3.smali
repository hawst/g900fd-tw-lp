.class Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$3;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "WarmWelcomeHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeExploreOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$3;->this$0:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 124
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage;->EXPLORE_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 125
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v0

    const/4 v1, 0x1

    .line 126
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->start(Z)V

    .line 127
    return-void
.end method
