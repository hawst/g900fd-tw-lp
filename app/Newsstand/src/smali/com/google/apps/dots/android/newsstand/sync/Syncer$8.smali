.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$8;
.super Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Form;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

.field final synthetic val$form:Lcom/google/apps/dots/proto/client/DotsShared$Form;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsShared$Form;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p2, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .param p3, "debugId"    # Ljava/lang/String;
    .param p4, "debugOwner"    # Ljava/lang/Object;

    .prologue
    .line 715
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$8;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$8;->val$form:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public callInternal()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 718
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "%s: syncing deps"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 719
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 721
    .local v0, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$8;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$8;->val$form:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->getPostTemplateId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$8;->val$form:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncFormTemplateBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    invoke-static {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2500(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 722
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$8;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->startTasks(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method
