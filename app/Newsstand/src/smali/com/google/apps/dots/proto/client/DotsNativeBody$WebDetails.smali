.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WebDetails"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;


# instance fields
.field private bitField0_:I

.field private scrollType_:I

.field private transparentBackground_:Z

.field private webFieldId_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2406
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2407
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2417
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->webFieldId_:Ljava/lang/String;

    .line 2439
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->transparentBackground_:Z

    .line 2458
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->scrollType_:I

    .line 2407
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;
    .locals 3

    .prologue
    .line 2489
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2493
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;
    return-object v0

    .line 2490
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;
    :catch_0
    move-exception v1

    .line 2491
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2403
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2498
    if-ne p1, p0, :cond_1

    .line 2501
    :cond_0
    :goto_0
    return v1

    .line 2499
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2500
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    .line 2501
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->webFieldId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->webFieldId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->transparentBackground_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->transparentBackground_:Z

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->scrollType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->scrollType_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->webFieldId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->webFieldId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getScrollType()I
    .locals 1

    .prologue
    .line 2460
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->scrollType_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2530
    const/4 v0, 0x0

    .line 2531
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2532
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->webFieldId_:Ljava/lang/String;

    .line 2533
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2535
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2536
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->transparentBackground_:Z

    .line 2537
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2539
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2540
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->scrollType_:I

    .line 2541
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2543
    :cond_2
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->cachedSize:I

    .line 2544
    return v0
.end method

.method public getTransparentBackground()Z
    .locals 1

    .prologue
    .line 2441
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->transparentBackground_:Z

    return v0
.end method

.method public getWebFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2419
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->webFieldId_:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 2507
    const/16 v0, 0x11

    .line 2508
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 2509
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->webFieldId_:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 2510
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->transparentBackground_:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    add-int v0, v2, v1

    .line 2511
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->scrollType_:I

    add-int v0, v1, v2

    .line 2512
    return v0

    .line 2509
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->webFieldId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 2510
    :cond_1
    const/4 v1, 0x2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2552
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2553
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2557
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2558
    :sswitch_0
    return-object p0

    .line 2563
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->webFieldId_:Ljava/lang/String;

    .line 2564
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    goto :goto_0

    .line 2568
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->transparentBackground_:Z

    .line 2569
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    goto :goto_0

    .line 2573
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2574
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 2576
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->scrollType_:I

    .line 2577
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    goto :goto_0

    .line 2579
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->scrollType_:I

    goto :goto_0

    .line 2553
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2403
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2517
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2518
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->webFieldId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2520
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2521
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->transparentBackground_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2523
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2524
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->scrollType_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2526
    :cond_2
    return-void
.end method
