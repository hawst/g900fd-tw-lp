.class public Lcom/google/apps/dots/android/newsstand/server/ServerUris;
.super Ljava/lang/Object;
.source "ServerUris.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;
    }
.end annotation


# static fields
.field private static final googleAdUrlDomainSuffixes:[Ljava/lang/String;


# instance fields
.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field private final resources:Landroid/content/res/Resources;

.field private final uriMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 85
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ".doubleclick.net"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ".googleadservices.com"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ".googlesyndication.com"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->googleAdUrlDomainSuffixes:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->uriMap:Ljava/util/Map;

    .line 94
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->resources:Landroid/content/res/Resources;

    .line 96
    return-void
.end method

.method private getConfigUrl(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/String;
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "optGcmRegistrationId"    # Ljava/lang/String;
    .param p3, "optOnboardQuizCompleted"    # Ljava/lang/Boolean;

    .prologue
    .line 419
    if-eqz p2, :cond_0

    if-nez p3, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    const-string v2, "Can\'t update both GCM registration ID and onboard quiz completion in same request."

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 421
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v1

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "config"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "cacheNonce"

    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    .line 422
    invoke-virtual {v3}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 423
    .local v0, "builder":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_3

    .line 424
    const-string v1, "gcmRegistrationId"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 428
    :cond_1
    :goto_1
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 419
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 425
    .restart local v0    # "builder":Landroid/net/Uri$Builder;
    :cond_3
    if-eqz p3, :cond_1

    .line 426
    const-string v1, "warmWelcomeCompleted"

    invoke-virtual {p3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1
.end method

.method private getProducerPath(Landroid/accounts/Account;Landroid/content/Context;I)Landroid/net/Uri;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resourceId"    # I

    .prologue
    .line 465
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->producerUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 466
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 467
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getSubscriptionTypePath(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;
    .locals 4
    .param p0, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .prologue
    .line 193
    sget-object v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$1;->$SwitchMap$com$google$apps$dots$android$newsstand$util$ProtoEnum$EditionType:[I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 201
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x47

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Attempted to get subscription path for EditionType "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", should not happen."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :pswitch_0
    const-string v0, "news_v2"

    .line 199
    :goto_0
    return-object v0

    .line 197
    :pswitch_1
    const-string v0, "curations"

    goto :goto_0

    .line 199
    :pswitch_2
    const-string v0, "magazines"

    goto :goto_0

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 99
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->uriMap:Ljava/util/Map;

    monitor-enter v2

    .line 100
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->uriMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    .line 101
    .local v0, "uris":Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;
    if-nez v0, :cond_0

    .line 102
    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    .end local v0    # "uris":Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->resources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-direct {v0, v1, v3, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;-><init>(Landroid/content/res/Resources;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Landroid/accounts/Account;)V

    .line 103
    .restart local v0    # "uris":Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->uriMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    :cond_0
    monitor-exit v2

    return-object v0

    .line 106
    .end local v0    # "uris":Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isGoogleAdHost(Ljava/lang/String;)Z
    .locals 6
    .param p1, "authority"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 471
    if-eqz p1, :cond_0

    .line 472
    sget-object v3, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->googleAdUrlDomainSuffixes:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 473
    .local v0, "domainSuffix":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 474
    const/4 v1, 0x1

    .line 478
    .end local v0    # "domainSuffix":Ljava/lang/String;
    :cond_0
    return v1

    .line 472
    .restart local v0    # "domainSuffix":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getAnalyticsUrl(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 377
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "analytics"

    .line 378
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 379
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppOffers(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "appId"    # Ljava/lang/String;

    .prologue
    .line 296
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "offers"

    .line 297
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 298
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 299
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppReadStatesUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "appId"    # Ljava/lang/String;

    .prologue
    .line 356
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "read-states"

    .line 357
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 358
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 359
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppSectionsCollection(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "appId"    # Ljava/lang/String;

    .prologue
    .line 289
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "editions"

    .line 290
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 291
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 292
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getArchiveMagazineUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "appId"    # Ljava/lang/String;

    .prologue
    .line 371
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "magazines/archive"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 372
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBaseUri(Landroid/accounts/Account;)Landroid/net/Uri;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->baseUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getConfigUrl(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v0, 0x0

    .line 399
    invoke-direct {p0, p1, v0, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getConfigUrl(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConfigUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "gcmRegistrationId"    # Ljava/lang/String;

    .prologue
    .line 406
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getConfigUrl(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConfigUrl(Landroid/accounts/Account;Z)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "onboardQuizCompleted"    # Z

    .prologue
    .line 414
    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getConfigUrl(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurationEditions(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "appId"    # Ljava/lang/String;

    .prologue
    .line 349
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "curation-editions"

    .line 350
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 351
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 352
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultSubscriptions(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 229
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "default-subscriptions"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEditionSearchResults(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 266
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->searchUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "news"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "q"

    .line 267
    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntitySearchResults(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 281
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->searchUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "entities"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "q"

    .line 282
    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExploreFeatured(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 252
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "featured"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExploreInternationalCurations(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "topicId"    # Ljava/lang/String;

    .prologue
    .line 220
    .line 221
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getExploreTopics(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "international"

    .line 223
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 224
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 225
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExploreSingleTopic(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "topicId"    # Ljava/lang/String;

    .prologue
    .line 216
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getExploreTopics(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExploreTopics(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "explore-topics"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFeedSearchResults(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 261
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->searchUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "feeds"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "q"

    .line 262
    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGcmAckUrl(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "gcmMessageId"    # Ljava/lang/String;
    .param p3, "gcmRegId"    # Ljava/lang/String;

    .prologue
    .line 437
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "gcm-received"

    .line 438
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "gcmMessageId"

    .line 439
    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "gcmRegistrationId"

    .line 440
    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 441
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGooglePrivacyPolicyUri(Landroid/accounts/Account;Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 461
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->google_privacy_policy_url:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getProducerPath(Landroid/accounts/Account;Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getLayoutResourceUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "filename"    # Ljava/lang/String;

    .prologue
    .line 389
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->resourceUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "layouts/v4/android"

    .line 390
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 391
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 392
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLayoutResourcesUrl(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 383
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->resourceUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "layouts/v4"

    .line 384
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 385
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMagazineCollection(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "appId"    # Ljava/lang/String;

    .prologue
    .line 342
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "magazines"

    .line 343
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 344
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 345
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMagazineSearchResults(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 271
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->searchUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "magazines"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "q"

    .line 272
    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getManageSubscriptionsUri(Landroid/accounts/Account;Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 449
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->manage_subscriptions_uri:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getProducerPath(Landroid/accounts/Account;Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getMyCurations(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 185
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMySubscriptions(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 189
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMySubscriptions(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMyNews(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 181
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->NEWS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMySubscriptions(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMyOffers(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "offers/promo"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMySubscriptions(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getSubscriptionTypePath(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOfferForOfferId(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "offerId"    # Ljava/lang/String;

    .prologue
    .line 238
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "offers/promo"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 239
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPersonalResourceUri(Landroid/accounts/Account;)Landroid/net/Uri;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalResourceUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getPostReadStateUrl(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "appId"    # Ljava/lang/String;
    .param p3, "postId"    # Ljava/lang/String;

    .prologue
    .line 363
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "read-states"

    .line 364
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 365
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 366
    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 367
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPostSearchResults(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 276
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->searchUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "posts"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "q"

    .line 277
    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReadNow(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "read-now"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRelatedArticles(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "postId"    # Ljava/lang/String;

    .prologue
    .line 256
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "related/posts/"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 257
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRelativeAnalyticsEventUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 433
    const-string v0, "event"

    return-object v0
.end method

.method public getResourceUri(Landroid/accounts/Account;)Landroid/net/Uri;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->resourceUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getSaved(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "saved"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSavedPost(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "postId"    # Ljava/lang/String;

    .prologue
    .line 247
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "saved"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 248
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSectionCollection(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "sectionId"    # Ljava/lang/String;

    .prologue
    .line 332
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "sections"

    .line 333
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 334
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 335
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTermsOfServiceUri(Landroid/accounts/Account;Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 457
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->terms_of_service_url:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getProducerPath(Landroid/accounts/Account;Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getWebviewBaseUrl(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 445
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->webviewBaseUrl:Ljava/lang/String;

    return-object v0
.end method

.method public isDotsBackend(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 145
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "path":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isGoogleBackend(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const-string v1, "/newsstand"

    .line 148
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "/producer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDotsBackend(Ljava/lang/String;)Z
    .locals 1
    .param p1, "uriString"    # Ljava/lang/String;

    .prologue
    .line 152
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isDotsBackend(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public isGoogleAdHost(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 156
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isGoogleAdHost(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isGoogleAdHost(Ljava/net/URI;)Z
    .locals 1
    .param p1, "uri"    # Ljava/net/URI;

    .prologue
    .line 160
    invoke-virtual {p1}, Ljava/net/URI;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isGoogleAdHost(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isGoogleBackend(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 141
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isGoogleHost(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isGucHost(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGoogleHost(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 130
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "host":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "google.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isGucHost(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 135
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "host":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "googleusercontent.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public qualifyRelativeSyncUri(Landroid/accounts/Account;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 164
    invoke-virtual {p2}, Landroid/net/Uri;->isRelative()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v1

    iget-object v0, v1, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->baseUri:Landroid/net/Uri;

    .line 166
    .local v0, "baseUri":Landroid/net/Uri;
    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 167
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 168
    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 169
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p2

    .line 171
    .end local v0    # "baseUri":Landroid/net/Uri;
    .end local p2    # "uri":Landroid/net/Uri;
    :cond_0
    return-object p2
.end method

.method public subscribe(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    .param p3, "appId"    # Ljava/lang/String;

    .prologue
    .line 303
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 304
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getSubscriptionTypePath(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 305
    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public subscriptionReorder(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    .param p3, "moveAppFamilyId"    # Ljava/lang/String;
    .param p4, "pivotAppFamilyId"    # Ljava/lang/String;

    .prologue
    .line 311
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v1

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 312
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getSubscriptionTypePath(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "reorder"

    .line 313
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 314
    invoke-virtual {v1, p3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 315
    .local v0, "builder":Landroid/net/Uri$Builder;
    if-eqz p4, :cond_0

    .line 316
    const-string v1, "pivotId"

    invoke-virtual {v0, v1, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 318
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public subscriptionTranslate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    .param p3, "appId"    # Ljava/lang/String;
    .param p4, "translationCode"    # Ljava/lang/String;

    .prologue
    .line 323
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->NEWS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne p2, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 324
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getUris(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;

    move-result-object v1

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/server/ServerUris$Uris;->personalUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 325
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getSubscriptionTypePath(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 326
    invoke-virtual {v1, p3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "translate"

    .line 327
    invoke-virtual {v1, v2, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 328
    .local v0, "builder":Landroid/net/Uri$Builder;
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 323
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
