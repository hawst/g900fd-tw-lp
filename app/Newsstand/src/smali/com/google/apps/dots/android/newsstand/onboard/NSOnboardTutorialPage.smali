.class public Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardTutorialPage;
.super Lcom/google/android/play/onboard/OnboardTutorialPage;
.source "NSOnboardTutorialPage.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardTutorialPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardTutorialPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/onboard/OnboardTutorialPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method


# virtual methods
.method public onEnterPage(Z)V
    .locals 3
    .param p1, "movingTowardsEnd"    # Z

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/google/android/play/onboard/OnboardTutorialPage;->onEnterPage(Z)V

    .line 31
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardTutorialPage;->getPageInfo()Lcom/google/android/play/onboard/OnboardPageInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardTutorialPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-interface {v1, v2}, Lcom/google/android/play/onboard/OnboardPageInfo;->getGroupPageIndex(Lcom/google/android/play/onboard/OnboardHostControl;)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;-><init>(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardTutorialScreen;->track(Z)V

    .line 32
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->flushAnalyticsEvents()V

    .line 33
    return-void
.end method
