.class public Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;
.super Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;
.source "CardSplashItem.java"


# static fields
.field public static final DK_HINT_TEXT:I

.field public static final DK_SOURCE_ASPECT_RATIO:I

.field public static final DK_SOURCE_ICON_ID:I

.field public static final DK_SOURCE_NAME:I

.field public static final DK_SPLASH_ATTACHMENT_ID:I

.field public static final DK_SPLASH_ON_CLICK_LISTENER:I

.field public static final DK_TIME:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I

.field public static final LAYOUTS_COMPACT:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSplashItem_splashAttachmentId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_SPLASH_ATTACHMENT_ID:I

    .line 21
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSplashItem_sourceIconId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_SOURCE_ICON_ID:I

    .line 23
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSplashItem_sourceName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_SOURCE_NAME:I

    .line 25
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSplashItem_sourceAspectRatio:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_SOURCE_ASPECT_RATIO:I

    .line 27
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSplashItem_hintText:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_HINT_TEXT:I

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSplashItem_time:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_TIME:I

    .line 31
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSplashItem_splashOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_SPLASH_ON_CLICK_LISTENER:I

    .line 33
    new-array v0, v2, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->EQUALITY_FIELDS:[I

    .line 35
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_splash_item_magazine:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->LAYOUTS:[I

    .line 36
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_splash_item_magazine_compact:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->LAYOUTS_COMPACT:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method


# virtual methods
.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 3
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 54
    if-nez p1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->primary_image:I

    .line 59
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 60
    .local v0, "imageView":Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    if-eqz v0, :cond_0

    .line 62
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_2

    .line 64
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$dimen;->action_bar_default_elevation:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    .line 63
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setElevation(F)V

    goto :goto_0

    .line 66
    :cond_2
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem$1;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem$1;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setRunWhenImageSet(Ljava/lang/Runnable;)V

    .line 72
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem$2;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem$2;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setRunWhenBitmapReleased(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
