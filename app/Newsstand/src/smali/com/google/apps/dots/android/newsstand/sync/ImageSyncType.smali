.class public final enum Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;
.super Ljava/lang/Enum;
.source "ImageSyncType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

.field public static final enum ALL_IMAGES:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

.field public static DEFAULT:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

.field public static final enum IMAGES_IN_BG:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

.field public static final enum NO_IMAGES:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

.field public static final enum PRIMARY_IMAGES:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;


# instance fields
.field private final bgRestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

.field private final bgUnrestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

.field private final fgRestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

.field private final fgUnrestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

.field private final optionResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x3

    const/4 v14, 0x2

    const v4, 0x3000b

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    const-string v1, "IMAGES_IN_BG"

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->image_sync_type_images_in_bg:I

    move v6, v4

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->IMAGES_IN_BG:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    .line 21
    new-instance v6, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    const-string v7, "NO_IMAGES"

    sget v9, Lcom/google/android/apps/newsstanddev/R$string;->image_sync_type_no_images:I

    const v10, 0x30001

    const v12, 0x30001

    move v8, v5

    move v11, v5

    move v13, v2

    invoke-direct/range {v6 .. v13}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v6, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->NO_IMAGES:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    .line 26
    new-instance v6, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    const-string v7, "PRIMARY_IMAGES"

    sget v9, Lcom/google/android/apps/newsstanddev/R$string;->image_sync_type_primary_images:I

    const v10, 0x30003

    const v12, 0x30003

    move v8, v14

    move v11, v5

    move v13, v2

    invoke-direct/range {v6 .. v13}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v6, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->PRIMARY_IMAGES:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    .line 31
    new-instance v6, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    const-string v7, "ALL_IMAGES"

    sget v9, Lcom/google/android/apps/newsstanddev/R$string;->image_sync_type_all_images:I

    move v8, v15

    move v10, v4

    move v11, v5

    move v12, v4

    move v13, v2

    invoke-direct/range {v6 .. v13}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v6, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->ALL_IMAGES:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->IMAGES_IN_BG:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->NO_IMAGES:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->PRIMARY_IMAGES:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    aput-object v1, v0, v14

    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->ALL_IMAGES:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    aput-object v1, v0, v15

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    .line 34
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->IMAGES_IN_BG:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 1
    .param p3, "optionResId"    # I
    .param p4, "fgUnrestricted"    # I
    .param p5, "fgRestricted"    # I
    .param p6, "bgUnrestricted"    # I
    .param p7, "bgRestricted"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->optionResId:I

    .line 57
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    invoke-direct {v0, p4}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;-><init>(I)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->fgUnrestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    .line 58
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    invoke-direct {v0, p5}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;-><init>(I)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->fgRestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    .line 59
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    invoke-direct {v0, p6}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;-><init>(I)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->bgUnrestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    .line 60
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    invoke-direct {v0, p7}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;-><init>(I)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->bgRestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    .line 61
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 65
    :try_start_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 67
    :goto_0
    return-object v1

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    return-object v0
.end method


# virtual methods
.method public getOptionStringResId()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->optionResId:I

    return v0
.end method

.method public getPolicy(ZZ)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;
    .locals 1
    .param p1, "foreground"    # Z
    .param p2, "restricted"    # Z

    .prologue
    .line 72
    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->fgRestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->fgUnrestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->bgRestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->bgUnrestricted:Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    goto :goto_0
.end method
