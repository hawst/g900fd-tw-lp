.class Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;
.super Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;
.source "AttachmentStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->retryBitmapDecodeWithGC(Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/util/RetryWithGC",
        "<",
        "Landroid/graphics/Bitmap;",
        "Ljava/io/IOException;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

.field final synthetic val$diskBlob:Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

.field final synthetic val$inputStreamBuffer:[B

.field final synthetic val$options:Landroid/graphics/BitmapFactory$Options;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;[BLandroid/graphics/BitmapFactory$Options;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;->val$diskBlob:Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;->val$inputStreamBuffer:[B

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;->val$options:Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;-><init>()V

    return-void
.end method


# virtual methods
.method protected work()Landroid/graphics/Bitmap;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    new-instance v1, Lcom/google/apps/dots/android/newsstand/io/PoolBufferedInputStream;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;->val$diskBlob:Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    .line 124
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->createInputStream()Ljava/io/InputStream;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;->val$inputStreamBuffer:[B

    invoke-direct {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/io/PoolBufferedInputStream;-><init>(Ljava/io/InputStream;[B)V

    .line 125
    .local v1, "in":Ljava/io/InputStream;
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;->val$options:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 126
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 127
    sget-object v2, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "decodeStream returned null"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    :cond_0
    return-object v0
.end method

.method protected bridge synthetic work()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$2;->work()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
