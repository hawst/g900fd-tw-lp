.class Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$7;
.super Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;
.source "EditableLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->setupListView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-void
.end method


# virtual methods
.method public getErrorMessageData()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    .line 224
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .line 225
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->access$400(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getRetryRunnable()Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->access$500(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)Ljava/lang/Runnable;

    move-result-object v3

    .line 224
    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method
