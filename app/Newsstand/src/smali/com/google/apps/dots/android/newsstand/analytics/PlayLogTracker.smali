.class public Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;
.super Ljava/lang/Object;
.source "PlayLogTracker.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker$PlayLogStoppingReceiver;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private account:Landroid/accounts/Account;

.field private final appContext:Landroid/content/Context;

.field private isRegistered:Z

.field private lastScreenSentToPlayLog:Ljava/lang/String;

.field private playLogger:Lcom/google/android/gms/playlog/PlayLogger;

.field private final receiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->appContext:Landroid/content/Context;

    .line 38
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker$PlayLogStoppingReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker$PlayLogStoppingReceiver;-><init>(Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker$1;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->receiver:Landroid/content/BroadcastReceiver;

    .line 39
    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->unregisterIfNeededInternal()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->unregisterIfNeeded()V

    return-void
.end method

.method private changeAccountIfNeeded(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "eventAccount"    # Landroid/accounts/Account;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->account:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->playLogger:Lcom/google/android/gms/playlog/PlayLogger;

    if-eqz v0, :cond_0

    .line 155
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->unregisterIfNeededInternal()V

    .line 154
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->appContext:Landroid/content/Context;

    invoke-direct {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->setupPlayLogger(Landroid/content/Context;Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method private registerIfNeeded()V
    .locals 4

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->isRegistered:Z

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->playLogger:Lcom/google/android/gms/playlog/PlayLogger;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/PlayLogger;->start()V

    .line 137
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->appContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->receiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.apps.dots.android.newsstand.NSApplication.action.APPLICATION_VISIBLE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->isRegistered:Z

    .line 141
    :cond_0
    return-void
.end method

.method private sendAppScreenView(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)V
    .locals 6
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "eventDriven"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 81
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    .line 83
    .local v1, "tagBuilder":Landroid/net/Uri$Builder;
    const-string v2, "Screen"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getScreen()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 87
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "tag":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->playLogger:Lcom/google/android/gms/playlog/PlayLogger;

    new-array v3, v5, [Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/playlog/PlayLogger;->logEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 89
    sget-object v3, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    if-eqz p2, :cond_0

    const-string v2, "VIEW FROM EVENT to PlayLog: [tag: %s]"

    :goto_0
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v3, v2, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getScreen()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->lastScreenSentToPlayLog:Ljava/lang/String;

    .line 93
    return-void

    .line 89
    :cond_0
    const-string v2, "VIEW to PlayLog: [tag: %s]"

    goto :goto_0
.end method

.method private sendEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V
    .locals 7
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 98
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    .line 100
    .local v1, "tagBuilder":Landroid/net/Uri$Builder;
    const-string v2, "Screen"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getScreen()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "Category"

    .line 101
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getCategory()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "Action"

    .line 102
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 104
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 105
    const-string v2, "Label"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 110
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "tag":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->playLogger:Lcom/google/android/gms/playlog/PlayLogger;

    new-array v3, v5, [Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/playlog/PlayLogger;->logEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 112
    sget-object v2, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "EVENT to PlayLog: [tag: \'%s\']"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    return-void
.end method

.method private setupPlayLogger(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v1, 0x0

    .line 47
    if-nez p2, :cond_0

    move-object v0, v1

    .line 48
    .local v0, "accountName":Ljava/lang/String;
    :goto_0
    new-instance v2, Lcom/google/android/gms/playlog/PlayLogger;

    const/4 v3, 0x4

    invoke-direct {v2, p1, v3, v0, v1}, Lcom/google/android/gms/playlog/PlayLogger;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/google/android/gms/playlog/PlayLogger$LoggerCallbacks;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->playLogger:Lcom/google/android/gms/playlog/PlayLogger;

    .line 49
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->account:Landroid/accounts/Account;

    .line 50
    return-void

    .line 47
    .end local v0    # "accountName":Ljava/lang/String;
    :cond_0
    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method private unregisterIfNeeded()V
    .locals 2

    .prologue
    .line 116
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker$1;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->ANALYTICS:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker$1;-><init>(Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    .line 121
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker$1;->execute()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 122
    return-void
.end method

.method private unregisterIfNeededInternal()V
    .locals 2

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->isRegistered:Z

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->playLogger:Lcom/google/android/gms/playlog/PlayLogger;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/PlayLogger;->stop()V

    .line 128
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->appContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->isRegistered:Z

    .line 131
    :cond_0
    return-void
.end method


# virtual methods
.method public supportsAccountlessEvents()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method public trackEvent(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "trackingId"    # Ljava/lang/String;
    .param p3, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->changeAccountIfNeeded(Landroid/accounts/Account;)V

    .line 55
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->registerIfNeeded()V

    .line 58
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getScreen()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getCategory()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 66
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, p3, v0}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->sendAppScreenView(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)V

    goto :goto_0

    .line 71
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->lastScreenSentToPlayLog:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->lastScreenSentToPlayLog:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getScreen()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 72
    :cond_4
    const/4 v0, 0x1

    invoke-direct {p0, p3, v0}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->sendAppScreenView(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)V

    .line 74
    :cond_5
    invoke-direct {p0, p3}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;->sendEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V

    goto :goto_0
.end method
