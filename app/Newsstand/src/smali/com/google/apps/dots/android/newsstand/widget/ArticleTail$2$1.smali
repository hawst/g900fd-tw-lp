.class Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;
.super Ljava/lang/Object;
.source "ArticleTail.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;

.field final synthetic val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

.field final synthetic val$useDarkTheme:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;->val$useDarkTheme:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;->val$owningEdition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;->val$useDarkTheme:Z

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->updateCardsList(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->access$100(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V

    .line 114
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->staticListView:Lcom/google/apps/dots/android/newsstand/widget/StaticListView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->access$000(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;)Lcom/google/apps/dots/android/newsstand/widget/StaticListView;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$2$1;->val$useDarkTheme:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/newsstanddev/R$color;->card_list_view_bg_dark:I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->setBackgroundResource(I)V

    .line 117
    return-void

    .line 114
    :cond_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$color;->card_list_view_bg_light:I

    goto :goto_0
.end method
