.class public Lcom/google/apps/dots/android/newsstand/http/HttpConstants;
.super Ljava/lang/Object;
.source "HttpConstants.java"


# static fields
.field public static final ACCOUNT_CONTEXT_KEY:Ljava/lang/String; = "account"

.field public static final ANDROID_ID_HEADER:Ljava/lang/String; = "X-AndroidId"

.field public static final AUTHORIZATION_HEADER:Ljava/lang/String; = "Authorization"

.field public static final CACHE_CONTROL_HEADER:Ljava/lang/String; = "Cache-Control"

.field public static final CLIENT_REQUEST_CONTEXT_VALUE:Ljava/lang/String; = "client"

.field public static final CONTENT_ENCODING_HEADER:Ljava/lang/String; = "Content-Encoding"

.field public static final CONTENT_TYPE_HEADER:Ljava/lang/String; = "Content-Type"

.field public static final COUNTRY_OVERRIDE_HEADER:Ljava/lang/String; = "X-CountryOverride"

.field public static final DATE_HEADER:Ljava/lang/String; = "Date"

.field public static final DOTS_BACKGROUND_SYNC_HEADER:Ljava/lang/String; = "X-Dots-Background-Sync"

.field public static final DOTS_DEVICE_BUILD_HEADER:Ljava/lang/String; = "X-Dots-Device-Build"

.field public static final DOTS_DEVICE_HEADER:Ljava/lang/String; = "X-Dots-Device"

.field public static final DOTS_DEVICE_ID_HEADER:Ljava/lang/String; = "X-Dots-Device-ID"

.field public static final DOTS_UPGRADE_REQUIRED_HEADER:Ljava/lang/String; = "X-Dots-Upgrade-Required"

.field public static final DOTS_UPGRADE_VERSION_HEADER:Ljava/lang/String; = "X-Dots-Upgrade-Version"

.field public static final ETAG_HEADER:Ljava/lang/String; = "ETag"

.field public static final EXPIRES_HEADER:Ljava/lang/String; = "Expires"

.field public static final IF_MODIFIED_SINCE_HEADER:Ljava/lang/String; = "If-Modified-Since"

.field public static final IF_NONE_MATCH_HEADER:Ljava/lang/String; = "If-None-Match"

.field public static final LAST_MODIFIED_HEADER:Ljava/lang/String; = "Last-Modified"

.field public static final LOCALE_HEADER:Ljava/lang/String; = "Accept-Language"

.field public static final LOCATION_HEADER:Ljava/lang/String; = "Location"

.field public static final OCTET_STREAM_TYPE:Ljava/lang/String; = "application/octet-stream"

.field public static final REQUEST_CONTEXT_KEY:Ljava/lang/String; = "request"

.field public static final USER_AGENT_HEADER:Ljava/lang/String; = "User-Agent"

.field public static final WEBVIEW_REQUEST_CONTEXT_VALUE:Ljava/lang/String; = "webview"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
