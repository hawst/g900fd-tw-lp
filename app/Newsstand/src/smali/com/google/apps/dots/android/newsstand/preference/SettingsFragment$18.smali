.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$18;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupInternalPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

.field final synthetic val$entries:[Ljava/lang/CharSequence;

.field final synthetic val$values:[Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;[Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .prologue
    .line 523
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$18;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$18;->val$values:[Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$18;->val$entries:[Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 527
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$18;->val$values:[Ljava/lang/CharSequence;

    invoke-static {v1, p2}, Lcom/google/apps/dots/android/newsstand/util/ArrayUtil;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 528
    .local v0, "index":I
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$18;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setShowOnboardQuizPreference(I)V

    .line 529
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$18;->val$entries:[Ljava/lang/CharSequence;

    aget-object v1, v1, v0

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 530
    const/4 v1, 0x1

    return v1
.end method
