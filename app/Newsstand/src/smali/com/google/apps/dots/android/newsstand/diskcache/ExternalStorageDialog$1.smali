.class final Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$1;
.super Ljava/lang/Object;
.source "ExternalStorageDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->resolveDiskCacheUnavailability(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/support/v4/app/FragmentActivity;

.field final synthetic val$destroyToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$1;->val$destroyToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 43
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    const-string v2, "externalStorageDir"

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->onFinished(Landroid/app/Activity;Z)V
    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->access$000(Landroid/app/Activity;Z)V

    .line 63
    :goto_0
    return-void

    .line 48
    :cond_0
    :try_start_0
    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Using external storage. Verifying DiskCache open..."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->open(Z)V

    .line 50
    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "...success"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    const/4 v2, 0x1

    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->onFinished(Landroid/app/Activity;Z)V
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->access$000(Landroid/app/Activity;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "ioe":Ljava/io/IOException;
    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Opening DiskCache failed, showing dialog."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$1;->val$destroyToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$1$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$1;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
