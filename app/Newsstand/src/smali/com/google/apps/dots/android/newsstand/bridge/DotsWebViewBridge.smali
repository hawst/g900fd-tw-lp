.class public Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;
.super Ljava/lang/Object;
.source "DotsWebViewBridge.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field protected activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field protected final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field protected dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

.field private final eventHandlers:Lcom/google/common/collect/Multimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

.field protected readingActivity:Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

.field protected final viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 1
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "dotsWebView"    # Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "readingActivity"    # Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;
    .param p5, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
    .param p6, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 64
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    .line 65
    invoke-static {}, Lcom/google/common/collect/HashMultimap;->create()Lcom/google/common/collect/HashMultimap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->eventHandlers:Lcom/google/common/collect/Multimap;

    .line 66
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 67
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->readingActivity:Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    .line 68
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    .line 69
    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 70
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public static buildCallbackJavaScript(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;
    .locals 4
    .param p0, "callbackKey"    # Ljava/lang/String;
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 89
    const-string v0, "dots.store.processCallback(\'%s\', %s);"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bind(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    if-nez v0, :cond_0

    .line 340
    :goto_0
    return-void

    .line 335
    :cond_0
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 336
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "bind - unspecified eventName"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->eventHandlers:Lcom/google/common/collect/Multimap;

    invoke-interface {v0, p1, p2}, Lcom/google/common/collect/Multimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clearReferences()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 73
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 74
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->eventHandlers:Lcom/google/common/collect/Multimap;

    invoke-interface {v0}, Lcom/google/common/collect/Multimap;->clear()V

    .line 76
    return-void
.end method

.method public endTiming(Ljava/lang/String;)V
    .locals 0
    .param p1, "methodName"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 495
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->asyncTraceEnd(Ljava/lang/String;)V

    .line 496
    return-void
.end method

.method protected varargs executeStatements([Ljava/lang/String;)V
    .locals 1
    .param p1, "statements"    # [Ljava/lang/String;

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    if-eqz v0, :cond_0

    .line 500
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->executeStatements([Ljava/lang/String;)V

    .line 502
    :cond_0
    return-void
.end method

.method public getPostsForSectionAsync(Ljava/lang/String;IZLjava/lang/String;)V
    .locals 4
    .param p1, "sectionId"    # Ljava/lang/String;
    .param p2, "max"    # I
    .param p3, "requireImages"    # Z
    .param p4, "callbackKey"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 427
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->getAppId()Ljava/lang/String;

    move-result-object v0

    .line 428
    .local v0, "originalAppId":Ljava/lang/String;
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 429
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "getPostsForSectionAsync - unspecified sectionId"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 486
    :cond_0
    :goto_0
    return-void

    .line 432
    :cond_1
    if-gez p2, :cond_2

    .line 433
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "getPostsForSectionAsync - invalid max"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 436
    :cond_2
    invoke-static {p4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 437
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "getPostsForSectionAsync - unspecified callbackKey"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getSectionsAsync(Ljava/lang/String;)V
    .locals 0
    .param p1, "callbackKey"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 346
    return-void
.end method

.method public gotoPage(I)V
    .locals 5
    .param p1, "pageNumber"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 117
    if-gez p1, :cond_0

    .line 118
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "gotoPage - invalid pageNumber: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    :goto_0
    return-void

    .line 121
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$3;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$3;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;I)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->safePostIfAllowed(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public isFullScreen()Z
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 322
    const/4 v0, 0x0

    return v0
.end method

.method protected isPostAllowed()Z
    .locals 1

    .prologue
    .line 535
    const/4 v0, 0x1

    return v0
.end method

.method public notify(Ljava/lang/String;)V
    .locals 7
    .param p1, "eventName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 543
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 544
    sget-object v2, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "notify - unspecified eventName"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 555
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->eventHandlers:Lcom/google/common/collect/Multimap;

    invoke-interface {v2, p1}, Lcom/google/common/collect/Multimap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 548
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->eventHandlers:Lcom/google/common/collect/Multimap;

    invoke-interface {v2, p1}, Lcom/google/common/collect/Multimap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    .line 550
    .local v1, "statements":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :try_start_0
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->executeStatements([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 551
    :catch_0
    move-exception v0

    .line 552
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Exception executing javascript: "

    const-string v2, ""

    invoke-static {v2}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-virtual {v3, v2, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public notifyIframeUnload()V
    .locals 0
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 328
    return-void
.end method

.method public onLayoutChange(IZII)V
    .locals 6
    .param p1, "pageCount"    # I
    .param p2, "isDone"    # Z
    .param p3, "pageWidth"    # I
    .param p4, "pageHeight"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 95
    if-gez p1, :cond_0

    .line 96
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onLayoutChange - invalid pageNumber: %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    :goto_0
    return-void

    .line 99
    :cond_0
    if-gez p3, :cond_1

    .line 100
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onLayoutChange - invalid pageWidth: %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 103
    :cond_1
    if-gez p4, :cond_2

    .line 104
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onLayoutChange - invalid pageHeight: %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 107
    :cond_2
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$2;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$2;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;IZII)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->safePost(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected onPostNotAllowed()V
    .locals 0

    .prologue
    .line 540
    return-void
.end method

.method public onScriptLoad()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 80
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$1;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 86
    return-void
.end method

.method public openAudio(Ljava/lang/String;)V
    .locals 0
    .param p1, "audioUri"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 253
    return-void
.end method

.method public openDrawer(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "fieldId"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .param p3, "restrictToSingleField"    # Z
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    if-nez v0, :cond_0

    .line 236
    :cond_0
    return-void
.end method

.method public openOriginalUrl(Ljava/lang/String;)V
    .locals 3
    .param p1, "postId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    if-nez v0, :cond_0

    .line 227
    :goto_0
    return-void

    .line 209
    :cond_0
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "openOriginalUrl - unspecified postId"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 213
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$5;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$5;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;)V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public openPlayStoreDoc(ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "docTypeValue"    # I
    .param p2, "backendDocId"    # Ljava/lang/String;
    .param p3, "parentBackendDocId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 303
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/DocType;->forProtoValue(I)Lcom/google/apps/dots/android/newsstand/util/DocType;

    move-result-object v0

    .line 304
    .local v0, "docType":Lcom/google/apps/dots/android/newsstand/util/DocType;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/DocType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/DocType;

    if-eq v0, v1, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Unknown doc type value %s"

    new-array v2, v2, [Ljava/lang/Object;

    .line 305
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    .line 304
    invoke-static {v1, v4, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 306
    new-instance v1, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$9;

    invoke-direct {v1, p0, v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$9;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Lcom/google/apps/dots/android/newsstand/util/DocType;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->safePostIfAllowed(Ljava/lang/Runnable;)V

    .line 315
    return-void

    :cond_0
    move v1, v3

    .line 304
    goto :goto_0
.end method

.method public pauseAudio()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 257
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$7;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$7;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->safePostIfAllowed(Ljava/lang/Runnable;)V

    .line 263
    return-void
.end method

.method public requestPurchase(Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 281
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$8;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$8;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->safePostIfAllowed(Ljava/lang/Runnable;)V

    .line 290
    return-void
.end method

.method public requestPurchaseInContext(IILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "backendId"    # I
    .param p2, "docTypeValue"    # I
    .param p3, "backendDocId"    # Ljava/lang/String;
    .param p4, "fullDocId"    # Ljava/lang/String;
    .param p5, "offerType"    # I
    .param p6, "parentBackendDocId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 297
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Deprecated method requestPurchaseInContext() called on DotsWebViewBridge."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    return-void
.end method

.method public resetReadingActivity()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->readingActivity:Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->readingActivity:Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;->resetPagesRead()V

    .line 418
    :cond_0
    return-void
.end method

.method protected safePost(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    .line 506
    .local v0, "localDotsWebView":Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;
    if-nez v0, :cond_0

    .line 517
    :goto_0
    return-void

    .line 509
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$12;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$12;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected safePostIfAllowed(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 522
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$13;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$13;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Ljava/lang/Runnable;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 532
    return-void
.end method

.method public sendAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "pageIndex"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 351
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "sendAnalyticsEvent(%s,%s,%s,%s,%s)"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    const/4 v3, 0x3

    aput-object p4, v2, v3

    const/4 v3, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 354
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 355
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Can not send analytics ads event: Null parameters found."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 367
    :goto_0
    return-void

    .line 360
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;

    move-object v1, p0

    move-object v2, p1

    move v3, p5

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->safePost(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public sendCustomAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "pageIndex"    # I
    .param p6, "dimensionsMetricsJson"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 373
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "sendCustomAnalyticsEvent(%s,%s,%s,%s,%s,%s)"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    const/4 v3, 0x3

    aput-object p4, v2, v3

    const/4 v3, 0x4

    .line 374
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    aput-object p6, v2, v3

    .line 373
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 377
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 378
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Can not send analytics event: Null parameters found."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 411
    :goto_0
    return-void

    .line 383
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p1

    move v4, p5

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->safePost(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public showCreatePost(Ljava/lang/String;)V
    .locals 0
    .param p1, "sectionId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 196
    return-void
.end method

.method public showEditPost(Ljava/lang/String;)V
    .locals 0
    .param p1, "postId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 202
    return-void
.end method

.method public showGotoMenu()V
    .locals 0
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 190
    return-void
.end method

.method public startTiming(Ljava/lang/String;)V
    .locals 0
    .param p1, "methodName"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 490
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->asyncTraceBegin(Ljava/lang/String;)V

    .line 491
    return-void
.end method

.method public switchToApp(Ljava/lang/String;)V
    .locals 3
    .param p1, "appId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 162
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "switchToApp - not implemented"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    return-void
.end method

.method public switchToArticle(Ljava/lang/String;)V
    .locals 3
    .param p1, "postId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    if-nez v0, :cond_0

    .line 158
    :goto_0
    return-void

    .line 139
    :cond_0
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "switchToArticle - unspecified postId"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 143
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$4;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$4;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->safePost(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public switchToSection(Ljava/lang/String;)V
    .locals 3
    .param p1, "sectionId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    if-nez v0, :cond_1

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "switchToSection - unspecified sectionId"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public switchToToc()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->dotsWebView:Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    if-nez v0, :cond_0

    .line 184
    :goto_0
    return-void

    .line 183
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    goto :goto_0
.end method

.method public toggleActionBar()V
    .locals 0
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 132
    return-void
.end method

.method public toggleFullScreen()V
    .locals 0
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 277
    return-void
.end method

.method public watchVideo(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 240
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$6;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$6;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->safePostIfAllowed(Ljava/lang/Runnable;)V

    .line 248
    return-void
.end method
