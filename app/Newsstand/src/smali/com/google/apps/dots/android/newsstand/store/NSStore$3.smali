.class Lcom/google/apps/dots/android/newsstand/store/NSStore$3;
.super Ljava/lang/Object;
.source "NSStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureFallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerDownload(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureFallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

.field final synthetic val$blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

.field final synthetic val$canonicalUri:Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

.field final synthetic val$manifestUri:Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

.field final synthetic val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$manifestUri:Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$canonicalUri:Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iput-object p7, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: Trouble downloading (%s) with manifest URI: %s, falling back to canonical URI: %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 288
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$manifestUri:Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$canonicalUri:Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    aput-object v4, v2, v3

    .line 286
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 289
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$canonicalUri:Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;->val$blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->download(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$1100(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
