.class public Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;
.super Lcom/google/apps/dots/android/newsstand/widget/StateTrackingOnScrollListener;
.source "JankBustinOnScrollListener.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/StateTrackingOnScrollListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 3
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;->isScrolling()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Bustin the jank!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->pauseBackgroundProcessingTemporarily()V

    .line 20
    :cond_0
    return-void
.end method
