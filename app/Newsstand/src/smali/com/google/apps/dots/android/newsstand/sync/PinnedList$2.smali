.class Lcom/google/apps/dots/android/newsstand/sync/PinnedList$2;
.super Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;
.source "PinnedList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/PinnedList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/PinnedList;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/PinnedList;
    .param p2, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p3, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList$2;->this$0:Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;-><init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method protected getFreshData()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/libraries/bind/data/DataException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList$2;->this$0:Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->access$100(Lcom/google/apps/dots/android/newsstand/sync/PinnedList;)Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList$2;->this$0:Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->account:Landroid/accounts/Account;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->access$000(Lcom/google/apps/dots/android/newsstand/sync/PinnedList;)Landroid/accounts/Account;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinned(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v5

    .line 75
    .local v5, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v6, v5, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v6, v6

    invoke-static {v6}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 76
    .local v3, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v7, v5, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v8, v7

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v8, :cond_4

    aget-object v2, v7, v6

    .line 77
    .local v2, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 78
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v9

    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromProto(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .line 79
    .local v1, "edition":Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    sget v9, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_EDITION:I

    invoke-virtual {v0, v9, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 80
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->hasLastSynced()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 81
    sget v9, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_LAST_SYNCED:I

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getLastSynced()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 83
    :cond_0
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->hasLastSyncStarted()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 84
    sget v9, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_LAST_SYNC_STARTED:I

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getLastSyncStarted()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 87
    :cond_1
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList$2;->this$0:Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->account:Landroid/accounts/Account;
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->access$000(Lcom/google/apps/dots/android/newsstand/sync/PinnedList;)Landroid/accounts/Account;

    move-result-object v9

    invoke-static {v9, v1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->getPendingSyncProgress(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Float;

    move-result-object v4

    .line 88
    .local v4, "pendingSyncProgress":Ljava/lang/Float;
    if-eqz v4, :cond_3

    .line 89
    sget v9, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_SYNC_PROGRESS:I

    invoke-virtual {v0, v9, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 95
    :cond_2
    :goto_1
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 90
    :cond_3
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->hasLastSyncProgressAtFailure()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 92
    sget v9, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_SYNC_PROGRESS:I

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getLastSyncProgressAtFailure()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 93
    sget v9, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_SYNC_FAILED:I

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 97
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "edition":Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .end local v2    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .end local v4    # "pendingSyncProgress":Ljava/lang/Float;
    :cond_4
    return-object v3
.end method
