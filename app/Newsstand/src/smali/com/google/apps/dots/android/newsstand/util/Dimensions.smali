.class public Lcom/google/apps/dots/android/newsstand/util/Dimensions;
.super Ljava/lang/Object;
.source "Dimensions.java"


# instance fields
.field public final height:I

.field public final width:I


# direct methods
.method public constructor <init>(FF)V
    .locals 2
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 24
    float-to-int v0, p1

    float-to-int v1, p2

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/util/Dimensions;-><init>(II)V

    .line 25
    return-void
.end method

.method public constructor <init>(II)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "width cannot be negative"

    invoke-static {v0, v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 37
    if-ltz p2, :cond_1

    :goto_1
    const-string v0, "height cannot be negative"

    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 38
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->width:I

    .line 39
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->height:I

    .line 40
    return-void

    :cond_0
    move v0, v2

    .line 36
    goto :goto_0

    :cond_1
    move v1, v2

    .line 37
    goto :goto_1
.end method

.method public static fromBitmap(Landroid/graphics/Bitmap;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    .locals 3
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 68
    if-nez p0, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/util/Dimensions;-><init>(II)V

    goto :goto_0
.end method

.method public static fromBitmapDrawable(Landroid/widget/ImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    .locals 3
    .param p0, "view"    # Landroid/widget/ImageView;

    .prologue
    const/4 v2, 0x0

    .line 75
    if-nez p0, :cond_0

    move-object v0, v2

    .line 76
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    :goto_0
    if-nez v0, :cond_1

    move-object v1, v2

    :goto_1
    return-object v1

    .line 75
    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_0
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    move-object v0, v1

    goto :goto_0

    .line 76
    .restart local v0    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 111
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->width:I

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->width:I

    if-ne v0, v1, :cond_0

    check-cast p1, Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    .end local p1    # "o":Ljava/lang/Object;
    iget v0, p1, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->height:I

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->height:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 106
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->width:I

    const v1, 0x186a0

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->height:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 101
    const-string v0, "[%sx%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->width:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->height:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
