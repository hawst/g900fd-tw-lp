.class final Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic$1;
.super Ljava/lang/Object;
.source "ExploreTopic.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    .locals 6
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 87
    .local v3, "topicId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 88
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "attachmentId":Ljava/lang/String;
    new-instance v4, Ljava/lang/Boolean;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/Boolean;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 90
    .local v1, "internationalCurationsTopic":Z
    const-string v4, "featured_topic"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-nez v1, :cond_0

    .line 91
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->featuredTopic(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    move-result-object v4

    .line 93
    :goto_0
    return-object v4

    :cond_0
    new-instance v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-direct {v4, v3, v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 98
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    move-result-object v0

    return-object v0
.end method
