.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment$1;
.super Ljava/lang/Object;
.source "SortByDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 29
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getMyMagazinesSortByField()I

    move-result v0

    if-eq p2, v0, :cond_0

    .line 30
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getSortByField(I)V

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;->dismiss()V

    .line 33
    return-void
.end method
