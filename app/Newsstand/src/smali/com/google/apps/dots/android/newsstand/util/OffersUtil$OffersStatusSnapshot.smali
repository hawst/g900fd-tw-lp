.class public Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;
.super Ljava/lang/Object;
.source "OffersUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/util/OffersUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OffersStatusSnapshot"
.end annotation


# instance fields
.field private snapshot:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "localOfferStatus":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-static {p1}, Lcom/google/common/collect/Maps;->newHashMap(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;->snapshot:Ljava/util/HashMap;

    .line 76
    return-void
.end method


# virtual methods
.method public isOfferLocallyAcceptedForEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;->snapshot:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isOfferLocallyAcceptedOrDeclined(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Z
    .locals 2
    .param p1, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;->snapshot:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
