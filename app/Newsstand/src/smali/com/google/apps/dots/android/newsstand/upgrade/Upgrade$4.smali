.class Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;
.super Ljava/lang/Object;
.source "Upgrade.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldNewsstandDiskCaches()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

.field final synthetic val$external:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->val$external:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 135
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->storeDirName()Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "storeDirName":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    # getter for: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$100(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->val$external:Z

    if-eqz v1, :cond_0

    move-object v1, v2

    :goto_0
    # invokes: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldNewsstandDiskCacheDirs(Ljava/io/File;Ljava/lang/String;)V
    invoke-static {v3, v4, v1}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$500(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Ljava/io/File;Ljava/lang/String;)V

    .line 138
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    # getter for: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$100(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->val$external:Z

    if-eqz v1, :cond_1

    move-object v1, v2

    :goto_1
    # invokes: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldNewsstandDiskCacheDirs(Ljava/io/File;Ljava/lang/String;)V
    invoke-static {v3, v4, v1}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$500(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Ljava/io/File;Ljava/lang/String;)V

    .line 140
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .line 141
    # getter for: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$100(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v4

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->val$external:Z

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 140
    :goto_2
    # invokes: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldNewsstandDiskCacheDirs(Ljava/io/File;Ljava/lang/String;)V
    invoke-static {v3, v4, v1}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$500(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Ljava/io/File;Ljava/lang/String;)V

    .line 142
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .line 143
    # getter for: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$100(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;->val$external:Z

    if-eqz v4, :cond_3

    .line 142
    .end local v0    # "storeDirName":Ljava/lang/String;
    :goto_3
    # invokes: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldNewsstandDiskCacheDirs(Ljava/io/File;Ljava/lang/String;)V
    invoke-static {v1, v3, v0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$500(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Ljava/io/File;Ljava/lang/String;)V

    .line 144
    return-void

    .restart local v0    # "storeDirName":Ljava/lang/String;
    :cond_0
    move-object v1, v0

    .line 137
    goto :goto_0

    :cond_1
    move-object v1, v0

    .line 138
    goto :goto_1

    :cond_2
    move-object v1, v2

    .line 141
    goto :goto_2

    :cond_3
    move-object v0, v2

    .line 143
    goto :goto_3
.end method
