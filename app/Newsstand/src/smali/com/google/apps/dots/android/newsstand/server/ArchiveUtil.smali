.class public Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;
.super Ljava/lang/Object;
.source "ArchiveUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public static addEditionToArchive(Landroid/app/Activity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 6
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 66
    if-nez p2, :cond_0

    .line 67
    const/4 v3, 0x1

    invoke-static {p0, v3}, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->handleArchivingFailure(Landroid/app/Activity;Z)V

    .line 97
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "appId":Ljava/lang/String;
    new-instance v3, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v3

    .line 72
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    invoke-virtual {v4, p1, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getArchiveMagazineUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v3

    .line 73
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v1

    .line 74
    .local v1, "clientAction":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v3, :cond_1

    .line 75
    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->makeArchiveHint(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setSimulationHint(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 77
    :cond_1
    new-instance v3, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 78
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    move-result-object v2

    .line 79
    .local v2, "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v3

    invoke-virtual {v3, p1, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$2;

    invoke-direct {v4, p2, p0}, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$2;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Landroid/app/Activity;)V

    sget-object v5, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_MAIN:Lcom/google/android/libraries/bind/async/Queue;

    invoke-static {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private static handleArchivingFailure(Landroid/app/Activity;Z)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "adding"    # Z

    .prologue
    .line 160
    if-eqz p1, :cond_0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->add_to_archive_failed:I

    :goto_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "message":Ljava/lang/String;
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->showToast(Landroid/app/Activity;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;Z)V

    .line 163
    return-void

    .line 160
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->remove_from_archive_failed:I

    goto :goto_0
.end method

.method public static removeEditionFromArchive(Landroid/app/Activity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 9
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 129
    if-nez p2, :cond_0

    .line 130
    invoke-static {p0, v7}, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->handleArchivingFailure(Landroid/app/Activity;Z)V

    .line 154
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "appId":Ljava/lang/String;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Removing edition %s from archive"

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 136
    new-instance v3, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    invoke-virtual {v3, v8}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v3

    .line 137
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    invoke-virtual {v4, p1, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getArchiveMagazineUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v3

    .line 138
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v1

    .line 139
    .local v1, "clientAction":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v3, :cond_1

    .line 140
    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->makeArchiveHint(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setSimulationHint(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 142
    :cond_1
    new-instance v3, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 143
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    move-result-object v2

    .line 144
    .local v2, "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v3

    invoke-virtual {v3, p1, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$4;

    invoke-direct {v4, p2}, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$4;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    sget-object v5, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_MAIN:Lcom/google/android/libraries/bind/async/Queue;

    invoke-static {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public static removeEditionFromArchive(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 2
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p1, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 105
    .line 106
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 109
    .local v0, "editionSummaryFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;>;"
    new-instance v1, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$3;

    invoke-direct {v1, p1}, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$3;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 117
    .local v1, "unarchiveCallback":Lcom/google/common/util/concurrent/FutureCallback;, "Lcom/google/common/util/concurrent/FutureCallback<-Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;>;"
    invoke-virtual {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 118
    return-void
.end method
