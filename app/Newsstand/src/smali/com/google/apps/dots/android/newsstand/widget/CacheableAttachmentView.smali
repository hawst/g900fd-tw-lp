.class public Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
.super Lcom/google/apps/dots/android/newsstand/widget/NSImageView;
.source "CacheableAttachmentView.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private attachmentId:Ljava/lang/String;

.field private backgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private bindAttachmentIdKey:Ljava/lang/Integer;

.field private bindTransformKey:Ljava/lang/Integer;

.field private cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

.field private hideBackgroundWhenLoaded:Z

.field private hideSiblingViewWhenLoadedId:I

.field private isAttached:Z

.field private isConstructed:Z

.field private isTemporarilyDetached:Z

.field private isVisible:Z

.field private loadEvenIfDetached:Z

.field private loadEvenIfNotVisible:Z

.field private mode:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;

.field private runWhenBitmapReleased:Ljava/lang/Runnable;

.field private runWhenImageSet:Ljava/lang/Runnable;

.field private transform:Lcom/google/apps/dots/android/newsstand/server/Transform;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 88
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;-><init>(Landroid/content/Context;)V

    .line 45
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;->NORMAL:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->mode:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;

    .line 50
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadEvenIfNotVisible:Z

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->hideBackgroundWhenLoaded:Z

    .line 89
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isConstructed:Z

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;->NORMAL:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->mode:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;

    .line 50
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadEvenIfNotVisible:Z

    .line 52
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->hideBackgroundWhenLoaded:Z

    .line 61
    sget-object v1, Lcom/google/android/apps/newsstanddev/R$styleable;->CacheableAttachmentView:[I

    .line 62
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 64
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->CacheableAttachmentView_hideBackgroundWhenLoaded:I

    .line 65
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->hideBackgroundWhenLoaded:Z

    .line 67
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->CacheableAttachmentView_hideSiblingViewWhenLoaded:I

    .line 68
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->hideSiblingViewWhenLoadedId:I

    .line 71
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->CacheableAttachmentView_bindAttachmentId:I

    .line 72
    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->bindAttachmentIdKey:Ljava/lang/Integer;

    .line 74
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->CacheableAttachmentView_bindTransform:I

    .line 75
    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->bindTransformKey:Ljava/lang/Integer;

    .line 76
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 77
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isConstructed:Z

    .line 78
    return-void
.end method

.method private loadBitmap()V
    .locals 5

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->mode:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;->NORMAL:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;

    if-ne v0, v1, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 230
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->releaseBitmap()V

    .line 231
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->markLoadStart()V

    .line 232
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->shouldLoad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Loading bitmap with attachmentId: %s, transform: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentViewCache()Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->DEFAULT:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->getBitmap(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    goto :goto_0
.end method

.method private loadOrReleaseAsNecessary()V
    .locals 2

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->shouldLoad()Z

    move-result v0

    .line 218
    .local v0, "shouldLoad":Z
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    if-nez v1, :cond_1

    .line 219
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadBitmap()V

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    if-eqz v1, :cond_0

    .line 221
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->releaseBitmap()V

    goto :goto_0
.end method

.method private shouldLoad()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 188
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isVisible:Z

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadEvenIfNotVisible:Z

    if-eqz v5, :cond_3

    :cond_0
    move v2, v4

    .line 189
    .local v2, "visibilityTest":Z
    :goto_0
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isAttached:Z

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isTemporarilyDetached:Z

    if-eqz v5, :cond_2

    :cond_1
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadEvenIfDetached:Z

    if-eqz v5, :cond_4

    :cond_2
    move v0, v4

    .line 190
    .local v0, "attachedTest":Z
    :goto_1
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/server/Transform;->isResizeTransform()Z

    move-result v5

    if-eqz v5, :cond_5

    move v1, v4

    .line 191
    .local v1, "transformReadyTest":Z
    :goto_2
    if-eqz v2, :cond_6

    and-int v5, v0, v1

    if-eqz v5, :cond_6

    :goto_3
    return v4

    .end local v0    # "attachedTest":Z
    .end local v1    # "transformReadyTest":Z
    .end local v2    # "visibilityTest":Z
    :cond_3
    move v2, v3

    .line 188
    goto :goto_0

    .restart local v2    # "visibilityTest":Z
    :cond_4
    move v0, v3

    .line 189
    goto :goto_1

    .restart local v0    # "attachedTest":Z
    :cond_5
    move v1, v3

    .line 190
    goto :goto_2

    .restart local v1    # "transformReadyTest":Z
    :cond_6
    move v4, v3

    .line 191
    goto :goto_3
.end method

.method private updateTransformIfNeeded()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 168
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getMeasuredWidth()I

    move-result v2

    .line 169
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getMeasuredHeight()I

    move-result v1

    .line 170
    .local v1, "height":I
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/server/Transform;->isResizeTransform()Z

    move-result v3

    if-nez v3, :cond_1

    add-int v3, v2, v1

    if-lez v3, :cond_1

    .line 173
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/server/Transform;->buildUpon()Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v0

    .line 177
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    if-lez v2, :cond_0

    if-lez v1, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/server/Transform;->isCroppingTransform()Z

    move-result v3

    if-nez v3, :cond_0

    .line 178
    invoke-virtual {v0, v8}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->crop(Z)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    .line 180
    :cond_0
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 181
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "updateTransformIfNeeded - generated transform %s for attachmentId: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    aput-object v7, v5, v6

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadOrReleaseAsNecessary()V

    .line 185
    .end local v0    # "builder":Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    :cond_1
    return-void
.end method


# virtual methods
.method public getAttachmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    return-object v0
.end method

.method public getTransform()Lcom/google/apps/dots/android/newsstand/server/Transform;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    return-object v0
.end method

.method public isLoadedOrFailed()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->isLoadedOrFailed()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->hasAlpha()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 318
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isAttached:Z

    .line 319
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->onAttachedToWindow()V

    .line 320
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onAttachedToWindow with attachmentId: %s, transform: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 321
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadOrReleaseAsNecessary()V

    .line 322
    return-void
.end method

.method public onCachedBitmapMissing()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->runWhenImageSet:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->runWhenImageSet:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 157
    :cond_0
    return-void
.end method

.method public onCachedBitmapReady(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)V
    .locals 5
    .param p1, "cachedBitmap"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .prologue
    .line 144
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Received bitmap with attachmentId: %s, transform: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .line 146
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 147
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->runWhenImageSet:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->runWhenImageSet:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 150
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 301
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->onDetachedFromWindow()V

    .line 302
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isAttached:Z

    .line 303
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isTemporarilyDetached:Z

    .line 304
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onDetachedFromWindow with attachmentId: %s, transform: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 305
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadOrReleaseAsNecessary()V

    .line 306
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 326
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isTemporarilyDetached:Z

    .line 327
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->onFinishTemporaryDetach()V

    .line 328
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onFinishTemporaryDetach with attachmentId: %s, transform: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 329
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadOrReleaseAsNecessary()V

    .line 330
    return-void
.end method

.method protected onImageBitmapDisplayed()V
    .locals 7

    .prologue
    .line 260
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->hideSiblingViewWhenLoadedId:I

    if-nez v2, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 265
    .local v1, "parent":Landroid/view/ViewGroup;
    if-eqz v1, :cond_0

    .line 266
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->hideSiblingViewWhenLoadedId:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 267
    .local v0, "hideSiblingViewWhenLoaded":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 268
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 270
    :cond_2
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Could not find view for hideSiblingViewWhenLoaded with id: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->hideSiblingViewWhenLoadedId:I

    .line 272
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getResourceName(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 270
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 161
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onLayout with attachmentId: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    invoke-super/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->onLayout(ZIIII)V

    .line 163
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->updateTransformIfNeeded()V

    .line 164
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 310
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isTemporarilyDetached:Z

    .line 311
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->onStartTemporaryDetach()V

    .line 312
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onStartTemporaryDetach with attachmentId: %s, transform: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 313
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadOrReleaseAsNecessary()V

    .line 314
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 343
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 346
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isConstructed:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadEvenIfNotVisible:Z

    if-nez v0, :cond_0

    .line 347
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->isVisible(Landroid/view/View;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isVisible:Z

    .line 348
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadOrReleaseAsNecessary()V

    .line 350
    :cond_0
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 334
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->onWindowVisibilityChanged(I)V

    .line 335
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadEvenIfNotVisible:Z

    if-nez v0, :cond_0

    .line 336
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->isVisible(Landroid/view/View;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isVisible:Z

    .line 337
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadOrReleaseAsNecessary()V

    .line 339
    :cond_0
    return-void
.end method

.method protected releaseBitmap()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 241
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    if-eqz v0, :cond_0

    .line 242
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Releaseing bitmap with attachmentId: %s, transform: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 244
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentViewCache()Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    invoke-virtual {v0, v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->releaseBitmap(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V

    .line 245
    iput-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .line 246
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->runWhenBitmapReleased:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->runWhenBitmapReleased:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 250
    :cond_0
    return-void
.end method

.method public setAttachmentId(Ljava/lang/String;)V
    .locals 1
    .param p1, "attachmentId"    # Ljava/lang/String;

    .prologue
    .line 110
    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentIdPx(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    .line 111
    return-void
.end method

.method public setAttachmentIdPx(Ljava/lang/String;II)V
    .locals 2
    .param p1, "attachmentId"    # Ljava/lang/String;
    .param p2, "widthPx"    # I
    .param p3, "heightPx"    # I

    .prologue
    .line 123
    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    const/4 v1, 0x1

    .line 124
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->crop(Z)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    .line 123
    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentIdPx(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    .line 125
    return-void
.end method

.method public setAttachmentIdPx(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)V
    .locals 1
    .param p1, "attachmentId"    # Ljava/lang/String;
    .param p2, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 129
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->mode:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView$ImageViewMode;

    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 131
    invoke-static {p2, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->releaseBitmap()V

    .line 135
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    .line 136
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 137
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->updateTransformIfNeeded()V

    .line 138
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadOrReleaseAsNecessary()V

    .line 139
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->requestLayout()V

    goto :goto_0
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 279
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setBackground(Landroid/graphics/drawable/Drawable;Z)V

    .line 280
    return-void
.end method

.method protected setBackground(Landroid/graphics/drawable/Drawable;Z)V
    .locals 0
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;
    .param p2, "preserve"    # Z

    .prologue
    .line 286
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 287
    if-eqz p2, :cond_0

    .line 288
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->backgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 290
    :cond_0
    return-void
.end method

.method public setHideBackgroundWhenLoaded(Z)V
    .locals 0
    .param p1, "hideBackgroundWhenLoaded"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->hideBackgroundWhenLoaded:Z

    .line 94
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 254
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 255
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->toggleBackgroundIfNeeded(Landroid/graphics/drawable/Drawable;)V

    .line 256
    return-void
.end method

.method public setLoadEvenIfDetached(Z)V
    .locals 1
    .param p1, "loadEvenIfDetached"    # Z

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadEvenIfDetached:Z

    if-eq p1, v0, :cond_0

    .line 196
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadEvenIfDetached:Z

    .line 197
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadOrReleaseAsNecessary()V

    .line 199
    :cond_0
    return-void
.end method

.method public setLoadEvenIfNotVisible(Z)V
    .locals 1
    .param p1, "loadWhenNotVisible"    # Z

    .prologue
    .line 202
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadEvenIfNotVisible:Z

    if-eq p1, v0, :cond_1

    .line 203
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadEvenIfNotVisible:Z

    .line 205
    if-nez p1, :cond_0

    .line 206
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->isVisible(Landroid/view/View;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->isVisible:Z

    .line 208
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->loadOrReleaseAsNecessary()V

    .line 210
    :cond_1
    return-void
.end method

.method public setRunWhenBitmapReleased(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->runWhenBitmapReleased:Ljava/lang/Runnable;

    .line 119
    return-void
.end method

.method public setRunWhenImageSet(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->runWhenImageSet:Ljava/lang/Runnable;

    .line 115
    return-void
.end method

.method protected toggleBackgroundIfNeeded(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "imageDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 293
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->hideBackgroundWhenLoaded:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->backgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 294
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->backgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 295
    .local v0, "newBackground":Landroid/graphics/drawable/Drawable;
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setBackground(Landroid/graphics/drawable/Drawable;Z)V

    .line 297
    .end local v0    # "newBackground":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void

    .line 294
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 10
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 359
    const/4 v0, 0x0

    .line 360
    .local v0, "newAttachmentId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 361
    .local v1, "newTransform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->bindAttachmentIdKey:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    .line 362
    if-nez p1, :cond_2

    move-object v0, v2

    .line 363
    :goto_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Updating bound data, old attachmentId: %s, new attachmentId: %s"

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->attachmentId:Ljava/lang/String;

    aput-object v6, v5, v7

    aput-object v0, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 365
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->bindTransformKey:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    .line 366
    if-nez p1, :cond_3

    :goto_1
    check-cast v2, Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-object v1, v2

    check-cast v1, Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 367
    if-eqz v1, :cond_0

    .line 368
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Updating bound data, old transform: %s, new transform: %s"

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    aput-object v5, v4, v7

    aput-object v1, v4, v8

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373
    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 374
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->bindAttachmentIdKey:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 376
    invoke-virtual {p0, v7, v7}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setMeasuredDimension(II)V

    .line 377
    if-eqz v1, :cond_4

    .line 378
    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentIdPx(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    .line 383
    :cond_1
    :goto_2
    return-void

    .line 362
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->bindAttachmentIdKey:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 366
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->bindTransformKey:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto :goto_1

    .line 380
    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentId(Ljava/lang/String;)V

    goto :goto_2
.end method
