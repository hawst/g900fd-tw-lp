.class Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$2;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "CollectionDataList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->checkStoreForNewVersion(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

.field final synthetic val$newVersionUri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$2;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$2;->val$newVersionUri:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 3
    .param p1, "mutationResponse"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$2;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$2;->val$newVersionUri:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    # invokes: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->handleNewVersion(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/Version;)V
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$1000(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/Version;)V

    .line 284
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 280
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$2;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    return-void
.end method
