.class public Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;
.super Ljava/lang/Object;
.source "LockSpace.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final exclusiveLocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TR;>;"
        }
    .end annotation
.end field

.field private final inclusiveLocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TR;>;"
        }
    .end annotation
.end field

.field private final lock:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;, "Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace<TR;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->lock:Ljava/lang/Object;

    .line 32
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->inclusiveLocks:Ljava/util/List;

    .line 33
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->exclusiveLocks:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->exclusiveLocks:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->inclusiveLocks:Ljava/util/List;

    return-object v0
.end method

.method private locked(Ljava/lang/Object;Z)Z
    .locals 3
    .param p2, "exclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;Z)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;, "Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace<TR;>;"
    .local p1, "region":Ljava/lang/Object;, "TR;"
    const/4 v1, 0x1

    .line 80
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->exclusiveLocks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 81
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->exclusiveLocks:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->intersects(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 92
    :cond_0
    :goto_1
    return v1

    .line 80
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :cond_2
    if-eqz p2, :cond_3

    .line 86
    const/4 v0, 0x0

    :goto_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->inclusiveLocks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 87
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->inclusiveLocks:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->intersects(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 92
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected intersects(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TR;)Z"
        }
    .end annotation

    .prologue
    .line 40
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;, "Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace<TR;>;"
    .local p1, "a":Ljava/lang/Object;, "TR;"
    .local p2, "b":Ljava/lang/Object;, "TR;"
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public lock(Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;
    .locals 2
    .param p2, "exclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;Z)",
            "Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;, "Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace<TR;>;"
    .local p1, "region":Ljava/lang/Object;, "TR;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 45
    :goto_0
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->locked(Ljava/lang/Object;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    :try_start_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->lock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 48
    :catch_0
    move-exception v0

    goto :goto_0

    .line 52
    :cond_0
    if-eqz p2, :cond_1

    :try_start_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->exclusiveLocks:Ljava/util/List;

    :goto_1
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 55
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;ZLjava/lang/Object;)V

    return-object v0

    .line 52
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->inclusiveLocks:Ljava/util/List;

    goto :goto_1

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method
