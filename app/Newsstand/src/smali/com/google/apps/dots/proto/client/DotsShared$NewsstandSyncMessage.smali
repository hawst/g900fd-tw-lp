.class public final Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NewsstandSyncMessage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;


# instance fields
.field private bitField0_:I

.field public magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

.field private messageId_:Ljava/lang/String;

.field public newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

.field public operation:[I

.field private serverSentTime_:J

.field private userId_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23138
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23139
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 23309
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->messageId_:Ljava/lang/String;

    .line 23331
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    .line 23334
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->userId_:Ljava/lang/String;

    .line 23356
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->serverSentTime_:J

    .line 23375
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    .line 23378
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    .line 23139
    return-void
.end method

.method public static parseFrom([B)Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 23624
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;

    return-object v0
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    .locals 5

    .prologue
    .line 23396
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23400
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    array-length v3, v3

    if-lez v3, :cond_0

    .line 23401
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    invoke-virtual {v3}, [I->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    .line 23403
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 23404
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    .line 23405
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 23406
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 23407
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->clone()Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    move-result-object v4

    aput-object v4, v3, v2

    .line 23405
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 23397
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 23398
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 23411
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 23412
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    .line 23413
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v3, v3

    if-ge v2, v3, :cond_4

    .line 23414
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v3, v3, v2

    if-eqz v3, :cond_3

    .line 23415
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->clone()Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    move-result-object v4

    aput-object v4, v3, v2

    .line 23413
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 23419
    .end local v2    # "i":I
    :cond_4
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 23135
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->clone()Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 23424
    if-ne p1, p0, :cond_1

    .line 23432
    :cond_0
    :goto_0
    return v1

    .line 23425
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 23426
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;

    .line 23427
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->messageId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->messageId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    .line 23428
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->userId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->userId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 23429
    :goto_2
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->serverSentTime_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->serverSentTime_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    .line 23431
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    .line 23432
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 23427
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->messageId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->messageId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    .line 23428
    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->userId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->userId_:Ljava/lang/String;

    .line 23429
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2
.end method

.method public getMessageId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23311
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->messageId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 23496
    const/4 v2, 0x0

    .line 23497
    .local v2, "size":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    array-length v4, v4

    if-lez v4, :cond_1

    .line 23498
    const/4 v0, 0x0

    .line 23499
    .local v0, "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_0

    aget v1, v5, v4

    .line 23501
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v7

    add-int/2addr v0, v7

    .line 23499
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 23503
    .end local v1    # "element":I
    :cond_0
    add-int/2addr v2, v0

    .line 23504
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 23506
    .end local v0    # "dataSize":I
    :cond_1
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_2

    .line 23507
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->userId_:Ljava/lang/String;

    .line 23508
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 23510
    :cond_2
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_3

    .line 23511
    const/4 v4, 0x3

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->serverSentTime_:J

    .line 23512
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 23514
    :cond_3
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-eqz v4, :cond_5

    .line 23515
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_5

    aget-object v1, v5, v4

    .line 23516
    .local v1, "element":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    if-eqz v1, :cond_4

    .line 23517
    const/4 v7, 0x4

    .line 23518
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 23515
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 23522
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :cond_5
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-eqz v4, :cond_7

    .line 23523
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v5, v4

    :goto_2
    if-ge v3, v5, :cond_7

    aget-object v1, v4, v3

    .line 23524
    .restart local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    if-eqz v1, :cond_6

    .line 23525
    const/4 v6, 0x5

    .line 23526
    invoke-static {v6, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v2, v6

    .line 23523
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 23530
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :cond_7
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_8

    .line 23531
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->messageId_:Ljava/lang/String;

    .line 23532
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 23534
    :cond_8
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->cachedSize:I

    .line 23535
    return v2
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23336
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->userId_:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 23436
    const/16 v1, 0x11

    .line 23437
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 23438
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->messageId_:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 23439
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    if-nez v2, :cond_4

    mul-int/lit8 v1, v1, 0x1f

    .line 23445
    :cond_0
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->userId_:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 23446
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->serverSentTime_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->serverSentTime_:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 23447
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-nez v2, :cond_6

    mul-int/lit8 v1, v1, 0x1f

    .line 23453
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-nez v2, :cond_8

    mul-int/lit8 v1, v1, 0x1f

    .line 23459
    :cond_2
    return v1

    .line 23438
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->messageId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 23441
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 23442
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    aget v4, v4, v0

    add-int v1, v2, v4

    .line 23441
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 23445
    .end local v0    # "i":I
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->userId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 23449
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 23450
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 23449
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 23450
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->hashCode()I

    move-result v2

    goto :goto_4

    .line 23455
    .end local v0    # "i":I
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 23456
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    move v2, v3

    :goto_6
    add-int v1, v4, v2

    .line 23455
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 23456
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->hashCode()I

    move-result v2

    goto :goto_6
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 23543
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 23544
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 23548
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 23549
    :sswitch_0
    return-object p0

    .line 23554
    :sswitch_1
    const/16 v5, 0x8

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 23555
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    array-length v1, v5

    .line 23556
    .local v1, "i":I
    add-int v5, v1, v0

    new-array v2, v5, [I

    .line 23557
    .local v2, "newArray":[I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 23558
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    .line 23559
    :goto_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_1

    .line 23560
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    .line 23561
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 23559
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 23564
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    goto :goto_0

    .line 23568
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->userId_:Ljava/lang/String;

    .line 23569
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    goto :goto_0

    .line 23573
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->serverSentTime_:J

    .line 23574
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    goto :goto_0

    .line 23578
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 23579
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-nez v5, :cond_3

    move v1, v4

    .line 23580
    .restart local v1    # "i":I
    :goto_2
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    .line 23581
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-eqz v5, :cond_2

    .line 23582
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 23584
    :cond_2
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    .line 23585
    :goto_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 23586
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;-><init>()V

    aput-object v6, v5, v1

    .line 23587
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 23588
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 23585
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 23579
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v1, v5

    goto :goto_2

    .line 23591
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;-><init>()V

    aput-object v6, v5, v1

    .line 23592
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 23596
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :sswitch_5
    const/16 v5, 0x2a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 23597
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-nez v5, :cond_6

    move v1, v4

    .line 23598
    .restart local v1    # "i":I
    :goto_4
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    .line 23599
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-eqz v5, :cond_5

    .line 23600
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 23602
    :cond_5
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    .line 23603
    :goto_5
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 23604
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;-><init>()V

    aput-object v6, v5, v1

    .line 23605
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 23606
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 23603
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 23597
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :cond_6
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v1, v5

    goto :goto_4

    .line 23609
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :cond_7
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;-><init>()V

    aput-object v6, v5, v1

    .line 23610
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 23614
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->messageId_:Ljava/lang/String;

    .line 23615
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    goto/16 :goto_0

    .line 23544
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23135
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 23464
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    array-length v2, v2

    if-lez v2, :cond_0

    .line 23465
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget v0, v3, v2

    .line 23466
    .local v0, "element":I
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 23465
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 23469
    .end local v0    # "element":I
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 23470
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->userId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 23472
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 23473
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->serverSentTime_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 23475
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-eqz v2, :cond_4

    .line 23476
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->newsAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v0, v3, v2

    .line 23477
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    if-eqz v0, :cond_3

    .line 23478
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 23476
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 23482
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-eqz v2, :cond_6

    .line 23483
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v0, v2, v1

    .line 23484
    .restart local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    if-eqz v0, :cond_5

    .line 23485
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 23483
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 23489
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :cond_6
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_7

    .line 23490
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->messageId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 23492
    :cond_7
    return-void
.end method
