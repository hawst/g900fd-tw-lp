.class public final enum Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
.super Ljava/lang/Enum;
.source "ProtoEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/util/ProtoEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EditionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

.field public static final enum CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

.field public static final enum GEOLOCATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

.field public static final enum MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

.field public static final enum NEWS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

.field public static final enum READ_NOW:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

.field public static final enum RELATED_POSTS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

.field public static final enum SAVED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

.field public static final enum SEARCH_POSTS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

.field public static final enum SECTION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

.field public static final enum TOPIC:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;


# instance fields
.field public final editionType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 255
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const-string v1, "READ_NOW"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->READ_NOW:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 256
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const-string v1, "SAVED"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SAVED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 257
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const-string v1, "NEWS"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->NEWS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 258
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const-string v1, "SECTION"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SECTION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 259
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const-string v1, "MAGAZINE"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 260
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const-string v1, "TOPIC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->TOPIC:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 261
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const-string v1, "GEOLOCATION"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->GEOLOCATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 262
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const-string v1, "SEARCH_POSTS"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SEARCH_POSTS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 263
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const-string v1, "CURATION"

    const/16 v2, 0x8

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 264
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const-string v1, "RELATED_POSTS"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->RELATED_POSTS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 254
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->READ_NOW:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SAVED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->NEWS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SECTION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->TOPIC:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->GEOLOCATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SEARCH_POSTS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->RELATED_POSTS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "editionType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 277
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 278
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->editionType:I

    .line 279
    return-void
.end method

.method public static fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    .locals 5
    .param p0, "editionType"    # I

    .prologue
    .line 269
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 270
    .local v0, "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    iget v4, v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->editionType:I

    if-ne v4, p0, :cond_0

    .line 274
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    :goto_1
    return-object v0

    .line 269
    .restart local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 274
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 254
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    .locals 1

    .prologue
    .line 254
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    return-object v0
.end method
