.class Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$1;
.super Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;
.source "MyLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->setupHeaderListLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 87
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->my_library_content:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 88
    .local v0, "contentView":Landroid/view/View;
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 89
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->pager:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/NSViewPager;

    # setter for: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$002(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;Landroid/support/v4/view/NSViewPager;)Landroid/support/v4/view/NSViewPager;

    .line 90
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->setupPager()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$100(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)V

    .line 91
    return-void
.end method

.method protected alwaysUseFloatingBackground()Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x1

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 2

    .prologue
    .line 100
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getHeaderShadowMode()I
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x3

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x1

    return v0
.end method

.method protected getViewPagerId()I
    .locals 1

    .prologue
    .line 95
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->pager:I

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    return v0
.end method
