.class Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$1;
.super Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;
.source "WebPartView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;ZLcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;Landroid/view/View;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;ZZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "nativeWidgetHelper"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;
    .param p4, "respectBoundaries"    # Z
    .param p5, "respectVisibility"    # Z

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;-><init>(Landroid/view/View;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;ZZ)V

    return-void
.end method


# virtual methods
.method protected onEnterScreen()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->onEnterScreen()V

    .line 128
    :cond_0
    return-void
.end method

.method protected onExitScreen()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->onExitScreen()V

    .line 135
    :cond_0
    return-void
.end method
