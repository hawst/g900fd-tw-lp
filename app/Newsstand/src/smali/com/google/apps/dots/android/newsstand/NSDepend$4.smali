.class final Lcom/google/apps/dots/android/newsstand/NSDepend$4;
.super Lcom/google/apps/dots/android/newsstand/async/QueueTask;
.source "NSDepend.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/NSDepend;->initClassLoaders()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V
    .locals 0
    .param p1, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;

    .prologue
    .line 1079
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    return-void
.end method


# virtual methods
.method protected doInBackground()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1082
    # getter for: Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Load dynamic libraries..."

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1083
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$array;->extra_jars:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 1084
    .local v0, "jar":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getClassLoaderForJar(Ljava/lang/String;)Ljava/lang/ClassLoader;

    .line 1083
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1086
    .end local v0    # "jar":Ljava/lang/String;
    :cond_0
    return-void
.end method
