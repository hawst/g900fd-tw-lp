.class Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3$1;
.super Ljava/lang/Object;
.source "NormalArticleWidget.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;->onSuccess(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;

    .prologue
    .line 381
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    const/4 v4, 0x0

    .line 390
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Failed to load html for webview: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 393
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOAD_FAILED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v0, v1, v4, p1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;ZLjava/lang/Throwable;)V

    .line 394
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 381
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3$1;->onSuccess(Ljava/lang/String;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/String;)V
    .locals 6
    .param p1, "html"    # Ljava/lang/String;

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 385
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v2

    .line 384
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getWebviewBaseUrl(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const-string v5, ""

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    return-void
.end method
