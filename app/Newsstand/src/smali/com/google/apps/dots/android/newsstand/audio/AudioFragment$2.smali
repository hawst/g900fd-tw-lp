.class Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;
.super Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;
.source "AudioFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->setupAudio()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method protected onReceiveUpdate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 126
    const-string v1, "status"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 127
    .local v0, "newAudioStatus":I
    packed-switch v0, :pswitch_data_0

    .line 146
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioStatus:I
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$902(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;I)I

    .line 147
    return-void

    .line 129
    :pswitch_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioControlBar:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hide()V

    .line 130
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->audio_error:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 133
    :pswitch_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioControlBar:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hide()V

    goto :goto_0

    .line 137
    :pswitch_3
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioStatus:I
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$900(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioStatus:I
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$900(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .line 138
    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioStatus:I
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$900(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioControlBar:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->show()V

    goto :goto_0

    .line 143
    :pswitch_4
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioControlBar:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->show()V

    goto :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method
