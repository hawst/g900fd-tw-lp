.class Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$5;
.super Ljava/lang/Object;
.source "MyLibraryFragment.java"

# interfaces
.implements Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->setupPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBeforeTabSelected(I)V
    .locals 0
    .param p1, "pageIndex"    # I

    .prologue
    .line 208
    return-void
.end method

.method public onTabSelected(I)V
    .locals 5
    .param p1, "pageIndex"    # I

    .prologue
    .line 212
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->isChangingState()Z
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$600(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .line 213
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v2

    if-nez v2, :cond_0

    .line 214
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-static {v2, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v1

    .line 215
    .local v1, "logicalPosition":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .line 216
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    sget v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DK_LIBRARY_PAGE:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 217
    .local v0, "libraryPage":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    invoke-direct {v3, v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)V

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 219
    .end local v0    # "libraryPage":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    .end local v1    # "logicalPosition":I
    :cond_0
    return-void
.end method
