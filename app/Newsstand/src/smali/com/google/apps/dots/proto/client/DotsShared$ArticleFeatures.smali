.class public final Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ArticleFeatures"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;


# instance fields
.field private bitField0_:I

.field private hasAudio_:Z

.field private hasSlideshow_:Z

.field private hasVideo_:Z

.field private hideFromTOC_:Z

.field private isAdvertisement_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17130
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17131
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 17136
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->isAdvertisement_:Z

    .line 17155
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasAudio_:Z

    .line 17174
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasVideo_:Z

    .line 17193
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasSlideshow_:Z

    .line 17212
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hideFromTOC_:Z

    .line 17131
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;
    .locals 3

    .prologue
    .line 17245
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 17249
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;
    return-object v0

    .line 17246
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;
    :catch_0
    move-exception v1

    .line 17247
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 17127
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 17254
    if-ne p1, p0, :cond_1

    .line 17257
    :cond_0
    :goto_0
    return v1

    .line 17255
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 17256
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    .line 17257
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->isAdvertisement_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->isAdvertisement_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasAudio_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasAudio_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasVideo_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasVideo_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasSlideshow_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasSlideshow_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hideFromTOC_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hideFromTOC_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 17296
    const/4 v0, 0x0

    .line 17297
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 17298
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->isAdvertisement_:Z

    .line 17299
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 17301
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 17302
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasAudio_:Z

    .line 17303
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 17305
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 17306
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasVideo_:Z

    .line 17307
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 17309
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 17310
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasSlideshow_:Z

    .line 17311
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 17313
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 17314
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hideFromTOC_:Z

    .line 17315
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 17317
    :cond_4
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->cachedSize:I

    .line 17318
    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 17265
    const/16 v0, 0x11

    .line 17266
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 17267
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->isAdvertisement_:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v4, v1

    .line 17268
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasAudio_:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v4, v1

    .line 17269
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasVideo_:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v4, v1

    .line 17270
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasSlideshow_:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v4, v1

    .line 17271
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hideFromTOC_:Z

    if-eqz v4, :cond_4

    :goto_4
    add-int v0, v1, v2

    .line 17272
    return v0

    :cond_0
    move v1, v3

    .line 17267
    goto :goto_0

    :cond_1
    move v1, v3

    .line 17268
    goto :goto_1

    :cond_2
    move v1, v3

    .line 17269
    goto :goto_2

    :cond_3
    move v1, v3

    .line 17270
    goto :goto_3

    :cond_4
    move v2, v3

    .line 17271
    goto :goto_4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17326
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 17327
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 17331
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 17332
    :sswitch_0
    return-object p0

    .line 17337
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->isAdvertisement_:Z

    .line 17338
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    goto :goto_0

    .line 17342
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasAudio_:Z

    .line 17343
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    goto :goto_0

    .line 17347
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasVideo_:Z

    .line 17348
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    goto :goto_0

    .line 17352
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasSlideshow_:Z

    .line 17353
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    goto :goto_0

    .line 17357
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hideFromTOC_:Z

    .line 17358
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    goto :goto_0

    .line 17327
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17127
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17277
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 17278
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->isAdvertisement_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 17280
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 17281
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasAudio_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 17283
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 17284
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasVideo_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 17286
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 17287
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hasSlideshow_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 17289
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 17290
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hideFromTOC_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 17292
    :cond_4
    return-void
.end method
