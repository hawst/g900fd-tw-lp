.class Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;
.super Ljava/lang/Object;
.source "AttachmentStore.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->decodeWithAllocatedBitmap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

.field final synthetic val$options:Landroid/graphics/BitmapFactory$Options;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Landroid/graphics/BitmapFactory$Options;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 257
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$options:Landroid/graphics/BitmapFactory$Options;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Landroid/graphics/Bitmap;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x4000

    .line 261
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->acquire(I)[B

    move-result-object v1

    .line 262
    .local v1, "inputStreamBuffer":[B
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$options:Landroid/graphics/BitmapFactory$Options;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->acquire(I)[B

    move-result-object v4

    iput-object v4, v3, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 263
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeDiskBlob()Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    move-result-object v0

    .line 265
    .local v0, "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    :try_start_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_0

    .line 266
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$options:Landroid/graphics/BitmapFactory$Options;

    const/4 v4, 0x1

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 269
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$options:Landroid/graphics/BitmapFactory$Options;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->retryBitmapDecodeWithGC(Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    invoke-static {v3, v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$400(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 270
    .local v2, "result":Landroid/graphics/Bitmap;
    if-nez v2, :cond_1

    .line 271
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Couldn\'t decode bitmap"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    .end local v2    # "result":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 284
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$options:Landroid/graphics/BitmapFactory$Options;

    iget-object v5, v5, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 285
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$options:Landroid/graphics/BitmapFactory$Options;

    iput-object v6, v4, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 286
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->close()V

    throw v3

    .line 274
    .restart local v2    # "result":Landroid/graphics/Bitmap;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->isDestroyed()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 275
    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Detected cancellation"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 277
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->getGenericCancellation()Ljava/util/concurrent/CancellationException;

    move-result-object v3

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 283
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 284
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$options:Landroid/graphics/BitmapFactory$Options;

    iget-object v4, v4, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 285
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->val$options:Landroid/graphics/BitmapFactory$Options;

    iput-object v6, v3, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 286
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->close()V

    return-object v2
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$5;->call()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
