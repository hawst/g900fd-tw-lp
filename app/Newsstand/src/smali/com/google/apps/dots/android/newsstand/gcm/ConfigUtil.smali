.class public Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;
.super Ljava/lang/Object;
.source "ConfigUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final appContext:Landroid/content/Context;

.field private final appVersion:I

.field private final cacheLock:Ljava/lang/Object;

.field private configFutures:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
            ">;>;"
        }
    .end annotation
.end field

.field private final nsClient:Lcom/google/apps/dots/android/newsstand/http/NSClient;

.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field private final serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/http/NSClient;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .param p3, "serverUris"    # Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .param p4, "nsClient"    # Lcom/google/apps/dots/android/newsstand/http/NSClient;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->cacheLock:Ljava/lang/Object;

    .line 61
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->configFutures:Ljava/util/Map;

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->appContext:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 66
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 67
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->nsClient:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    .line 68
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->appContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->getVersionCode(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->appVersion:I

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    .prologue
    .line 46
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->appVersion:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "x2"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->requestConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->onNewConfig(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->appContext:Landroid/content/Context;

    return-object v0
.end method

.method private addRequestFutureToMapIfAbsent(Landroid/accounts/Account;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    .local p2, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->cacheLock:Ljava/lang/Object;

    monitor-enter v1

    .line 146
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->configFutures:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->configFutures:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    :cond_0
    monitor-exit v1

    .line 150
    return-object p2

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getMemoryCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v3, 0x0

    .line 72
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->cacheLock:Ljava/lang/Object;

    monitor-enter v4

    .line 73
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->configFutures:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 74
    .local v0, "configFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v2

    if-nez v2, :cond_1

    .line 75
    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v3

    .line 84
    :goto_0
    return-object v2

    .line 78
    :cond_1
    :try_start_1
    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v4

    goto :goto_0

    .line 86
    .end local v0    # "configFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 79
    .restart local v0    # "configFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    :catch_0
    move-exception v1

    .line 80
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_3
    monitor-exit v4

    move-object v2, v3

    goto :goto_0

    .line 81
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 82
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    monitor-exit v4

    move-object v2, v3

    goto :goto_0

    .line 83
    .end local v1    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v1

    .line 84
    .local v1, "e":Ljava/util/concurrent/CancellationException;
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v2, v3

    goto :goto_0
.end method

.method private onNewConfig(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "clientConfig"    # Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .prologue
    const/4 v4, 0x0

    .line 285
    sget-object v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "New client config, %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->toStringer(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 286
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->putMemoryCachedConfig(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V

    .line 288
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->encodeBase64(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setClientConfigString(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v1, "areMagazinesAvailable"

    .line 292
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getEnableMagazines()Z

    move-result v2

    .line 291
    invoke-virtual {v0, p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 294
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getDisableNewsstand()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    invoke-static {v4}, Ljava/lang/System;->exit(I)V

    .line 299
    :cond_0
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getSyncFrequencyMillis()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->updatePeriodicSync(Landroid/accounts/Account;J)V

    .line 300
    return-void
.end method

.method private putMemoryCachedConfig(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "config"    # Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .prologue
    .line 93
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->cacheLock:Ljava/lang/Object;

    monitor-enter v1

    .line 95
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->configFutures:Ljava/util/Map;

    invoke-static {p2}, Lcom/google/common/util/concurrent/Futures;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    monitor-exit v1

    .line 97
    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private requestConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 22
    .param p1, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "background"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    move-object/from16 v19, v0

    .line 221
    .local v19, "account":Landroid/accounts/Account;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v5, "gcmRegistrationId"

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 223
    .local v20, "gcmRegId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v5, "gcmRegisteredForUser2"

    const/4 v6, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v4, v0, v5, v6}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Landroid/accounts/Account;Ljava/lang/String;Z)Z

    move-result v17

    .line 225
    .local v17, "gcmRegisteredForUser":Z
    if-eqz p2, :cond_0

    sget-object v7, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    .line 227
    .local v7, "priority":Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;
    :goto_0
    if-eqz v20, :cond_1

    if-nez v17, :cond_1

    .line 228
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v4

    if-nez v4, :cond_1

    const/16 v21, 0x1

    .line 230
    .local v21, "postGcmReg":Z
    :goto_1
    if-nez v21, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 231
    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getReportShowedOnboardQuiz(Landroid/accounts/Account;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v18, 0x1

    .line 233
    .local v18, "reportOnboardQuizCompleted":Z
    :goto_2
    if-eqz v21, :cond_3

    .line 235
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getConfigUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 236
    .local v3, "gcmRegistrationUrl":Ljava/lang/String;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    const/4 v4, 0x0

    new-array v4, v4, [B

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;-><init>(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;Ljava/util/Locale;)V

    .line 248
    .end local v3    # "gcmRegistrationUrl":Ljava/lang/String;
    .local v2, "clientRequest":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->nsClient:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    const/4 v5, 0x0

    .line 249
    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v2, v5}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->request(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v12, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;

    move-object/from16 v13, p0

    move-object v14, v2

    move-object/from16 v15, v19

    move/from16 v16, v21

    invoke-direct/range {v12 .. v18}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$2;-><init>(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;Landroid/accounts/Account;ZZZ)V

    .line 248
    invoke-static {v4, v12}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    return-object v4

    .line 225
    .end local v2    # "clientRequest":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    .end local v7    # "priority":Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;
    .end local v18    # "reportOnboardQuizCompleted":Z
    .end local v21    # "postGcmReg":Z
    :cond_0
    sget-object v7, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    goto :goto_0

    .line 228
    .restart local v7    # "priority":Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;
    :cond_1
    const/16 v21, 0x0

    goto :goto_1

    .line 231
    .restart local v21    # "postGcmReg":Z
    :cond_2
    const/16 v18, 0x0

    goto :goto_2

    .line 238
    .restart local v18    # "reportOnboardQuizCompleted":Z
    :cond_3
    if-eqz v18, :cond_4

    .line 239
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    const/4 v5, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v4, v0, v5}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getConfigUrl(Landroid/accounts/Account;Z)Ljava/lang/String;

    move-result-object v9

    .line 240
    .local v9, "onboardQuizCompletedUrl":Ljava/lang/String;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    const/4 v4, 0x0

    new-array v10, v4, [B

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object v13, v7

    invoke-direct/range {v8 .. v14}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;-><init>(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;Ljava/util/Locale;)V

    .line 242
    .restart local v2    # "clientRequest":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    goto :goto_3

    .line 244
    .end local v2    # "clientRequest":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    .end local v9    # "onboardQuizCompletedUrl":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getConfigUrl(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v11

    .line 245
    .local v11, "configUrl":Ljava/lang/String;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    move-object v10, v2

    move-object v15, v7

    invoke-direct/range {v10 .. v16}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;-><init>(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;Ljava/util/Locale;)V

    .restart local v2    # "clientRequest":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    goto :goto_3
.end method

.method private updateGCMAndGetFreshConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p1, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "background"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 180
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    .line 181
    .local v3, "account":Landroid/accounts/Account;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v1, "gcmRegistrationId"

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 182
    .local v2, "gcmRegId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v1, "gcmRegIdAppVersion"

    invoke-virtual {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 185
    .local v6, "gcmAppVersion":I
    if-eqz v2, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->appVersion:I

    if-eq v0, v6, :cond_1

    .line 186
    :cond_0
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-nez v0, :cond_1

    .line 187
    sget-object v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Need to get GCM registration"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->registerWithGcm()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    new-instance v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$1;-><init>(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)V

    .line 189
    invoke-static {v7, v0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 207
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->requestConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getMemoryCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v1

    .line 104
    .local v1, "clientConfig":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    if-eqz v1, :cond_0

    move-object v3, v1

    .line 117
    :goto_0
    return-object v3

    .line 107
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v3, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getClientConfigString(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, "encodedClientConfig":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 110
    :try_start_0
    new-instance v3, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    invoke-direct {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;-><init>()V

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->decodeBase64(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-object v1, v0

    .line 111
    invoke-direct {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->putMemoryCachedConfig(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v1

    .line 112
    goto :goto_0

    .line 113
    :catch_0
    move-exception v3

    .line 117
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getCachedOrFreshConfigFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "background"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    .line 128
    .local v0, "account":Landroid/accounts/Account;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .line 129
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->cacheLock:Ljava/lang/Object;

    monitor-enter v3

    .line 130
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->configFutures:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 131
    .local v1, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->wasFailure(Ljava/util/concurrent/Future;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 132
    monitor-exit v3

    .line 135
    .end local v1    # "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    :goto_0
    return-object v1

    .restart local v1    # "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getFreshConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .end local v1    # "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    monitor-exit v3

    goto :goto_0

    .line 136
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getFreshConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "background"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    .line 167
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->updateGCMAndGetFreshConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 166
    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->addRequestFutureToMapIfAbsent(Landroid/accounts/Account;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public hasCachedConfig(Landroid/accounts/Account;)Z
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getMemoryCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 158
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getClientConfigString(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerWithGcm()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSCLIENT_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$3;-><init>(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->submit(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public reportOnboardQuizCompleted(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setReportShowedOnboardQuiz(Landroid/accounts/Account;Z)V

    .line 175
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->requestConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
