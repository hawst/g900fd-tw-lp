.class final Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState$1;
.super Ljava/lang/Object;
.source "NewsArticlePagerFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 81
    const-class v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 82
    .local v1, "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    const-class v2, Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    .line 83
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    .line 85
    .local v0, "articleState":Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    invoke-direct {v2, v1, v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 90
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    move-result-object v0

    return-object v0
.end method
