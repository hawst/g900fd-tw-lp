.class public Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;
.super Ljava/lang/Object;
.source "LoadStateHelper.java"


# instance fields
.field private eventHandler:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;

.field private loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

.field private loadedRunnable:Ljava/lang/Runnable;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 18
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->view:Landroid/view/View;

    .line 19
    return-void
.end method


# virtual methods
.method public getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    return-object v0
.end method

.method public isLoaded()Z
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;)V
    .locals 2
    .param p1, "loadState"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .prologue
    .line 36
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;ZLjava/lang/Throwable;)V

    .line 37
    return-void
.end method

.method public setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "loadState"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;ZLjava/lang/Throwable;)V

    .line 41
    return-void
.end method

.method public setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;ZLjava/lang/Throwable;)V
    .locals 2
    .param p1, "loadState"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .param p2, "strict"    # Z
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 44
    if-eqz p2, :cond_4

    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-eq v0, p1, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 49
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->eventHandler:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->eventHandler:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->view:Landroid/view/View;

    invoke-interface {v0, v1, p1, p3}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;->onLoadStateChanged(Landroid/view/View;Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;Ljava/lang/Throwable;)V

    .line 53
    :cond_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadedRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    .line 54
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadedRunnable:Ljava/lang/Runnable;

    .line 57
    :cond_2
    :goto_1
    return-void

    .line 45
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 46
    :cond_4
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne v0, p1, :cond_0

    goto :goto_1
.end method

.method public setLoadStateEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V
    .locals 0
    .param p1, "eventHandler"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->eventHandler:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;

    .line 61
    return-void
.end method

.method public setLoadedRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "loadedRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadedRunnable:Ljava/lang/Runnable;

    .line 27
    return-void
.end method

.method public stealLoadedRunnable()Ljava/lang/Runnable;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadedRunnable:Ljava/lang/Runnable;

    .line 31
    .local v0, "loadedRunnable":Ljava/lang/Runnable;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->loadedRunnable:Ljava/lang/Runnable;

    .line 32
    return-object v0
.end method
