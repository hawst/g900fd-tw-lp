.class public final enum Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
.super Ljava/lang/Enum;
.source "ProtoEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/util/ProtoEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LinkType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

.field public static final enum APPLICATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

.field public static final enum APPLICATION_FAMILY_SUMMARY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

.field public static final enum APPLICATION_SUMMARY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

.field public static final enum ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

.field public static final enum COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

.field public static final enum FORM:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

.field public static final enum FORM_TEMPLATE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

.field public static final enum LAYOUT_LINK:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

.field public static final enum POST:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

.field public static final enum SECTION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;


# instance fields
.field private linkType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 89
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const-string v1, "APPLICATION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v7}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->APPLICATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 90
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const-string v1, "POST"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v4, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->POST:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 91
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const-string v1, "SECTION"

    invoke-direct {v0, v1, v5, v8}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->SECTION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 92
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const-string v1, "FORM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v6, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->FORM:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 93
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const-string v1, "ATTACHMENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v7, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 94
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const-string v1, "COLLECTION_ROOT"

    invoke-direct {v0, v1, v8, v4}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 95
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const-string v1, "APPLICATION_FAMILY_SUMMARY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->APPLICATION_FAMILY_SUMMARY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 96
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const-string v1, "APPLICATION_SUMMARY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v6}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->APPLICATION_SUMMARY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 97
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const-string v1, "FORM_TEMPLATE"

    const/16 v2, 0x8

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->FORM_TEMPLATE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 98
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const-string v1, "LAYOUT_LINK"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->LAYOUT_LINK:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 88
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->APPLICATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->POST:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->SECTION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->FORM:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->APPLICATION_FAMILY_SUMMARY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->APPLICATION_SUMMARY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->FORM_TEMPLATE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->LAYOUT_LINK:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "linkType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 113
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->linkType:I

    .line 114
    return-void
.end method

.method public static fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .locals 5
    .param p0, "linkType"    # I

    .prologue
    .line 104
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 105
    .local v0, "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    iget v4, v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->linkType:I

    if-ne v4, p0, :cond_0

    .line 109
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    :goto_1
    return-object v0

    .line 104
    .restart local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 109
    .end local v0    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 88
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    return-object v0
.end method
