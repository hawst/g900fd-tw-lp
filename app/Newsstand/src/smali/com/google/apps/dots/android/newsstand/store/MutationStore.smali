.class public Lcom/google/apps/dots/android/newsstand/store/MutationStore;
.super Ljava/lang/Object;
.source "MutationStore.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final backendSimulator:Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

.field private cleanupRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

.field private final eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

.field private lockSpace:Lcom/google/common/cache/LoadingCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/cache/LoadingCache",
            "<",
            "Ljava/io/File;",
            "Lcom/google/apps/dots/android/newsstand/store/AsyncLock;",
            ">;"
        }
    .end annotation
.end field

.field private final mutationLogsDir:Ljava/io/File;

.field private final nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field private final retryPolicy:Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;

.field private final storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;Lcom/google/apps/dots/android/newsstand/events/EventNotifier;)V
    .locals 3
    .param p1, "appContext"    # Landroid/content/Context;
    .param p2, "nsStore"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p3, "storeCache"    # Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    .param p4, "eventNotifier"    # Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->retryPolicy:Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;

    .line 73
    invoke-static {}, Lcom/google/common/cache/CacheBuilder;->newBuilder()Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/cache/CacheBuilder;->weakValues()Lcom/google/common/cache/CacheBuilder;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/MutationStore$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)V

    invoke-virtual {v0, v1}, Lcom/google/common/cache/CacheBuilder;->build(Lcom/google/common/cache/CacheLoader;)Lcom/google/common/cache/LoadingCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->lockSpace:Lcom/google/common/cache/LoadingCache;

    .line 81
    new-instance v0, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 82
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/MutationStore$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$2;-><init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->cleanupRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 92
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 93
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->backendSimulator:Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

    .line 94
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "mutationLogs"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutationLogsDir:Ljava/io/File;

    .line 95
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    .line 96
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->cleanup()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Landroid/accounts/Account;Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getMutationLogFile(Landroid/accounts/Account;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->retryPolicy:Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Ljava/io/File;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;
    .param p1, "x1"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getMutationLog(Ljava/io/File;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->backendSimulator:Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

    return-object v0
.end method

.method static synthetic access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/common/cache/LoadingCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->lockSpace:Lcom/google/common/cache/LoadingCache;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/events/EventNotifier;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->isBadMutationLog(Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;)Z

    move-result v0

    return v0
.end method

.method private cleanup()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 428
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutationLogsDir:Ljava/io/File;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/store/MutationStore$6;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$6;-><init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)V

    invoke-virtual {v2, v4}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    .line 434
    .local v0, "files":[Ljava/io/File;
    if-nez v0, :cond_1

    .line 441
    :cond_0
    return-void

    .line 437
    :cond_1
    array-length v4, v0

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v0, v2

    .line 438
    .local v1, "mutLogFile":Ljava/io/File;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Cleanup: found %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v3

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 439
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->upload(Ljava/io/File;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 437
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private getMutationLog(Ljava/io/File;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .locals 11
    .param p1, "mutLogFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 401
    invoke-static {}, Lcom/google/common/io/Closer;->create()Lcom/google/common/io/Closer;

    move-result-object v2

    .line 402
    .local v2, "closer":Lcom/google/common/io/Closer;
    const-string v6, "MutStore-loading"

    const-string v7, "log %s"

    new-array v8, v8, [Ljava/lang/Object;

    aput-object p1, v8, v9

    invoke-static {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 404
    const/4 v4, 0x0

    .line 405
    .local v4, "log":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 406
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2, v6}, Lcom/google/common/io/Closer;->register(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v5

    check-cast v5, Ljava/io/InputStream;

    .line 407
    .local v5, "mutLogIn":Ljava/io/InputStream;
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v8

    long-to-int v7, v8

    invoke-static {v6, v5, v7}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->readFromStream(Lcom/google/protobuf/nano/MessageNano;Ljava/io/InputStream;I)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    move-object v4, v0

    .line 408
    sget-object v6, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "%s: found mutation log, %d actions"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 409
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getBatchEndpointUri()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, v4, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v10, v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    .line 408
    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418
    .end local v5    # "mutLogIn":Ljava/io/InputStream;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 419
    invoke-virtual {v2}, Lcom/google/common/io/Closer;->close()V

    return-object v4

    .line 412
    :catch_0
    move-exception v3

    .line 414
    .local v3, "e":Ljava/lang/Throwable;
    :try_start_1
    sget-object v6, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Mutation log corrupted"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 415
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 416
    invoke-virtual {v2, v3}, Lcom/google/common/io/Closer;->rethrow(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v6

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418
    .end local v3    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v6

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 419
    invoke-virtual {v2}, Lcom/google/common/io/Closer;->close()V

    throw v6
.end method

.method private getMutationLogFile(Landroid/accounts/Account;Ljava/lang/String;)Ljava/io/File;
    .locals 7
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 392
    const-string v2, "%s.%s"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 393
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getOriginalName(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 392
    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->filenameSafeStringHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x1

    const-string v4, "mut"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 394
    .local v0, "filename":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutationLogsDir:Ljava/io/File;

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1

    .line 393
    .end local v0    # "filename":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isBadMutationLog(Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;)Z
    .locals 2
    .param p1, "mutationLog"    # Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    .prologue
    .line 376
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getBatchEndpointUri()Ljava/lang/String;

    move-result-object v0

    .line 377
    .local v0, "batchEndpointUri":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "read-states/null"

    .line 382
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 383
    :cond_0
    const/4 v1, 0x1

    .line 385
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private upload(Ljava/io/File;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1, "mutLogFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 266
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "%s: Uploading mutations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->lockSpace:Lcom/google/common/cache/LoadingCache;

    invoke-interface {v0, p1}, Lcom/google/common/cache/LoadingCache;->getUnchecked(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->STORE_MUTATION:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v1, p0, v2, p1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;-><init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Ljava/util/concurrent/Executor;Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->with(Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 117
    sget-object v2, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "%s: getting."

    new-array v4, v0, [Ljava/lang/Object;

    iget-object v5, p2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    if-ne v2, v3, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 120
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 121
    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;-><init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    .line 120
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 119
    goto :goto_0
.end method

.method public getAny(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v0, p2, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->anyVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public getAvailable(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v0, p2, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->availableVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public getFresh(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v0, p2, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->freshVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public getReallyFresh(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v0, p2, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->reallyFreshVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "mutation"    # Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreMutation;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->id:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getMutationLogFile(Landroid/accounts/Account;Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    .line 194
    .local v5, "mutLogFile":Ljava/io/File;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "%s: submitting mutation %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p2, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->id:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p2, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->action:Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getUri()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 195
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->lockSpace:Lcom/google/common/cache/LoadingCache;

    invoke-interface {v0, v5}, Lcom/google/common/cache/LoadingCache;->getUnchecked(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->STORE_MUTATION:Lcom/google/apps/dots/android/newsstand/async/Queue;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;-><init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;Landroid/accounts/Account;Ljava/io/File;)V

    invoke-virtual {v6, v0}, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->with(Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public requestCleanup(J)V
    .locals 3
    .param p1, "delay"    # J

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->cleanupRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 425
    return-void
.end method
