.class public Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;
.super Ljava/lang/Object;
.source "NewsBridgeEventHandler.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;


# instance fields
.field private newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

.field private newsWebView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;)V
    .locals 0
    .param p1, "newsWebView"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    .param p2, "newsBridgeResponder"    # Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsWebView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .line 18
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .line 19
    return-void
.end method

.method private getWebViewContainingActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsWebView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bind(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;

    .prologue
    .line 161
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsWebView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .line 27
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .line 28
    return-void
.end method

.method public gotoPage(I)V
    .locals 0
    .param p1, "pageNumber"    # I

    .prologue
    .line 44
    return-void
.end method

.method public onLayoutChange(IZII)V
    .locals 1
    .param p1, "pageCount"    # I
    .param p2, "isDone"    # Z
    .param p3, "pageWidth"    # I
    .param p4, "pageHeight"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsWebView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onLayoutChange(IZII)V

    .line 39
    return-void
.end method

.method public onScriptLoad()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsWebView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoad()V

    .line 33
    return-void
.end method

.method public openAudio(Ljava/lang/String;)V
    .locals 2
    .param p1, "audioUri"    # Ljava/lang/String;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->getWebViewContainingActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->openAudio(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public openDrawer(Ljava/lang/String;IZ)V
    .locals 2
    .param p1, "fieldId"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .param p3, "restrictToSingleField"    # Z

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->getWebViewContainingActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->openDrawer(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;IZ)V

    .line 83
    return-void
.end method

.method public openOriginalUrl(Ljava/lang/String;)V
    .locals 2
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->getWebViewContainingActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->openOriginalUrl(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public openPlayStoreDoc(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "docTypeValue"    # I
    .param p2, "backendDocId"    # Ljava/lang/String;
    .param p3, "parentBackendDocId"    # Ljava/lang/String;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->getWebViewContainingActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->openPlayStoreDoc(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;ILjava/lang/String;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public pauseAudio()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->getWebViewContainingActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->pauseAudio(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    .line 98
    return-void
.end method

.method public requestPurchase(Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->getWebViewContainingActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->requestPurchase(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method public requestPurchaseInContext(IILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8
    .param p1, "backendId"    # I
    .param p2, "docTypeValue"    # I
    .param p3, "backendDocId"    # Ljava/lang/String;
    .param p4, "fullDocId"    # Ljava/lang/String;
    .param p5, "offerType"    # I
    .param p6, "parentBackendDocId"    # Ljava/lang/String;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .line 118
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->getWebViewContainingActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    .line 117
    invoke-virtual/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->requestPurchaseInContext(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;IILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 125
    return-void
.end method

.method public sendAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "pageIndex"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->sendAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 138
    return-void
.end method

.method public sendCustomAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 7
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "pageIndex"    # I
    .param p6, "dimensionsMetricsJson"    # Ljava/lang/String;

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->sendCustomAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 153
    return-void
.end method

.method public switchToApp(Ljava/lang/String;)V
    .locals 2
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->getWebViewContainingActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->switchToApp(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public switchToArticle(Ljava/lang/String;)V
    .locals 2
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->getWebViewContainingActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->switchToArticle(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public switchToSection(Ljava/lang/String;)V
    .locals 0
    .param p1, "sectionId"    # Ljava/lang/String;

    .prologue
    .line 66
    return-void
.end method

.method public switchToToc()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public toggleActionBar()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsWebView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->toggleActionBar()V

    .line 49
    return-void
.end method

.method public toggleFullScreen()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public watchVideo(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->newsBridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;->getWebViewContainingActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->watchVideo(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V

    .line 88
    return-void
.end method
