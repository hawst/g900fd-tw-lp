.class public final Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$DataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SectionData"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;


# instance fields
.field public sectionId:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8398
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8399
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 8402
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    .line 8399
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;
    .locals 3

    .prologue
    .line 8414
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8418
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_0

    .line 8419
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    invoke-virtual {v2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    .line 8421
    :cond_0
    return-object v0

    .line 8415
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;
    :catch_0
    move-exception v1

    .line 8416
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 8395
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 8426
    if-ne p1, p0, :cond_0

    const/4 v1, 0x1

    .line 8429
    :goto_0
    return v1

    .line 8427
    :cond_0
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 8428
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    .line 8429
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    iget-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 8455
    const/4 v2, 0x0

    .line 8456
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 8457
    const/4 v0, 0x0

    .line 8458
    .local v0, "dataSize":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v1, v4, v3

    .line 8460
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 8458
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 8462
    .end local v1    # "element":Ljava/lang/String;
    :cond_0
    add-int/2addr v2, v0

    .line 8463
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    .line 8465
    .end local v0    # "dataSize":I
    :cond_1
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->cachedSize:I

    .line 8466
    return v2
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 8433
    const/16 v1, 0x11

    .line 8434
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 8435
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    if-nez v2, :cond_1

    mul-int/lit8 v1, v1, 0x1f

    .line 8441
    :cond_0
    return v1

    .line 8437
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 8438
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    add-int v1, v3, v2

    .line 8437
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8438
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 8474
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 8475
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 8479
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 8480
    :sswitch_0
    return-object p0

    .line 8485
    :sswitch_1
    const/16 v4, 0xa

    invoke-static {p1, v4}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 8486
    .local v0, "arrayLength":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    array-length v1, v4

    .line 8487
    .local v1, "i":I
    add-int v4, v1, v0

    new-array v2, v4, [Ljava/lang/String;

    .line 8488
    .local v2, "newArray":[Ljava/lang/String;
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    invoke-static {v4, v6, v2, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 8489
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    .line 8490
    :goto_1
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_1

    .line 8491
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 8492
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 8490
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 8495
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    goto :goto_0

    .line 8475
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8395
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8446
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 8447
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->sectionId:[Ljava/lang/String;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 8448
    .local v0, "element":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 8447
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 8451
    .end local v0    # "element":Ljava/lang/String;
    :cond_0
    return-void
.end method
