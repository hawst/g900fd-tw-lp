.class final Lcom/google/apps/dots/android/newsstand/card/CardContinueReading$1;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardContinueReading.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardContinueReading;->createCardData(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Z)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$inLiteMode:Z

.field final synthetic val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Z)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading$1;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading$1;->val$inLiteMode:Z

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 56
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v0, p2, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading$1;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v1, v1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 57
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading$1;->val$inLiteMode:Z

    .line 58
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setInLiteMode(Z)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v0

    const/16 v1, 0xc8

    .line 59
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->startForResult(I)V

    .line 60
    return-void
.end method
