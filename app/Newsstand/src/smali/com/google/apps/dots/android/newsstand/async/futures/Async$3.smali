.class final Lcom/google/apps/dots/android/newsstand/async/futures/Async$3;
.super Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnSuccess;
.source "Async.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/Async;->asTransform(Lcom/google/common/base/Function;)Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnSuccess",
        "<TI;TO;>;"
    }
.end annotation


# instance fields
.field final synthetic val$function:Lcom/google/common/base/Function;


# direct methods
.method constructor <init>(Lcom/google/common/base/Function;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$3;->val$function:Lcom/google/common/base/Function;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnSuccess;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TI;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TO;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 135
    .local p1, "input":Ljava/lang/Object;, "TI;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$3;->val$function:Lcom/google/common/base/Function;

    invoke-interface {v0, p1}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
