.class public Lcom/google/apps/dots/android/newsstand/media/VideoActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
.source "VideoActivity.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x9
.end annotation


# instance fields
.field private autoStart:Z

.field private baseLayout:Landroid/widget/FrameLayout;

.field private bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

.field private closeOnCompletion:Z

.field private mediaView:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

.field private postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

.field private shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

.field private streamingProgress:I

.field private videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;-><init>(Z)V

    .line 65
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->streamingProgress:I

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/VideoActivity;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)Lcom/google/apps/dots/android/newsstand/media/VideoItem;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;Lcom/google/apps/dots/android/newsstand/media/VideoItem;)Lcom/google/apps/dots/android/newsstand/media/VideoItem;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/VideoActivity;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->setupPlayer()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    .prologue
    .line 44
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->streamingProgress:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/VideoActivity;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->streamingProgress:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->closeOnCompletion:Z

    return v0
.end method

.method private setupMenuHelpers()V
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    .line 96
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    .line 97
    return-void
.end method

.method private setupPlayer()V
    .locals 7

    .prologue
    const/4 v5, -0x1

    .line 158
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/media/VideoItem;->value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 162
    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;-><init>(Landroid/app/Activity;)V

    const-string v3, "http://www.youtube.com/watch?v="

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/media/VideoItem;->value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 163
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->getServiceId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v4, v3}, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->setUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/media/VideoItem;->value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 164
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->getServiceId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->setVideoId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->closeOnCompletion:Z

    .line 165
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->setCloseOnCompletion(Z)Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;

    move-result-object v3

    const/4 v4, 0x0

    .line 166
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->start(Z)V

    .line 167
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/media/VideoItem;->postId:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->markPostAsRead(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->finish()V

    .line 206
    :cond_0
    :goto_1
    return-void

    .line 163
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 171
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/media/VideoItem;->value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 172
    new-instance v3, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$2;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v4

    invoke-direct {v3, p0, p0, v4}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$2;-><init>(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->mediaView:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .line 189
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 191
    .local v1, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v3, 0x11

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 192
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->mediaView:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 193
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->baseLayout:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->mediaView:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v3, v4, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 195
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/media/VideoItem;->value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getStreamingVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    move-result-object v2

    .line 196
    .local v2, "video":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "attachmentId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 198
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->mediaView:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->setAttachmentId(Ljava/lang/String;)V

    .line 202
    :goto_2
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->autoStart:Z

    if-eqz v3, :cond_0

    .line 203
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->mediaView:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->start()V

    goto :goto_1

    .line 200
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->mediaView:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getOriginalUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->setVideoUri(Landroid/net/Uri;)V

    goto :goto_2
.end method

.method private setupVideoItem()V
    .locals 6

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "videoItem"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    .line 101
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    if-nez v3, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "postId"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 103
    .local v2, "postId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "owningEdition"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 105
    .local v1, "owningEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 106
    .local v0, "futures":Ljava/util/List;, "Ljava/util/List<Lcom/google/common/util/concurrent/ListenableFuture<*>;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appSummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 107
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList(Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;-><init>(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)V

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 151
    .end local v0    # "futures":Ljava/util/List;, "Ljava/util/List<Lcom/google/common/util/concurrent/ListenableFuture<*>;>;"
    .end local v1    # "owningEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v2    # "postId":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    if-eqz v3, :cond_1

    .line 152
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->setupPlayer()V

    .line 154
    :cond_1
    return-void
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x4

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 273
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public markPostAsRead(Ljava/lang/String;)V
    .locals 4
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "readingEdition"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 264
    .local v1, "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "owningEdition"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 265
    .local v0, "owningEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-eqz v1, :cond_0

    .line 266
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 267
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v2

    .line 266
    invoke-static {v2, v1, v0, p1}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    .line 269
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 73
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->baseLayout:Landroid/widget/FrameLayout;

    .line 76
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->baseLayout:Landroid/widget/FrameLayout;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->videoPlayer:I

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setId(I)V

    .line 77
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 78
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 79
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->baseLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->baseLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->setContentView(Landroid/view/View;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "autoStart"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->autoStart:Z

    .line 83
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "closeOnCompletion"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->closeOnCompletion:Z

    .line 84
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->setupVideoItem()V

    .line 85
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->setupMenuHelpers()V

    .line 86
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 235
    .local v0, "menuInflater":Landroid/view/MenuInflater;
    sget v1, Lcom/google/android/apps/newsstanddev/R$menu;->video_activity_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 236
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onDestroy()V

    .line 91
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->onDestroyView()V

    .line 92
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 249
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 250
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->finish()V

    .line 259
    :goto_0
    return v0

    .line 252
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->menu_save:I

    if-ne v1, v2, :cond_1

    .line 253
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-static {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil;->savePost(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    goto :goto_0

    .line 255
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->menu_unsave:I

    if-ne v1, v2, :cond_2

    .line 256
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-static {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil;->unsavePost(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    goto :goto_0

    .line 259
    :cond_2
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/media/VideoItem;->value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->mediaView:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->streamingProgress:I

    .line 215
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onPause()V

    .line 216
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "owningEdition"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 242
    .local v0, "owningEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    invoke-virtual {v1, p1, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 243
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {v1, p1, v0, v2}, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    .line 244
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 220
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onResume()V

    .line 222
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/media/VideoItem;->value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->mediaView:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->start()V

    .line 225
    :cond_0
    return-void
.end method
