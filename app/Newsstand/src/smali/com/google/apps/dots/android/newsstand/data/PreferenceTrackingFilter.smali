.class public abstract Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;
.super Lcom/google/android/libraries/bind/data/InvalidatingFilter;
.source "PreferenceTrackingFilter.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/DataList$DataListListener;


# instance fields
.field private prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

.field private final preferenceKeys:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/Queue;Z[Ljava/lang/String;)V
    .locals 0
    .param p1, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;
    .param p2, "isReadOnly"    # Z
    .param p3, "preferenceKeys"    # [Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/data/InvalidatingFilter;-><init>(Lcom/google/android/libraries/bind/async/Queue;Z)V

    .line 22
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->preferenceKeys:[Ljava/lang/String;

    .line 23
    return-void
.end method

.method private registerPreferencesListeners()V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 48
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter$1;-><init>(Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->preferenceKeys:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 54
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private unregisterPreferencesListeners()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 58
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 60
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onDataListRegisteredForInvalidation()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->registerPreferencesListeners()V

    .line 39
    return-void
.end method

.method public onDataListUnregisteredForInvalidation()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->unregisterPreferencesListeners()V

    .line 44
    return-void
.end method

.method public onNewDataList(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/data/InvalidatingFilter;->onNewDataList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 28
    if-eqz p1, :cond_0

    .line 29
    invoke-virtual {p1, p0}, Lcom/google/android/libraries/bind/data/DataList;->addListener(Lcom/google/android/libraries/bind/data/DataList$DataListListener;)V

    .line 30
    invoke-virtual {p1}, Lcom/google/android/libraries/bind/data/DataList;->isRegisteredForInvalidation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;->registerPreferencesListeners()V

    .line 34
    :cond_0
    return-void
.end method
