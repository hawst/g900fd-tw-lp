.class public final Lcom/google/apps/dots/proto/client/DotsShared$RoleList;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RoleList"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$RoleList;


# instance fields
.field public roleId:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25779
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25780
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 25783
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    .line 25780
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$RoleList;
    .locals 3

    .prologue
    .line 25795
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25799
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$RoleList;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_0

    .line 25800
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    invoke-virtual {v2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    .line 25802
    :cond_0
    return-object v0

    .line 25796
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$RoleList;
    :catch_0
    move-exception v1

    .line 25797
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25776
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->clone()Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 25807
    if-ne p1, p0, :cond_0

    const/4 v1, 0x1

    .line 25810
    :goto_0
    return v1

    .line 25808
    :cond_0
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 25809
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .line 25810
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$RoleList;
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    iget-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 25836
    const/4 v2, 0x0

    .line 25837
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 25838
    const/4 v0, 0x0

    .line 25839
    .local v0, "dataSize":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v1, v4, v3

    .line 25841
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 25839
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 25843
    .end local v1    # "element":Ljava/lang/String;
    :cond_0
    add-int/2addr v2, v0

    .line 25844
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    .line 25846
    .end local v0    # "dataSize":I
    :cond_1
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->cachedSize:I

    .line 25847
    return v2
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 25814
    const/16 v1, 0x11

    .line 25815
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 25816
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    if-nez v2, :cond_1

    mul-int/lit8 v1, v1, 0x1f

    .line 25822
    :cond_0
    return v1

    .line 25818
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 25819
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    add-int v1, v3, v2

    .line 25818
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25819
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$RoleList;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 25855
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 25856
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 25860
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 25861
    :sswitch_0
    return-object p0

    .line 25866
    :sswitch_1
    const/16 v4, 0xa

    invoke-static {p1, v4}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 25867
    .local v0, "arrayLength":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    array-length v1, v4

    .line 25868
    .local v1, "i":I
    add-int v4, v1, v0

    new-array v2, v4, [Ljava/lang/String;

    .line 25869
    .local v2, "newArray":[Ljava/lang/String;
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    invoke-static {v4, v6, v2, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 25870
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    .line 25871
    :goto_1
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_1

    .line 25872
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 25873
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 25871
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 25876
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    goto :goto_0

    .line 25856
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25776
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25827
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25828
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 25829
    .local v0, "element":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 25828
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 25832
    .end local v0    # "element":Ljava/lang/String;
    :cond_0
    return-void
.end method
