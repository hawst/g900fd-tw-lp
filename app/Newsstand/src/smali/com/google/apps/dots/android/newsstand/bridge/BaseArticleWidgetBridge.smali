.class public abstract Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;
.super Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;
.source "BaseArticleWidgetBridge.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field protected app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

.field protected post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

.field protected section:Lcom/google/apps/dots/proto/client/DotsShared$Section;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "context"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "articleWidget"    # Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "readingActivity"    # Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;
    .param p5, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
    .param p6, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 38
    invoke-direct/range {p0 .. p6}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 39
    return-void
.end method


# virtual methods
.method public clearReferences()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->clearReferences()V

    .line 50
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    .line 51
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 52
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 53
    return-void
.end method

.method public openDrawer(Ljava/lang/String;IZ)V
    .locals 12
    .param p1, "fieldId"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .param p3, "restrictToSingleField"    # Z
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v7, 0x0

    .line 59
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-nez v6, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 63
    sget-object v6, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "openDrawer - unspecified fieldId: %s"

    new-array v9, v11, [Ljava/lang/Object;

    aput-object p1, v9, v7

    invoke-virtual {v6, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 66
    :cond_2
    if-gez p2, :cond_3

    .line 67
    sget-object v6, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "openDrawer - invalid offset: %d"

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v7

    invoke-virtual {v6, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 70
    :cond_3
    const/4 v4, 0x0

    .line 71
    .local v4, "primaryImageId":Ljava/lang/String;
    const-string v6, "primaryImage"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->hasSummary()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 72
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasPrimaryImage()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 73
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v4

    .line 75
    :cond_4
    const/4 v2, 0x0

    .line 76
    .local v2, "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v8, v6, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v9, v8

    move v6, v7

    :goto_1
    if-ge v6, v9, :cond_6

    aget-object v1, v8, v6

    .line 77
    .local v1, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget-object v10, v1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    if-eqz v4, :cond_9

    .line 78
    :cond_5
    iget-object v10, v1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v5, v10, p2

    .line 81
    .local v5, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v4, :cond_c

    .line 82
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasImage()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 83
    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .end local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v6, v6, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget-object v8, v1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-direct {v2, v6, p2, v8, v5}, Lcom/google/apps/dots/android/newsstand/media/MediaItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .line 108
    .end local v1    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .end local v5    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    .restart local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    :cond_6
    :goto_2
    if-eqz v2, :cond_10

    .line 109
    move-object v3, v2

    .line 110
    .local v3, "mediaItemToShow":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    new-instance v6, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;

    invoke-direct {v6, p0, v3, p3}, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;Lcom/google/apps/dots/android/newsstand/media/MediaItem;Z)V

    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->safePostIfAllowed(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 85
    .end local v3    # "mediaItemToShow":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .restart local v1    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .restart local v5    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_7
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v10

    if-nez v10, :cond_8

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 86
    :cond_8
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 87
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "attachmentId":Ljava/lang/String;
    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 76
    .end local v0    # "attachmentId":Ljava/lang/String;
    .end local v5    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 88
    .restart local v5    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_a
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getStreamingVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 92
    .restart local v0    # "attachmentId":Ljava/lang/String;
    :cond_b
    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    .end local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v6, v6, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-direct {v2, v6, p2, p1, v5}, Lcom/google/apps/dots/android/newsstand/media/VideoItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .line 93
    .restart local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    goto :goto_2

    .line 98
    .end local v0    # "attachmentId":Ljava/lang/String;
    :cond_c
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasAudio()Z

    move-result v6

    if-eqz v6, :cond_d

    .line 99
    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    .end local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v6, v6, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-direct {v2, v6, p2, p1, v5}, Lcom/google/apps/dots/android/newsstand/media/AudioItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .restart local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    goto :goto_2

    .line 100
    :cond_d
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v6

    if-nez v6, :cond_e

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 101
    :cond_e
    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    .end local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v6, v6, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-direct {v2, v6, p2, p1, v5}, Lcom/google/apps/dots/android/newsstand/media/VideoItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .restart local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    goto :goto_2

    .line 103
    :cond_f
    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .end local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v6, v6, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-direct {v2, v6, p2, p1, v5}, Lcom/google/apps/dots/android/newsstand/media/MediaItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .line 105
    .restart local v2    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    goto :goto_2

    .line 123
    .end local v1    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .end local v5    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_10
    sget-object v6, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "openDrawer - invalid fieldId: %s"

    new-array v9, v11, [Ljava/lang/Object;

    aput-object p1, v9, v7

    invoke-virtual {v6, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public sendAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "pageIndex"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 131
    invoke-super/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->sendAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 132
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 137
    .local v1, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 138
    .local v2, "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    .line 157
    .local v0, "app":Lcom/google/apps/dots/proto/client/DotsShared$Application;
    goto :goto_0
.end method

.method public setData(Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Section;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 0
    .param p1, "app"    # Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .param p2, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;
    .param p3, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    .line 43
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 44
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 45
    return-void
.end method
