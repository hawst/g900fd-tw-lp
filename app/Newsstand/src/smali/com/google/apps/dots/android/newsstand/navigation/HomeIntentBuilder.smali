.class public Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "HomeIntentBuilder.java"


# instance fields
.field private final account:Landroid/accounts/Account;

.field private finishImmediately:Z

.field private homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

.field private libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

.field private readNowEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 26
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->DEFAULT_HOME_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 39
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->account:Landroid/accounts/Account;

    .line 40
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/content/Context;)V

    .line 26
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->DEFAULT_HOME_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 44
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->account:Landroid/accounts/Account;

    .line 45
    return-void
.end method

.method public static nonActivityMake(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 84
    const-class v2, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    .line 85
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 86
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    if-eqz v2, :cond_2

    .line 87
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->account:Landroid/accounts/Account;

    invoke-direct {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/home/HomePage;Landroid/accounts/Account;)V

    .line 88
    .local v0, "homeFragmentState":Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 89
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->readNowEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eqz v2, :cond_0

    .line 90
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "Cannot have a non-null perspective if the homePage is not ReadNow."

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 93
    :cond_0
    const-string v3, "ReadNowFragment_state"

    new-instance v4, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->readNowEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    :goto_0
    invoke-direct {v4, v2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 103
    :cond_1
    :goto_1
    const-string v2, "HomeFragment_state"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 105
    .end local v0    # "homeFragmentState":Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
    :cond_2
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->finishImmediately:Z

    if-eqz v2, :cond_3

    .line 106
    const-string v2, "finish_immediately"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 108
    :cond_3
    return-object v1

    .line 93
    .restart local v0    # "homeFragmentState":Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->readNowEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    goto :goto_0

    .line 95
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 96
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    if-eqz v2, :cond_6

    .line 97
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "Cannot have a non-null libraryPage if the homePage is not MyLibrary."

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 100
    :cond_6
    const-string v3, "MyLibraryFragment_state"

    new-instance v4, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    if-nez v2, :cond_7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->DEFAULT_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    :goto_2
    invoke-direct {v4, v2}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)V

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    goto :goto_2
.end method

.method public setHomeFragmentState(Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;
    .locals 1
    .param p1, "homeFragmentState"    # Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    .prologue
    .line 63
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 64
    return-object p0
.end method

.method public setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;
    .locals 0
    .param p1, "homePage"    # Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 49
    return-object p0
.end method

.method public setLibraryFragmentState(Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;
    .locals 1
    .param p1, "libraryFragmentState"    # Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    .prologue
    .line 68
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 69
    return-object p0
.end method

.method public setLibraryPage(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;
    .locals 0
    .param p1, "libraryPage"    # Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 54
    return-object p0
.end method

.method public setReadNowEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->readNowEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 59
    return-object p0
.end method

.method public setReadNowFragmentState(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;
    .locals 1
    .param p1, "readNowFragmentState"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    .prologue
    .line 73
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->readNowEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 74
    return-object p0
.end method
