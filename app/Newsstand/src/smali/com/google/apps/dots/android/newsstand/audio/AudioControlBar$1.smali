.class Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$1;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "AudioControlBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$100(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    if-nez v0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .line 114
    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$100(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .line 115
    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v1

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->postId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->start()V

    goto :goto_0
.end method
