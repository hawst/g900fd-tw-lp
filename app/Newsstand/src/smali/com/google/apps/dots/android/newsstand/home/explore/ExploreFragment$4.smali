.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$4;
.super Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.source "ExploreFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->getTopicsListWithSpecialItems()Lcom/google/android/libraries/bind/data/DataList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;
    .param p2, "x0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 2
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v1, 0x0

    .line 158
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/play/MarketInfo;->areMagazinesAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->makeStoreCard()Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->access$100(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 160
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->makeFeaturedCard()Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 164
    :goto_0
    return-object p1

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->makeFeaturedCard()Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreFragment;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method
