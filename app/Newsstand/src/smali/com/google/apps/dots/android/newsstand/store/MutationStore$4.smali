.class Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "MutationStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$mutLogFile:Ljava/io/File;

.field final synthetic val$mutation:Lcom/google/apps/dots/android/newsstand/store/StoreMutation;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;Landroid/accounts/Account;Ljava/io/File;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutation:Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$account:Landroid/accounts/Account;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutLogFile:Ljava/io/File;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 199
    invoke-static {}, Lcom/google/common/io/Closer;->create()Lcom/google/common/io/Closer;

    move-result-object v1

    .line 200
    .local v1, "closer":Lcom/google/common/io/Closer;
    const-string v10, "MutStore-mutate"

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutation:Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    iget-object v11, v11, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->id:Ljava/lang/String;

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v9

    .line 203
    .local v9, "traceRestore":I
    :try_start_0
    new-instance v8, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutation:Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    iget-object v10, v10, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->id:Ljava/lang/String;

    sget-object v11, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v8, v10, v11}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 204
    .local v8, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v10

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$account:Landroid/accounts/Account;

    invoke-virtual {v10, v11, v8}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->get(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    move-result-object v0

    .line 205
    .local v0, "cachedResponse":Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;, "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    if-eqz v0, :cond_0

    .line 206
    iget-object v7, v0, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->item:Ljava/lang/Object;

    check-cast v7, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .line 209
    .local v7, "oldMutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    iget-object v10, v7, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    move-result-object v6

    .line 210
    .local v6, "newRoot":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->backendSimulator:Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$400(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

    move-result-object v10

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutation:Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    iget-object v11, v11, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->action:Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-virtual {v10, v6, v11}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->applyActionTo(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    .line 213
    new-instance v5, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    new-instance v10, Lcom/google/apps/dots/android/newsstand/store/Version;

    iget-object v11, v7, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    iget-wide v12, v11, Lcom/google/apps/dots/android/newsstand/store/Version;->serverVersion:J

    iget-object v11, v7, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    iget v11, v11, Lcom/google/apps/dots/android/newsstand/store/Version;->localMutationCount:I

    add-int/lit8 v11, v11, 0x1

    invoke-direct {v10, v12, v13, v11}, Lcom/google/apps/dots/android/newsstand/store/Version;-><init>(JI)V

    iget-object v11, v7, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    invoke-direct {v5, v6, v10, v11}, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/android/newsstand/store/Version;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V

    .line 220
    .local v5, "newMutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v10

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$account:Landroid/accounts/Account;

    new-instance v12, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    iget-object v13, v0, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iget v14, v0, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->sizeKb:I

    invoke-direct {v12, v13, v5, v14}, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;-><init>(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/lang/Object;I)V

    invoke-virtual {v10, v11, v8, v12}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->put(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;)V

    .line 226
    .end local v5    # "newMutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    .end local v6    # "newRoot":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .end local v7    # "oldMutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    :cond_0
    new-instance v4, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    invoke-direct {v4}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;-><init>()V

    .line 227
    .local v4, "mutationLogDelta":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutLogFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_1

    .line 228
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutLogFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z

    .line 231
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v10

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$account:Landroid/accounts/Account;

    invoke-virtual {v10, v11}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getOriginalName(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->setAccount(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    .line 232
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutation:Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    iget-object v10, v10, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->id:Ljava/lang/String;

    invoke-virtual {v4, v10}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->setBatchEndpointUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    .line 236
    :cond_1
    iget-object v10, v4, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutation:Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    iget-object v11, v11, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->action:Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 237
    invoke-static {v10, v11}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    iput-object v10, v4, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 239
    const-string v10, "MutStore-write-log"

    const-string v11, "%s, %s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutation:Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    iget-object v14, v14, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->id:Ljava/lang/String;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    iget-object v14, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutLogFile:Ljava/io/File;

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 240
    new-instance v10, Ljava/io/FileOutputStream;

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutLogFile:Ljava/io/File;

    const/4 v12, 0x1

    invoke-direct {v10, v11, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-virtual {v1, v10}, Lcom/google/common/io/Closer;->register(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v3

    check-cast v3, Ljava/io/OutputStream;

    .line 241
    .local v3, "mutLogOut":Ljava/io/OutputStream;
    invoke-static {v4, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->writeToStream(Lcom/google/protobuf/nano/MessageNano;Ljava/io/OutputStream;)V

    .line 242
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 244
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v10

    const-string v11, "%s: notifying content URI"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutation:Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    iget-object v14, v14, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->id:Ljava/lang/String;

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 246
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$800(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v10

    sget-object v11, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v12, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutation:Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    iget-object v12, v12, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->id:Ljava/lang/String;

    .line 247
    invoke-static {v11, v12}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    iget-object v12, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$account:Landroid/accounts/Account;

    const/4 v13, 0x0

    .line 248
    invoke-static {v12, v13}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->makeNotificationExtras(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/Version;)Ljava/util/Map;

    move-result-object v12

    .line 246
    invoke-virtual {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->notify(Landroid/net/Uri;Ljava/util/Map;)V

    .line 250
    iget-object v12, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->val$mutation:Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    iget-object v10, v10, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    sget-object v11, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    if-ne v10, v11, :cond_2

    const-wide/16 v10, 0x7d0

    :goto_0
    invoke-virtual {v12, v10, v11}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->requestCleanup(J)V

    .line 253
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 258
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    .line 259
    invoke-virtual {v1}, Lcom/google/common/io/Closer;->close()V

    return-object v10

    .line 250
    :cond_2
    const-wide/16 v10, 0x7530

    goto :goto_0

    .line 254
    .end local v0    # "cachedResponse":Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;, "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    .end local v3    # "mutLogOut":Ljava/io/OutputStream;
    .end local v4    # "mutationLogDelta":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .end local v8    # "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    :catch_0
    move-exception v2

    .line 255
    .local v2, "e":Ljava/lang/Throwable;
    :try_start_1
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v10

    invoke-virtual {v10, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    .line 256
    invoke-virtual {v1, v2}, Lcom/google/common/io/Closer;->rethrow(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v10

    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258
    .end local v2    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v10

    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    .line 259
    invoke-virtual {v1}, Lcom/google/common/io/Closer;->close()V

    throw v10
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$4;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
