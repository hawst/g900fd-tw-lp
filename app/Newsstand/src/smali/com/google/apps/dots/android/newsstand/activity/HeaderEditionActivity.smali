.class public Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "HeaderEditionActivity.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>()V

    return-void
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 61
    if-nez p1, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    const-string v0, "HeaderEditionFragment_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->handleExtras(Landroid/os/Bundle;)Z

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 40
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    return-object v0
.end method

.method public onActivityReenter(ILandroid/content/Intent;)V
    .locals 2
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 54
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onActivityReenter(ILandroid/content/Intent;)V

    .line 55
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->handleUpcomingResult(Landroid/os/Bundle;)Z

    .line 58
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->handleOnBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onBackPressed()V

    .line 93
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->header_edition_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->setContentView(I)V

    .line 30
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->edition_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .line 32
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 35
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 36
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onNewIntent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 48
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 49
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;->onOptionsItemSelectedCloseDrawerIfNeeded(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const/4 v0, 0x1

    .line 74
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
