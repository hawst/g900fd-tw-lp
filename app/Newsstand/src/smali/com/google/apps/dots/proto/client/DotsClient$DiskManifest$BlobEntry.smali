.class public final Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BlobEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;


# instance fields
.field private bitField0_:I

.field private eTag_:[B

.field private expiration_:J

.field private key_:[B

.field private lastModified_:J

.field private location_:I

.field private offset_:J

.field public pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

.field private readTime_:J

.field private size_:J

.field private writeTime_:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 16
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 176
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->key_:[B

    .line 198
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->offset_:J

    .line 217
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->size_:J

    .line 236
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->readTime_:J

    .line 255
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->writeTime_:J

    .line 274
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    .line 296
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->lastModified_:J

    .line 315
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->expiration_:J

    .line 334
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    .line 337
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->location_:I

    .line 16
    return-void
.end method


# virtual methods
.method public clearETag()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 1

    .prologue
    .line 290
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    .line 291
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 292
    return-object p0
.end method

.method public clearExpiration()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 2

    .prologue
    .line 328
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->expiration_:J

    .line 329
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 330
    return-object p0
.end method

.method public clearLastModified()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 2

    .prologue
    .line 309
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->lastModified_:J

    .line 310
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 311
    return-object p0
.end method

.method public clearLocation()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 1

    .prologue
    .line 350
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->location_:I

    .line 351
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 352
    return-object p0
.end method

.method public clearOffset()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 2

    .prologue
    .line 211
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->offset_:J

    .line 212
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 213
    return-object p0
.end method

.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 5

    .prologue
    .line 375
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 380
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    .line 381
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 382
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 383
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->clone()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    move-result-object v4

    aput-object v4, v3, v2

    .line 381
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 376
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 377
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 387
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->clone()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 392
    if-ne p1, p0, :cond_1

    .line 403
    :cond_0
    :goto_0
    return v1

    .line 393
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 394
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 395
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->key_:[B

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->key_:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->offset_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->offset_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->size_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->size_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->readTime_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->readTime_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->writeTime_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->writeTime_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    .line 400
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->lastModified_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->lastModified_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->expiration_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->expiration_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    .line 403
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->location_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->location_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getETag()[B
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    return-object v0
.end method

.method public getExpiration()J
    .locals 2

    .prologue
    .line 317
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->expiration_:J

    return-wide v0
.end method

.method public getKey()[B
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->key_:[B

    return-object v0
.end method

.method public getLastModified()J
    .locals 2

    .prologue
    .line 298
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->lastModified_:J

    return-wide v0
.end method

.method public getLocation()I
    .locals 1

    .prologue
    .line 339
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->location_:I

    return v0
.end method

.method public getOffset()J
    .locals 2

    .prologue
    .line 200
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->offset_:J

    return-wide v0
.end method

.method public getReadTime()J
    .locals 2

    .prologue
    .line 238
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->readTime_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 478
    const/4 v1, 0x0

    .line 479
    .local v1, "size":I
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 480
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->key_:[B

    .line 481
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v2

    add-int/2addr v1, v2

    .line 483
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 484
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->offset_:J

    .line 485
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 487
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 488
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->size_:J

    .line 489
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 491
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 492
    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->readTime_:J

    .line 493
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 495
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_4

    .line 496
    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->writeTime_:J

    .line 497
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 499
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_5

    .line 500
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    .line 501
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v2

    add-int/2addr v1, v2

    .line 503
    :cond_5
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_6

    .line 504
    const/4 v2, 0x7

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->lastModified_:J

    .line 505
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 507
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_7

    .line 508
    const/16 v2, 0x8

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->expiration_:J

    .line 509
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 511
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    if-eqz v2, :cond_9

    .line 512
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_9

    aget-object v0, v3, v2

    .line 513
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    if-eqz v0, :cond_8

    .line 514
    const/16 v5, 0x9

    .line 515
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 512
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 519
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    :cond_9
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_a

    .line 520
    const/16 v2, 0xa

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->location_:I

    .line 521
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 523
    :cond_a
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->cachedSize:I

    .line 524
    return v1
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 219
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->size_:J

    return-wide v0
.end method

.method public getWriteTime()J
    .locals 2

    .prologue
    .line 257
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->writeTime_:J

    return-wide v0
.end method

.method public hasETag()Z
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExpiration()Z
    .locals 1

    .prologue
    .line 325
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastModified()Z
    .locals 1

    .prologue
    .line 306
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    .line 408
    const/16 v1, 0x11

    .line 409
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 410
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->key_:[B

    if-nez v2, :cond_3

    mul-int/lit8 v1, v1, 0x1f

    .line 416
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->offset_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->offset_:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v1, v2, v3

    .line 417
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->size_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->size_:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v1, v2, v3

    .line 418
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->readTime_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->readTime_:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v1, v2, v3

    .line 419
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->writeTime_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->writeTime_:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v1, v2, v3

    .line 420
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    if-nez v2, :cond_4

    mul-int/lit8 v1, v1, 0x1f

    .line 426
    :cond_1
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->lastModified_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->lastModified_:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v1, v2, v3

    .line 427
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->expiration_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->expiration_:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v1, v2, v3

    .line 428
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    if-nez v2, :cond_5

    mul-int/lit8 v1, v1, 0x1f

    .line 434
    :cond_2
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->location_:I

    add-int v1, v2, v3

    .line 435
    return v1

    .line 412
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->key_:[B

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 413
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->key_:[B

    aget-byte v3, v3, v0

    add-int v1, v2, v3

    .line 412
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 422
    .end local v0    # "i":I
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 423
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    aget-byte v3, v3, v0

    add-int v1, v2, v3

    .line 422
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 430
    .end local v0    # "i":I
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 431
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    aget-object v2, v2, v0

    if-nez v2, :cond_6

    const/4 v2, 0x0

    :goto_3
    add-int v1, v3, v2

    .line 430
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 431
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 532
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 533
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 537
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 538
    :sswitch_0
    return-object p0

    .line 543
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->key_:[B

    .line 544
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    goto :goto_0

    .line 548
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->offset_:J

    .line 549
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    goto :goto_0

    .line 553
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->size_:J

    .line 554
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    goto :goto_0

    .line 558
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->readTime_:J

    .line 559
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    goto :goto_0

    .line 563
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->writeTime_:J

    .line 564
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    goto :goto_0

    .line 568
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    .line 569
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    goto :goto_0

    .line 573
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->lastModified_:J

    .line 574
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    goto :goto_0

    .line 578
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->expiration_:J

    .line 579
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    goto :goto_0

    .line 583
    :sswitch_9
    const/16 v6, 0x4a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 584
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    if-nez v6, :cond_2

    move v1, v5

    .line 585
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    .line 586
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    if-eqz v6, :cond_1

    .line 587
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 589
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    .line 590
    :goto_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 591
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;-><init>()V

    aput-object v7, v6, v1

    .line 592
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 593
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 590
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 584
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    :cond_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v1, v6

    goto :goto_1

    .line 596
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    :cond_3
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;-><init>()V

    aput-object v7, v6, v1

    .line 597
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 601
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 602
    .local v4, "temp":I
    if-eqz v4, :cond_4

    const/4 v6, 0x1

    if-eq v4, v6, :cond_4

    const/4 v6, 0x2

    if-ne v4, v6, :cond_5

    .line 605
    :cond_4
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->location_:I

    .line 606
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit16 v6, v6, 0x100

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    goto/16 :goto_0

    .line 608
    :cond_5
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->location_:I

    goto/16 :goto_0

    .line 533
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    move-result-object v0

    return-object v0
.end method

.method public setETag([B)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 1
    .param p1, "value"    # [B

    .prologue
    .line 279
    if-nez p1, :cond_0

    .line 280
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 282
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    .line 283
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 284
    return-object p0
.end method

.method public setExpiration(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 320
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->expiration_:J

    .line 321
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 322
    return-object p0
.end method

.method public setKey([B)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 1
    .param p1, "value"    # [B

    .prologue
    .line 181
    if-nez p1, :cond_0

    .line 182
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 184
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->key_:[B

    .line 185
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 186
    return-object p0
.end method

.method public setLastModified(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 301
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->lastModified_:J

    .line 302
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 303
    return-object p0
.end method

.method public setLocation(I)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 342
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->location_:I

    .line 343
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 344
    return-object p0
.end method

.method public setOffset(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 203
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->offset_:J

    .line 204
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 205
    return-object p0
.end method

.method public setReadTime(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 241
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->readTime_:J

    .line 242
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 243
    return-object p0
.end method

.method public setSize(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 222
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->size_:J

    .line 223
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 224
    return-object p0
.end method

.method public setWriteTime(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 260
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->writeTime_:J

    .line 261
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    .line 262
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 440
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 441
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->key_:[B

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 443
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 444
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->offset_:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 446
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 447
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->size_:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 449
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 450
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->readTime_:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 452
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 453
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->writeTime_:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 455
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 456
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->eTag_:[B

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 458
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 459
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->lastModified_:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 461
    :cond_6
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    .line 462
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->expiration_:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 464
    :cond_7
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    if-eqz v1, :cond_9

    .line 465
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_9

    aget-object v0, v2, v1

    .line 466
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    if-eqz v0, :cond_8

    .line 467
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 465
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 471
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    :cond_9
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_a

    .line 472
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->location_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 474
    :cond_a
    return-void
.end method
