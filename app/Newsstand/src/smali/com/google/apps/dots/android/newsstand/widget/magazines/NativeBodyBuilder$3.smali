.class Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$3;
.super Ljava/lang/Object;
.source "NativeBodyBuilder.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->initEventDispatcher()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Landroid/net/Uri;)V
    .locals 3
    .param p1, "event"    # Landroid/net/Uri;

    .prologue
    .line 203
    const-string v1, "path"

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getStringQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "path":Ljava/lang/String;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    .line 205
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 206
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSectionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;

    move-result-object v1

    .line 207
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->setLocalUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;

    move-result-object v1

    .line 208
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->start()V

    .line 209
    return-void
.end method
