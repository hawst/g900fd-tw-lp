.class public final Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SectionSummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;


# instance fields
.field private bitField0_:I

.field private correspondingImageSectionId_:Ljava/lang/String;

.field private correspondingTextSectionId_:Ljava/lang/String;

.field private curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

.field public features:[I

.field private isMetered_:Z

.field private layoutEngineVersionOverride_:I

.field public popularPostIds:[Ljava/lang/String;

.field private sectionId_:Ljava/lang/String;

.field private showTopicTags_:Z

.field private title_:Ljava/lang/String;

.field private tocType_:I

.field private untranslatedSectionId_:Ljava/lang/String;

.field private updated_:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27783
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27784
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 27789
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->sectionId_:Ljava/lang/String;

    .line 27811
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->untranslatedSectionId_:Ljava/lang/String;

    .line 27833
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->title_:Ljava/lang/String;

    .line 27855
    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->isMetered_:Z

    .line 27874
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->updated_:J

    .line 27893
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->showTopicTags_:Z

    .line 27912
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    .line 27915
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingImageSectionId_:Ljava/lang/String;

    .line 27937
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingTextSectionId_:Ljava/lang/String;

    .line 27959
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    .line 27981
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->tocType_:I

    .line 28000
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->layoutEngineVersionOverride_:I

    .line 27784
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    .locals 3

    .prologue
    .line 28041
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28045
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_0

    .line 28046
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    invoke-virtual {v2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    .line 28048
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 28049
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    .line 28051
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    if-eqz v2, :cond_2

    .line 28052
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    .line 28054
    :cond_2
    return-object v0

    .line 28042
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :catch_0
    move-exception v1

    .line 28043
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 27780
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28059
    if-ne p1, p0, :cond_1

    .line 28072
    :cond_0
    :goto_0
    return v1

    .line 28060
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 28061
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 28062
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->sectionId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->sectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 28063
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 28064
    :goto_3
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->isMetered_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->isMetered_:Z

    if-ne v3, v4, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->updated_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->updated_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->showTopicTags_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->showTopicTags_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    .line 28068
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingImageSectionId_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingImageSectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 28069
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingTextSectionId_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingTextSectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 28070
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    .line 28071
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    if-nez v3, :cond_3

    .line 28072
    :goto_6
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->tocType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->tocType_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->layoutEngineVersionOverride_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->layoutEngineVersionOverride_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 28062
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->sectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->sectionId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->untranslatedSectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->untranslatedSectionId_:Ljava/lang/String;

    .line 28063
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->title_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->title_:Ljava/lang/String;

    .line 28064
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    .line 28068
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingImageSectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingImageSectionId_:Ljava/lang/String;

    .line 28069
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingTextSectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingTextSectionId_:Ljava/lang/String;

    .line 28070
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_5

    .line 28071
    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    .line 28072
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_6
.end method

.method public getLayoutEngineVersionOverride()I
    .locals 1

    .prologue
    .line 28002
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->layoutEngineVersionOverride_:I

    return v0
.end method

.method public getSectionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27791
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->sectionId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 28155
    const/4 v2, 0x0

    .line 28156
    .local v2, "size":I
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    .line 28157
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->sectionId_:Ljava/lang/String;

    .line 28158
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 28160
    :cond_0
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_1

    .line 28161
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->title_:Ljava/lang/String;

    .line 28162
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 28164
    :cond_1
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_2

    .line 28165
    const/4 v4, 0x3

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->isMetered_:Z

    .line 28166
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 28168
    :cond_2
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_3

    .line 28169
    const/4 v4, 0x4

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->updated_:J

    .line 28170
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 28172
    :cond_3
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_4

    .line 28173
    const/4 v4, 0x5

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->showTopicTags_:Z

    .line 28174
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 28176
    :cond_4
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_5

    .line 28177
    const/4 v4, 0x6

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->untranslatedSectionId_:Ljava/lang/String;

    .line 28178
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 28180
    :cond_5
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_7

    .line 28181
    const/4 v0, 0x0

    .line 28182
    .local v0, "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_6

    aget-object v1, v5, v4

    .line 28184
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 28182
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 28186
    .end local v1    # "element":Ljava/lang/String;
    :cond_6
    add-int/2addr v2, v0

    .line 28187
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 28189
    .end local v0    # "dataSize":I
    :cond_7
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_8

    .line 28190
    const/16 v4, 0x8

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingImageSectionId_:Ljava/lang/String;

    .line 28191
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 28193
    :cond_8
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_9

    .line 28194
    const/16 v4, 0x9

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingTextSectionId_:Ljava/lang/String;

    .line 28195
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 28197
    :cond_9
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    array-length v4, v4

    if-lez v4, :cond_b

    .line 28198
    const/4 v0, 0x0

    .line 28199
    .restart local v0    # "dataSize":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    array-length v5, v4

    :goto_1
    if-ge v3, v5, :cond_a

    aget v1, v4, v3

    .line 28201
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v6

    add-int/2addr v0, v6

    .line 28199
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 28203
    .end local v1    # "element":I
    :cond_a
    add-int/2addr v2, v0

    .line 28204
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    .line 28206
    .end local v0    # "dataSize":I
    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    if-eqz v3, :cond_c

    .line 28207
    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    .line 28208
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 28210
    :cond_c
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit16 v3, v3, 0x100

    if-eqz v3, :cond_d

    .line 28211
    const/16 v3, 0xc

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->tocType_:I

    .line 28212
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 28214
    :cond_d
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit16 v3, v3, 0x200

    if-eqz v3, :cond_e

    .line 28215
    const/16 v3, 0xd

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->layoutEngineVersionOverride_:I

    .line 28216
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 28218
    :cond_e
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->cachedSize:I

    .line 28219
    return v2
.end method

.method public getShowTopicTags()Z
    .locals 1

    .prologue
    .line 27895
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->showTopicTags_:Z

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27835
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getTocType()I
    .locals 1

    .prologue
    .line 27983
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->tocType_:I

    return v0
.end method

.method public hasCorrespondingImageSectionId()Z
    .locals 1

    .prologue
    .line 27928
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCorrespondingTextSectionId()Z
    .locals 1

    .prologue
    .line 27950
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLayoutEngineVersionOverride()Z
    .locals 1

    .prologue
    .line 28010
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUntranslatedSectionId()Z
    .locals 1

    .prologue
    .line 27824
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28078
    const/16 v1, 0x11

    .line 28079
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 28080
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->sectionId_:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 28081
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_1
    add-int v1, v6, v2

    .line 28082
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->title_:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_2
    add-int v1, v6, v2

    .line 28083
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->isMetered_:Z

    if-eqz v2, :cond_5

    move v2, v4

    :goto_3
    add-int v1, v6, v2

    .line 28084
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->updated_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->updated_:J

    const/16 v10, 0x20

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 28085
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->showTopicTags_:Z

    if-eqz v6, :cond_6

    :goto_4
    add-int v1, v2, v4

    .line 28086
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    if-nez v2, :cond_7

    mul-int/lit8 v1, v1, 0x1f

    .line 28092
    :cond_0
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingImageSectionId_:Ljava/lang/String;

    if-nez v2, :cond_9

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 28093
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingTextSectionId_:Ljava/lang/String;

    if-nez v2, :cond_a

    move v2, v3

    :goto_6
    add-int v1, v4, v2

    .line 28094
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    if-nez v2, :cond_b

    mul-int/lit8 v1, v1, 0x1f

    .line 28100
    :cond_1
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    if-nez v4, :cond_c

    :goto_7
    add-int v1, v2, v3

    .line 28101
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->tocType_:I

    add-int v1, v2, v3

    .line 28102
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->layoutEngineVersionOverride_:I

    add-int v1, v2, v3

    .line 28103
    return v1

    .line 28080
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->sectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 28081
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->untranslatedSectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 28082
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->title_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_5
    move v2, v5

    .line 28083
    goto :goto_3

    :cond_6
    move v4, v5

    .line 28085
    goto :goto_4

    .line 28088
    :cond_7
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 28089
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_8

    move v2, v3

    :goto_9
    add-int v1, v4, v2

    .line 28088
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 28089
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_9

    .line 28092
    .end local v0    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingImageSectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5

    .line 28093
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingTextSectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6

    .line 28096
    :cond_b
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 28097
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    aget v4, v4, v0

    add-int v1, v2, v4

    .line 28096
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 28100
    .end local v0    # "i":I
    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->hashCode()I

    move-result v3

    goto :goto_7
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 28227
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 28228
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 28232
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 28233
    :sswitch_0
    return-object p0

    .line 28238
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->sectionId_:Ljava/lang/String;

    .line 28239
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    goto :goto_0

    .line 28243
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->title_:Ljava/lang/String;

    .line 28244
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    goto :goto_0

    .line 28248
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->isMetered_:Z

    .line 28249
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    goto :goto_0

    .line 28253
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->updated_:J

    .line 28254
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    goto :goto_0

    .line 28258
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->showTopicTags_:Z

    .line 28259
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    goto :goto_0

    .line 28263
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->untranslatedSectionId_:Ljava/lang/String;

    .line 28264
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    goto :goto_0

    .line 28268
    :sswitch_7
    const/16 v5, 0x3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 28269
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    array-length v1, v5

    .line 28270
    .local v1, "i":I
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 28271
    .local v2, "newArray":[Ljava/lang/String;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    invoke-static {v5, v8, v2, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 28272
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    .line 28273
    :goto_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_1

    .line 28274
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 28275
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 28273
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 28278
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    goto/16 :goto_0

    .line 28282
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingImageSectionId_:Ljava/lang/String;

    .line 28283
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    goto/16 :goto_0

    .line 28287
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingTextSectionId_:Ljava/lang/String;

    .line 28288
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    goto/16 :goto_0

    .line 28292
    :sswitch_a
    const/16 v5, 0x50

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 28293
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    array-length v1, v5

    .line 28294
    .restart local v1    # "i":I
    add-int v5, v1, v0

    new-array v2, v5, [I

    .line 28295
    .local v2, "newArray":[I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    invoke-static {v5, v8, v2, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 28296
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    .line 28297
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_2

    .line 28298
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    .line 28299
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 28297
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 28302
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    goto/16 :goto_0

    .line 28306
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[I
    :sswitch_b
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    if-nez v5, :cond_3

    .line 28307
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    .line 28309
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 28313
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 28314
    .local v4, "temp":I
    if-eqz v4, :cond_4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_5

    .line 28318
    :cond_4
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->tocType_:I

    .line 28319
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    goto/16 :goto_0

    .line 28321
    :cond_5
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->tocType_:I

    goto/16 :goto_0

    .line 28326
    .end local v4    # "temp":I
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->layoutEngineVersionOverride_:I

    .line 28327
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x200

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    goto/16 :goto_0

    .line 28228
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27780
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 28108
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 28109
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->sectionId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 28111
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    .line 28112
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->title_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 28114
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_2

    .line 28115
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->isMetered_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 28117
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_3

    .line 28118
    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->updated_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 28120
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_4

    .line 28121
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->showTopicTags_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 28123
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_5

    .line 28124
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->untranslatedSectionId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 28126
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 28127
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->popularPostIds:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_6

    aget-object v0, v3, v2

    .line 28128
    .local v0, "element":Ljava/lang/String;
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 28127
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 28131
    .end local v0    # "element":Ljava/lang/String;
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_7

    .line 28132
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingImageSectionId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 28134
    :cond_7
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_8

    .line 28135
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->correspondingTextSectionId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 28137
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    array-length v2, v2

    if-lez v2, :cond_9

    .line 28138
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->features:[I

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_9

    aget v0, v2, v1

    .line 28139
    .local v0, "element":I
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 28138
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 28142
    .end local v0    # "element":I
    :cond_9
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    if-eqz v1, :cond_a

    .line 28143
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->curation_:Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 28145
    :cond_a
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_b

    .line 28146
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->tocType_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 28148
    :cond_b
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_c

    .line 28149
    const/16 v1, 0xd

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->layoutEngineVersionOverride_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 28151
    :cond_c
    return-void
.end method
