.class public Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;
.super Ljava/lang/Object;
.source "FeedbackInformation.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final activity:Landroid/app/Activity;

.field private allStacksFile:Ljava/io/File;

.field private crashInfoFile:Ljava/io/File;

.field private hprofFile:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->activity:Landroid/app/Activity;

    .line 69
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 70
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "hprof"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    new-instance v1, Ljava/io/File;

    const-string v2, "hprof"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->hprofFile:Ljava/io/File;

    .line 73
    :cond_0
    const-string v1, "allstacks"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 74
    new-instance v1, Ljava/io/File;

    const-string v2, "allstacks"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->allStacksFile:Ljava/io/File;

    .line 76
    :cond_1
    const-string v1, "crashinfo"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    new-instance v1, Ljava/io/File;

    const-string v2, "crashinfo"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->crashInfoFile:Ljava/io/File;

    .line 82
    :cond_2
    return-void
.end method

.method public static capture(Landroid/content/Context;Ljava/lang/Throwable;Z)Ljava/util/Map;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ex"    # Ljava/lang/Throwable;
    .param p2, "dumpHprof"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Throwable;",
            "Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v4

    .line 90
    .local v4, "policy":Landroid/os/StrictMode$ThreadPolicy;
    invoke-static {p0, p2}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->dumpHprof(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v3

    .line 91
    .local v3, "hprofPath":Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->dumpAllStacks(Landroid/content/Context;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "allStacksPath":Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->dumpCrashInfo(Landroid/content/Context;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    .line 93
    .local v1, "crashInfoPath":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->dumpScreenshot(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v5

    .line 94
    .local v5, "screenshotPath":Ljava/lang/String;
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 96
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    .line 97
    .local v2, "extras":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "hprof"

    invoke-static {v2, v6, v3}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->putIfNotNull(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v6, "allstacks"

    invoke-static {v2, v6, v0}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->putIfNotNull(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v6, "crashinfo"

    invoke-static {v2, v6, v1}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->putIfNotNull(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v6, "screenshot"

    invoke-static {v2, v6, v5}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->putIfNotNull(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    return-object v2
.end method

.method private static closeQuietly(Ljava/io/InputStream;)V
    .locals 4
    .param p0, "in"    # Ljava/io/InputStream;

    .prologue
    .line 333
    if-eqz p0, :cond_0

    .line 335
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 336
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Error closing input stream"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static dumpAllStacks(Landroid/content/Context;Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 239
    :try_start_0
    const-string v2, "currents.allstacks"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->initFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 240
    .local v1, "file":Ljava/io/File;
    if-eqz p1, :cond_0

    .line 241
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->getAllStacks()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v2, v1, v3}, Lcom/google/common/io/Files;->write(Ljava/lang/CharSequence;Ljava/io/File;Ljava/nio/charset/Charset;)V

    .line 243
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 246
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return-object v2

    .line 244
    :catch_0
    move-exception v0

    .line 245
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Error dumping all stacks"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 246
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static dumpCrashInfo(Landroid/content/Context;Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 211
    const/4 v5, 0x0

    .line 213
    .local v5, "parcel":Landroid/os/Parcel;
    :try_start_0
    const-string v7, "currents.crashinfo"

    invoke-static {p0, v7}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->initFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 214
    .local v4, "file":Ljava/io/File;
    if-eqz p1, :cond_0

    .line 215
    new-instance v1, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;

    invoke-direct {v1, p1}, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;-><init>(Ljava/lang/Throwable;)V

    .line 216
    .local v1, "crashInfo":Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v5

    .line 217
    const/4 v7, 0x0

    invoke-virtual {v1, v5, v7}, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 218
    invoke-virtual {v5}, Landroid/os/Parcel;->marshall()[B

    move-result-object v6

    .line 219
    .local v6, "parcelBytes":[B
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 220
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 221
    .local v2, "dos":Ljava/io/DataOutputStream;
    array-length v7, v6

    invoke-virtual {v2, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 222
    invoke-virtual {v2, v6}, Ljava/io/DataOutputStream;->write([B)V

    .line 223
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 224
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-static {v7, v4}, Lcom/google/common/io/Files;->write([BLjava/io/File;)V

    .line 226
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "crashInfo":Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;
    .end local v2    # "dos":Ljava/io/DataOutputStream;
    .end local v6    # "parcelBytes":[B
    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 231
    if-eqz v5, :cond_1

    .line 232
    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V

    .end local v4    # "file":Ljava/io/File;
    :cond_1
    :goto_0
    return-object v7

    .line 227
    :catch_0
    move-exception v3

    .line 228
    .local v3, "e":Ljava/lang/Throwable;
    :try_start_1
    sget-object v7, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "Error dumping crash info"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v7, v3, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229
    const/4 v7, 0x0

    .line 231
    if-eqz v5, :cond_1

    .line 232
    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    .line 231
    .end local v3    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v7

    if-eqz v5, :cond_2

    .line 232
    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V

    :cond_2
    throw v7
.end method

.method private static dumpHprof(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enabled"    # Z

    .prologue
    .line 199
    if-eqz p1, :cond_0

    .line 200
    :try_start_0
    const-string v2, "currents.hprof"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->initFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 201
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/os/Debug;->dumpHprofData(Ljava/lang/String;)V

    .line 202
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 207
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return-object v2

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Error dumping hprof"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static dumpScreenshot(Landroid/app/Activity;)Ljava/lang/String;
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 252
    const/4 v0, 0x0

    return-object v0
.end method

.method private static getAllStacks()Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v8, 0x0

    .line 180
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-static {}, Ljava/lang/Thread;->getAllStackTraces()Ljava/util/Map;

    move-result-object v4

    .line 182
    .local v4, "stackTraces":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Thread;[Ljava/lang/StackTraceElement;>;"
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 183
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Thread;[Ljava/lang/StackTraceElement;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Thread;

    .line 184
    .local v5, "thread":Ljava/lang/Thread;
    const-string v6, "Thread %s %s %s\n"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v10, 0x0

    .line 185
    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v7, v10

    const/4 v10, 0x1

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v7, v10

    const/4 v10, 0x2

    invoke-virtual {v5}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v11

    aput-object v11, v7, v10

    .line 184
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/StackTraceElement;

    array-length v10, v6

    move v7, v8

    :goto_0
    if-ge v7, v10, :cond_0

    aget-object v1, v6, v7

    .line 187
    .local v1, "element":Ljava/lang/StackTraceElement;
    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, 0x3

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, "  "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 190
    .end local v1    # "element":Ljava/lang/StackTraceElement;
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Thread;[Ljava/lang/StackTraceElement;>;"
    .end local v5    # "thread":Ljava/lang/Thread;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 193
    .end local v4    # "stackTraces":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Thread;[Ljava/lang/StackTraceElement;>;"
    :goto_1
    return-object v6

    .line 191
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Error reading all stacks"

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v6, v0, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private static initFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 309
    invoke-virtual {p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 310
    .local v0, "dir":Ljava/io/File;
    if-nez v0, :cond_1

    .line 311
    const/4 v1, 0x0

    .line 317
    :cond_0
    :goto_0
    return-object v1

    .line 313
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 314
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 315
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private static putIfNotNull(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 105
    .local p0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 106
    invoke-interface {p0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    :cond_0
    return-void
.end method


# virtual methods
.method public getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method public getLocalLogs()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 286
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 287
    .local v2, "localLogs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    const/4 v5, 0x0

    .line 288
    .local v5, "totalLogBytes":I
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->activity:Landroid/app/Activity;

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->logFiles(Landroid/content/Context;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/io/File;

    .line 289
    .local v4, "logFile":Ljava/io/File;
    const/4 v0, 0x0

    .line 291
    .local v0, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    .end local v0    # "in":Ljava/io/InputStream;
    .local v1, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v1}, Lcom/google/common/io/ByteStreams;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v3

    .line 293
    .local v3, "logBytes":[B
    array-length v7, v3

    add-int/2addr v5, v7

    .line 294
    const/high16 v7, 0x60000

    if-gt v5, v7, :cond_0

    .line 295
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 302
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->closeQuietly(Ljava/io/InputStream;)V

    move-object v0, v1

    .line 303
    .end local v1    # "in":Ljava/io/InputStream;
    .restart local v0    # "in":Ljava/io/InputStream;
    goto :goto_0

    .line 302
    .end local v0    # "in":Ljava/io/InputStream;
    .restart local v1    # "in":Ljava/io/InputStream;
    :cond_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->closeQuietly(Ljava/io/InputStream;)V

    .line 305
    .end local v1    # "in":Ljava/io/InputStream;
    .end local v3    # "logBytes":[B
    .end local v4    # "logFile":Ljava/io/File;
    :cond_1
    return-object v2

    .line 299
    .restart local v0    # "in":Ljava/io/InputStream;
    .restart local v4    # "logFile":Ljava/io/File;
    :catch_0
    move-exception v7

    .line 302
    :goto_1
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->closeQuietly(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    :goto_2
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->closeQuietly(Ljava/io/InputStream;)V

    throw v6

    .end local v0    # "in":Ljava/io/InputStream;
    .restart local v1    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "in":Ljava/io/InputStream;
    .restart local v0    # "in":Ljava/io/InputStream;
    goto :goto_2

    .line 299
    .end local v0    # "in":Ljava/io/InputStream;
    .restart local v1    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v7

    move-object v0, v1

    .end local v1    # "in":Ljava/io/InputStream;
    .restart local v0    # "in":Ljava/io/InputStream;
    goto :goto_1
.end method

.method public readCrashInfo()Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;
    .locals 9

    .prologue
    .line 134
    const/4 v1, 0x0

    .line 135
    .local v1, "in":Ljava/io/DataInputStream;
    const/4 v3, 0x0

    .line 137
    .local v3, "parcel":Landroid/os/Parcel;
    :try_start_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->crashInfoFile:Ljava/io/File;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->crashInfoFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 138
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->crashInfoFile:Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v6}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    .end local v1    # "in":Ljava/io/DataInputStream;
    .local v2, "in":Ljava/io/DataInputStream;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    .line 140
    .local v5, "parcelBytesLength":I
    new-array v4, v5, [B

    .line 141
    .local v4, "parcelBytes":[B
    invoke-virtual {v2, v4}, Ljava/io/DataInputStream;->read([B)I

    .line 142
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 143
    const/4 v6, 0x0

    array-length v7, v4

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 144
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 145
    new-instance v6, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;

    invoke-direct {v6, v3}, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;-><init>(Landroid/os/Parcel;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 150
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->closeQuietly(Ljava/io/InputStream;)V

    .line 151
    if-eqz v3, :cond_0

    .line 152
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    :cond_0
    move-object v1, v2

    .line 155
    .end local v2    # "in":Ljava/io/DataInputStream;
    .end local v4    # "parcelBytes":[B
    .end local v5    # "parcelBytesLength":I
    .restart local v1    # "in":Ljava/io/DataInputStream;
    :goto_0
    return-object v6

    .line 150
    :cond_1
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->closeQuietly(Ljava/io/InputStream;)V

    .line 151
    if-eqz v3, :cond_2

    .line 152
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 155
    :cond_2
    :goto_1
    const/4 v6, 0x0

    goto :goto_0

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/Throwable;
    :goto_2
    :try_start_2
    sget-object v6, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Error reading crash info"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v6, v0, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 150
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->closeQuietly(Ljava/io/InputStream;)V

    .line 151
    if-eqz v3, :cond_2

    .line 152
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    goto :goto_1

    .line 150
    .end local v0    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v6

    :goto_3
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->closeQuietly(Ljava/io/InputStream;)V

    .line 151
    if-eqz v3, :cond_3

    .line 152
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    :cond_3
    throw v6

    .line 150
    .end local v1    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    :catchall_1
    move-exception v6

    move-object v1, v2

    .end local v2    # "in":Ljava/io/DataInputStream;
    .restart local v1    # "in":Ljava/io/DataInputStream;
    goto :goto_3

    .line 147
    .end local v1    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "in":Ljava/io/DataInputStream;
    .restart local v1    # "in":Ljava/io/DataInputStream;
    goto :goto_2
.end method
