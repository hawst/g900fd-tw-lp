.class final Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$2;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "MediaItemsSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->getMediaDrawerList(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)Lcom/google/android/libraries/bind/data/DataList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$dataList:Lcom/google/android/libraries/bind/data/DataList;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$2;->val$dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 8
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    const/4 v3, 0x0

    .line 92
    if-nez p1, :cond_0

    .line 93
    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v5, "Unable to retrieve post"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 97
    .local v2, "validItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v5, v4

    :goto_1
    if-ge v3, v5, :cond_2

    aget-object v0, v4, v3

    .line 98
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->SUPPORTED_MEDIA_DRAWER_TYPES:Ljava/util/List;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->access$100()Ljava/util/List;

    move-result-object v6

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemType(Lcom/google/apps/dots/proto/client/DotsShared$Item;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    sget-object v6, Lcom/google/apps/dots/shared/FieldIds;->NON_MEDIA_FIELDS:Ljava/util/HashSet;

    iget-object v7, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    .line 99
    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 100
    const/4 v1, 0x0

    .local v1, "offset":I
    :goto_2
    iget-object v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v6, v6

    if-ge v1, v6, :cond_1

    .line 101
    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    # invokes: Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->makeMediaItemData(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;I)Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v6, v0, v1}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->access$200(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 97
    .end local v1    # "offset":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 107
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$2;->val$dataList:Lcom/google/android/libraries/bind/data/DataList;

    new-instance v4, Lcom/google/android/libraries/bind/data/Snapshot;

    sget v5, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_MEDIA_ITEM:I

    invoke-direct {v4, v5, v2}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(ILjava/util/List;)V

    sget-object v5, Lcom/google/android/libraries/bind/data/DataChange;->AFFECTS_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/DataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 89
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$2;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
