.class Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;
.super Ljava/lang/Object;
.source "Janitor.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->evictOldBlobs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

.field final synthetic val$sampler:Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;

.field final synthetic val$totalCount:[I

.field final synthetic val$totalSize:[J

.field final synthetic val$unpinnedCount:[I

.field final synthetic val$unpinnedSize:[J


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;[J[I[J[I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;->val$sampler:Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;->val$unpinnedSize:[J

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;->val$unpinnedCount:[I

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;->val$totalSize:[J

    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;->val$totalCount:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;JJJZ)V
    .locals 8
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "readTime"    # J
    .param p4, "writeTime"    # J
    .param p6, "size"    # J
    .param p8, "pinned"    # Z

    .prologue
    const/4 v6, 0x0

    .line 214
    if-nez p8, :cond_0

    .line 215
    invoke-static {p2, p3, p4, p5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v0, v2

    .line 216
    .local v0, "lastAccessed":I
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;->val$sampler:Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;

    long-to-double v2, p6

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->next(ID)V

    .line 217
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;->val$unpinnedSize:[J

    aget-wide v2, v1, v6

    add-long/2addr v2, p6

    aput-wide v2, v1, v6

    .line 218
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;->val$unpinnedCount:[I

    aget v2, v1, v6

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v6

    .line 220
    .end local v0    # "lastAccessed":I
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;->val$totalSize:[J

    aget-wide v2, v1, v6

    add-long/2addr v2, p6

    aput-wide v2, v1, v6

    .line 221
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;->val$totalCount:[I

    aget v2, v1, v6

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v6

    .line 222
    return-void
.end method
