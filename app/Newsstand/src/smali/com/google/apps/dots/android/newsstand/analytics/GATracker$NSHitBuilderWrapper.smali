.class Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;
.super Ljava/lang/Object;
.source "GATracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/analytics/GATracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NSHitBuilderWrapper"
.end annotation


# instance fields
.field private eventBuilder:Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

.field private screenViewBuilder:Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;


# direct methods
.method constructor <init>(Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V
    .locals 0
    .param p1, "eventBuilder"    # Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    .prologue
    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 262
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->eventBuilder:Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    .line 263
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;)V
    .locals 0
    .param p1, "screenViewBuilder"    # Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    .prologue
    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->screenViewBuilder:Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    .line 259
    return-void
.end method


# virtual methods
.method public build()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->screenViewBuilder:Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->screenViewBuilder:Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;->build()Ljava/util/Map;

    move-result-object v0

    .line 287
    :goto_0
    return-object v0

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->eventBuilder:Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->eventBuilder:Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 289
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Wrapper is empty; nothing to build"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;
    .locals 1
    .param p1, "index"    # I
    .param p2, "dimension"    # Ljava/lang/String;

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->screenViewBuilder:Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    if-eqz v0, :cond_1

    .line 267
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->screenViewBuilder:Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    .line 271
    :cond_0
    :goto_0
    return-object p0

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->eventBuilder:Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->eventBuilder:Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    goto :goto_0
.end method

.method public setCustomMetric(IF)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;
    .locals 1
    .param p1, "index"    # I
    .param p2, "metric"    # F

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->screenViewBuilder:Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->screenViewBuilder:Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;->setCustomMetric(IF)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    .line 280
    :cond_0
    :goto_0
    return-object p0

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->eventBuilder:Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->eventBuilder:Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomMetric(IF)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    goto :goto_0
.end method
