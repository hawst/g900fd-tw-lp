.class Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList$1;
.super Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
.source "RawOffersList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;
    .param p2, "primaryKey"    # I

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 3
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList$1;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 57
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_OFFER_ID:I

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 58
    sget v1, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_OFFER_SUMMARY:I

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 59
    sget v1, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_STORE_TYPE:I

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 60
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList$1;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 61
    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 0

    .prologue
    .line 53
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList$1;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    return-void
.end method
