.class public Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "AddMissingPostFilter.java"


# instance fields
.field private final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

.field private final postId:Ljava/lang/String;

.field private final primaryKey:I

.field private final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/Queue;ILjava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 1
    .param p1, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;
    .param p2, "primaryKey"    # I
    .param p3, "postId"    # Ljava/lang/String;
    .param p4, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    .line 29
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 37
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .line 39
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->postId:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 41
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->primaryKey:I

    .line 42
    return-void
.end method


# virtual methods
.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 11
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v6, 0x0

    .line 46
    const/4 v10, 0x0

    .line 47
    .local v10, "postExistsInList":Z
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v8, v2, :cond_0

    .line 48
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->postId:Ljava/lang/String;

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    sget v5, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ID:I

    invoke-virtual {v2, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 49
    const/4 v10, 0x1

    .line 54
    :cond_0
    if-nez v10, :cond_1

    .line 57
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->postId:Ljava/lang/String;

    invoke-virtual {v2, v3, v5}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 58
    .local v9, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    if-eqz v9, :cond_1

    .line 59
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v1

    .line 61
    .local v1, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 62
    .local v0, "missingPostData":Lcom/google/android/libraries/bind/data/Data;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->primaryKey:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->postId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 65
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v2, v2, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v2, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 66
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->readStateCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    move-result-object v4

    .line 70
    .local v4, "readStateCollection":Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    :goto_1
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-static/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper;->addPostReadingData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;ILcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 80
    invoke-interface {p1, v6, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 83
    .end local v0    # "missingPostData":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .end local v4    # "readStateCollection":Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .end local v9    # "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    :cond_1
    return-object p1

    .line 47
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 67
    .restart local v0    # "missingPostData":Lcom/google/android/libraries/bind/data/Data;
    .restart local v1    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .restart local v9    # "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    :cond_3
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->emptyCollection()Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    move-result-object v4

    goto :goto_1
.end method
