.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Product"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;


# instance fields
.field private avgRating_:F

.field private bitField0_:I

.field private catalogId_:J

.field private description_:Ljava/lang/String;

.field private highPrice_:Ljava/lang/String;

.field private lowPrice_:Ljava/lang/String;

.field private name_:Ljava/lang/String;

.field private numReviews_:I

.field private numStores_:I

.field private query_:Ljava/lang/String;

.field public reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

.field private thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13113
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 13114
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 13384
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->query_:Ljava/lang/String;

    .line 13406
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->catalogId_:J

    .line 13425
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->name_:Ljava/lang/String;

    .line 13447
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->description_:Ljava/lang/String;

    .line 13507
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->lowPrice_:Ljava/lang/String;

    .line 13529
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->highPrice_:Ljava/lang/String;

    .line 13551
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numStores_:I

    .line 13570
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->avgRating_:F

    .line 13589
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numReviews_:I

    .line 13608
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    .line 13114
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;
    .locals 5

    .prologue
    .line 13632
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 13636
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v3, :cond_0

    .line 13637
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 13639
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_1

    .line 13640
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 13642
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 13643
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    .line 13644
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    array-length v3, v3

    if-ge v2, v3, :cond_3

    .line 13645
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    aget-object v3, v3, v2

    if-eqz v3, :cond_2

    .line 13646
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    move-result-object v4

    aput-object v4, v3, v2

    .line 13644
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 13633
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 13634
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 13650
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;
    :cond_3
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 13110
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 13655
    if-ne p1, p0, :cond_1

    .line 13669
    :cond_0
    :goto_0
    return v1

    .line 13656
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 13657
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    .line 13658
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->query_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->query_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->catalogId_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->catalogId_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->name_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->name_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 13660
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->description_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->description_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 13661
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-nez v3, :cond_3

    .line 13662
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_3

    .line 13663
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->lowPrice_:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->lowPrice_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 13664
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->highPrice_:Ljava/lang/String;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->highPrice_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 13665
    :goto_7
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numStores_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numStores_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->avgRating_:F

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->avgRating_:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numReviews_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numReviews_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    .line 13669
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 13658
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->query_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->query_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->name_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->name_:Ljava/lang/String;

    .line 13660
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->description_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->description_:Ljava/lang/String;

    .line 13661
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 13662
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 13663
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->lowPrice_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->lowPrice_:Ljava/lang/String;

    .line 13664
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->highPrice_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->highPrice_:Ljava/lang/String;

    .line 13665
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_7
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13449
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13427
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13386
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->query_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 13741
    const/4 v1, 0x0

    .line 13742
    .local v1, "size":I
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 13743
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->query_:Ljava/lang/String;

    .line 13744
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 13746
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 13747
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->catalogId_:J

    .line 13748
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 13750
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 13751
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->name_:Ljava/lang/String;

    .line 13752
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 13754
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 13755
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->description_:Ljava/lang/String;

    .line 13756
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 13758
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v2, :cond_4

    .line 13759
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 13760
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 13762
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_5

    .line 13763
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 13764
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 13766
    :cond_5
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_6

    .line 13767
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->lowPrice_:Ljava/lang/String;

    .line 13768
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 13770
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_7

    .line 13771
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->highPrice_:Ljava/lang/String;

    .line 13772
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 13774
    :cond_7
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_8

    .line 13775
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numStores_:I

    .line 13776
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 13778
    :cond_8
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_9

    .line 13779
    const/16 v2, 0xa

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->avgRating_:F

    .line 13780
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    .line 13782
    :cond_9
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_a

    .line 13783
    const/16 v2, 0xb

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numReviews_:I

    .line 13784
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 13786
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    if-eqz v2, :cond_c

    .line 13787
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_c

    aget-object v0, v3, v2

    .line 13788
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    if-eqz v0, :cond_b

    .line 13789
    const/16 v5, 0xc

    .line 13790
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 13787
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 13794
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    :cond_c
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->cachedSize:I

    .line 13795
    return v1
.end method

.method public getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 13490
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getUrl()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;
    .locals 1

    .prologue
    .line 13471
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    return-object v0
.end method

.method public hasThumbnail()Z
    .locals 1

    .prologue
    .line 13500
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUrl()Z
    .locals 1

    .prologue
    .line 13481
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 13673
    const/16 v1, 0x11

    .line 13674
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 13675
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->query_:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 13676
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->catalogId_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->catalogId_:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 13677
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->name_:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 13678
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->description_:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 13679
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-nez v2, :cond_4

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 13680
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_5

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 13681
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->lowPrice_:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 13682
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->highPrice_:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    :goto_6
    add-int v1, v4, v2

    .line 13683
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numStores_:I

    add-int v1, v2, v4

    .line 13684
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->avgRating_:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 13685
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numReviews_:I

    add-int v1, v2, v4

    .line 13686
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    if-nez v2, :cond_8

    mul-int/lit8 v1, v1, 0x1f

    .line 13692
    :cond_0
    return v1

    .line 13675
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->query_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 13677
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->name_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 13678
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->description_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 13679
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;->hashCode()I

    move-result v2

    goto :goto_3

    .line 13680
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto :goto_4

    .line 13681
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->lowPrice_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5

    .line 13682
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->highPrice_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6

    .line 13688
    :cond_8
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 13689
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    move v2, v3

    :goto_8
    add-int v1, v4, v2

    .line 13688
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 13689
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->hashCode()I

    move-result v2

    goto :goto_8
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 13803
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 13804
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 13808
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 13809
    :sswitch_0
    return-object p0

    .line 13814
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->query_:Ljava/lang/String;

    .line 13815
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    goto :goto_0

    .line 13819
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->catalogId_:J

    .line 13820
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    goto :goto_0

    .line 13824
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->name_:Ljava/lang/String;

    .line 13825
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    goto :goto_0

    .line 13829
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->description_:Ljava/lang/String;

    .line 13830
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    goto :goto_0

    .line 13834
    :sswitch_5
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-nez v5, :cond_1

    .line 13835
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 13837
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 13841
    :sswitch_6
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v5, :cond_2

    .line 13842
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 13844
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 13848
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->lowPrice_:Ljava/lang/String;

    .line 13849
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    goto :goto_0

    .line 13853
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->highPrice_:Ljava/lang/String;

    .line 13854
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    goto :goto_0

    .line 13858
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numStores_:I

    .line 13859
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    goto/16 :goto_0

    .line 13863
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->avgRating_:F

    .line 13864
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    goto/16 :goto_0

    .line 13868
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numReviews_:I

    .line 13869
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    goto/16 :goto_0

    .line 13873
    :sswitch_c
    const/16 v5, 0x62

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 13874
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    if-nez v5, :cond_4

    move v1, v4

    .line 13875
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    .line 13876
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    if-eqz v5, :cond_3

    .line 13877
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 13879
    :cond_3
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    .line 13880
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 13881
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;-><init>()V

    aput-object v6, v5, v1

    .line 13882
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 13883
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 13880
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 13874
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    array-length v1, v5

    goto :goto_1

    .line 13886
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    :cond_5
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;-><init>()V

    aput-object v6, v5, v1

    .line 13887
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 13804
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x55 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13110
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13697
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 13698
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->query_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 13700
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 13701
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->catalogId_:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 13703
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 13704
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->name_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 13706
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 13707
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->description_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 13709
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v1, :cond_4

    .line 13710
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 13712
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v1, :cond_5

    .line 13713
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 13715
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_6

    .line 13716
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->lowPrice_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 13718
    :cond_6
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_7

    .line 13719
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->highPrice_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 13721
    :cond_7
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_8

    .line 13722
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numStores_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 13724
    :cond_8
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_9

    .line 13725
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->avgRating_:F

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 13727
    :cond_9
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_a

    .line 13728
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->numReviews_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 13730
    :cond_a
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    if-eqz v1, :cond_c

    .line 13731
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->reviews:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_c

    aget-object v0, v2, v1

    .line 13732
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    if-eqz v0, :cond_b

    .line 13733
    const/16 v4, 0xc

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 13731
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 13737
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    :cond_c
    return-void
.end method
