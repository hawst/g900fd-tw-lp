.class public Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;
.super Ljava/lang/Object;
.source "ExploreSingleTopicHeaderFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

.field public final topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V
    .locals 0
    .param p1, "topic"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    .param p2, "expandedShelfType"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 22
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .line 23
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 27
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 28
    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    .line 30
    .local v0, "otherState":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 33
    .end local v0    # "otherState":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->ordinal()I

    move-result v0

    goto :goto_0
.end method
