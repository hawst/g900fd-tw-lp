.class public Lcom/google/apps/dots/android/newsstand/activity/HeaderPlainEditionActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "HeaderPlainEditionActivity.java"


# instance fields
.field private editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>()V

    return-void
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 46
    if-nez p1, :cond_1

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    const-string v0, "HeaderPlainEditionFragment_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/HeaderPlainEditionActivity;->editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->handleExtras(Landroid/os/Bundle;)Z

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 35
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/HeaderPlainEditionActivity;->editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/HeaderPlainEditionActivity;->editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->header_plain_edition_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/HeaderPlainEditionActivity;->setContentView(I)V

    .line 24
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->edition_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/HeaderPlainEditionActivity;->editionFragment:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .line 26
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/HeaderPlainEditionActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 29
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/HeaderPlainEditionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/HeaderPlainEditionActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 30
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 42
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/HeaderPlainEditionActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 43
    return-void
.end method
