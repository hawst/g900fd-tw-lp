.class public Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "MediaItemIntentBuilder.java"


# instance fields
.field private mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

.field private owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private postId:Ljava/lang/String;

.field private readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private restrictToSingleField:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 25
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    instance-of v0, v0, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->activity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->startAudio(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/media/AudioItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 75
    const/4 v0, 0x0

    .line 89
    :goto_0
    return-object v0

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    instance-of v0, v0, Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    if-eqz v0, :cond_1

    .line 77
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->activity:Landroid/app/Activity;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    .line 78
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->setVideoItem(Lcom/google/apps/dots/android/newsstand/media/VideoItem;)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 79
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 80
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->setOwningEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->postId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 85
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->setMediaItem(Lcom/google/apps/dots/android/newsstand/media/MediaItem;)Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->postId:Ljava/lang/String;

    .line 86
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->restrictToSingleField:Z

    .line 87
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->restrictToSingleField(Z)Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 88
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public restrictToSingleField(Z)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;
    .locals 0
    .param p1, "restrictToSingleField"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->restrictToSingleField:Z

    .line 65
    return-object p0
.end method

.method public setMediaItem(Lcom/google/apps/dots/android/newsstand/media/MediaItem;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;
    .locals 0
    .param p1, "mediaItem"    # Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 32
    return-object p0
.end method

.method public setOwningEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 56
    return-object p0
.end method

.method public setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;
    .locals 0
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->postId:Ljava/lang/String;

    .line 40
    return-object p0
.end method

.method public setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 48
    return-object p0
.end method

.method public startIfAvailable()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->startIfAvailable(Z)V

    .line 95
    return-void
.end method

.method public startIfAvailable(Z)V
    .locals 1
    .param p1, "addToBackStack"    # Z

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    instance-of v0, v0, Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    if-eqz v0, :cond_0

    .line 99
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->startIfOnline(Z)V

    .line 103
    :goto_0
    return-void

    .line 101
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->start(Z)V

    goto :goto_0
.end method
