.class public Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;
.super Ljava/lang/Object;
.source "NewsWebViewHtmlProcessor.java"


# static fields
.field private static dfs:Ljava/text/DecimalFormatSymbols;


# instance fields
.field private baseHtml:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;->dfs:Ljava/text/DecimalFormatSymbols;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "baseHtml"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    .line 27
    return-void
.end method

.method private processBaseHtml(F)Ljava/lang/String;
    .locals 11
    .param p1, "viewportDpi"    # F

    .prologue
    const/high16 v10, 0x42c80000    # 100.0f

    .line 38
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 39
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget v7, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v7, v7

    .line 40
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getXDpi()F

    move-result v8

    div-float/2addr v7, v8

    mul-float/2addr v7, p1

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v2, v8

    .line 41
    .local v2, "defaultViewportWidth":I
    iget v7, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v7, v7

    .line 42
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getYDpi()F

    move-result v8

    div-float/2addr v7, v8

    mul-float/2addr v7, p1

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 43
    .local v0, "defaultViewportHeight":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getXDpi()F

    move-result v7

    div-float/2addr v7, p1

    iget v8, v3, Landroid/util/DisplayMetrics;->density:F

    div-float v1, v7, v8

    .line 50
    .local v1, "defaultViewportScale":F
    mul-float v7, v10, v1

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-float v7, v8

    div-float v4, v7, v10

    .line 54
    .local v4, "quantizedDefaultViewportScale":F
    int-to-float v7, v2

    mul-float/2addr v7, v1

    div-float/2addr v7, v4

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v6, v8

    .line 56
    .local v6, "quantizedViewportWidth":I
    int-to-float v7, v0

    mul-float/2addr v7, v1

    div-float/2addr v7, v4

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v5, v8

    .line 59
    .local v5, "quantizedViewportHeight":I
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 58
    invoke-direct {p0, v6, v5, v7, v4}, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;->processBaseHtml(IIIF)Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method private processBaseHtml(IIIF)Ljava/lang/String;
    .locals 14
    .param p1, "viewportWidth"    # I
    .param p2, "viewportHeight"    # I
    .param p3, "viewportDpi"    # I
    .param p4, "viewportScale"    # F

    .prologue
    .line 70
    const-string v9, "%s://%s/"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "newsstand-content"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    .line 71
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    .line 70
    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 72
    .local v2, "contentBaseUri":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    const-string v10, "content://com.google.android.apps.currents/"

    invoke-virtual {v9, v10, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    .line 73
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLoadExtraJs()Ljava/lang/String;

    move-result-object v5

    .line 74
    .local v5, "loadExtraJs":Ljava/lang/String;
    invoke-static {v5}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 75
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    const-string v10, "<head>"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, 0x1e

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, "<head><script src=\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\'></script>"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    .line 78
    :cond_0
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v9, "0.00"

    sget-object v10, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;->dfs:Ljava/text/DecimalFormatSymbols;

    invoke-direct {v7, v9, v10}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 79
    .local v7, "scaleFormat":Ljava/text/DecimalFormat;
    move/from16 v0, p4

    float-to-double v10, v0

    invoke-virtual {v7, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    .line 80
    .local v4, "initialScaleStr":Ljava/lang/String;
    move-object v6, v4

    .line 81
    .local v6, "maxScaleStr":Ljava/lang/String;
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "<meta name=\"viewport\" content=\"width=%1$d,target-densitydpi=%2$d,initial-scale=%3$s,minimum-scale=%4$s,maximum-scale=%5$s,user-scalable=yes\"/>"

    const/4 v11, 0x5

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    .line 84
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    .line 85
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    aput-object v4, v11, v12

    const/4 v12, 0x3

    aput-object v4, v11, v12

    const/4 v12, 0x4

    aput-object v6, v11, v12

    .line 81
    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 89
    .local v8, "viewportReplacement":Ljava/lang/String;
    const-string v9, "<meta name=\"viewport\".*/>"

    invoke-static {v9}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v9

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    .line 90
    invoke-virtual {v9, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 91
    .local v3, "html":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    return-object v3
.end method


# virtual methods
.method public processBaseHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDefaultViewportDpi()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;->processBaseHtml(F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
