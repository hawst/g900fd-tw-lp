.class public final Lcom/google/apps/dots/proto/client/DotsShared$Lab;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lab"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;


# instance fields
.field private bitField0_:I

.field private labId_:I

.field private labName_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29368
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29369
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 29382
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labId_:I

    .line 29401
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    .line 29369
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    .locals 3

    .prologue
    .line 29434
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29438
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    return-object v0

    .line 29435
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    :catch_0
    move-exception v1

    .line 29436
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 29365
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 29443
    if-ne p1, p0, :cond_1

    .line 29447
    :cond_0
    :goto_0
    return v1

    .line 29444
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 29445
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    .line 29446
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labId_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labId_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 29447
    goto :goto_0

    .line 29446
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    .line 29447
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getLabId()I
    .locals 1

    .prologue
    .line 29384
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labId_:I

    return v0
.end method

.method public getLabName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29403
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 29470
    const/4 v0, 0x0

    .line 29471
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 29472
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labId_:I

    .line 29473
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 29475
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 29476
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    .line 29477
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29479
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->cachedSize:I

    .line 29480
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 29451
    const/16 v0, 0x11

    .line 29452
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 29453
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labId_:I

    add-int v0, v1, v2

    .line 29454
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 29455
    return v0

    .line 29454
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29488
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 29489
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 29493
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 29494
    :sswitch_0
    return-object p0

    .line 29499
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 29500
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 29505
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labId_:I

    .line 29506
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    goto :goto_0

    .line 29508
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labId_:I

    goto :goto_0

    .line 29513
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    .line 29514
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    goto :goto_0

    .line 29489
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29365
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    move-result-object v0

    return-object v0
.end method

.method public setLabId(I)Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 29387
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labId_:I

    .line 29388
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    .line 29389
    return-object p0
.end method

.method public setLabName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 29406
    if-nez p1, :cond_0

    .line 29407
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29409
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    .line 29410
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    .line 29411
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29460
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 29461
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labId_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 29463
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 29464
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->labName_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 29466
    :cond_1
    return-void
.end method
