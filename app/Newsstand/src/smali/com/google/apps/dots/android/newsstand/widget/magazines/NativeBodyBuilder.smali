.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;
.super Ljava/lang/Object;
.source "NativeBodyBuilder.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    }
.end annotation


# static fields
.field private static final IMAGE_PART_COUNT_MAX:I = 0xfa

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field public static final NATIVE_BODY_EVENT_SCHEME:Ljava/lang/String; = "nativebody"

.field private static final NAV_TO_SCHEME_ID_PREFIX:Ljava/lang/String; = "navto://"

.field private static final SYSTEM_EVENT_SCOPE:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;


# instance fields
.field private anonPartIdCounter:I

.field private final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

.field private imagePartCounter:I

.field private final nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

.field pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

.field final partModels:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;",
            ">;"
        }
    .end annotation
.end field

.field final post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

.field final postIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item;",
            ">;"
        }
    .end annotation
.end field

.field private final rootPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->SYSTEM_EVENT_SCOPE:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    .line 82
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/util/Map;Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 2
    .param p1, "nativeBodies"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;
    .param p2, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p4, "rootPartView"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;
    .param p5, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item;",
            ">;",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "postIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;>;"
    const/4 v1, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->partModels:Ljava/util/Map;

    .line 95
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->anonPartIdCounter:I

    .line 96
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->imagePartCounter:I

    .line 144
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->rootPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;

    .line 145
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 146
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->postIndex:Ljava/util/Map;

    .line 147
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .line 148
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 149
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->initEventDispatcher()V

    .line 150
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->jumpToPostId(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->jumpToExternalId(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .param p3, "x3"    # Z

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildWebPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    return-void
.end method

.method private buildActivatorPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 5
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "activatorPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 813
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorPartView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v4

    .line 814
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->hasActivatorDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 815
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getActivatorDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorPartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;)V

    .line 813
    invoke-virtual {p1, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    .line 816
    iget-object v0, p2, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildChildren(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 817
    return-void

    .line 815
    :cond_0
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;-><init>()V

    goto :goto_0
.end method

.method private buildChildren(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 7
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "parentPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .param p3, "children"    # [Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 520
    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view:Landroid/view/View;

    instance-of v5, v5, Landroid/view/ViewGroup;

    if-eqz v5, :cond_2

    .line 521
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    .line 524
    .local v3, "parentView":Landroid/view/ViewGroup;
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getType()I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_1

    array-length v5, p3

    if-ne v5, v2, :cond_1

    .line 525
    .local v2, "isOnlyChildOfRoot":Z
    :goto_0
    array-length v5, p3

    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v0, p3, v4

    .line 526
    .local v0, "childPart":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->forChild()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    move-result-object v1

    .line 527
    .local v1, "childPartContext":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    invoke-direct {p0, v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V

    .line 528
    iget-object v6, v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view:Landroid/view/View;

    if-eqz v6, :cond_0

    .line 529
    iget-object v6, v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 525
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .end local v0    # "childPart":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .end local v1    # "childPartContext":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .end local v2    # "isOnlyChildOfRoot":Z
    :cond_1
    move v2, v4

    .line 524
    goto :goto_0

    .line 535
    .end local v3    # "parentView":Landroid/view/ViewGroup;
    :cond_2
    return-void
.end method

.method private buildFlipperPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 4
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "flipperPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 642
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v2

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 644
    .local v0, "flipperPartView":Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    .line 645
    iget-object v1, p2, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-direct {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildPartAdapter(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->setPartAdapter(Landroid/widget/ListAdapter;)V

    .line 646
    return-void
.end method

.method private buildFramePart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 3
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "framePart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 590
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    .line 591
    iget-object v0, p2, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildChildren(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 592
    return-void
.end method

.method private buildImagePart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 8
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "imagePart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 649
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getImageDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;

    move-result-object v3

    .line 650
    .local v3, "imageDetails":Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;
    if-nez v3, :cond_0

    .line 651
    const-string v5, "no imageDetails"

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-direct {p0, v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    .line 668
    :goto_0
    return-void

    .line 657
    :cond_0
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->getImageFieldId()Ljava/lang/String;

    move-result-object v1

    .local v1, "fieldId":Ljava/lang/String;
    if-eqz v1, :cond_1

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->postIndex:Ljava/util/Map;

    .line 659
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;->getImageValueIndex()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getImage(Lcom/google/apps/dots/proto/client/DotsShared$Item;I)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    .local v2, "image":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    if-eqz v2, :cond_1

    .line 660
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    .local v0, "attachmentId":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 661
    .end local v0    # "attachmentId":Ljava/lang/String;
    .end local v2    # "image":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_1
    const-string v6, "problem with image field "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_1
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-direct {p0, v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :cond_2
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 665
    .restart local v0    # "attachmentId":Ljava/lang/String;
    .restart local v2    # "image":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_3
    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V

    .line 666
    .local v4, "imagePartView":Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;
    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->setAttachmentId(Ljava/lang/String;)V

    .line 667
    invoke-virtual {p1, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    goto :goto_0
.end method

.method private buildMediaPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 25
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "mediaPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 698
    invoke-virtual/range {p2 .. p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getMediaDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    move-result-object v22

    .line 699
    .local v22, "mediaDetails":Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;
    if-nez v22, :cond_0

    .line 700
    const-string v4, "no mediaDetails"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    .line 774
    :goto_0
    return-void

    .line 705
    :cond_0
    const/16 v24, 0x0

    .line 706
    .local v24, "streamingVideo":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    const/16 v17, 0x0

    .line 707
    .local v17, "audio":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->getVideoFieldId()Ljava/lang/String;

    move-result-object v18

    .local v18, "fieldId":Ljava/lang/String;
    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->postIndex:Ljava/util/Map;

    .line 708
    move-object/from16 v0, v18

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .local v21, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    if-eqz v21, :cond_1

    .line 709
    invoke-static/range {v21 .. v21}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getStreamingVideo(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    move-result-object v24

    if-nez v24, :cond_3

    .line 710
    invoke-static/range {v21 .. v21}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getAudio(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    move-result-object v17

    if-nez v17, :cond_3

    .line 711
    .end local v21    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_1
    const-string v5, "problem with video field "

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :cond_2
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 714
    .restart local v21    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_3
    if-eqz v24, :cond_4

    .line 715
    invoke-virtual/range {v24 .. v24}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getOriginalUri()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 716
    invoke-virtual/range {v24 .. v24}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getOriginalUri()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".mov"

    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->endsWithIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 717
    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 718
    invoke-virtual/range {v24 .. v24}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getOriginalUri()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x2c

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v7, v10

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "video field "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " looks like a .mov, unplayable: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 717
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 723
    :cond_4
    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;-><init>(Landroid/app/Activity;)V

    new-instance v5, Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v6, v6, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    const/4 v7, 0x0

    .line 724
    invoke-static/range {v21 .. v21}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v10

    move-object/from16 v0, v18

    invoke-direct {v5, v6, v7, v0, v10}, Lcom/google/apps/dots/android/newsstand/media/VideoItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->setVideoItem(Lcom/google/apps/dots/android/newsstand/media/VideoItem;)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    move-result-object v4

    const/4 v5, 0x1

    .line 725
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->setAutoStart(Z)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    move-result-object v4

    const/4 v5, 0x1

    .line 726
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->setCloseOnCompletion(Z)Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;

    move-result-object v4

    .line 727
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/VideoIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v20

    .line 729
    .local v20, "fullScreenIntent":Landroid/content/Intent;
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->getForceFullscreen()Z

    move-result v19

    .line 731
    .local v19, "forceFullscreen":Z
    if-nez v19, :cond_9

    .line 733
    new-instance v8, Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

    invoke-direct {v8}, Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;-><init>()V

    .line 734
    .local v8, "options":Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->getEnableControls()Z

    move-result v4

    iput-boolean v4, v8, Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;->enableControls:Z

    .line 735
    move-object/from16 v0, v20

    iput-object v0, v8, Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;->fullScreenIntent:Landroid/content/Intent;

    .line 736
    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;

    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    move-object/from16 v7, p2

    invoke-direct/range {v3 .. v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;)V

    .line 738
    .local v3, "mediaPartView":Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;
    if-eqz v24, :cond_7

    .line 739
    invoke-virtual/range {v24 .. v24}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->hasAttachmentId()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 740
    invoke-virtual/range {v24 .. v24}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getAttachmentId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->setAttachmentId(Ljava/lang/String;)V

    .line 751
    :cond_5
    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    goto/16 :goto_0

    .line 741
    :cond_6
    invoke-virtual/range {v24 .. v24}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->hasOriginalUri()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 742
    invoke-virtual/range {v24 .. v24}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getOriginalUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->setVideoUri(Landroid/net/Uri;)V

    goto :goto_2

    .line 745
    :cond_7
    invoke-virtual/range {v17 .. v17}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->hasAttachmentId()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 746
    invoke-virtual/range {v17 .. v17}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getAttachmentId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->setAttachmentId(Ljava/lang/String;)V

    goto :goto_2

    .line 747
    :cond_8
    invoke-virtual/range {v17 .. v17}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->hasOriginalUri()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 748
    invoke-virtual/range {v17 .. v17}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getOriginalUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->setVideoUri(Landroid/net/Uri;)V

    goto :goto_2

    .line 754
    .end local v3    # "mediaPartView":Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;
    .end local v8    # "options":Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;
    :cond_9
    invoke-virtual/range {p2 .. p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 755
    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->generateAnonymousPartId()Ljava/lang/String;

    move-result-object v12

    .line 756
    .local v12, "partId":Ljava/lang/String;
    :goto_3
    new-instance v9, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchPartView;

    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v10

    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v11

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-direct/range {v9 .. v16}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchPartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Ljava/lang/String;IIZZ)V

    .line 758
    .local v9, "fullScreenButton":Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchPartView;
    new-instance v23, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$9;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v12, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$9;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Ljava/lang/String;Landroid/content/Intent;)V

    .line 768
    .local v23, "startFullscreen":Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    sget-object v5, Lcom/google/apps/dots/shared/EventCode;->INPUT_TOUCH_ON_TAP:Lcom/google/apps/dots/shared/EventCode;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v12, v6, v7

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    move-object/from16 v0, v23

    invoke-virtual {v4, v5, v6, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 770
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    sget-object v5, Lcom/google/apps/dots/shared/EventCode;->MEDIA_DO_START:Lcom/google/apps/dots/shared/EventCode;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v12, v6, v7

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    move-object/from16 v0, v23

    invoke-virtual {v4, v5, v6, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 772
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    goto/16 :goto_0

    .line 755
    .end local v9    # "fullScreenButton":Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchPartView;
    .end local v12    # "partId":Ljava/lang/String;
    .end local v23    # "startFullscreen":Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;
    :cond_a
    invoke-virtual/range {p2 .. p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v12

    goto :goto_3
.end method

.method private buildPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 1
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "part"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 360
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V

    .line 361
    return-void
.end method

.method private buildPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V
    .locals 11
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "part"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .param p3, "isOnlyChildOfRoot"    # Z

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->partModels:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;

    iput-object v0, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->model:Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;

    .line 369
    const/4 v9, 0x0

    .line 370
    .local v9, "layoutParams":Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView$LayoutParams;
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getType()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 371
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->configureLayoutParams(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView$LayoutParams;

    move-result-object v9

    .line 374
    :cond_0
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->hasType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 453
    :cond_1
    :goto_0
    :pswitch_0
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 454
    iget-object v10, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view:Landroid/view/View;

    .line 456
    .local v10, "resultView":Landroid/view/View;
    invoke-static {v10, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->setViewBuildContext(Landroid/view/View;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;)V

    .line 457
    if-eqz v9, :cond_2

    .line 458
    invoke-virtual {v10, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 461
    :cond_2
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getAppearance()Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    move-result-object v7

    .line 462
    .local v7, "appearance":Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->hasBackgroundColor()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 463
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->parseQuietly(Ljava/lang/String;I)I

    move-result v8

    .line 464
    .local v8, "color":I
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->isTransparent(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 465
    invoke-virtual {v10, v8}, Landroid/view/View;->setBackgroundColor(I)V

    .line 469
    .end local v8    # "color":I
    :cond_3
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->model:Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;

    if-eqz v0, :cond_6

    .line 470
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->model:Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;->bindTo(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;)V

    .line 477
    .end local v7    # "appearance":Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;
    .end local v10    # "resultView":Landroid/view/View;
    :cond_4
    :goto_1
    return-void

    .line 377
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildRootPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    goto :goto_0

    .line 380
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildFramePart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    goto :goto_0

    .line 383
    :pswitch_3
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildScrollPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V

    goto :goto_0

    .line 386
    :pswitch_4
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildFlipperPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    goto :goto_0

    .line 389
    :pswitch_5
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->imagePartCounter:I

    const/16 v1, 0xfa

    if-gt v0, v1, :cond_5

    .line 390
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildImagePart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 391
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->imagePartCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->imagePartCounter:I

    goto :goto_0

    .line 393
    :cond_5
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Exceeded ImagePart limit for %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v4, v4, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 397
    :pswitch_6
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildPdfPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    goto :goto_0

    .line 400
    :pswitch_7
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildMediaPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    goto :goto_0

    .line 404
    :pswitch_8
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v3

    move-object v1, p0

    move v4, p3

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;ZLcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    goto/16 :goto_0

    .line 437
    :pswitch_9
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildTouchPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    goto/16 :goto_0

    .line 446
    :pswitch_a
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildActivatorPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    goto/16 :goto_0

    .line 472
    .restart local v7    # "appearance":Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;
    .restart local v10    # "resultView":Landroid/view/View;
    :cond_6
    if-eqz v7, :cond_4

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->hasVisible()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 473
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->getVisible()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_7
    const/4 v0, 0x4

    goto :goto_2

    .line 375
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_a
    .end packed-switch
.end method

.method private buildPartAdapter(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)Landroid/widget/ListAdapter;
    .locals 1
    .param p1, "parentPartContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "children"    # [Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 481
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$8;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$8;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;)V

    return-object v0
.end method

.method private buildPdfPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 12
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "pdfPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 671
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPdfDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;

    move-result-object v8

    .line 672
    .local v8, "pdfDetails":Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;
    if-nez v8, :cond_0

    .line 673
    const-string v1, "no pdfDetails"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-direct {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    .line 695
    :goto_0
    return-void

    .line 679
    :cond_0
    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->getPdfFieldId()Ljava/lang/String;

    move-result-object v6

    .local v6, "fieldId":Ljava/lang/String;
    if-eqz v6, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->postIndex:Ljava/util/Map;

    .line 680
    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getPdf(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    move-result-object v7

    .local v7, "pdf":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;
    if-eqz v7, :cond_1

    .line 681
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;->getAttachmentId()Ljava/lang/String;

    move-result-object v3

    .local v3, "attachmentId":Ljava/lang/String;
    if-nez v3, :cond_3

    .line 682
    .end local v3    # "attachmentId":Ljava/lang/String;
    .end local v7    # "pdf":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;
    :cond_1
    const-string v2, "problem with pdf field "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-direct {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 685
    .restart local v3    # "attachmentId":Ljava/lang/String;
    .restart local v7    # "pdf":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;
    :cond_3
    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->getPage()I

    move-result v1

    add-int/lit8 v4, v1, 0x1

    .line 686
    .local v4, "page":I
    const/4 v10, 0x0

    .line 688
    .local v10, "view":Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;
    :try_start_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v2

    .line 689
    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;->getBackgroundColor()Ljava/lang/String;

    move-result-object v5

    const/4 v11, -0x1

    invoke-static {v5, v11}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->parseQuietly(Ljava/lang/String;I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 694
    .end local v10    # "view":Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;
    .local v0, "view":Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;
    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    goto :goto_0

    .line 690
    .end local v0    # "view":Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;
    .restart local v10    # "view":Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;
    :catch_0
    move-exception v9

    .line 692
    .local v9, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Cannot render PDFs because the library could not be loaded."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v9, v2, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v10

    .end local v10    # "view":Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;
    .restart local v0    # "view":Lcom/google/apps/dots/android/newsstand/widget/magazines/PdfPartView;
    goto :goto_2
.end method

.method private buildRootPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 4
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "rootPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 574
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->rootPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;->setNBContext(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V

    .line 575
    const/4 v0, 0x0

    .line 576
    .local v0, "layoutDetails":Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->hasLayoutDetails()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 577
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getLayoutDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    move-result-object v0

    .line 578
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->rootPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;

    .line 580
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->hasLocation()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->getLocation()Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    move-result-object v1

    :goto_0
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 579
    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyUtil;->toRectF(Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v1

    .line 578
    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;->letterboxForContentRect(Landroid/graphics/RectF;)V

    .line 585
    :goto_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->rootPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;

    invoke-virtual {p1, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    .line 586
    iget-object v1, p2, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildChildren(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 587
    return-void

    .line 580
    :cond_0
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;-><init>()V

    goto :goto_0

    .line 583
    :cond_1
    const-string v1, "no layout info for root"

    invoke-direct {p0, v1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_1
.end method

.method private buildScrollPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V
    .locals 8
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "scrollPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .param p3, "isOnlyChildOfRoot"    # Z

    .prologue
    .line 608
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v2

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V

    .line 611
    .local v0, "scrollPartView":Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->hasScrollDetails()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getScrollDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    move-result-object v6

    .line 613
    .local v6, "scrollDetails":Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;
    :goto_0
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->hasZoomMax()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 614
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->getZoomMax()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setMaxScale(F)V

    .line 616
    :cond_0
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->hasZoomMin()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 617
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->getZoomMin()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setMinScale(F)V

    .line 619
    :cond_1
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->hasInitialOffset()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 621
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getLetterboxScale()F

    move-result v1

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->getInitialOffset()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getX()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 622
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getLetterboxScale()F

    move-result v2

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->getInitialOffset()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getY()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    .line 620
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setInitialOffset(FF)V

    .line 624
    :cond_2
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->hasScrollExtent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 625
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->getScrollExtent()Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    move-result-object v1

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyUtil;->toRectF(Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v7

    .line 626
    .local v7, "scrollExtent":Landroid/graphics/RectF;
    invoke-virtual {p0, p1, v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->scaleRectForLetterboxing(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setScrollExtent(Landroid/graphics/RectF;)V

    .line 629
    .end local v7    # "scrollExtent":Landroid/graphics/RectF;
    :cond_3
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getLetterboxScale()F

    move-result v1

    iget-object v2, v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 628
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->adjustSnapControlsForLetterboxing(FLjava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setSnapControls(Ljava/util/List;)V

    .line 631
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    .line 633
    iget-object v1, p2, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildChildren(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 636
    if-eqz p3, :cond_4

    .line 637
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    .line 639
    :cond_4
    return-void

    .line 611
    .end local v6    # "scrollDetails":Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;
    :cond_5
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;-><init>()V

    goto :goto_0
.end method

.method private buildTouchPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 9
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "touchPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 595
    .line 596
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->hasTouchDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getTouchDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    move-result-object v8

    .line 597
    .local v8, "touchDetails":Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;
    :goto_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchPartView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v2

    .line 598
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v3

    .line 599
    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->getNumTicksHorizontal()I

    move-result v4

    .line 600
    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->getNumTicksVertical()I

    move-result v5

    .line 601
    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->getAllowFling()Z

    move-result v6

    .line 602
    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;->getWrapAround()Z

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchPartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Ljava/lang/String;IIZZ)V

    .line 597
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    .line 603
    iget-object v0, p2, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildChildren(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 604
    return-void

    .line 596
    .end local v8    # "touchDetails":Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;
    :cond_0
    new-instance v8, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;

    invoke-direct {v8}, Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;-><init>()V

    goto :goto_0
.end method

.method private buildWebPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V
    .locals 12
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "webPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .param p3, "isOnlyChildOfRoot"    # Z

    .prologue
    .line 778
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getWebDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    move-result-object v11

    .line 779
    .local v11, "webDetails":Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;
    if-nez v11, :cond_1

    .line 780
    const-string v1, "no webDetails"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-direct {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    .line 810
    :cond_0
    :goto_0
    return-void

    .line 783
    :cond_1
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->getWebFieldId()Ljava/lang/String;

    move-result-object v6

    .line 785
    .local v6, "fieldId":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->getWebFieldId()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->postIndex:Ljava/util/Map;

    .line 786
    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getInlineFrame(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    move-result-object v9

    .local v9, "inlineFrame":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    if-nez v9, :cond_4

    .line 787
    .end local v9    # "inlineFrame":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    :cond_2
    const-string v2, "problem with web field "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-direct {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 791
    .restart local v9    # "inlineFrame":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    :cond_4
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    .line 792
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 794
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSectionId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v5, v5, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    const/4 v10, 0x0

    move-object v7, p2

    move v8, p3

    invoke-direct/range {v0 .. v10}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;ZLcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;Z)V

    .line 801
    .local v0, "webPartView":Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->getTransparentBackground()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 802
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->makeBackgroundTransparent()V

    .line 804
    :cond_5
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    .line 806
    if-eqz p3, :cond_0

    .line 808
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    goto :goto_0
.end method

.method private configureLayoutParams(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView$LayoutParams;
    .locals 5
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "part"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 558
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getLayoutDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    move-result-object v1

    .line 559
    .local v1, "layoutDetails":Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;
    if-nez v1, :cond_0

    .line 560
    const/4 v3, 0x0

    .line 569
    :goto_0
    return-object v3

    .line 562
    :cond_0
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->getLocation()Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    move-result-object v0

    .line 563
    .local v0, "contentLocation":Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    invoke-static {v0, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyUtil;->toRectF(Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v2

    .line 564
    .local v2, "scaledLocation":Landroid/graphics/RectF;
    invoke-virtual {p0, p1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->scaleRectForLetterboxing(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    .line 567
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->location(Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    .line 569
    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView$LayoutParams;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->getMatchParentExtent()Z

    move-result v4

    invoke-direct {v3, v2, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView$LayoutParams;-><init>(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method private generateAnonymousPartId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 173
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->anonPartIdCounter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->anonPartIdCounter:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "__part_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->rootPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    return-object v0
.end method

.method private getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;
    .locals 0

    .prologue
    .line 296
    return-object p0
.end method

.method private static getViewBuildContext(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 512
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->tagNBBuildContext:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    return-object v0
.end method

.method private jumpToExternalId(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
    .locals 8
    .param p1, "externalId"    # Ljava/lang/String;
    .param p2, "pageLocation"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .prologue
    .line 249
    const-string v4, "navto://"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 250
    .local v1, "fullNavTo":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getExternalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    const-string v5, "external_postUrl"

    .line 252
    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item;

    move-result-object v4

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getUrlHref(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Ljava/lang/String;

    move-result-object v4

    .line 251
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    const-string v5, "external_postResolvedUrl"

    .line 254
    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item;

    move-result-object v4

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getUrlHref(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Ljava/lang/String;

    move-result-object v4

    .line 253
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 255
    :cond_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    if-eqz v4, :cond_2

    .line 256
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    invoke-interface {v4, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    .line 276
    :goto_1
    return-void

    .line 249
    .end local v1    # "fullNavTo":Ljava/lang/String;
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 260
    .restart local v1    # "fullNavTo":Ljava/lang/String;
    :cond_2
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    .line 261
    .local v0, "activity":Landroid/app/Activity;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    .line 262
    .local v3, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 263
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSectionId()Ljava/lang/String;

    move-result-object v5

    const-string v4, "navto://"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v6, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 262
    :goto_2
    invoke-static {v0, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher;->findPostIdInSectionWithUrl(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 264
    .local v2, "postIdFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/String;>;"
    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$5;

    invoke-direct {v4, p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$5;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    invoke-virtual {v3, v2, v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 263
    .end local v2    # "postIdFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/String;>;"
    :cond_3
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private jumpToPostId(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
    .locals 6
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "pageLocation"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .prologue
    .line 227
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 228
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    if-eqz v3, :cond_1

    .line 229
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    invoke-interface {v3, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    if-eqz p1, :cond_0

    .line 237
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->tryParseObjectId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v1

    .local v1, "id":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    if-eqz v1, :cond_0

    const/4 v3, 0x0

    .line 238
    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;I)Ljava/lang/String;

    move-result-object v0

    .local v0, "appId":Ljava/lang/String;
    if-eqz v0, :cond_0

    const/4 v3, 0x4

    .line 239
    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;I)Ljava/lang/String;

    move-result-object v2

    .local v2, "sectionId":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 242
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v4

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->magazineEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 243
    invoke-virtual {v3, p1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v3

    .line 244
    invoke-virtual {v3, p2}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v3

    const/4 v4, 0x1

    .line 245
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->start(Z)V

    goto :goto_0
.end method

.method private logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "message"    # Lcom/google/protobuf/nano/MessageNano;

    .prologue
    .line 279
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$6;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$6;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Lcom/google/apps/dots/android/newsstand/async/Queue;Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    .line 284
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$6;->execute()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 285
    return-void
.end method

.method private static setViewBuildContext(Landroid/view/View;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    .prologue
    .line 508
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->tagNBBuildContext:I

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 509
    return-void
.end method


# virtual methods
.method public buildHierarchy()V
    .locals 5

    .prologue
    .line 300
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->rootPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->rootPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;->getHeight()I

    move-result v4

    if-le v3, v4, :cond_0

    const/4 v0, 0x1

    .line 301
    .local v0, "landscape":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->getLandscapeNativeBody()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    move-result-object v1

    .line 303
    .local v1, "nativeBody":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
    :goto_1
    if-nez v1, :cond_2

    .line 304
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x22

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "missing body for landscape = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    invoke-direct {p0, v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    .line 319
    :goto_2
    return-void

    .line 300
    .end local v0    # "landscape":Z
    .end local v1    # "nativeBody":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 301
    .restart local v0    # "landscape":Z
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .line 302
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->getPortraitNativeBody()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    move-result-object v1

    goto :goto_1

    .line 307
    .restart local v1    # "nativeBody":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
    :cond_2
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->hasRootPart()Z

    move-result v3

    if-nez v3, :cond_3

    .line 308
    const-string v3, "no root part"

    invoke-direct {p0, v3, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_2

    .line 311
    :cond_3
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->getRootPart()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    move-result-object v2

    .line 312
    .local v2, "rootPart":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->hasType()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getType()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_4

    .line 313
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildPartModels(Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 315
    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    invoke-direct {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;-><init>()V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->SYSTEM_EVENT_SCOPE:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->eventScope(Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    move-result-object v3

    invoke-direct {p0, v3, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    goto :goto_2

    .line 317
    :cond_4
    const-string v3, "root part not ROOT_PART"

    invoke-direct {p0, v3, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->logInvalidMessage(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_2
.end method

.method public buildPartModels(Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 6
    .param p1, "part"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 325
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v1

    .line 326
    .local v1, "partId":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 327
    const/4 v2, 0x0

    .line 328
    .local v2, "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->hasType()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 329
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getType()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 341
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;

    .end local v2    # "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;
    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;-><init>(Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 345
    .restart local v2    # "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;
    :cond_0
    :goto_0
    :sswitch_0
    if-eqz v2, :cond_1

    .line 347
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getNBContext()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->SYSTEM_EVENT_SCOPE:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    invoke-virtual {v2, v3, v4, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;->configureEvents(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 348
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->partModels:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    .end local v2    # "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;
    :cond_1
    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_2

    aget-object v0, v4, v3

    .line 352
    .local v0, "child":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildPartModels(Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 351
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 334
    .end local v0    # "child":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .restart local v2    # "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;
    :sswitch_1
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/SwitchPart;

    .end local v2    # "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;
    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SwitchPart;-><init>(Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 335
    .restart local v2    # "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;
    goto :goto_0

    .line 337
    :sswitch_2
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/TimerPart;

    .end local v2    # "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;
    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TimerPart;-><init>(Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 338
    .restart local v2    # "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;
    goto :goto_0

    .line 354
    .end local v2    # "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;
    :cond_2
    return-void

    .line 329
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v0, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    return-object v0
.end method

.method public getAsyncToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    return-object v0
.end method

.method public getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    return-object v0
.end method

.method public getLetterboxScale()F
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->rootPartView:Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;->getLetterboxScale()F

    move-result v0

    return v0
.end method

.method public getPagingScroller()Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    return-object v0
.end method

.method public initEventDispatcher()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 177
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    const-string v1, "nativebody"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    sget-object v1, Lcom/google/apps/dots/shared/EventCode;->SYSTEM_DO_NAV_TO:Lcom/google/apps/dots/shared/EventCode;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->SYSTEM_EVENT_SCOPE:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$1;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 188
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    sget-object v1, Lcom/google/apps/dots/shared/EventCode;->SYSTEM_DO_NAV_TO_PAGE:Lcom/google/apps/dots/shared/EventCode;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->SYSTEM_EVENT_SCOPE:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$2;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 199
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    sget-object v1, Lcom/google/apps/dots/shared/EventCode;->SYSTEM_DO_NAV_TO_RESOURCE:Lcom/google/apps/dots/shared/EventCode;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->SYSTEM_EVENT_SCOPE:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$3;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 211
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->setUnhandledEventCallback(Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 221
    return-void
.end method

.method public onDestroyed(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 821
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getViewBuildContext(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    move-result-object v0

    .line 822
    .local v0, "partContext":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    if-eqz v0, :cond_1

    .line 823
    iget-boolean v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->isEventScopeOwner:Z

    if-eqz v1, :cond_0

    .line 824
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->removeHandlersAndCallbacks(Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;)V

    .line 826
    :cond_0
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->model:Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;

    if-eqz v1, :cond_1

    .line 827
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->model:Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;->unbindFrom(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;)V

    .line 831
    :cond_1
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    if-eqz v1, :cond_2

    .line 832
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->imagePartCounter:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->imagePartCounter:I

    .line 834
    :cond_2
    return-void
.end method

.method public scaleRectForLetterboxing(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 5
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 541
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getLetterboxScale()F

    move-result v2

    .line 544
    .local v2, "scale":F
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->location:Landroid/graphics/RectF;

    iget v0, v3, Landroid/graphics/RectF;->left:F

    .line 545
    .local v0, "locationLeft":F
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->location:Landroid/graphics/RectF;

    iget v1, v3, Landroid/graphics/RectF;->top:F

    .line 546
    .local v1, "locationTop":F
    invoke-virtual {p2, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 547
    invoke-static {p2, v2}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->scale(Landroid/graphics/RectF;F)V

    .line 548
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->round(Landroid/graphics/RectF;)V

    .line 549
    mul-float v3, v0, v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    mul-float v4, v1, v2

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p2, v3, v4}, Landroid/graphics/RectF;->offset(FF)V

    .line 550
    return-object p2
.end method
