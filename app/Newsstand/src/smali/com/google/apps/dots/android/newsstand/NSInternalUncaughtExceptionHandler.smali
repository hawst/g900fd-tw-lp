.class public Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;
.super Ljava/lang/Object;
.source "NSInternalUncaughtExceptionHandler.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final appContext:Landroid/content/Context;

.field private final priorDefault:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;->appContext:Landroid/content/Context;

    .line 24
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;->priorDefault:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 25
    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 10
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    const/4 v9, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 29
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 30
    .local v2, "isMain":Z
    sget-object v6, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "UncaughtException on %s%s"

    new-array v8, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v5

    if-eqz v2, :cond_2

    const-string v3, " (main thread)"

    :goto_0
    aput-object v3, v8, v4

    invoke-virtual {v6, p2, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v3

    if-nez v3, :cond_3

    move v1, v4

    .line 32
    .local v1, "enabled":Z
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;->appContext:Landroid/content/Context;

    .line 33
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v6, Lcom/google/android/apps/newsstanddev/R$bool;->dump_hprof_on_uncaught_exception:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 34
    .local v0, "dumpHprof":Z
    if-eqz v1, :cond_1

    .line 36
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;->appContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isCrashReporterRunningInForeground(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 37
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;->appContext:Landroid/content/Context;

    invoke-static {v3, p2, v0}, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->schedule(Landroid/content/Context;Ljava/lang/Throwable;Z)V

    .line 39
    :cond_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Exception handler called: %s"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-virtual {v3, v6, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    invoke-static {v9}, Ljava/lang/System;->exit(I)V

    .line 42
    :cond_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Calling prior default"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;->priorDefault:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v3, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 44
    return-void

    .line 30
    .end local v0    # "dumpHprof":Z
    .end local v1    # "enabled":Z
    :cond_2
    const-string v3, ""

    goto :goto_0

    :cond_3
    move v1, v5

    .line 31
    goto :goto_1
.end method
