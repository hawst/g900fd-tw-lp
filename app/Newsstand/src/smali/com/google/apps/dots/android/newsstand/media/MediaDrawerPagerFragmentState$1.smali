.class final Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState$1;
.super Ljava/lang/Object;
.source "MediaDrawerPagerFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;
    .locals 8
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 83
    const-class v7, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    invoke-virtual {v7}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 84
    .local v1, "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "postId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-lez v7, :cond_0

    move v3, v0

    .line 86
    .local v3, "restrictToSingleField":Z
    :goto_0
    const-class v7, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v7}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 87
    .local v4, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-lez v7, :cond_1

    move v5, v0

    .line 88
    .local v5, "expandedCaptionVisible":Z
    :goto_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Ljava/lang/String;ZLcom/google/apps/dots/android/newsstand/edition/Edition;Z)V

    return-object v0

    .end local v3    # "restrictToSingleField":Z
    .end local v4    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v5    # "expandedCaptionVisible":Z
    :cond_0
    move v3, v6

    .line 85
    goto :goto_0

    .restart local v3    # "restrictToSingleField":Z
    .restart local v4    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_1
    move v5, v6

    .line 87
    goto :goto_1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 94
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    move-result-object v0

    return-object v0
.end method
