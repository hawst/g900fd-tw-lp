.class public Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;
.super Ljava/lang/Object;
.source "NSClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/http/NSClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClientResponse"
.end annotation


# instance fields
.field public final contentEncoding:Ljava/lang/String;

.field public final contentType:Ljava/lang/String;

.field public final data:Ljava/io/InputStream;

.field public final eTag:Ljava/lang/String;

.field public final expiration:Ljava/lang/Long;

.field public final lastModified:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/io/InputStream;
    .param p2, "eTag"    # Ljava/lang/String;
    .param p3, "lastModified"    # Ljava/lang/Long;
    .param p4, "expiration"    # Ljava/lang/Long;
    .param p5, "contentType"    # Ljava/lang/String;
    .param p6, "contentEncoding"    # Ljava/lang/String;

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->data:Ljava/io/InputStream;

    .line 120
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->eTag:Ljava/lang/String;

    .line 121
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->lastModified:Ljava/lang/Long;

    .line 122
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->expiration:Ljava/lang/Long;

    .line 123
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->contentType:Ljava/lang/String;

    .line 124
    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->contentEncoding:Ljava/lang/String;

    .line 125
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 129
    const-class v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "hasData"

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->data:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 130
    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "eTag"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->eTag:Ljava/lang/String;

    .line 131
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "lastModified"

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->lastModified:Ljava/lang/Long;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/Date;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->lastModified:Ljava/lang/Long;

    .line 132
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "expiration"

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->expiration:Ljava/lang/Long;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/util/Date;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->expiration:Ljava/lang/Long;

    .line 133
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    :goto_2
    invoke-virtual {v1, v2, v0}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "contentType"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->contentType:Ljava/lang/String;

    .line 134
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "contentEncoding"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->contentEncoding:Ljava/lang/String;

    .line 135
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->omitNullValues()Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->lastModified:Ljava/lang/Long;

    goto :goto_1

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;->expiration:Ljava/lang/Long;

    goto :goto_2
.end method
