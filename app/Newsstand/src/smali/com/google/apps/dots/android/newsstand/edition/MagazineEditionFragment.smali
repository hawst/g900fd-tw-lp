.class public Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;
.source "MagazineEditionFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$TransitionDelegate;,
        Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

.field private backgroundView:Landroid/view/View;

.field private cardGroup:Lcom/google/android/libraries/bind/card/GridGroup;

.field private cardList:Lcom/google/android/libraries/bind/data/DataList;

.field private cardListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private cardPaddingWidth:I

.field private connectivityListener:Ljava/lang/Runnable;

.field private currentlyCenteredPosition:I

.field private final editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private hasLiteMode:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

.field private headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

.field private headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

.field private isHeaderFloating:Z

.field private final liteModeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private magazineAccessibilityWarning:Landroid/widget/TextView;

.field private magazineCardList:Lcom/google/android/libraries/bind/data/DataList;

.field private modeToggleMenuItem:Landroid/view/MenuItem;

.field private pageViewOnScrollListener:Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;

.field private postIdToCenterOn:Ljava/lang/String;

.field private restrictToLiteMode:Z

.field private tocType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    const-class v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 168
    const/4 v0, 0x0

    const-string v1, "MagazineEditionFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->magazine_edition_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 158
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 160
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->liteModeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 169
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateRestrictToLiteMode()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # I

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->onPageView(I)V

    return-void
.end method

.method static synthetic access$1202(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->tocType:I

    return p1
.end method

.method static synthetic access$1302(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->hasLiteMode:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNumColumns()I

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/libraries/bind/card/GridGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardGroup:Lcom/google/android/libraries/bind/card/GridGroup;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateMagazineAccessibilityWarning()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->postIdToCenterOn:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Ljava/lang/String;Ljava/lang/Integer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Integer;
    .param p3, "x3"    # Z

    .prologue
    .line 105
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->centerOnPostId(Ljava/lang/String;Ljava/lang/Integer;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->inLiteMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->fetchMostRecentlyReadPost()V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateContinueReadingPost(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPostId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPageNumber()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineAccessibilityWarning:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;ILcom/google/android/libraries/bind/data/Data;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getExpectedCardHeight(ILcom/google/android/libraries/bind/data/Data;)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->currentlyCenteredPosition:I

    return v0
.end method

.method static synthetic access$502(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->currentlyCenteredPosition:I

    return p1
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->isHeaderFloating:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->isHeaderFloating:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->modeToggleMenuItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->backgroundView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->backgroundView:Landroid/view/View;

    return-object p1
.end method

.method private centerOnPostId(Ljava/lang/String;Ljava/lang/Integer;Z)V
    .locals 6
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "optPageNumber"    # Ljava/lang/Integer;
    .param p3, "smoothScroll"    # Z

    .prologue
    .line 987
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 988
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->postIdToCenterOn:Ljava/lang/String;

    .line 1042
    :cond_0
    :goto_0
    return-void

    .line 995
    :cond_1
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->tocType:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_2

    .line 997
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {p1, v4}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->getCombinedPostIdAndPageNumber(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 1001
    .local v1, "cardPrimaryKey":Ljava/lang/String;
    :goto_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4, v1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v2

    .line 1002
    .local v2, "position":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 1003
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 1006
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNumColumns()I

    move-result v4

    div-int/2addr v2, v4

    .line 1008
    add-int/lit8 v2, v2, 0x1

    .line 1010
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->postIdToCenterOn:Ljava/lang/String;

    .line 1011
    move v3, v2

    .line 1015
    .local v3, "positionToScrollTo":I
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->liteModeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;

    invoke-direct {v5, p0, v0, v3, p3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$16;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/android/libraries/bind/data/Data;IZ)V

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 999
    .end local v0    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "cardPrimaryKey":Ljava/lang/String;
    .end local v2    # "position":I
    .end local v3    # "positionToScrollTo":I
    :cond_2
    move-object v1, p1

    .restart local v1    # "cardPrimaryKey":Ljava/lang/String;
    goto :goto_1
.end method

.method private edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 178
    const/4 v0, 0x0

    .line 180
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    goto :goto_0
.end method

.method private fetchMostRecentlyReadPost()V
    .locals 6

    .prologue
    .line 470
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineCardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v4

    invoke-static {v4}, Lcom/google/common/collect/Sets;->newHashSetWithExpectedSize(I)Ljava/util/HashSet;

    move-result-object v3

    .line 471
    .local v3, "tocPostIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineCardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 472
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineCardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4, v0}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v4

    sget v5, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->DK_PRIMARY_KEY:I

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 471
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 474
    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$5;

    invoke-direct {v1, p0, v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Ljava/util/Set;)V

    .line 481
    .local v1, "inThisTocPredicate":Lcom/google/common/base/Predicate;, "Lcom/google/common/base/Predicate<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->liteModeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    .line 483
    .local v2, "liteModeToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->getAppId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->getMostRecentPostIdFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/common/base/Predicate;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$6;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    .line 482
    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 493
    return-void
.end method

.method private findInListView(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/libraries/bind/data/BindingViewGroup;
    .locals 6
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "page"    # Ljava/lang/Integer;

    .prologue
    .line 882
    move-object v0, p1

    .line 885
    .local v0, "primaryKey":Ljava/lang/String;
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->tocType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_0

    .line 886
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->getCombinedPostIdAndPageNumber(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 888
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    const-class v2, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    sget v5, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->DK_PRIMARY_KEY:I

    aput v5, v3, v4

    invoke-static {v1, v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->getFirstViewWithProperty(Landroid/view/ViewGroup;Ljava/lang/Class;Ljava/lang/Object;[I)Lcom/google/android/libraries/bind/data/BindingViewGroup;

    move-result-object v1

    return-object v1
.end method

.method private getExpectedCardHeight(ILcom/google/android/libraries/bind/data/Data;)I
    .locals 2
    .param p1, "listWidth"    # I
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 1048
    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_IMAGE_ID:I

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_TITLE:I

    .line 1049
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1051
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$dimen;->article_image_size_compact:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1068
    :goto_0
    return v0

    .line 1052
    :cond_1
    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IMAGE_ID:I

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-nez v0, :cond_2

    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IMAGE_ID:I

    .line 1053
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1058
    :cond_2
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNumColumns()I

    move-result v0

    div-int v0, p1, v0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardPaddingWidth:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_IMAGE_HEIGHT_TO_WIDTH_RATIO:I

    .line 1059
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsFloat(I)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    .line 1060
    :cond_3
    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IMAGE_ID:I

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1065
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNumColumns()I

    move-result v0

    div-int v0, p1, v0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardPaddingWidth:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IMAGE_HEIGHT_TO_WIDTH_RATIO:I

    .line 1066
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsFloat(I)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    .line 1068
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getHeroPrimaryKey()Ljava/lang/String;
    .locals 3

    .prologue
    .line 899
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineCardList:Lcom/google/android/libraries/bind/data/DataList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 900
    .local v0, "heroLogoData":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_0

    .line 901
    sget v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->DK_PRIMARY_KEY:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    .line 903
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getMagazinePageTransitionView(Lcom/google/android/libraries/bind/data/BindingViewGroup;I)Landroid/view/View;
    .locals 2
    .param p1, "container"    # Lcom/google/android/libraries/bind/data/BindingViewGroup;
    .param p2, "page"    # I

    .prologue
    .line 910
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;

    if-eqz v0, :cond_0

    .line 911
    check-cast p1, Landroid/view/ViewGroup;

    .line 922
    .end local p1    # "container":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    :goto_0
    return-object p1

    .line 912
    .restart local p1    # "container":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    :cond_0
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;

    if-eqz v0, :cond_1

    .line 913
    check-cast p1, Landroid/view/ViewGroup;

    .end local p1    # "container":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->image:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 914
    .restart local p1    # "container":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    :cond_1
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;

    if-eqz v0, :cond_4

    .line 915
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-ne v0, v1, :cond_3

    .line 916
    check-cast p1, Landroid/view/ViewGroup;

    .end local p1    # "container":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    if-nez p2, :cond_2

    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->leftImage:I

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->rightImage:I

    goto :goto_1

    .line 919
    .restart local p1    # "container":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    :cond_3
    check-cast p1, Landroid/view/View;

    goto :goto_0

    .line 922
    :cond_4
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private getNumColumns()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 206
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->tocType:I

    packed-switch v1, :pswitch_data_0

    .line 218
    :goto_0
    :pswitch_0
    return v0

    .line 212
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$integer;->magazine_toc_pair_of_images_num_cols:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    goto :goto_0

    .line 215
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$integer;->magazine_toc_single_image_num_cols:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    goto :goto_0

    .line 206
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private inLiteMode()Z
    .locals 1

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 185
    const/4 v0, 0x0

    .line 187
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->inLiteMode:Z

    goto :goto_0
.end method

.method private initialPageNumber()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 200
    const/4 v0, 0x0

    .line 202
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPageNumber:Ljava/lang/Integer;

    goto :goto_0
.end method

.method private initialPostId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 195
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    goto :goto_0
.end method

.method private onPageView(I)V
    .locals 5
    .param p1, "page"    # I

    .prologue
    .line 1097
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onPageView: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1098
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->sendAnalyticsEditionEventIfNeeded(I)V

    .line 1099
    return-void
.end method

.method private sendAnalyticsEditionEventIfNeeded(I)V
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 1102
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1108
    :cond_0
    :goto_0
    return-void

    .line 1107
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->inLiteMode:Z

    invoke-virtual {v1, p1, v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->trackAnalytics(IZ)V

    goto :goto_0
.end method

.method private setupAdapter()V
    .locals 3

    .prologue
    .line 496
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->MAGAZINE_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    .line 497
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$7;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->MAGAZINE_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 504
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 505
    return-void
.end method

.method private setupHeaderListLayout()V
    .locals 4

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 322
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 323
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 322
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 324
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBackgroundResource(I)V

    .line 325
    return-void
.end method

.method private setupMagazineAccessibilityWarning()V
    .locals 2

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineAccessibilityWarning:Landroid/widget/TextView;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$8;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$8;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 515
    return-void
.end method

.method private setupObserver()V
    .locals 1

    .prologue
    .line 418
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 461
    return-void
.end method

.method private setupPageViewAnalytics()V
    .locals 2

    .prologue
    .line 406
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$3;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->pageViewOnScrollListener:Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;

    .line 412
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->pageViewOnScrollListener:Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 413
    return-void
.end method

.method private updateActionBarTitle()V
    .locals 3

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$10;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$10;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 614
    return-void
.end method

.method private updateAdapter()V
    .locals 4

    .prologue
    .line 678
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v1

    if-nez v1, :cond_0

    .line 785
    :goto_0
    return-void

    .line 682
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v1, :cond_1

    .line 683
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 686
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->inLiteMode()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->cardList(Landroid/content/Context;Z)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineCardList:Lcom/google/android/libraries/bind/data/DataList;

    .line 690
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineCardList:Lcom/google/android/libraries/bind/data/DataList;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    invoke-direct {v2, p0, v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    .line 775
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->pageViewOnScrollListener:Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->reset()V

    .line 776
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 779
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setFixedNumColumns(I)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardGroup:Lcom/google/android/libraries/bind/card/GridGroup;

    .line 780
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 781
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->MAGAZINE_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 782
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->useSpacerTopPadding(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    move-result-object v0

    .line 783
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardGroup:Lcom/google/android/libraries/bind/card/GridGroup;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 784
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    goto :goto_0
.end method

.method private updateContinueReadingPost(Ljava/lang/String;)V
    .locals 6
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 788
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardGroup:Lcom/google/android/libraries/bind/card/GridGroup;

    const-string v2, "continue_reading_header"

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/card/GridGroup;->removeHeader(Ljava/lang/String;)Z

    .line 789
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineCardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    .line 790
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 791
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardGroup:Lcom/google/android/libraries/bind/card/GridGroup;

    .line 793
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v3

    .line 794
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v4

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineCardList:Lcom/google/android/libraries/bind/data/DataList;

    .line 795
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    sget v5, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {v1, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 796
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->inLiteMode()Z

    move-result v5

    .line 792
    invoke-static {v3, v4, v1, v5}, Lcom/google/apps/dots/android/newsstand/card/CardContinueReading;->createCardData(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    const-string v3, "continue_reading_header"

    .line 791
    invoke-virtual {v2, v1, v3}, Lcom/google/android/libraries/bind/card/GridGroup;->addHeader(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 798
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardGroup:Lcom/google/android/libraries/bind/card/GridGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/card/GridGroup;->invalidateRows()V

    .line 800
    :cond_0
    return-void
.end method

.method private updateEditionSummary()V
    .locals 2

    .prologue
    .line 601
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 602
    return-void
.end method

.method private updateHeroView()V
    .locals 6

    .prologue
    .line 620
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    .line 621
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "MagazineEditionFragment_heroAspectRatio"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 622
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$dimen;->magazine_edition_header_cover_height:I

    .line 623
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 624
    .local v0, "headerLogoHeight":I
    int-to-float v2, v0

    .line 625
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "MagazineEditionFragment_heroAspectRatio"

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 626
    .local v1, "headerLogoWidth":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v4, 0x11

    invoke-direct {v3, v1, v0, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 630
    .end local v0    # "headerLogoHeight":I
    .end local v1    # "headerLogoWidth":I
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$11;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$11;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 658
    return-void
.end method

.method private updateImageRotator()V
    .locals 5

    .prologue
    .line 805
    .line 806
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->cardList(Landroid/content/Context;Z)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->EQUALITY_FIELDS:[I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$14;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v3, p0, v4}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$14;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/DataList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 821
    .local v0, "imageIdList":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setImageIdList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 822
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->startRefresh()V

    .line 823
    return-void
.end method

.method private updateMagazineAccessibilityWarning()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 946
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->hasLiteMode:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    if-ne v0, v1, :cond_0

    .line 968
    :goto_0
    return-void

    .line 951
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->hasLiteMode:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;->DOES_NOT_HAVE_LITE_MODE:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    if-ne v0, v1, :cond_1

    .line 953
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setVisibility(I)V

    .line 954
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineAccessibilityWarning:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 957
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$15;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$15;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 965
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setVisibility(I)V

    .line 966
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineAccessibilityWarning:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateRestrictToLiteMode()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 835
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->restrictToLiteMode:Z

    .line 836
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->canForegroundSyncEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 838
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinnedVersion(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;

    move-result-object v0

    .line 839
    .local v0, "pinnedVersion":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 840
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->restrictToLiteMode:Z

    .line 843
    .end local v0    # "pinnedVersion":Ljava/lang/Integer;
    :cond_0
    return-void
.end method

.method private updateTabTitle()V
    .locals 3

    .prologue
    .line 661
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$12;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$12;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 675
    return-void
.end method


# virtual methods
.method protected bridge synthetic createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->createTransitionDelegate()Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$TransitionDelegate;

    move-result-object v0

    return-object v0
.end method

.method protected createTransitionDelegate()Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$TransitionDelegate;
    .locals 2

    .prologue
    .line 1112
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$TransitionDelegate;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$TransitionDelegate;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$1;)V

    return-object v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x1

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 1089
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v1

    .line 1090
    .local v1, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 1091
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "help_context_key"

    const-string v3, "mobile_magazine_object"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    const-string v2, "editionInfo"

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    return-object v0
.end method

.method public handleOtherExtras(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 974
    const-string v1, "MagazineEditionFragment_centerOnInitialPostId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 976
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPostId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPageNumber()Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->findInListView(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/libraries/bind/data/BindingViewGroup;

    move-result-object v0

    .line 977
    .local v0, "match":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    if-nez v0, :cond_0

    .line 978
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPostId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPageNumber()Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->centerOnPostId(Ljava/lang/String;Ljava/lang/Integer;Z)V

    .line 981
    .end local v0    # "match":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    :cond_0
    return-void
.end method

.method public handleRemapSharedElements(Ljava/util/List;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 854
    .local p1, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getUpcomingState()Landroid/os/Bundle;

    move-result-object v0

    .line 855
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 856
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 857
    const-string v6, "MagazineEditionFragment_state"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    .line 858
    .local v4, "state":Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
    iget-object v3, v4, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    .line 859
    .local v3, "postId":Ljava/lang/String;
    iget-object v2, v4, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPageNumber:Ljava/lang/Integer;

    .line 861
    .local v2, "page":Ljava/lang/Integer;
    invoke-direct {p0, v3, v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->findInListView(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/libraries/bind/data/BindingViewGroup;

    move-result-object v1

    .line 863
    .local v1, "match":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    const/4 v5, 0x0

    .line 864
    .local v5, "transitionTargetView":Landroid/view/View;
    if-eqz v1, :cond_3

    .line 865
    if-nez v2, :cond_2

    const/4 v6, 0x0

    :goto_0
    invoke-direct {p0, v1, v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getMagazinePageTransitionView(Lcom/google/android/libraries/bind/data/BindingViewGroup;I)Landroid/view/View;

    move-result-object v5

    .line 870
    :cond_0
    :goto_1
    if-eqz v5, :cond_4

    .line 871
    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->magazine_reading_activity_hero:I

    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {p2, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 879
    .end local v1    # "match":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    .end local v2    # "page":Ljava/lang/Integer;
    .end local v3    # "postId":Ljava/lang/String;
    .end local v4    # "state":Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
    .end local v5    # "transitionTargetView":Landroid/view/View;
    :cond_1
    :goto_2
    return-void

    .line 865
    .restart local v1    # "match":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    .restart local v2    # "page":Ljava/lang/Integer;
    .restart local v3    # "postId":Ljava/lang/String;
    .restart local v4    # "state":Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
    .restart local v5    # "transitionTargetView":Landroid/view/View;
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_0

    .line 866
    :cond_3
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getHeroPrimaryKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 867
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    goto :goto_1

    .line 875
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 876
    invoke-interface {p2}, Ljava/util/Map;->clear()V

    goto :goto_2
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 334
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 335
    sget v0, Lcom/google/android/apps/newsstanddev/R$menu;->magazine_edition_fragment_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 336
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setImageIdList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 400
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->stopRefresh()V

    .line 401
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 402
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onDestroyView()V

    .line 403
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 365
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_switch_mode:I

    if-ne v1, v3, :cond_2

    .line 366
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->restrictToLiteMode:Z

    if-eqz v1, :cond_0

    .line 367
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineLiteOnlyDialog;->show(Landroid/support/v4/app/FragmentActivity;)V

    .line 390
    :goto_0
    return v2

    .line 372
    :cond_0
    new-instance v3, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->inLiteMode()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    invoke-direct {v3, v4, v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V

    invoke-virtual {p0, v3, v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->changeState(Landroid/os/Parcelable;Z)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 375
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v3, 0x102002c

    if-ne v1, v3, :cond_5

    .line 376
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isTaskRoot()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 377
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v0

    .line 378
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-eqz v0, :cond_4

    .line 379
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->goUpHierarchy(Landroid/app/Activity;)V

    .line 387
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->supportFinishAfterTransition()V

    goto :goto_0

    .line 381
    .restart local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_4
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 382
    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 383
    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setLibraryPage(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v1

    .line 384
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->start()V

    goto :goto_2

    .line 390
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_5
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    .line 340
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->menu_switch_mode:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->modeToggleMenuItem:Landroid/view/MenuItem;

    .line 343
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->hasLiteMode:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;->DOES_HAVE_LITE_MODE:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    if-ne v1, v2, :cond_1

    .line 346
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 349
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->inLiteMode()Z

    move-result v0

    .line 350
    .local v0, "isInLiteMode":Z
    if-eqz v0, :cond_0

    .line 351
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->modeToggleMenuItem:Landroid/view/MenuItem;

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->magazine_mode_toggle_image_lite:I

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 355
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->modeToggleMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->getAltFormatToggleA11yString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 356
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->modeToggleMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 357
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->modeToggleMenuItem:Landroid/view/MenuItem;

    invoke-static {v1, v3}, Landroid/support/v4/view/MenuItemCompat;->setShowAsAction(Landroid/view/MenuItem;I)V

    .line 361
    .end local v0    # "isInLiteMode":Z
    :goto_1
    return-void

    .line 353
    .restart local v0    # "isInLiteMode":Z
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->modeToggleMenuItem:Landroid/view/MenuItem;

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->magazine_mode_toggle_image_print:I

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 359
    .end local v0    # "isInLiteMode":Z
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->modeToggleMenuItem:Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 932
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->hasLiteMode:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;->DOES_HAVE_LITE_MODE:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    if-ne v0, v1, :cond_0

    .line 933
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->inLiteMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 936
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 941
    :goto_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onResume()V

    .line 942
    return-void

    .line 938
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->invalidateOptionsMenu()V

    .line 939
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateMagazineAccessibilityWarning()V

    goto :goto_0
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 224
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 225
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->setupHeaderListLayout()V

    .line 227
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->magazine_accessibility_warning:I

    .line 228
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->magazineAccessibilityWarning:Landroid/widget/TextView;

    .line 230
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 238
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->setupPageViewAnalytics()V

    .line 239
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->setupObserver()V

    .line 240
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->setupAdapter()V

    .line 241
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->setupMagazineAccessibilityWarning()V

    .line 244
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$dimen;->default_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardPaddingWidth:I

    .line 245
    return-void
.end method

.method protected restoreIntents(Landroid/app/Activity;Landroid/os/Parcelable;)Ljava/util/List;
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Parcelable;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1073
    instance-of v1, p2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 1074
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    .line 1076
    .local v0, "magazineEditionFragmentState":Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/content/Intent;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v3, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 1078
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 1079
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setLibraryPage(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v3

    .line 1080
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;

    invoke-direct {v3, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;)V

    .line 1082
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1076
    invoke-static {v1}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1084
    .end local v0    # "magazineEditionFragmentState":Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected updateErrorView()V
    .locals 1

    .prologue
    .line 826
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->refreshErrorView()Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 827
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 104
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;)V
    .locals 13
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 520
    if-eqz p2, :cond_0

    iget-object v10, p2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v11, p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v10, v11}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    :cond_0
    move v2, v8

    .line 521
    .local v2, "editionChanged":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 522
    sget-object v10, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v11, "updateViews: editionChanged"

    new-array v12, v9, [Ljava/lang/Object;

    invoke-virtual {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 523
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v10}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 524
    sget-object v10, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    iput-object v10, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->hasLiteMode:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    .line 525
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateEditionSummary()V

    .line 526
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateActionBarTitle()V

    .line 527
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateTabTitle()V

    .line 528
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateHeroView()V

    .line 529
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateImageRotator()V

    .line 530
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateErrorView()V

    .line 531
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateRestrictToLiteMode()V

    .line 532
    invoke-direct {p0, v9}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->sendAnalyticsEditionEventIfNeeded(I)V

    .line 535
    :cond_1
    if-nez v2, :cond_2

    iget-boolean v10, p2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->inLiteMode:Z

    .line 536
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iget-boolean v11, p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->inLiteMode:Z

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    :cond_2
    move v3, v8

    .line 537
    .local v3, "inLiteModeChanged":Z
    :goto_1
    if-eqz v3, :cond_4

    .line 538
    sget-object v10, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v11, "updateViews: inLiteModeChanged"

    new-array v12, v9, [Ljava/lang/Object;

    invoke-virtual {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->liteModeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v10}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 544
    const/4 v6, 0x0

    .line 548
    .local v6, "otherModePostId":Ljava/lang/String;
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v10}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getChildCount()I

    move-result v10

    if-lez v10, :cond_9

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 549
    invoke-virtual {v10, v9}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getId()I

    move-result v10

    sget v11, Lcom/google/android/apps/newsstanddev/R$id;->play_header_spacer:I

    if-ne v10, v11, :cond_9

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 550
    invoke-virtual {v10, v9}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    move-result v10

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getHeight()I

    move-result v11

    div-int/lit8 v11, v11, 0x3

    if-le v10, v11, :cond_9

    move v5, v8

    .line 553
    .local v5, "nearTheTop":Z
    :goto_2
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v10}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_3

    if-nez v5, :cond_3

    .line 555
    iget v10, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->currentlyCenteredPosition:I

    add-int/lit8 v10, v10, -0x1

    .line 556
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNumColumns()I

    move-result v11

    mul-int v0, v10, v11

    .line 557
    .local v0, "centeredCardListPosition":I
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    new-instance v11, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$9;

    invoke-direct {v11, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$9;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    invoke-static {v10, v0, v11}, Lcom/google/apps/dots/android/newsstand/util/DataListUtil;->findClosestData(Lcom/google/android/libraries/bind/data/DataList;ILcom/google/common/base/Predicate;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 567
    .local v1, "closestData":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v1, :cond_3

    .line 568
    sget v10, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {v1, v10}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 569
    .local v7, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v10, v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    aget-object v10, v10, v9

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getObjectId()Ljava/lang/String;

    move-result-object v6

    .line 575
    .end local v0    # "centeredCardListPosition":I
    .end local v1    # "closestData":Lcom/google/android/libraries/bind/data/Data;
    .end local v7    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_3
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateAdapter()V

    .line 577
    invoke-static {v6}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 578
    const/4 v10, 0x0

    invoke-direct {p0, v6, v10, v8}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->centerOnPostId(Ljava/lang/String;Ljava/lang/Integer;Z)V

    .line 585
    .end local v5    # "nearTheTop":Z
    .end local v6    # "otherModePostId":Ljava/lang/String;
    :cond_4
    if-nez v3, :cond_5

    iget-object v10, p2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    iget-object v11, p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    .line 586
    invoke-static {v10, v11}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPageNumber:Ljava/lang/Integer;

    iget-object v11, p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPageNumber:Ljava/lang/Integer;

    .line 587
    invoke-static {v10, v11}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_a

    :cond_5
    move v4, v8

    .line 588
    .local v4, "initialPostIdPageNumberChanged":Z
    :goto_3
    if-eqz v4, :cond_6

    .line 589
    sget-object v8, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v10, "updateViews: initialPostId/PageNumberChanged"

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v8, v10, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 593
    if-nez v2, :cond_6

    if-nez v3, :cond_6

    .line 594
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v8}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData()V

    .line 595
    iget-object v8, p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateContinueReadingPost(Ljava/lang/String;)V

    .line 598
    :cond_6
    return-void

    .end local v2    # "editionChanged":Z
    .end local v3    # "inLiteModeChanged":Z
    .end local v4    # "initialPostIdPageNumberChanged":Z
    :cond_7
    move v2, v9

    .line 520
    goto/16 :goto_0

    .restart local v2    # "editionChanged":Z
    :cond_8
    move v3, v9

    .line 536
    goto/16 :goto_1

    .restart local v3    # "inLiteModeChanged":Z
    .restart local v6    # "otherModePostId":Ljava/lang/String;
    :cond_9
    move v5, v9

    .line 550
    goto :goto_2

    .end local v6    # "otherModePostId":Ljava/lang/String;
    :cond_a
    move v4, v9

    .line 587
    goto :goto_3
.end method
