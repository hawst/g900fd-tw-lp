.class Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "Pinner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpinUnsubscribedCurationsAndMigratePinnedEntityTopics(Landroid/accounts/Account;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

.field final synthetic val$account:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    .prologue
    .line 528
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method

.method private getCuratedTopicEditions(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/Map;
    .locals 3
    .param p1, "libraryRoot"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 537
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    .line 538
    .local v0, "curatedTopicEditions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;>;"
    new-instance v1, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    invoke-direct {v1, p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v2, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5$1;

    invoke-direct {v2, p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5$1;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;Ljava/util/Map;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 563
    return-object v0
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 22
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-object/from16 v16, v0

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Pinner;->lock:Ljava/lang/Object;
    invoke-static/range {v16 .. v16}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->access$300(Lcom/google/apps/dots/android/newsstand/sync/Pinner;)Ljava/lang/Object;

    move-result-object v17

    monitor-enter v17

    .line 570
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v12

    .line 572
    .local v12, "pinnedEntityTopics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    move-object/from16 v16, v0

    .line 573
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->getCuratedTopicEditions(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/Map;

    move-result-object v3

    .line 574
    .local v3, "curatedTopicEditions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;>;"
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    .line 575
    .local v4, "curatedTopicEditionsSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    .line 576
    .local v7, "editions":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    invoke-interface {v4, v7}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 623
    .end local v3    # "curatedTopicEditions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;>;"
    .end local v4    # "curatedTopicEditionsSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    .end local v7    # "editions":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    .end local v12    # "pinnedEntityTopics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    :catchall_0
    move-exception v16

    monitor-exit v17
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v16

    .line 580
    .restart local v3    # "curatedTopicEditions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;>;"
    .restart local v4    # "curatedTopicEditionsSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    .restart local v12    # "pinnedEntityTopics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-object/from16 v16, v0

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    invoke-static/range {v16 .. v16}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->access$400(Lcom/google/apps/dots/android/newsstand/sync/Pinner;)Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v10

    .line 581
    .local v10, "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->val$account:Landroid/accounts/Account;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    invoke-static {v0, v10, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->access$500(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v9

    .line 582
    .local v9, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v0, v9, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v13

    .line 584
    .local v13, "pinnedItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 585
    .local v8, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_3

    .line 586
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-virtual/range {v16 .. v16}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromProto(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v6

    .line 587
    .local v6, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v14

    .line 588
    .local v14, "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    sget-object v16, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_2

    invoke-interface {v4, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_2

    .line 590
    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 591
    :cond_2
    sget-object v16, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->TOPIC:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_1

    .line 595
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v12, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 598
    .end local v6    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v14    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    :cond_3
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v16

    move/from16 v0, v16

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v16

    check-cast v16, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-object/from16 v0, v16

    iput-object v0, v9, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Pinner;->storePinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V
    invoke-static {v0, v10, v9}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->access$600(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V

    .line 604
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v16

    if-nez v16, :cond_5

    const/4 v15, 0x1

    .line 607
    .local v15, "unpinAllTopicEditions":Z
    :goto_2
    invoke-interface {v12}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_4
    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_8

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 608
    .local v11, "pinnedEntityTopicId":Ljava/lang/String;
    invoke-interface {v3, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 610
    invoke-interface {v3, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 612
    .local v5, "curatedTopicEditionsToPin":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 613
    .restart local v6    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->val$account:Landroid/accounts/Account;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v6, v2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->pin(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V

    goto :goto_4

    .line 604
    .end local v5    # "curatedTopicEditionsToPin":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    .end local v6    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v11    # "pinnedEntityTopicId":Ljava/lang/String;
    .end local v15    # "unpinAllTopicEditions":Z
    :cond_5
    const/4 v15, 0x0

    goto :goto_2

    .line 616
    .restart local v5    # "curatedTopicEditionsToPin":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    .restart local v11    # "pinnedEntityTopicId":Ljava/lang/String;
    .restart local v15    # "unpinAllTopicEditions":Z
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->val$account:Landroid/accounts/Account;

    move-object/from16 v20, v0

    invoke-interface {v12, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpin(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    goto :goto_3

    .line 617
    .end local v5    # "curatedTopicEditionsToPin":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    :cond_7
    if-eqz v15, :cond_4

    .line 620
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->val$account:Landroid/accounts/Account;

    move-object/from16 v20, v0

    invoke-interface {v12, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpin(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    goto :goto_3

    .line 623
    .end local v11    # "pinnedEntityTopicId":Ljava/lang/String;
    :cond_8
    monitor-exit v17
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 528
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    return-void
.end method
