.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;
.source "ReadNowEditionScreen.java"


# instance fields
.field private final page:I


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;I)V
    .locals 0
    .param p1, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;
    .param p2, "page"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 17
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;->page:I

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 58
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 59
    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;

    .line 60
    .local v0, "screen":Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;->page:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;->page:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 62
    .end local v0    # "screen":Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;
    :cond_0
    return v1
.end method

.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 23
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;->page:I

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPage(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 24
    return-object p1
.end method

.method protected getDedupeExpiryTime()J
    .locals 2

    .prologue
    .line 48
    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method protected getPublisherTrackingId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 29
    const-string v0, "ReadNow"

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ReadNowEditionScreen;->page:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isDedupable()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method
