.class public abstract Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;
.super Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;
.source "ClusteredGridGroup.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final SHELF_HEADER_EQUALITY_FIELDS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->SHELF_HEADER_EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 0
    .param p1, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 31
    return-void
.end method


# virtual methods
.method protected abstract getComparatorForSortingClusters()Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getStringToClusterOn(Lcom/google/android/libraries/bind/data/Data;)Ljava/lang/String;
.end method

.method protected makeRows(Ljava/util/List;I)Ljava/util/List;
    .locals 24
    .param p2, "primaryKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v18

    move-object/from16 v0, v18

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v17, v0

    .line 42
    .local v17, "viewWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->fixedNumColumns:Ljava/lang/Integer;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->fixedNumColumns:Ljava/lang/Integer;

    move-object/from16 v18, v0

    .line 43
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 45
    .local v9, "nColumns":I
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v8

    .line 48
    .local v8, "nCards":I
    div-int v18, v8, v9

    add-int/lit8 v18, v18, 0x1

    invoke-static/range {v18 .. v18}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v13

    .line 49
    .local v13, "rows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;>;"
    const/4 v11, 0x0

    .line 50
    .local v11, "rowCards":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v4, 0x0

    .line 52
    .local v4, "currentStringToClusterOn":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->getComparatorForSortingClusters()Ljava/util/Comparator;

    move-result-object v18

    invoke-static {}, Lcom/google/common/collect/Ordering;->natural()Lcom/google/common/collect/Ordering;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/google/common/collect/TreeMultimap;->create(Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/google/common/collect/TreeMultimap;

    move-result-object v3

    .line 53
    .local v3, "clusterMap":Lcom/google/common/collect/TreeMultimap;, "Lcom/google/common/collect/TreeMultimap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v8, :cond_1

    .line 54
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/android/libraries/bind/data/Data;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->getStringToClusterOn(Lcom/google/android/libraries/bind/data/Data;)Ljava/lang/String;

    move-result-object v15

    .line 55
    .local v15, "stringToClusterOn":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v15, v0}, Lcom/google/common/collect/TreeMultimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 53
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 43
    .end local v3    # "clusterMap":Lcom/google/common/collect/TreeMultimap;, "Lcom/google/common/collect/TreeMultimap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v4    # "currentStringToClusterOn":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v8    # "nCards":I
    .end local v9    # "nColumns":I
    .end local v11    # "rowCards":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v13    # "rows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;>;"
    .end local v15    # "stringToClusterOn":Ljava/lang/String;
    :cond_0
    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->minColumnWidthPx:Ljava/lang/Integer;

    move-object/from16 v19, v0

    .line 44
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    div-int v19, v17, v19

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v9

    goto :goto_0

    .line 58
    .restart local v3    # "clusterMap":Lcom/google/common/collect/TreeMultimap;, "Lcom/google/common/collect/TreeMultimap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v4    # "currentStringToClusterOn":Ljava/lang/String;
    .restart local v6    # "i":I
    .restart local v8    # "nCards":I
    .restart local v9    # "nColumns":I
    .restart local v11    # "rowCards":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v13    # "rows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;>;"
    :cond_1
    const/4 v14, 0x0

    .line 59
    .local v14, "shelfHeaderAdded":Z
    invoke-virtual {v3}, Lcom/google/common/collect/TreeMultimap;->entries()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 61
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 62
    .restart local v15    # "stringToClusterOn":Ljava/lang/String;
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 63
    .local v7, "index":I
    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 64
    move-object v4, v15

    .line 67
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v10

    .line 68
    .local v10, "row":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->maxRows:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ne v10, v0, :cond_3

    .line 94
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v7    # "index":I
    .end local v10    # "row":I
    .end local v15    # "stringToClusterOn":Ljava/lang/String;
    :cond_2
    return-object v13

    .line 72
    .restart local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v7    # "index":I
    .restart local v10    # "row":I
    .restart local v15    # "stringToClusterOn":Ljava/lang/String;
    :cond_3
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v10, v1}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->makeRowData(II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v12

    .line 73
    .local v12, "rowData":Lcom/google/android/libraries/bind/data/Data;
    if-nez v14, :cond_7

    const/16 v18, 0x1

    :goto_3
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v12, v15, v1}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->makeShelfHeader(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v12

    .line 74
    new-instance v18, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;

    const/16 v20, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-direct {v0, v1, v12}, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;-><init>(ILcom/google/android/libraries/bind/data/Data;)V

    move-object/from16 v0, v18

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    const/4 v14, 0x1

    .line 76
    const/4 v11, 0x0

    .line 79
    .end local v10    # "row":I
    .end local v12    # "rowData":Lcom/google/android/libraries/bind/data/Data;
    :cond_4
    if-eqz v11, :cond_5

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ne v0, v9, :cond_6

    .line 81
    :cond_5
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v10

    .line 82
    .restart local v10    # "row":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->maxRows:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v10, v0, :cond_2

    .line 86
    sget-object v18, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v20, "Making generator for row %d"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    invoke-static {v9}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v11

    .line 88
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11, v9}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->makeRowViewGenerator(ILjava/util/List;I)Lcom/google/android/libraries/bind/card/ViewGenerator;

    move-result-object v16

    .line 89
    .local v16, "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v10, v1}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->makeRowData(ILcom/google/android/libraries/bind/card/ViewGenerator;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v12

    .line 90
    .restart local v12    # "rowData":Lcom/google/android/libraries/bind/data/Data;
    new-instance v18, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v12}, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;-><init>(Lcom/google/android/libraries/bind/card/ViewGenerator;Lcom/google/android/libraries/bind/data/Data;)V

    move-object/from16 v0, v18

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    .end local v10    # "row":I
    .end local v12    # "rowData":Lcom/google/android/libraries/bind/data/Data;
    .end local v16    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :cond_6
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 73
    .restart local v10    # "row":I
    .restart local v12    # "rowData":Lcom/google/android/libraries/bind/data/Data;
    :cond_7
    const/16 v18, 0x0

    goto :goto_3
.end method

.method protected makeShelfHeader(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;
    .locals 2
    .param p1, "rowData"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "stringToClusterOn"    # Ljava/lang/String;
    .param p3, "isFirstHeader"    # Z

    .prologue
    .line 98
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    if-nez p3, :cond_0

    sget v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->LAYOUT:I

    .line 99
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 98
    invoke-virtual {p1, v1, v0}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 100
    sget v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->SHELF_HEADER_EQUALITY_FIELDS:[I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 101
    sget v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE:I

    invoke-virtual {p1, v0, p2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 102
    sget v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE_TEXT_COLOR:I

    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->shelf_header_title_color:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 103
    return-object p1

    .line 98
    :cond_0
    sget v0, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->LAYOUT_FIRST:I

    goto :goto_0
.end method
