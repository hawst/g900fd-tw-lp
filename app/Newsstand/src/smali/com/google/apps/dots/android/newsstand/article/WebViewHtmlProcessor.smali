.class public Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;
.super Ljava/lang/Object;
.source "WebViewHtmlProcessor.java"


# static fields
.field private static dfs:Ljava/text/DecimalFormatSymbols;


# instance fields
.field private applyFontSizePreference:Z

.field private baseHtml:Ljava/lang/String;

.field private final widget:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

.field private final zoomable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->dfs:Ljava/text/DecimalFormatSymbols;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "widget"    # Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;
    .param p2, "baseHtml"    # Ljava/lang/String;
    .param p3, "zoomable"    # Z

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->applyFontSizePreference:Z

    .line 38
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->widget:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

    .line 39
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    .line 40
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->zoomable:Z

    .line 41
    return-void
.end method

.method private getDefaultViewportDpi()F
    .locals 5

    .prologue
    .line 149
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v3, v4, :cond_0

    sget-object v0, Lcom/google/apps/dots/shared/ArticleRenderSettings;->FONT_DPI_MAP_PHONE:Ljava/util/Map;

    .line 151
    .local v0, "dpiMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;Ljava/lang/Integer;>;"
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getArticleFontSize()Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    move-result-object v2

    .line 152
    .local v2, "prefsFontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->applyFontSizePreference:Z

    if-eqz v3, :cond_1

    move-object v1, v2

    .line 153
    .local v1, "fontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    :goto_1
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-float v3, v3

    return v3

    .line 149
    .end local v0    # "dpiMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;Ljava/lang/Integer;>;"
    .end local v1    # "fontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    .end local v2    # "prefsFontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    :cond_0
    sget-object v0, Lcom/google/apps/dots/shared/ArticleRenderSettings;->FONT_DPI_MAP_TABLET:Ljava/util/Map;

    goto :goto_0

    .line 152
    .restart local v0    # "dpiMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;Ljava/lang/Integer;>;"
    .restart local v2    # "prefsFontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    :cond_1
    sget-object v1, Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;->MEDIUM:Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    goto :goto_1
.end method

.method private processBaseHtml(FZ)Ljava/lang/String;
    .locals 11
    .param p1, "viewportDpi"    # F
    .param p2, "quantizeScale"    # Z

    .prologue
    const/high16 v10, 0x42c80000    # 100.0f

    .line 83
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 84
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget v7, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v7, v7

    .line 85
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getXDpi()F

    move-result v8

    div-float/2addr v7, v8

    mul-float/2addr v7, p1

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v2, v8

    .line 86
    .local v2, "defaultViewportWidth":I
    iget v7, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v7, v7

    .line 87
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getYDpi()F

    move-result v8

    div-float/2addr v7, v8

    mul-float/2addr v7, p1

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 88
    .local v0, "defaultViewportHeight":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getXDpi()F

    move-result v7

    div-float/2addr v7, p1

    iget v8, v3, Landroid/util/DisplayMetrics;->density:F

    div-float v1, v7, v8

    .line 90
    .local v1, "defaultViewportScale":F
    if-eqz p2, :cond_0

    .line 95
    mul-float v7, v10, v1

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-float v7, v8

    div-float v4, v7, v10

    .line 99
    .local v4, "quantizedDefaultViewportScale":F
    int-to-float v7, v2

    mul-float/2addr v7, v1

    div-float/2addr v7, v4

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v6, v8

    .line 101
    .local v6, "quantizedViewportWidth":I
    int-to-float v7, v0

    mul-float/2addr v7, v1

    div-float/2addr v7, v4

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v5, v8

    .line 104
    .local v5, "quantizedViewportHeight":I
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 103
    invoke-direct {p0, v6, v5, v7, v4}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtml(IIIF)Ljava/lang/String;

    move-result-object v7

    .line 106
    .end local v4    # "quantizedDefaultViewportScale":F
    .end local v5    # "quantizedViewportHeight":I
    .end local v6    # "quantizedViewportWidth":I
    :goto_0
    return-object v7

    .line 107
    :cond_0
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 106
    invoke-direct {p0, v2, v0, v7, v1}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtml(IIIF)Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method private processBaseHtml(IIIF)Ljava/lang/String;
    .locals 16
    .param p1, "viewportWidth"    # I
    .param p2, "viewportHeight"    # I
    .param p3, "viewportDpi"    # I
    .param p4, "viewportScale"    # F

    .prologue
    .line 113
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->widget:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

    move/from16 v0, p3

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p4

    invoke-virtual {v11, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->setCurrentViewportData(IIIF)V

    .line 117
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->widget:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->scrollTo(II)V

    .line 120
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->widget:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getHttpHandle()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    move-result-object v11

    if-eqz v11, :cond_1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->widget:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

    .line 121
    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getHttpHandle()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    move-result-object v11

    invoke-interface {v11}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;->getContentBaseUri()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 123
    .local v4, "contentBaseUri":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    const-string v12, "content://com.google.android.apps.currents/"

    invoke-virtual {v11, v12, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    .line 124
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLoadExtraJs()Ljava/lang/String;

    move-result-object v7

    .line 125
    .local v7, "loadExtraJs":Ljava/lang/String;
    invoke-static {v7}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 126
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    const-string v12, "<head>"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, 0x1e

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v15, "<head><script src=\'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\'></script>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    .line 129
    :cond_0
    new-instance v9, Ljava/text/DecimalFormat;

    const-string v11, "0.00"

    sget-object v12, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->dfs:Ljava/text/DecimalFormatSymbols;

    invoke-direct {v9, v11, v12}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 130
    .local v9, "scaleFormat":Ljava/text/DecimalFormat;
    move/from16 v0, p4

    float-to-double v12, v0

    invoke-virtual {v9, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    .line 131
    .local v6, "initialScaleStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->zoomable:Z

    if-eqz v11, :cond_2

    const/high16 v11, 0x40400000    # 3.0f

    mul-float v11, v11, p4

    float-to-double v12, v11

    .line 132
    invoke-virtual {v9, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    .line 133
    .local v8, "maxScaleStr":Ljava/lang/String;
    :goto_1
    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v12, "<meta name=\"viewport\" content=\"width=%1$d,target-densitydpi=%2$d,initial-scale=%3$s,minimum-scale=%4$s,maximum-scale=%5$s,user-scalable=yes\"/>"

    const/4 v13, 0x5

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 136
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    .line 137
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    aput-object v6, v13, v14

    const/4 v14, 0x3

    aput-object v6, v13, v14

    const/4 v14, 0x4

    aput-object v8, v13, v14

    .line 133
    invoke-static {v11, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 141
    .local v10, "viewportReplacement":Ljava/lang/String;
    const-string v11, "<meta name=\"viewport\".*/>"

    invoke-static {v11}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->baseHtml:Ljava/lang/String;

    .line 142
    invoke-virtual {v11, v12}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 143
    .local v5, "html":Ljava/lang/String;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    return-object v5

    .line 121
    .end local v4    # "contentBaseUri":Ljava/lang/String;
    .end local v5    # "html":Ljava/lang/String;
    .end local v6    # "initialScaleStr":Ljava/lang/String;
    .end local v7    # "loadExtraJs":Ljava/lang/String;
    .end local v8    # "maxScaleStr":Ljava/lang/String;
    .end local v9    # "scaleFormat":Ljava/text/DecimalFormat;
    .end local v10    # "viewportReplacement":Ljava/lang/String;
    :cond_1
    const-string v11, "content://%s/"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    .line 122
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .restart local v4    # "contentBaseUri":Ljava/lang/String;
    .restart local v6    # "initialScaleStr":Ljava/lang/String;
    .restart local v7    # "loadExtraJs":Ljava/lang/String;
    .restart local v9    # "scaleFormat":Ljava/text/DecimalFormat;
    :cond_2
    move-object v8, v6

    .line 132
    goto :goto_1
.end method


# virtual methods
.method public processBaseHtml()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->zoomable:Z

    if-eqz v0, :cond_0

    .line 48
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getXDpi()F

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtml(FZ)Ljava/lang/String;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->getDefaultViewportDpi()F

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtml(FZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public processBaseHtmlForTarget(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;)Ljava/lang/String;
    .locals 5
    .param p1, "target"    # Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    .prologue
    const/4 v4, 0x0

    .line 59
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->getShouldDisplay()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->getWidth()I

    move-result v2

    if-lez v2, :cond_0

    .line 60
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->getHeight()I

    move-result v2

    if-gtz v2, :cond_1

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtml()Ljava/lang/String;

    move-result-object v2

    .line 77
    :goto_0
    return-object v2

    .line 64
    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->widget:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getWidthInInches()F

    move-result v3

    div-float v1, v2, v3

    .line 66
    .local v1, "fitWidthDpi":F
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->widget:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getHeightInInches()F

    move-result v3

    div-float v0, v2, v3

    .line 67
    .local v0, "fitHeightDpi":F
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->getScaling()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 77
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtml()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 69
    :pswitch_0
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-direct {p0, v2, v4}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtml(FZ)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 71
    :pswitch_1
    invoke-direct {p0, v1, v4}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtml(FZ)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 73
    :pswitch_2
    invoke-direct {p0, v0, v4}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtml(FZ)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 67
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
