.class public Lcom/google/apps/dots/android/newsstand/audio/MediaButtonIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaButtonIntentReceiver.java"


# static fields
.field private static mLastClickTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/apps/dots/android/newsstand/audio/MediaButtonIntentReceiver;->mLastClickTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 23
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 24
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 28
    .local v3, "intentAction":Ljava/lang/String;
    const-string v7, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 29
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->pauseAudio(Landroid/content/Context;)V

    goto :goto_0

    .line 30
    :cond_2
    const-string v7, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 31
    const-string v7, "android.intent.extra.KEY_EVENT"

    .line 32
    invoke-virtual {p2, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/view/KeyEvent;

    .line 34
    .local v2, "event":Landroid/view/KeyEvent;
    if-eqz v2, :cond_0

    .line 38
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    .line 39
    .local v6, "keycode":I
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    .line 40
    .local v1, "action":I
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    .line 42
    .local v4, "eventtime":J
    if-nez v1, :cond_4

    .line 43
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v7

    if-nez v7, :cond_4

    .line 50
    const/16 v7, 0x4f

    if-eq v6, v7, :cond_3

    const/16 v7, 0x55

    if-ne v6, v7, :cond_5

    :cond_3
    sget-wide v8, Lcom/google/apps/dots/android/newsstand/audio/MediaButtonIntentReceiver;->mLastClickTime:J

    sub-long v8, v4, v8

    const-wide/16 v10, 0x12c

    cmp-long v7, v8, v10

    if-gez v7, :cond_5

    .line 53
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->nextAudio(Landroid/content/Context;)V

    .line 54
    const-wide/16 v8, 0x0

    sput-wide v8, Lcom/google/apps/dots/android/newsstand/audio/MediaButtonIntentReceiver;->mLastClickTime:J

    .line 84
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/MediaButtonIntentReceiver;->isOrderedBroadcast()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/MediaButtonIntentReceiver;->abortBroadcast()V

    goto :goto_0

    .line 56
    :cond_5
    sparse-switch v6, :sswitch_data_0

    .line 79
    :goto_2
    sput-wide v4, Lcom/google/apps/dots/android/newsstand/audio/MediaButtonIntentReceiver;->mLastClickTime:J

    goto :goto_1

    .line 58
    :sswitch_0
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->togglePlayPauseAudio(Landroid/content/Context;)V

    goto :goto_2

    .line 61
    :sswitch_1
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->cancelAudio(Landroid/content/Context;)V

    goto :goto_2

    .line 64
    :sswitch_2
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->nextAudio(Landroid/content/Context;)V

    goto :goto_2

    .line 67
    :sswitch_3
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->previousAudio(Landroid/content/Context;)V

    goto :goto_2

    .line 70
    :sswitch_4
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->pauseAudio(Landroid/content/Context;)V

    goto :goto_2

    .line 73
    :sswitch_5
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->playAudio(Landroid/content/Context;)V

    goto :goto_2

    .line 76
    :sswitch_6
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->togglePlayPauseAudio(Landroid/content/Context;)V

    goto :goto_2

    .line 56
    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x55 -> :sswitch_6
        0x56 -> :sswitch_1
        0x57 -> :sswitch_2
        0x58 -> :sswitch_3
        0x7e -> :sswitch_5
        0x7f -> :sswitch_4
    .end sparse-switch
.end method
