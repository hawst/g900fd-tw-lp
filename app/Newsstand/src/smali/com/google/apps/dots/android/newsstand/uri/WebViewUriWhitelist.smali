.class public Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;
.super Ljava/lang/Object;
.source "WebViewUriWhitelist.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private whitelistPattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->generatePattern(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method private generatePattern(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "patternLines":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 64
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 65
    .local v2, "patterns":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 66
    .local v1, "pattern":Ljava/lang/String;
    const-string v4, "#"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 69
    const-string v4, "(^%s$)"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    .end local v1    # "pattern":Ljava/lang/String;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 73
    sget-object v3, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Whitelist file empty; not generating pattern."

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    :goto_1
    return-void

    .line 76
    :cond_2
    const/16 v3, 0x7c

    :try_start_0
    invoke-static {v3}, Lcom/google/common/base/Joiner;->on(C)Lcom/google/common/base/Joiner;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->whitelistPattern:Ljava/util/regex/Pattern;

    .line 78
    sget-object v3, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Generated whitelist pattern %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->whitelistPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v7}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/util/regex/PatternSyntaxException;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Problem building webview uri whitelist regex: %s"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/regex/PatternSyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public load()V
    .locals 4

    .prologue
    .line 34
    sget-object v1, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "load()"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    new-instance v0, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist$1;-><init>(Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;)V

    .line 50
    .local v0, "getWhitelistFileContents":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Ljava/util/List<Ljava/lang/String;>;>;"
    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CACHE_WARMUP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/async/Queue;->submit(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist$2;-><init>(Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;)V

    invoke-static {v1, v2}, Lcom/google/common/util/concurrent/Futures;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)V

    .line 61
    return-void
.end method

.method public matches(Ljava/lang/String;)Z
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 86
    sget-object v1, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "matches(%s)"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    const/4 v0, 0x0

    .line 88
    .local v0, "matches":Z
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->whitelistPattern:Ljava/util/regex/Pattern;

    if-nez v1, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->load()V

    .line 93
    :goto_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Does %s match? %b"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    return v0

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->whitelistPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0
.end method
