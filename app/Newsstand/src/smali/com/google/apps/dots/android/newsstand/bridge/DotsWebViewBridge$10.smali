.class Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;
.super Ljava/lang/Object;
.source "DotsWebViewBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->sendAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

.field final synthetic val$action:Ljava/lang/String;

.field final synthetic val$category:Ljava/lang/String;

.field final synthetic val$label:Ljava/lang/String;

.field final synthetic val$pageIndex:I

.field final synthetic val$postId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    .prologue
    .line 360
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->val$postId:Ljava/lang/String;

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->val$pageIndex:I

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->val$category:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->val$action:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->val$label:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 363
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->val$postId:Ljava/lang/String;

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->val$pageIndex:I

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->val$category:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->val$action:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$10;->val$label:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 364
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->track(Z)V

    .line 365
    return-void
.end method
