.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;
.super Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;
.source "FlipperPartView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;
    }
.end annotation


# instance fields
.field private final adjacentStateWindow:I

.field private blockNextTransition:Z

.field private currentState:I

.field private currentTransition:Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;

.field private final enqueueTransitions:Z

.field private final eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

.field private isDestroyed:Z

.field private isLoaded:Z

.field private oldState:I

.field private partAdapter:Landroid/widget/ListAdapter;

.field private final partId:Ljava/lang/String;

.field private final pendingTransitions:Lcom/google/android/libraries/bind/collections/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/bind/collections/RingBuffer",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;",
            ">;"
        }
    .end annotation
.end field

.field private final stateViews:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

.field private final unblockAndProcessPendingRunnable:Ljava/lang/Runnable;

.field private final upcomingStateWindow:I

.field private final waitUntilLoaded:Z

.field private wasCurrentTransitionBlockedOnLoad:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;
    .param p3, "eventScope"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;
    .param p4, "flipperPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    const/4 v5, 0x1

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V

    .line 26
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->waitUntilLoaded:Z

    .line 28
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->enqueueTransitions:Z

    .line 33
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->adjacentStateWindow:I

    .line 35
    const/4 v1, 0x3

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->upcomingStateWindow:I

    .line 65
    invoke-static {}, Lcom/google/android/libraries/bind/collections/RingBuffer;->create()Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->pendingTransitions:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 73
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->stateViews:Landroid/util/SparseArray;

    .line 259
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->unblockAndProcessPendingRunnable:Ljava/lang/Runnable;

    .line 79
    invoke-virtual {p4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->hasFlipperDetails()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getFlipperDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    move-result-object v0

    .line 80
    .local v0, "details":Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;
    :goto_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->getInitialState()I

    move-result v2

    iget-object v3, p4, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v3, v3

    .line 81
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->getLoopAround()Z

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;-><init>(IIZ)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    .line 82
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    .line 84
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;->stateCount:I

    if-nez v1, :cond_0

    .line 85
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->isLoaded:Z

    .line 88
    :cond_0
    invoke-virtual {p4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->partId:Ljava/lang/String;

    .line 89
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->partId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 90
    invoke-interface {p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->configureEvents(Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;)V

    .line 92
    :cond_1
    return-void

    .line 79
    .end local v0    # "details":Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;
    :cond_2
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;-><init>()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;Landroid/net/Uri;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->getAnimationMillis(Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->doTransition(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->cleanup()V

    return-void
.end method

.method static synthetic access$402(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->blockNextTransition:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->processPendingTransitions()V

    return-void
.end method

.method private cleanup()V
    .locals 4

    .prologue
    .line 288
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    iget v3, v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;->stateCount:I

    if-ge v2, v3, :cond_4

    .line 289
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->distanceToCurrentState(I)I

    move-result v1

    .line 290
    .local v1, "d":I
    if-lez v1, :cond_0

    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->isStateUpcoming(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 292
    :cond_0
    if-nez v1, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-direct {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->getStateView(IZ)Landroid/view/View;

    .line 288
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 292
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 294
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->stateViews:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 296
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_1

    .line 297
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->removeView(Landroid/view/View;)V

    .line 298
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->stateViews:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_2

    .line 302
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "d":I
    :cond_4
    return-void
.end method

.method private configureEvents(Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;)V
    .locals 9
    .param p1, "eventDispatcher"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 126
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    iget v3, v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;->stateCount:I

    if-ge v1, v3, :cond_0

    .line 127
    move v2, v1

    .line 128
    .local v2, "index":I
    sget-object v3, Lcom/google/apps/dots/shared/EventCode;->FLIPPER_DO_SKIP_TO:Lcom/google/apps/dots/shared/EventCode;

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->partId:Ljava/lang/String;

    aput-object v5, v4, v6

    .line 129
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$1;

    invoke-direct {v5, p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;I)V

    .line 128
    invoke-virtual {p1, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 126
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 137
    .end local v2    # "index":I
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    iget v3, v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;->stateCount:I

    add-int/lit8 v3, v3, -0x1

    neg-int v1, v3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    iget v3, v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;->stateCount:I

    if-ge v1, v3, :cond_1

    .line 138
    move v0, v1

    .line 139
    .local v0, "delta":I
    sget-object v3, Lcom/google/apps/dots/shared/EventCode;->FLIPPER_DO_SKIP_BY:Lcom/google/apps/dots/shared/EventCode;

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->partId:Ljava/lang/String;

    aput-object v5, v4, v6

    .line 140
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$2;

    invoke-direct {v5, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;I)V

    .line 139
    invoke-virtual {p1, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 137
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 148
    .end local v0    # "delta":I
    :cond_1
    return-void
.end method

.method private distanceToCurrentState(I)I
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 270
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentState:I

    sub-int/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 271
    .local v0, "d":I
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    iget-boolean v1, v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;->loopAround:Z

    if-eqz v1, :cond_0

    .line 272
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;->stateCount:I

    sub-int/2addr v1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 274
    :cond_0
    return v0
.end method

.method private doTransition(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;)V
    .locals 1
    .param p1, "transition"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->pendingTransitions:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->addLast(Ljava/lang/Object;)V

    .line 172
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->processPendingTransitions()V

    .line 173
    return-void
.end method

.method private getAnimationMillis(Landroid/net/Uri;)I
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 121
    const-string v1, "animationMillis"

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getIntQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 122
    .local v0, "millis":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getStateView(IZ)Landroid/view/View;
    .locals 3
    .param p1, "i"    # I
    .param p2, "initiallyVisible"    # Z

    .prologue
    const/4 v0, 0x0

    .line 151
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->stateViews:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    .line 152
    .local v1, "index":I
    if-gez v1, :cond_3

    .line 154
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->isDestroyed:Z

    if-eqz v2, :cond_1

    .line 155
    .local v0, "child":Landroid/view/View;
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->stateViews:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 157
    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->addView(Landroid/view/View;)V

    .line 159
    if-eqz p2, :cond_2

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 163
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    :goto_2
    return-object v0

    .line 154
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->partAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 159
    .restart local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v2, 0x4

    goto :goto_1

    .line 163
    .end local v0    # "child":Landroid/view/View;
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->stateViews:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    move-object v0, v2

    goto :goto_2
.end method

.method private isCurrentChildLoading()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 203
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentState:I

    invoke-direct {p0, v2, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->getStateView(IZ)Landroid/view/View;

    move-result-object v0

    .line 204
    .local v0, "child":Landroid/view/View;
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;

    .end local v0    # "child":Landroid/view/View;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;->isLoadComplete()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private isCurrentTransitionBlockedOnLoad()Z
    .locals 1

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->isCurrentChildLoading()Z

    move-result v0

    return v0
.end method

.method private isStateUpcoming(I)Z
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 278
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->pendingTransitions:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 279
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->pendingTransitions:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;->state:I

    if-ne v1, p1, :cond_0

    .line 280
    const/4 v1, 0x1

    .line 283
    :goto_1
    return v1

    .line 278
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 283
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private proceedWithCurrentTransition()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 223
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->wasCurrentTransitionBlockedOnLoad:Z

    if-nez v3, :cond_1

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 224
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->oldState:I

    invoke-direct {p0, v3, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->getStateView(IZ)Landroid/view/View;

    move-result-object v2

    .line 225
    .local v2, "oldChild":Landroid/view/View;
    if-eqz v2, :cond_2

    .line 226
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentTransition:Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;

    iget v3, v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;->animationMillis:I

    new-instance v6, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$3;

    invoke-direct {v6, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;)V

    invoke-static {v2, v3, v4, v6}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fade(Landroid/view/View;IILjava/lang/Runnable;)V

    .line 237
    :goto_1
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->blockNextTransition:Z

    .line 240
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->partId:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 241
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v1

    .line 242
    .local v1, "eventDispatcher":Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;
    sget-object v3, Lcom/google/apps/dots/shared/EventCode;->FLIPPER_OFF:Lcom/google/apps/dots/shared/EventCode;

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->partId:Ljava/lang/String;

    aput-object v7, v6, v5

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->oldState:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {v3, v6}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->dispatch(Ljava/lang/String;)V

    .line 243
    sget-object v3, Lcom/google/apps/dots/shared/EventCode;->FLIPPER_ON:Lcom/google/apps/dots/shared/EventCode;

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->partId:Ljava/lang/String;

    aput-object v7, v6, v5

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentState:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {v3, v6}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->dispatch(Ljava/lang/String;)V

    .line 246
    .end local v1    # "eventDispatcher":Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;
    :cond_0
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentState:I

    invoke-direct {p0, v3, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->getStateView(IZ)Landroid/view/View;

    move-result-object v0

    .line 247
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_3

    .line 248
    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 249
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentTransition:Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;

    iget v3, v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;->animationMillis:I

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->unblockAndProcessPendingRunnable:Ljava/lang/Runnable;

    invoke-static {v0, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fade(Landroid/view/View;IILjava/lang/Runnable;)V

    .line 257
    :goto_2
    return-void

    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "oldChild":Landroid/view/View;
    :cond_1
    move v3, v5

    .line 223
    goto :goto_0

    .line 234
    .restart local v2    # "oldChild":Landroid/view/View;
    :cond_2
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->cleanup()V

    goto :goto_1

    .line 253
    .restart local v0    # "child":Landroid/view/View;
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->unblockAndProcessPendingRunnable:Ljava/lang/Runnable;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentTransition:Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;

    iget v4, v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;->animationMillis:I

    int-to-long v4, v4

    invoke-virtual {p0, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2
.end method

.method private processPendingTransitions()V
    .locals 4

    .prologue
    .line 176
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->blockNextTransition:Z

    if-eqz v2, :cond_1

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->pendingTransitions:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/collections/RingBuffer;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 184
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->pendingTransitions:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/collections/RingBuffer;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;

    .line 185
    .local v1, "transition":Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;
    iget v2, v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;->state:I

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentState:I

    if-eq v2, v3, :cond_1

    .line 186
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentTransition:Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;

    .line 191
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentState:I

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->oldState:I

    .line 192
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentTransition:Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView$Transition;->state:I

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentState:I

    .line 194
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentState:I

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->getStateView(IZ)Landroid/view/View;

    move-result-object v0

    .line 196
    .local v0, "child":Landroid/view/View;
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->isCurrentTransitionBlockedOnLoad()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->wasCurrentTransitionBlockedOnLoad:Z

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->blockNextTransition:Z

    .line 197
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->wasCurrentTransitionBlockedOnLoad:Z

    if-nez v2, :cond_0

    .line 198
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->proceedWithCurrentTransition()V

    goto :goto_0
.end method


# virtual methods
.method public isLoadComplete()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->isLoaded:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;->isLoadComplete()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->isLoaded:Z

    .line 111
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->isLoaded:Z

    return v0

    .line 110
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 116
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;->onDetachedFromWindow()V

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->isDestroyed:Z

    .line 118
    return-void
.end method

.method public onLoadComplete()V
    .locals 1

    .prologue
    .line 213
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;->onLoadComplete()V

    .line 214
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;->stateCount:I

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->wasCurrentTransitionBlockedOnLoad:Z

    if-eqz v0, :cond_0

    .line 215
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->isCurrentTransitionBlockedOnLoad()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->wasCurrentTransitionBlockedOnLoad:Z

    .line 216
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->wasCurrentTransitionBlockedOnLoad:Z

    if-nez v0, :cond_0

    .line 217
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->proceedWithCurrentTransition()V

    .line 220
    :cond_0
    return-void
.end method

.method public setPartAdapter(Landroid/widget/ListAdapter;)V
    .locals 1
    .param p1, "partAdapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->partAdapter:Landroid/widget/ListAdapter;

    .line 100
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;->stateCount:I

    if-lez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->switcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/StateSwitcher;->state:I

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->currentState:I

    .line 103
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FlipperPartView;->cleanup()V

    .line 105
    :cond_0
    return-void
.end method
