.class public Lcom/google/apps/dots/android/newsstand/widget/NSImageView;
.super Lcom/google/android/libraries/bind/widget/BoundImageView;
.source "NSImageView.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/Bound;
.implements Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "WrongCall"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;
    }
.end annotation


# static fields
.field public static final DEFAULT_FADE_IN_MS:I = 0x12c

.field private static final DEFAULT_FADE_IN_SLOW_LOAD_THRESHOLD_MS:I = 0x32

.field private static final MAX_AUTO_SCALE:F = 2.0f

.field private static final MAX_USER_SCALE:F = 6.0f

.field public static final SCALING_BIAS_HEIGHT:I = 0x2

.field public static final SCALING_BIAS_NONE:I = 0x0

.field public static final SCALING_BIAS_WIDTH:I = 0x1


# instance fields
.field private currentColorFilterColor:I

.field private dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

.field private fadeInMs:I

.field private fadeInSlowLoadThresholdMs:I

.field protected fadeInType:Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

.field private forceLayoutOnSetDrawable:Z

.field private highlightColorIsStrict:Z

.field private highlightColorList:Landroid/content/res/ColorStateList;

.field private ignoreRequestLayout:Z

.field private imageGravity:I

.field private isRead:Z

.field private isScrolling:Z

.field private loadStartTime:J

.field private minScale:F

.field originalRect:Landroid/graphics/RectF;

.field private scaleDetector:Landroid/view/ScaleGestureDetector;

.field scaledRect:Landroid/graphics/RectF;

.field private scalingBias:I

.field private scrollDetector:Landroid/view/GestureDetector;

.field private final transform:Landroid/graphics/Matrix;

.field private zoomable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v5, 0x0

    .line 95
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BoundImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->ignoreRequestLayout:Z

    .line 68
    iput v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->currentColorFilterColor:I

    .line 69
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;->NONE:Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->fadeInType:Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    .line 70
    const/16 v2, 0x12c

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->fadeInMs:I

    .line 71
    const/16 v2, 0x32

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->fadeInSlowLoadThresholdMs:I

    .line 72
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->loadStartTime:J

    .line 73
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->zoomable:Z

    .line 76
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->minScale:F

    .line 77
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isScrolling:Z

    .line 79
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    .line 81
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    .line 82
    const/16 v2, 0x11

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->imageGravity:I

    .line 83
    iput v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scalingBias:I

    .line 332
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->originalRect:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaledRect:Landroid/graphics/RectF;

    .line 96
    sget-object v2, Lcom/google/android/apps/newsstanddev/R$styleable;->NSImageView:[I

    invoke-virtual {p1, p2, v2, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 98
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->NSImageView_pinchzoom:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setZoomable(Z)V

    .line 99
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->NSImageView_scalingBias:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setScalingBias(I)V

    .line 100
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;->values()[Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->NSImageView_fadeInType:I

    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;->NONE:Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    .line 101
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;->ordinal()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    aget-object v2, v2, v3

    .line 100
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setFadeIn(Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;)V

    .line 102
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->NSImageView_forceLayoutOnSetDrawable:I

    .line 103
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->forceLayoutOnSetDrawable:Z

    .line 106
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->NSImageView_highlightColor:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->highlightColorList:Landroid/content/res/ColorStateList;

    .line 107
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->NSImageView_highlightColorIsStrict:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->highlightColorIsStrict:Z

    .line 109
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isInEditMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    const/4 v1, 0x0

    .line 112
    .local v1, "editModeDrawable":Landroid/graphics/drawable/Drawable;
    :try_start_0
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->NSImageView_editModeDrawable:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 116
    :goto_0
    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 120
    .end local v1    # "editModeDrawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 121
    return-void

    .line 113
    .restart local v1    # "editModeDrawable":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Landroid/graphics/Matrix;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    .prologue
    .line 40
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->minScale:F

    return v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;FFLcom/google/apps/dots/android/newsstand/util/Dimensions;[F)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NSImageView;
    .param p1, "x1"    # F
    .param p2, "x2"    # F
    .param p3, "x3"    # Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    .param p4, "x4"    # [F

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->translateImage(FFLcom/google/apps/dots/android/newsstand/util/Dimensions;[F)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isScrolling:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NSImageView;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isScrolling:Z

    return p1
.end method

.method private translateImage(FFLcom/google/apps/dots/android/newsstand/util/Dimensions;[F)V
    .locals 6
    .param p1, "targetX"    # F
    .param p2, "targetY"    # F
    .param p3, "imageDimensions"    # Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    .param p4, "output"    # [F

    .prologue
    const/4 v5, 0x0

    .line 336
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 338
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->originalRect:Landroid/graphics/RectF;

    iget v3, p3, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->width:I

    int-to-float v3, v3

    iget v4, p3, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->height:I

    int-to-float v4, v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 339
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaledRect:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->originalRect:Landroid/graphics/RectF;

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 340
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaledRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaledRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    invoke-static {p1, v5, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->translateInterval(FFFFF)F

    move-result v0

    .line 342
    .local v0, "translateX":F
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaledRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaledRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-static {p2, v5, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->translateInterval(FFFFF)F

    move-result v1

    .line 343
    .local v1, "translateY":F
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 344
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 345
    if-eqz p4, :cond_0

    .line 346
    const/4 v2, 0x0

    aput v0, p4, v2

    .line 347
    const/4 v2, 0x1

    aput v1, p4, v2

    .line 349
    :cond_0
    return-void
.end method

.method public static translateInterval(FFFFF)F
    .locals 5
    .param p0, "target"    # F
    .param p1, "frameMin"    # F
    .param p2, "frameMax"    # F
    .param p3, "imgMin"    # F
    .param p4, "imgMax"    # F

    .prologue
    .line 311
    sub-float v3, p4, p3

    sub-float v4, p2, p1

    sub-float v0, v3, v4

    .line 312
    .local v0, "d":F
    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-lez v3, :cond_2

    .line 313
    sub-float v3, p1, p3

    sub-float v2, v3, v0

    .line 314
    .local v2, "targetMin":F
    sub-float v3, p2, p4

    add-float v1, v3, v0

    .line 315
    .local v1, "targetMax":F
    cmpg-float v3, p0, v2

    if-gez v3, :cond_0

    .line 324
    .end local v1    # "targetMax":F
    .end local v2    # "targetMin":F
    :goto_0
    return v2

    .line 317
    .restart local v1    # "targetMax":F
    .restart local v2    # "targetMin":F
    :cond_0
    cmpl-float v3, p0, v1

    if-lez v3, :cond_1

    move v2, v1

    .line 318
    goto :goto_0

    :cond_1
    move v2, p0

    .line 320
    goto :goto_0

    .line 324
    .end local v1    # "targetMax":F
    .end local v2    # "targetMin":F
    :cond_2
    add-float v3, p2, p1

    sub-float/2addr v3, p4

    sub-float/2addr v3, p3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v2, v3, v4

    goto :goto_0
.end method


# virtual methods
.method public applyGravity()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 474
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    if-nez v3, :cond_0

    .line 486
    :goto_0
    return-void

    .line 477
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    iget v4, v4, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->width:I

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 478
    .local v0, "fitX":F
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    iget v4, v4, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->height:I

    int-to-float v4, v4

    div-float v1, v3, v4

    .line 479
    .local v1, "fitY":F
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->minScale:F

    .line 480
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 481
    .local v2, "outRect":Landroid/graphics/Rect;
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->imageGravity:I

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->minScale:F

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    iget v5, v5, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->width:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->minScale:F

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    iget v6, v6, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->height:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    .line 482
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getHeight()I

    move-result v8

    invoke-direct {v6, v9, v9, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 481
    invoke-static {v3, v4, v5, v6, v2}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 483
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->minScale:F

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->minScale:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 484
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget v5, v2, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 485
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public canScroll(I)Z
    .locals 7
    .param p1, "dx"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 280
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->zoomable:Z

    if-nez v2, :cond_1

    .line 295
    :cond_0
    :goto_0
    return v1

    .line 283
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 285
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->originalRect:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    iget v3, v3, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->width:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    iget v4, v4, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->height:I

    int-to-float v4, v4

    invoke-virtual {v2, v6, v6, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 286
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaledRect:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->originalRect:Landroid/graphics/RectF;

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 289
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaledRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 291
    int-to-float v2, p1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaledRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaledRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    invoke-static {v2, v6, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->translateInterval(FFFFF)F

    move-result v0

    .line 292
    .local v0, "translateX":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v3, 0x3dcccccd    # 0.1f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 4

    .prologue
    .line 525
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->highlightColorList:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_0

    .line 526
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->highlightColorList:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getDrawableState()[I

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 530
    .local v0, "highlightColor":I
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->currentColorFilterColor:I

    if-eq v1, v0, :cond_0

    .line 533
    packed-switch v0, :pswitch_data_0

    .line 541
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->highlightColorIsStrict:Z

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorFilter(IZ)Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 543
    :goto_0
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->currentColorFilterColor:I

    .line 548
    .end local v0    # "highlightColor":I
    :cond_0
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundImageView;->drawableStateChanged()V

    .line 549
    return-void

    .line 538
    .restart local v0    # "highlightColor":I
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->clearColorFilter()V

    goto :goto_0

    .line 533
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public getDimensions()Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 130
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->fromBitmapDrawable(Landroid/widget/ImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    return-object v0
.end method

.method public getImageGravity()I
    .locals 1

    .prologue
    .line 464
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->imageGravity:I

    return v0
.end method

.method public getOriginalZoom()F
    .locals 2

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getDimensions()Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v1

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->width:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public getScaledDimensions()Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 136
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getDimensions()Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v0

    .line 137
    .local v0, "dimensions":Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 138
    .local v1, "matrix":Landroid/graphics/Matrix;
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 143
    .end local v0    # "dimensions":Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    :cond_0
    :goto_0
    return-object v0

    .line 141
    .restart local v0    # "dimensions":Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    :cond_1
    const/4 v3, 0x2

    new-array v2, v3, [F

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->width:I

    int-to-float v3, v3

    aput v3, v2, v4

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->height:I

    int-to-float v3, v3

    aput v3, v2, v5

    .line 142
    .local v2, "vecs":[F
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapVectors([F)V

    .line 143
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    .end local v0    # "dimensions":Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    aget v3, v2, v4

    aget v4, v2, v5

    invoke-direct {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/Dimensions;-><init>(FF)V

    goto :goto_0
.end method

.method public getScalingBias()I
    .locals 1

    .prologue
    .line 489
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scalingBias:I

    return v0
.end method

.method public getZoom()F
    .locals 3

    .prologue
    .line 273
    const/16 v1, 0x9

    new-array v0, v1, [F

    .line 274
    .local v0, "oldValues":[F
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 275
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 276
    const/4 v1, 0x0

    aget v1, v0, v1

    return v1
.end method

.method public isRead()Z
    .locals 1

    .prologue
    .line 553
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isRead:Z

    return v0
.end method

.method public isZoomable()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->zoomable:Z

    return v0
.end method

.method public markLoadStart()V
    .locals 2

    .prologue
    .line 397
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->loadStartTime:J

    .line 398
    return-void
.end method

.method public onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isRead()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 576
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Lcom/google/android/libraries/bind/widget/BoundImageView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 577
    .local v0, "drawableState":[I
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->STATE_READ:[I

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->mergeDrawableStates([I[I)[I

    .line 580
    .end local v0    # "drawableState":[I
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundImageView;->onCreateDrawableState(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method protected onImageBitmapDisplayed()V
    .locals 0

    .prologue
    .line 428
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 432
    invoke-super/range {p0 .. p5}, Lcom/google/android/libraries/bind/widget/BoundImageView;->onLayout(ZIIII)V

    .line 433
    if-eqz p1, :cond_0

    .line 434
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->applyGravity()V

    .line 436
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 357
    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scalingBias:I

    if-eqz v6, :cond_0

    .line 358
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 359
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 360
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 361
    .local v4, "intrinsicWidth":I
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 363
    .local v3, "intrinsicHeight":I
    if-lez v4, :cond_0

    if-lez v3, :cond_0

    .line 364
    int-to-float v6, v4

    int-to-float v7, v3

    div-float v0, v6, v7

    .line 369
    .local v0, "aspectRatio":F
    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scalingBias:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 370
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 371
    .local v5, "width":I
    int-to-float v6, v5

    div-float/2addr v6, v0

    float-to-int v2, v6

    .line 372
    .local v2, "height":I
    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 381
    .end local v0    # "aspectRatio":F
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v2    # "height":I
    .end local v3    # "intrinsicHeight":I
    .end local v4    # "intrinsicWidth":I
    .end local v5    # "width":I
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundImageView;->onMeasure(II)V

    .line 382
    return-void

    .line 373
    .restart local v0    # "aspectRatio":F
    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "intrinsicHeight":I
    .restart local v4    # "intrinsicWidth":I
    :cond_1
    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scalingBias:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 374
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 375
    .restart local v2    # "height":I
    int-to-float v6, v2

    mul-float/2addr v6, v0

    float-to-int v5, v6

    .line 376
    .restart local v5    # "width":I
    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0
.end method

.method protected onScaleBegin()V
    .locals 0

    .prologue
    .line 301
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 501
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaleDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v1, :cond_1

    .line 502
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 503
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 518
    :cond_0
    :goto_0
    return v0

    .line 507
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scrollDetector:Landroid/view/GestureDetector;

    if-eqz v1, :cond_2

    .line 508
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scrollDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 509
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 514
    :goto_1
    :pswitch_0
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isScrolling:Z

    if-nez v1, :cond_0

    .line 518
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 512
    :pswitch_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isScrolling:Z

    goto :goto_1

    .line 509
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 458
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->ignoreRequestLayout:Z

    if-nez v0, :cond_0

    .line 459
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundImageView;->requestLayout()V

    .line 461
    :cond_0
    return-void
.end method

.method public resetZoom()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 261
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getOriginalZoom()F

    move-result v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getZoom()F

    move-result v2

    div-float v0, v1, v2

    .line 262
    .local v0, "newScale":F
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 263
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 264
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    const/4 v2, 0x0

    invoke-direct {p0, v3, v3, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->translateImage(FFLcom/google/apps/dots/android/newsstand/util/Dimensions;[F)V

    .line 265
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->invalidate()V

    .line 266
    return-void
.end method

.method public setFadeIn(Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;)V
    .locals 0
    .param p1, "fadeInType"    # Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    .prologue
    .line 385
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->fadeInType:Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    .line 386
    return-void
.end method

.method public setFadeInDuration(I)V
    .locals 0
    .param p1, "millis"    # I

    .prologue
    .line 389
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->fadeInMs:I

    .line 390
    return-void
.end method

.method public setFadeInSlowLoadThreshold(I)V
    .locals 0
    .param p1, "millis"    # I

    .prologue
    .line 393
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->fadeInSlowLoadThresholdMs:I

    .line 394
    return-void
.end method

.method public setHighlightColorIsStrict(Z)V
    .locals 0
    .param p1, "strict"    # Z

    .prologue
    .line 570
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->highlightColorIsStrict:Z

    .line 571
    return-void
.end method

.method public setHighlightColorListRefId(I)V
    .locals 1
    .param p1, "highlightColorListRefId"    # I

    .prologue
    .line 565
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 566
    :goto_0
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->highlightColorList:Landroid/content/res/ColorStateList;

    .line 567
    return-void

    .line 566
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 402
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 404
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->fromBitmapDrawable(Landroid/widget/ImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    .line 405
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->applyGravity()V

    .line 407
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->fadeInType:Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;->NONE:Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    if-eq v2, v3, :cond_0

    if-eqz p1, :cond_0

    .line 408
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->fadeInType:Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;->IF_SLOW_LOAD:Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    if-ne v2, v3, :cond_1

    .line 409
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->loadStartTime:J

    sub-long v0, v2, v4

    .line 411
    .local v0, "timeSinceLoad":J
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->fadeInSlowLoadThresholdMs:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 412
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->onImageBitmapDisplayed()V

    .line 423
    .end local v0    # "timeSinceLoad":J
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->fadeInMs:I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$3;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)V

    invoke-static {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fadeIn(Landroid/view/View;ILjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 440
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->forceLayoutOnSetDrawable:Z

    if-nez v0, :cond_0

    .line 441
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->ignoreRequestLayout:Z

    .line 443
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 444
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->ignoreRequestLayout:Z

    .line 445
    return-void
.end method

.method public setImageGravity(I)V
    .locals 1
    .param p1, "gravity"    # I

    .prologue
    .line 468
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->imageGravity:I

    .line 469
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 470
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->applyGravity()V

    .line 471
    return-void
.end method

.method public setImageResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 449
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->forceLayoutOnSetDrawable:Z

    if-nez v0, :cond_0

    .line 450
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->ignoreRequestLayout:Z

    .line 452
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundImageView;->setImageResource(I)V

    .line 453
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->ignoreRequestLayout:Z

    .line 454
    return-void
.end method

.method public setIsRead(Z)V
    .locals 1
    .param p1, "isRead"    # Z

    .prologue
    .line 558
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isRead:Z

    if-eq v0, p1, :cond_0

    .line 559
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isRead:Z

    .line 560
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->refreshDrawableState()V

    .line 562
    :cond_0
    return-void
.end method

.method public setScalingBias(I)V
    .locals 1
    .param p1, "scalingBias"    # I

    .prologue
    .line 493
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scalingBias:I

    if-eq v0, p1, :cond_0

    .line 494
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scalingBias:I

    .line 495
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->requestLayout()V

    .line 497
    :cond_0
    return-void
.end method

.method public setZoomable(Z)V
    .locals 3
    .param p1, "zoomable"    # Z

    .prologue
    const/4 v0, 0x0

    .line 156
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->zoomable:Z

    .line 157
    if-eqz p1, :cond_2

    .line 158
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaleDetector:Landroid/view/ScaleGestureDetector;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaleDetector:Landroid/view/ScaleGestureDetector;

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scrollDetector:Landroid/view/GestureDetector;

    if-nez v0, :cond_1

    .line 203
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scrollDetector:Landroid/view/GestureDetector;

    .line 236
    :cond_1
    :goto_0
    return-void

    .line 233
    :cond_2
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scaleDetector:Landroid/view/ScaleGestureDetector;

    .line 234
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->scrollDetector:Landroid/view/GestureDetector;

    goto :goto_0
.end method

.method public zoomBy(FFF)V
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "scale"    # F

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getZoom()F

    move-result v2

    .line 247
    .local v2, "oldScale":F
    mul-float v3, p3, v2

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->minScale:F

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    const/high16 v4, 0x40c00000    # 6.0f

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 248
    .local v0, "newScale":F
    div-float v1, v0, v2

    .line 250
    .local v1, "newScaleFactor":F
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    neg-float v4, p1

    neg-float v5, p2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 251
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    invoke-virtual {v3, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 252
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 253
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->translateImage(FFLcom/google/apps/dots/android/newsstand/util/Dimensions;[F)V

    .line 254
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->invalidate()V

    .line 255
    return-void
.end method
