.class public Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;
.super Ljava/lang/Object;
.source "NSCookieStore.java"

# interfaces
.implements Ljava/net/CookieStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$CheckAdIdReceiver;
    }
.end annotation


# static fields
.field private static final AD_ID_KEY:Ljava/lang/String; = "ad_id"

.field private static final COOKIE_PREFIX:Ljava/lang/String; = "cookie:"

.field private static final FLUSH_INTERVAL_MS:J = 0x7530L

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final PREFS_NAME_PREFIX:Ljava/lang/String; = "NSCookieStore"

.field private static final URI_PREFIX:Ljava/lang/String; = "uri:"


# instance fields
.field private final delegate:Ljava/net/CookieStore;

.field private dirty:Z

.field private isDisposed:Z

.field private final prefs:Landroid/content/SharedPreferences;

.field private final prefsName:Ljava/lang/String;

.field private final token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 4
    .param p1, "appContext"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/net/CookieManager;

    invoke-direct {v0}, Ljava/net/CookieManager;-><init>()V

    invoke-virtual {v0}, Ljava/net/CookieManager;->getCookieStore()Ljava/net/CookieStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    .line 62
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 64
    const-string v0, "NSCookieStore_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->accountHash(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefsName:Ljava/lang/String;

    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefsName:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefs:Landroid/content/SharedPreferences;

    .line 67
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$1;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$1;-><init>(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 74
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$1;->execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 76
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$CheckAdIdReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$CheckAdIdReceiver;-><init>(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$1;)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.apps.dots.android.newsstand.NSApplication.action.APPLICATION_VISIBLE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 78
    return-void

    .line 64
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->load()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->requestNextFlush()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->checkAdId(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->save()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->isDisposed:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->requestFlush(Z)V

    return-void
.end method

.method static synthetic access$700()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private checkAdId(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 249
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->CACHE_WARMUP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$4;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$4;-><init>(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->execute(Ljava/lang/Runnable;)V

    .line 278
    return-void
.end method

.method private load()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 177
    sget-object v6, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Loading cookies"

    new-array v8, v12, [Ljava/lang/Object;

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 179
    invoke-static {}, Lcom/google/common/collect/ArrayListMultimap;->create()Lcom/google/common/collect/ArrayListMultimap;

    move-result-object v0

    .line 180
    .local v0, "allCookies":Lcom/google/common/collect/ArrayListMultimap;, "Lcom/google/common/collect/ArrayListMultimap<Ljava/net/URI;Ljava/net/HttpCookie;>;"
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 181
    .local v4, "key":Ljava/lang/String;
    const-string v6, "uri:"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 182
    const-string v6, "uri:"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v5

    .line 183
    .local v5, "uri":Ljava/net/URI;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "Loading cookies for host %s"

    new-array v9, v13, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-virtual {v6, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    const-string v6, ","

    invoke-static {v6}, Lcom/google/common/base/Splitter;->on(Ljava/lang/String;)Lcom/google/common/base/Splitter;

    move-result-object v6

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefs:Landroid/content/SharedPreferences;

    const-string v9, ""

    invoke-interface {v8, v4, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 185
    .local v2, "cookieIdStr":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefs:Landroid/content/SharedPreferences;

    const-string v6, "cookie:"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v11

    if-eqz v11, :cond_2

    invoke-virtual {v10, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    :goto_0
    const-string v10, ""

    invoke-interface {v9, v6, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 186
    .local v3, "cookieStr":Ljava/lang/String;
    invoke-static {v3}, Ljava/net/HttpCookie;->parse(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/HttpCookie;

    .line 187
    .local v1, "cookie":Ljava/net/HttpCookie;
    sget-object v9, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v10, "Loaded cookie %s"

    new-array v11, v13, [Ljava/lang/Object;

    aput-object v3, v11, v12

    invoke-virtual {v9, v10, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    invoke-virtual {v0, v5, v1}, Lcom/google/common/collect/ArrayListMultimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 185
    .end local v1    # "cookie":Ljava/net/HttpCookie;
    .end local v3    # "cookieStr":Ljava/lang/String;
    :cond_2
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 194
    .end local v2    # "cookieIdStr":Ljava/lang/String;
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "uri":Ljava/net/URI;
    :cond_3
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    monitor-enter v7

    .line 195
    :try_start_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    invoke-interface {v6}, Ljava/net/CookieStore;->removeAll()Z

    .line 196
    invoke-virtual {v0}, Lcom/google/common/collect/ArrayListMultimap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/URI;

    .line 197
    .restart local v5    # "uri":Ljava/net/URI;
    invoke-virtual {v0, v5}, Lcom/google/common/collect/ArrayListMultimap;->get(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/HttpCookie;

    .line 198
    .restart local v1    # "cookie":Ljava/net/HttpCookie;
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    invoke-interface {v9, v5, v1}, Ljava/net/CookieStore;->add(Ljava/net/URI;Ljava/net/HttpCookie;)V

    goto :goto_2

    .line 202
    .end local v1    # "cookie":Ljava/net/HttpCookie;
    .end local v5    # "uri":Ljava/net/URI;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 201
    :cond_5
    const/4 v6, 0x0

    :try_start_1
    iput-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->dirty:Z

    .line 202
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 203
    return-void
.end method

.method private requestFlush(Z)V
    .locals 3
    .param p1, "requestNextFlush"    # Z

    .prologue
    .line 153
    sget-object v0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "requesting flush"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$2;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$2;-><init>(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;Z)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->execute(Ljava/lang/Runnable;)V

    .line 163
    return-void
.end method

.method private requestNextFlush()V
    .locals 4

    .prologue
    .line 166
    sget-object v0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "requestNextFlush"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 167
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$3;-><init>(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;)V

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 173
    return-void
.end method

.method private save()V
    .locals 14

    .prologue
    .line 215
    sget-object v8, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v9, "Saving cookies to prefs %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefsName:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 217
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    monitor-enter v9

    .line 218
    :try_start_0
    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->dirty:Z

    if-nez v8, :cond_0

    .line 219
    sget-object v8, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v10, "Not saving"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v8, v10, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    monitor-exit v9

    .line 246
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-static {}, Lcom/google/common/collect/ArrayListMultimap;->create()Lcom/google/common/collect/ArrayListMultimap;

    move-result-object v0

    .line 223
    .local v0, "allCookies":Lcom/google/common/collect/ArrayListMultimap;, "Lcom/google/common/collect/ArrayListMultimap<Ljava/net/URI;Ljava/net/HttpCookie;>;"
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    invoke-interface {v8}, Ljava/net/CookieStore;->getURIs()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/net/URI;

    .line 224
    .local v7, "uri":Ljava/net/URI;
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    invoke-interface {v10, v7}, Ljava/net/CookieStore;->get(Ljava/net/URI;)Ljava/util/List;

    move-result-object v10

    invoke-virtual {v0, v7, v10}, Lcom/google/common/collect/ArrayListMultimap;->putAll(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 227
    .end local v0    # "allCookies":Lcom/google/common/collect/ArrayListMultimap;, "Lcom/google/common/collect/ArrayListMultimap<Ljava/net/URI;Ljava/net/HttpCookie;>;"
    .end local v7    # "uri":Ljava/net/URI;
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    .line 226
    .restart local v0    # "allCookies":Lcom/google/common/collect/ArrayListMultimap;, "Lcom/google/common/collect/ArrayListMultimap<Ljava/net/URI;Ljava/net/HttpCookie;>;"
    :cond_1
    const/4 v8, 0x0

    :try_start_1
    iput-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->dirty:Z

    .line 227
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 230
    .local v6, "prefsEditor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 231
    const/4 v4, 0x0

    .line 232
    .local v4, "nextCookieId":I
    invoke-virtual {v0}, Lcom/google/common/collect/ArrayListMultimap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/net/URI;

    .line 233
    .restart local v7    # "uri":Ljava/net/URI;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 234
    .local v3, "cookieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v0, v7}, Lcom/google/common/collect/ArrayListMultimap;->get(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/HttpCookie;

    .line 235
    .local v1, "cookie":Ljava/net/HttpCookie;
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "nextCookieId":I
    .local v5, "nextCookieId":I
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 236
    .local v2, "cookieIdStr":Ljava/lang/String;
    const-string v8, "cookie:"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_2

    invoke-virtual {v11, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    :goto_4
    invoke-virtual {v1}, Ljava/net/HttpCookie;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v6, v8, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 237
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    sget-object v8, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v11, "wrote cookie id %s: %s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    const/4 v13, 0x1

    aput-object v1, v12, v13

    invoke-virtual {v8, v11, v12}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v4, v5

    .line 239
    .end local v5    # "nextCookieId":I
    .restart local v4    # "nextCookieId":I
    goto :goto_3

    .line 236
    .end local v4    # "nextCookieId":I
    .restart local v5    # "nextCookieId":I
    :cond_2
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 240
    .end local v1    # "cookie":Ljava/net/HttpCookie;
    .end local v2    # "cookieIdStr":Ljava/lang/String;
    .end local v5    # "nextCookieId":I
    .restart local v4    # "nextCookieId":I
    :cond_3
    const-string v8, "uri:"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    if-eqz v11, :cond_4

    invoke-virtual {v10, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    :goto_5
    const-string v10, ","

    invoke-static {v10}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v10

    invoke-virtual {v10, v3}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v8, v10}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 241
    sget-object v8, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v10, "wrote %d cookies for host %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-virtual {v7}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v8, v10, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 240
    :cond_4
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 244
    .end local v3    # "cookieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "uri":Ljava/net/URI;
    :cond_5
    const-string v8, "ad_id"

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefs:Landroid/content/SharedPreferences;

    const-string v10, "ad_id"

    const-string v11, ""

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v8, v9}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 245
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method


# virtual methods
.method public add(Ljava/net/URI;Ljava/net/HttpCookie;)V
    .locals 6
    .param p1, "uri"    # Ljava/net/URI;
    .param p2, "cookie"    # Ljava/net/HttpCookie;

    .prologue
    const/4 v5, 0x1

    .line 88
    sget-object v0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Set cookie for host %s: [%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    monitor-enter v1

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    invoke-interface {v0, p1, p2}, Ljava/net/CookieStore;->add(Ljava/net/URI;Ljava/net/HttpCookie;)V

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->dirty:Z

    .line 92
    monitor-exit v1

    .line 93
    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public dispose()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 81
    sget-object v0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "dispose()"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->requestFlush(Z)V

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->isDisposed:Z

    .line 84
    return-void
.end method

.method public get(Ljava/net/URI;)Ljava/util/List;
    .locals 8
    .param p1, "uri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/net/HttpCookie;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 98
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    monitor-enter v3

    .line 99
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    invoke-interface {v2, p1}, Ljava/net/CookieStore;->get(Ljava/net/URI;)Ljava/util/List;

    move-result-object v1

    .line 100
    .local v1, "cookies":Ljava/util/List;, "Ljava/util/List<Ljava/net/HttpCookie;>;"
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    sget-object v2, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 102
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 103
    sget-object v2, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Returning %d cookies for host %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/HttpCookie;

    .line 105
    .local v0, "cookie":Ljava/net/HttpCookie;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "\t%s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 100
    .end local v0    # "cookie":Ljava/net/HttpCookie;
    .end local v1    # "cookies":Ljava/util/List;, "Ljava/util/List<Ljava/net/HttpCookie;>;"
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 108
    .restart local v1    # "cookies":Ljava/util/List;, "Ljava/util/List<Ljava/net/HttpCookie;>;"
    :cond_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "No cookies found for host %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    :cond_1
    return-object v1
.end method

.method public getCookies()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/net/HttpCookie;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    sget-object v0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Getting all cookies"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    monitor-enter v1

    .line 118
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    invoke-interface {v0}, Ljava/net/CookieStore;->getCookies()Ljava/util/List;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getURIs()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Getting all uris"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    monitor-enter v1

    .line 126
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    invoke-interface {v0}, Ljava/net/CookieStore;->getURIs()Ljava/util/List;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public remove(Ljava/net/URI;Ljava/net/HttpCookie;)Z
    .locals 6
    .param p1, "uri"    # Ljava/net/URI;
    .param p2, "cookie"    # Ljava/net/HttpCookie;

    .prologue
    .line 132
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Removing cookie for host %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    monitor-enter v2

    .line 135
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    invoke-interface {v1, p1, p2}, Ljava/net/CookieStore;->remove(Ljava/net/URI;Ljava/net/HttpCookie;)Z

    move-result v0

    .line 136
    .local v0, "result":Z
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->dirty:Z

    or-int/2addr v1, v0

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->dirty:Z

    .line 137
    monitor-exit v2

    .line 138
    return v0

    .line 137
    .end local v0    # "result":Z
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeAll()Z
    .locals 4

    .prologue
    .line 143
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Removing all cookies"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    monitor-enter v2

    .line 146
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->delegate:Ljava/net/CookieStore;

    invoke-interface {v1}, Ljava/net/CookieStore;->removeAll()Z

    move-result v0

    .line 147
    .local v0, "result":Z
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->dirty:Z

    or-int/2addr v1, v0

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->dirty:Z

    .line 148
    monitor-exit v2

    .line 149
    return v0

    .line 148
    .end local v0    # "result":Z
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
