.class public abstract Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;
.super Lcom/google/android/libraries/bind/data/RefreshTask;
.source "AsyncTokenRefreshTask.java"


# instance fields
.field public final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/data/RefreshTask;-><init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V

    .line 19
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 21
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->register(Lcom/google/android/libraries/bind/async/Cancellable;)Z

    .line 22
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/RefreshTask;->cancel()V

    .line 28
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroy()V

    .line 29
    return-void
.end method
