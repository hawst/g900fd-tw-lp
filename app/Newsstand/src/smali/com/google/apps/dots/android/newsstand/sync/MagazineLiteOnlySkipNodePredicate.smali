.class public Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;
.super Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;
.source "MagazineLiteOnlySkipNodePredicate.java"


# instance fields
.field private allPostIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;-><init>()V

    .line 19
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;->allPostIds:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;->allPostIds:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public apply(Lcom/google/protobuf/nano/MessageNano;)Z
    .locals 11
    .param p1, "input"    # Lcom/google/protobuf/nano/MessageNano;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 34
    instance-of v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-eqz v6, :cond_5

    move-object v2, p1

    .line 35
    check-cast v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 38
    .local v2, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    const/4 v1, 0x0

    .line 39
    .local v1, "leftHasCorrespondingText":Z
    const/4 v3, 0x0

    .line 40
    .local v3, "rightHasCorrespondingText":Z
    iget-object v7, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v8, v7

    move v6, v5

    :goto_0
    if-ge v6, v8, :cond_2

    aget-object v0, v7, v6

    .line 41
    .local v0, "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getFormat()I

    move-result v9

    if-ne v9, v4, :cond_0

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;->allPostIds:Ljava/util/Set;

    .line 42
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getObjectId()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 43
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getIndex()I

    move-result v9

    if-nez v9, :cond_1

    .line 44
    const/4 v1, 0x1

    .line 40
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 45
    :cond_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getIndex()I

    move-result v9

    if-ne v9, v4, :cond_0

    .line 46
    const/4 v3, 0x1

    goto :goto_1

    .line 52
    .end local v0    # "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    :cond_2
    if-eqz v1, :cond_4

    if-eqz v3, :cond_4

    .line 62
    .end local v1    # "leftHasCorrespondingText":Z
    .end local v2    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .end local v3    # "rightHasCorrespondingText":Z
    :cond_3
    :goto_2
    return v4

    .line 57
    .restart local v1    # "leftHasCorrespondingText":Z
    .restart local v2    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .restart local v3    # "rightHasCorrespondingText":Z
    :cond_4
    iget-object v6, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v6, v6

    if-ne v6, v4, :cond_5

    if-nez v1, :cond_3

    if-nez v3, :cond_3

    .end local v1    # "leftHasCorrespondingText":Z
    .end local v2    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .end local v3    # "rightHasCorrespondingText":Z
    :cond_5
    move v4, v5

    .line 62
    goto :goto_2
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lcom/google/protobuf/nano/MessageNano;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;->apply(Lcom/google/protobuf/nano/MessageNano;)Z

    move-result v0

    return v0
.end method

.method public onPreTraverseCollection(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V
    .locals 2
    .param p1, "collection"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .prologue
    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate$1;-><init>(Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;)V

    .line 29
    .local v0, "visitor":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;>;"
    new-instance v1, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    invoke-direct {v1, p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 30
    return-void
.end method
