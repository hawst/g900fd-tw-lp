.class public Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;
.super Landroid/app/Service;
.source "MagazinesUserContentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;,
        Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static fetching:Z

.field private static finalMagazinesClearer:Lcom/google/android/libraries/bind/async/DelayedRunnable;

.field private static finalMagazinesData:Lcom/google/common/collect/TreeMultimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/TreeMultimap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static finalMagazinesHash:I

.field private static lock:Ljava/lang/Object;

.field private static myMagazinesEventCallback:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

.field private static myOffersEventCallback:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

.field private static pinnedList:Lcom/google/android/libraries/bind/data/DataList;

.field private static pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private static recentlyReadList:Lcom/google/android/libraries/bind/data/DataList;

.field private static recentlyReadListObserver:Lcom/google/android/libraries/bind/data/DataObserver;


# instance fields
.field private mBinder:Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    const-class v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 77
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->lock:Ljava/lang/Object;

    .line 93
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetching:Z

    .line 109
    new-instance v0, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 110
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$1;

    invoke-direct {v2}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$1;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesClearer:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 109
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 156
    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 72
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->clearFinalMagazinesData(Z)V

    return-void
.end method

.method static synthetic access$1000(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->processMyMagazinesResponse(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    return-void
.end method

.method static synthetic access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$300()Lcom/google/android/libraries/bind/async/DelayedRunnable;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesClearer:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    return-object v0
.end method

.method static synthetic access$400()Lcom/google/common/collect/TreeMultimap;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesData:Lcom/google/common/collect/TreeMultimap;

    return-object v0
.end method

.method static synthetic access$500(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetchMagazinesForWidget(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$600(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetchMyOffersInternal(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V

    return-void
.end method

.method static synthetic access$702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 72
    sput-boolean p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetching:Z

    return p0
.end method

.method static synthetic access$800(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->processMyOffersResponse(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    return-void
.end method

.method static synthetic access$900(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetchMyMagazinesInternal(Landroid/content/Context;)V

    return-void
.end method

.method private static clearFinalMagazinesData(Z)V
    .locals 1
    .param p0, "clearHashToo"    # Z

    .prologue
    .line 639
    const/4 v0, 0x0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesData:Lcom/google/common/collect/TreeMultimap;

    .line 640
    if-eqz p0, :cond_0

    .line 641
    const/4 v0, 0x0

    sput v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesHash:I

    .line 643
    :cond_0
    return-void
.end method

.method private static computeFinalDataFromAppSummaries(Ljava/util/List;)Lcom/google/common/collect/TreeMultimap;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;",
            ">;)",
            "Lcom/google/common/collect/TreeMultimap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 531
    .local p0, "myMagazinesAppSummaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v8

    .line 532
    .local v8, "updateTimestamps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v9

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinned(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v6

    .line 533
    .local v6, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v10, v6, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v11, v10

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v11, :cond_1

    aget-object v4, v10, v9

    .line 534
    .local v4, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->hasLastSynced()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 535
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v3

    .line 536
    .local v3, "edition":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getType()I

    move-result v12

    const/4 v13, 0x5

    if-ne v12, v13, :cond_0

    .line 537
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getMagazine()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;->getAppId()Ljava/lang/String;

    move-result-object v0

    .line 538
    .local v0, "appId":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getLastSynced()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-interface {v8, v0, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    .end local v0    # "appId":Ljava/lang/String;
    .end local v3    # "edition":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 543
    .end local v4    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->recentlyReadHelper()Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    move-result-object v9

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->loadRecentlyRead(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;

    move-result-object v7

    .line 544
    .local v7, "recentlyRead":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;
    iget-object v11, v7, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;->item:[Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    array-length v12, v11

    const/4 v9, 0x0

    move v10, v9

    :goto_1
    if-ge v10, v12, :cond_4

    aget-object v4, v11, v10

    .line 545
    .local v4, "item":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->hasLastRead()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 546
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v3

    .line 547
    .restart local v3    # "edition":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getType()I

    move-result v9

    const/4 v13, 0x5

    if-ne v9, v13, :cond_3

    .line 548
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getMagazine()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;->getAppId()Ljava/lang/String;

    move-result-object v0

    .line 549
    .restart local v0    # "appId":Ljava/lang/String;
    invoke-interface {v8, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 550
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->getLastRead()J

    move-result-wide v14

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    cmp-long v9, v14, v16

    if-lez v9, :cond_3

    .line 551
    :cond_2
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->getLastRead()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v0, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    .end local v0    # "appId":Ljava/lang/String;
    .end local v3    # "edition":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    :cond_3
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto :goto_1

    .line 557
    .end local v4    # "item":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    :cond_4
    new-instance v9, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$11;

    invoke-direct {v9}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$11;-><init>()V

    .line 564
    invoke-static {}, Lcom/google/common/collect/Ordering;->arbitrary()Lcom/google/common/collect/Ordering;

    move-result-object v10

    .line 558
    invoke-static {v9, v10}, Lcom/google/common/collect/TreeMultimap;->create(Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/google/common/collect/TreeMultimap;

    move-result-object v5

    .line 566
    .local v5, "magazineWidgetInfoByRecentness":Lcom/google/common/collect/TreeMultimap;, "Lcom/google/common/collect/TreeMultimap<Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;>;"
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 567
    .local v1, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    iget-object v0, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 568
    .restart local v0    # "appId":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v2

    .line 570
    .local v2, "coverAttachmentId":Ljava/lang/String;
    invoke-static {v1, v8}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->getTimestampForAppSummary(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Ljava/util/Map;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v11, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;

    invoke-direct {v11, v0, v2}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v10, v11}, Lcom/google/common/collect/TreeMultimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_2

    .line 574
    .end local v0    # "appId":Ljava/lang/String;
    .end local v1    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .end local v2    # "coverAttachmentId":Ljava/lang/String;
    :cond_5
    return-object v5
.end method

.method private static computeFinalDataFromOfferSummaries(Ljava/util/List;)Lcom/google/common/collect/TreeMultimap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;",
            ">;)",
            "Lcom/google/common/collect/TreeMultimap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 588
    .local p0, "offerSummaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;>;"
    new-instance v6, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$12;

    invoke-direct {v6}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$12;-><init>()V

    .line 595
    invoke-static {}, Lcom/google/common/collect/Ordering;->arbitrary()Lcom/google/common/collect/Ordering;

    move-result-object v7

    .line 589
    invoke-static {v6, v7}, Lcom/google/common/collect/TreeMultimap;->create(Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/google/common/collect/TreeMultimap;

    move-result-object v3

    .line 597
    .local v3, "magazineWidgetInfoByRecentness":Lcom/google/common/collect/TreeMultimap;, "Lcom/google/common/collect/TreeMultimap<Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 598
    .local v5, "offerSummary":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v1

    .line 599
    .local v1, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    iget-object v0, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 600
    .local v0, "appId":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v2

    .line 601
    .local v2, "coverAttachmentId":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v4

    .line 603
    .local v4, "offerId":Ljava/lang/String;
    const/4 v7, 0x0

    invoke-static {v1, v7}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->getTimestampForAppSummary(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    new-instance v8, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;

    invoke-direct {v8, v0, v2, v4}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v7, v8}, Lcom/google/common/collect/TreeMultimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 607
    .end local v0    # "appId":Ljava/lang/String;
    .end local v1    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .end local v2    # "coverAttachmentId":Ljava/lang/String;
    .end local v4    # "offerId":Ljava/lang/String;
    .end local v5    # "offerSummary":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    :cond_0
    return-object v3
.end method

.method private static fetchMagazinesForWidget(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 346
    sget-object v4, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->lock:Ljava/lang/Object;

    monitor-enter v4

    .line 349
    :try_start_0
    sget-boolean v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetching:Z

    if-nez v3, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    if-nez v3, :cond_1

    .line 350
    :cond_0
    monitor-exit v4

    .line 378
    :goto_0
    return-void

    .line 352
    :cond_1
    const/4 v3, 0x1

    sput-boolean v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetching:Z

    .line 354
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v2

    .line 355
    .local v2, "configUtil":Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v0

    .line 356
    .local v0, "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    if-eqz v0, :cond_2

    .line 357
    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetchMyOffersInternal(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V

    .line 377
    :goto_1
    monitor-exit v4

    goto :goto_0

    .end local v0    # "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .end local v2    # "configUtil":Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 363
    .restart local v0    # "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .restart local v2    # "configUtil":Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v5}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getFreshConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 364
    .local v1, "configFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    new-instance v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$6;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$6;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private static fetchMyMagazinesInternal(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 429
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 430
    :try_start_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 431
    .local v0, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v2

    .line 432
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    iget-object v5, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    .line 431
    invoke-virtual {v2, v0, v4}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getAny(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 433
    .local v1, "mutationResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    new-instance v2, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$9;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$9;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 444
    monitor-exit v3

    .line 445
    return-void

    .line 444
    .end local v0    # "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .end local v1    # "mutationResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static fetchMyOffersInternal(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "config"    # Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .prologue
    .line 381
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 382
    :try_start_0
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getEnableMagazines()Z

    move-result v2

    if-nez v2, :cond_0

    .line 383
    sget-object v2, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Magazines are not enabled for this country, so no offers or magazines"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 384
    const/4 v2, 0x0

    sput-boolean v2, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetching:Z

    .line 385
    monitor-exit v3

    .line 402
    :goto_0
    return-void

    .line 387
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 388
    .local v0, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v2

    .line 389
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    iget-object v5, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyOffers(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    .line 388
    invoke-virtual {v2, v0, v4}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getAny(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 390
    .local v1, "mutationResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    new-instance v2, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$7;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$7;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 401
    monitor-exit v3

    goto :goto_0

    .end local v0    # "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .end local v1    # "mutationResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static getTimestampForAppSummary(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Ljava/util/Map;)J
    .locals 8
    .param p0, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .local p1, "updateTimestamps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-wide/16 v6, 0x0

    .line 614
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getPublicationDate()J

    move-result-wide v2

    .line 615
    .local v2, "updateTimestamp":J
    cmp-long v1, v2, v6

    if-eqz v1, :cond_0

    .line 616
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getUpdateTime()J

    move-result-wide v4

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getUpdateTime()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-gez v1, :cond_1

    .line 617
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getUpdateTime()J

    move-result-wide v2

    .line 619
    :cond_1
    if-eqz p1, :cond_2

    .line 620
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 621
    .local v0, "overrideUpdateTimestamp":Ljava/lang/Long;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-lez v1, :cond_2

    .line 622
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 626
    .end local v0    # "overrideUpdateTimestamp":Ljava/lang/Long;
    :cond_2
    return-wide v2
.end method

.method public static declared-synchronized initMyMagazinesObserver(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 9
    .param p0, "contextArgument"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 270
    const-class v4, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;

    monitor-enter v4

    :try_start_0
    sget-object v5, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->lock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 274
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 277
    .local v0, "context":Landroid/content/Context;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 278
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    .line 277
    invoke-static {v3, v6}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 279
    .local v1, "myMagazinesUri":Landroid/net/Uri;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->myMagazinesEventCallback:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    if-nez v3, :cond_0

    .line 280
    new-instance v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$2;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadExecutor()Lcom/google/common/util/concurrent/ListeningExecutorService;

    move-result-object v6

    invoke-direct {v3, v6, v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$2;-><init>(Ljava/util/concurrent/Executor;Landroid/content/Context;)V

    sput-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->myMagazinesEventCallback:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    .line 286
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v3

    sget-object v6, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->myMagazinesEventCallback:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v3, v1, v6}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->registerObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 293
    :cond_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 294
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyOffers(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    .line 293
    invoke-static {v3, v6}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 295
    .local v2, "myOffersUri":Landroid/net/Uri;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->myOffersEventCallback:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    if-nez v3, :cond_1

    .line 296
    new-instance v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$3;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadExecutor()Lcom/google/common/util/concurrent/ListeningExecutorService;

    move-result-object v6

    invoke-direct {v3, v6, v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$3;-><init>(Ljava/util/concurrent/Executor;Landroid/content/Context;)V

    sput-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->myOffersEventCallback:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    .line 302
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v3

    sget-object v6, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->myOffersEventCallback:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v3, v2, v6}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->registerObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 307
    :cond_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->pinnedList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v3, :cond_2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    if-eqz v3, :cond_2

    .line 308
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->pinnedList:Lcom/google/android/libraries/bind/data/DataList;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v3, v6}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 311
    :cond_2
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->pinnedList()Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    move-result-object v3

    const/4 v6, 0x2

    new-array v6, v6, [I

    const/4 v7, 0x0

    sget v8, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_LAST_SYNCED:I

    aput v8, v6, v7

    const/4 v7, 0x1

    sget v8, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_EDITION:I

    aput v8, v6, v7

    invoke-virtual {v3, v6}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->filter([I)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    sput-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->pinnedList:Lcom/google/android/libraries/bind/data/DataList;

    .line 313
    new-instance v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$4;

    invoke-direct {v3, v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$4;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 319
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->pinnedList:Lcom/google/android/libraries/bind/data/DataList;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v3, v6}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 325
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->recentlyReadList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v3, :cond_3

    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->recentlyReadListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    if-eqz v3, :cond_3

    .line 326
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->recentlyReadList:Lcom/google/android/libraries/bind/data/DataList;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->recentlyReadListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v3, v6}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 328
    :cond_3
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->recentlyReadList()Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    move-result-object v3

    sput-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->recentlyReadList:Lcom/google/android/libraries/bind/data/DataList;

    .line 329
    new-instance v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$5;

    invoke-direct {v3, v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$5;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->recentlyReadListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 335
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->recentlyReadList:Lcom/google/android/libraries/bind/data/DataList;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->recentlyReadListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v3, v6}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 338
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 339
    monitor-exit v4

    return-void

    .line 338
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "myMagazinesUri":Landroid/net/Uri;
    .end local v2    # "myOffersUri":Landroid/net/Uri;
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 270
    :catchall_1
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method private static notifyFinsky(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 630
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.play.CONTENT_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 631
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "Play.DataType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 633
    const-string v1, "Play.BackendId"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 635
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 636
    return-void
.end method

.method private static processMyMagazinesResponse(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mutationResponse"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 449
    sget-object v5, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->lock:Ljava/lang/Object;

    monitor-enter v5

    .line 451
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 452
    .local v3, "myMagazinesAppSummaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;>;"
    invoke-static {}, Lcom/google/android/play/utils/collections/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    .line 453
    .local v1, "archivedAppIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v4, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    iget-object v6, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {v4, v6}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v6, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$10;

    invoke-direct {v6, v3, v1}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$10;-><init>(Ljava/util/List;Ljava/util/Set;)V

    invoke-virtual {v4, v6}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 469
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 470
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 471
    .local v0, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 472
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 477
    .end local v0    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .end local v1    # "archivedAppIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;>;"
    .end local v3    # "myMagazinesAppSummaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 476
    .restart local v1    # "archivedAppIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;>;"
    .restart local v3    # "myMagazinesAppSummaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;>;"
    :cond_1
    const/4 v4, 0x0

    :try_start_1
    invoke-static {p0, v3, v4}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->updateMagazinesData(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    .line 477
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478
    return-void
.end method

.method private static processMyOffersResponse(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mutationResponse"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 406
    sget-object v2, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 408
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 409
    .local v0, "myOfferSummaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;>;"
    new-instance v1, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {v1, v3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v3, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$8;

    invoke-direct {v3, v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$8;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 420
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 421
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetchMyMagazinesInternal(Landroid/content/Context;)V

    .line 425
    :goto_0
    monitor-exit v2

    .line 426
    return-void

    .line 423
    :cond_0
    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->updateMagazinesData(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    goto :goto_0

    .line 425
    .end local v0    # "myOfferSummaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static updateMagazinesData(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "magazineAppSummaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;>;"
    .local p2, "offerSummaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;>;"
    const/4 v5, 0x0

    .line 484
    sget-object v1, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesClearer:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->cancel()V

    .line 485
    if-eqz p1, :cond_0

    .line 486
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->computeFinalDataFromAppSummaries(Ljava/util/List;)Lcom/google/common/collect/TreeMultimap;

    move-result-object v1

    sput-object v1, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesData:Lcom/google/common/collect/TreeMultimap;

    .line 493
    :goto_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesData:Lcom/google/common/collect/TreeMultimap;

    invoke-virtual {v1}, Lcom/google/common/collect/TreeMultimap;->hashCode()I

    move-result v0

    .line 494
    .local v0, "newHash":I
    sget v1, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesHash:I

    if-eq v0, v1, :cond_1

    .line 495
    sput v0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesHash:I

    .line 499
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->notifyFinsky(Landroid/content/Context;)V

    .line 504
    sget-object v1, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesClearer:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    const-wide/32 v2, 0x493e0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 512
    :goto_1
    sput-boolean v5, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetching:Z

    .line 513
    return-void

    .line 488
    .end local v0    # "newHash":I
    :cond_0
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->computeFinalDataFromOfferSummaries(Ljava/util/List;)Lcom/google/common/collect/TreeMultimap;

    move-result-object v1

    sput-object v1, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesData:Lcom/google/common/collect/TreeMultimap;

    goto :goto_0

    .line 510
    .restart local v0    # "newHash":I
    :cond_1
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->clearFinalMagazinesData(Z)V

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->mBinder:Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 142
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 143
    new-instance v1, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->mBinder:Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;

    .line 145
    :try_start_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->initAccountIfNeeded(Landroid/accounts/Account;)Landroid/accounts/Account;
    :try_end_0
    .catch Lcom/google/apps/dots/android/newsstand/auth/NoGoogleAccountException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_0
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Lcom/google/apps/dots/android/newsstand/auth/NoGoogleAccountException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "No Google accounts on this device, so no magazines will be returned when requested"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
