.class public final Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OfferSummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;


# instance fields
.field private acceptVerb_:Ljava/lang/String;

.field private amount_:Ljava/lang/String;

.field private appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

.field private appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

.field private backendDocId_:Ljava/lang/String;

.field private backendId_:I

.field private bitField0_:I

.field private currencyCode_:Ljava/lang/String;

.field private description_:Ljava/lang/String;

.field private docType_:I

.field private expirationDateMillis_:J

.field private fiveStarRating_:F

.field private formattedFullAmount_:Ljava/lang/String;

.field private fullDocId_:Ljava/lang/String;

.field private fullPriceMicros_:J

.field private localizedPrice_:Ljava/lang/String;

.field private micros_:J

.field private name_:Ljava/lang/String;

.field private offerId_:Ljava/lang/String;

.field private offerType_:I

.field private rejectVerb_:Ljava/lang/String;

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26566
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 26567
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 26579
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerId_:Ljava/lang/String;

    .line 26601
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->currencyCode_:Ljava/lang/String;

    .line 26623
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->amount_:Ljava/lang/String;

    .line 26645
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->name_:Ljava/lang/String;

    .line 26667
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->description_:Ljava/lang/String;

    .line 26689
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendId_:I

    .line 26708
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->docType_:I

    .line 26727
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendDocId_:Ljava/lang/String;

    .line 26749
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullDocId_:Ljava/lang/String;

    .line 26771
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerType_:I

    .line 26790
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->micros_:J

    .line 26809
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fiveStarRating_:F

    .line 26828
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->type_:I

    .line 26885
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->localizedPrice_:Ljava/lang/String;

    .line 26907
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->expirationDateMillis_:J

    .line 26926
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->acceptVerb_:Ljava/lang/String;

    .line 26948
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->rejectVerb_:Ljava/lang/String;

    .line 26970
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullPriceMicros_:J

    .line 26989
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->formattedFullAmount_:Ljava/lang/String;

    .line 26567
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .locals 3

    .prologue
    .line 27041
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 27045
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v2, :cond_0

    .line 27046
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 27048
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-eqz v2, :cond_1

    .line 27049
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 27051
    :cond_1
    return-object v0

    .line 27042
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    :catch_0
    move-exception v1

    .line 27043
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 26563
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 27056
    if-ne p1, p0, :cond_1

    .line 27079
    :cond_0
    :goto_0
    return v1

    .line 27057
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 27058
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 27059
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->currencyCode_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->currencyCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 27060
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->amount_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->amount_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 27061
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->name_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->name_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 27062
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->description_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->description_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 27063
    :goto_5
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendId_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendId_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->docType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->docType_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendDocId_:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendDocId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 27066
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullDocId_:Ljava/lang/String;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullDocId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 27067
    :goto_7
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerType_:I

    if-ne v3, v4, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->micros_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->micros_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fiveStarRating_:F

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fiveStarRating_:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-nez v3, :cond_3

    .line 27072
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-nez v3, :cond_3

    .line 27073
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->localizedPrice_:Ljava/lang/String;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->localizedPrice_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 27074
    :goto_a
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->expirationDateMillis_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->expirationDateMillis_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->acceptVerb_:Ljava/lang/String;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->acceptVerb_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 27076
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->rejectVerb_:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->rejectVerb_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 27077
    :goto_c
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullPriceMicros_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullPriceMicros_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->formattedFullAmount_:Ljava/lang/String;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->formattedFullAmount_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 27079
    goto/16 :goto_0

    .line 27059
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->currencyCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->currencyCode_:Ljava/lang/String;

    .line 27060
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->amount_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->amount_:Ljava/lang/String;

    .line 27061
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->name_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->name_:Ljava/lang/String;

    .line 27062
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->description_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->description_:Ljava/lang/String;

    .line 27063
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendDocId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendDocId_:Ljava/lang/String;

    .line 27066
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullDocId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullDocId_:Ljava/lang/String;

    .line 27067
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 27072
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 27073
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->localizedPrice_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->localizedPrice_:Ljava/lang/String;

    .line 27074
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->acceptVerb_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->acceptVerb_:Ljava/lang/String;

    .line 27076
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->rejectVerb_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->rejectVerb_:Ljava/lang/String;

    .line 27077
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->formattedFullAmount_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->formattedFullAmount_:Ljava/lang/String;

    .line 27079
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0
.end method

.method public getAcceptVerb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26928
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->acceptVerb_:Ljava/lang/String;

    return-object v0
.end method

.method public getAmount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26625
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->amount_:Ljava/lang/String;

    return-object v0
.end method

.method public getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 1

    .prologue
    .line 26868
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    return-object v0
.end method

.method public getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1

    .prologue
    .line 26849
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    return-object v0
.end method

.method public getBackendDocId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26729
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendDocId_:Ljava/lang/String;

    return-object v0
.end method

.method public getBackendId()I
    .locals 1

    .prologue
    .line 26691
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendId_:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26669
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getDocType()I
    .locals 1

    .prologue
    .line 26710
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->docType_:I

    return v0
.end method

.method public getExpirationDateMillis()J
    .locals 2

    .prologue
    .line 26909
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->expirationDateMillis_:J

    return-wide v0
.end method

.method public getFiveStarRating()F
    .locals 1

    .prologue
    .line 26811
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fiveStarRating_:F

    return v0
.end method

.method public getFullDocId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26751
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullDocId_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26647
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getOfferId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26581
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerId_:Ljava/lang/String;

    return-object v0
.end method

.method public getOfferType()I
    .locals 1

    .prologue
    .line 26773
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerType_:I

    return v0
.end method

.method public getRejectVerb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26950
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->rejectVerb_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 27178
    const/4 v0, 0x0

    .line 27179
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 27180
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->currencyCode_:Ljava/lang/String;

    .line 27181
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27183
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 27184
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->amount_:Ljava/lang/String;

    .line 27185
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27187
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 27188
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->name_:Ljava/lang/String;

    .line 27189
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27191
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    .line 27192
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->description_:Ljava/lang/String;

    .line 27193
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27195
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    .line 27196
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendId_:I

    .line 27197
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27199
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_5

    .line 27200
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->docType_:I

    .line 27201
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27203
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_6

    .line 27204
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendDocId_:Ljava/lang/String;

    .line 27205
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27207
    :cond_6
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_7

    .line 27208
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullDocId_:Ljava/lang/String;

    .line 27209
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27211
    :cond_7
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_8

    .line 27212
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerType_:I

    .line 27213
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27215
    :cond_8
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_9

    .line 27216
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->micros_:J

    .line 27217
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 27219
    :cond_9
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_a

    .line 27220
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fiveStarRating_:F

    .line 27221
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 27223
    :cond_a
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_b

    .line 27224
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->type_:I

    .line 27225
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27227
    :cond_b
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v1, :cond_c

    .line 27228
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 27229
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27231
    :cond_c
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-eqz v1, :cond_d

    .line 27232
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 27233
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27235
    :cond_d
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_e

    .line 27236
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->localizedPrice_:Ljava/lang/String;

    .line 27237
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27239
    :cond_e
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_f

    .line 27240
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerId_:Ljava/lang/String;

    .line 27241
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27243
    :cond_f
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_10

    .line 27244
    const/16 v1, 0x11

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->expirationDateMillis_:J

    .line 27245
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 27247
    :cond_10
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const v2, 0x8000

    and-int/2addr v1, v2

    if-eqz v1, :cond_11

    .line 27248
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->acceptVerb_:Ljava/lang/String;

    .line 27249
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27251
    :cond_11
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const/high16 v2, 0x10000

    and-int/2addr v1, v2

    if-eqz v1, :cond_12

    .line 27252
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->rejectVerb_:Ljava/lang/String;

    .line 27253
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27255
    :cond_12
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const/high16 v2, 0x20000

    and-int/2addr v1, v2

    if-eqz v1, :cond_13

    .line 27256
    const/16 v1, 0x14

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullPriceMicros_:J

    .line 27257
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 27259
    :cond_13
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const/high16 v2, 0x40000

    and-int/2addr v1, v2

    if-eqz v1, :cond_14

    .line 27260
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->formattedFullAmount_:Ljava/lang/String;

    .line 27261
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27263
    :cond_14
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->cachedSize:I

    .line 27264
    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 26830
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->type_:I

    return v0
.end method

.method public hasAcceptVerb()Z
    .locals 2

    .prologue
    .line 26939
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAmount()Z
    .locals 1

    .prologue
    .line 26636
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBackendDocId()Z
    .locals 1

    .prologue
    .line 26740
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExpirationDateMillis()Z
    .locals 1

    .prologue
    .line 26917
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRejectVerb()Z
    .locals 2

    .prologue
    .line 26961
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v2, 0x0

    .line 27083
    const/16 v0, 0x11

    .line 27084
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 27085
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 27086
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->currencyCode_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 27087
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->amount_:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 27088
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->name_:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 27089
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->description_:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 27090
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendId_:I

    add-int v0, v1, v3

    .line 27091
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->docType_:I

    add-int v0, v1, v3

    .line 27092
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendDocId_:Ljava/lang/String;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    add-int v0, v3, v1

    .line 27093
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullDocId_:Ljava/lang/String;

    if-nez v1, :cond_6

    move v1, v2

    :goto_6
    add-int v0, v3, v1

    .line 27094
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerType_:I

    add-int v0, v1, v3

    .line 27095
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->micros_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->micros_:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 27096
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fiveStarRating_:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v0, v1, v3

    .line 27097
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->type_:I

    add-int v0, v1, v3

    .line 27098
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-nez v1, :cond_7

    move v1, v2

    :goto_7
    add-int v0, v3, v1

    .line 27099
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-nez v1, :cond_8

    move v1, v2

    :goto_8
    add-int v0, v3, v1

    .line 27100
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->localizedPrice_:Ljava/lang/String;

    if-nez v1, :cond_9

    move v1, v2

    :goto_9
    add-int v0, v3, v1

    .line 27101
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->expirationDateMillis_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->expirationDateMillis_:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 27102
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->acceptVerb_:Ljava/lang/String;

    if-nez v1, :cond_a

    move v1, v2

    :goto_a
    add-int v0, v3, v1

    .line 27103
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->rejectVerb_:Ljava/lang/String;

    if-nez v1, :cond_b

    move v1, v2

    :goto_b
    add-int v0, v3, v1

    .line 27104
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullPriceMicros_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullPriceMicros_:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 27105
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->formattedFullAmount_:Ljava/lang/String;

    if-nez v3, :cond_c

    :goto_c
    add-int v0, v1, v2

    .line 27106
    return v0

    .line 27085
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_0

    .line 27086
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->currencyCode_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_1

    .line 27087
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->amount_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_2

    .line 27088
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->name_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_3

    .line 27089
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->description_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_4

    .line 27092
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendDocId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_5

    .line 27093
    :cond_6
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullDocId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_6

    .line 27098
    :cond_7
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hashCode()I

    move-result v1

    goto/16 :goto_7

    .line 27099
    :cond_8
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hashCode()I

    move-result v1

    goto/16 :goto_8

    .line 27100
    :cond_9
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->localizedPrice_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_9

    .line 27102
    :cond_a
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->acceptVerb_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    .line 27103
    :cond_b
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->rejectVerb_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    .line 27105
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->formattedFullAmount_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_c
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27272
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 27273
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 27277
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 27278
    :sswitch_0
    return-object p0

    .line 27283
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->currencyCode_:Ljava/lang/String;

    .line 27284
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto :goto_0

    .line 27288
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->amount_:Ljava/lang/String;

    .line 27289
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto :goto_0

    .line 27293
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->name_:Ljava/lang/String;

    .line 27294
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto :goto_0

    .line 27298
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->description_:Ljava/lang/String;

    .line 27299
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto :goto_0

    .line 27303
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendId_:I

    .line 27304
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto :goto_0

    .line 27308
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->docType_:I

    .line 27309
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto :goto_0

    .line 27313
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendDocId_:Ljava/lang/String;

    .line 27314
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto :goto_0

    .line 27318
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullDocId_:Ljava/lang/String;

    .line 27319
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto :goto_0

    .line 27323
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerType_:I

    .line 27324
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto/16 :goto_0

    .line 27328
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->micros_:J

    .line 27329
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto/16 :goto_0

    .line 27333
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fiveStarRating_:F

    .line 27334
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto/16 :goto_0

    .line 27338
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 27339
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 27343
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->type_:I

    .line 27344
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto/16 :goto_0

    .line 27346
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->type_:I

    goto/16 :goto_0

    .line 27351
    .end local v1    # "temp":I
    :sswitch_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-nez v2, :cond_3

    .line 27352
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 27354
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 27358
    :sswitch_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-nez v2, :cond_4

    .line 27359
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 27361
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 27365
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->localizedPrice_:Ljava/lang/String;

    .line 27366
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto/16 :goto_0

    .line 27370
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerId_:Ljava/lang/String;

    .line 27371
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto/16 :goto_0

    .line 27375
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->expirationDateMillis_:J

    .line 27376
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto/16 :goto_0

    .line 27380
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->acceptVerb_:Ljava/lang/String;

    .line 27381
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const v3, 0x8000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto/16 :goto_0

    .line 27385
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->rejectVerb_:Ljava/lang/String;

    .line 27386
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const/high16 v3, 0x10000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto/16 :goto_0

    .line 27390
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullPriceMicros_:J

    .line 27391
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const/high16 v3, 0x20000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto/16 :goto_0

    .line 27395
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->formattedFullAmount_:Ljava/lang/String;

    .line 27396
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const/high16 v3, 0x40000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    goto/16 :goto_0

    .line 27273
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5d -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa0 -> :sswitch_14
        0xaa -> :sswitch_15
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26563
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27111
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 27112
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->currencyCode_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 27114
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 27115
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->amount_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 27117
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 27118
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->name_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 27120
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    .line 27121
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->description_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 27123
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    .line 27124
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendId_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 27126
    :cond_4
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_5

    .line 27127
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->docType_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 27129
    :cond_5
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    .line 27130
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->backendDocId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 27132
    :cond_6
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_7

    .line 27133
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullDocId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 27135
    :cond_7
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_8

    .line 27136
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerType_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 27138
    :cond_8
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_9

    .line 27139
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->micros_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 27141
    :cond_9
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_a

    .line 27142
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fiveStarRating_:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 27144
    :cond_a
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_b

    .line 27145
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 27147
    :cond_b
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v0, :cond_c

    .line 27148
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 27150
    :cond_c
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-eqz v0, :cond_d

    .line 27151
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 27153
    :cond_d
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_e

    .line 27154
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->localizedPrice_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 27156
    :cond_e
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_f

    .line 27157
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->offerId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 27159
    :cond_f
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_10

    .line 27160
    const/16 v0, 0x11

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->expirationDateMillis_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 27162
    :cond_10
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_11

    .line 27163
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->acceptVerb_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 27165
    :cond_11
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_12

    .line 27166
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->rejectVerb_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 27168
    :cond_12
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    if-eqz v0, :cond_13

    .line 27169
    const/16 v0, 0x14

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->fullPriceMicros_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 27171
    :cond_13
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->bitField0_:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    if-eqz v0, :cond_14

    .line 27172
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->formattedFullAmount_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 27174
    :cond_14
    return-void
.end method
