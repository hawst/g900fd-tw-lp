.class public Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;
.super Ljava/lang/Object;
.source "AttachmentUtil.java"


# static fields
.field private static final ATTACHMENT_ID_PATTERN:Ljava/util/regex/Pattern;

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 25
    const-string v0, "[A-Za-z0-9\\-\\_:.]+"

    .line 26
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->ATTACHMENT_ID_PATTERN:Ljava/util/regex/Pattern;

    .line 25
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getIconAttachmentId(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Ljava/lang/String;
    .locals 2
    .param p0, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 67
    const/4 v0, 0x0

    .line 68
    .local v0, "iconAttachmentId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasIconImage()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    .line 71
    :cond_0
    if-nez v0, :cond_1

    .line 72
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v0

    .line 74
    :cond_1
    return-object v0
.end method

.method public static getIconHeightToWidthRatio(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;F)F
    .locals 5
    .param p0, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p1, "defaultRatio"    # F

    .prologue
    const/4 v4, 0x0

    .line 78
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasIconImage()Z

    move-result v3

    if-nez v3, :cond_1

    .line 90
    .end local p1    # "defaultRatio":F
    :cond_0
    :goto_0
    return p1

    .line 82
    .restart local p1    # "defaultRatio":F
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v1

    .line 83
    .local v1, "iconImage":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getWidth()I

    move-result v3

    int-to-float v2, v3

    .line 84
    .local v2, "width":F
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getHeight()I

    move-result v3

    int-to-float v0, v3

    .line 85
    .local v0, "height":F
    cmpl-float v3, v2, v4

    if-lez v3, :cond_0

    cmpl-float v3, v0, v4

    if-lez v3, :cond_0

    .line 86
    div-float p1, v0, v2

    goto :goto_0
.end method

.method public static getObjectIdProto(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .locals 7
    .param p0, "attachmentId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 41
    move-object v0, p0

    .line 42
    .local v0, "objectIdGuess":Ljava/lang/String;
    const/4 v1, 0x0

    .line 47
    .local v1, "result":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->tryParseObjectId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v1

    .line 48
    if-eqz v1, :cond_2

    .line 60
    :cond_0
    if-nez v1, :cond_1

    .line 61
    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Unable to extract object id from attachmentId: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    :cond_1
    return-object v1

    .line 53
    :cond_2
    const/16 v3, 0x2d

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    const/16 v4, 0x3a

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 54
    .local v2, "separatorIndex":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 58
    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 59
    goto :goto_0
.end method

.method public static isValidAttachmentId(Ljava/lang/String;)Z
    .locals 1
    .param p0, "attachmentId"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-static {p0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->ATTACHMENT_ID_PATTERN:Ljava/util/regex/Pattern;

    .line 36
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getObjectIdProto(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidAttachmentIdOrUrl(Ljava/lang/String;)Z
    .locals 1
    .param p0, "attachmentId"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->isValidAttachmentId(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;->isUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
