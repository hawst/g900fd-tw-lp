.class Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;
.super Ljava/lang/Object;
.source "AudioPlayerService.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 225
    packed-switch p1, :pswitch_data_0

    .line 245
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 227
    :pswitch_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->isPrepared()Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$200(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$300(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->pausedByTransientAudioFocusLoss:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$402(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Z)Z

    .line 229
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->play(Z)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$500(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Z)V

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$700(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaButtonReceiver:Landroid/content/ComponentName;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$600(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    goto :goto_0

    .line 234
    :pswitch_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->isPrepared()Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$200(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$300(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->pausedByTransientAudioFocusLoss:Z
    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$402(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Z)Z

    .line 236
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->play(Z)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$500(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Z)V

    goto :goto_0

    .line 240
    :pswitch_3
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->isPrepared()Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$200(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$300(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->pausedByTransientAudioFocusLoss:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$400(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->pausedByTransientAudioFocusLoss:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$402(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Z)Z

    .line 242
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->play(Z)V
    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$500(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Z)V

    goto :goto_0

    .line 225
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
