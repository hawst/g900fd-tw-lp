.class Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;
.super Ljava/lang/Object;
.source "JsonStoreLoader.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->load(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Ljava/lang/Object;",
        "Lorg/codehaus/jackson/node/ObjectNode;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

.field final synthetic val$applicationFuture:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$articleTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$idToAdTemplatesFuture:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

.field final synthetic val$postFuture:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$sectionFuture:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$useLegacyLayoutFuture:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$userRolesFuture:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$applicationFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$sectionFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$postFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$idToAdTemplatesFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p7, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$userRolesFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$articleTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p9, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$useLegacyLayoutFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p10, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    iput-object p11, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->apply(Ljava/lang/Object;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    return-object v0
.end method

.method public apply(Ljava/lang/Object;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 11
    .param p1, "unusedInput"    # Ljava/lang/Object;

    .prologue
    .line 199
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$applicationFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v8}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;

    .line 200
    .local v0, "app":Lcom/google/apps/dots/proto/client/DotsShared$Application;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v8}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .line 201
    .local v2, "form":Lcom/google/apps/dots/proto/client/DotsShared$Form;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$sectionFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v8}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 202
    .local v5, "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$postFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v8}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 203
    .local v4, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$idToAdTemplatesFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v8}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 204
    .local v3, "idToAdTemplates":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;"
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$userRolesFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v8}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .line 205
    .local v7, "userRoles":Lcom/google/apps/dots/proto/client/DotsShared$RoleList;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$articleTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v8}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 206
    .local v1, "articleTemplate":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$useLegacyLayoutFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v8}, Lcom/google/common/util/concurrent/Futures;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    .line 209
    .local v6, "useLegacyLayout":Ljava/lang/Boolean;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v10, v10, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addExecutionContextData(Lorg/codehaus/jackson/node/ObjectNode;Landroid/accounts/Account;)V
    invoke-static {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->access$000(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Landroid/accounts/Account;)V

    .line 210
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addLabsData(Lorg/codehaus/jackson/node/ObjectNode;)V
    invoke-static {v8, v9}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->access$100(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;)V

    .line 211
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addAdsData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/util/Map;)V
    invoke-static {v8, v9, v0, v4, v3}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->access$200(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/util/Map;)V

    .line 212
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addSectionHeaderData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V
    invoke-static {v8, v9, v5}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->access$300(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V

    .line 213
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addUserRolesData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$RoleList;)V
    invoke-static {v8, v9, v7}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->access$400(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$RoleList;)V

    .line 214
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addTextNameData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V
    invoke-static {v8, v9, v0, v5}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->access$500(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V

    .line 215
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addContentData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;ZLcom/google/apps/dots/proto/client/DotsShared$Form;)V
    invoke-static {v8, v9, v1, v10, v2}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->access$600(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;ZLcom/google/apps/dots/proto/client/DotsShared$Form;)V

    .line 216
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;->val$jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    return-object v8
.end method
