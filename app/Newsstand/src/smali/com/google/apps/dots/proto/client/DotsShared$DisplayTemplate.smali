.class public final Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayTemplate"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;


# instance fields
.field private mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

.field private phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9117
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9118
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
    .locals 3

    .prologue
    .line 9599
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 9603
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-eqz v2, :cond_0

    .line 9604
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 9606
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-eqz v2, :cond_1

    .line 9607
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 9609
    :cond_1
    return-object v0

    .line 9600
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
    :catch_0
    move-exception v1

    .line 9601
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 9114
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 9614
    if-ne p1, p0, :cond_1

    .line 9618
    :cond_0
    :goto_0
    return v1

    .line 9615
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 9616
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 9617
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 9618
    goto :goto_0

    .line 9617
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 9618
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getMainTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .locals 1

    .prologue
    .line 9553
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    return-object v0
.end method

.method public getPhoneTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .locals 1

    .prologue
    .line 9572
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 9641
    const/4 v0, 0x0

    .line 9642
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-eqz v1, :cond_0

    .line 9643
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 9644
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9646
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-eqz v1, :cond_1

    .line 9647
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 9648
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9650
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->cachedSize:I

    .line 9651
    return v0
.end method

.method public hasPhoneTemplate()Z
    .locals 1

    .prologue
    .line 9582
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 9622
    const/16 v0, 0x11

    .line 9623
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 9624
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 9625
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 9626
    return v0

    .line 9624
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->hashCode()I

    move-result v1

    goto :goto_0

    .line 9625
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9659
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 9660
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 9664
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 9665
    :sswitch_0
    return-object p0

    .line 9670
    :sswitch_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-nez v1, :cond_1

    .line 9671
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 9673
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 9677
    :sswitch_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-nez v1, :cond_2

    .line 9678
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 9680
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 9660
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x2a -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9114
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9631
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-eqz v0, :cond_0

    .line 9632
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->mainTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 9634
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-eqz v0, :cond_1

    .line 9635
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->phoneTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 9637
    :cond_1
    return-void
.end method
