.class Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$3;
.super Ljava/lang/Object;
.source "ActionableToastBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->show(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

.field final synthetic val$operation:Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$3;->val$operation:Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "widget"    # Landroid/view/View;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->actionText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->access$100(Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->actionText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->access$100(Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 181
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$3;->val$operation:Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;->onActionClicked(Landroid/content/Context;)V

    .line 182
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->hide(Z)V

    .line 183
    return-void
.end method
