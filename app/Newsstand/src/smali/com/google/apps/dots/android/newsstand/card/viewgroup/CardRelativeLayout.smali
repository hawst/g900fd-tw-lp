.class public Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;
.super Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;
.source "CardRelativeLayout.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/card/ResizingCard;
.implements Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;
.implements Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;


# instance fields
.field private final cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

.field private final foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

.field private isRead:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;-><init>(Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    .line 25
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    .line 38
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->initialize(Lcom/google/android/play/cardview/CardViewGroup;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->initialize(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->initialize(Landroid/view/ViewGroup;Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method


# virtual methods
.method public canEnlarge()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->canEnlarge()Z

    move-result v0

    return v0
.end method

.method public canShrink()Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->canShrink()Z

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 182
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->draw(Landroid/graphics/Canvas;)V

    .line 183
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->draw(Landroid/view/ViewGroup;Landroid/graphics/Canvas;)V

    .line 184
    return-void
.end method

.method public drawableHotspotChanged(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 158
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->drawableHotspotChanged(FF)V

    .line 159
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->drawableHotspotChanged(FF)V

    .line 160
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->drawableStateChanged()V

    .line 151
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->drawableStateChanged(Landroid/view/ViewGroup;)V

    .line 154
    :cond_0
    return-void
.end method

.method public getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    return-object v0
.end method

.method public isRead()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->isRead:Z

    return v0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 143
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->jumpDrawablesToCurrentState()V

    .line 144
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->jumpDrawablesToCurrentState()V

    .line 145
    return-void
.end method

.method protected makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->onAttachedToWindow()V

    .line 103
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onAttachedToWindow()V

    .line 104
    return-void
.end method

.method public onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->isRead()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 125
    .local v0, "drawableState":[I
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->STATE_READ:[I

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->mergeDrawableStates([I[I)[I

    .line 128
    .end local v0    # "drawableState":[I
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->onDetachedFromWindow()V

    .line 97
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onDetachedFromWindow()V

    .line 98
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->onFinishTemporaryDetach()V

    .line 85
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onFinishTemporaryDetach()V

    .line 86
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 170
    invoke-super/range {p0 .. p5}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->onLayout(ZIIII)V

    .line 171
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->onLayout()V

    .line 172
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 176
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->onSizeChanged(IIII)V

    .line 177
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->onSizeChanged()V

    .line 178
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->onStartTemporaryDetach()V

    .line 91
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->cardViewGroupHelper:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->onStartTemporaryDetach()V

    .line 92
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundColor(Lcom/google/android/play/cardview/CardViewGroup;I)V

    .line 119
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundResource(Lcom/google/android/play/cardview/CardViewGroup;I)V

    .line 114
    return-void
.end method

.method public setIsRead(Z)V
    .locals 1
    .param p1, "isRead"    # Z

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->isRead:Z

    if-eq v0, p1, :cond_0

    .line 56
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->isRead:Z

    .line 57
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->refreshDrawableState()V

    .line 58
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->dispatchSetIsRead(Landroid/view/ViewGroup;Z)V

    .line 60
    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 164
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->setVisibility(I)V

    .line 166
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 138
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->foregroundProvider:Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
