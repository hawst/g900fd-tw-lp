.class Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog$1;
.super Ljava/lang/Object;
.source "TranslateDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;

    .line 82
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->languageToLanguageCode(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->access$100(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    # setter for: Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->translationCode:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->access$002(Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 83
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;

    const/4 v1, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->translationCode:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;->access$002(Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 88
    return-void
.end method
