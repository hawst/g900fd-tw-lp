.class public Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;
.super Ljava/lang/Object;
.source "ManifestBlobResolver.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;
    }
.end annotation


# instance fields
.field private final lock:Ljava/lang/Object;

.field private final resourceBundles:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->lock:Ljava/lang/Object;

    .line 20
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->resourceBundles:Ljava/util/WeakHashMap;

    .line 27
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .prologue
    .line 18
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->getUriMapKey(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;Ljava/lang/Object;)[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->findRef(Ljava/lang/Object;)[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/Class;

    .prologue
    .line 18
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->getObjectMapKey(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private findRef(Ljava/lang/Object;)[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 5
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 97
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 98
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->resourceBundles:Ljava/util/WeakHashMap;

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;

    .line 99
    .local v0, "resourceBundle":Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;
    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->uriMap:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    .line 100
    .local v1, "resourceLinkRef":[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    if-eqz v1, :cond_0

    .line 101
    monitor-exit v3

    .line 105
    .end local v0    # "resourceBundle":Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;
    .end local v1    # "resourceLinkRef":[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    :goto_0
    return-object v1

    .line 104
    :cond_1
    monitor-exit v3

    .line 105
    const/4 v1, 0x0

    goto :goto_0

    .line 104
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static getObjectMapKey(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/String;
    .locals 5
    .param p0, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getUriMapKey(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Ljava/lang/String;
    .locals 5
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .prologue
    .line 89
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private resolve(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 124
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->getObjectMapKey(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->lock:Ljava/lang/Object;

    monitor-enter v4

    .line 126
    :try_start_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->resourceBundles:Ljava/util/WeakHashMap;

    invoke-virtual {v3}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;

    .line 127
    .local v2, "resourceBundle":Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;
    iget-object v5, v2, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->objectMap:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 128
    .local v1, "object":Ljava/lang/Object;
    if-eqz v1, :cond_0

    .line 129
    monitor-exit v4

    .line 133
    .end local v1    # "object":Ljava/lang/Object;
    .end local v2    # "resourceBundle":Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;
    :goto_0
    return-object v1

    .line 132
    :cond_1
    monitor-exit v4

    .line 133
    const/4 v1, 0x0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method


# virtual methods
.method public makeBundle()Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;
    .locals 4

    .prologue
    .line 77
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;-><init>(Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;)V

    .line 78
    .local v0, "resourceBundle":Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 79
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->resourceBundles:Ljava/util/WeakHashMap;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    monitor-exit v2

    .line 81
    return-object v0

    .line 80
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public resolve(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .prologue
    .line 110
    invoke-static {p2, p3}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->getUriMapKey(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->findRef(Ljava/lang/Object;)[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    .line 111
    .local v0, "resourceLinkRef":[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    aget-object v1, v0, v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resolveAppFamilySummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 1
    .param p1, "appFamilyId"    # Ljava/lang/String;

    .prologue
    .line 119
    const-class v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->resolve(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    return-object v0
.end method

.method public resolveAppSummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 115
    const-class v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->resolve(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    return-object v0
.end method
