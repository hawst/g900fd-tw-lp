.class public Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;
.super Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;
.source "NSAccountNameManager.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field protected final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method protected constructor <init>(Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V
    .locals 0
    .param p1, "accountManagerDelegate"    # Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;
    .param p2, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;-><init>(Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;)V

    .line 36
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 37
    return-void
.end method

.method public static create(Landroid/accounts/AccountManager;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;
    .locals 1
    .param p0, "accountManager"    # Landroid/accounts/AccountManager;
    .param p1, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;-><init>(Landroid/accounts/AccountManager;)V

    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;->create(Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;

    move-result-object v0

    return-object v0
.end method

.method public static create(Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;
    .locals 1
    .param p0, "accountManagerDelegate"    # Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;
    .param p1, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 29
    new-instance v0, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;-><init>(Lcom/google/apps/dots/android/newsstand/auth/AccountManagerDelegate;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    .line 30
    .local v0, "instance":Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;->init()V

    .line 31
    return-object v0
.end method


# virtual methods
.method protected onAccountRenamed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "currentName"    # Ljava/lang/String;
    .param p2, "previousName"    # Ljava/lang/String;
    .param p3, "originalName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->onAccountRenamed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 54
    .local v0, "selectedAccount":Landroid/accounts/Account;
    if-eqz v0, :cond_1

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 55
    invoke-static {v1, p3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->isSetupDone()Z

    move-result v1

    if-nez v1, :cond_2

    .line 59
    sget-object v1, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Current account rename was detected during bootstrapping."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ii(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->accountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setAccount(Landroid/accounts/Account;)V

    .line 67
    :cond_1
    :goto_0
    return-void

    .line 63
    :cond_2
    sget-object v1, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Current account was renamed while app was running. Restarting."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->killAndRestartIfInForeground()V

    goto :goto_0
.end method

.method protected persistNameMap(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v1, "accountNameMap"

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setStringMap(Ljava/lang/String;Ljava/util/Map;)V

    .line 47
    return-void
.end method

.method protected retrieveNameMap()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v1, "accountNameMap"

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getStringMap(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
