.class public abstract Lcom/google/apps/dots/android/newsstand/service/AsyncService;
.super Landroid/app/Service;
.source "AsyncService.java"


# instance fields
.field private creationTime:J

.field protected final logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private final pending:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 32
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->pending:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/service/AsyncService;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/service/AsyncService;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->pending:Ljava/util/Set;

    return-object v0
.end method

.method private track(Landroid/content/Intent;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p2, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    const-string v0, "resultReceiver"

    .line 60
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/os/ResultReceiver;

    .line 61
    .local v2, "receiver":Landroid/os/ResultReceiver;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 62
    .local v4, "intentStartTime":J
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->pending:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 63
    new-instance v0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;

    move-object v1, p0

    move-object v3, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;-><init>(Lcom/google/apps/dots/android/newsstand/service/AsyncService;Landroid/os/ResultReceiver;Landroid/content/Intent;JLcom/google/common/util/concurrent/ListenableFuture;)V

    .line 101
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadExecutor()Lcom/google/common/util/concurrent/ListeningExecutorService;

    move-result-object v1

    .line 63
    invoke-static {p2, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 102
    return-void
.end method


# virtual methods
.method protected abstract handleIntent(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 38
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 39
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->creationTime:J

    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Created"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    return-void
.end method

.method public onDestroy()V
    .locals 8

    .prologue
    .line 45
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Destroyed after %.2f seconds"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->creationTime:J

    sub-long/2addr v4, v6

    long-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Handling intent %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/navigation/IntentBuilder;->toString(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->handleIntent(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->track(Landroid/content/Intent;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 53
    const/4 v0, 0x2

    return v0
.end method
