.class public Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;
.super Landroid/widget/LinearLayout;
.source "GroupRowMoneyshotLayout.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowLayout;


# static fields
.field private static final NOT_FOUND:I = -0x1

.field private static final NUM_CARDS:I = 0x3

.field private static final ineligibleCardLayouts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_item:I

    .line 30
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item:I

    .line 31
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_horizontal:I

    .line 32
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine:I

    .line 33
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine_horizontal:I

    .line 34
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_one_column:I

    .line 35
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_two_column:I

    .line 36
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 30
    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;->ineligibleCardLayouts:Ljava/util/List;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method private areAllCardsEligible(Ljava/util/List;Ljava/util/ArrayList;I)Z
    .locals 4
    .param p3, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .local p2, "cardListIndices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    .line 121
    add-int v3, p3, v2

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/Data;

    .line 122
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 123
    .local v0, "cardResId":Ljava/lang/Integer;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;->ineligibleCardLayouts:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 124
    const/4 v3, 0x0

    .line 127
    .end local v0    # "cardResId":Ljava/lang/Integer;
    .end local v1    # "data":Lcom/google/android/libraries/bind/data/Data;
    :goto_1
    return v3

    .line 120
    .restart local v0    # "cardResId":Ljava/lang/Integer;
    .restart local v1    # "data":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 127
    .end local v0    # "cardResId":Ljava/lang/Integer;
    .end local v1    # "data":Lcom/google/android/libraries/bind/data/Data;
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private findMoneyshotCard(Ljava/util/List;Ljava/util/ArrayList;I)I
    .locals 7
    .param p3, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .local p2, "cardListIndices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v6, 0x3

    .line 101
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v6, :cond_1

    .line 102
    add-int v4, p3, v3

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/Data;

    .line 103
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-virtual {v1, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 105
    .local v0, "cardResId":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 104
    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getResizedLayoutId(ILjava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v2

    .line 106
    .local v2, "enlargedResourceId":Ljava/lang/Integer;
    if-eqz v2, :cond_0

    const/4 v4, 0x2

    .line 107
    invoke-static {v1, v4}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->isPrimaryImageEligibleForLargeLayout(Lcom/google/android/libraries/bind/data/Data;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 108
    add-int v4, p3, v3

    .line 111
    .end local v0    # "cardResId":Ljava/lang/Integer;
    .end local v1    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "enlargedResourceId":Ljava/lang/Integer;
    :goto_1
    return v4

    .line 101
    .restart local v0    # "cardResId":Ljava/lang/Integer;
    .restart local v1    # "data":Lcom/google/android/libraries/bind/data/Data;
    .restart local v2    # "enlargedResourceId":Ljava/lang/Integer;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 111
    .end local v0    # "cardResId":Ljava/lang/Integer;
    .end local v1    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "enlargedResourceId":Ljava/lang/Integer;
    :cond_1
    const/4 v4, -0x1

    goto :goto_1
.end method

.method private makeCardIndexArray(Ljava/util/ArrayList;I)[I
    .locals 4
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)[I"
        }
    .end annotation

    .prologue
    .local p1, "cardListIndices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v3, 0x3

    .line 136
    new-array v1, v3, [I

    .line 137
    .local v1, "returnArray":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 138
    add-int v2, p2, v0

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v1, v0

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_0
    return-object v1
.end method


# virtual methods
.method public arrangeLayout(Ljava/util/List;Ljava/util/ArrayList;I)[I
    .locals 5
    .param p3, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)[I"
        }
    .end annotation

    .prologue
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .local p2, "cardListIndices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .line 76
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v4, p3, 0x3

    if-ge v3, v4, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-object v2

    .line 81
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;->findMoneyshotCard(Ljava/util/List;Ljava/util/ArrayList;I)I

    move-result v1

    .line 82
    .local v1, "moneyshotCardIndex":I
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;->areAllCardsEligible(Ljava/util/List;Ljava/util/ArrayList;I)Z

    move-result v0

    .line 84
    .local v0, "eligibleCards":Z
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    if-eqz v0, :cond_0

    .line 85
    invoke-static {p2, v1, p3}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 86
    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;->makeCardIndexArray(Ljava/util/ArrayList;I)[I

    move-result-object v2

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 54
    .local v2, "parentView":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 55
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 56
    .local v0, "desiredHeight":I
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 57
    .local v1, "newHeightMeasureSpec":I
    invoke-super {p0, p1, v1}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 61
    .end local v0    # "desiredHeight":I
    .end local v1    # "newHeightMeasureSpec":I
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public prepareGroupRowForRecycling()V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method public shouldEnlarge()Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    return v0
.end method

.method public shouldShrink()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method
