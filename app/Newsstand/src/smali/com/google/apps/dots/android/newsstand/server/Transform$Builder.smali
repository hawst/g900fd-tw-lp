.class public Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
.super Ljava/lang/Object;
.source "Transform.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/server/Transform;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private crop:Z

.field private fcrop64Bottom:I

.field private fcrop64Left:I

.field private fcrop64Right:I

.field private fcrop64Top:I

.field private height:I

.field private original:Z

.field private width:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const v1, 0xffff

    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Left:I

    .line 57
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Top:I

    .line 58
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Right:I

    .line 59
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Bottom:I

    .line 63
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/server/Transform;)V
    .locals 2
    .param p1, "target"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    const v1, 0xffff

    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Left:I

    .line 57
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Top:I

    .line 58
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Right:I

    .line 59
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Bottom:I

    .line 66
    iget v0, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width:I

    .line 67
    iget v0, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height:I

    .line 68
    iget-boolean v0, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;->crop:Z

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->crop:Z

    .line 69
    iget v0, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Left:I

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Left:I

    .line 70
    iget v0, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Top:I

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Top:I

    .line 71
    iget v0, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Right:I

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Right:I

    .line 72
    iget v0, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Bottom:I

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Bottom:I

    .line 73
    iget-boolean v0, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;->original:Z

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    .line 74
    return-void
.end method


# virtual methods
.method public build()Lcom/google/apps/dots/android/newsstand/server/Transform;
    .locals 10

    .prologue
    .line 82
    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/Transform;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width:I

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height:I

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->crop:Z

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Left:I

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Top:I

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Right:I

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Bottom:I

    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/google/apps/dots/android/newsstand/server/Transform;-><init>(IIZIIIIZLcom/google/apps/dots/android/newsstand/server/Transform$1;)V

    return-object v0
.end method

.method public crop(Z)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->crop:Z

    .line 104
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->crop:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    and-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    .line 105
    return-object p0

    .line 104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fcrop64Bottom(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .locals 7
    .param p1, "fcrop64Bottom"    # I

    .prologue
    const v6, 0xffff

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 149
    if-ltz p1, :cond_0

    if-gt p1, v6, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "fcrop64Bottom must be within [0, 0xFFFF], but was %s"

    new-array v4, v1, [Ljava/lang/Object;

    .line 150
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    .line 149
    invoke-static {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 151
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Bottom:I

    .line 152
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    if-ne p1, v6, :cond_1

    :goto_1
    and-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    .line 153
    return-object p0

    :cond_0
    move v0, v2

    .line 149
    goto :goto_0

    :cond_1
    move v1, v2

    .line 152
    goto :goto_1
.end method

.method public fcrop64Left(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .locals 6
    .param p1, "fcrop64Left"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    if-ltz p1, :cond_0

    const v0, 0xffff

    if-gt p1, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "fcrop64Left must be within [0, 0xFFFF], but was %s"

    new-array v4, v1, [Ljava/lang/Object;

    .line 114
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    .line 113
    invoke-static {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 115
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Left:I

    .line 116
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    if-nez p1, :cond_1

    :goto_1
    and-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    .line 117
    return-object p0

    :cond_0
    move v0, v2

    .line 113
    goto :goto_0

    :cond_1
    move v1, v2

    .line 116
    goto :goto_1
.end method

.method public fcrop64Right(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .locals 7
    .param p1, "fcrop64Right"    # I

    .prologue
    const v6, 0xffff

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 137
    if-ltz p1, :cond_0

    if-gt p1, v6, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "fcrop64Right must be within [0, 0xFFFF], but was %s"

    new-array v4, v1, [Ljava/lang/Object;

    .line 138
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    .line 137
    invoke-static {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 139
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Right:I

    .line 140
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    if-ne p1, v6, :cond_1

    :goto_1
    and-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    .line 141
    return-object p0

    :cond_0
    move v0, v2

    .line 137
    goto :goto_0

    :cond_1
    move v1, v2

    .line 140
    goto :goto_1
.end method

.method public fcrop64Top(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .locals 6
    .param p1, "fcrop64Top"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 125
    if-ltz p1, :cond_0

    const v0, 0xffff

    if-gt p1, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "fcrop64Top must be within [0, 0xFFFF], but was %s"

    new-array v4, v1, [Ljava/lang/Object;

    .line 126
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    .line 125
    invoke-static {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 127
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Top:I

    .line 128
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    if-nez p1, :cond_1

    :goto_1
    and-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    .line 129
    return-object p0

    :cond_0
    move v0, v2

    .line 125
    goto :goto_0

    :cond_1
    move v1, v2

    .line 128
    goto :goto_1
.end method

.method public height(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .locals 2
    .param p1, "height"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height:I

    .line 94
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    if-gtz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    and-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    .line 95
    return-object p0

    .line 94
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public original(Z)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .locals 3
    .param p1, "value"    # Z

    .prologue
    const v2, 0xffff

    const/4 v1, 0x0

    .line 157
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    .line 158
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    if-eqz v0, :cond_0

    .line 159
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width:I

    .line 160
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height:I

    .line 161
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Left:I

    .line 162
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Top:I

    .line 163
    iput v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Right:I

    .line 164
    iput v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Bottom:I

    .line 165
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->crop:Z

    .line 167
    :cond_0
    return-object p0
.end method

.method public square(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public width(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .locals 2
    .param p1, "width"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width:I

    .line 88
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    if-gtz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    and-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original:Z

    .line 89
    return-object p0

    .line 88
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
