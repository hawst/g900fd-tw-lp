.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$2;
.super Lcom/google/android/libraries/bind/data/DataObserver;
.source "MyMagazinesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 3
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 92
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Card group DataList changed."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateErrorViewData()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V

    .line 97
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateCardBuilder()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->access$300(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V

    .line 98
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateViewPosition()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->access$400(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V

    .line 99
    return-void
.end method
