.class Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1$1;
.super Ljava/lang/Object;
.source "AttachmentStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureFallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;->apply(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureFallback",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;

.field final synthetic val$options:Landroid/graphics/BitmapFactory$Options;

.field final synthetic val$storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Landroid/graphics/BitmapFactory$Options;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1$1;->val$storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1$1;->val$options:Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1$1;->val$storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$1$1;->val$options:Landroid/graphics/BitmapFactory$Options;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->decodeWithAllocatedBitmap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$100(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 108
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
