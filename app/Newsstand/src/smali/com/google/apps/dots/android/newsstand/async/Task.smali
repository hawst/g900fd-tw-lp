.class public abstract Lcom/google/apps/dots/android/newsstand/async/Task;
.super Ljava/lang/Object;
.source "Task.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<*>;>;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<+TV;>;>;"
    }
.end annotation


# instance fields
.field private final executor:Ljava/util/concurrent/Executor;

.field private final lock:Ljava/lang/Object;

.field private nextFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation
.end field

.field private nextRunnable:Ljava/lang/Runnable;

.field private final priority:J


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p1, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 33
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;J)V

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;J)V
    .locals 2
    .param p1, "executor"    # Ljava/util/concurrent/Executor;
    .param p2, "priority"    # J

    .prologue
    .line 39
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->lock:Ljava/lang/Object;

    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->executor:Ljava/util/concurrent/Executor;

    .line 41
    iput-wide p2, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->priority:J

    .line 42
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/Task;->reset()V

    .line 43
    return-void
.end method


# virtual methods
.method public abstract call()Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/Task;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public compareTo(Lcom/google/apps/dots/android/newsstand/async/Task;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/Task",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 101
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    .local p1, "other":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<*>;"
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->priority:J

    iget-wide v2, p1, Lcom/google/apps/dots/android/newsstand/async/Task;->priority:J

    invoke-static {v0, v1, v2, v3}, Lcom/google/common/primitives/Longs;->compare(JJ)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 20
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/async/Task;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/Task;->compareTo(Lcom/google/apps/dots/android/newsstand/async/Task;)I

    move-result v0

    return v0
.end method

.method public execute()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 89
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->executor:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->nextRunnable:Ljava/lang/Runnable;

    invoke-interface {v1, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->nextFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 91
    .local v0, "thisResult":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/Task;->reset()V

    .line 92
    monitor-exit v2

    return-object v0

    .line 93
    .end local v0    # "thisResult":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->nextFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->track(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 79
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/Task;->execute()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public next()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 68
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->nextFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    monitor-exit v1

    return-object v0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public reset()V
    .locals 4

    .prologue
    .line 46
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 50
    :try_start_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;->create(Ljava/util/concurrent/Callable;)Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;

    move-result-object v0

    .line 51
    .local v0, "nsFutureTask":Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask<Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;>;"
    move-object v1, v0

    .line 52
    .local v1, "wrappedFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;>;"
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->nextRunnable:Ljava/lang/Runnable;

    .line 59
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->dereference(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/Task;->nextFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 60
    monitor-exit v3

    .line 61
    return-void

    .line 60
    .end local v0    # "nsFutureTask":Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSListenableFutureTask<Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;>;"
    .end local v1    # "wrappedFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
