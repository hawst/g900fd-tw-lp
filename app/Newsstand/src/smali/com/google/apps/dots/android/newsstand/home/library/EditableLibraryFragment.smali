.class public abstract Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
.source "EditableLibraryFragment.java"


# instance fields
.field private adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

.field private final addMoreDummyEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private connectivityListener:Ljava/lang/Runnable;

.field emptyRowAllowed:Z

.field private final newsCommitHandler:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;

.field private prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

.field private sourceCardList:Lcom/google/android/libraries/bind/data/DataList;

.field private sourceGroup:Lcom/google/android/libraries/bind/card/CardGroup;

.field protected final updateHeaderCardsRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;-><init>()V

    .line 69
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->newsCommitHandler:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;

    .line 91
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->updateHeaderCardsRunnable:Ljava/lang/Runnable;

    .line 105
    const-string v0, "add_more"

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->addMoreDummyEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->updateHeaderCards()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->updateErrorView()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->connectivityListener:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->addMoreDummyEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getRetryRunnable()Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->account()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method private getRetryRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 272
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$8;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$8;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V

    return-object v0
.end method

.method private prependWarmWelcomeCardIfNeeded(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)V
    .locals 1
    .param p1, "warmWelcomeCard"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "rowId"    # Ljava/lang/String;

    .prologue
    .line 335
    if-nez p1, :cond_0

    .line 339
    :goto_0
    return-void

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {v0, p2, p1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->prepend(Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    goto :goto_0
.end method

.method private refreshIfNeeded()V
    .locals 2

    .prologue
    .line 445
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 446
    .local v0, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getSubscriptionsUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->refreshIfNeeded(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    .line 447
    return-void
.end method

.method private setEmptyRowAllowed(Z)V
    .locals 0
    .param p1, "allowed"    # Z

    .prologue
    .line 259
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->emptyRowAllowed:Z

    .line 260
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->updateEmptyView()V

    .line 261
    return-void
.end method

.method private setupListView()V
    .locals 5

    .prologue
    .line 169
    new-instance v2, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 173
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getSourceCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$5;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v3, p0, v4}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 204
    .local v1, "sourceCardListWithAddMore":Lcom/google/android/libraries/bind/data/DataList;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    invoke-direct {v2, v1}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    sget v3, Lcom/google/android/apps/newsstanddev/R$dimen;->my_sources_column_width:I

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setColumnWidthResId(I)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->newsCommitHandler:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;

    .line 205
    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/card/GridGroup;->makeEditable(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;)Lcom/google/android/libraries/bind/card/CardGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->sourceGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    .line 207
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->sourceGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$6;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/card/CardGroup;->setEmptyRowProvider(Lcom/google/android/libraries/bind/data/DataProvider;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 216
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->sourceGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 219
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 220
    .local v0, "cardList":Lcom/google/android/libraries/bind/data/DataList;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    .line 221
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$7;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v3, p0, v4}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 229
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 230
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 231
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-direct {v3}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 233
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getLibraryTag()Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setTag(Ljava/lang/Object;)V

    .line 234
    return-void
.end method

.method private updateEmptyView()V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->sourceGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/card/CardGroup;->invalidateRows()V

    .line 265
    return-void
.end method

.method private updateErrorView()V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->refreshErrorView()Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 269
    return-void
.end method

.method private updateHeaderCards()V
    .locals 5

    .prologue
    .line 287
    const/4 v0, 0x1

    .line 289
    .local v0, "emptyRowAllowed":Z
    const/4 v2, 0x0

    .line 290
    .local v2, "onlineCardVisible":Z
    const/4 v1, 0x0

    .line 292
    .local v1, "offlineCardVisible":Z
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    const-string v4, "OFFLINE_WARM_WELCOME"

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->remove(Ljava/lang/String;)Z

    .line 293
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    const-string v4, "ONLINE_WARM_WELCOME"

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->remove(Ljava/lang/String;)Z

    .line 295
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->isOnDeviceOnly()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v3

    if-nez v3, :cond_2

    .line 296
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->isOfflineCardVisible()Z

    move-result v1

    .line 302
    :goto_0
    if-eqz v2, :cond_4

    .line 303
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getOnlineWarmWelcomeCard()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    const-string v4, "ONLINE_WARM_WELCOME"

    invoke-direct {p0, v3, v4}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->prependWarmWelcomeCardIfNeeded(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)V

    .line 307
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->useSpacerTopPadding(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 310
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->setEmptyRowAllowed(Z)V

    .line 311
    return-void

    .line 298
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->isOnlineCardVisible()Z

    move-result v2

    .line 299
    if-nez v2, :cond_3

    const/4 v0, 0x1

    :goto_2
    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 304
    :cond_4
    if-eqz v1, :cond_1

    .line 305
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getOfflineWarmWelcomeCard()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    const-string v4, "OFFLINE_WARM_WELCOME"

    invoke-direct {p0, v3, v4}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->prependWarmWelcomeCardIfNeeded(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method commitNewsEdit(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
    .locals 12
    .param p1, "groupList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "operation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .prologue
    .line 350
    iget-object v9, p2, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    invoke-virtual {p1, v9}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v1

    .line 351
    .local v1, "currentPosition":I
    const/4 v9, -0x1

    if-ne v1, v9, :cond_0

    .line 391
    :goto_0
    return-void

    .line 354
    :cond_0
    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    .line 355
    .local v2, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v9, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_PURCHASED:I

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v4

    .line 356
    .local v4, "isPurchased":Z
    iget v9, p2, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    packed-switch v9, :pswitch_data_0

    goto :goto_0

    .line 379
    :pswitch_0
    sget v9, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 380
    .local v3, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v8

    .line 381
    .local v8, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-virtual {v3, v8}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    new-instance v10, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$9;

    invoke-direct {v10, p0, v4}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$9;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;Z)V

    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 358
    .end local v3    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v8    # "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    :pswitch_1
    sget v9, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_FAMILY_ID:I

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    .line 359
    .local v0, "appFamilyId":Ljava/lang/String;
    const/4 v6, 0x0

    .line 360
    .local v6, "pivotAppFamilyId":Ljava/lang/String;
    iget-object v9, p2, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-lez v9, :cond_1

    .line 362
    iget-object v9, p2, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/lit8 v7, v9, -0x1

    .line 366
    .local v7, "pivotPosition":I
    if-ge v7, v1, :cond_2

    .line 367
    invoke-virtual {p1, v7}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v5

    .line 372
    .local v5, "pivot":Lcom/google/android/libraries/bind/data/Data;
    :goto_1
    sget v9, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_FAMILY_ID:I

    invoke-virtual {v5, v9}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v6

    .line 374
    .end local v5    # "pivot":Lcom/google/android/libraries/bind/data/Data;
    .end local v7    # "pivotPosition":I
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->account()Landroid/accounts/Account;

    move-result-object v10

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getSubscriptionType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v11

    invoke-static {v9, v10, v11, v0, v6}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->reorderSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 369
    .restart local v7    # "pivotPosition":I
    :cond_2
    add-int/lit8 v9, v7, 0x1

    invoke-virtual {p1, v9}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v5

    .restart local v5    # "pivot":Lcom/google/android/libraries/bind/data/Data;
    goto :goto_1

    .line 356
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public doOnCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 111
    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->editable_library_fragment:I

    invoke-virtual {p1, v2, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 112
    .local v1, "rootView":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->setRootView(Landroid/view/View;)V

    .line 113
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 114
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->setupListView()V

    .line 116
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->setHasOptionsMenu(Z)V

    .line 119
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$3;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V

    .line 137
    .local v0, "connectivityRunnable":Ljava/lang/Runnable;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 139
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$4;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "downloadedOnly"

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 146
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->updateErrorView()V

    .line 147
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->updateHeaderCards()V

    .line 148
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->refreshIfNeeded()V

    .line 150
    return-object v1
.end method

.method protected abstract getLibraryTag()Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
.end method

.method protected getOfflineEmptyRow()Lcom/google/android/libraries/bind/data/Data;
    .locals 3

    .prologue
    .line 322
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_download:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->empty_list_caption_offline:I

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeStandardEmptyCardData(Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method protected getOfflineWarmWelcomeCard()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 331
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getOnlineWarmWelcomeCard()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSourceCardList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->sourceCardList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getSubscriptionsCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->sourceCardList:Lcom/google/android/libraries/bind/data/DataList;

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->sourceCardList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method protected abstract getSubscriptionType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
.end method

.method protected abstract getSubscriptionsCardList()Lcom/google/android/libraries/bind/data/DataList;
.end method

.method protected getSubscriptionsUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 249
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getSubscriptionType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMySubscriptions(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected isOfflineCardVisible()Z
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    return v0
.end method

.method protected isOnlineCardVisible()Z
    .locals 1

    .prologue
    .line 318
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 155
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 156
    sget v0, Lcom/google/android/apps/newsstanddev/R$menu;->editable_library_fragment_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 157
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 435
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->onDestroyView()V

    .line 436
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->connectivityListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 437
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    if-eqz v0, :cond_1

    .line 440
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 442
    :cond_1
    return-void
.end method

.method onNewsOperationDisallowed(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
    .locals 6
    .param p1, "groupList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "operation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .prologue
    .line 394
    iget v4, p2, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 395
    iget-object v4, p2, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    .line 396
    .local v0, "currentPosition":I
    const/4 v4, -0x1

    if-ne v0, v4, :cond_1

    .line 414
    .end local v0    # "currentPosition":I
    :cond_0
    :goto_0
    return-void

    .line 399
    .restart local v0    # "currentPosition":I
    :cond_1
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 400
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    invoke-virtual {v1, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 401
    .local v2, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->addMoreDummyEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v2, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 404
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    .line 405
    .local v3, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$10;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$10;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 161
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->menu_sync_on_device:I

    if-ne v0, v1, :cond_0

    .line 162
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->refreshSubscriptionCollectionAndStartSync(Ljava/lang/Runnable;)V

    .line 163
    const/4 v0, 0x1

    .line 165
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 451
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->onResume()V

    .line 452
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->getMsSincePause()J

    move-result-wide v0

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 453
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->refreshIfNeeded()V

    .line 455
    :cond_0
    return-void
.end method

.method refreshSubscriptionCollectionAndStartSync(Ljava/lang/Runnable;)V
    .locals 5
    .param p1, "onRefreshCompleted"    # Ljava/lang/Runnable;

    .prologue
    .line 417
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    .line 418
    .local v2, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    iget-object v3, v2, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getSubscriptionsUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 419
    .local v0, "mySubscriptionsUri":Ljava/lang/String;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v3, v0, v4}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 420
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->freshVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v1

    .line 421
    .local v1, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$11;

    invoke-direct {v4, p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$11;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;Ljava/lang/Runnable;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 431
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 0
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 343
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->setUserVisibleHint(Z)V

    .line 344
    if-eqz p1, :cond_0

    .line 345
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->refreshIfNeeded()V

    .line 347
    :cond_0
    return-void
.end method
