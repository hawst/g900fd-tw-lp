.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$12;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "MagazineEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateTabTitle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 661
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 6
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 664
    if-eqz p1, :cond_0

    .line 666
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$600(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 667
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->title(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 666
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setSingleTabTitle(Ljava/lang/CharSequence;)V

    .line 669
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$600(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->magazine_issue_name_format:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 670
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 671
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->title(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 669
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setSingleTabContentDescription(Ljava/lang/CharSequence;)V

    .line 673
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 661
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$12;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
