.class Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;
.super Ljava/lang/Object;
.source "NSFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InstanceStats"
.end annotation


# instance fields
.field private final clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;",
            ">;"
        }
    .end annotation
.end field

.field public final maxAllowedResumed:I

.field public resumedCount:I


# direct methods
.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 0
    .param p2, "maxAllowedResumed"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 272
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->clazz:Ljava/lang/Class;

    .line 274
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->maxAllowedResumed:I

    .line 275
    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->resumedCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->resumedCount:I

    .line 295
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->resumedCount:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    .line 296
    return-void

    .line 295
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 278
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->resumedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->resumedCount:I

    .line 279
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isStrictModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->resumedCount:I

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->maxAllowedResumed:I

    if-le v0, v1, :cond_0

    .line 286
    # getter for: Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Too many instance of fragment %s detected, was %d, max is %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->clazz:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->resumedCount:I

    .line 287
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->maxAllowedResumed:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 286
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291
    :cond_0
    return-void
.end method
