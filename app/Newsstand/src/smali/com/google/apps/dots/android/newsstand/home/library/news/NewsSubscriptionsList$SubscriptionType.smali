.class public final enum Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;
.super Ljava/lang/Enum;
.source "NewsSubscriptionsList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SubscriptionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

.field public static final enum FREE:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

.field public static final enum NOT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

.field public static final enum PAID:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    const-string v1, "NOT_SUBSCRIBED"

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->NOT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    .line 57
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    const-string v1, "FREE"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->FREE:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    .line 58
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    const-string v1, "PAID"

    invoke-direct {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->PAID:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    .line 55
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->NOT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->FREE:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->PAID:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 55
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    return-object v0
.end method
