.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;
.super Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAttachmentBlob(Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

.field final synthetic val$attachmentId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p2, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .param p3, "debugId"    # Ljava/lang/String;
    .param p4, "debugOwner"    # Ljava/lang/Object;

    .prologue
    .line 1029
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->val$attachmentId:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public callInternal()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1034
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v2

    .line 1036
    .local v2, "mgr":Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1037
    new-instance v4, Lcom/google/apps/dots/android/newsstand/sync/OfflineSyncException;

    const-string v5, "not connected"

    invoke-direct {v4, v5}, Lcom/google/apps/dots/android/newsstand/sync/OfflineSyncException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1040
    :cond_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v6

    iget-boolean v6, v6, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->userRequested:Z

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .line 1041
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v7

    iget-boolean v7, v7, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->wifiOnlyDownloadOverride:Z

    .line 1040
    invoke-virtual {v2, v6, v7}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->getApplicableSyncPolicy(ZZ)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    move-result-object v3

    .line 1043
    .local v3, "policy":Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    sget-object v7, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->NEWS_PRIMARY_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    if-ne v6, v7, :cond_1

    .line 1044
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->allowsNewsPrimaryImages()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    sget-object v7, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->NEWS_OTHER_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    if-ne v6, v7, :cond_4

    .line 1045
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->allowsNewsAllImages()Z

    move-result v6

    if-nez v6, :cond_4

    :cond_2
    move v1, v4

    .line 1047
    .local v1, "isDisallowedNewsImage":Z
    :goto_0
    if-nez v1, :cond_5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    sget-object v7, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->MAGAZINE_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    if-ne v6, v7, :cond_5

    .line 1048
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->allowsMagazines()Z

    move-result v6

    if-nez v6, :cond_5

    move v0, v4

    .line 1051
    .local v0, "isDisallowedMagsImage":Z
    :goto_1
    if-nez v1, :cond_3

    if-eqz v0, :cond_6

    .line 1057
    :cond_3
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .line 1058
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->attachmentStore:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$3900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2800(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->val$attachmentId:Ljava/lang/String;

    sget-object v8, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->cacheRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-static {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$3800(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20$1;

    invoke-direct {v5, p0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20$1;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;Z)V

    .line 1057
    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1081
    :goto_2
    return-object v4

    .end local v0    # "isDisallowedMagsImage":Z
    .end local v1    # "isDisallowedNewsImage":Z
    :cond_4
    move v1, v5

    .line 1045
    goto :goto_0

    .restart local v1    # "isDisallowedNewsImage":Z
    :cond_5
    move v0, v5

    .line 1048
    goto :goto_1

    .line 1080
    .restart local v0    # "isDisallowedMagsImage":Z
    :cond_6
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v6

    const-string v7, "%s: syncing attachment"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v5

    invoke-virtual {v6, v7, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1081
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->attachmentStore:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$3900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2800(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$20;->val$attachmentId:Ljava/lang/String;

    sget-object v8, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 1082
    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->storeRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-static {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1200(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v6

    .line 1081
    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    goto :goto_2
.end method
