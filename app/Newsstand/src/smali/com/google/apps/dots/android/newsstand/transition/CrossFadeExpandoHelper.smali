.class public Lcom/google/apps/dots/android/newsstand/transition/CrossFadeExpandoHelper;
.super Ljava/lang/Object;
.source "CrossFadeExpandoHelper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# direct methods
.method public static sharedElementEnterTransition(Landroid/view/View;IILandroid/view/View;Z)Landroid/transition/Transition;
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "inset"    # I
    .param p2, "fadeToColor"    # I
    .param p3, "fadeOutView"    # Landroid/view/View;
    .param p4, "isEntering"    # Z

    .prologue
    .line 22
    new-instance v3, Lcom/google/android/play/transition/Scale;

    invoke-direct {v3, p4}, Lcom/google/android/play/transition/Scale;-><init>(Z)V

    .line 23
    invoke-virtual {v3, p1}, Lcom/google/android/play/transition/Scale;->setOriginatingViewInset(I)Lcom/google/android/play/transition/Scale;

    move-result-object v3

    if-eqz p4, :cond_0

    const-wide/16 v4, 0x0

    .line 24
    :goto_0
    invoke-virtual {v3, v4, v5}, Lcom/google/android/play/transition/Scale;->setStartDelay(J)Landroid/transition/Transition;

    move-result-object v3

    .line 25
    invoke-virtual {v3, p0}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    .line 26
    .local v1, "scale":Landroid/transition/Transition;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;

    invoke-direct {v3, p4}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;-><init>(Z)V

    .line 27
    invoke-virtual {v3, p2}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->setFadeToColor(I)Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;

    move-result-object v3

    .line 28
    invoke-virtual {v3, p1}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->setInset(I)Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;

    move-result-object v3

    .line 29
    invoke-virtual {v3, p3}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->setFadeOutView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;

    move-result-object v3

    .line 30
    invoke-virtual {v3, p0}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v3

    .line 31
    invoke-virtual {v3, p3}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    .line 32
    .local v0, "fadeOut":Landroid/transition/Transition;
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/transition/Transition;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v3}, Lcom/google/android/play/transition/PlayTransitionUtil;->aggregate([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v2

    .line 33
    .local v2, "silhouette":Landroid/transition/Transition;
    new-instance v3, Landroid/transition/ArcMotion;

    invoke-direct {v3}, Landroid/transition/ArcMotion;-><init>()V

    invoke-virtual {v2, v3}, Landroid/transition/Transition;->setPathMotion(Landroid/transition/PathMotion;)V

    .line 34
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/transition/Transition;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 35
    return-object v2

    .line 23
    .end local v0    # "fadeOut":Landroid/transition/Transition;
    .end local v1    # "scale":Landroid/transition/Transition;
    .end local v2    # "silhouette":Landroid/transition/Transition;
    :cond_0
    const-wide/16 v4, 0x64

    goto :goto_0
.end method

.method public static sharedElementEnterTransition(Landroid/view/View;IZ)Landroid/transition/Transition;
    .locals 5
    .param p0, "view"    # Landroid/view/View;
    .param p1, "inset"    # I
    .param p2, "isEntering"    # Z

    .prologue
    .line 40
    new-instance v3, Lcom/google/android/play/transition/Scale;

    invoke-direct {v3, p2}, Lcom/google/android/play/transition/Scale;-><init>(Z)V

    .line 41
    invoke-virtual {v3, p1}, Lcom/google/android/play/transition/Scale;->setOriginatingViewInset(I)Lcom/google/android/play/transition/Scale;

    move-result-object v3

    .line 42
    invoke-virtual {v3, p0}, Lcom/google/android/play/transition/Scale;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    .line 43
    .local v1, "scale":Landroid/transition/Transition;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;

    invoke-direct {v3, p2}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;-><init>(Z)V

    .line 44
    invoke-virtual {v3, p0}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    .line 45
    .local v0, "fadeOut":Landroid/transition/Transition;
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/transition/Transition;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v3}, Lcom/google/android/play/transition/PlayTransitionUtil;->aggregate([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v2

    .line 46
    .local v2, "silhouette":Landroid/transition/Transition;
    new-instance v3, Landroid/transition/ArcMotion;

    invoke-direct {v3}, Landroid/transition/ArcMotion;-><init>()V

    invoke-virtual {v2, v3}, Landroid/transition/Transition;->setPathMotion(Landroid/transition/PathMotion;)V

    .line 47
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutLinearIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/transition/Transition;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 48
    return-object v2
.end method
