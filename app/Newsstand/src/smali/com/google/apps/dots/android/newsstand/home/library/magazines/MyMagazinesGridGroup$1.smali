.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "MyMagazinesGridGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->createShelfHeader(Ljava/lang/String;III)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 120
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->year:I
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->access$000(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)I

    move-result v0

    if-lez v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    move-object v1, p2

    check-cast v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    .line 122
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->year:I
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->access$000(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)I

    move-result v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->month:I
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->access$100(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)I

    move-result v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->endMonth:I
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->access$200(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)I

    move-result v5

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->areIssuesArchived:Z
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->access$300(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)Z

    move-result v8

    if-nez v8, :cond_0

    :goto_0
    move-object v2, p1

    .line 121
    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->createIntentBuilder(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/view/View;IIIZ)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    invoke-static/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->access$400(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/view/View;IIIZ)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v7

    .line 127
    .end local p2    # "activity":Landroid/app/Activity;
    .local v7, "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    :goto_1
    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->start()V

    .line 128
    return-void

    .end local v7    # "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    .restart local p2    # "activity":Landroid/app/Activity;
    :cond_0
    move v6, v2

    .line 122
    goto :goto_0

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .end local p2    # "activity":Landroid/app/Activity;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    .line 125
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->appFamilyId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->access$500(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->areIssuesArchived:Z
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->access$300(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 124
    :goto_2
    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->createIntentBuilder(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/view/View;Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    invoke-static {v0, p2, p1, v1, v6}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->access$600(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/view/View;Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v7

    .restart local v7    # "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    goto :goto_1

    .end local v7    # "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    :cond_2
    move v6, v2

    .line 125
    goto :goto_2
.end method
