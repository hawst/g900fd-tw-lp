.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/RecentMagazinesDataList;
.super Lcom/google/android/libraries/bind/data/FilteredDataList;
.source "RecentMagazinesDataList.java"


# instance fields
.field private final pinnedList:Lcom/google/android/libraries/bind/data/DataList;

.field private final pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private final recentlyReadList:Lcom/google/android/libraries/bind/data/DataList;

.field private final recentlyReadListObserver:Lcom/google/android/libraries/bind/data/DataObserver;


# virtual methods
.method protected onRegisterForInvalidation()V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/FilteredDataList;->onRegisterForInvalidation()V

    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/RecentMagazinesDataList;->pinnedList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/RecentMagazinesDataList;->pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/RecentMagazinesDataList;->recentlyReadList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/RecentMagazinesDataList;->recentlyReadListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 52
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/FilteredDataList;->onUnregisterForInvalidation()V

    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/RecentMagazinesDataList;->pinnedList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/RecentMagazinesDataList;->pinnedListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 58
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/RecentMagazinesDataList;->recentlyReadList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/RecentMagazinesDataList;->recentlyReadListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 59
    return-void
.end method
