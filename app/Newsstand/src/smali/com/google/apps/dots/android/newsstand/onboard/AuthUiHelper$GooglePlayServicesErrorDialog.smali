.class public Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;
.super Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;
.source "AuthUiHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GooglePlayServicesErrorDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 2

    .prologue
    .line 573
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "errorCode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 578
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;->getErrorCode()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getErrorDialog(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public setErrorCode(I)V
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 564
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 565
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 566
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "args":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 568
    .restart local v0    # "args":Landroid/os/Bundle;
    :cond_0
    const-string v1, "errorCode"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 569
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$GooglePlayServicesErrorDialog;->setArguments(Landroid/os/Bundle;)V

    .line 570
    return-void
.end method
