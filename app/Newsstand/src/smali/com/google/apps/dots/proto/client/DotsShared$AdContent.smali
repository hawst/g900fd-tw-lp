.class public final Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AdContent"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AdContent;


# instance fields
.field private adSystem_:I

.field private adTemplateId_:Ljava/lang/String;

.field private bitField0_:I

.field private houseAdsEnabled_:Z

.field private isReady_:Z

.field private phoneTemplate_:Ljava/lang/String;

.field public phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

.field private publisherId_:Ljava/lang/String;

.field private tabletTemplate_:Ljava/lang/String;

.field public tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3066
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3067
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3072
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adSystem_:I

    .line 3091
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .line 3094
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .line 3097
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->publisherId_:Ljava/lang/String;

    .line 3119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->isReady_:Z

    .line 3138
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneTemplate_:Ljava/lang/String;

    .line 3160
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletTemplate_:Ljava/lang/String;

    .line 3182
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adTemplateId_:Ljava/lang/String;

    .line 3204
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->houseAdsEnabled_:Z

    .line 3067
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    .locals 5

    .prologue
    .line 3241
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3245
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 3246
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .line 3247
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 3248
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 3249
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    move-result-object v4

    aput-object v4, v3, v2

    .line 3247
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3242
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 3243
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 3253
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 3254
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .line 3255
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v3, v3

    if-ge v2, v3, :cond_3

    .line 3256
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v3, v3, v2

    if-eqz v3, :cond_2

    .line 3257
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    move-result-object v4

    aput-object v4, v3, v2

    .line 3255
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3261
    .end local v2    # "i":I
    :cond_3
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3063
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3266
    if-ne p1, p0, :cond_1

    .line 3276
    :cond_0
    :goto_0
    return v1

    .line 3267
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 3268
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    .line 3269
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adSystem_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adSystem_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .line 3270
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .line 3271
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->publisherId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->publisherId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 3272
    :goto_1
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->isReady_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->isReady_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneTemplate_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneTemplate_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 3274
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletTemplate_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletTemplate_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 3275
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adTemplateId_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adTemplateId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 3276
    :goto_4
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->houseAdsEnabled_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->houseAdsEnabled_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 3271
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->publisherId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->publisherId_:Ljava/lang/String;

    .line 3272
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneTemplate_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneTemplate_:Ljava/lang/String;

    .line 3274
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletTemplate_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletTemplate_:Ljava/lang/String;

    .line 3275
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adTemplateId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adTemplateId_:Ljava/lang/String;

    .line 3276
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4
.end method

.method public getAdSystem()I
    .locals 1

    .prologue
    .line 3074
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adSystem_:I

    return v0
.end method

.method public getAdTemplateId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3184
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adTemplateId_:Ljava/lang/String;

    return-object v0
.end method

.method public getHouseAdsEnabled()Z
    .locals 1

    .prologue
    .line 3206
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->houseAdsEnabled_:Z

    return v0
.end method

.method public getIsReady()Z
    .locals 1

    .prologue
    .line 3121
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->isReady_:Z

    return v0
.end method

.method public getPhoneTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3140
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneTemplate_:Ljava/lang/String;

    return-object v0
.end method

.method public getPublisherId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3099
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->publisherId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 3346
    const/4 v1, 0x0

    .line 3347
    .local v1, "size":I
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-eqz v3, :cond_1

    .line 3348
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v0, v4, v3

    .line 3349
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    if-eqz v0, :cond_0

    .line 3350
    const/4 v6, 0x1

    .line 3351
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 3348
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3355
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-eqz v3, :cond_3

    .line 3356
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v4, v3

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 3357
    .restart local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    if-eqz v0, :cond_2

    .line 3358
    const/4 v5, 0x2

    .line 3359
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3356
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3363
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_4

    .line 3364
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->isReady_:Z

    .line 3365
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3367
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_5

    .line 3368
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneTemplate_:Ljava/lang/String;

    .line 3369
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3371
    :cond_5
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_6

    .line 3372
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletTemplate_:Ljava/lang/String;

    .line 3373
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3375
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_7

    .line 3376
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->publisherId_:Ljava/lang/String;

    .line 3377
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3379
    :cond_7
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_8

    .line 3380
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adTemplateId_:Ljava/lang/String;

    .line 3381
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3383
    :cond_8
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_9

    .line 3384
    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->houseAdsEnabled_:Z

    .line 3385
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3387
    :cond_9
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_a

    .line 3388
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adSystem_:I

    .line 3389
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 3391
    :cond_a
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->cachedSize:I

    .line 3392
    return v1
.end method

.method public getTabletTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3162
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletTemplate_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAdTemplateId()Z
    .locals 1

    .prologue
    .line 3195
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPhoneTemplate()Z
    .locals 1

    .prologue
    .line 3151
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPublisherId()Z
    .locals 1

    .prologue
    .line 3110
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTabletTemplate()Z
    .locals 1

    .prologue
    .line 3173
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3281
    const/16 v1, 0x11

    .line 3282
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 3283
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adSystem_:I

    add-int v1, v2, v6

    .line 3284
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-nez v2, :cond_2

    mul-int/lit8 v1, v1, 0x1f

    .line 3290
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-nez v2, :cond_4

    mul-int/lit8 v1, v1, 0x1f

    .line 3296
    :cond_1
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->publisherId_:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 3297
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->isReady_:Z

    if-eqz v2, :cond_7

    move v2, v4

    :goto_1
    add-int v1, v6, v2

    .line 3298
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneTemplate_:Ljava/lang/String;

    if-nez v2, :cond_8

    move v2, v3

    :goto_2
    add-int v1, v6, v2

    .line 3299
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletTemplate_:Ljava/lang/String;

    if-nez v2, :cond_9

    move v2, v3

    :goto_3
    add-int v1, v6, v2

    .line 3300
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adTemplateId_:Ljava/lang/String;

    if-nez v6, :cond_a

    :goto_4
    add-int v1, v2, v3

    .line 3301
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->houseAdsEnabled_:Z

    if-eqz v3, :cond_b

    :goto_5
    add-int v1, v2, v4

    .line 3302
    return v1

    .line 3286
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 3287
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v3

    :goto_7
    add-int v1, v6, v2

    .line 3286
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 3287
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->hashCode()I

    move-result v2

    goto :goto_7

    .line 3292
    .end local v0    # "i":I
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 3293
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v3

    :goto_9
    add-int v1, v6, v2

    .line 3292
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 3293
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->hashCode()I

    move-result v2

    goto :goto_9

    .line 3296
    .end local v0    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->publisherId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_7
    move v2, v5

    .line 3297
    goto :goto_1

    .line 3298
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneTemplate_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 3299
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletTemplate_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    .line 3300
    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adTemplateId_:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_4

    :cond_b
    move v4, v5

    .line 3301
    goto :goto_5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 3400
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3401
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3405
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3406
    :sswitch_0
    return-object p0

    .line 3411
    :sswitch_1
    const/16 v6, 0xa

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3412
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-nez v6, :cond_2

    move v1, v5

    .line 3413
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .line 3414
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-eqz v6, :cond_1

    .line 3415
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3417
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .line 3418
    :goto_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 3419
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;-><init>()V

    aput-object v7, v6, v1

    .line 3420
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3421
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3418
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3412
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :cond_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v1, v6

    goto :goto_1

    .line 3424
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :cond_3
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;-><init>()V

    aput-object v7, v6, v1

    .line 3425
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3429
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :sswitch_2
    const/16 v6, 0x12

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3430
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-nez v6, :cond_5

    move v1, v5

    .line 3431
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .line 3432
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-eqz v6, :cond_4

    .line 3433
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3435
    :cond_4
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .line 3436
    :goto_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_6

    .line 3437
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;-><init>()V

    aput-object v7, v6, v1

    .line 3438
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3439
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3436
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 3430
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :cond_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v1, v6

    goto :goto_3

    .line 3442
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :cond_6
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;-><init>()V

    aput-object v7, v6, v1

    .line 3443
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3447
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->isReady_:Z

    .line 3448
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    goto/16 :goto_0

    .line 3452
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneTemplate_:Ljava/lang/String;

    .line 3453
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    goto/16 :goto_0

    .line 3457
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletTemplate_:Ljava/lang/String;

    .line 3458
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    goto/16 :goto_0

    .line 3462
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->publisherId_:Ljava/lang/String;

    .line 3463
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    goto/16 :goto_0

    .line 3467
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adTemplateId_:Ljava/lang/String;

    .line 3468
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    goto/16 :goto_0

    .line 3472
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->houseAdsEnabled_:Z

    .line 3473
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    goto/16 :goto_0

    .line 3477
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3478
    .local v4, "temp":I
    if-eqz v4, :cond_7

    const/4 v6, 0x1

    if-eq v4, v6, :cond_7

    const/4 v6, 0x2

    if-eq v4, v6, :cond_7

    const/4 v6, 0x3

    if-eq v4, v6, :cond_7

    const/4 v6, 0x4

    if-eq v4, v6, :cond_7

    const/4 v6, 0x5

    if-ne v4, v6, :cond_8

    .line 3484
    :cond_7
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adSystem_:I

    .line 3485
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    goto/16 :goto_0

    .line 3487
    :cond_8
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adSystem_:I

    goto/16 :goto_0

    .line 3401
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3063
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3307
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-eqz v2, :cond_1

    .line 3308
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 3309
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    if-eqz v0, :cond_0

    .line 3310
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3308
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3314
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-eqz v2, :cond_3

    .line 3315
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v0, v2, v1

    .line 3316
    .restart local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    if-eqz v0, :cond_2

    .line 3317
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3315
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3321
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_4

    .line 3322
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->isReady_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3324
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_5

    .line 3325
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneTemplate_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3327
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_6

    .line 3328
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletTemplate_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3330
    :cond_6
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_7

    .line 3331
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->publisherId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3333
    :cond_7
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_8

    .line 3334
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adTemplateId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3336
    :cond_8
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_9

    .line 3337
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->houseAdsEnabled_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3339
    :cond_9
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_a

    .line 3340
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->adSystem_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3342
    :cond_a
    return-void
.end method
