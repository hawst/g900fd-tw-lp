.class public abstract Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;
.super Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;
.source "NSHeroReloadoDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;",
        ">",
        "Lcom/google/android/play/transition/delegate/HeroReloadoDelegate",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private animationStateCounter:Lcom/google/android/play/animation/AnimationStateCounter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    invoke-direct {p0}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method protected cardClassTypes(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)[Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[",
            "Ljava/lang/Class;"
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/play/cardview/CardViewGroup;

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected bridge synthetic cardClassTypes(Ljava/lang/Object;)[Ljava/lang/Class;
    .locals 1

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->cardClassTypes(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)[Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method protected getControllerType()I
    .locals 1

    .prologue
    .line 124
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    const/4 v0, 0x2

    return v0
.end method

.method protected handleEnterRemapSharedElements(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    .local p2, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->handleEnterRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V

    .line 43
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getHeroElementVisible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 45
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 46
    invoke-interface {p3}, Ljava/util/Map;->clear()V

    .line 48
    new-instance v1, Landroid/transition/Explode;

    invoke-direct {v1}, Landroid/transition/Explode;-><init>()V

    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->setReturnTransition(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 55
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->tab_bar:I

    invoke-virtual {v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 53
    .local v0, "tabBar":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method protected bridge synthetic handleEnterRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->handleEnterRemapSharedElements(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/Map;)V

    return-void
.end method

.method protected handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 62
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isTransitioningToExit()Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->sharedElementView(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 63
    invoke-static {v0, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/transition/TransitionUtil;->setupTransitionForSlowLoad(Landroid/view/View;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 67
    :cond_0
    return-void
.end method

.method protected bridge synthetic handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected onReturnTransitionEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V
    .locals 1
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    invoke-super {p0, p1, p2}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->onReturnTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 111
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->animationStateCounter:Lcom/google/android/play/animation/AnimationStateCounter;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->animationStateCounter:Lcom/google/android/play/animation/AnimationStateCounter;

    invoke-virtual {v0}, Lcom/google/android/play/animation/AnimationStateCounter;->stopTracking()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->animationStateCounter:Lcom/google/android/play/animation/AnimationStateCounter;

    .line 115
    :cond_0
    return-void
.end method

.method protected bridge synthetic onReturnTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->onReturnTransitionEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected onReturnTransitionStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V
    .locals 3
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 98
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    invoke-super {p0, p1, p2}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->onReturnTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 100
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getCurrentListView()Landroid/view/ViewGroup;

    move-result-object v0

    .line 101
    .local v0, "cardsParent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 102
    new-instance v1, Lcom/google/android/play/animation/AnimationStateCounter;

    invoke-direct {v1, v0}, Lcom/google/android/play/animation/AnimationStateCounter;-><init>(Landroid/view/ViewGroup;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->animationStateCounter:Lcom/google/android/play/animation/AnimationStateCounter;

    .line 103
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->animationStateCounter:Lcom/google/android/play/animation/AnimationStateCounter;

    invoke-virtual {v1}, Lcom/google/android/play/animation/AnimationStateCounter;->startTracking()V

    .line 105
    :cond_0
    return-void
.end method

.method protected bridge synthetic onReturnTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->onReturnTransitionStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected onSharedElementEnterTransitionEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V
    .locals 3
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    invoke-super {p0, p1, p2}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->onSharedElementEnterTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 78
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/play/transition/PlayTransitionUtil;->setHeaderListLayoutClipChildren(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)V

    .line 79
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->tab_bar:I

    invoke-virtual {v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 80
    .local v0, "tabBar":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 81
    return-void
.end method

.method protected bridge synthetic onSharedElementEnterTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->onSharedElementEnterTransitionEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected onSharedElementEnterTransitionStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V
    .locals 2
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    invoke-super {p0, p1, p2}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->onSharedElementEnterTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 72
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/play/transition/PlayTransitionUtil;->setHeaderListLayoutClipChildren(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)V

    .line 73
    return-void
.end method

.method protected bridge synthetic onSharedElementEnterTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->onSharedElementEnterTransitionStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected onSharedElementReturnTransitionEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V
    .locals 0
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    invoke-super {p0, p1, p2}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->onSharedElementReturnTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 94
    return-void
.end method

.method protected bridge synthetic onSharedElementReturnTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->onSharedElementReturnTransitionEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected onSharedElementReturnTransitionStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V
    .locals 2
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    invoke-super {p0, p1, p2}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->onSharedElementReturnTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 86
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/play/transition/PlayTransitionUtil;->setHeaderListLayoutClipChildren(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)V

    .line 87
    return-void
.end method

.method protected bridge synthetic onSharedElementReturnTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->onSharedElementReturnTransitionStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    invoke-super {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->prepareViewsForTransition(Ljava/lang/Object;)V

    .line 32
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSharedElementsUseOverlay(Z)V

    .line 33
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->sharedElementView(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 34
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->expando_hero:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 35
    sget-object v0, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/async/JankLock;->pauseTemporarily(J)V

    .line 36
    return-void
.end method

.method protected bridge synthetic prepareViewsForTransition(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    return-void
.end method
