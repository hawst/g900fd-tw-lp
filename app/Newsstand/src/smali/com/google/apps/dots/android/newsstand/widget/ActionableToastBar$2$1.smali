.class Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2$1;
.super Landroid/view/View$AccessibilityDelegate;
.source "ActionableToastBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "host"    # Landroid/view/ViewGroup;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 102
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->ACCESSIBILITY_ACTIONS_TO_HIDE_BAR:Ljava/util/Set;

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;->val$contentView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 104
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->currentlyHidden:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->access$000(Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->hide(Z)V

    .line 108
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method
