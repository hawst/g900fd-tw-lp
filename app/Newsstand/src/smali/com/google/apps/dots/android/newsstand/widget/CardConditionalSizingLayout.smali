.class public Lcom/google/apps/dots/android/newsstand/widget/CardConditionalSizingLayout;
.super Lcom/google/apps/dots/android/newsstand/widget/ConditionalSizingLayout;
.source "CardConditionalSizingLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CardConditionalSizingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/CardConditionalSizingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/ConditionalSizingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    return-void
.end method


# virtual methods
.method protected checkCondition()Z
    .locals 2

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/CardConditionalSizingLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 27
    .local v0, "parent":Landroid/view/ViewParent;
    :cond_0
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/widget/ConditionalSizeViewGroup;

    if-nez v1, :cond_1

    .line 28
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 29
    if-nez v0, :cond_0

    .line 30
    const/4 v1, 0x0

    .line 33
    :goto_0
    return v1

    :cond_1
    move-object v1, v0

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/ConditionalSizeViewGroup;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/widget/ConditionalSizeViewGroup;->shouldEnlarge()Z

    move-result v1

    goto :goto_0
.end method
