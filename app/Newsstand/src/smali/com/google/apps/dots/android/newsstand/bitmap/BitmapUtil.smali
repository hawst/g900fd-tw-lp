.class public Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;
.super Ljava/lang/Object;
.source "BitmapUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method public static bytesPerPixel(Landroid/graphics/Bitmap$Config;)I
    .locals 3
    .param p0, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    const/4 v0, 0x2

    .line 42
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil$1;->$SwitchMap$android$graphics$Bitmap$Config:[I

    invoke-virtual {p0}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 44
    :pswitch_0
    const/4 v0, 0x4

    .line 50
    :goto_0
    :pswitch_1
    return v0

    .line 48
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static getBitmapInfo(Landroid/content/res/AssetFileDescriptor;)Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    .locals 1
    .param p0, "afd"    # Landroid/content/res/AssetFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    :try_start_0
    invoke-virtual {p0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;->getBitmapInfo(Ljava/io/InputStream;)Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 76
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V

    throw v0
.end method

.method public static getBitmapInfo(Ljava/io/InputStream;)Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    .locals 15
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v14, 0x19

    const/16 v13, 0x18

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 84
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 85
    .local v3, "boundsOnly":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v9, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 86
    const/16 v11, 0x1a

    new-array v6, v11, [B

    .line 90
    .local v6, "header":[B
    array-length v11, v6

    invoke-static {p0, v6, v10, v11}, Lcom/google/common/io/ByteStreams;->read(Ljava/io/InputStream;[BII)I

    move-result v7

    .line 91
    .local v7, "headerLength":I
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v6, v10, v7}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 92
    .local v1, "bis":Ljava/io/ByteArrayInputStream;
    new-instance v8, Ljava/io/SequenceInputStream;

    invoke-direct {v8, v1, p0}, Ljava/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    .line 93
    .end local p0    # "in":Ljava/io/InputStream;
    .local v8, "in":Ljava/io/InputStream;
    const/4 v11, 0x0

    invoke-static {v8, v11, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 96
    const/4 v5, 0x1

    .line 97
    .local v5, "hasAlpha":Z
    const-string v11, "image/jpeg"

    iget-object v12, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 98
    const/4 v5, 0x0

    .line 126
    :cond_0
    :goto_0
    new-instance v9, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    iget v10, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v11, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget-object v12, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-direct {v9, v10, v11, v5, v12}, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;-><init>(IIZLjava/lang/String;)V

    return-object v9

    .line 99
    :cond_1
    const-string v11, "image/gif"

    iget-object v12, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 100
    const/4 v5, 0x1

    goto :goto_0

    .line 101
    :cond_2
    const-string v11, "image/png"

    iget-object v12, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 104
    if-ge v14, v7, :cond_0

    .line 105
    aget-byte v2, v6, v13

    .line 106
    .local v2, "bitDepth":I
    aget-byte v4, v6, v14

    .line 107
    .local v4, "colorType":I
    sget-object v11, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v12, "PNG bitDepth[%s] colorType[%s]"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v13, v10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v13, v9

    invoke-virtual {v11, v12, v13}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    and-int/lit8 v11, v4, 0x4

    if-eqz v11, :cond_3

    move v5, v9

    .line 109
    :goto_1
    goto :goto_0

    :cond_3
    move v5, v10

    .line 108
    goto :goto_1

    .line 110
    .end local v2    # "bitDepth":I
    .end local v4    # "colorType":I
    :cond_4
    iget-object v11, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    if-eqz v11, :cond_5

    const-string v11, "image/webp"

    iget-object v12, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    .line 111
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 113
    :cond_5
    if-ge v13, v7, :cond_0

    .line 115
    const/16 v11, 0x8

    aget-byte v11, v6, v11

    const/16 v12, 0x57

    if-ne v11, v12, :cond_0

    const/16 v11, 0x9

    aget-byte v11, v6, v11

    const/16 v12, 0x45

    if-ne v11, v12, :cond_0

    const/16 v11, 0xa

    aget-byte v11, v6, v11

    const/16 v12, 0x42

    if-ne v11, v12, :cond_0

    const/16 v11, 0xb

    aget-byte v11, v6, v11

    const/16 v12, 0x50

    if-ne v11, v12, :cond_0

    .line 120
    const-string v11, "image/webp"

    iput-object v11, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    .line 121
    aget-byte v0, v6, v13

    .line 122
    .local v0, "alphaByte":I
    const/high16 v11, 0x10000

    and-int/2addr v11, v0

    if-eqz v11, :cond_6

    move v5, v9

    :goto_2
    goto/16 :goto_0

    :cond_6
    move v5, v10

    goto :goto_2
.end method

.method public static getBitmapSizeBytes(Landroid/graphics/Bitmap;)I
    .locals 2
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 58
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 61
    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getAllocationByteCount()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 66
    :goto_0
    return v0

    .line 62
    :catch_0
    move-exception v0

    .line 66
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v1

    mul-int/2addr v0, v1

    goto :goto_0
.end method
