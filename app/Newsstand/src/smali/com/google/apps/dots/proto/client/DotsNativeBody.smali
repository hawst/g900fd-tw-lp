.class public interface abstract Lcom/google/apps/dots/proto/client/DotsNativeBody;
.super Ljava/lang/Object;
.source "DotsNativeBody.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$TimerDetails;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$PdfDetails;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$SwitchDetails;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$TouchDetails;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$ImageDetails;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;,
        Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    }
.end annotation
