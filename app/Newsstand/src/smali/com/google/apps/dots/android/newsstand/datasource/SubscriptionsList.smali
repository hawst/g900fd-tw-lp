.class public abstract Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;
.super Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
.source "SubscriptionsList.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;


# static fields
.field private static final APP_FAMILY_LIST_EQUALITY_FIELDS:[I

.field private static final EQUALITY_FIELDS:[I

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    const-class v0, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 33
    const/4 v0, 0x4

    new-array v0, v0, [I

    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_UPDATED:I

    aput v1, v0, v3

    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_ARCHIVED:I

    aput v1, v0, v4

    const/4 v1, 0x2

    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_PURCHASED:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_NUM_ISSUES:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;->EQUALITY_FIELDS:[I

    .line 39
    new-array v0, v4, [I

    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_UPDATED:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;->APP_FAMILY_LIST_EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;->DK_EDITION:I

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;-><init>(I)V

    .line 43
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method


# virtual methods
.method protected equalityFields()[I
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;->EQUALITY_FIELDS:[I

    return-object v0
.end method

.method protected abstract makeEdition(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
.end method

.method protected processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
    .locals 2
    .param p1, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "rootNode"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList$3;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;->primaryKey()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList$3;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;I)V

    .line 189
    .local v0, "visitor":Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;

    invoke-direct {v1, p2, p3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 190
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->getResults()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method protected shouldStorePermanently()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    return v0
.end method
