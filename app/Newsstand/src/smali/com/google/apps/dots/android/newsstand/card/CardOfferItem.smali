.class public Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;
.super Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;
.source "CardOfferItem.java"


# static fields
.field public static final DK_DESCRIPTION:I

.field public static final DK_EXPIRATION_TIME:I

.field public static final DK_FOOTER_BACKGROUND_COLOR:I

.field public static final DK_NEGATIVE_ICON:I

.field public static final DK_NEGATIVE_ON_CLICK_LISTENER:I

.field public static final DK_NEGATIVE_TEXT:I

.field public static final DK_POSITIVE_ICON:I

.field public static final DK_POSITIVE_ON_CLICK_LISTENER:I

.field public static final DK_POSITIVE_TEXT:I

.field public static final DK_SOURCE_ASPECT_RATIO:I

.field public static final DK_SOURCE_ICON_ID:I

.field public static final DK_SOURCE_ICON_SIZE:I

.field public static final DK_SPLASH_ATTACHMENT_ID:I

.field public static final DK_SPLASH_ON_CLICK_LISTENER:I

.field public static final DK_SPLASH_SUBTITLE:I

.field public static final DK_SPLASH_TITLE:I

.field public static final DK_TITLE:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I

.field private static final LAYOUT_MAGAZINE:I

.field private static final LAYOUT_MAGAZINE_COMPACT:I

.field private static final LAYOUT_MAGAZINE_HORIZONTAL:I

.field private static final LAYOUT_NORMAL:I

.field private static final LAYOUT_NORMAL_COMPACT:I

.field private static final LAYOUT_NORMAL_HORIZONTAL:I

.field private static final MAGAZINE_BACKGROUND_COLOR_RES_ID:I

.field private static final NEWS_BACKGROUND_COLOR_RES_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_splashAttachmentId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SPLASH_ATTACHMENT_ID:I

    .line 46
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_sourceIconId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SOURCE_ICON_ID:I

    .line 48
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_sourceIconSize:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SOURCE_ICON_SIZE:I

    .line 50
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_sourceAspectRatio:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SOURCE_ASPECT_RATIO:I

    .line 52
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_splashTitle:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SPLASH_TITLE:I

    .line 54
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_splashSubtitle:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SPLASH_SUBTITLE:I

    .line 56
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_TITLE:I

    .line 58
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_description:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_DESCRIPTION:I

    .line 60
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_positiveText:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_POSITIVE_TEXT:I

    .line 62
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_negativeText:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_NEGATIVE_TEXT:I

    .line 64
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_expirationTime:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_EXPIRATION_TIME:I

    .line 66
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_positiveOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_POSITIVE_ON_CLICK_LISTENER:I

    .line 69
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_negativeOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_NEGATIVE_ON_CLICK_LISTENER:I

    .line 72
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_positiveIcon:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_POSITIVE_ICON:I

    .line 74
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_negativeIcon:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_NEGATIVE_ICON:I

    .line 76
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_splashOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SPLASH_ON_CLICK_LISTENER:I

    .line 78
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardOfferItem_footerBackgroundColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_FOOTER_BACKGROUND_COLOR:I

    .line 80
    new-array v0, v2, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->EQUALITY_FIELDS:[I

    .line 83
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_NORMAL:I

    .line 84
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_compact:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_NORMAL_COMPACT:I

    .line 85
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_horizontal:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_NORMAL_HORIZONTAL:I

    .line 88
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_MAGAZINE:I

    .line 89
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine_compact:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_MAGAZINE_COMPACT:I

    .line 91
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine_horizontal:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_MAGAZINE_HORIZONTAL:I

    .line 94
    const/4 v0, 0x6

    new-array v0, v0, [I

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_NORMAL:I

    aput v1, v0, v2

    const/4 v1, 0x1

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_NORMAL_COMPACT:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_NORMAL_HORIZONTAL:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_MAGAZINE:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_MAGAZINE_COMPACT:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_MAGAZINE_HORIZONTAL:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUTS:[I

    .line 102
    sget v0, Lcom/google/android/apps/newsstanddev/R$color;->magazine_offer_color_background:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->MAGAZINE_BACKGROUND_COLOR_RES_ID:I

    .line 104
    sget v0, Lcom/google/android/apps/newsstanddev/R$color;->news_offer_color_background:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->NEWS_BACKGROUND_COLOR_RES_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 115
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 118
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 119
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 42
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->invalidateListIfNecessary(Lcom/google/android/libraries/bind/data/DataList;)V

    return-void
.end method

.method public static fillOffersCardData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;ZLjava/lang/String;)V
    .locals 23
    .param p0, "cardData"    # Lcom/google/android/libraries/bind/data/Data;
    .param p1, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p2, "listToLocallyModify"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p3, "longToastDisplayTime"    # Z
    .param p4, "analyticsReadingScreen"    # Ljava/lang/String;

    .prologue
    .line 144
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v12

    .line 146
    .local v12, "offerId":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v4

    .line 147
    .local v4, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getName()Ljava/lang/String;

    move-result-object v13

    .line 149
    .local v13, "offerTitle":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->getLayoutId(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)I

    move-result v8

    .line 150
    .local v8, "layoutId":I
    sget v19, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 152
    sget v19, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v12}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 155
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SPLASH_ATTACHMENT_ID:I

    invoke-static/range {p1 .. p1}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->getSplashAttachmentId(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 156
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SOURCE_ICON_ID:I

    .line 157
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconAttachmentId(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Ljava/lang/String;

    move-result-object v20

    .line 156
    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 159
    sget v20, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_USE_ROUNDED_SOURCE_ICON:I

    .line 160
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v19

    const/16 v21, 0x2

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_7

    const/16 v19, 0x1

    :goto_0
    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    .line 159
    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 161
    sget v19, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_ROUNDED_SOURCE_ICON_BACKGROUND:I

    .line 162
    invoke-static/range {p1 .. p1}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->getOfferBackgroundColor(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    .line 161
    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 165
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v18

    .line 166
    .local v18, "storeType":I
    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 167
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SOURCE_ICON_SIZE:I

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    sget v21, Lcom/google/android/apps/newsstanddev/R$dimen;->card_offer_item_topic_logo_size:I

    .line 168
    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    .line 167
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 174
    :cond_0
    :goto_1
    const/high16 v19, 0x3f800000    # 1.0f

    const/high16 v20, 0x3fc00000    # 1.5f

    .line 175
    move/from16 v0, v20

    invoke-static {v4, v0}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconHeightToWidthRatio(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;F)F

    move-result v20

    div-float v5, v19, v20

    .line 176
    .local v5, "aspectRatioInverse":F
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SOURCE_ASPECT_RATIO:I

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 177
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SPLASH_TITLE:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 180
    invoke-static {v13}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_1

    .line 181
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_TITLE:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v13}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 184
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getDescription()Ljava/lang/String;

    move-result-object v6

    .line 185
    .local v6, "description":Ljava/lang/String;
    invoke-static {v6}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_2

    .line 186
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_DESCRIPTION:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getDescription()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 189
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->hasAcceptVerb()Z

    move-result v19

    if-eqz v19, :cond_9

    .line 190
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAcceptVerb()Ljava/lang/String;

    move-result-object v15

    .line 195
    .local v15, "positiveText":Ljava/lang/String;
    :goto_2
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_POSITIVE_TEXT:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v15}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 198
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->hasRejectVerb()Z

    move-result v19

    if-eqz v19, :cond_a

    .line 199
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getRejectVerb()Ljava/lang/String;

    move-result-object v10

    .line 204
    .local v10, "negativeText":Ljava/lang/String;
    :goto_3
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_NEGATIVE_TEXT:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v10}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 206
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->hasExpirationDateMillis()Z

    move-result v19

    if-eqz v19, :cond_3

    .line 208
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getExpirationDateMillis()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->relativePastTimeString(J)Ljava/lang/String;

    move-result-object v16

    .line 209
    .local v16, "relativeOfferExpirationTime":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v19

    sget v20, Lcom/google/android/apps/newsstanddev/R$string;->offer_expiration_time:I

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v16, v21, v22

    invoke-virtual/range {v19 .. v21}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 211
    .local v7, "expirationMessage":Ljava/lang/String;
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_EXPIRATION_TIME:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 214
    .end local v7    # "expirationMessage":Ljava/lang/String;
    .end local v16    # "relativeOfferExpirationTime":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    move/from16 v3, p3

    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->getPositiveClickListener(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;Lcom/google/android/libraries/bind/data/DataList;Z)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;

    move-result-object v14

    .line 216
    .local v14, "positiveClickListener":Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    if-eqz v14, :cond_4

    .line 217
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_POSITIVE_ON_CLICK_LISTENER:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v14}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 221
    :cond_4
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->getNegativeClickListener(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;

    move-result-object v9

    .line 222
    .local v9, "negativeClickListener":Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    if-eqz v9, :cond_5

    .line 223
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_NEGATIVE_ON_CLICK_LISTENER:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v9}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 227
    :cond_5
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->getSplashClickListener(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;

    move-result-object v17

    .line 228
    .local v17, "splashOnClickListener":Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    if-eqz v17, :cond_6

    .line 229
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SPLASH_ON_CLICK_LISTENER:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 232
    :cond_6
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_FOOTER_BACKGROUND_COLOR:I

    invoke-static/range {p1 .. p1}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->getOfferBackgroundColor(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 237
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v19

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromSummaries(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v11

    .line 239
    .local v11, "offerEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    sget v19, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    new-instance v20, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$1;

    move-object/from16 v0, v20

    move-object/from16 v1, p4

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v11, v2}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$1;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 246
    return-void

    .line 160
    .end local v5    # "aspectRatioInverse":F
    .end local v6    # "description":Ljava/lang/String;
    .end local v9    # "negativeClickListener":Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    .end local v10    # "negativeText":Ljava/lang/String;
    .end local v11    # "offerEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v14    # "positiveClickListener":Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    .end local v15    # "positiveText":Ljava/lang/String;
    .end local v17    # "splashOnClickListener":Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    .end local v18    # "storeType":I
    :cond_7
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 169
    .restart local v18    # "storeType":I
    :cond_8
    if-nez v18, :cond_0

    .line 170
    sget v19, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SOURCE_ICON_SIZE:I

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    sget v21, Lcom/google/android/apps/newsstanddev/R$dimen;->card_offer_item_news_logo_size:I

    .line 171
    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    .line 170
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 193
    .restart local v5    # "aspectRatioInverse":F
    .restart local v6    # "description":Ljava/lang/String;
    :cond_9
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v19

    sget v20, Lcom/google/android/apps/newsstanddev/R$string;->add_to_library:I

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "positiveText":Ljava/lang/String;
    goto/16 :goto_2

    .line 202
    :cond_a
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v19

    sget v20, Lcom/google/android/apps/newsstanddev/R$string;->no_thanks:I

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "negativeText":Ljava/lang/String;
    goto/16 :goto_3
.end method

.method private static getLayoutId(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)I
    .locals 2
    .param p0, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v0

    .line 253
    .local v0, "offerStoreType":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 254
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_MAGAZINE:I

    .line 256
    :goto_0
    return v1

    :cond_0
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUT_NORMAL:I

    goto :goto_0
.end method

.method private static getNegativeClickListener(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    .locals 1
    .param p0, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p1, "listToLocallyModify"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "analyticsReadingScreen"    # Ljava/lang/String;

    .prologue
    .line 314
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$4;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$4;-><init>(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;Lcom/google/android/libraries/bind/data/DataList;)V

    return-object v0
.end method

.method private static getOfferBackgroundColor(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)I
    .locals 2
    .param p0, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 359
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v0

    .line 360
    .local v0, "offerStoreType":I
    packed-switch v0, :pswitch_data_0

    .line 370
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v1

    iget-object v1, v1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 369
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorResIdForEditionId(Ljava/lang/String;)I

    move-result v1

    :goto_0
    return v1

    .line 362
    :pswitch_0
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->MAGAZINE_BACKGROUND_COLOR_RES_ID:I

    goto :goto_0

    .line 364
    :pswitch_1
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->NEWS_BACKGROUND_COLOR_RES_ID:I

    goto :goto_0

    .line 367
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getIdToUseForColor(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Ljava/lang/String;

    move-result-object v1

    .line 366
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorResIdForEditionId(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 360
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static getOpenableEditionPositiveClickListener(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    .locals 2
    .param p0, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p1, "analyticsReadingScreen"    # Ljava/lang/String;
    .param p2, "listToLocallyModify"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 290
    .line 291
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->getEditionSummaryForOffer(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 292
    .local v0, "offerEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$3;

    invoke-direct {v1, p0, p2, p1, v0}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$3;-><init>(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-object v1
.end method

.method private static getPositiveClickListener(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;Lcom/google/android/libraries/bind/data/DataList;Z)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    .locals 2
    .param p0, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p1, "analyticsReadingScreen"    # Ljava/lang/String;
    .param p2, "listToLocallyModify"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p3, "longToastDisplayTime"    # Z

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v0

    .line 264
    .local v0, "offerStoreType":I
    packed-switch v0, :pswitch_data_0

    .line 283
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 266
    :pswitch_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$2;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$2;-><init>(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;ZLjava/lang/String;Lcom/google/android/libraries/bind/data/DataList;)V

    goto :goto_0

    .line 279
    :pswitch_1
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->getOpenableEditionPositiveClickListener(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;

    move-result-object v1

    goto :goto_0

    .line 264
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static getSplashAttachmentId(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Ljava/lang/String;
    .locals 4
    .param p0, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v0

    .line 348
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasHeroShotImage()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 349
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getHeroShotImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v2

    .line 355
    :goto_0
    return-object v2

    .line 351
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v1

    .line 352
    .local v1, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    iget-object v2, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 353
    iget-object v2, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    goto :goto_0

    .line 355
    :cond_1
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static getSplashClickListener(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    .locals 3
    .param p0, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p1, "analyticsReadingScreen"    # Ljava/lang/String;

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v1

    .line 327
    .local v1, "offerStoreType":I
    packed-switch v1, :pswitch_data_0

    .line 342
    :pswitch_0
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 331
    :pswitch_1
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->getEditionSummaryForOffer(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 332
    .local v0, "offerEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$5;

    invoke-direct {v2, p1, p0, v0}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$5;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    goto :goto_0

    .line 327
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static invalidateListIfNecessary(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 0
    .param p0, "list"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 377
    if-eqz p0, :cond_0

    .line 378
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData()V

    .line 380
    :cond_0
    return-void
.end method


# virtual methods
.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 4
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 125
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->primary_image:I

    .line 126
    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;

    .line 127
    .local v1, "imageView":Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;
    if-nez v1, :cond_0

    .line 139
    :goto_0
    return-void

    .line 131
    :cond_0
    const/4 v0, -0x1

    .line 132
    .local v0, "iconSize":I
    if-eqz p1, :cond_1

    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SOURCE_ICON_SIZE:I

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 133
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->DK_SOURCE_ICON_SIZE:I

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 135
    :cond_1
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 136
    .local v2, "params":Landroid/view/ViewGroup$LayoutParams;
    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 137
    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 138
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
