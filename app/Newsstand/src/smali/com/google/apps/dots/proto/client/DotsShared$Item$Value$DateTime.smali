.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DateTime"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;


# instance fields
.field private bitField0_:I

.field private value_:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11257
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 11258
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 11263
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->value_:J

    .line 11258
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;
    .locals 3

    .prologue
    .line 11292
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11296
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;
    return-object v0

    .line 11293
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;
    :catch_0
    move-exception v1

    .line 11294
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11254
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 11301
    if-ne p1, p0, :cond_1

    .line 11304
    :cond_0
    :goto_0
    return v1

    .line 11302
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 11303
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    .line 11304
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->value_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->value_:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 11323
    const/4 v0, 0x0

    .line 11324
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 11325
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->value_:J

    .line 11326
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 11328
    :cond_0
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->cachedSize:I

    .line 11329
    return v0
.end method

.method public getValue()J
    .locals 2

    .prologue
    .line 11265
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->value_:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 11308
    const/16 v0, 0x11

    .line 11309
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 11310
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->value_:J

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->value_:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 11311
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11337
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 11338
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 11342
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 11343
    :sswitch_0
    return-object p0

    .line 11348
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->value_:J

    .line 11349
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->bitField0_:I

    goto :goto_0

    .line 11338
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11254
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11316
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 11317
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->value_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 11319
    :cond_0
    return-void
.end method
