.class public final Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AdUnit"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;


# instance fields
.field private bitField0_:I

.field private code_:Ljava/lang/String;

.field public deviceRestrict:[I

.field private height_:I

.field private location_:I

.field public locations:[I

.field private name_:Ljava/lang/String;

.field private orientationRestrict_:I

.field public size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

.field private width_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 282
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 283
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 306
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->location_:I

    .line 325
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->orientationRestrict_:I

    .line 344
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    .line 347
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->code_:Ljava/lang/String;

    .line 369
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->name_:Ljava/lang/String;

    .line 391
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->width_:I

    .line 410
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->height_:I

    .line 429
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Size;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    .line 432
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    .line 283
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    .locals 5

    .prologue
    .line 453
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 457
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    array-length v3, v3

    if-lez v3, :cond_0

    .line 458
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    invoke-virtual {v3}, [I->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    .line 460
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 461
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Size;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    .line 462
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 463
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 464
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Size;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Size;

    move-result-object v4

    aput-object v4, v3, v2

    .line 462
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 454
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 455
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 468
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    array-length v3, v3

    if-lez v3, :cond_3

    .line 469
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    invoke-virtual {v3}, [I->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    .line 471
    :cond_3
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 279
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 476
    if-ne p1, p0, :cond_1

    .line 487
    :cond_0
    :goto_0
    return v1

    .line 477
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 478
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .line 479
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->location_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->location_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->orientationRestrict_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->orientationRestrict_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    .line 481
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->code_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->code_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 482
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->name_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->name_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 483
    :goto_2
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->width_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->width_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->height_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->height_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    .line 486
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    .line 487
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 481
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->code_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->code_:Ljava/lang/String;

    .line 482
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->name_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->name_:Ljava/lang/String;

    .line 483
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2
.end method

.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->code_:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 412
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->height_:I

    return v0
.end method

.method public getLocation()I
    .locals 1

    .prologue
    .line 308
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->location_:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getOrientationRestrict()I
    .locals 1

    .prologue
    .line 327
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->orientationRestrict_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 561
    const/4 v2, 0x0

    .line 562
    .local v2, "size":I
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    .line 563
    const/4 v4, 0x1

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->location_:I

    .line 564
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 566
    :cond_0
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_1

    .line 567
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->code_:Ljava/lang/String;

    .line 568
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 570
    :cond_1
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_2

    .line 571
    const/4 v4, 0x3

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->width_:I

    .line 572
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 574
    :cond_2
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_3

    .line 575
    const/4 v4, 0x4

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->height_:I

    .line 576
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 578
    :cond_3
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    if-eqz v4, :cond_5

    .line 579
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_5

    aget-object v1, v5, v4

    .line 580
    .local v1, "element":Lcom/google/apps/dots/proto/client/DotsShared$Size;
    if-eqz v1, :cond_4

    .line 581
    const/4 v7, 0x5

    .line 582
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 579
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 586
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Size;
    :cond_5
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_6

    .line 587
    const/4 v4, 0x6

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->orientationRestrict_:I

    .line 588
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 590
    :cond_6
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    array-length v4, v4

    if-lez v4, :cond_8

    .line 591
    const/4 v0, 0x0

    .line 592
    .local v0, "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_7

    aget v1, v5, v4

    .line 594
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v7

    add-int/2addr v0, v7

    .line 592
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 596
    .end local v1    # "element":I
    :cond_7
    add-int/2addr v2, v0

    .line 597
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 599
    .end local v0    # "dataSize":I
    :cond_8
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    array-length v4, v4

    if-lez v4, :cond_a

    .line 600
    const/4 v0, 0x0

    .line 601
    .restart local v0    # "dataSize":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    array-length v5, v4

    :goto_2
    if-ge v3, v5, :cond_9

    aget v1, v4, v3

    .line 603
    .restart local v1    # "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v6

    add-int/2addr v0, v6

    .line 601
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 605
    .end local v1    # "element":I
    :cond_9
    add-int/2addr v2, v0

    .line 606
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    .line 608
    .end local v0    # "dataSize":I
    :cond_a
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_b

    .line 609
    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->name_:Ljava/lang/String;

    .line 610
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 612
    :cond_b
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->cachedSize:I

    .line 613
    return v2
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 393
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->width_:I

    return v0
.end method

.method public hasCode()Z
    .locals 1

    .prologue
    .line 360
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHeight()Z
    .locals 1

    .prologue
    .line 420
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 382
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOrientationRestrict()Z
    .locals 1

    .prologue
    .line 335
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWidth()Z
    .locals 1

    .prologue
    .line 401
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 491
    const/16 v1, 0x11

    .line 492
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 493
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->location_:I

    add-int v1, v2, v4

    .line 494
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->orientationRestrict_:I

    add-int v1, v2, v4

    .line 495
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    if-nez v2, :cond_3

    mul-int/lit8 v1, v1, 0x1f

    .line 501
    :cond_0
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->code_:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 502
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->name_:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 503
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->width_:I

    add-int v1, v2, v4

    .line 504
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->height_:I

    add-int v1, v2, v4

    .line 505
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    if-nez v2, :cond_6

    mul-int/lit8 v1, v1, 0x1f

    .line 511
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    if-nez v2, :cond_8

    mul-int/lit8 v1, v1, 0x1f

    .line 517
    :cond_2
    return v1

    .line 497
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 498
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    aget v4, v4, v0

    add-int v1, v2, v4

    .line 497
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 501
    .end local v0    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->code_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 502
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->name_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 507
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 508
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 507
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 508
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Size;->hashCode()I

    move-result v2

    goto :goto_4

    .line 513
    .end local v0    # "i":I
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 514
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    aget v3, v3, v0

    add-int v1, v2, v3

    .line 513
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    .locals 10
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 621
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 622
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 626
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 627
    :sswitch_0
    return-object p0

    .line 632
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 633
    .local v4, "temp":I
    if-eq v4, v8, :cond_1

    if-eq v4, v9, :cond_1

    const/4 v6, 0x3

    if-eq v4, v6, :cond_1

    const/4 v6, 0x4

    if-ne v4, v6, :cond_2

    .line 637
    :cond_1
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->location_:I

    .line 638
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    goto :goto_0

    .line 640
    :cond_2
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->location_:I

    goto :goto_0

    .line 645
    .end local v4    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->code_:Ljava/lang/String;

    .line 646
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    goto :goto_0

    .line 650
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->width_:I

    .line 651
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    goto :goto_0

    .line 655
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->height_:I

    .line 656
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    goto :goto_0

    .line 660
    :sswitch_5
    const/16 v6, 0x2a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 661
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    if-nez v6, :cond_4

    move v1, v5

    .line 662
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$Size;

    .line 663
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Size;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    if-eqz v6, :cond_3

    .line 664
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 666
    :cond_3
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    .line 667
    :goto_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_5

    .line 668
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Size;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Size;-><init>()V

    aput-object v7, v6, v1

    .line 669
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 670
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 667
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 661
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Size;
    :cond_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    array-length v1, v6

    goto :goto_1

    .line 673
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Size;
    :cond_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Size;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Size;-><init>()V

    aput-object v7, v6, v1

    .line 674
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 678
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Size;
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 679
    .restart local v4    # "temp":I
    if-eq v4, v8, :cond_6

    if-ne v4, v9, :cond_7

    .line 681
    :cond_6
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->orientationRestrict_:I

    .line 682
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    goto/16 :goto_0

    .line 684
    :cond_7
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->orientationRestrict_:I

    goto/16 :goto_0

    .line 689
    .end local v4    # "temp":I
    :sswitch_7
    const/16 v6, 0x38

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 690
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    array-length v1, v6

    .line 691
    .restart local v1    # "i":I
    add-int v6, v1, v0

    new-array v2, v6, [I

    .line 692
    .local v2, "newArray":[I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 693
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    .line 694
    :goto_3
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_8

    .line 695
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v7

    aput v7, v6, v1

    .line 696
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 694
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 699
    :cond_8
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v7

    aput v7, v6, v1

    goto/16 :goto_0

    .line 703
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[I
    :sswitch_8
    const/16 v6, 0x40

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 704
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    array-length v1, v6

    .line 705
    .restart local v1    # "i":I
    add-int v6, v1, v0

    new-array v2, v6, [I

    .line 706
    .restart local v2    # "newArray":[I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 707
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    .line 708
    :goto_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_9

    .line 709
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v7

    aput v7, v6, v1

    .line 710
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 708
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 713
    :cond_9
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v7

    aput v7, v6, v1

    goto/16 :goto_0

    .line 717
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[I
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->name_:Ljava/lang/String;

    .line 718
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    goto/16 :goto_0

    .line 622
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 279
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 522
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 523
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->location_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 525
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    .line 526
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->code_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 528
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_2

    .line 529
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->width_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 531
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_3

    .line 532
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->height_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 534
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    if-eqz v2, :cond_5

    .line 535
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_5

    aget-object v0, v3, v2

    .line 536
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Size;
    if-eqz v0, :cond_4

    .line 537
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 535
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 541
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Size;
    :cond_5
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_6

    .line 542
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->orientationRestrict_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 544
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    array-length v2, v2

    if-lez v2, :cond_7

    .line 545
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->deviceRestrict:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget v0, v3, v2

    .line 546
    .local v0, "element":I
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 545
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 549
    .end local v0    # "element":I
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    array-length v2, v2

    if-lez v2, :cond_8

    .line 550
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->locations:[I

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_8

    aget v0, v2, v1

    .line 551
    .restart local v0    # "element":I
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 550
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 554
    .end local v0    # "element":I
    :cond_8
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_9

    .line 555
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->name_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 557
    :cond_9
    return-void
.end method
