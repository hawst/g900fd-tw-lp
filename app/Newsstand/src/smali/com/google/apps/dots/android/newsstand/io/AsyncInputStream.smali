.class public Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;
.super Ljava/io/InputStream;
.source "AsyncInputStream.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private closed:Z

.field private final inputStreamFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/io/InputStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "inputStreamFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/io/InputStream;>;"
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->closed:Z

    .line 23
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->inputStreamFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream$1;-><init>(Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;)V

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->closed:Z

    return v0
.end method

.method private getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->inputStreamFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    return-object v0
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->inputStreamFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->inputStreamFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 44
    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    if-nez v0, :cond_1

    .line 46
    :cond_0
    const/4 v0, 0x0

    .line 48
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    goto :goto_0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 53
    monitor-enter p0

    .line 54
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->closed:Z

    .line 55
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->inputStreamFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->inputStreamFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 63
    return-void

    .line 55
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 69
    .local v0, "inputStream":Ljava/io/InputStream;
    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v1

    return v1

    .line 72
    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not read from input stream."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public read([B)I
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 3
    .param p1, "buffer"    # [B
    .param p2, "byteOffset"    # I
    .param p3, "byteCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 84
    .local v0, "inputStream":Ljava/io/InputStream;
    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    return v1

    .line 87
    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not read from input stream."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public skip(J)J
    .locals 3
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->inputStreamFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->inputStreamFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 94
    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    if-nez v0, :cond_1

    .line 96
    :cond_0
    const-wide/16 v0, 0x0

    .line 98
    :goto_0
    return-wide v0

    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    goto :goto_0
.end method
