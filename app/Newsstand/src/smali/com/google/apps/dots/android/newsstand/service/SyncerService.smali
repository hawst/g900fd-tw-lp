.class public Lcom/google/apps/dots/android/newsstand/service/SyncerService;
.super Lcom/google/apps/dots/android/newsstand/service/AsyncService;
.source "SyncerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static notifyObserversRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

.field private static final pendingOps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    const-class v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 88
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->pendingOps:Ljava/util/Map;

    .line 484
    new-instance v0, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 485
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/service/SyncerService$4;

    invoke-direct {v2}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$4;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->notifyObserversRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 484
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/service/AsyncService;-><init>()V

    .line 94
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/android/libraries/bind/async/DelayedRunnable;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->notifyObserversRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    return-object v0
.end method

.method static synthetic access$200()Ljava/util/Map;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->pendingOps:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300()V
    .locals 0

    .prologue
    .line 62
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->notifyObservers()V

    return-void
.end method

.method static synthetic access$700(II)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # I

    .prologue
    .line 62
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->resultBundle(II)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected static cancelLowerPriorityOp(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;)Z
    .locals 2
    .param p0, "oldPendingOp"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .param p1, "newPendingOp"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    .prologue
    const/4 v0, 0x1

    .line 267
    if-eqz p0, :cond_1

    .line 268
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->sync:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p1, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->sync:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->userRequested:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p1, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->userRequested:Z

    if-nez v1, :cond_0

    .line 270
    iput-boolean v0, p1, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->preemptivelyCancelled:Z

    .line 276
    :goto_0
    return v0

    .line 273
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->stop()V

    .line 276
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getPendingSyncProgress(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Float;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 280
    sget-object v1, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->pendingOps:Ljava/util/Map;

    invoke-static {p0, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    .line 281
    .local v0, "pending":Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->progress:Ljava/lang/Float;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static notifyObservers()V
    .locals 3

    .prologue
    .line 494
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$SyncerServiceUris;->contentUri()Landroid/net/Uri;

    move-result-object v0

    .line 495
    .local v0, "contentUri":Landroid/net/Uri;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->notify(Landroid/net/Uri;Ljava/util/Map;)V

    .line 496
    return-void
.end method

.method private static resultBundle(II)Landroid/os/Bundle;
    .locals 2
    .param p0, "successes"    # I
    .param p1, "failures"    # I

    .prologue
    .line 477
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 478
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "numSuccesses"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 479
    const-string v1, "numFailures"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 480
    return-object v0
.end method

.method private syncMyCurationsLibrary(Landroid/accounts/Account;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 417
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 418
    .local v0, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v1

    .line 419
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyCurations(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 418
    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getReallyFresh(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method private syncMyMagazinesLibrary(Landroid/accounts/Account;[B)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "gcmMessage"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "[B)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 423
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    .line 425
    .local v5, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v6

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v8

    invoke-virtual {v8, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v5, v8}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getFresh(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 429
    .local v4, "newMyMagazinesFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->notificationsEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 430
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v3

    .line 431
    .local v3, "newMagazineAppIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 433
    :try_start_0
    invoke-static {p2}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->parseFrom([B)Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;

    move-result-object v1

    .line 434
    .local v1, "message":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    iget-object v8, v1, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->magazinesAdded:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    array-length v9, v8

    move v6, v7

    :goto_0
    if-ge v6, v9, :cond_0

    aget-object v2, v8, v6

    .line 435
    .local v2, "newItem":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->getAppId()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v3, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 434
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 437
    .end local v1    # "message":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    .end local v2    # "newItem":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :catch_0
    move-exception v0

    .line 438
    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "Can\'t parse NewsstandSyncMessage that triggered the MyMagazines library sync"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v6, v0, v8, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 441
    .end local v0    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :cond_0
    new-instance v6, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;

    invoke-direct {v6, p0, v3}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;-><init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService;Ljava/util/Set;)V

    sget-object v7, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_MAIN:Lcom/google/android/libraries/bind/async/Queue;

    invoke-static {v4, v6, v7}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 472
    .end local v3    # "newMagazineAppIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v4    # "newMyMagazinesFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    :cond_1
    return-object v4
.end method

.method private syncMyNewsLibrary(Landroid/accounts/Account;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 412
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 413
    .local v0, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyNews(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getReallyFresh(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public createPendingOp(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;ZZ)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .param p3, "sync"    # Z
    .param p4, "userRequested"    # Z

    .prologue
    .line 247
    new-instance v1, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;-><init>(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;ZZ)V

    .line 248
    .local v1, "newPendingOp":Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 250
    .local v0, "key":Landroid/util/Pair;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->pendingOps:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    .line 251
    .local v2, "oldPendingOp":Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->cancelLowerPriorityOp(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 252
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->pendingOps:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    :cond_0
    return-object v1
.end method

.method public handleIntent(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 32
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 293
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v20

    .line 294
    .local v20, "action":Ljava/lang/String;
    if-nez v20, :cond_0

    .line 295
    sget-object v5, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Missing action"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 296
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Missing action"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 406
    :goto_0
    return-object v5

    .line 299
    :cond_0
    const-string v5, "extraAccount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v19

    check-cast v19, Landroid/accounts/Account;

    .line 300
    .local v19, "account":Landroid/accounts/Account;
    if-nez v19, :cond_1

    .line 301
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v19

    .line 302
    if-nez v19, :cond_1

    .line 303
    sget-object v5, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "No selected account"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 304
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "No selected account"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto :goto_0

    .line 307
    :cond_1
    invoke-static/range {v19 .. v19}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v10

    .line 309
    .local v10, "userWriteToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    const-string v5, "edition"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .line 310
    .local v23, "extraEdition":Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    const-string v5, "userRequested"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v30

    .line 311
    .local v30, "userRequested":Z
    const-string v5, "wifiOnlyDownloadOverride"

    const/4 v6, 0x0

    .line 312
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v31

    .line 313
    .local v31, "wifiOnlyDownloadOverride":Z
    const-string v5, "anyFreshness"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v21

    .line 314
    .local v21, "anyFreshness":Z
    const-string v5, "pinnedVersion"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v28

    .line 315
    .local v28, "pinnedVersion":I
    const-string v5, "pinEdition"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 316
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    move/from16 v2, v28

    invoke-virtual {v5, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->pin(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V

    .line 317
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v23

    move/from16 v3, v30

    invoke-virtual {v0, v1, v2, v5, v3}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->createPendingOp(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;ZZ)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v5

    .line 318
    move/from16 v0, v31

    # invokes: Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->setWifiOnlyDownloadOverride(Z)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    invoke-static {v5, v0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->access$400(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;Z)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v5

    .line 319
    move/from16 v0, v28

    # invokes: Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->setPinnedVersion(I)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    invoke-static {v5, v0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->access$500(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;I)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v5

    .line 320
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->start()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto/16 :goto_0

    .line 322
    :cond_2
    const-string v5, "unpinEdition"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 323
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpin(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 324
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v23

    move/from16 v3, v30

    invoke-virtual {v0, v1, v2, v5, v3}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->createPendingOp(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;ZZ)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->start()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto/16 :goto_0

    .line 326
    :cond_3
    const-string v5, "syncEdition"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 327
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v23

    move/from16 v3, v30

    invoke-virtual {v0, v1, v2, v5, v3}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->createPendingOp(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;ZZ)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v5

    .line 328
    move/from16 v0, v21

    # invokes: Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->setAnyFreshness(Z)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    invoke-static {v5, v0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->access$600(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;Z)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v5

    .line 329
    move/from16 v0, v28

    # invokes: Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->setPinnedVersion(I)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    invoke-static {v5, v0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->access$500(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;I)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v5

    .line 330
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->start()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto/16 :goto_0

    .line 332
    :cond_4
    const-string v5, "fullSync"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 333
    new-instance v5, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncStartEvent;

    move-object/from16 v0, v20

    invoke-direct {v5, v0, v10}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncStartEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncStartEvent;->track()V

    .line 334
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 335
    .local v16, "syncStartTime":J
    move-object/from16 v24, v19

    .line 336
    .local v24, "finalAccount":Landroid/accounts/Account;
    invoke-static/range {v19 .. v19}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v22

    .line 338
    .local v22, "configToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v5

    const-wide/16 v12, 0x0

    invoke-virtual {v5, v12, v13}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->requestCleanup(J)V

    .line 341
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinned(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v27

    .line 342
    .local v27, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v5, v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v26

    .line 343
    .local v26, "pendingSyncs":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/async/Task<*>;>;"
    const/4 v5, 0x1

    new-array v8, v5, [I

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput v6, v8, v5

    .line 344
    .local v8, "failureCount":[I
    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v15, v5

    .line 345
    .local v15, "total":I
    move-object/from16 v0, v27

    iget-object v12, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v13, v12

    const/4 v5, 0x0

    move v11, v5

    :goto_1
    if-ge v11, v13, :cond_5

    aget-object v25, v12, v11

    .line 346
    .local v25, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromProto(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .line 347
    .local v9, "edition":Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Queuing up %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/16 v18, 0x0

    aput-object v9, v14, v18

    invoke-virtual {v5, v6, v14}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 348
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v30

    invoke-virtual {v0, v1, v9, v5, v2}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->createPendingOp(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;ZZ)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v5

    .line 350
    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getPinnedVersion()I

    move-result v6

    # invokes: Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->setPinnedVersion(I)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->access$500(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;I)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v7

    .line 351
    .local v7, "pending":Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_MAIN:Lcom/google/android/libraries/bind/async/Queue;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v10}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;-><init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService;Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;[ILcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 370
    .local v4, "task":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<Ljava/lang/Object;>;"
    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    add-int/lit8 v5, v11, 0x1

    move v11, v5

    goto :goto_1

    .line 373
    .end local v4    # "task":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<Ljava/lang/Object;>;"
    .end local v7    # "pending":Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .end local v9    # "edition":Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .end local v25    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_5
    new-instance v11, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;

    sget-object v13, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_MAIN:Lcom/google/android/libraries/bind/async/Queue;

    move-object/from16 v12, p0

    move-object v14, v8

    move-object/from16 v18, v10

    invoke-direct/range {v11 .. v18}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$2;-><init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService;Ljava/util/concurrent/Executor;[IIJLcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    move-object/from16 v0, v26

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v5, v0, v6}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getFreshConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    const/4 v6, 0x0

    .line 392
    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 390
    move-object/from16 v0, v26

    invoke-static {v5, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->doSequentially(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto/16 :goto_0

    .line 396
    .end local v8    # "failureCount":[I
    .end local v15    # "total":I
    .end local v16    # "syncStartTime":J
    .end local v22    # "configToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .end local v24    # "finalAccount":Landroid/accounts/Account;
    .end local v26    # "pendingSyncs":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/async/Task<*>;>;"
    .end local v27    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_6
    const-string v5, "syncMyNews"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 397
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->syncMyNewsLibrary(Landroid/accounts/Account;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto/16 :goto_0

    .line 398
    :cond_7
    const-string v5, "syncMyCurations"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 399
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->syncMyCurationsLibrary(Landroid/accounts/Account;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto/16 :goto_0

    .line 400
    :cond_8
    const-string v5, "syncMyMagazines"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 401
    const-string v5, "gcmMessage"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->syncMyMagazinesLibrary(Landroid/accounts/Account;[B)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto/16 :goto_0

    .line 402
    :cond_9
    const-string v5, "syncConfig"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 403
    invoke-static/range {v19 .. v19}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v29

    .line 404
    .local v29, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v5, v0, v6}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getFreshConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto/16 :goto_0

    .line 406
    .end local v29    # "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    :cond_a
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v11, "Unrecognized action: "

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_b

    invoke-virtual {v11, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_2
    invoke-direct {v6, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto/16 :goto_0

    :cond_b
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 500
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 286
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->onCreate()V

    .line 287
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    .line 288
    return-void
.end method
