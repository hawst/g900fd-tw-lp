.class Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$1;
.super Ljava/lang/Object;
.source "NSContentInputStreamProvider.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        "Ljava/io/InputStream;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$1;->this$0:Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "storeResponse"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/util/concurrent/Futures;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$1;->apply(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
