.class public final Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EditionProto"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;,
        Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;,
        Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;,
        Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;,
        Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;,
        Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;,
        Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;,
        Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;


# instance fields
.field private bitField0_:I

.field private curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

.field private geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

.field private magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

.field private news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

.field private relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

.field private searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

.field private section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

.field private topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2024
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2025
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3173
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->type_:I

    .line 2025
    return-void
.end method

.method public static parseFrom([B)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 3595
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    return-object v0
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 3

    .prologue
    .line 3362
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3366
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    if-eqz v2, :cond_0

    .line 3367
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    .line 3369
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    if-eqz v2, :cond_1

    .line 3370
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    .line 3372
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    if-eqz v2, :cond_2

    .line 3373
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    .line 3375
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    if-eqz v2, :cond_3

    .line 3376
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    .line 3378
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    if-eqz v2, :cond_4

    .line 3379
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    .line 3381
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    if-eqz v2, :cond_5

    .line 3382
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    .line 3384
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    if-eqz v2, :cond_6

    .line 3385
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    .line 3387
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    if-eqz v2, :cond_7

    .line 3388
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    .line 3390
    :cond_7
    return-object v0

    .line 3363
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    :catch_0
    move-exception v1

    .line 3364
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2021
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3395
    if-ne p1, p0, :cond_1

    .line 3406
    :cond_0
    :goto_0
    return v1

    .line 3396
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 3397
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 3398
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    if-nez v3, :cond_3

    .line 3399
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    if-nez v3, :cond_3

    .line 3400
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    if-nez v3, :cond_3

    .line 3401
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    if-nez v3, :cond_3

    .line 3402
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    if-nez v3, :cond_3

    .line 3403
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    if-nez v3, :cond_3

    .line 3404
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    if-nez v3, :cond_3

    .line 3405
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 3406
    goto :goto_0

    .line 3398
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    .line 3399
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    .line 3400
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    .line 3401
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    .line 3402
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    .line 3403
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    .line 3404
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    .line 3405
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    .line 3406
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0
.end method

.method public getCuratedTopic()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
    .locals 1

    .prologue
    .line 3327
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    return-object v0
.end method

.method public getMagazine()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;
    .locals 1

    .prologue
    .line 3232
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    return-object v0
.end method

.method public getNews()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;
    .locals 1

    .prologue
    .line 3194
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    return-object v0
.end method

.method public getRelatedPosts()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;
    .locals 1

    .prologue
    .line 3308
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    return-object v0
.end method

.method public getSearchPosts()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;
    .locals 1

    .prologue
    .line 3289
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    return-object v0
.end method

.method public getSection()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;
    .locals 1

    .prologue
    .line 3213
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3457
    const/4 v0, 0x0

    .line 3458
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3459
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->type_:I

    .line 3460
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3462
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    if-eqz v1, :cond_1

    .line 3463
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    .line 3464
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3466
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    if-eqz v1, :cond_2

    .line 3467
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    .line 3468
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3470
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    if-eqz v1, :cond_3

    .line 3471
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    .line 3472
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3474
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    if-eqz v1, :cond_4

    .line 3475
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    .line 3476
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3478
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    if-eqz v1, :cond_5

    .line 3479
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    .line 3480
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3482
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    if-eqz v1, :cond_6

    .line 3483
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    .line 3484
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3486
    :cond_6
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    if-eqz v1, :cond_7

    .line 3487
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    .line 3488
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3490
    :cond_7
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    if-eqz v1, :cond_8

    .line 3491
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    .line 3492
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3494
    :cond_8
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->cachedSize:I

    .line 3495
    return v0
.end method

.method public getTopic()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;
    .locals 1

    .prologue
    .line 3251
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 3175
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->type_:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 3410
    const/16 v0, 0x11

    .line 3411
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 3412
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->type_:I

    add-int v0, v1, v3

    .line 3413
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 3414
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 3415
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 3416
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 3417
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 3418
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    add-int v0, v3, v1

    .line 3419
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    if-nez v1, :cond_6

    move v1, v2

    :goto_6
    add-int v0, v3, v1

    .line 3420
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    if-nez v3, :cond_7

    :goto_7
    add-int v0, v1, v2

    .line 3421
    return v0

    .line 3413
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;->hashCode()I

    move-result v1

    goto :goto_0

    .line 3414
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->hashCode()I

    move-result v1

    goto :goto_1

    .line 3415
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;->hashCode()I

    move-result v1

    goto :goto_2

    .line 3416
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->hashCode()I

    move-result v1

    goto :goto_3

    .line 3417
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->hashCode()I

    move-result v1

    goto :goto_4

    .line 3418
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;->hashCode()I

    move-result v1

    goto :goto_5

    .line 3419
    :cond_6
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;->hashCode()I

    move-result v1

    goto :goto_6

    .line 3420
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->hashCode()I

    move-result v2

    goto :goto_7
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3503
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3504
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3508
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3509
    :sswitch_0
    return-object p0

    .line 3514
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 3515
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_2

    .line 3526
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->type_:I

    .line 3527
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->bitField0_:I

    goto :goto_0

    .line 3529
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->type_:I

    goto :goto_0

    .line 3534
    .end local v1    # "temp":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    if-nez v2, :cond_3

    .line 3535
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    .line 3537
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3541
    :sswitch_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    if-nez v2, :cond_4

    .line 3542
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    .line 3544
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3548
    :sswitch_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    if-nez v2, :cond_5

    .line 3549
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    .line 3551
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3555
    :sswitch_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    if-nez v2, :cond_6

    .line 3556
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    .line 3558
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3562
    :sswitch_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    if-nez v2, :cond_7

    .line 3563
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    .line 3565
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3569
    :sswitch_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    if-nez v2, :cond_8

    .line 3570
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    .line 3572
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3576
    :sswitch_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    if-nez v2, :cond_9

    .line 3577
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    .line 3579
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3583
    :sswitch_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    if-nez v2, :cond_a

    .line 3584
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    .line 3586
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3504
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2021
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    return-object v0
.end method

.method public setCuratedTopic(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    .prologue
    .line 3330
    if-nez p1, :cond_0

    .line 3331
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3333
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    .line 3334
    return-object p0
.end method

.method public setMagazine(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    .prologue
    .line 3235
    if-nez p1, :cond_0

    .line 3236
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3238
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    .line 3239
    return-object p0
.end method

.method public setNews(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    .prologue
    .line 3197
    if-nez p1, :cond_0

    .line 3198
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3200
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    .line 3201
    return-object p0
.end method

.method public setRelatedPosts(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    .prologue
    .line 3311
    if-nez p1, :cond_0

    .line 3312
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3314
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    .line 3315
    return-object p0
.end method

.method public setSearchPosts(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    .prologue
    .line 3292
    if-nez p1, :cond_0

    .line 3293
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3295
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    .line 3296
    return-object p0
.end method

.method public setSection(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    .prologue
    .line 3216
    if-nez p1, :cond_0

    .line 3217
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3219
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    .line 3220
    return-object p0
.end method

.method public setType(I)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 3178
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->type_:I

    .line 3179
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->bitField0_:I

    .line 3180
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3426
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3427
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3429
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    if-eqz v0, :cond_1

    .line 3430
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->news_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3432
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    if-eqz v0, :cond_2

    .line 3433
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->section_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3435
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    if-eqz v0, :cond_3

    .line 3436
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->magazine_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$MagazineEditionInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3438
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    if-eqz v0, :cond_4

    .line 3439
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->topic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3441
    :cond_4
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    if-eqz v0, :cond_5

    .line 3442
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->geolocation_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3444
    :cond_5
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    if-eqz v0, :cond_6

    .line 3445
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->searchPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SearchPostsEditionInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3447
    :cond_6
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    if-eqz v0, :cond_7

    .line 3448
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->relatedPosts_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3450
    :cond_7
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    if-eqz v0, :cond_8

    .line 3451
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->curatedTopic_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3453
    :cond_8
    return-void
.end method
