.class Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "HeaderEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateHeroView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 8
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 344
    if-nez p1, :cond_1

    const/4 v2, 0x0

    .line 347
    .local v2, "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    :goto_0
    instance-of v3, v2, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;

    if-eqz v3, :cond_2

    .line 348
    check-cast v2, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;

    .end local v2    # "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$600(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;->apply(Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;)V

    .line 349
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$600(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->setVisibility(I)V

    .line 350
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$700(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setVisibility(I)V

    .line 360
    :goto_1
    const/high16 v1, -0x1000000

    .line 361
    .local v1, "backgroundColor":I
    if-eqz p1, :cond_0

    .line 362
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 363
    .local v0, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasHeaderBackgroundColor()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 365
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getHeaderBackgroundColor()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->parseQuietly(Ljava/lang/String;I)I

    move-result v1

    .line 368
    .end local v0    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->backgroundView:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 371
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroView:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setBackgroundResource(I)V

    .line 372
    return-void

    .line 345
    .end local v1    # "backgroundColor":I
    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->iconSource()Lcom/google/apps/dots/android/newsstand/icon/IconSource;

    move-result-object v2

    goto :goto_0

    .line 351
    .restart local v2    # "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    :cond_2
    instance-of v3, v2, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    if-eqz v3, :cond_3

    .line 352
    check-cast v2, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    .end local v2    # "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$700(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .line 353
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$dimen;->edition_header_logo_size:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 352
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/icon/IconId;->apply(Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;I)V

    .line 354
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$700(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setVisibility(I)V

    .line 355
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$600(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->setVisibility(I)V

    goto :goto_1

    .line 357
    .restart local v2    # "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$700(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setVisibility(I)V

    .line 358
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$600(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->setVisibility(I)V

    goto :goto_1
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 341
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
