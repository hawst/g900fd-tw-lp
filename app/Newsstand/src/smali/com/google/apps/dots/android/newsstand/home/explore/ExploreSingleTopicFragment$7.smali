.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;
.super Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;
.source "ExploreSingleTopicFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private originalTitleColor:I

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    .prologue
    .line 299
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method protected cardClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 358
    const-class v0, Lcom/google/android/play/cardview/CardViewGroup;

    return-object v0
.end method

.method protected bridge synthetic handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 299
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 338
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 340
    invoke-virtual {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->titleInSharedElements(Ljava/util/List;Ljava/util/List;)Landroid/widget/TextView;

    move-result-object v0

    .line 341
    .local v0, "titleView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 342
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->originalTitleColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 344
    :cond_0
    return-void
.end method

.method protected bridge synthetic handleEnterSetSharedElementEnd(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 299
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected bridge synthetic handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 299
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 321
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 326
    invoke-virtual {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->titleInSharedElements(Ljava/util/List;Ljava/util/List;)Landroid/widget/TextView;

    move-result-object v1

    .line 327
    .local v1, "titleView":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 328
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->shelf_header_title_color:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 329
    .local v0, "shelfHeaderColor":I
    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->originalTitleColor:I

    .line 330
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 332
    .end local v0    # "shelfHeaderColor":I
    :cond_0
    return-void
.end method

.method protected bridge synthetic handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 299
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected handleExitRejectedSharedElements(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Ljava/util/List;)V
    .locals 1
    .param p1, "controller"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 349
    .local p2, "rejectedViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->handleExitRejectedSharedElements(Ljava/lang/Object;Ljava/util/List;)V

    .line 350
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 352
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 354
    :cond_0
    return-void
.end method

.method protected bridge synthetic handleExitRejectedSharedElements(Ljava/lang/Object;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 299
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->handleExitRejectedSharedElements(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Ljava/util/List;)V

    return-void
.end method

.method protected bridge synthetic headerListLayout(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 299
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->headerListLayout(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    return-object v0
.end method

.method protected headerListLayout(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->access$100(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V
    .locals 0

    .prologue
    .line 299
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)V

    return-void
.end method

.method protected prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    .prologue
    .line 305
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    .line 307
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->postponeEnterTransition()V

    .line 309
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->access$300(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setShuffleOnFirstLoad(Z)V

    .line 310
    return-void
.end method

.method protected bridge synthetic prepareViewsForTransition(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 299
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$7;->prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;)V

    return-void
.end method
