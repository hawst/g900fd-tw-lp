.class public Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper;
.super Ljava/lang/Object;
.source "PostReadingHelper.java"


# direct methods
.method public static addPostReadingData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;ILcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 12
    .param p0, "cardData"    # Lcom/google/android/libraries/bind/data/Data;
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .param p2, "optSectionSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    .param p3, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p4, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p5, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;
    .param p6, "postIndex"    # I
    .param p7, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 45
    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 46
    .local v4, "postId":Ljava/lang/String;
    sget v8, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ID:I

    invoke-virtual {p0, v8, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 47
    sget v8, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {p0, v8, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 48
    sget v8, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_INDEX:I

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 49
    instance-of v8, p3, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    if-eqz v8, :cond_0

    .line 50
    sget v8, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_SECTION_SUMMARY:I

    invoke-virtual {p0, v8, p2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 55
    :cond_0
    instance-of v8, p3, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    if-eqz v8, :cond_1

    move-object v8, p3

    check-cast v8, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    .line 56
    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->getAppId()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 57
    move-object v3, p3

    .line 61
    .local v3, "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :goto_0
    sget v8, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ORIGINAL_EDITION:I

    invoke-virtual {p0, v8, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 62
    sget v8, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_VIEW_TYPE:I

    sget-object v9, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;->ARTICLE:Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;

    invoke-virtual {p0, v8, v9}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 63
    sget v9, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_LOADER:I

    .line 64
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v8

    sget-object v10, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v8, v10, :cond_2

    const/4 v8, 0x1

    :goto_1
    invoke-static {v8, p1}, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper;->makeArticleLoaderProvider(ZLcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;

    move-result-object v8

    .line 63
    invoke-virtual {p0, v9, v8}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 66
    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v7

    .line 67
    .local v7, "subscribed":Z
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getIsMetered()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 68
    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 69
    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->isRead(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    const/4 v6, 0x1

    .line 70
    .local v6, "showMeteredDialog":Z
    :goto_2
    sget v8, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_IS_METERED:I

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 71
    sget v8, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_IS_SUBSCRIBED:I

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 74
    sget v8, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_VERSION:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getUpdated()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 80
    move-object/from16 v0, p7

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    .line 81
    .local v2, "account":Landroid/accounts/Account;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;

    sget-object v8, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {v5, v8, v2, p1}, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper$1;-><init>(Ljava/util/concurrent/Executor;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    .line 96
    .local v5, "shareParamsTask":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<Lcom/google/apps/dots/android/newsstand/share/ShareParams;>;"
    sget v8, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_SHARE_PARAMS:I

    invoke-virtual {p0, v8, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 97
    return-void

    .line 59
    .end local v2    # "account":Landroid/accounts/Account;
    .end local v3    # "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v5    # "shareParamsTask":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<Lcom/google/apps/dots/android/newsstand/share/ShareParams;>;"
    .end local v6    # "showMeteredDialog":Z
    .end local v7    # "subscribed":Z
    :cond_1
    iget-object v8, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v3

    .restart local v3    # "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    goto :goto_0

    .line 64
    :cond_2
    const/4 v8, 0x0

    goto :goto_1

    .line 69
    .restart local v7    # "subscribed":Z
    :cond_3
    const/4 v6, 0x0

    goto :goto_2
.end method

.method private static makeArticleLoaderProvider(ZLcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;
    .locals 4
    .param p0, "isMagazine"    # Z
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 101
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;

    iget-object v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
