.class public Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;
.super Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;
.source "CardMagazinePageItem.java"


# static fields
.field public static final DK_IMAGE_HEIGHT_TO_WIDTH_RATIO:I

.field public static final DK_IMAGE_ID:I

.field public static final DK_IS_INITIAL_POST:I

.field public static final DK_ON_CLICK_LISTENER:I

.field public static final DK_TRANSFORM:I

.field public static final DK_VERSION:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePageItem_version:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_VERSION:I

    .line 36
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePageItem_imageId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IMAGE_ID:I

    .line 38
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePageItem_transform:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_TRANSFORM:I

    .line 40
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePageItem_imageAspectRatio:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IMAGE_HEIGHT_TO_WIDTH_RATIO:I

    .line 44
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePageItem_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_ON_CLICK_LISTENER:I

    .line 47
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePageItem_isInitialPost:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IS_INITIAL_POST:I

    .line 49
    new-array v0, v3, [I

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_VERSION:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->EQUALITY_FIELDS:[I

    .line 53
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_magazine_page_item:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->LAYOUTS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    return-void
.end method

.method public static fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v4, 0x0

    .line 71
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_VERSION:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getUpdated()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p3, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 73
    const/4 v0, 0x0

    .line 74
    .local v0, "scrubberImages":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-ne v1, v2, :cond_2

    .line 75
    iget-object v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v1, v1

    if-lez v1, :cond_1

    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 81
    :goto_0
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 82
    aget-object v1, v0, v4

    invoke-static {p3, v1}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->putScrubberImageInfo(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)V

    .line 88
    :cond_0
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_ON_CLICK_LISTENER:I

    .line 89
    invoke-static {p2, p1, v4}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->makeOnClickListener(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;I)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;

    move-result-object v2

    .line 88
    invoke-virtual {p3, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 90
    return-void

    .line 75
    :cond_1
    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    goto :goto_0

    .line 78
    :cond_2
    iget-object v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v1, v1

    if-lez v1, :cond_3

    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    :goto_1
    goto :goto_0

    :cond_3
    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    goto :goto_1
.end method

.method public static makeOnClickListener(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;I)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    .locals 1
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .param p2, "pageNumber"    # I

    .prologue
    .line 120
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;I)V

    return-object v0
.end method

.method public static putScrubberImageInfo(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)V
    .locals 7
    .param p0, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p1, "scrubberImage"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 93
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getHeight()I

    move-result v2

    .line 94
    .local v2, "imageHeight":I
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getWidth()I

    move-result v3

    .line 95
    .local v3, "imageWidth":I
    int-to-float v4, v2

    int-to-float v5, v3

    div-float v0, v4, v5

    .line 96
    .local v0, "aspectRatio":F
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IMAGE_ID:I

    .line 97
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v5

    .line 96
    invoke-virtual {p0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 102
    cmpl-float v4, v0, v6

    if-lez v4, :cond_0

    .line 103
    div-float v1, v6, v0

    .line 104
    .local v1, "cropFraction":F
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_TRANSFORM:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    const v6, 0x477fff00    # 65535.0f

    mul-float/2addr v6, v1

    .line 106
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Bottom(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v5

    .line 107
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v5

    .line 104
    invoke-virtual {p0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 108
    const/high16 v0, 0x40000000    # 2.0f

    .line 115
    .end local v1    # "cropFraction":F
    :goto_0
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IMAGE_HEIGHT_TO_WIDTH_RATIO:I

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 116
    return-void

    .line 113
    :cond_0
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_TRANSFORM:I

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/bind/data/Data;->remove(I)V

    goto :goto_0
.end method
