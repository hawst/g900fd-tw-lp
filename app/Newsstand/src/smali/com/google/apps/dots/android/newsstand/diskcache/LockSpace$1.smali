.class Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;
.super Ljava/lang/Object;
.source "LockSpace.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->lock(Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private locked:Z

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;

.field final synthetic val$exclusive:Z

.field final synthetic val$region:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;ZLjava/lang/Object;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;

    .prologue
    .line 55
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;, "Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;

    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->val$exclusive:Z

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->val$region:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->locked:Z

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 71
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;, "Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;"
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "region"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->val$region:Ljava/lang/Object;

    .line 72
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "locked"

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->locked:Z

    .line 73
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unlock()V
    .locals 3

    .prologue
    .line 61
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;, "Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;"
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->locked:Z

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 62
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->lock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->access$000(Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 63
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->locked:Z

    .line 64
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->val$exclusive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->exclusiveLocks:Ljava/util/List;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->access$100(Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;)Ljava/util/List;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->val$region:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->lock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->access$000(Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 66
    monitor-exit v1

    .line 67
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$1;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->inclusiveLocks:Ljava/util/List;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;->access$200(Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
