.class Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$9;
.super Ljava/lang/Object;
.source "DotsWebViewBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->openPlayStoreDoc(ILjava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

.field final synthetic val$backendDocId:Ljava/lang/String;

.field final synthetic val$docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field final synthetic val$parentBackendDocId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Lcom/google/apps/dots/android/newsstand/util/DocType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    .prologue
    .line 306
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$9;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$9;->val$docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$9;->val$backendDocId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$9;->val$parentBackendDocId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$9;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$9;->val$docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->forDocType(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$9;->val$backendDocId:Ljava/lang/String;

    .line 310
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$9;->val$parentBackendDocId:Ljava/lang/String;

    .line 311
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->setParentBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v0

    .line 312
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->start()V

    .line 313
    return-void
.end method
