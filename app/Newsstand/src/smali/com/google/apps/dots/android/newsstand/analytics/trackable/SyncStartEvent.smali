.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncStartEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;
.source "SyncStartEvent.java"


# instance fields
.field private final syncTrigger:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "syncTrigger"    # Ljava/lang/String;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 17
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 18
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncStartEvent;->syncTrigger:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 2
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 25
    const-string v0, "SyncMetadata"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncStartEvent;->syncTrigger:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v0, "Sync Started"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 29
    return-object p1
.end method
