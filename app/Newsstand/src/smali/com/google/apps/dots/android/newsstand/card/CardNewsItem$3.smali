.class final Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardNewsItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;ZZZZLjava/lang/String;Lcom/google/android/libraries/bind/data/Data;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$data:Lcom/google/android/libraries/bind/data/Data;

.field final synthetic val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$postId:Ljava/lang/String;

.field final synthetic val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

.field final synthetic val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$data:Lcom/google/android/libraries/bind/data/Data;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$postId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 251
    new-instance v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->track(Z)V

    .line 253
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 255
    .local v0, "card":Landroid/view/View;
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$data:Lcom/google/android/libraries/bind/data/Data;

    sget v3, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_INDEX:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 258
    .local v1, "postIndex":Ljava/lang/Integer;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    invoke-direct {v2, p2}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 259
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 260
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setOriginalEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$postId:Ljava/lang/String;

    .line 261
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    .line 262
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    .line 263
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v2

    .line 264
    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostIndex(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v2

    .line 265
    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v2

    .line 266
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->start()V

    .line 267
    return-void

    .line 253
    .end local v0    # "card":Landroid/view/View;
    .end local v1    # "postIndex":Ljava/lang/Integer;
    :cond_0
    const-class v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;

    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->card:I

    .line 254
    invoke-static {p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewInParent(Landroid/view/View;Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
