.class public Lcom/google/apps/dots/android/newsstand/article/PageLocation;
.super Ljava/lang/Object;
.source "PageLocation.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final pageFraction:Ljava/lang/Float;

.field private final pageNumber:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, v0, v0}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;-><init>(Ljava/lang/Float;Ljava/lang/Integer;)V

    .line 27
    return-void
.end method

.method private constructor <init>(Ljava/lang/Float;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "pageFraction"    # Ljava/lang/Float;
    .param p2, "pageNumber"    # Ljava/lang/Integer;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    .line 31
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageNumber:Ljava/lang/Integer;

    .line 32
    return-void
.end method

.method public static fromFraction(Ljava/lang/Float;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    .locals 2
    .param p0, "pageFraction"    # Ljava/lang/Float;

    .prologue
    .line 35
    invoke-virtual {p0}, Ljava/lang/Float;->isNaN()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Tried to create PageLocation from NaN Float"

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 37
    new-instance v0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;-><init>(Ljava/lang/Float;Ljava/lang/Integer;)V

    return-object v0

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static fromNumber(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    .locals 2
    .param p0, "pageNumber"    # Ljava/lang/Integer;

    .prologue
    .line 41
    new-instance v0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;-><init>(Ljava/lang/Float;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    .locals 9
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 45
    invoke-static {p0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 46
    new-instance v3, Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    invoke-direct {v3}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;-><init>()V

    .line 70
    :goto_0
    return-object v3

    .line 47
    :cond_0
    const/16 v3, 0x2e

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-ltz v3, :cond_3

    .line 49
    :try_start_0
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 50
    .local v1, "pageFraction":Ljava/lang/Float;
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    .line 51
    :cond_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Invalid PageLocation format %s. Expecting either a float in [0, 1] or a positive integer."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    .end local v1    # "pageFraction":Ljava/lang/Float;
    :goto_1
    new-instance v3, Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    invoke-direct {v3}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;-><init>()V

    goto :goto_0

    .line 53
    .restart local v1    # "pageFraction":Ljava/lang/Float;
    :cond_2
    :try_start_1
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->fromFraction(Ljava/lang/Float;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_0

    .line 55
    .end local v1    # "pageFraction":Ljava/lang/Float;
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Invalid PageLocation format %s. Expecting either a float in [0, 1] or a positive integer."

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v0, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 60
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_3
    :try_start_2
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 61
    .local v2, "pageNumber":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-gez v3, :cond_4

    .line 62
    sget-object v3, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Invalid PageLocation format %s. Expecting either a float in [0, 1] or a positive integer."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 66
    .end local v2    # "pageNumber":Ljava/lang/Integer;
    :catch_1
    move-exception v0

    .line 67
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Invalid PageLocation format %s. Expecting either a float in [0, 1] or a positive integer."

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v0, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 64
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v2    # "pageNumber":Ljava/lang/Integer;
    :cond_4
    :try_start_3
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->fromNumber(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 107
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 108
    check-cast v0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .line 109
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageNumber:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageNumber:Ljava/lang/Integer;

    .line 110
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 112
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    :cond_0
    return v1
.end method

.method public getNonNullPageFraction()F
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public getPageFraction()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    return-object v0
.end method

.method public getPageNumber()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageNumber:Ljava/lang/Integer;

    return-object v0
.end method

.method public hasValidPageFraction()Z
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValidPageNumber()Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageNumber:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageNumber:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 117
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageNumber:Ljava/lang/Integer;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageNumber:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageNumber:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 100
    const-string v0, ""

    .line 102
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageFraction:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->pageNumber:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
