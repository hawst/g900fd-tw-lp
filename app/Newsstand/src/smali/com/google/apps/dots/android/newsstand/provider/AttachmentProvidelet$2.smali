.class Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$2;
.super Ljava/lang/Object;
.source "AttachmentProvidelet.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureFallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->getAttachmentFD(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureFallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

.field final synthetic val$attachmentId:Ljava/lang/String;

.field final synthetic val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$2;->this$0:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$2;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$2;->val$attachmentId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$2;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$2;->val$attachmentId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
