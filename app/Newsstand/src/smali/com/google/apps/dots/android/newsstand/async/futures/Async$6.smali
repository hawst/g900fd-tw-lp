.class final Lcom/google/apps/dots/android/newsstand/async/futures/Async$6;
.super Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;
.source "Async.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transformNoCancellation(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture",
        "<TI;TO;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 186
    .local p1, "function":Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;, "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform<-TI;+TO;>;"
    .local p2, "inputFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TI;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;-><init>(Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Lcom/google/common/util/concurrent/ListenableFuture;)V

    return-void
.end method


# virtual methods
.method protected propagateCancellation(Ljava/util/concurrent/Future;Z)V
    .locals 0
    .param p2, "mayInterruptIfRunning"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    return-void
.end method
