.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredSubscriptionPurchasedScreen;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;
.source "MeteredSubscriptionPurchasedScreen.java"


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 18
    return-void
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 5
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredSubscriptionPurchasedScreen;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v2

    .line 24
    .local v2, "originalEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-object v0, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 25
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    iget-object v1, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 27
    .local v1, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v3

    .line 28
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v3

    iget-object v4, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 29
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v3

    .line 30
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredSubscriptionPurchasedScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 31
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredSubscriptionPurchasedScreen;->getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setUserSubscriptionType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v3

    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 32
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 34
    return-object p1
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 39
    const-string v0, "Subscription Purchased"

    return-object v0
.end method
