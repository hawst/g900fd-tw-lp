.class public Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;
.super Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
.source "SavedEdition.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setType(I)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 21
    return-void
.end method


# virtual methods
.method public editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 35
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->SAVED:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 25
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    return v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string v0, "CAAiCENBb1NBQ2dBUAE"

    return-object v0
.end method

.method protected bridge synthetic getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;->getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    move-result-object v0

    return-object v0
.end method

.method protected getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/saved/SavedList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->savedList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 41
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getSaved(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public showKeepOnDeviceUi()Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public showOnDeviceOnlyUi()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public supportsReadStates()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public supportsSubscription()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;->readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method
