.class public Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
.super Ljava/lang/Object;
.source "ByteArray.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
        ">;"
    }
.end annotation


# static fields
.field public static final LEX:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final bytes:[B

.field private hash:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->LEX:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "bytes"    # [B

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes:[B

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)[B
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes:[B

    return-object v0
.end method

.method public static of([B)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .locals 1
    .param p0, "bytes"    # [B

    .prologue
    .line 26
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;-><init>([B)V

    return-object v0
.end method


# virtual methods
.method public bytes()[B
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes:[B

    return-object v0
.end method

.method public compareTo(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)I
    .locals 1
    .param p1, "another"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    .line 67
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->LEX:Ljava/util/Comparator;

    invoke-interface {v0, p0, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 11
    check-cast p1, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->compareTo(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 62
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes:[B

    check-cast p1, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public get(I)B
    .locals 1
    .param p1, "location"    # I

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 49
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->hash:I

    .line 50
    .local v0, "h":I
    if-nez v0, :cond_1

    .line 51
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    .line 52
    if-nez v0, :cond_0

    .line 53
    const/4 v0, 0x1

    .line 55
    :cond_0
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->hash:I

    .line 57
    :cond_1
    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes:[B

    array-length v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes:[B

    invoke-static {v0}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
