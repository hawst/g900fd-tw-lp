.class public Lcom/google/apps/dots/android/newsstand/model/PostUtil;
.super Ljava/lang/Object;
.source "PostUtil.java"


# static fields
.field public static final LANDSCAPE_THUMBNAIL_PAIR_PATTERN:Ljava/util/regex/Pattern;

.field public static final PORTRAIT_THUMBNAIL_PAIR_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 25
    const-string v0, "thumbnail_landscape[01]"

    .line 26
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->LANDSCAPE_THUMBNAIL_PAIR_PATTERN:Ljava/util/regex/Pattern;

    .line 27
    const-string v0, "thumbnail[01]"

    .line 28
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->PORTRAIT_THUMBNAIL_PAIR_PATTERN:Ljava/util/regex/Pattern;

    .line 27
    return-void
.end method

.method public static findAudioItemFromUri(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    .locals 11
    .param p0, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p1, "audioUri"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 94
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v7, v6

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_2

    aget-object v0, v6, v5

    .line 95
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    const/4 v8, 0x1

    if-ne v3, v8, :cond_1

    .line 96
    const/4 v1, 0x0

    .line 97
    .local v1, "offset":I
    iget-object v8, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v9, v8

    move v3, v4

    :goto_1
    if-ge v3, v9, :cond_1

    aget-object v2, v8, v3

    .line 98
    .local v2, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasAudio()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getAudio()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getOriginalUri()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 99
    new-instance v3, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-direct {v3, v4, v1, v5, v2}, Lcom/google/apps/dots/android/newsstand/media/AudioItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .line 105
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .end local v1    # "offset":I
    .end local v2    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :goto_2
    return-object v3

    .line 101
    .restart local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .restart local v1    # "offset":I
    .restart local v2    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 97
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 94
    .end local v1    # "offset":I
    .end local v2    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 105
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_2
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public static findItemFromFieldId(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .locals 5
    .param p0, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 79
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 81
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v3, v1

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v0, v1, v2

    .line 82
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 87
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :goto_2
    return-object v0

    :cond_0
    move v1, v2

    .line 80
    goto :goto_0

    .line 81
    .restart local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 87
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static findValueFromMediaItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/android/newsstand/media/MediaItem;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    .locals 3
    .param p0, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p1, "mediaItem"    # Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .prologue
    .line 109
    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->fieldId:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->findItemFromFieldId(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item;

    move-result-object v0

    .line 110
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iget v2, p1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->offset:I

    aget-object v1, v1, v2

    goto :goto_0
.end method

.method public static getAudioItemsList(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/util/List;
    .locals 14
    .param p0, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/media/AudioItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 114
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 115
    .local v1, "mediaItemsList":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/media/AudioItem;>;"
    iget-object v7, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v8, v7

    move v6, v5

    :goto_0
    if-ge v6, v8, :cond_2

    aget-object v0, v7, v6

    .line 116
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    const/4 v9, 0x1

    if-ne v4, v9, :cond_1

    .line 117
    const/4 v2, 0x0

    .line 118
    .local v2, "offset":I
    iget-object v9, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v10, v9

    move v4, v5

    :goto_1
    if-ge v4, v10, :cond_1

    aget-object v3, v9, v4

    .line 119
    .local v3, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasAudio()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 120
    new-instance v11, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    iget-object v12, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget-object v13, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-direct {v11, v12, v2, v13, v3}, Lcom/google/apps/dots/android/newsstand/media/AudioItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 118
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 115
    .end local v2    # "offset":I
    .end local v3    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_1
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 127
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_2
    return-object v1
.end method

.method public static getFirstVideoItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Lcom/google/apps/dots/android/newsstand/media/VideoItem;
    .locals 11
    .param p0, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    const/4 v4, 0x0

    .line 131
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v7, v6

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_4

    aget-object v0, v6, v5

    .line 132
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    const/16 v8, 0x9

    if-eq v3, v8, :cond_0

    iget v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    const/16 v8, 0xc

    if-ne v3, v8, :cond_3

    .line 133
    :cond_0
    const/4 v1, 0x0

    .line 134
    .local v1, "offset":I
    iget-object v8, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v9, v8

    move v3, v4

    :goto_1
    if-ge v3, v9, :cond_3

    aget-object v2, v8, v3

    .line 135
    .local v2, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v10

    if-nez v10, :cond_1

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 136
    :cond_1
    new-instance v3, Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-direct {v3, v4, v1, v5, v2}, Lcom/google/apps/dots/android/newsstand/media/VideoItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .line 142
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .end local v1    # "offset":I
    .end local v2    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :goto_2
    return-object v3

    .line 138
    .restart local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .restart local v1    # "offset":I
    .restart local v2    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 134
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 131
    .end local v1    # "offset":I
    .end local v2    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 142
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_4
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public static mergeDuplicateFieldIdItems(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 12
    .param p0, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 46
    invoke-static {}, Lcom/google/common/collect/Maps;->newLinkedHashMap()Ljava/util/LinkedHashMap;

    move-result-object v5

    .line 47
    .local v5, "mergedItems":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;>;"
    iget-object v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v9, v8

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    if-ge v7, v9, :cond_2

    aget-object v3, v8, v7

    .line 48
    .local v3, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget-object v1, v3, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    .line 53
    .local v1, "fieldId":Ljava/lang/String;
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .line 54
    .local v4, "mergedItem":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    if-nez v4, :cond_1

    .line 55
    invoke-interface {v5, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    :cond_0
    :goto_1
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    .line 59
    :cond_1
    iget v6, v3, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    iget v10, v4, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    if-ne v6, v10, :cond_0

    .line 60
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item;->getSafeType()I

    move-result v6

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item;->getSafeType()I

    move-result v10

    if-ne v6, v10, :cond_0

    .line 63
    iget-object v6, v4, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iget-object v10, v3, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    const-class v11, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-static {v6, v10, v11}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iput-object v6, v4, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    goto :goto_1

    .line 67
    .end local v1    # "fieldId":Ljava/lang/String;
    .end local v3    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .end local v4    # "mergedItem":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_2
    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v6

    new-array v6, v6, [Lcom/google/apps/dots/proto/client/DotsShared$Item;

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .line 68
    const/4 v2, 0x0

    .line 69
    .local v2, "i":I
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 70
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;>;"
    iget-object v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    aput-object v6, v8, v2

    .line 71
    add-int/lit8 v2, v2, 0x1

    .line 72
    goto :goto_2

    .line 73
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;>;"
    :cond_3
    return-void
.end method
