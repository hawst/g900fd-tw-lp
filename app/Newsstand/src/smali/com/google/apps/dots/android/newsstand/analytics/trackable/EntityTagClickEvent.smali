.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;
.source "EntityTagClickEvent.java"


# instance fields
.field private final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 29
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 30
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 31
    return-void
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 3
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 95
    .local v0, "originalEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    const-string v1, "Entity Tag Click"

    invoke-virtual {p1, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 96
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 97
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 98
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 99
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 100
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 101
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setUserSubscriptionType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 103
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->addCuratedDimensionIfNecessary(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 105
    return-object p1
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    const/4 v14, 0x3

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 38
    sget-object v8, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent$1;->$SwitchMap$com$google$apps$dots$android$newsstand$util$ProtoEnum$EditionType:[I

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 84
    const/4 v5, 0x0

    .line 87
    .local v5, "screen":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 40
    .end local v5    # "screen":Ljava/lang/String;
    :pswitch_0
    const-string v5, "ReadNow"

    .line 41
    .restart local v5    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 43
    .end local v5    # "screen":Ljava/lang/String;
    :pswitch_1
    const-string v9, "%s - %s"

    new-array v10, v13, [Ljava/lang/Object;

    const-string v8, "[Topic]"

    aput-object v8, v10, v11

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v8, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    .line 44
    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getDescription()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v10, v12

    .line 43
    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 45
    .restart local v5    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 47
    .end local v5    # "screen":Ljava/lang/String;
    :pswitch_2
    const-string v9, "%s - %s"

    new-array v10, v13, [Ljava/lang/Object;

    const-string v8, "[Topic]"

    aput-object v8, v10, v11

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v8, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .line 48
    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v10, v12

    .line 47
    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 49
    .restart local v5    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 53
    .end local v5    # "screen":Ljava/lang/String;
    :pswitch_3
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v8, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 54
    .local v1, "owningEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v1, v8}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummary(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v2

    .line 55
    .local v2, "owningEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-object v8, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "appName":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v8, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->getSection(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Section;

    move-result-object v6

    .line 57
    .local v6, "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getName()Ljava/lang/String;

    move-result-object v7

    .line 58
    .local v7, "sectionName":Ljava/lang/String;
    const-string v8, "%s %s - %s"

    new-array v9, v14, [Ljava/lang/Object;

    const-string v10, "[Section]"

    aput-object v10, v9, v11

    aput-object v0, v9, v12

    aput-object v7, v9, v13

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 60
    .restart local v5    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 62
    .end local v0    # "appName":Ljava/lang/String;
    .end local v1    # "owningEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v2    # "owningEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .end local v5    # "screen":Ljava/lang/String;
    .end local v6    # "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    .end local v7    # "sectionName":Ljava/lang/String;
    :pswitch_4
    const-string v5, "Bookmarks"

    .line 63
    .restart local v5    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 65
    .end local v5    # "screen":Ljava/lang/String;
    :pswitch_5
    const-string v5, "Search"

    .line 66
    .restart local v5    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 68
    .end local v5    # "screen":Ljava/lang/String;
    :pswitch_6
    const-string v8, "%s %s - %s"

    new-array v9, v14, [Ljava/lang/Object;

    const-string v10, "[Magazine - TOC]"

    aput-object v10, v9, v11

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 69
    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppFamilyName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v13

    .line 68
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 70
    .restart local v5    # "screen":Ljava/lang/String;
    goto/16 :goto_0

    .line 74
    .end local v5    # "screen":Ljava/lang/String;
    :pswitch_7
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v8, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;->getPostId()Ljava/lang/String;

    move-result-object v4

    .line 76
    .local v4, "postId":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v8

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EntityTagClickEvent;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    sget-object v10, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    invoke-virtual {v8, v9, v4, v10}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 75
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 77
    .local v3, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    if-nez v3, :cond_1

    .line 78
    new-instance v9, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v10, "Could not find post for postId = "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual {v10, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    :goto_1
    invoke-direct {v9, v8}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_0
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 80
    :cond_1
    const-string v8, "%s %s - %s"

    new-array v9, v14, [Ljava/lang/Object;

    const-string v10, "[Related]"

    aput-object v10, v9, v11

    .line 81
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v13

    .line 80
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 82
    .restart local v5    # "screen":Ljava/lang/String;
    goto/16 :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
