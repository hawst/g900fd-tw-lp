.class public Lcom/google/apps/dots/android/newsstand/http/HttpModule$NSGoogleHttpClient;
.super Lcom/google/android/gms/http/GoogleHttpClient;
.source "HttpModule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/http/HttpModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "NSGoogleHttpClient"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x1388

    const/4 v2, 0x1

    .line 107
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, ""

    invoke-direct {p0, v0, v1, v2, v2}, Lcom/google/android/gms/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;ZZ)V

    .line 108
    iput v3, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$NSGoogleHttpClient;->connectTimeout:I

    .line 109
    iput v3, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$NSGoogleHttpClient;->readTimeout:I

    .line 111
    return-void
.end method

.method protected static isRedirect(Lorg/apache/http/HttpResponse;)Z
    .locals 4
    .param p0, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    const/4 v2, 0x0

    .line 161
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 162
    .local v1, "statusLine":Lorg/apache/http/StatusLine;
    if-eqz v1, :cond_1

    .line 163
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 164
    .local v0, "code":I
    const/16 v3, 0x12d

    if-eq v0, v3, :cond_0

    const/16 v3, 0x12e

    if-eq v0, v3, :cond_0

    const/16 v3, 0x133

    if-ne v0, v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 168
    .end local v0    # "code":I
    :cond_1
    return v2
.end method


# virtual methods
.method protected beforeSend(Ljava/net/HttpURLConnection;)V
    .locals 2
    .param p1, "connection"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 183
    invoke-super {p0, p1}, Lcom/google/android/gms/http/GoogleHttpClient;->beforeSend(Ljava/net/HttpURLConnection;)V

    .line 185
    const-string v0, "User-Agent"

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/google/apps/dots/android/newsstand/http/HttpModule;->getUserAgent(Landroid/content/Context;)Ljava/lang/String;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/http/HttpModule;->access$100(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 9
    .param p1, "host"    # Lorg/apache/http/HttpHost;
    .param p2, "request"    # Lorg/apache/http/HttpRequest;
    .param p3, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    invoke-static {}, Lcom/google/android/play/utils/collections/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    .local v4, "redirectUris":Ljava/util/Set;, "Ljava/util/Set<Ljava/net/URI;>;"
    move-object v0, p2

    .line 118
    check-cast v0, Lorg/apache/http/client/methods/HttpUriRequest;

    .line 119
    .local v0, "currentRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 121
    :goto_0
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v7

    const/4 v8, 0x6

    if-ge v7, v8, :cond_3

    .line 122
    invoke-virtual {p0, v0, p3}, Lcom/google/apps/dots/android/newsstand/http/HttpModule$NSGoogleHttpClient;->superExecute(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 123
    .local v6, "response":Lorg/apache/http/HttpResponse;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/http/HttpModule$NSGoogleHttpClient;->isRedirect(Lorg/apache/http/HttpResponse;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 124
    const-string v7, "Location"

    invoke-interface {v6, v7}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    .line 125
    .local v2, "locationHeader":Lorg/apache/http/Header;
    const/4 v3, 0x0

    .line 127
    .local v3, "newUri":Ljava/net/URI;
    if-nez v2, :cond_0

    const/4 v3, 0x0

    .line 132
    :goto_1
    if-nez v3, :cond_1

    .line 133
    new-instance v7, Ljava/io/IOException;

    const-string v8, "Invalid redirect URI"

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 127
    :cond_0
    :try_start_0
    new-instance v7, Ljava/net/URI;

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v3, v7

    goto :goto_1

    .line 136
    :cond_1
    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 137
    new-instance v7, Ljava/io/IOException;

    const-string v8, "Redirect loop"

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 140
    :cond_2
    instance-of v7, v0, Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 141
    const/4 v5, 0x0

    .line 143
    .local v5, "requestBase":Lorg/apache/http/client/methods/HttpRequestBase;
    :try_start_1
    check-cast v0, Lorg/apache/http/client/methods/HttpRequestBase;

    .end local v0    # "currentRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpRequestBase;->clone()Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "requestBase":Lorg/apache/http/client/methods/HttpRequestBase;
    check-cast v5, Lorg/apache/http/client/methods/HttpRequestBase;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 147
    .restart local v5    # "requestBase":Lorg/apache/http/client/methods/HttpRequestBase;
    invoke-virtual {v5, v3}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V

    .line 148
    move-object v0, v5

    .line 149
    .restart local v0    # "currentRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    .end local v0    # "currentRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    .end local v5    # "requestBase":Lorg/apache/http/client/methods/HttpRequestBase;
    :catch_0
    move-exception v1

    .line 145
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v7, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v7, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 157
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .end local v2    # "locationHeader":Lorg/apache/http/Header;
    .end local v3    # "newUri":Ljava/net/URI;
    .end local v6    # "response":Lorg/apache/http/HttpResponse;
    .restart local v0    # "currentRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_3
    new-instance v7, Ljava/io/IOException;

    const-string v8, "Stuck in a long redirect chain"

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 153
    .restart local v6    # "response":Lorg/apache/http/HttpResponse;
    :cond_4
    return-object v6

    .line 128
    .restart local v2    # "locationHeader":Lorg/apache/http/Header;
    .restart local v3    # "newUri":Ljava/net/URI;
    :catch_1
    move-exception v7

    goto :goto_1
.end method

.method protected superExecute(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 1
    .param p1, "request"    # Lorg/apache/http/HttpRequest;
    .param p2, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    instance-of v0, p1, Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v0, :cond_0

    .line 175
    check-cast p1, Lorg/apache/http/client/methods/HttpUriRequest;

    .end local p1    # "request":Lorg/apache/http/HttpRequest;
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0

    .line 177
    .restart local p1    # "request":Lorg/apache/http/HttpRequest;
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
