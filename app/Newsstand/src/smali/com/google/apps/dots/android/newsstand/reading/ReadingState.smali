.class public Lcom/google/apps/dots/android/newsstand/reading/ReadingState;
.super Ljava/lang/Object;
.source "ReadingState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/reading/ReadingState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field public final pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

.field public final postId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/reading/ReadingState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "postId"    # Ljava/lang/String;
    .param p3, "pageLocation"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 28
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->postId:Ljava/lang/String;

    .line 29
    if-nez p3, :cond_0

    new-instance p3, Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .end local p3    # "pageLocation":Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    invoke-direct {p3}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;-><init>()V

    :cond_0
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .line 30
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 34
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 35
    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    .line 36
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/reading/ReadingState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->postId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->postId:Ljava/lang/String;

    .line 37
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .line 38
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 40
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/reading/ReadingState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->postId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 50
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "{%s - %s/%s}"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->postId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->postId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 67
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
