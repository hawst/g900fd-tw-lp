.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;
.super Ljava/lang/Object;
.source "TitleIssuesFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final appFamilyId:Ljava/lang/String;

.field public final hideArchive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "appFamilyId"    # Ljava/lang/String;
    .param p2, "hideArchive"    # Z

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->appFamilyId:Ljava/lang/String;

    .line 19
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->hideArchive:Z

    .line 20
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 30
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 31
    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    .line 32
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->appFamilyId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->appFamilyId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->hideArchive:Z

    .line 33
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->hideArchive:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 35
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->appFamilyId:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 24
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "titleIssuesFragmentState: %s - %s}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->appFamilyId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->hideArchive:Z

    .line 25
    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 24
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->appFamilyId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->hideArchive:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 52
    return-void
.end method
