.class public final enum Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;
.super Ljava/lang/Enum;
.source "NSActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivityState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

.field public static final enum CONSTRUCTED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

.field public static final enum CREATED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

.field public static final enum DESTROYED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

.field public static final enum RESUMED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

.field public static final enum STARTED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87
    new-instance v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    const-string v1, "CONSTRUCTED"

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->CONSTRUCTED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 88
    new-instance v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    const-string v1, "CREATED"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->CREATED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 89
    new-instance v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->STARTED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 90
    new-instance v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    const-string v1, "RESUMED"

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->RESUMED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 91
    new-instance v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    const-string v1, "DESTROYED"

    invoke-direct {v0, v1, v6}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->DESTROYED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 86
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->CONSTRUCTED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->CREATED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->STARTED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->RESUMED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->DESTROYED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->$VALUES:[Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 86
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->$VALUES:[Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    return-object v0
.end method
