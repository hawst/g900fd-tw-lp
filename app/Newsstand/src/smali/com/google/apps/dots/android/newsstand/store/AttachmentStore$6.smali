.class Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$6;
.super Ljava/lang/Object;
.source "AttachmentStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/android/newsstand/server/Transform;",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$storeRequest:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 315
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$6;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$6;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$6;->val$storeRequest:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "defaultTransform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/server/Transform;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$6;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$500(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$6;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$6;->val$storeRequest:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform(Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 315
    check-cast p1, Lcom/google/apps/dots/android/newsstand/server/Transform;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$6;->apply(Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
