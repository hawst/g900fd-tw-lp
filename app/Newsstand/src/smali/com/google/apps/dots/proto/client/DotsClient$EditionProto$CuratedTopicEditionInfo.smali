.class public final Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CuratedTopicEditionInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;


# instance fields
.field private appFamilyId_:Ljava/lang/String;

.field private appId_:Ljava/lang/String;

.field private bitField0_:I

.field private description_:Ljava/lang/String;

.field private leadCurationClientEntityId_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2944
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2945
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2950
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    .line 2972
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    .line 2994
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    .line 3016
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 2945
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
    .locals 3

    .prologue
    .line 3051
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3055
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
    return-object v0

    .line 3052
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
    :catch_0
    move-exception v1

    .line 3053
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3060
    if-ne p1, p0, :cond_1

    .line 3066
    :cond_0
    :goto_0
    return v1

    .line 3061
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 3062
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    .line 3063
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 3064
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 3065
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 3066
    goto :goto_0

    .line 3063
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    .line 3064
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    .line 3065
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 3066
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getAppFamilyId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2952
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    return-object v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2974
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2996
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getLeadCurationClientEntityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3018
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3097
    const/4 v0, 0x0

    .line 3098
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3099
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    .line 3100
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3102
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3103
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    .line 3104
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3106
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 3107
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    .line 3108
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3110
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 3111
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 3112
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3114
    :cond_3
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->cachedSize:I

    .line 3115
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 3070
    const/16 v0, 0x11

    .line 3071
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 3072
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 3073
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 3074
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 3075
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_3
    add-int v0, v1, v2

    .line 3076
    return v0

    .line 3072
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 3073
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 3074
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 3075
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3123
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3124
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3128
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3129
    :sswitch_0
    return-object p0

    .line 3134
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    .line 3135
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    goto :goto_0

    .line 3139
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    .line 3140
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    goto :goto_0

    .line 3144
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    .line 3145
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    goto :goto_0

    .line 3149
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 3150
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    goto :goto_0

    .line 3124
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2941
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;

    move-result-object v0

    return-object v0
.end method

.method public setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2955
    if-nez p1, :cond_0

    .line 2956
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2958
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    .line 2959
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    .line 2960
    return-object p0
.end method

.method public setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2977
    if-nez p1, :cond_0

    .line 2978
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2980
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    .line 2981
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    .line 2982
    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2999
    if-nez p1, :cond_0

    .line 3000
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3002
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    .line 3003
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    .line 3004
    return-object p0
.end method

.method public setLeadCurationClientEntityId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 3021
    if-nez p1, :cond_0

    .line 3022
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3024
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 3025
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    .line 3026
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3081
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3082
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3084
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3085
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->appId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3087
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 3088
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->description_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3090
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 3091
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$CuratedTopicEditionInfo;->leadCurationClientEntityId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3093
    :cond_3
    return-void
.end method
