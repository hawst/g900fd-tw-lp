.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;
.super Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;
.source "ExploreSingleTopicHeaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->setupHeaderListLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 118
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->explore_single_topic_background:I

    const/4 v2, 0x0

    .line 119
    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 120
    .local v0, "backgroundView":Landroid/view/View;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->transition_image:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    # setter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->transitionDummyImage:Landroid/widget/ImageView;
    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$002(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 121
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->background_image:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    # setter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerImage:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$102(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 122
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerImage:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$100(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v1

    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .line 123
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->image_rotator_overlay_color:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3, v4}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 122
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 125
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 126
    return-void
.end method

.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 112
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->explore_single_topic_content:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 113
    .local v0, "contentView":Landroid/view/View;
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 114
    return-void
.end method

.method protected getContentProtectionMode()I
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x1

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 2

    .prologue
    .line 130
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x1

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method
