.class public interface abstract Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;
.super Ljava/lang/Object;
.source "AppFamilyList.java"


# static fields
.field public static final DK_APP_FAMILY_ID:I

.field public static final DK_APP_FAMILY_SUMMARY:I

.field public static final DK_DESCRIPTION:I

.field public static final DK_ICON_ID:I

.field public static final DK_NAME:I

.field public static final DK_NUM_ISSUES:I

.field public static final DK_UPDATED:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->AppFamilyList_appFamilyId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_APP_FAMILY_ID:I

    .line 10
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->AppFamilyList_appFamilySummary:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_APP_FAMILY_SUMMARY:I

    .line 11
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->AppFamilyList_name:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_NAME:I

    .line 12
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->AppFamilyList_iconId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_ICON_ID:I

    .line 13
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->AppFamilyList_description:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_DESCRIPTION:I

    .line 14
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->AppFamilyList_updated:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_UPDATED:I

    .line 15
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->AppFamilyList_numIssues:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_NUM_ISSUES:I

    return-void
.end method
