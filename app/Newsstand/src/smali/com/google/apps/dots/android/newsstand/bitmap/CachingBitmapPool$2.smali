.class Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;
.super Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;
.source "CachingBitmapPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getPoolBitmap(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/util/RetryWithGC",
        "<",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

.field final synthetic val$config:Landroid/graphics/Bitmap$Config;

.field final synthetic val$height:I

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;IILandroid/graphics/Bitmap$Config;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;->this$0:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    iput p2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;->val$width:I

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;->val$height:I

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;->val$config:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;-><init>()V

    return-void
.end method


# virtual methods
.method protected work()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 216
    const-string v0, "bitmap-create"

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;)I

    .line 218
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/async/JankLock;->blockUntilJankPermitted()V

    .line 219
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;->val$width:I

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;->val$height:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;->val$config:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 221
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    throw v0
.end method

.method protected bridge synthetic work()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$2;->work()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
