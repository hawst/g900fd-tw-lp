.class Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$1;
.super Ljava/lang/Object;
.source "DiskCache.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->unpinAll(Landroid/util/SparseIntArray;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

.field final synthetic val$changed:[Z

.field final synthetic val$defaultSnapshotId:I

.field final synthetic val$pinSnapshotIds:Landroid/util/SparseIntArray;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;[ZLandroid/util/SparseIntArray;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    .prologue
    .line 496
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$1;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$1;->val$changed:[Z

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$1;->val$pinSnapshotIds:Landroid/util/SparseIntArray;

    iput p4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$1;->val$defaultSnapshotId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;)V
    .locals 6
    .param p1, "region"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 499
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->touch()V

    .line 500
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 501
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$1;->val$changed:[Z

    const/4 v1, 0x0

    aget-boolean v2, v0, v1

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$1;->val$pinSnapshotIds:Landroid/util/SparseIntArray;

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$1;->val$defaultSnapshotId:I

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->unpinAll(Landroid/util/SparseIntArray;I)Z

    move-result v3

    or-int/2addr v2, v3

    aput-boolean v2, v0, v1

    .line 502
    return-void
.end method
