.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AltFormat"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;


# instance fields
.field private bitField0_:I

.field private format_:I

.field private index_:I

.field private objectId_:Ljava/lang/String;

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13910
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13911
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 13921
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->format_:I

    .line 13940
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->objectId_:Ljava/lang/String;

    .line 13962
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->type_:I

    .line 13981
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->index_:I

    .line 13911
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    .locals 3

    .prologue
    .line 14013
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 14017
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    return-object v0

    .line 14014
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    :catch_0
    move-exception v1

    .line 14015
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 13907
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 14022
    if-ne p1, p0, :cond_1

    .line 14026
    :cond_0
    :goto_0
    return v1

    .line 14023
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 14024
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 14025
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->format_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->format_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->objectId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->objectId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 14026
    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->type_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->index_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->index_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 14025
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->objectId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->objectId_:Ljava/lang/String;

    .line 14026
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getFormat()I
    .locals 1

    .prologue
    .line 13923
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->format_:I

    return v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 13983
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->index_:I

    return v0
.end method

.method public getObjectId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13942
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->objectId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 14059
    const/4 v0, 0x0

    .line 14060
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14061
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->format_:I

    .line 14062
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14064
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14065
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->objectId_:Ljava/lang/String;

    .line 14066
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14068
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 14069
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->type_:I

    .line 14070
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14072
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 14073
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->index_:I

    .line 14074
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14076
    :cond_3
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->cachedSize:I

    .line 14077
    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 13964
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->type_:I

    return v0
.end method

.method public hasFormat()Z
    .locals 1

    .prologue
    .line 13931
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 1

    .prologue
    .line 13972
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 14032
    const/16 v0, 0x11

    .line 14033
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 14034
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->format_:I

    add-int v0, v1, v2

    .line 14035
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->objectId_:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 14036
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->type_:I

    add-int v0, v1, v2

    .line 14037
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->index_:I

    add-int v0, v1, v2

    .line 14038
    return v0

    .line 14035
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->objectId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 14085
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 14086
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 14090
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 14091
    :sswitch_0
    return-object p0

    .line 14096
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 14097
    .local v1, "temp":I
    if-eq v1, v3, :cond_1

    if-ne v1, v4, :cond_2

    .line 14099
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->format_:I

    .line 14100
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    goto :goto_0

    .line 14102
    :cond_2
    iput v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->format_:I

    goto :goto_0

    .line 14107
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->objectId_:Ljava/lang/String;

    .line 14108
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    goto :goto_0

    .line 14112
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 14113
    .restart local v1    # "temp":I
    if-eq v1, v5, :cond_3

    if-eqz v1, :cond_3

    if-eq v1, v4, :cond_3

    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    const/4 v2, 0x4

    if-eq v1, v2, :cond_3

    const/4 v2, 0x5

    if-eq v1, v2, :cond_3

    const/16 v2, 0x8

    if-eq v1, v2, :cond_3

    const/16 v2, 0x9

    if-eq v1, v2, :cond_3

    const/16 v2, 0xb

    if-eq v1, v2, :cond_3

    const/16 v2, 0xc

    if-eq v1, v2, :cond_3

    const/16 v2, 0xd

    if-eq v1, v2, :cond_3

    const/16 v2, 0xe

    if-eq v1, v2, :cond_3

    const/16 v2, 0xf

    if-eq v1, v2, :cond_3

    const/16 v2, 0x10

    if-eq v1, v2, :cond_3

    const/16 v2, 0x11

    if-eq v1, v2, :cond_3

    const/16 v2, 0x12

    if-eq v1, v2, :cond_3

    const/16 v2, 0x13

    if-ne v1, v2, :cond_4

    .line 14130
    :cond_3
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->type_:I

    .line 14131
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    goto :goto_0

    .line 14133
    :cond_4
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->type_:I

    goto :goto_0

    .line 14138
    .end local v1    # "temp":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->index_:I

    .line 14139
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    goto/16 :goto_0

    .line 14086
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13907
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14043
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14044
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->format_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 14046
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14047
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->objectId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 14049
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 14050
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 14052
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 14053
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->index_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 14055
    :cond_3
    return-void
.end method
