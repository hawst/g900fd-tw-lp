.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter$1;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "MagazineTileFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;->transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;

.field final synthetic val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 54
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineClickEvent;

    const-string v1, "MyMagazines"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineClickEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    const/4 v1, 0x1

    .line 55
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineClickEvent;->track(Z)V

    .line 57
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v0, p2, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->source_icon:I

    .line 58
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->setTransitionHeroElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->start()V

    .line 60
    return-void
.end method
