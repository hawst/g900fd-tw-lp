.class public Lcom/google/apps/dots/android/newsstand/widget/WifiOnlyDownloadDialog;
.super Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;
.source "WifiOnlyDownloadDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "OneTimeDownloadDialog"


# instance fields
.field private final edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/WifiOnlyDownloadDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 30
    return-void
.end method

.method public static show(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 2
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 33
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/WifiOnlyDownloadDialog;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/WifiOnlyDownloadDialog;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 34
    .local v0, "dialog":Lcom/google/apps/dots/android/newsstand/widget/WifiOnlyDownloadDialog;
    const-string v1, "OneTimeDownloadDialog"

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->showSupportDialogCarefully(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V

    .line 35
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v3, 0x1

    .line 49
    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    .line 51
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WifiOnlyDownloadDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 52
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->pinEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v1

    .line 53
    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v1

    .line 54
    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setWifiOnlyDownloadOverride(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    const/4 v1, -0x3

    if-ne p2, v1, :cond_0

    .line 58
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 59
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 60
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WifiOnlyDownloadDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->pin(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V

    .line 61
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/toast/Toasts;->notifyUserOfDownloadLater()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/WifiOnlyDownloadDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->wifi_only_download_override_dialog_title:I

    .line 40
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->wifi_only_download_override_dialog_message:I

    .line 41
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->wifi_only_download_override_dialog_button_download_now:I

    .line 42
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->wifi_only_download_override_dialog_button_download_later:I

    .line 43
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
