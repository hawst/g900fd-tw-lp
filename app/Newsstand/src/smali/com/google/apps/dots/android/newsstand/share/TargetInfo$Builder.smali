.class public Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;
.super Ljava/lang/Object;
.source "TargetInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/share/TargetInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private appFamilyId:Ljava/lang/String;

.field private appId:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field private offerId:Ljava/lang/String;

.field private postId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->appFamilyId:Ljava/lang/String;

    .line 88
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->appId:Ljava/lang/String;

    .line 89
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->postId:Ljava/lang/String;

    .line 90
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->offerId:Ljava/lang/String;

    .line 91
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->description:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public build()Lcom/google/apps/dots/android/newsstand/share/TargetInfo;
    .locals 7

    .prologue
    .line 146
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->appFamilyId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->appId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->postId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->offerId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;Ljava/lang/String;)V

    return-object v0
.end method

.method public getAppFamilyId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->appFamilyId:Ljava/lang/String;

    return-object v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->appId:Ljava/lang/String;

    return-object v0
.end method

.method public getPostId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->postId:Ljava/lang/String;

    return-object v0
.end method

.method public setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;
    .locals 0
    .param p1, "appFamilyId"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->appFamilyId:Ljava/lang/String;

    .line 100
    return-object p0
.end method

.method public setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->appId:Ljava/lang/String;

    .line 105
    return-object p0
.end method

.method public setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;
    .locals 0
    .param p1, "location"    # Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 95
    return-object p0
.end method

.method public setOfferId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;
    .locals 0
    .param p1, "offerId"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->offerId:Ljava/lang/String;

    .line 115
    return-object p0
.end method

.method public setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;
    .locals 0
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->postId:Ljava/lang/String;

    .line 110
    return-object p0
.end method
