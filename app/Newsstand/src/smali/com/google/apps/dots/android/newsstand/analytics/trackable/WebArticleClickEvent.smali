.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
.source "WebArticleClickEvent.java"


# instance fields
.field private final publisher:Ljava/lang/String;

.field private final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private final title:Ljava/lang/String;

.field private final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "publisher"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "url"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;-><init>()V

    .line 28
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 29
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->publisher:Ljava/lang/String;

    .line 30
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->title:Ljava/lang/String;

    .line 31
    invoke-static {p4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->url:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 2
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 95
    const-string v0, "Article Click"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->url:Ljava/lang/String;

    .line 96
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->title:Ljava/lang/String;

    .line 97
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->publisher:Ljava/lang/String;

    .line 98
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->publisher:Ljava/lang/String;

    .line 99
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    const/4 v1, 0x0

    .line 100
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 102
    const-string v0, "WebArticleEvent"

    const/4 v1, 0x1

    .line 103
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    .line 102
    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->addCuratedDimensionIfNecessary(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 106
    return-object p1
.end method

.method protected fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "publisherEvent"    # Z

    .prologue
    .line 88
    const-string v0, "Internal"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setCategory(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    return-object v0
.end method

.method protected getPublisherTrackingId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 37
    sget-object v3, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent$1;->$SwitchMap$com$google$apps$dots$android$newsstand$util$ProtoEnum$EditionType:[I

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 71
    const/4 v2, 0x0

    .line 74
    .local v2, "screen":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 39
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_0
    const-string v2, "ReadNow"

    .line 40
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 42
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_1
    const-string v4, "%s - %s"

    new-array v5, v8, [Ljava/lang/Object;

    const-string v3, "[Topic]"

    aput-object v3, v5, v6

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    .line 43
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getDescription()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    .line 42
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 44
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 46
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_2
    const-string v4, "%s - %s"

    new-array v5, v8, [Ljava/lang/Object;

    const-string v3, "[Topic]"

    aput-object v3, v5, v6

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .line 47
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    .line 46
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 48
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 50
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_3
    new-instance v3, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v4, "Sections should not display web articles."

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 52
    :pswitch_4
    new-instance v3, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v4, "Bookmarks should not display web articles."

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 54
    :pswitch_5
    const-string v2, "Search"

    .line 55
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 57
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_6
    new-instance v3, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v4, "Magazines should not display web articles."

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 61
    :pswitch_7
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;->getPostId()Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "postId":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    invoke-virtual {v3, v4, v1, v5}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 62
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 64
    .local v0, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    if-nez v0, :cond_1

    .line 65
    new-instance v4, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v5, "Could not find post for postId = "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-direct {v4, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 67
    :cond_1
    const-string v3, "%s %s - %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "[Related]"

    aput-object v5, v4, v6

    .line 68
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 67
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 69
    .restart local v2    # "screen":Ljava/lang/String;
    goto/16 :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
