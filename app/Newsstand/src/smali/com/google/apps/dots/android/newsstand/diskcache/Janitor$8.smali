.class Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$8;
.super Ljava/lang/Object;
.source "Janitor.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->closeStoreFiles()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 350
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$8;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;)V
    .locals 8
    .param p1, "region"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 353
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    iget-wide v6, v3, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->lastAccessedTime:J

    sub-long v0, v4, v6

    .line 354
    .local v0, "ago":J
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v2

    .line 355
    .local v2, "storeFile":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 356
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$8;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$1100(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    move-result-object v3

    iget-wide v4, v3, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->openFileTimeoutMs:J

    cmp-long v3, v0, v4

    if-lez v3, :cond_1

    .line 357
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->close(Z)V

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->flush()V

    goto :goto_0
.end method
