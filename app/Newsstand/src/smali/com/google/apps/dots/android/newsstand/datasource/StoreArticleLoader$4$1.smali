.class Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4$1;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;->apply(Lcom/google/apps/dots/proto/client/DotsShared$Form;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;",
        "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4$1;->this$1:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .locals 2
    .param p1, "formTemplate"    # Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;

    .prologue
    .line 190
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->getTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getTemplateForDevice(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 187
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4$1;->apply(Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    return-object v0
.end method
