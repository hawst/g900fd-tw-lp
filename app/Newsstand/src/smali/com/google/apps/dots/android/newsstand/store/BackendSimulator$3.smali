.class Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$3;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;
.source "BackendSimulator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->handleSavedPost(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor",
        "<",
        "Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

.field final synthetic val$alreadySaved:[Z

.field final synthetic val$postId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;Ljava/lang/String;[Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$3;->val$postId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$3;->val$alreadySaved:[Z

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 0

    .prologue
    .line 341
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$3;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 3
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;
    .param p2, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 344
    iget-object v0, p2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$3;->val$postId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$3;->val$alreadySaved:[Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    .line 346
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;->finish()V

    .line 348
    :cond_0
    return-void
.end method
