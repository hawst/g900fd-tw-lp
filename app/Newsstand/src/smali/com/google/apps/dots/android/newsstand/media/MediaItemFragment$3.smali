.class Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;
.super Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;
.source "MediaItemFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->getImageView(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

.field final synthetic val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;->val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOnDoubleTapUp(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    const v4, 0x3d4ccccd    # 0.05f

    .line 127
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;->val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getZoom()F

    move-result v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;->val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getOriginalZoom()F

    move-result v3

    add-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;->val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 128
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getZoom()F

    move-result v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;->val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getOriginalZoom()F

    move-result v3

    sub-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    const/4 v0, 0x1

    .line 129
    .local v0, "isDefaultZoom":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 130
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;->val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->zoomBy(FFF)V

    .line 134
    :goto_1
    return v1

    .end local v0    # "isDefaultZoom":Z
    :cond_0
    move v0, v1

    .line 128
    goto :goto_0

    .line 132
    .restart local v0    # "isDefaultZoom":Z
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;->val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->resetZoom()V

    goto :goto_1
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->toggleLightsOnMode()V

    .line 122
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
