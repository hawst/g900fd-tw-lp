.class public Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
.super Ljava/lang/Object;
.source "MutationResponse.java"


# instance fields
.field public final simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

.field public final storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

.field public final version:Lcom/google/apps/dots/android/newsstand/store/Version;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/android/newsstand/store/Version;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V
    .locals 0
    .param p1, "simulatedRoot"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .param p2, "version"    # Lcom/google/apps/dots/android/newsstand/store/Version;
    .param p3, "storeResponse"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .line 15
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    .line 16
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    .line 17
    return-void
.end method
