.class final Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState$1;
.super Ljava/lang/Object;
.source "TitleIssuesFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "appFamilyId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 61
    .local v1, "hideArchive":Z
    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    invoke-direct {v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;-><init>(Ljava/lang/String;Z)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 66
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    move-result-object v0

    return-object v0
.end method
