.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "SortByDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 23
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->sort_by:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 24
    sget v1, Lcom/google/android/apps/newsstanddev/R$array;->my_magazines_sort_by_items:I

    .line 25
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getMyMagazinesSortByField()I

    move-result v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;)V

    .line 24
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 34
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
