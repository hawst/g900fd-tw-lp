.class Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge$1;
.super Ljava/lang/Object;
.source "JavascriptInterfaceInjector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;->updateSnapPoints(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;

.field final synthetic val$stringSnapPoints:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge$1;->val$stringSnapPoints:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 104
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v2, "snapPointsList":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->COMMA_SPLITTER:Lcom/google/common/base/Splitter;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$900()Lcom/google/common/base/Splitter;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge$1;->val$stringSnapPoints:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 106
    .local v3, "stringSnapPoint":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/common/primitives/Ints;->tryParse(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 107
    .local v0, "integerSnapPoint":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 108
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$1000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v5, "Incomprehensible snap point \"%s\""

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    .end local v0    # "integerSnapPoint":Ljava/lang/Integer;
    .end local v3    # "stringSnapPoint":Ljava/lang/String;
    :goto_1
    return-void

    .line 111
    .restart local v0    # "integerSnapPoint":Ljava/lang/Integer;
    .restart local v3    # "stringSnapPoint":Ljava/lang/String;
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    .end local v0    # "integerSnapPoint":Ljava/lang/Integer;
    .end local v3    # "stringSnapPoint":Ljava/lang/String;
    :cond_1
    invoke-static {v2}, Lcom/google/common/primitives/Ints;->toArray(Ljava/util/Collection;)[I

    move-result-object v1

    .line 114
    .local v1, "snapPointsArray":[I
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->webView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reportUpdatedSnapPoints([I)V

    goto :goto_1
.end method
