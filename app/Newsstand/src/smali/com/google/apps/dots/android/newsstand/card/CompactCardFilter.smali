.class public Lcom/google/apps/dots/android/newsstand/card/CompactCardFilter;
.super Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;
.source "CompactCardFilter.java"


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 18
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "useCompactLists"

    aput-object v2, v1, v3

    invoke-direct {p0, v0, v3, v1}, Lcom/google/apps/dots/android/newsstand/data/PreferenceTrackingFilter;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;Z[Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 6
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->useCompactMode()Z

    move-result v2

    .line 24
    .local v2, "useCompactMode":Z
    if-eqz v2, :cond_1

    .line 25
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 26
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 27
    .local v1, "layoutResId":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 28
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->toCompactLayout(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 32
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "layoutResId":Ljava/lang/Integer;
    :cond_1
    return-object p1
.end method
