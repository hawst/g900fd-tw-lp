.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;
.super Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;
.source "MagazineEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->setupHeaderListLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 248
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->magazine_edition_background:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    # setter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->backgroundView:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$902(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Landroid/view/View;)Landroid/view/View;

    .line 297
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->backgroundView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$900(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 300
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 301
    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->backgroundView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$900(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->header_image_rotator:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .line 300
    # setter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1002(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .line 302
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1000(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    move-result-object v0

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 303
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->image_rotator_overlay_color:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 302
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 305
    return-void
.end method

.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 267
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->magazine_edition_content:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 268
    .local v0, "contentView":Landroid/view/View;
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 270
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    # setter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$402(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 271
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 292
    return-void
.end method

.method protected addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 251
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->magazine_edition_hero:I

    const/4 v2, 0x0

    .line 252
    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 251
    # setter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$102(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 253
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 262
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 263
    return-void
.end method

.method protected getHeaderHeight()I
    .locals 2

    .prologue
    .line 309
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->MAGAZINE_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x1

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    return v0
.end method
