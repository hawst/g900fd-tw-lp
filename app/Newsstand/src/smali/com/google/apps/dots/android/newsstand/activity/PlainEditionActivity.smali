.class public Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "PlainEditionActivity.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private plainEditionFragment:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>()V

    return-void
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 49
    if-nez p1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    const-string v0, "PlainEditionFragment_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;->plainEditionFragment:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->handleExtras(Landroid/os/Bundle;)Z

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 37
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;->plainEditionFragment:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;->plainEditionFragment:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->plain_edition_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;->setContentView(I)V

    .line 27
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->plain_edition_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;->plainEditionFragment:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .line 29
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 32
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 33
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 43
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onNewIntent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 45
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 46
    return-void
.end method
