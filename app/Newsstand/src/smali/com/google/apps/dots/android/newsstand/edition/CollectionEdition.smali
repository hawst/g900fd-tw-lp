.class public abstract Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
.super Lcom/google/apps/dots/android/newsstand/edition/Edition;
.source "CollectionEdition.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method protected constructor <init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V
    .locals 0
    .param p1, "editionProto"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 45
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method


# virtual methods
.method protected addSyncOnSuccessCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 2
    .param p2, "refreshedEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 168
    .local p1, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    instance-of v1, p2, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v1, :cond_0

    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .line 170
    .end local p2    # "refreshedEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 172
    .local v0, "editionToSync":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :goto_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$3;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$3;-><init>(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 182
    return-void

    .end local v0    # "editionToSync":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .restart local p2    # "refreshedEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    move-object v0, p2

    .line 170
    goto :goto_0
.end method

.method public cardList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->cardListBuilder()Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    move-result-object v0

    .line 63
    .local v0, "cardListBuilder":Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->useSpacerTopPadding(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 64
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    return-object v1
.end method

.method protected abstract getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
.end method

.method public headerImageList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 87
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->supportsHeaderImages(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    const/4 v0, 0x0

    .line 90
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->EQUALITY_FIELDS:[I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$1;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    invoke-direct {v2, p0, v3}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    goto :goto_0
.end method

.method public rawCardList(Landroid/content/Context;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->rawCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public readStateCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 78
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->readStatesUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p0, v0}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->getCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    move-result-object v0

    return-object v0
.end method

.method public readStatesUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->supportsReadStates()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getAppReadStatesUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;
.end method

.method public readingList(Landroid/content/Context;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->readingList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public refresh(Landroid/content/Context;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clearCardList"    # Z
    .param p3, "syncOnSuccess"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "ZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 116
    if-eqz p2, :cond_1

    .line 117
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;

    move-result-object v0

    .line 118
    .local v0, "editionCardList":Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
    const-wide/16 v4, 0x1388

    invoke-virtual {v0, v3, v4, v5, v3}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->freshen(ZJZ)V

    .line 121
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 122
    .local v1, "freshenFuture":Lcom/google/common/util/concurrent/SettableFuture;, "Lcom/google/common/util/concurrent/SettableFuture<Ljava/lang/Object;>;"
    new-instance v3, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$2;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$2;-><init>(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 131
    move-object v2, v1

    .line 136
    .end local v0    # "editionCardList":Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
    .end local v1    # "freshenFuture":Lcom/google/common/util/concurrent/SettableFuture;, "Lcom/google/common/util/concurrent/SettableFuture<Ljava/lang/Object;>;"
    .local v2, "result":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    :goto_0
    if-eqz p3, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v3

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {v3, v4, p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 137
    invoke-virtual {p0, v2, p0}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->addSyncOnSuccessCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 140
    :cond_0
    return-object v2

    .line 133
    .end local v2    # "result":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p0, p1, v3}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->requestFreshVersion(Landroid/content/Context;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .restart local v2    # "result":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    goto :goto_0
.end method

.method public requestFreshVersion(Landroid/content/Context;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isBackground"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 147
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    .line 148
    .local v2, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 149
    .local v0, "futures":Ljava/util/List;, "Ljava/util/List<Lcom/google/common/util/concurrent/ListenableFuture<*>;>;"
    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 150
    .local v3, "uri":Ljava/lang/String;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v5, v3, v6}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->freshVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v1

    .line 151
    .local v1, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    if-eqz p2, :cond_0

    .line 152
    sget-object v5, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    invoke-virtual {v1, v5}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .line 154
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v5

    invoke-virtual {v5, v2, v1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 156
    .end local v1    # "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .end local v3    # "uri":Ljava/lang/String;
    :cond_1
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList(Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    return-object v4
.end method

.method protected supportsHeaderImages(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Z
    .locals 1
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 82
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getUseHeaderBackgroundImages()Z

    move-result v0

    return v0
.end method

.method public supportsRefresh()Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x1

    return v0
.end method

.method public abstract syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
