.class public Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;
.super Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;
.source "SearchResultsList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;,
        Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    }
.end annotation


# instance fields
.field public final query:Ljava/lang/String;

.field public final type:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    .param p3, "query"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;-><init>(Landroid/content/Context;)V

    .line 33
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->type:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    .line 34
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->query:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 46
    sget-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$1;->$SwitchMap$com$google$apps$dots$android$newsstand$search$SearchResultsList$Type:[I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->type:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 58
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 48
    :pswitch_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->query:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getPostSearchResults(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 50
    :pswitch_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->query:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMagazineSearchResults(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 52
    :pswitch_2
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->query:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getEditionSearchResults(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 54
    :pswitch_3
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->query:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getFeedSearchResults(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 56
    :pswitch_4
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->query:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getEntitySearchResults(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected visitor(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
    .locals 6
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p3, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 40
    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->appContext:Landroid/content/Context;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;-><init>(Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    return-object v0
.end method
