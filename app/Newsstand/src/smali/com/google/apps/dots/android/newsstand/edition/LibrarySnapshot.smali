.class public Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;
.super Ljava/lang/Object;
.source "LibrarySnapshot.java"


# instance fields
.field private final offersStatusSnapshot:Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;

.field private final purchasedEditionSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation
.end field

.field public final subscribedEditionSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/util/Set;Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;)V
    .locals 0
    .param p3, "offersStatusSnapshot"    # Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;",
            "Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;",
            ")V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, "subscribedEditionSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    .local p2, "purchasedEditionSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->subscribedEditionSet:Ljava/util/Set;

    .line 20
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->purchasedEditionSet:Ljava/util/Set;

    .line 21
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->offersStatusSnapshot:Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;

    .line 22
    return-void
.end method


# virtual methods
.method public getOffersStatusSnapshot()Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->offersStatusSnapshot:Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;

    return-object v0
.end method

.method public isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->purchasedEditionSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->subscribedEditionSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->offersStatusSnapshot:Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;

    .line 26
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;->isOfferLocallyAcceptedForEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
