.class public Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;
.super Ljava/lang/Object;
.source "UriDispatcher.java"


# static fields
.field private static final HTTP:Ljava/util/regex/Pattern;

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const-class v0, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 35
    const-string v0, "https?"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->HTTP:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getOfferTypeParameterQuietly(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/util/OfferType;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 174
    const-string v2, "offer_type"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getIntQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 175
    .local v0, "rValue":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/OfferType;->forProtoValue(I)Lcom/google/apps/dots/android/newsstand/util/OfferType;

    move-result-object v2

    .line 182
    :goto_0
    return-object v2

    .line 178
    :cond_0
    const-string v2, "offer_type"

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getStringQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 179
    .local v1, "sValue":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 180
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/OfferType;->valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/OfferType;

    move-result-object v2

    goto :goto_0

    .line 182
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isHttp(Landroid/net/Uri;)Z
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 138
    sget-object v0, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->HTTP:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public static maybeRewriteWebviewRequestUri(Landroid/webkit/WebView;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 8
    .param p0, "view"    # Landroid/webkit/WebView;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 142
    const-string v2, "newsstand-content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 143
    invoke-virtual {p0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 150
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "http"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    .line 151
    sget-object v2, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Rewrote %s:// protocol to http:// -- %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "newsstand-content"

    aput-object v5, v4, v6

    aput-object p1, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    :cond_0
    :goto_0
    return-object p1

    .line 153
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isGoogleAdHost(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-le v2, v3, :cond_0

    .line 156
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->adShieldClient()Lcom/google/android/gms/ads/adshield/AdShieldClient;

    move-result-object v0

    .line 158
    .local v0, "adShieldClient":Lcom/google/android/gms/ads/adshield/AdShieldClient;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/ads/adshield/AdShieldClient;->getSignalsUrlKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 159
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/ads/adshield/AdShieldClient;->addSignalsToAdRequest(Landroid/net/Uri;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p1

    .line 160
    sget-object v2, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Added signals to ad uri: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/ads/adshield/UrlParseException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 162
    :catch_0
    move-exception v1

    .line 164
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Error adding signals to ad request: %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 165
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 167
    .local v1, "e":Lcom/google/android/gms/ads/adshield/UrlParseException;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "AdShield couldn\'t process URI %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static show(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/net/Uri;)V
    .locals 6
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 47
    sget-object v3, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "show %s"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object p1, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->startInAppPurchaseIfRequested(Landroid/app/Activity;Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 49
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->showInPlayStoreIfRequested(Landroid/app/Activity;Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 50
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->startNewsstandPreviewIfRequested(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 51
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->showInBrowser(Landroid/app/Activity;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move v0, v2

    .line 52
    .local v0, "shown":Z
    :goto_0
    if-nez v0, :cond_1

    .line 53
    sget-object v3, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "could not show %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-virtual {v3, v4, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    :cond_1
    return-void

    .end local v0    # "shown":Z
    :cond_2
    move v0, v1

    .line 51
    goto :goto_0
.end method

.method public static showInBrowser(Landroid/app/Activity;Landroid/net/Uri;)Z
    .locals 8
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 59
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->isHttp(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 60
    sget-object v3, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "showing in browser %s"

    new-array v5, v6, [Ljava/lang/Object;

    aput-object p1, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/BrowserIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/BrowserIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 66
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;
    :goto_0
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;->setUri(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;->start()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :goto_1
    return v6

    .line 63
    .end local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;-><init>(Landroid/app/Activity;)V

    .restart local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;
    goto :goto_0

    .line 67
    :catch_0
    move-exception v1

    .line 68
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Couldn\'t find activity to open URL %s"

    new-array v5, v6, [Ljava/lang/Object;

    aput-object p1, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->nonhttp_url:I

    new-array v5, v6, [Ljava/lang/Object;

    aput-object p1, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 70
    .local v2, "msg":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-static {p0, v2, v3, v6}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->showToast(Landroid/app/Activity;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;Z)V

    goto :goto_1
.end method

.method public static showInPlayStoreIfRequested(Landroid/app/Activity;Landroid/net/Uri;)Z
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 105
    sget-object v2, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "show in play store if requested? %s"

    new-array v4, v0, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->isPlayStoreUri(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    sget-object v2, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Showing uri %s in the Play Store app"

    new-array v4, v0, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setPath(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->start()V

    .line 111
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static startInAppPurchaseIfRequested(Landroid/app/Activity;Landroid/net/Uri;)Z
    .locals 9
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 76
    sget-object v4, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Start play store purchase if requested? %s"

    new-array v8, v5, [Ljava/lang/Object;

    aput-object p1, v8, v6

    invoke-virtual {v4, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    const-string v4, "newsstand_in_app_purchase"

    .line 78
    invoke-static {p1, v4}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getStringQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 79
    .local v2, "inAppPurchaseContext":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->isInAppPurchaseUri(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 80
    sget-object v4, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Starting in-app purchase for uri %s"

    new-array v8, v5, [Ljava/lang/Object;

    aput-object p1, v8, v6

    invoke-virtual {v4, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    const/4 v3, 0x0

    .line 83
    .local v3, "provider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
    :try_start_0
    instance-of v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;

    if-eqz v4, :cond_0

    .line 84
    move-object v0, p0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;

    move-object v4, v0

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->getIAPConversionEventProvider()Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;

    move-result-object v3

    .line 87
    :cond_0
    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    invoke-direct {v4, p0, v3}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;)V

    .line 88
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->getOfferTypeParameterQuietly(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/util/OfferType;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setOfferType(Lcom/google/apps/dots/android/newsstand/util/OfferType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v4

    const-string v7, "offer_filter"

    .line 89
    invoke-static {p1, v7}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getStringQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setOfferFilter(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v4

    .line 90
    invoke-virtual {v4, p1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setPlayStoreUri(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v4

    const-string v7, "house_ad"

    .line 91
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v4, v7}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setIsHouseAd(Z)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v4

    const/4 v7, 0x1

    .line 92
    invoke-virtual {v4, v7}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setShowPurchaseToast(Z)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v4

    .line 93
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->start()V
    :try_end_0
    .catch Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException; {:try_start_0 .. :try_end_0} :catch_0

    move v4, v5

    .line 100
    .end local v3    # "provider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
    :goto_0
    return v4

    .line 95
    .restart local v3    # "provider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
    :catch_0
    move-exception v1

    .line 96
    .local v1, "e":Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Failed to start in-app purchase: %s"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-virtual {v4, v7, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move v4, v6

    .line 97
    goto :goto_0

    .end local v1    # "e":Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;
    .end local v3    # "provider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
    :cond_1
    move v4, v6

    .line 100
    goto :goto_0
.end method

.method public static startNewsstandPreviewIfRequested(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/net/Uri;)Z
    .locals 5
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 116
    const-string v0, "newsstand_preview"

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getStringQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 133
    :goto_0
    return v0

    .line 118
    :cond_0
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->isBooksAppUri(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    sget-object v0, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Starting preview for books uri %s"

    new-array v4, v2, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-virtual {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 121
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->setIsPreview(Z)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;

    .line 122
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->setWebUri(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;

    .line 123
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->setAddToMyEbooks(Z)Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;

    move-result-object v0

    .line 124
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->setPromptBeforeAdding(Z)Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->start()V

    move v0, v2

    .line 126
    goto :goto_0

    .line 127
    :cond_1
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->isMovieTrailerUri(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    sget-object v0, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Starting preview for movie trailer uri %s"

    new-array v4, v2, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-virtual {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->setIsPreview(Z)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->setWebUri(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->start()V

    move v0, v2

    .line 130
    goto :goto_0

    .line 132
    :cond_2
    sget-object v0, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Didn\'t recognize preview uri %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-virtual {v0, v3, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->showInBrowser(Landroid/app/Activity;Landroid/net/Uri;)Z

    move-result v0

    goto :goto_0
.end method
