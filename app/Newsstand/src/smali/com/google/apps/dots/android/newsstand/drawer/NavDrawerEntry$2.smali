.class final Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$2;
.super Ljava/lang/Object;
.source "NavDrawerEntry.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    .locals 6
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 140
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 141
    .local v1, "entryTypeInt":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;->values()[Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;

    move-result-object v4

    aget-object v0, v4, v1

    .line 142
    .local v0, "entryType":Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$3;->$SwitchMap$com$google$apps$dots$android$newsstand$drawer$NavDrawerEntry$EntryType:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 152
    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4}, Ljava/lang/IllegalStateException;-><init>()V

    throw v4

    .line 145
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 146
    .local v2, "homePageTypeInt":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->values()[Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    move-result-object v4

    aget-object v4, v4, v2

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->getNavDrawerEntryFromHomePageType(Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;)Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    move-result-object v4

    .line 150
    .end local v2    # "homePageTypeInt":I
    :goto_0
    return-object v4

    .line 149
    :pswitch_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 150
    .local v3, "miscTypeInt":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->values()[Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    move-result-object v4

    aget-object v4, v4, v3

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->getNavDrawerEntryFromMiscType(Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;)Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    move-result-object v4

    goto :goto_0

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$2;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 157
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$2;->newArray(I)[Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    move-result-object v0

    return-object v0
.end method
