.class Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "AudioPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setupAudio()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 422
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 8
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    const/4 v7, 0x0

    .line 425
    if-nez p1, :cond_0

    .line 426
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdateAndStopSelf()V
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1000(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    .line 455
    :goto_0
    return-void

    .line 429
    :cond_0
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->getAudioItemsList(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/util/List;

    move-result-object v0

    .line 430
    .local v0, "audioItemsInPost":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/media/AudioItem;>;"
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->indexInPost:I
    invoke-static {v5, v7}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1102(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;I)I

    .line 431
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 432
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1200(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 433
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->indexInPost:I
    invoke-static {v5, v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1102(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;I)I

    .line 437
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1200(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->findValueFromMediaItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/android/newsstand/media/MediaItem;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v4

    .line 438
    .local v4, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasAudio()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getAudio()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getOriginalUri()Ljava/lang/String;

    move-result-object v1

    .line 440
    .local v1, "audioUri":Ljava/lang/String;
    :goto_2
    :try_start_0
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$300(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 441
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$300(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v5

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 442
    :catch_0
    move-exception v2

    .line 443
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1300()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v5

    const-string v6, "Error trying to set data source"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v2, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 444
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdateAndStopSelf()V
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1000(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    goto :goto_0

    .line 431
    .end local v1    # "audioUri":Ljava/lang/String;
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .end local v4    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 438
    .restart local v4    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_3
    const-string v1, ""

    goto :goto_2

    .line 445
    .restart local v1    # "audioUri":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 446
    .local v2, "e":Ljava/lang/SecurityException;
    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1300()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v5

    const-string v6, "Security exception setting data source"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v2, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 447
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdateAndStopSelf()V
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1000(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    goto :goto_0

    .line 448
    .end local v2    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v2

    .line 449
    .local v2, "e":Ljava/lang/IllegalStateException;
    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1300()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v5

    const-string v6, "Tried to set data source when mediaPlayer is in the wrong state"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v2, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 450
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdateAndStopSelf()V
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1000(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    goto/16 :goto_0

    .line 451
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v2

    .line 452
    .local v2, "e":Ljava/io/IOException;
    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1300()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v5

    const-string v6, "Error trying to set data source"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v2, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 453
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdateAndStopSelf()V
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1000(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 422
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
