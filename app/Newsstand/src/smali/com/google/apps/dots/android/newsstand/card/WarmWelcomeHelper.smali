.class public Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;
.super Ljava/lang/Object;
.source "WarmWelcomeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    .line 30
    return-void
.end method

.method public static inContext(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method protected addButton1Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "textId"    # I
    .param p3, "onClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 72
    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BUTTON_1_TEXT:I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    invoke-virtual {v1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 73
    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BUTTON_1_ON_CLICK_LISTENER:I

    invoke-virtual {p1, v0, p3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 74
    return-void
.end method

.method protected addButton2Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "textId"    # I
    .param p3, "onClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 77
    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BUTTON_2_TEXT:I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    invoke-virtual {v1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 78
    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BUTTON_2_ON_CLICK_LISTENER:I

    invoke-virtual {p1, v0, p3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 79
    return-void
.end method

.method protected addCommonFields(Lcom/google/android/libraries/bind/data/Data;III)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "logoDrawableId"    # I
    .param p3, "titleTextId"    # I
    .param p4, "bodyTextId"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    invoke-virtual {v0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    .line 62
    invoke-virtual {v1, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addCommonFields(Lcom/google/android/libraries/bind/data/Data;ILjava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method protected addCommonFields(Lcom/google/android/libraries/bind/data/Data;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "logoDrawableId"    # I
    .param p3, "titleText"    # Ljava/lang/String;
    .param p4, "bodyText"    # Ljava/lang/String;

    .prologue
    .line 66
    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_LOGO_DRAWABLE_ID:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 67
    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_TITLE:I

    invoke-virtual {p1, v0, p3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 68
    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->DK_BODY:I

    invoke-virtual {p1, v0, p4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 69
    return-void
.end method

.method public addDismissibleBookmarksCardIfNeeded(Lcom/google/android/libraries/bind/card/CardListBuilder;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;
    .locals 3
    .param p1, "cardListBuilder"    # Lcom/google/android/libraries/bind/card/CardListBuilder;

    .prologue
    .line 222
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->BOOKMARKS:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getShowWarmWelcome(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 223
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeDismissibleBookmarksCard(Lcom/google/android/libraries/bind/card/CardListBuilder;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 224
    .local v0, "card":Lcom/google/android/libraries/bind/data/Data;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->BOOKMARKS:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->prepend(Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 226
    .end local v0    # "card":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    return-object p0
.end method

.method protected applyEmptyViewBehindWarmWelcome(Lcom/google/android/libraries/bind/data/Data;II)Lcom/google/android/libraries/bind/data/Data;
    .locals 2
    .param p1, "warmWelcomeCardData"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "emptyViewIconResId"    # I
    .param p3, "emptyViewMessageTextId"    # I

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    invoke-static {p1, v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->addStandardEmptyCardData(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;

    .line 180
    sget v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    .line 181
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->getEmptyBackgroundLayoutId(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 180
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 182
    return-object p1
.end method

.method public clearDismissibleWelcomePreferences()Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;
    .locals 6

    .prologue
    .line 43
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    .line 44
    .local v1, "prefs":Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->values()[Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 45
    .local v0, "cardType":Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;
    const/4 v5, 0x1

    invoke-virtual {v1, v0, v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setShowWarmWelcome(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;Z)V

    .line 44
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 47
    .end local v0    # "cardType":Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;
    :cond_0
    return-object p0
.end method

.method public getDismissibleReadNowOnlineCardIfNeeded()Lcom/google/android/libraries/bind/data/Data;
    .locals 6

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 200
    .local v0, "cardToAdd":Lcom/google/android/libraries/bind/data/Data;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->getInstance()Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->getNewFeatureCardToShow()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 201
    .local v1, "newFeatureCardData":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v1, :cond_0

    .line 202
    sget v2, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_IMAGE:I

    .line 203
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget v3, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_TITLE:I

    .line 204
    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget v4, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_TEXT:I

    .line 205
    invoke-virtual {v1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sget v5, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_SHOW_PREF_KEY:I

    .line 206
    invoke-virtual {v1, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v5

    .line 202
    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeDismissibleReadNowOnlineFeatureCard(IIILjava/lang/String;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 208
    :cond_0
    return-object v0
.end method

.method protected makeBaseCardData()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->getStandardLayoutId(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeBaseCardData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method protected makeBaseCardData(I)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "resId"    # I

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 56
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 57
    return-object v0
.end method

.method protected makeDismissFeatureCardDataListOnClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 2
    .param p1, "preferenceKey"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->getInstance()Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;

    move-result-object v0

    .line 90
    .local v0, "helper":Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;Ljava/lang/String;)V

    return-object v1
.end method

.method protected makeDismissPrependedOnClickListener(Lcom/google/android/libraries/bind/card/CardListBuilder;Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Landroid/view/View$OnClickListener;
    .locals 2
    .param p1, "cardListBuilder"    # Lcom/google/android/libraries/bind/card/CardListBuilder;
    .param p2, "dismissibleScreen"    # Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .prologue
    .line 100
    .line 101
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makePreferenceSettingRunnable(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Ljava/lang/Runnable;

    move-result-object v0

    .line 102
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeRowRemovingRunnable(Lcom/google/android/libraries/bind/card/CardListBuilder;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v1

    .line 100
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->makeHidingOnClickListener(Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method protected makeDismissibleBookmarksCard(Lcom/google/android/libraries/bind/card/CardListBuilder;)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "cardListBuilder"    # Lcom/google/android/libraries/bind/card/CardListBuilder;

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeBaseCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 213
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_wwc_bookmarks_scaleable:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_title_bookmarks:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_body_bookmarks:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addCommonFields(Lcom/google/android/libraries/bind/data/Data;III)V

    .line 216
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_button_dismiss:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->BOOKMARKS:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 217
    invoke-virtual {p0, p1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeDismissPrependedOnClickListener(Lcom/google/android/libraries/bind/card/CardListBuilder;Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 216
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton1Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 218
    return-object v0
.end method

.method public makeDismissibleMyMagazinesOfflineCard(Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 9
    .param p1, "afterDismissal"    # Ljava/lang/Runnable;

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeBaseCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 284
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_wwc_offline_scaleable:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_title_offline_my_magazines:I

    .line 285
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_body_offline_my_magazines_new:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v8, Lcom/google/android/apps/newsstanddev/R$string;->download_content:I

    .line 287
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 285
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 284
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addCommonFields(Lcom/google/android/libraries/bind/data/Data;ILjava/lang/String;Ljava/lang/String;)V

    .line 288
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_button_dismiss:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_MAGAZINES_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 290
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makePreferenceSettingRunnable(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Ljava/lang/Runnable;

    move-result-object v2

    .line 289
    invoke-static {v2, p1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->makeHidingOnClickListener(Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 288
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton1Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 292
    return-object v0
.end method

.method public makeDismissibleMyMagazinesOnlineCard(Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "afterDismissal"    # Ljava/lang/Runnable;

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeBaseCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 270
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_wwc_mymagazines_scaleable:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_title_my_magazines:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_body_my_magazines:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addCommonFields(Lcom/google/android/libraries/bind/data/Data;III)V

    .line 273
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_button_empty_my_magazines:I

    .line 274
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeStoreOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 273
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton1Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 275
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_button_dismiss:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_MAGAZINES_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 277
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makePreferenceSettingRunnable(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Ljava/lang/Runnable;

    move-result-object v2

    .line 276
    invoke-static {v2, p1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->makeHidingOnClickListener(Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 275
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton2Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 279
    return-object v0
.end method

.method public makeDismissibleMyNewsOfflineCard(Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 9
    .param p1, "afterDismissal"    # Ljava/lang/Runnable;

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeBaseCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 243
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_wwc_offline_scaleable:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_title_offline_my_news:I

    .line 244
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_body_offline_my_news_new:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v8, Lcom/google/android/apps/newsstanddev/R$string;->download_content:I

    .line 246
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 244
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 243
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addCommonFields(Lcom/google/android/libraries/bind/data/Data;ILjava/lang/String;Ljava/lang/String;)V

    .line 247
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_button_dismiss:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_NEWS_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 249
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makePreferenceSettingRunnable(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Ljava/lang/Runnable;

    move-result-object v2

    .line 248
    invoke-static {v2, p1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->makeHidingOnClickListener(Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 247
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton1Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 250
    return-object v0
.end method

.method public makeDismissibleMyNewsOnlineCard(Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 5
    .param p1, "afterDismissal"    # Ljava/lang/Runnable;

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeBaseCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 231
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_wwc_mynews_scaleable:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->my_news_get_started_title:I

    .line 232
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_body_my_news:I

    .line 233
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 231
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addCommonFields(Lcom/google/android/libraries/bind/data/Data;ILjava/lang/String;Ljava/lang/String;)V

    .line 234
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->explore_title:I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeExploreOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton1Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 235
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_button_dismiss:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_NEWS_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 237
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makePreferenceSettingRunnable(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Ljava/lang/Runnable;

    move-result-object v2

    .line 236
    invoke-static {v2, p1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->makeHidingOnClickListener(Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 235
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton2Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 238
    return-object v0
.end method

.method public makeDismissibleMyTopicsOfflineCard(Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 9
    .param p1, "afterDismissal"    # Ljava/lang/Runnable;

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeBaseCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 309
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_wwc_offline_scaleable:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_title_offline_my_topics:I

    .line 310
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_body_offline_my_topics:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v8, Lcom/google/android/apps/newsstanddev/R$string;->download_content:I

    .line 312
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 311
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 309
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addCommonFields(Lcom/google/android/libraries/bind/data/Data;ILjava/lang/String;Ljava/lang/String;)V

    .line 313
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_button_dismiss:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_TOPICS_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 315
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makePreferenceSettingRunnable(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Ljava/lang/Runnable;

    move-result-object v2

    .line 314
    invoke-static {v2, p1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->makeHidingOnClickListener(Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 313
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton1Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 317
    return-object v0
.end method

.method public makeDismissibleMyTopicsOnlineCard(Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 5
    .param p1, "afterDismissal"    # Ljava/lang/Runnable;

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeBaseCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 297
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_wwc_mytopics_scaleable:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_title_my_topics:I

    .line 298
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->context:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_body_my_topics:I

    .line 299
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 297
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addCommonFields(Lcom/google/android/libraries/bind/data/Data;ILjava/lang/String;Ljava/lang/String;)V

    .line 300
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->explore_title:I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeExploreOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton1Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 301
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_button_dismiss:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_TOPICS_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .line 303
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makePreferenceSettingRunnable(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Ljava/lang/Runnable;

    move-result-object v2

    .line 302
    invoke-static {v2, p1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->makeHidingOnClickListener(Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 301
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton2Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 304
    return-object v0
.end method

.method protected makeDismissibleReadNowOnlineFeatureCard(IIILjava/lang/String;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "logoDrawableId"    # I
    .param p2, "titleTextId"    # I
    .param p3, "bodyTextId"    # I
    .param p4, "preferenceKey"    # Ljava/lang/String;

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeBaseCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 188
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addCommonFields(Lcom/google/android/libraries/bind/data/Data;III)V

    .line 192
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_button_dismiss:I

    .line 193
    invoke-virtual {p0, p4}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeDismissFeatureCardDataListOnClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 192
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton1Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 194
    return-object v0
.end method

.method protected makeExploreOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$3;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$3;-><init>(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;)V

    return-object v0
.end method

.method public makeMyMagazinesOnlineEmptyRow()Lcom/google/android/libraries/bind/data/Data;
    .locals 3

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makePersistentMyMagazinesOnlineEmptyCard()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 264
    .local v0, "card":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_news:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->empty_list_caption_my_magazines:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->applyEmptyViewBehindWarmWelcome(Lcom/google/android/libraries/bind/data/Data;II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    return-object v1
.end method

.method protected makePersistentMyMagazinesOnlineEmptyCard()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeBaseCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 255
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_wwc_shop:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_title_empty_my_magazines:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_body_empty_my_magazines:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addCommonFields(Lcom/google/android/libraries/bind/data/Data;III)V

    .line 257
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->warm_welcome_button_empty_my_magazines:I

    .line 258
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeStoreOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 257
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addButton1Fields(Lcom/google/android/libraries/bind/data/Data;ILandroid/view/View$OnClickListener;)V

    .line 259
    return-object v0
.end method

.method protected makePreferenceSettingRunnable(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Ljava/lang/Runnable;
    .locals 1
    .param p1, "cardType"    # Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    .prologue
    .line 135
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$4;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$4;-><init>(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)V

    return-object v0
.end method

.method protected makeRowRemovingRunnable(Lcom/google/android/libraries/bind/card/CardListBuilder;Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 1
    .param p1, "cardListBuilder"    # Lcom/google/android/libraries/bind/card/CardListBuilder;
    .param p2, "rowId"    # Ljava/lang/String;

    .prologue
    .line 163
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$6;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$6;-><init>(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;Lcom/google/android/libraries/bind/card/CardListBuilder;Ljava/lang/String;)V

    return-object v0
.end method

.method protected makeStoreOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$2;-><init>(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;)V

    return-object v0
.end method
