.class public Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "ShelfDetailedHeader.java"


# static fields
.field public static final DK_BUTTON_TEXT:I

.field public static final DK_ON_CLICK_LISTENER:I

.field public static final DK_SUBTITLE:I

.field public static final DK_TITLE:I

.field public static final DK_TITLE_TEXT_COLOR:I

.field public static final DK_TITLE_TRANSITION_NAME:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUT:I

.field public static final LAYOUT_FIRST:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ShelfDetailedHeader_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_TITLE:I

    .line 15
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ShelfDetailedHeader_titleTextColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_TITLE_TEXT_COLOR:I

    .line 16
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ShelfDetailedHeader_titleTransitionName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_TITLE_TRANSITION_NAME:I

    .line 17
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ShelfDetailedHeader_subtitle:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_SUBTITLE:I

    .line 19
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ShelfDetailedHeader_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_ON_CLICK_LISTENER:I

    .line 20
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ShelfDetailedHeader_buttonText:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_BUTTON_TEXT:I

    .line 23
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->shelf_detailed_header_first:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->LAYOUT_FIRST:I

    .line 26
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->shelf_detailed_header:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->LAYOUT:I

    .line 28
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method
