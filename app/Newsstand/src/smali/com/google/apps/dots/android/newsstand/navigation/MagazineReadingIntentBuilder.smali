.class public Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "MagazineReadingIntentBuilder.java"


# instance fields
.field private attachmentId:Ljava/lang/String;

.field private final edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private inLiteMode:Z

.field private pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

.field private postId:Ljava/lang/String;

.field private final readingState:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

.field private transform:Lcom/google/apps/dots/android/newsstand/server/Transform;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 35
    instance-of v0, p2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 36
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->readingState:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .line 38
    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    .line 39
    .end local p2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/util/MagazineUtil;->getDefaultToLiteModeForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->inLiteMode:Z

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "readingState"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 44
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->readingState:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    .line 47
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/util/MagazineUtil;->getDefaultToLiteModeForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->inLiteMode:Z

    .line 48
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 83
    const-class v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 84
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->readingState:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    if-nez v2, :cond_1

    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->postId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->inLiteMode:Z

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V

    .line 87
    .local v1, "readingState":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    :goto_0
    const-string v2, "MagazineReadingFragment_state"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 88
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->attachmentId:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    if-eqz v2, :cond_0

    .line 89
    const-string v2, "MagazineReadingFragment_transitionImageTransform"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 90
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/server/Transform;->toString()Ljava/lang/String;

    move-result-object v3

    .line 89
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    const-string v2, "MagazineReadingFragment_transitionImageAttachment"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->attachmentId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    :cond_0
    return-object v0

    .line 84
    .end local v1    # "readingState":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->readingState:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->postId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->postId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->getArticleLoader(ZLjava/lang/String;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v0

    .line 102
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->prefetchMagazineResources(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 104
    :cond_0
    return-void
.end method

.method public setInLiteMode(Z)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;
    .locals 0
    .param p1, "inLiteMode"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->inLiteMode:Z

    .line 62
    return-object p0
.end method

.method public setPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;
    .locals 0
    .param p1, "pageLocation"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .line 57
    return-object p0
.end method

.method public setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;
    .locals 0
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->postId:Ljava/lang/String;

    .line 52
    return-object p0
.end method

.method public setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 72
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->activity:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->magazine_reading_activity_hero:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 73
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 74
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 75
    .local v0, "attachmentView":Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getTransform()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 76
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getAttachmentId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->attachmentId:Ljava/lang/String;

    .line 78
    .end local v0    # "attachmentView":Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    :cond_0
    return-object p0
.end method
