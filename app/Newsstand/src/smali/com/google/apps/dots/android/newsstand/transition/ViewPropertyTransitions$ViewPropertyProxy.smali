.class abstract Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;
.super Ljava/lang/Object;
.source "ViewPropertyTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "ViewPropertyProxy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 219
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;, "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$1;

    .prologue
    .line 219
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;, "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy<TT;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getPropertyValue(Landroid/view/View;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract getTypeEvaluator()Landroid/animation/TypeEvaluator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/animation/TypeEvaluator",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getViewPropertyName()Ljava/lang/String;
.end method
