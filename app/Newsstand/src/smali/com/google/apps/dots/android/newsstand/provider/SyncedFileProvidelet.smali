.class public Lcom/google/apps/dots/android/newsstand/provider/SyncedFileProvidelet;
.super Lcom/google/apps/dots/android/newsstand/provider/AbstractProvidelet;
.source "SyncedFileProvidelet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/provider/AbstractProvidelet;-><init>()V

    return-void
.end method

.method private getFilename(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 25
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContentType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 29
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public openAssetFile(ILandroid/net/Uri;Ljava/lang/String;Landroid/content/ContentProvider;)Landroid/content/res/AssetFileDescriptor;
    .locals 6
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "mode"    # Ljava/lang/String;
    .param p4, "contentProvider"    # Landroid/content/ContentProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    .line 37
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/provider/SyncedFileProvidelet;->getFilename(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->LAYOUT_LINK:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 36
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 39
    .local v0, "response":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->extractAFD(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 40
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->isNoPumpFd(Landroid/net/Uri;)Z

    move-result v2

    .line 39
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->afdForFuture(Lcom/google/common/util/concurrent/ListenableFuture;Z)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    return-object v1
.end method
