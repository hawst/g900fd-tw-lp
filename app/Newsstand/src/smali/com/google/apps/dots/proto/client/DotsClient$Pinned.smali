.class public final Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Pinned"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;


# instance fields
.field private account_:Ljava/lang/String;

.field private bitField0_:I

.field public item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 793
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 794
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1134
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 1137
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    .line 794
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .locals 5

    .prologue
    .line 1170
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1174
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 1175
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 1176
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 1177
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 1178
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->clone()Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-result-object v4

    aput-object v4, v3, v2

    .line 1176
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1171
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 1172
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 1182
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 790
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->clone()Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1187
    if-ne p1, p0, :cond_1

    .line 1191
    :cond_0
    :goto_0
    return v1

    .line 1188
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1189
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    .line 1190
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 1191
    goto :goto_0

    .line 1190
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    .line 1191
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getAccount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 1223
    const/4 v1, 0x0

    .line 1224
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    if-eqz v2, :cond_1

    .line 1225
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 1226
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    if-eqz v0, :cond_0

    .line 1227
    const/4 v5, 0x1

    .line 1228
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1225
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1232
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 1233
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    .line 1234
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1236
    :cond_2
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->cachedSize:I

    .line 1237
    return v1
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1195
    const/16 v1, 0x11

    .line 1196
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 1197
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    if-nez v2, :cond_1

    mul-int/lit8 v1, v1, 0x1f

    .line 1203
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    if-nez v4, :cond_3

    :goto_0
    add-int v1, v2, v3

    .line 1204
    return v1

    .line 1199
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1200
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 1199
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1200
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->hashCode()I

    move-result v2

    goto :goto_2

    .line 1203
    .end local v0    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1245
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1246
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1250
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1251
    :sswitch_0
    return-object p0

    .line 1256
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1257
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    if-nez v5, :cond_2

    move v1, v4

    .line 1258
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 1259
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    if-eqz v5, :cond_1

    .line 1260
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1262
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 1263
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1264
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;-><init>()V

    aput-object v6, v5, v1

    .line 1265
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1266
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1263
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1257
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v1, v5

    goto :goto_1

    .line 1269
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;-><init>()V

    aput-object v6, v5, v1

    .line 1270
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1274
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    .line 1275
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->bitField0_:I

    goto :goto_0

    .line 1246
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 790
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v0

    return-object v0
.end method

.method public setAccount(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1142
    if-nez p1, :cond_0

    .line 1143
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1145
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    .line 1146
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->bitField0_:I

    .line 1147
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1209
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    if-eqz v1, :cond_1

    .line 1210
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1211
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    if-eqz v0, :cond_0

    .line 1212
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1210
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1216
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 1217
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->account_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1219
    :cond_2
    return-void
.end method
