.class Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "MutationStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->apply(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

.field final synthetic val$mutLogFile:Ljava/io/File;

.field final synthetic val$response:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/io/File;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->val$response:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->val$mutLogFile:Ljava/io/File;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 137
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    iget-object v14, v14, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .line 138
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    invoke-static {v14}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    iget-object v15, v15, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v15, v15, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-object/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->get(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    move-result-object v6

    .line 139
    .local v6, "cacheItem":Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;, "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    if-eqz v6, :cond_0

    .line 140
    iget-object v14, v6, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->item:Ljava/lang/Object;

    invoke-static {v14}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v14

    .line 182
    :goto_0
    return-object v14

    .line 143
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->val$response:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iget-object v14, v14, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-interface {v14}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeAssetFileDescriptor()Landroid/content/res/AssetFileDescriptor;

    move-result-object v3

    .line 144
    .local v3, "blobAfd":Landroid/content/res/AssetFileDescriptor;
    invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v4

    .line 145
    .local v4, "blobInputStream":Ljava/io/InputStream;
    const-string v14, "MutStore-open"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    iget-object v15, v15, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v15, v15, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v14 .. v16}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v13

    .line 146
    .local v13, "traceRestore":I
    const/4 v8, 0x0

    .line 148
    .local v8, "corruptionException":Ljava/lang/Exception;
    :try_start_0
    invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v14

    long-to-int v5, v14

    .line 149
    .local v5, "blobLength":I
    new-instance v14, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {v14}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;-><init>()V

    invoke-static {v14, v4, v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->readFromStream(Lcom/google/protobuf/nano/MessageNano;Ljava/io/InputStream;I)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v12

    check-cast v12, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .line 151
    .local v12, "simulatedRoot":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    iget-object v14, v14, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->val$mutLogFile:Ljava/io/File;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getMutationLog(Ljava/io/File;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    invoke-static {v14, v15}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Ljava/io/File;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    move-result-object v10

    .line 154
    .local v10, "mutationLog":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    if-eqz v10, :cond_1

    .line 155
    const-string v14, "MutStore-sim"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    iget-object v15, v15, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v15, v15, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v14 .. v16}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 156
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    iget-object v14, v14, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->backendSimulator:Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;
    invoke-static {v14}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$400(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

    move-result-object v14

    invoke-virtual {v14, v12, v10}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->applyActionsTo(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;)V

    .line 157
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 159
    iget-object v14, v10, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v2, v14

    .line 165
    .local v2, "actionsCount":I
    :goto_1
    new-instance v11, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    new-instance v14, Lcom/google/apps/dots/android/newsstand/store/Version;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->val$response:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iget-object v15, v15, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    iget-wide v0, v15, Lcom/google/apps/dots/android/newsstand/store/Version;->serverVersion:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-direct {v14, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/Version;-><init>(JI)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->val$response:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    invoke-direct {v11, v12, v14, v15}, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/android/newsstand/store/Version;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V

    .line 171
    .local v11, "mutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    div-int/lit16 v7, v5, 0x400

    .line 172
    .local v7, "cacheItemSizeKb":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    iget-object v14, v14, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    invoke-static {v14}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    iget-object v15, v15, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v15, v15, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-object/from16 v16, v0

    new-instance v17, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->val$response:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v11, v7}, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;-><init>(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/lang/Object;I)V

    invoke-virtual/range {v14 .. v17}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->put(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;)V

    .line 175
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    .line 180
    invoke-static {v13}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    .line 181
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 182
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    iget-object v15, v15, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;
    invoke-static {v15}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$600(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->val$response:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v0, v1, v8}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->deleteStoreFileForCorruptResponseIfNeeded(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 161
    .end local v2    # "actionsCount":I
    .end local v7    # "cacheItemSizeKb":I
    .end local v11    # "mutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    :cond_1
    :try_start_1
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v14

    const-string v15, "%s: no mutation log for request"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162
    const/4 v2, 0x0

    .restart local v2    # "actionsCount":I
    goto/16 :goto_1

    .line 176
    .end local v2    # "actionsCount":I
    .end local v5    # "blobLength":I
    .end local v10    # "mutationLog":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .end local v12    # "simulatedRoot":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    :catch_0
    move-exception v9

    .line 177
    .local v9, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    move-object v8, v9

    .line 178
    :try_start_2
    throw v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 180
    .end local v9    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :catchall_0
    move-exception v14

    invoke-static {v13}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    .line 181
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 182
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    iget-object v15, v15, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;
    invoke-static {v15}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$600(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->val$response:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v0, v1, v8}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->deleteStoreFileForCorruptResponseIfNeeded(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/lang/Exception;)V

    throw v14
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
