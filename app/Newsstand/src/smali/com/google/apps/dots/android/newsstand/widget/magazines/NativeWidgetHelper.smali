.class Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;
.super Ljava/lang/Object;
.source "NativeWidgetHelper.java"


# static fields
.field private static tempMatrix:Landroid/graphics/Matrix;

.field private static tempRectF:Landroid/graphics/RectF;


# instance fields
.field private final contentArea:Landroid/graphics/RectF;

.field private final screenRect:Landroid/graphics/RectF;

.field private final view:Landroid/view/View;

.field private final viewGroup:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->tempMatrix:Landroid/graphics/Matrix;

    .line 28
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->tempRectF:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->contentArea:Landroid/graphics/RectF;

    .line 26
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->screenRect:Landroid/graphics/RectF;

    .line 152
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    .line 153
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    :goto_0
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->viewGroup:Landroid/view/ViewGroup;

    .line 155
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 156
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->screenRect:Landroid/graphics/RectF;

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 157
    return-void

    .line 153
    .end local v0    # "metrics":Landroid/util/DisplayMetrics;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getGlobalTransform(Landroid/view/View;Landroid/graphics/Matrix;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "output"    # Landroid/graphics/Matrix;

    .prologue
    .line 36
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 39
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 42
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 43
    .local v0, "parent":Landroid/view/ViewParent;
    if-nez v0, :cond_1

    .line 51
    :cond_0
    :goto_1
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_3

    move-object p0, v0

    .line 52
    check-cast p0, Landroid/view/View;

    .line 56
    goto :goto_0

    .line 44
    :cond_1
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    if-eqz v2, :cond_2

    move-object v2, v0

    .line 45
    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    invoke-interface {v2, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;->applyLayoutToViewCoordsTransform(Landroid/graphics/Matrix;)V

    goto :goto_1

    .line 46
    :cond_2
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 47
    check-cast v1, Landroid/view/View;

    .line 48
    .local v1, "viewParent":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_1

    .line 57
    .end local v1    # "viewParent":Landroid/view/View;
    :cond_3
    return-void
.end method

.method public static getTransformScale(Landroid/view/View;)F
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 72
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->tempMatrix:Landroid/graphics/Matrix;

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->getGlobalTransform(Landroid/view/View;Landroid/graphics/Matrix;)V

    .line 73
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->tempMatrix:Landroid/graphics/Matrix;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->getMatrixScale(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method private static intervalMargin(FFFF)F
    .locals 2
    .param p0, "l1"    # F
    .param p1, "r1"    # F
    .param p2, "l2"    # F
    .param p3, "r2"    # F

    .prologue
    .line 130
    sub-float v0, p2, p1

    sub-float v1, p0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public final applyDefaultLayoutToViewCoordsTransform(Landroid/graphics/Matrix;)V
    .locals 2
    .param p1, "output"    # Landroid/graphics/Matrix;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 65
    return-void
.end method

.method public final getContentArea()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->contentArea:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getScreenRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->screenRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getSwipeDistanceToScreen()F
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->tempMatrix:Landroid/graphics/Matrix;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->getGlobalTransform(Landroid/view/View;Landroid/graphics/Matrix;)V

    .line 123
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->tempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->getSwipeDistanceToScreen(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method public getSwipeDistanceToScreen(Landroid/graphics/Matrix;)F
    .locals 7
    .param p1, "globalTransform"    # Landroid/graphics/Matrix;

    .prologue
    const/4 v6, 0x0

    .line 138
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->tempRectF:Landroid/graphics/RectF;

    .line 139
    .local v1, "transformedContentArea":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->contentArea:Landroid/graphics/RectF;

    invoke-virtual {v1, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 140
    invoke-virtual {p1, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 144
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->screenRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v5, v1, Landroid/graphics/RectF;->right:F

    invoke-static {v6, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->intervalMargin(FFFF)F

    move-result v0

    .line 146
    .local v0, "horizontalDistance":F
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->screenRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget v4, v1, Landroid/graphics/RectF;->top:F

    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    invoke-static {v6, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->intervalMargin(FFFF)F

    move-result v2

    .line 148
    .local v2, "verticalDistance":F
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->screenRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float v3, v0, v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->screenRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float v4, v2, v4

    add-float/2addr v3, v4

    return v3
.end method

.method public getVisibleFraction(Z)F
    .locals 7
    .param p1, "respectClipping"    # Z

    .prologue
    const/4 v5, 0x0

    .line 90
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->tempRectF:Landroid/graphics/RectF;

    .line 91
    .local v2, "transformedContentArea":Landroid/graphics/RectF;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->tempMatrix:Landroid/graphics/Matrix;

    .line 92
    .local v0, "globalTransform":Landroid/graphics/Matrix;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    invoke-static {v6, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->getGlobalTransform(Landroid/view/View;Landroid/graphics/Matrix;)V

    .line 93
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->contentArea:Landroid/graphics/RectF;

    invoke-virtual {v2, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 94
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 95
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->areaOf(Landroid/graphics/RectF;)F

    move-result v1

    .line 97
    .local v1, "transformedArea":F
    cmpl-float v6, v1, v5

    if-lez v6, :cond_1

    .line 98
    if-eqz p1, :cond_2

    .line 100
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 101
    .local v4, "visibleRect":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 102
    .local v3, "viewParent":Landroid/view/ViewParent;
    if-eqz v3, :cond_0

    .line 103
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->contentArea:Landroid/graphics/RectF;

    invoke-virtual {v5, v4}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 104
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    const/4 v6, 0x0

    invoke-interface {v3, v5, v4, v6}, Landroid/view/ViewParent;->getChildVisibleRect(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 105
    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    .line 108
    :cond_0
    invoke-virtual {v2, v4}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 112
    .end local v3    # "viewParent":Landroid/view/ViewParent;
    .end local v4    # "visibleRect":Landroid/graphics/Rect;
    :goto_0
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->areaOf(Landroid/graphics/RectF;)F

    move-result v5

    div-float/2addr v5, v1

    .line 114
    :cond_1
    return v5

    .line 110
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->screenRect:Landroid/graphics/RectF;

    invoke-static {v2, v5}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->intersectWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public final isLoadComplete()Z
    .locals 3

    .prologue
    .line 177
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->viewGroup:Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    .line 178
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 179
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 180
    .local v0, "child":Landroid/view/View;
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;

    if-eqz v2, :cond_0

    .line 181
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;

    .end local v0    # "child":Landroid/view/View;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;->isLoadComplete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 182
    const/4 v2, 0x0

    .line 187
    .end local v1    # "i":I
    :goto_1
    return v2

    .line 178
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 187
    .end local v1    # "i":I
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->isVisible(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public varargs notify(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "optionalArgs"    # [Ljava/lang/Object;

    .prologue
    .line 259
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 260
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 261
    .local v0, "child":Landroid/view/View;
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/widget/EventSupport;

    if-eqz v2, :cond_0

    .line 262
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/EventSupport;

    .end local v0    # "child":Landroid/view/View;
    invoke-interface {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/EventSupport;->notify(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 265
    :cond_1
    return-void
.end method

.method public final onLoadComplete()V
    .locals 2

    .prologue
    .line 224
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 225
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    if-eqz v1, :cond_0

    .line 226
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    .end local v0    # "parent":Landroid/view/ViewParent;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;->onLoadComplete()V

    .line 228
    :cond_0
    return-void
.end method

.method public final onPageChanged()V
    .locals 2

    .prologue
    .line 244
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 245
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    if-eqz v1, :cond_0

    .line 246
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    .end local v0    # "parent":Landroid/view/ViewParent;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;->onPageChanged()V

    .line 248
    :cond_0
    return-void
.end method

.method public final onScrolled()V
    .locals 2

    .prologue
    .line 234
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 235
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    if-eqz v1, :cond_0

    .line 236
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    .end local v0    # "parent":Landroid/view/ViewParent;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;->onScrolled()V

    .line 238
    :cond_0
    return-void
.end method

.method public final onTransformChanged()V
    .locals 3

    .prologue
    .line 163
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->viewGroup:Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    .line 164
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 165
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 166
    .local v0, "child":Landroid/view/View;
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;

    if-eqz v2, :cond_0

    .line 167
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;

    .end local v0    # "child":Landroid/view/View;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;->onTransformChanged()V

    .line 164
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 171
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method public final onUnhandledClick()V
    .locals 2

    .prologue
    .line 194
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 195
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    if-eqz v1, :cond_0

    .line 196
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    .end local v0    # "parent":Landroid/view/ViewParent;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;->onUnhandledClick()V

    .line 198
    :cond_0
    return-void
.end method

.method public final onUnhandledFling(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)V
    .locals 2
    .param p1, "flingDirection"    # Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    .prologue
    .line 204
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 205
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    if-eqz v1, :cond_0

    .line 206
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    .end local v0    # "parent":Landroid/view/ViewParent;
    invoke-interface {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;->onUnhandledFling(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)V

    .line 208
    :cond_0
    return-void
.end method

.method public final onZoomAttempt()V
    .locals 2

    .prologue
    .line 214
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 215
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    if-eqz v1, :cond_0

    .line 216
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;

    .end local v0    # "parent":Landroid/view/ViewParent;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;->onZoomAttempt()V

    .line 218
    :cond_0
    return-void
.end method

.method public final setContentArea(FFFF)V
    .locals 1
    .param p1, "l"    # F
    .param p2, "t"    # F
    .param p3, "r"    # F
    .param p4, "b"    # F

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->contentArea:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 252
    return-void
.end method
