.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NativeBody"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;


# instance fields
.field private bitField0_:I

.field private nativeBodyVersion_:I

.field private rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 375
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 376
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 381
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->nativeBodyVersion_:I

    .line 376
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
    .locals 3

    .prologue
    .line 430
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 434
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-eqz v2, :cond_0

    .line 435
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 437
    :cond_0
    return-object v0

    .line 431
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
    :catch_0
    move-exception v1

    .line 432
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 372
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 442
    if-ne p1, p0, :cond_1

    .line 446
    :cond_0
    :goto_0
    return v1

    .line 443
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 444
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    .line 445
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->nativeBodyVersion_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->nativeBodyVersion_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 446
    goto :goto_0

    .line 445
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 446
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getRootPart()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 469
    const/4 v0, 0x0

    .line 470
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 471
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->nativeBodyVersion_:I

    .line 472
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 474
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-eqz v1, :cond_1

    .line 475
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 476
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 478
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->cachedSize:I

    .line 479
    return v0
.end method

.method public hasRootPart()Z
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 450
    const/16 v0, 0x11

    .line 451
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 452
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->nativeBodyVersion_:I

    add-int v0, v1, v2

    .line 453
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 454
    return v0

    .line 453
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 487
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 488
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 492
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 493
    :sswitch_0
    return-object p0

    .line 498
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->nativeBodyVersion_:I

    .line 499
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->bitField0_:I

    goto :goto_0

    .line 503
    :sswitch_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-nez v1, :cond_1

    .line 504
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 506
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 488
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 372
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 459
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 460
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->nativeBodyVersion_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    if-eqz v0, :cond_1

    .line 463
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->rootPart_:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 465
    :cond_1
    return-void
.end method
