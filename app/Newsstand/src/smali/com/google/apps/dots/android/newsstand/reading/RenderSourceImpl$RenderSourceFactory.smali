.class public final Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$RenderSourceFactory;
.super Ljava/lang/Object;
.source "RenderSourceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RenderSourceFactory"
.end annotation


# direct methods
.method public static newsRenderSource(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Ljava/lang/Integer;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
    .locals 10
    .param p0, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p1, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "articleLoader"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p3, "optPostIndex"    # Ljava/lang/Integer;
    .param p4, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 61
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 62
    new-instance v5, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleBaseHtmlLoader;

    invoke-direct {v5, p2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleBaseHtmlLoader;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    .line 64
    .local v5, "baseHtmlLoader":Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;
    new-instance v6, Lcom/google/apps/dots/android/newsstand/reading/NewsJsonStoreHelper;

    invoke-direct {v6}, Lcom/google/apps/dots/android/newsstand/reading/NewsJsonStoreHelper;-><init>()V

    .line 65
    .local v6, "jsonStoreHelper":Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;

    .line 72
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->itemJsonSerializer()Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    move-result-object v8

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v9}, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Ljava/lang/Integer;Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;)V

    return-object v0
.end method
