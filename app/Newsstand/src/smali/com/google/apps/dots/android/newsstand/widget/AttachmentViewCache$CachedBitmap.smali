.class public Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
.super Ljava/lang/Object;
.source "AttachmentViewCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CachedBitmap"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    }
.end annotation


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private final cacheKey:Ljava/lang/Object;

.field private hasAlpha:Z

.field private loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

.field private final readyListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;",
            ">;"
        }
    .end annotation
.end field

.field private refCount:I

.field private final token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method private constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .param p1, "cacheKey"    # Ljava/lang/Object;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->refCount:I

    .line 51
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;->INIT:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    .line 52
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->readyListeners:Ljava/util/Set;

    .line 53
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 56
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->cacheKey:Ljava/lang/Object;

    .line 57
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->registerReadyListener(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->readyListeners:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->refCount:I

    return v0
.end method

.method static synthetic access$208(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->refCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->refCount:I

    return v0
.end method

.method static synthetic access$210(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->refCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->refCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->clearReadyListener(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->bitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->cacheKey:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->hasAlpha:Z

    return p1
.end method

.method private clearReadyListener(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->readyListeners:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->readyListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 82
    :cond_0
    return-void
.end method

.method private registerReadyListener(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->readyListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method


# virtual methods
.method public bitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public hasAlpha()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->hasAlpha:Z

    return v0
.end method

.method public isLoadedOrFailed()Z
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;->FAILED:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
