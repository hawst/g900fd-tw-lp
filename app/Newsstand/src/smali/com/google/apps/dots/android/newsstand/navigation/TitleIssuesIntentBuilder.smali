.class public Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;
.source "TitleIssuesIntentBuilder.java"


# instance fields
.field private appFamilyId:Ljava/lang/String;

.field private hideArchive:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->hideArchive:Z

    .line 27
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;-><init>(Landroid/content/Context;)V

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->hideArchive:Z

    .line 31
    return-void
.end method

.method public static nonActivityMake(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 45
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->appFamilyId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    const-class v1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesActivity;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 47
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "TitleIssuesFragment_state"

    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->appFamilyId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->hideArchive:Z

    invoke-direct {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 49
    return-object v0
.end method

.method public setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;
    .locals 0
    .param p1, "appFamilyId"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->appFamilyId:Ljava/lang/String;

    .line 35
    return-object p0
.end method

.method public setHideArchive(Z)Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;
    .locals 0
    .param p1, "hideArchive"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->hideArchive:Z

    .line 40
    return-object p0
.end method
