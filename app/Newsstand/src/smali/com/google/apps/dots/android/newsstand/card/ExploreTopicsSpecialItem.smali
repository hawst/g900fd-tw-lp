.class public Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "ExploreTopicsSpecialItem.java"


# static fields
.field public static final DK_BACKGROUND:I

.field public static final DK_ICON_DRAWABLE:I

.field public static final DK_ON_CLICK_LISTENER:I

.field public static final DK_TITLE:I

.field public static final LAYOUTS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 14
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ExploreTopicsSpecialItem_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_ON_CLICK_LISTENER:I

    .line 15
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ExploreTopicsSpecialItem_iconDrawable:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_ICON_DRAWABLE:I

    .line 16
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ExploreTopicsSpecialItem_background:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_BACKGROUND:I

    .line 17
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ExploreTopicsSpecialItem_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->DK_TITLE:I

    .line 19
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->explore_topics_special_item:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;->LAYOUTS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsSpecialItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method
