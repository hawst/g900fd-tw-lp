.class final Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState$1;
.super Ljava/lang/Object;
.source "MagazineReadingState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 96
    const-class v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .line 97
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    .line 98
    .local v1, "readingState":Lcom/google/apps/dots/android/newsstand/reading/ReadingState;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 99
    .local v0, "inLiteMode":Z
    :goto_0
    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    invoke-direct {v2, v1, v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;-><init>(Lcom/google/apps/dots/android/newsstand/reading/ReadingState;Z)V

    return-object v2

    .line 98
    .end local v0    # "inLiteMode":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 104
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    move-result-object v0

    return-object v0
.end method
