.class public Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;
.super Landroid/app/Service;
.source "NewsWidgetService.java"


# static fields
.field private static final ACCEPTED_CARD_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final EQUALITY_FIELDS:[I

.field private static bitmapCacheForwardDirectionEntries:I

.field private static bitmapCacheReverseDirectionEntries:I

.field private static currentPosition:I

.field private static dataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private static observerAccount:Landroid/accounts/Account;

.field private static refreshObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

.field private static final widgetFilter:Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;


# instance fields
.field private imageCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

.field private refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private sourceIconCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private stopSelfRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v4, 0x0

    .line 71
    sput v4, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    .line 80
    sput v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->bitmapCacheReverseDirectionEntries:I

    .line 82
    sput v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->bitmapCacheForwardDirectionEntries:I

    .line 86
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS:[I

    aget v0, v0, v4

    .line 87
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_FULL_IMAGE:[I

    aget v1, v1, v4

    .line 88
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_COMPACT:[I

    aget v2, v2, v4

    .line 89
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_FULL_IMAGE_COMPACT:[I

    aget v3, v3, v4

    .line 90
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 86
    invoke-static {v0, v1, v2, v3}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->ACCEPTED_CARD_TYPES:Ljava/util/List;

    .line 91
    new-instance v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$1;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$1;-><init>(Ljava/util/concurrent/Executor;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->widgetFilter:Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;

    .line 97
    const/4 v0, 0x1

    new-array v0, v0, [I

    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->DEFAULT_PRIMARY_KEY:I

    aput v1, v0, v4

    sput-object v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->EQUALITY_FIELDS:[I

    .line 100
    new-instance v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$2;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$2;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    .line 116
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->whenSetup()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$4;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$4;-><init>()V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 123
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 108
    new-instance v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$3;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$3;-><init>(Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->stopSelfRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000()Ljava/util/List;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->ACCEPTED_CARD_TYPES:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$102(Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Landroid/accounts/Account;

    .prologue
    .line 58
    sput-object p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->observerAccount:Landroid/accounts/Account;

    return-object p0
.end method

.method static synthetic access$200()V
    .locals 0

    .prologue
    .line 58
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->registerObserver()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;
    .param p1, "x1"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->updateRemoteViews(I)V

    return-void
.end method

.method private configureRemoteViewImage(Landroid/widget/RemoteViews;Lcom/google/android/libraries/bind/data/Data;IZ)V
    .locals 5
    .param p1, "rv"    # Landroid/widget/RemoteViews;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p3, "appWidgetId"    # I
    .param p4, "useCompactLayout"    # Z

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 373
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ID:I

    invoke-virtual {p2, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    .line 374
    .local v1, "imageId":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 375
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->image:I

    invoke-virtual {p1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 376
    const/4 v2, 0x1

    invoke-direct {p0, v1, v2, p3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getImageFromCache(Ljava/lang/String;ZI)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 377
    .local v0, "image":Landroid/graphics/Bitmap;
    if-eqz p4, :cond_1

    .line 378
    if-nez v0, :cond_0

    .line 379
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->image:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->image_not_available:I

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 398
    .end local v0    # "image":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 381
    .restart local v0    # "image":Landroid/graphics/Bitmap;
    :cond_0
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->image:I

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0

    .line 384
    :cond_1
    if-nez v0, :cond_3

    .line 385
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->image:I

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 386
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->image_loading:I

    invoke-virtual {p1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 387
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->image_loading_text:I

    .line 388
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->loading:I

    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 387
    invoke-virtual {p1, v3, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 388
    :cond_2
    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->image_not_available_offline:I

    goto :goto_1

    .line 391
    :cond_3
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->image:I

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 392
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->image_loading:I

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    .line 396
    .end local v0    # "image":Landroid/graphics/Bitmap;
    :cond_4
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->image:I

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method

.method private configureRemoteViewImageAttribution(Landroid/widget/RemoteViews;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p1, "rv"    # Landroid/widget/RemoteViews;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 363
    sget v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ATTRIBUTION:I

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->attribution:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 365
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->attribution:I

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ATTRIBUTION:I

    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 369
    :goto_0
    return-void

    .line 367
    :cond_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->attribution:I

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method

.method private configureRemoteViews(Landroid/widget/RemoteViews;Lcom/google/android/libraries/bind/data/Data;IZ)V
    .locals 4
    .param p1, "rv"    # Landroid/widget/RemoteViews;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p3, "appWidgetId"    # I
    .param p4, "useCompactLayout"    # Z

    .prologue
    .line 339
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->configureRemoteViewImage(Landroid/widget/RemoteViews;Lcom/google/android/libraries/bind/data/Data;IZ)V

    .line 341
    if-nez p4, :cond_0

    .line 342
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->configureRemoteViewImageAttribution(Landroid/widget/RemoteViews;Lcom/google/android/libraries/bind/data/Data;)V

    .line 345
    :cond_0
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->title:I

    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_TITLE:I

    invoke-virtual {p2, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 346
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->source_name:I

    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_NAME:I

    invoke-virtual {p2, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 347
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_ICON_ID:I

    .line 348
    invoke-virtual {p2, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3, p3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getSourceIconFromCache(Ljava/lang/String;ZI)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 349
    .local v0, "icon":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 350
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->source_icon:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->image_not_available:I

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 354
    :goto_0
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->time:I

    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_TIME:I

    invoke-virtual {p2, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 358
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->DEFAULT_PRIMARY_KEY:I

    invoke-virtual {p2, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    .line 359
    .local v1, "postId":Ljava/lang/String;
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->widget_article_layout:I

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getReadingIntent(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 360
    return-void

    .line 352
    .end local v1    # "postId":Ljava/lang/String;
    :cond_1
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->source_icon:I

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private fetchDataListIfNecessary()V
    .locals 3

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 303
    :goto_0
    return-void

    .line 292
    :cond_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->readNowList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->EQUALITY_FIELDS:[I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->widgetFilter:Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    .line 293
    new-instance v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$7;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$7;-><init>(Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->dataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 302
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->dataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    goto :goto_0
.end method

.method private getBitmap(Lcom/google/common/util/concurrent/ListenableFuture;ZI)Landroid/graphics/Bitmap;
    .locals 3
    .param p2, "updateViewsIfNotAvailable"    # Z
    .param p3, "appWidgetId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;ZI)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .local p1, "bitmapFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x0

    .line 478
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 495
    :cond_0
    :goto_0
    return-object v0

    .line 481
    :cond_1
    invoke-interface {p1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 482
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0

    .line 484
    :cond_2
    if-eqz p2, :cond_0

    .line 485
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$8;

    invoke-direct {v2, p0, p3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$8;-><init>(Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;I)V

    invoke-virtual {v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private getBitmapFuture(Ljava/lang/String;FF)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "attachmentId"    # Ljava/lang/String;
    .param p2, "transformWidth"    # F
    .param p3, "transformHeight"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "FF)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 522
    new-instance v1, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    float-to-int v2, p2

    .line 523
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v1

    float-to-int v2, p3

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    .line 524
    .local v0, "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    invoke-virtual {v1, v2, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method private getBitmapFutureFromCache(Landroid/support/v4/util/LruCache;FFLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p2, "transformWidth"    # F
    .param p3, "transformHeight"    # F
    .param p4, "imageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;FF",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 507
    .local p1, "cache":Landroid/support/v4/util/LruCache;, "Landroid/support/v4/util/LruCache<Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;>;"
    invoke-static {p4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 508
    const/4 v0, 0x0

    .line 517
    :cond_0
    :goto_0
    return-object v0

    .line 511
    :cond_1
    invoke-virtual {p1, p4}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 512
    .local v0, "bitmapFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;"
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 513
    :cond_2
    invoke-direct {p0, p4, p2, p3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getBitmapFuture(Ljava/lang/String;FF)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 514
    invoke-virtual {p1, p4, v0}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private getBitmapFutureFromCache(Landroid/support/v4/util/LruCache;FLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p2, "transformSize"    # F
    .param p3, "imageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;F",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 501
    .local p1, "cache":Landroid/support/v4/util/LruCache;, "Landroid/support/v4/util/LruCache<Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;>;"
    invoke-direct {p0, p1, p2, p2, p3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getBitmapFutureFromCache(Landroid/support/v4/util/LruCache;FFLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private getImageFromCache(Ljava/lang/String;ZI)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "imageId"    # Ljava/lang/String;
    .param p2, "updateViews"    # Z
    .param p3, "appWidgetId"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 450
    const/high16 v7, 0x3f000000    # 0.5f

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getSmallerDisplayDimension()I

    move-result v8

    int-to-float v8, v8

    mul-float v6, v7, v8

    .line 451
    .local v6, "width":F
    move v0, v6

    .line 452
    .local v0, "height":F
    const v7, 0x3f666666    # 0.9f

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getSmallerDisplayDimension()I

    move-result v8

    int-to-float v8, v8

    mul-float v4, v7, v8

    .line 453
    .local v4, "smallerDimension":F
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x10

    if-lt v7, v8, :cond_0

    .line 454
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 455
    .local v1, "manager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v1, p3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v5

    .line 456
    .local v5, "widgetOptions":Landroid/os/Bundle;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v7

    const-string v8, "appWidgetMaxWidth"

    float-to-int v9, v4

    invoke-virtual {v5, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getPixelsFromDips(I)F

    move-result v3

    .line 458
    .local v3, "maxWidgetWidth":F
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v7

    const-string v8, "appWidgetMaxHeight"

    float-to-int v9, v4

    invoke-virtual {v5, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getPixelsFromDips(I)F

    move-result v2

    .line 464
    .local v2, "maxWidgetHeight":F
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v7, v7

    invoke-static {v3, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 465
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v7, v7

    invoke-static {v2, v7}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 467
    .end local v1    # "manager":Landroid/appwidget/AppWidgetManager;
    .end local v2    # "maxWidgetHeight":F
    .end local v3    # "maxWidgetWidth":F
    .end local v5    # "widgetOptions":Landroid/os/Bundle;
    :cond_0
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->imageCache:Landroid/support/v4/util/LruCache;

    invoke-direct {p0, v7, v6, v0, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getBitmapFutureFromCache(Landroid/support/v4/util/LruCache;FFLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-direct {p0, v7, p2, p3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getBitmap(Lcom/google/common/util/concurrent/ListenableFuture;ZI)Landroid/graphics/Bitmap;

    move-result-object v7

    return-object v7
.end method

.method private getReadingIntent(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 401
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    .line 402
    invoke-static {v2, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->makeExternalReadingIntent(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x10000000

    .line 401
    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private getSourceIconFromCache(Ljava/lang/String;ZI)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "iconId"    # Ljava/lang/String;
    .param p2, "updateViews"    # Z
    .param p3, "appWidgetId"    # I

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->sourceIconCache:Landroid/support/v4/util/LruCache;

    const/high16 v1, 0x43480000    # 200.0f

    invoke-direct {p0, v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getBitmapFutureFromCache(Landroid/support/v4/util/LruCache;FLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getBitmap(Lcom/google/common/util/concurrent/ListenableFuture;ZI)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static registerObserver()V
    .locals 4

    .prologue
    .line 144
    sget-object v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->observerAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 145
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 146
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->observerAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getReadNow(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 145
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->registerObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 148
    :cond_0
    return-void
.end method

.method private scheduleStop()I
    .locals 4

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->stopSelfRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 280
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->stopSelfRunnable:Ljava/lang/Runnable;

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 284
    :goto_0
    const/4 v0, 0x2

    return v0

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->stopSelfRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private sendArticleSeenAnalyticsEvent()V
    .locals 4

    .prologue
    .line 529
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    sget v3, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 530
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v1, :cond_0

    .line 531
    sget v2, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    .line 532
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;

    .line 534
    .local v0, "analyticsProvider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;
    if-eqz v0, :cond_0

    .line 535
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->track(Z)V

    .line 538
    .end local v0    # "analyticsProvider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;
    :cond_0
    return-void
.end method

.method public static setAccount(Landroid/accounts/Account;)V
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 127
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 128
    sget-object v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->observerAccount:Landroid/accounts/Account;

    invoke-static {p0, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->unregisterObserver()V

    .line 132
    sput-object p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->observerAccount:Landroid/accounts/Account;

    .line 133
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->registerObserver()V

    goto :goto_0
.end method

.method private stopImmediately()I
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->stopSelfRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->stopSelfRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 274
    const/4 v0, 0x2

    return v0
.end method

.method private static unregisterObserver()V
    .locals 4

    .prologue
    .line 137
    sget-object v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->observerAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 138
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 139
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->observerAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getReadNow(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 138
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->unregisterObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 141
    :cond_0
    return-void
.end method

.method private updateImageCache(Lcom/google/android/libraries/bind/data/DataList;I)V
    .locals 3
    .param p1, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "appWidgetId"    # I

    .prologue
    .line 409
    const/4 v2, 0x0

    invoke-direct {p0, v2, p1, p2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->updateImageCacheForPositionOffset(ILcom/google/android/libraries/bind/data/DataList;I)V

    .line 415
    sget v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->bitmapCacheForwardDirectionEntries:I

    .local v0, "forwardOffset":I
    :goto_0
    if-lez v0, :cond_0

    .line 417
    invoke-direct {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->updateImageCacheForPositionOffset(ILcom/google/android/libraries/bind/data/DataList;I)V

    .line 416
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 419
    :cond_0
    sget v2, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->bitmapCacheReverseDirectionEntries:I

    neg-int v1, v2

    .local v1, "reverseOffset":I
    :goto_1
    if-gez v1, :cond_1

    .line 421
    invoke-direct {p0, v1, p1, p2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->updateImageCacheForPositionOffset(ILcom/google/android/libraries/bind/data/DataList;I)V

    .line 420
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 423
    :cond_1
    return-void
.end method

.method private updateImageCacheAllocationIfNecessary(Z)V
    .locals 1
    .param p1, "useCompactLayout"    # Z

    .prologue
    const/4 v0, 0x2

    .line 426
    if-eqz p1, :cond_0

    .line 428
    const/4 v0, 0x0

    sput v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->bitmapCacheReverseDirectionEntries:I

    .line 429
    const/4 v0, 0x4

    sput v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->bitmapCacheForwardDirectionEntries:I

    .line 435
    :goto_0
    return-void

    .line 432
    :cond_0
    sput v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->bitmapCacheReverseDirectionEntries:I

    .line 433
    sput v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->bitmapCacheForwardDirectionEntries:I

    goto :goto_0
.end method

.method private updateImageCacheForPositionOffset(ILcom/google/android/libraries/bind/data/DataList;I)V
    .locals 5
    .param p1, "positionOffset"    # I
    .param p2, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p3, "appWidgetId"    # I

    .prologue
    const/4 v4, 0x0

    .line 439
    sget v2, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    add-int/2addr v2, p1

    invoke-virtual {p2}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p2}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v3

    rem-int v1, v2, v3

    .line 440
    .local v1, "position":I
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 441
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-nez v0, :cond_0

    .line 446
    :goto_0
    return-void

    .line 444
    :cond_0
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ID:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v4, p3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getImageFromCache(Ljava/lang/String;ZI)Landroid/graphics/Bitmap;

    .line 445
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_ICON_ID:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v4, p3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getSourceIconFromCache(Ljava/lang/String;ZI)Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private updateRemoteViews(I)V
    .locals 6
    .param p1, "appWidgetId"    # I

    .prologue
    .line 312
    if-nez p1, :cond_1

    .line 335
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    sget v5, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 317
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_0

    .line 321
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->useCompactLayout(Landroid/content/Context;I)Z

    move-result v3

    .line 322
    .local v3, "useCompactLayout":Z
    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->updateImageCacheAllocationIfNecessary(Z)V

    .line 323
    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getPackageName()Ljava/lang/String;

    move-result-object v5

    if-eqz v3, :cond_2

    sget v4, Lcom/google/android/apps/newsstanddev/R$layout;->news_widget_compact:I

    :goto_1
    invoke-direct {v2, v5, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 325
    .local v2, "rv":Landroid/widget/RemoteViews;
    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->widget_empty_layout:I

    const/16 v5, 0x8

    invoke-virtual {v2, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 326
    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->widget_article_layout:I

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 328
    invoke-direct {p0, v2, v0, p1, v3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->configureRemoteViews(Landroid/widget/RemoteViews;Lcom/google/android/libraries/bind/data/Data;IZ)V

    .line 330
    const/4 v4, 0x1

    invoke-static {p0, p1, v2, v4}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->configureNavigationButtons(Landroid/content/Context;ILandroid/widget/RemoteViews;Z)V

    .line 332
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 333
    .local v1, "manager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v1, p1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 334
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->scheduleStop()I

    goto :goto_0

    .line 323
    .end local v1    # "manager":Landroid/appwidget/AppWidgetManager;
    .end local v2    # "rv":Landroid/widget/RemoteViews;
    :cond_2
    sget v4, Lcom/google/android/apps/newsstanddev/R$layout;->news_widget:I

    goto :goto_1
.end method

.method private validatePositionAndUpdateCache(I)V
    .locals 2
    .param p1, "appWidgetId"    # I

    .prologue
    .line 306
    sget v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    .line 307
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v1

    rem-int/2addr v0, v1

    sput v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    .line 308
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->updateImageCache(Lcom/google/android/libraries/bind/data/DataList;I)V

    .line 309
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 189
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 157
    const/4 v0, 0x5

    .line 160
    .local v0, "numCacheEntries":I
    new-instance v1, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$5;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$5;-><init>(Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;I)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->imageCache:Landroid/support/v4/util/LruCache;

    .line 167
    new-instance v1, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$6;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$6;-><init>(Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;I)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->sourceIconCache:Landroid/support/v4/util/LruCache;

    .line 174
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->dataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 184
    :cond_1
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 185
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v7, 0x0

    .line 194
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 196
    .local v1, "action":Ljava/lang/String;
    const-string v8, "NewsWidgetService_cancelAction"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 197
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->stopImmediately()I

    move-result v7

    .line 266
    :goto_0
    return v7

    .line 200
    :cond_0
    const-string v8, "appWidgetId"

    invoke-virtual {p1, v8, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 203
    .local v2, "appWidgetId":I
    if-nez v2, :cond_1

    .line 204
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->scheduleStop()I

    move-result v7

    goto :goto_0

    .line 207
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 208
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v4

    .line 210
    .local v4, "isConnected":Z
    if-nez v0, :cond_2

    .line 211
    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showTapToSignIn(Landroid/content/Context;I)V

    .line 212
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->scheduleStop()I

    move-result v7

    goto :goto_0

    .line 215
    :cond_2
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    if-nez v8, :cond_3

    .line 216
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v8

    iput-object v8, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 219
    :cond_3
    const-string v8, "NewsWidgetService_articlePosition"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 220
    const-string v8, "NewsWidgetService_articlePosition"

    sget v9, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    invoke-virtual {p1, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    sput v8, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    .line 223
    :cond_4
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->fetchDataListIfNecessary()V

    .line 224
    const-string v8, "NewsWidgetService_refreshAction"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 225
    if-eqz v4, :cond_5

    .line 226
    sget-object v8, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;->requestFreshVersion(Landroid/content/Context;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 227
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v5

    .line 228
    .local v5, "manager":Landroid/appwidget/AppWidgetManager;
    new-instance v8, Landroid/content/ComponentName;

    const-class v9, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;

    invoke-direct {v8, p0, v9}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 229
    invoke-virtual {v5, v8}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v3

    .line 230
    .local v3, "appWidgetIds":[I
    array-length v8, v3

    :goto_1
    if-ge v7, v8, :cond_5

    aget v6, v3, v7

    .line 231
    .local v6, "widgetId":I
    invoke-static {p0, v6}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showRefreshProgressIndicator(Landroid/content/Context;I)V

    .line 230
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 234
    .end local v3    # "appWidgetIds":[I
    .end local v5    # "manager":Landroid/appwidget/AppWidgetManager;
    .end local v6    # "widgetId":I
    :cond_5
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->scheduleStop()I

    move-result v7

    goto :goto_0

    .line 235
    :cond_6
    const-string v7, "NewsWidgetService_nextAction"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 239
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->sendArticleSeenAnalyticsEvent()V

    .line 241
    sget v7, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    add-int/lit8 v7, v7, 0x1

    sput v7, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    .line 247
    :cond_7
    :goto_2
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v7}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v7

    if-nez v7, :cond_9

    .line 248
    invoke-static {p0, v2, v0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showLoadingView(Landroid/content/Context;ILandroid/accounts/Account;)V

    .line 249
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->scheduleStop()I

    move-result v7

    goto/16 :goto_0

    .line 242
    :cond_8
    const-string v7, "NewsWidgetService_previousAction"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 243
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->sendArticleSeenAnalyticsEvent()V

    .line 244
    sget v7, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    add-int/lit8 v7, v7, -0x1

    sput v7, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->currentPosition:I

    goto :goto_2

    .line 251
    :cond_9
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->readNowWidgetList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v7}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v7

    if-nez v7, :cond_a

    .line 252
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showNoPostsFound(Landroid/content/Context;I)V

    .line 253
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->scheduleStop()I

    move-result v7

    goto/16 :goto_0

    .line 256
    :cond_a
    const-string v7, "NewsWidgetService_nextAction"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    const-string v7, "NewsWidgetService_previousAction"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 257
    :cond_b
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->validatePositionAndUpdateCache(I)V

    .line 258
    const-string v7, "NewsWidgetService_nextAction"

    .line 259
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 258
    invoke-static {p0, v2, v7}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showNavigationProgressIndicator(Landroid/content/Context;IZ)V

    .line 260
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->updateRemoteViews(I)V

    .line 266
    :cond_c
    :goto_3
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->scheduleStop()I

    move-result v7

    goto/16 :goto_0

    .line 261
    :cond_d
    const-string v7, "NewsWidgetService_updateAction"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 262
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->validatePositionAndUpdateCache(I)V

    .line 263
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->updateRemoteViews(I)V

    goto :goto_3
.end method
