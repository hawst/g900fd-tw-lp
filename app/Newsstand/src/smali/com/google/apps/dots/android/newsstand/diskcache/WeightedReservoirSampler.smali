.class public Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;
.super Ljava/lang/Object;
.source "WeightedReservoirSampler.java"


# instance fields
.field private final random:Ljava/util/Random;

.field private final samples:[I

.field private totalWeight:D


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "numSamples"    # I

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->random:Ljava/util/Random;

    .line 16
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->samples:[I

    .line 17
    return-void
.end method

.method private binomial(D)I
    .locals 11
    .param p1, "p"    # D

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 21
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->samples:[I

    array-length v2, v6

    .line 22
    .local v2, "n":I
    const-wide/16 v6, 0x0

    cmpg-double v6, p1, v6

    if-gtz v6, :cond_1

    .line 23
    const/4 v3, 0x0

    .line 35
    :cond_0
    :goto_0
    return v3

    .line 24
    :cond_1
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpl-double v6, p1, v6

    if-lez v6, :cond_2

    .line 25
    sub-double v6, v8, p1

    invoke-direct {p0, v6, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->binomial(D)I

    move-result v6

    sub-int v3, v2, v6

    goto :goto_0

    .line 27
    :cond_2
    sub-double v6, v8, p1

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    .line 28
    .local v0, "c":D
    const-wide/16 v4, 0x0

    .line 29
    .local v4, "y":D
    const/4 v3, 0x0

    .line 31
    .local v3, "x":I
    :goto_1
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->random:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextDouble()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    add-double/2addr v6, v8

    add-double/2addr v4, v6

    .line 32
    int-to-double v6, v2

    cmpg-double v6, v4, v6

    if-gez v6, :cond_0

    .line 33
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public getSamples()[I
    .locals 4

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->totalWeight:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->samples:[I

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next(ID)V
    .locals 10
    .param p1, "item"    # I
    .param p2, "weight"    # D

    .prologue
    .line 42
    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->totalWeight:D

    add-double/2addr v6, p2

    iput-wide v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->totalWeight:D

    .line 43
    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->totalWeight:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-lez v5, :cond_0

    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->totalWeight:D

    div-double v2, p2, v6

    .line 44
    .local v2, "p":D
    :goto_0
    invoke-direct {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->binomial(D)I

    move-result v4

    .line 45
    .local v4, "swaps":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v4, :cond_1

    .line 47
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->random:Ljava/util/Random;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->samples:[I

    array-length v6, v6

    sub-int/2addr v6, v0

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    add-int v1, v0, v5

    .line 48
    .local v1, "index":I
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->samples:[I

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->samples:[I

    aget v6, v6, v0

    aput v6, v5, v1

    .line 49
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->samples:[I

    aput p1, v5, v0

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 43
    .end local v0    # "i":I
    .end local v1    # "index":I
    .end local v2    # "p":D
    .end local v4    # "swaps":I
    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto :goto_0

    .line 51
    .restart local v0    # "i":I
    .restart local v2    # "p":D
    .restart local v4    # "swaps":I
    :cond_1
    return-void
.end method
