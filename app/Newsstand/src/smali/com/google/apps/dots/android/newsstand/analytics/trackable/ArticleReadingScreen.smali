.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;
.source "ArticleReadingScreen.java"


# instance fields
.field private final page:I

.field private final postId:Ljava/lang/String;

.field private final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;I)V
    .locals 0
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "postId"    # Ljava/lang/String;
    .param p4, "page"    # I

    .prologue
    .line 35
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 37
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 41
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->postId:Ljava/lang/String;

    .line 42
    iput p4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->page:I

    .line 43
    return-void
.end method

.method private getRelatedArticlesScreen(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "postId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 114
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    invoke-virtual {v1, v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 113
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 115
    .local v0, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    if-nez v0, :cond_1

    .line 116
    new-instance v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v3, "Could not find post for postId = "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {v2, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_1
    const-string v1, "%s %s - %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "[Related]"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 119
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 118
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 134
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 135
    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;

    .line 136
    .local v0, "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->postId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->postId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->page:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->page:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 137
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 138
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 140
    .end local v0    # "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;
    :cond_0
    return v1
.end method

.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 11
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->postId:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->getPostSummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v1

    .line 58
    .local v1, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 60
    .local v0, "originalEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->postId:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v5

    .line 61
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v5

    iget-object v6, v1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    .line 62
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v5

    iget-object v6, v1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    .line 63
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v5

    .line 64
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v5

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v6, v6, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 65
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v5

    .line 66
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppFamilyName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v5

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->page:I

    .line 67
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPage(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 68
    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setUserSubscriptionType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v5

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 69
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 77
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v5, v5, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    if-eqz v5, :cond_2

    .line 78
    const-string v2, "ReadNow"

    .line 95
    .local v2, "readFromString":Ljava/lang/String;
    :goto_0
    if-eqz v2, :cond_0

    .line 96
    const-string v5, "ReadFrom"

    invoke-static {p1, v5, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_0
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v3

    .line 100
    .local v3, "readingEditionAppId":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 101
    const-string v5, "ReadFromAppId"

    invoke-static {p1, v5, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_1
    const-string v5, "TranslatedLanguage"

    .line 106
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->getTranslatedLanguage()Ljava/lang/String;

    move-result-object v6

    .line 105
    invoke-static {p1, v5, v6}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {p1, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->addCuratedDimensionIfNecessary(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 109
    return-object p1

    .line 79
    .end local v2    # "readFromString":Ljava/lang/String;
    .end local v3    # "readingEditionAppId":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v5, v5, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    if-eqz v5, :cond_4

    .line 80
    const-string v5, "[Topic] - "

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v5, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .line 81
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v6, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "readFromString":Ljava/lang/String;
    :goto_1
    goto :goto_0

    .end local v2    # "readFromString":Ljava/lang/String;
    :cond_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 82
    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v5, v5, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v5, :cond_5

    .line 83
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v5, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->getSection(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Section;

    move-result-object v4

    .line 84
    .local v4, "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    const-string v5, "[Section] "

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 85
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x3

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 86
    .restart local v2    # "readFromString":Ljava/lang/String;
    goto/16 :goto_0

    .end local v2    # "readFromString":Ljava/lang/String;
    .end local v4    # "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    :cond_5
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v5, v5, Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    if-eqz v5, :cond_6

    .line 87
    const-string v2, "Bookmarks"

    .restart local v2    # "readFromString":Ljava/lang/String;
    goto/16 :goto_0

    .line 88
    .end local v2    # "readFromString":Ljava/lang/String;
    :cond_6
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v5, v5, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    if-eqz v5, :cond_7

    .line 89
    const-string v2, "Search"

    .restart local v2    # "readFromString":Ljava/lang/String;
    goto/16 :goto_0

    .line 90
    .end local v2    # "readFromString":Ljava/lang/String;
    :cond_7
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v5, v5, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    if-eqz v5, :cond_8

    .line 91
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->postId:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->getRelatedArticlesScreen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "readFromString":Ljava/lang/String;
    goto/16 :goto_0

    .line 93
    .end local v2    # "readFromString":Ljava/lang/String;
    :cond_8
    const/4 v2, 0x0

    .restart local v2    # "readFromString":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method protected getDedupeExpiryTime()J
    .locals 2

    .prologue
    .line 124
    const-wide/32 v0, 0xafc8

    return-wide v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->postId:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->getPostSummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    .line 49
    .local v0, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    const-string v1, "[Article] "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 50
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 146
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->postId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->page:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleReadingScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isDedupable()Z
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x1

    return v0
.end method
