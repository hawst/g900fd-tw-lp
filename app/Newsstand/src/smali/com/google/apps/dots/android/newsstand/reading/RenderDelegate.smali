.class public interface abstract Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;
.super Ljava/lang/Object;
.source "RenderDelegate.java"


# virtual methods
.method public abstract onArticleOverscrolled(IIZZ)V
.end method

.method public abstract onArticleScrolled(IIIZ)V
.end method

.method public abstract onArticleUnhandledTouchEvent()V
.end method

.method public abstract onLayoutChange(III)V
.end method

.method public abstract onLayoutComplete()V
.end method

.method public abstract onLayoutFailed(Ljava/lang/Throwable;)V
.end method

.method public abstract onPageChanged(I)V
.end method

.method public abstract onToggleActionBar()V
.end method

.method public abstract onTouchEvent(Landroid/view/MotionEvent;)V
.end method
