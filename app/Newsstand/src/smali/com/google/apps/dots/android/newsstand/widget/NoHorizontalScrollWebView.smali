.class public abstract Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;
.super Landroid/webkit/WebView;
.source "NoHorizontalScrollWebView.java"


# instance fields
.field private lastOverScroll:Z

.field private useScrollingHack:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->lastOverScroll:Z

    .line 28
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->lastOverScroll:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->requestDisallowInterceptTouchEvent(Z)V

    .line 29
    return-void
.end method


# virtual methods
.method protected onOverScrolled(IIZZ)V
    .locals 1
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->useScrollingHack:Z

    if-eqz v0, :cond_0

    .line 56
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->lastOverScroll:Z

    .line 57
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->lastOverScroll:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->requestDisallowInterceptTouchEvent(Z)V

    .line 59
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebView;->onOverScrolled(IIZZ)V

    .line 60
    return-void

    .line 57
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 37
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->useScrollingHack:Z

    if-eqz v2, :cond_1

    .line 38
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->lastOverScroll:Z

    .line 39
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 40
    .local v0, "handled":Z
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->lastOverScroll:Z

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->requestDisallowInterceptTouchEvent(Z)V

    .line 43
    .end local v0    # "handled":Z
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected useHorizontalScrollingHack()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->useScrollingHack:Z

    .line 33
    return-void
.end method
