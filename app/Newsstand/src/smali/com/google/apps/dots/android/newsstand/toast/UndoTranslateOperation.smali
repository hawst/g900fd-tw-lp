.class public Lcom/google/apps/dots/android/newsstand/toast/UndoTranslateOperation;
.super Lcom/google/apps/dots/android/newsstand/toast/UndoToastBarOperation;
.source "UndoTranslateOperation.java"


# instance fields
.field private editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V
    .locals 2
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p4, "translatedToLanguageCode"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->undo:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/toast/UndoToastBarOperation;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p3, p4}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->getTranslatedEditionSummary(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/toast/UndoTranslateOperation;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 32
    return-void
.end method


# virtual methods
.method public onActionClicked(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/toast/UndoTranslateOperation;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/toast/UndoTranslateOperation;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/toast/UndoTranslateOperation;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->resetNewsSubscriptionTranslation(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    .line 42
    return-void
.end method
