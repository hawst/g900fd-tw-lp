.class Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$1;
.super Ljava/lang/Object;
.source "CrashReportActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 41
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;

    const/4 v3, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->exitOnDismiss:Z
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->access$002(Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;Z)Z

    .line 42
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->feedbackMechanism()Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;

    move-result-object v1

    .line 43
    .local v1, "mechanism":Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;

    invoke-direct {v0, v2}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;-><init>(Landroid/app/Activity;)V

    .line 44
    .local v0, "info":Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;->launchCrashReportIntent(Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;)V

    .line 45
    return-void
.end method
