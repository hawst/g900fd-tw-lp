.class public Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;
.super Landroid/support/v4/app/Fragment;
.source "ActionMessageFragment.java"


# instance fields
.field private actionMessage:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

.field private actionMessageData:Lcom/google/android/libraries/bind/data/Data;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->action_message_fragment:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 24
    .local v0, "contentView":Landroid/view/View;
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->action_message:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;->actionMessage:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    .line 25
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;->actionMessageData:Lcom/google/android/libraries/bind/data/Data;

    if-eqz v1, :cond_0

    .line 26
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;->actionMessage:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;->actionMessageData:Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->configure(Lcom/google/android/libraries/bind/data/Data;)V

    .line 28
    :cond_0
    return-object v0
.end method

.method public setActionMessageData(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;
    .locals 1
    .param p1, "actionMessageData"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;->actionMessageData:Lcom/google/android/libraries/bind/data/Data;

    .line 37
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;->actionMessage:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/ActionMessageFragment;->actionMessage:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->configure(Lcom/google/android/libraries/bind/data/Data;)V

    .line 40
    :cond_0
    return-object p0
.end method
