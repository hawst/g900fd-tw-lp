.class Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "EditionPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updateImageRotator()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

.field final synthetic val$pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 471
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;->val$pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 3
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 474
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;"
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 476
    .local v0, "headerImageList":Lcom/google/android/libraries/bind/data/DataList;
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->setupImageRotator(Lcom/google/android/libraries/bind/data/DataList;)V
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->access$600(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Lcom/google/android/libraries/bind/data/DataList;)V

    .line 477
    return-void

    .line 474
    .end local v0    # "headerImageList":Lcom/google/android/libraries/bind/data/DataList;
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;->val$pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .line 475
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->headerImageList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 471
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
