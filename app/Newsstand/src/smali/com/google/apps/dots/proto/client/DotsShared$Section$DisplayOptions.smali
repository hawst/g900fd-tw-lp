.class public final Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Section;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayOptions"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;


# instance fields
.field private bitField0_:I

.field private defaultSortDirection_:I

.field private defaultSortField_:Ljava/lang/String;

.field private displayStyle_:I

.field private displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

.field private headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

.field private matchSourceOrder_:Z

.field private phoneDisplayStyle_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20605
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20606
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 20627
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayStyle_:I

    .line 20646
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->phoneDisplayStyle_:I

    .line 20684
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortField_:Ljava/lang/String;

    .line 20706
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortDirection_:I

    .line 20744
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->matchSourceOrder_:Z

    .line 20606
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;
    .locals 3

    .prologue
    .line 20779
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20783
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v2, :cond_0

    .line 20784
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 20786
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v2, :cond_1

    .line 20787
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 20789
    :cond_1
    return-object v0

    .line 20780
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;
    :catch_0
    move-exception v1

    .line 20781
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20602
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 20794
    if-ne p1, p0, :cond_1

    .line 20802
    :cond_0
    :goto_0
    return v1

    .line 20795
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 20796
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    .line 20797
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayStyle_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayStyle_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->phoneDisplayStyle_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->phoneDisplayStyle_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_3

    .line 20799
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortField_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortField_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 20800
    :goto_2
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortDirection_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortDirection_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_3

    .line 20802
    :goto_3
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->matchSourceOrder_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->matchSourceOrder_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 20797
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 20799
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortField_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortField_:Ljava/lang/String;

    .line 20800
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 20802
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3
.end method

.method public getDisplayTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
    .locals 1

    .prologue
    .line 20667
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    return-object v0
.end method

.method public getHeaderTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
    .locals 1

    .prologue
    .line 20727
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 20846
    const/4 v0, 0x0

    .line 20847
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 20848
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayStyle_:I

    .line 20849
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 20851
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 20852
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortField_:Ljava/lang/String;

    .line 20853
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20855
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 20856
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortDirection_:I

    .line 20857
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 20859
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v1, :cond_3

    .line 20860
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 20861
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20863
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 20864
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->phoneDisplayStyle_:I

    .line 20865
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 20867
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v1, :cond_5

    .line 20868
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 20869
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20871
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_6

    .line 20872
    const/16 v1, 0x17

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->matchSourceOrder_:Z

    .line 20873
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 20875
    :cond_6
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->cachedSize:I

    .line 20876
    return v0
.end method

.method public hasDisplayTemplate()Z
    .locals 1

    .prologue
    .line 20677
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 20807
    const/16 v0, 0x11

    .line 20808
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 20809
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayStyle_:I

    add-int v0, v1, v3

    .line 20810
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->phoneDisplayStyle_:I

    add-int v0, v1, v3

    .line 20811
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 20812
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortField_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 20813
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortDirection_:I

    add-int v0, v1, v3

    .line 20814
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 20815
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->matchSourceOrder_:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_3
    add-int v0, v2, v1

    .line 20816
    return v0

    .line 20811
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->hashCode()I

    move-result v1

    goto :goto_0

    .line 20812
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortField_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 20814
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->hashCode()I

    move-result v2

    goto :goto_2

    .line 20815
    :cond_3
    const/4 v1, 0x2

    goto :goto_3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20884
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 20885
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 20889
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 20890
    :sswitch_0
    return-object p0

    .line 20895
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 20896
    .local v1, "temp":I
    if-eqz v1, :cond_1

    if-eq v1, v4, :cond_1

    if-eq v1, v5, :cond_1

    if-eq v1, v6, :cond_1

    if-eq v1, v7, :cond_1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_2

    .line 20903
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayStyle_:I

    .line 20904
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    goto :goto_0

    .line 20906
    :cond_2
    iput v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayStyle_:I

    goto :goto_0

    .line 20911
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortField_:Ljava/lang/String;

    .line 20912
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    goto :goto_0

    .line 20916
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 20917
    .restart local v1    # "temp":I
    if-eqz v1, :cond_3

    if-eq v1, v4, :cond_3

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 20920
    :cond_3
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortDirection_:I

    .line 20921
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    goto :goto_0

    .line 20923
    :cond_4
    iput v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortDirection_:I

    goto :goto_0

    .line 20928
    .end local v1    # "temp":I
    :sswitch_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v2, :cond_5

    .line 20929
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 20931
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 20935
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 20936
    .restart local v1    # "temp":I
    if-eqz v1, :cond_6

    if-eq v1, v4, :cond_6

    if-eq v1, v5, :cond_6

    if-eq v1, v6, :cond_6

    if-eq v1, v7, :cond_6

    const/4 v2, 0x6

    if-eq v1, v2, :cond_6

    const/4 v2, 0x7

    if-ne v1, v2, :cond_7

    .line 20943
    :cond_6
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->phoneDisplayStyle_:I

    .line 20944
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    goto :goto_0

    .line 20946
    :cond_7
    iput v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->phoneDisplayStyle_:I

    goto/16 :goto_0

    .line 20951
    .end local v1    # "temp":I
    :sswitch_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v2, :cond_8

    .line 20952
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 20954
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 20958
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->matchSourceOrder_:Z

    .line 20959
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    goto/16 :goto_0

    .line 20885
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x6a -> :sswitch_4
        0x70 -> :sswitch_5
        0xb2 -> :sswitch_6
        0xb8 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20602
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20821
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 20822
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayStyle_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 20824
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 20825
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortField_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 20827
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 20828
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->defaultSortDirection_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 20830
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v0, :cond_3

    .line 20831
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->displayTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 20833
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 20834
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->phoneDisplayStyle_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 20836
    :cond_4
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v0, :cond_5

    .line 20837
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->headerTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 20839
    :cond_5
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 20840
    const/16 v0, 0x17

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->matchSourceOrder_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 20842
    :cond_6
    return-void
.end method
