.class public final enum Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;
.super Ljava/lang/Enum;
.source "HidingActionBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

.field public static final enum MODE_AUTO_HIDE:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

.field public static final enum MODE_HIDE_ON_SCROLL:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61
    new-instance v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    const-string v1, "MODE_AUTO_HIDE"

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->MODE_AUTO_HIDE:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    const-string v1, "MODE_HIDE_ON_SCROLL"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->MODE_HIDE_ON_SCROLL:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    .line 60
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->MODE_AUTO_HIDE:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->MODE_HIDE_ON_SCROLL:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->$VALUES:[Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    const-class v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->$VALUES:[Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    return-object v0
.end method
