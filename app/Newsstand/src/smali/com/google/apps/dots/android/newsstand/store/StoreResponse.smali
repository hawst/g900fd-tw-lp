.class public Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
.super Ljava/lang/Object;
.source "StoreResponse.java"


# instance fields
.field public final blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

.field public final blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

.field public final version:Lcom/google/apps/dots/android/newsstand/store/Version;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;Lcom/google/apps/dots/android/newsstand/store/Version;)V
    .locals 0
    .param p1, "blobFile"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p2, "blobMetadata"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .param p3, "version"    # Lcom/google/apps/dots/android/newsstand/store/Version;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    .line 19
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    .line 20
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    .line 21
    return-void
.end method
