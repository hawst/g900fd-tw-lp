.class final Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "AnimationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fade(Landroid/view/View;IILjava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$fadeIn:Z

.field final synthetic val$finishedRunnable:Ljava/lang/Runnable;

.field final synthetic val$oldLayerType:I

.field final synthetic val$useHardwareLayer:Z

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(ZLandroid/view/View;IZLjava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$useHardwareLayer:Z

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$view:Landroid/view/View;

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$oldLayerType:I

    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$fadeIn:Z

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$finishedRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 106
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$useHardwareLayer:Z

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$view:Landroid/view/View;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$oldLayerType:I

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$view:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$fadeIn:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$view:Landroid/view/View;

    # invokes: Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->setAlphaAnimatorTag(Landroid/view/View;Landroid/animation/ObjectAnimator;)V
    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->access$000(Landroid/view/View;Landroid/animation/ObjectAnimator;)V

    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$finishedRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil$1;->val$finishedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 115
    :cond_1
    return-void

    .line 109
    :cond_2
    const/4 v0, 0x4

    goto :goto_0
.end method
