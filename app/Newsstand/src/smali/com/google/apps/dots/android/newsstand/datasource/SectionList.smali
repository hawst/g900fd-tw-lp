.class public Lcom/google/apps/dots/android/newsstand/datasource/SectionList;
.super Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
.source "SectionList.java"


# static fields
.field protected static final DEFAULT_EQUALITY_FIELDS:[I

.field public static final DK_SECTION_ID:I

.field public static final DK_SECTION_NAME:I

.field public static final DK_SECTION_SUMMARY:I


# instance fields
.field private final account:Landroid/accounts/Account;

.field private final edition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

.field private onDevicePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->SectionList_sectionId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_ID:I

    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->SectionList_sectionName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_NAME:I

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->SectionList_sectionSummary:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_SUMMARY:I

    .line 32
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_NAME:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DEFAULT_EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 39
    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_ID:I

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;-><init>(I)V

    .line 42
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 45
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->account:Landroid/accounts/Account;

    .line 46
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    .end local p1    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->edition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/datasource/SectionList;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/SectionList;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->account:Landroid/accounts/Account;

    return-object v0
.end method


# virtual methods
.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->edition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->sectionCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onRegisterForInvalidation()V
    .locals 5

    .prologue
    .line 56
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->onRegisterForInvalidation()V

    .line 57
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/SectionList$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/SectionList$1;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/SectionList;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "downloadedOnly"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "pinnedAccounts"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->onDevicePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->account:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->invalidateApiUri(Landroid/accounts/Account;)V

    .line 65
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->onUnregisterForInvalidation()V

    .line 70
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->onDevicePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->onDevicePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 72
    return-void
.end method

.method protected processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
    .locals 2
    .param p1, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "rootNode"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList$2;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->primaryKey()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/SectionList$2;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/SectionList;I)V

    .line 93
    .local v0, "visitor":Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;

    invoke-direct {v1, p2, p3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 94
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->getResults()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method
