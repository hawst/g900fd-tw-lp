.class public interface abstract Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowLayout;
.super Ljava/lang/Object;
.source "GroupRowLayout.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/ConditionalSizeViewGroup;


# virtual methods
.method public abstract arrangeLayout(Ljava/util/List;Ljava/util/ArrayList;I)[I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)[I"
        }
    .end annotation
.end method

.method public abstract prepareGroupRowForRecycling()V
.end method
