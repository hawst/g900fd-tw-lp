.class public Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
.super Ljava/lang/Object;
.source "HomeFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final account:Landroid/accounts/Account;

.field public final homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/home/HomePage;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "homePage"    # Lcom/google/apps/dots/android/newsstand/home/HomePage;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 28
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->account:Landroid/accounts/Account;

    .line 29
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 38
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 39
    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    .line 40
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->account:Landroid/accounts/Account;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->account:Landroid/accounts/Account;

    .line 41
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 43
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->account:Landroid/accounts/Account;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 33
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "{home_page: %s, account: %s}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->account:Landroid/accounts/Account;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->account:Landroid/accounts/Account;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 60
    return-void
.end method
