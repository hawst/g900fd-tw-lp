.class public abstract Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
.source "StatefulFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S::",
        "Landroid/os/Parcelable;",
        ">",
        "Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;"
    }
.end annotation


# instance fields
.field private final defaultState:Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field

.field protected eventHandler:Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler",
            "<TS;>;"
        }
    .end annotation
.end field

.field private final fragmentLayoutResId:I

.field private initialState:Landroid/os/Bundle;

.field private isChangingState:Z

.field private ownsActionBar:Z

.field private final stateExtraKey:Ljava/lang/String;

.field private stateStack:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TS;>;"
        }
    .end annotation
.end field

.field private upcomingState:Landroid/os/Bundle;


# direct methods
.method protected constructor <init>(Landroid/os/Parcelable;Ljava/lang/String;I)V
    .locals 1
    .param p2, "stateExtraKey"    # Ljava/lang/String;
    .param p3, "fragmentLayoutResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    .local p1, "defaultState":Landroid/os/Parcelable;, "TS;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;-><init>()V

    .line 48
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateExtraKey:Ljava/lang/String;

    .line 49
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->fragmentLayoutResId:I

    .line 50
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->defaultState:Landroid/os/Parcelable;

    .line 51
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    .line 52
    return-void
.end method

.method private changeStateStack(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    .local p1, "newStateStack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TS;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 222
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 224
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    .line 226
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    .line 227
    .local v1, "oldState":Landroid/os/Parcelable;, "TS;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    .line 228
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    .line 229
    .local v0, "newState":Landroid/os/Parcelable;, "TS;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v5, "changeState: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v2, v5, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 230
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onStateSet(Landroid/os/Parcelable;)V

    .line 231
    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    .line 232
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    .line 233
    return-void

    .end local v0    # "newState":Landroid/os/Parcelable;, "TS;"
    .end local v1    # "oldState":Landroid/os/Parcelable;, "TS;"
    :cond_0
    move v2, v4

    .line 222
    goto :goto_0
.end method

.method private popStateIfPossible()Z
    .locals 7

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 267
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 269
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    .line 273
    .local v1, "oldState":Landroid/os/Parcelable;, "TS;"
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 294
    :goto_1
    return v4

    .end local v1    # "oldState":Landroid/os/Parcelable;, "TS;"
    :cond_1
    move v2, v4

    .line 267
    goto :goto_0

    .line 276
    .restart local v1    # "oldState":Landroid/os/Parcelable;, "TS;"
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 277
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    .line 278
    .local v0, "newState":Landroid/os/Parcelable;, "TS;"
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 286
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    .line 287
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v5, "popState: %s"

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v0, v6, v4

    invoke-virtual {v2, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onStateSet(Landroid/os/Parcelable;)V

    .line 289
    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    .line 290
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    .line 291
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->eventHandler:Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;

    if-eqz v2, :cond_3

    .line 292
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->eventHandler:Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;

    invoke-interface {v2, p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;->onStateChanged(Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    :cond_3
    move v4, v3

    .line 294
    goto :goto_1
.end method


# virtual methods
.method public final changeState(Landroid/os/Parcelable;Z)V
    .locals 5
    .param p2, "userDriven"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;Z)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    .local p1, "newState":Landroid/os/Parcelable;, "TS;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 188
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 189
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "state unchanged"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    if-nez v1, :cond_0

    .line 197
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    .line 200
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    .line 201
    .local v0, "oldState":Landroid/os/Parcelable;, "TS;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 202
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    :goto_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "changeState: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onStateSet(Landroid/os/Parcelable;)V

    .line 208
    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    .line 209
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    .line 210
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->eventHandler:Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;

    if-eqz v1, :cond_0

    .line 211
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->eventHandler:Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;

    invoke-interface {v1, p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;->onStateChanged(Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 204
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final doOnCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    const/4 v3, 0x0

    .line 68
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->fragmentLayoutResId:I

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 69
    .local v0, "rootView":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->setRootView(Landroid/view/View;)V

    .line 70
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onViewCreated(Landroid/view/View;)V

    .line 73
    const/4 v1, 0x0

    .line 74
    .local v1, "stateHandled":Z
    if-eqz p3, :cond_0

    .line 75
    invoke-virtual {p0, p3}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->handleExtras(Landroid/os/Bundle;)Z

    move-result v1

    .line 77
    :cond_0
    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 78
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->handleExtras(Landroid/os/Bundle;)Z

    move-result v1

    .line 80
    :cond_1
    if-nez v1, :cond_2

    .line 81
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->defaultState:Landroid/os/Parcelable;

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 85
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHasOptionsMenu()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->setHasOptionsMenu(Z)V

    .line 86
    return-object v0
.end method

.method protected abstract getHasOptionsMenu()Z
.end method

.method protected getInitialState()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 183
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->initialState:Landroid/os/Bundle;

    return-object v0
.end method

.method protected getUpcomingState()Landroid/os/Bundle;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 150
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->upcomingState:Landroid/os/Bundle;

    return-object v0
.end method

.method public final handleExtras(Landroid/os/Bundle;)Z
    .locals 8
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 96
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v5

    const-string v6, "Got extras: %s"

    new-array v7, v4, [Ljava/lang/Object;

    aput-object p1, v7, v3

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateExtraKey:Ljava/lang/String;

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 99
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->initialState:Landroid/os/Bundle;

    .line 100
    const-string v5, "addToBackStack"

    .line 101
    invoke-virtual {p1, v5, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 102
    .local v0, "addtoBackStack":Z
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateExtraKey:Ljava/lang/String;

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 105
    .local v2, "stateObject":Ljava/lang/Object;
    instance-of v5, v2, Ljava/util/ArrayList;

    if-eqz v5, :cond_2

    move-object v1, v2

    .line 106
    check-cast v1, Ljava/util/ArrayList;

    .line 111
    .end local v2    # "stateObject":Ljava/lang/Object;
    .local v1, "newStateStack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TS;>;"
    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isActivityStarted()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    .line 112
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gtz v5, :cond_0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->defaultState:Landroid/os/Parcelable;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->state()Landroid/os/Parcelable;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 113
    :cond_0
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Parcelable;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->pushState(Landroid/os/Parcelable;)V

    .line 117
    :goto_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->initialState:Landroid/os/Bundle;

    move v3, v4

    .line 120
    .end local v0    # "addtoBackStack":Z
    .end local v1    # "newStateStack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TS;>;"
    :cond_1
    return v3

    .line 108
    .restart local v0    # "addtoBackStack":Z
    .restart local v2    # "stateObject":Ljava/lang/Object;
    :cond_2
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 109
    .restart local v1    # "newStateStack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TS;>;"
    check-cast v2, Landroid/os/Parcelable;

    .end local v2    # "stateObject":Ljava/lang/Object;
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 115
    :cond_3
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->changeStateStack(Ljava/util/ArrayList;)V

    goto :goto_1
.end method

.method public handleOnBackPressed()Z
    .locals 1

    .prologue
    .line 298
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->popStateIfPossible()Z

    move-result v0

    return v0
.end method

.method public handleUpcomingResult(Landroid/os/Bundle;)Z
    .locals 4
    .param p1, "extras"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    const/4 v3, 0x0

    .line 131
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Got extras: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->upcomingState:Landroid/os/Bundle;

    .line 134
    return v3
.end method

.method protected isChangingState()Z
    .locals 1

    .prologue
    .line 216
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    return v0
.end method

.method protected abstract logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
.end method

.method public onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    .line 58
    sget-object v1, Lcom/google/android/apps/newsstanddev/R$styleable;->StatefulFragment:[I

    invoke-virtual {p1, p2, v1}, Landroid/app/Activity;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 59
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->StatefulFragment_ownsActionBar:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->ownsActionBar:Z

    .line 60
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 61
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 139
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->onResume()V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->upcomingState:Landroid/os/Bundle;

    .line 142
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 165
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 166
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateExtraKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 167
    return-void
.end method

.method protected onStateSet(Landroid/os/Parcelable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .prologue
    .line 326
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    .local p1, "newState":Landroid/os/Parcelable;, "TS;"
    return-void
.end method

.method protected abstract onViewCreated(Landroid/view/View;)V
.end method

.method public ownsActionBar()Z
    .locals 1

    .prologue
    .line 310
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->ownsActionBar:Z

    return v0
.end method

.method public final pushState(Landroid/os/Parcelable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    .local p1, "newState":Landroid/os/Parcelable;, "TS;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 241
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 243
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 244
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Trying to push the same state"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v3

    .line 241
    goto :goto_0

    .line 248
    :cond_2
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    .line 251
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    .line 252
    .local v0, "oldState":Landroid/os/Parcelable;, "TS;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v4, "pushState: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v1, v4, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onStateSet(Landroid/os/Parcelable;)V

    .line 256
    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    .line 257
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->isChangingState:Z

    .line 258
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->eventHandler:Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;

    if-eqz v1, :cond_0

    .line 259
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->eventHandler:Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;

    invoke-interface {v1, p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;->onStateChanged(Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    goto :goto_1
.end method

.method public final setEventHandler(Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    .local p1, "eventHandler":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler<TS;>;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->eventHandler:Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;

    .line 91
    return-void
.end method

.method public setInitialState(Landroid/os/Parcelable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    .local p1, "state":Landroid/os/Parcelable;, "TS;"
    new-instance v0, Landroid/os/Bundle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 175
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 176
    .local v1, "stack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TS;>;"
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateExtraKey:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 178
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->setArguments(Landroid/os/Bundle;)V

    .line 179
    return-void
.end method

.method protected final setupSubFragmentStateIfNeeded(Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1, "subFragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 154
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 155
    check-cast v0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;

    .line 157
    .local v0, "statefulFragment":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<*>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getInitialState()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getInitialState()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->setArguments(Landroid/os/Bundle;)V

    .line 161
    .end local v0    # "statefulFragment":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<*>;"
    :cond_0
    return-void
.end method

.method public state()Landroid/os/Parcelable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation

    .prologue
    .line 170
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<TS;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->stateStack:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    goto :goto_0
.end method

.method protected abstract updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;TS;)V"
        }
    .end annotation
.end method
