.class public Lcom/google/apps/dots/android/newsstand/navigation/PlainEditionIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "PlainEditionIntentBuilder.java"


# instance fields
.field private final intent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 18
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/PlainEditionIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlainEditionIntentBuilder;->intent:Landroid/content/Intent;

    .line 19
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlainEditionIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "PlainEditionFragment_state"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 20
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlainEditionIntentBuilder;->intent:Landroid/content/Intent;

    return-object v0
.end method
