.class Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;
.super Ljava/lang/Object;
.source "AttachmentStore.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getMutableBitmap(IILandroid/graphics/Bitmap$Config;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

.field final synthetic val$config:Landroid/graphics/Bitmap$Config;

.field final synthetic val$height:I

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;IILandroid/graphics/Bitmap$Config;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iput p2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;->val$width:I

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;->val$height:I

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;->val$config:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Landroid/graphics/Bitmap;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 147
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;->val$width:I

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;->val$height:I

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;->val$config:Landroid/graphics/Bitmap$Config;

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getPoolBitmap(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 148
    .local v0, "mutable":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 149
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Couldn\'t allocate mutable bitmap"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 151
    :cond_0
    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$3;->call()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
