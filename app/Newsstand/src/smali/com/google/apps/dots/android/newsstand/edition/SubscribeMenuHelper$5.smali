.class Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "SubscribeMenuHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->unsubscribe(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$unsubscribeActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;->val$unsubscribeActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;->val$account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 4
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;->val$unsubscribeActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;->val$account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 169
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v2

    const/4 v3, 0x1

    .line 168
    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->removeSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V

    .line 170
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 165
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
