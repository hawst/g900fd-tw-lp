.class Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;
.super Ljava/lang/Object;
.source "AuthUiHelper.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->getConfig()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    .prologue
    .line 376
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    const/4 v4, 0x0

    .line 385
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 386
    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "An authentication error has occurred."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 387
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->getAuthPromptIntent(Ljava/lang/Throwable;)Landroid/content/Intent;

    move-result-object v0

    .line 389
    .local v0, "promptIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showedAuthPromptWithoutDialog:Z
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->access$300(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 390
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    const/4 v2, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showedAuthPromptWithoutDialog:Z
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->access$302(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;Z)Z

    .line 391
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    const/16 v2, 0x2c8

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->startActivityForResult(Landroid/content/Intent;I)V

    .line 404
    .end local v0    # "promptIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 393
    .restart local v0    # "promptIntent":Landroid/content/Intent;
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    # setter for: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showedAuthPromptWithoutDialog:Z
    invoke-static {v1, v4}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->access$302(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;Z)Z

    .line 394
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showAuthErrorDialog()V

    .line 396
    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;

    const/4 v2, -0x2

    invoke-direct {v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;-><init>(ILjava/lang/Throwable;)V

    .line 397
    invoke-virtual {v1, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AccountAuthFailedEvent;->track(Z)V

    .line 398
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->flushAnalyticsEvents()V

    goto :goto_0

    .line 402
    .end local v0    # "promptIntent":Landroid/content/Intent;
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->showFirstTimeNoNetworkDialog()V

    goto :goto_0
.end method

.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
    .locals 1
    .param p1, "clientConfig"    # Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->onAuthSuccess()V

    .line 380
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 376
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$4;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V

    return-void
.end method
