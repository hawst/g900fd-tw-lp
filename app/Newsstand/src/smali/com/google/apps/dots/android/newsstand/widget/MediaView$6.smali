.class Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "MediaView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initGestureDetector()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 613
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->triggerFullScreen()V

    .line 642
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 616
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->options:Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$1100(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;->enableControls:Z

    if-eqz v0, :cond_2

    .line 617
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->isMediaPlayable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 619
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 620
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    .line 636
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 622
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    goto :goto_0

    .line 626
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->start()V

    goto :goto_0

    .line 630
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_3

    .line 631
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->start()V

    goto :goto_0

    .line 633
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->pause()V

    goto :goto_0
.end method
