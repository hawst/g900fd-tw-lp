.class public final Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DiskManifest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;


# instance fields
.field private bitField0_:I

.field public entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

.field private version_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 632
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->version_:I

    .line 651
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 10
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    .locals 5

    .prologue
    .line 665
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 669
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 670
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 671
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 672
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 673
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->clone()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    move-result-object v4

    aput-object v4, v3, v2

    .line 671
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 666
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 667
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 677
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->clone()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 682
    if-ne p1, p0, :cond_1

    .line 686
    :cond_0
    :goto_0
    return v1

    .line 683
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 684
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;

    .line 685
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->version_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->version_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 686
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 718
    const/4 v1, 0x0

    .line 719
    .local v1, "size":I
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 720
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->version_:I

    .line 721
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 723
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    if-eqz v2, :cond_2

    .line 724
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 725
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    if-eqz v0, :cond_1

    .line 726
    const/4 v5, 0x2

    .line 727
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 724
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 731
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_2
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->cachedSize:I

    .line 732
    return v1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 690
    const/16 v1, 0x11

    .line 691
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 692
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->version_:I

    add-int v1, v2, v3

    .line 693
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    if-nez v2, :cond_1

    mul-int/lit8 v1, v1, 0x1f

    .line 699
    :cond_0
    return v1

    .line 695
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 696
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    add-int v1, v3, v2

    .line 695
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 696
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 740
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 741
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 745
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 746
    :sswitch_0
    return-object p0

    .line 751
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->version_:I

    .line 752
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->bitField0_:I

    goto :goto_0

    .line 756
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 757
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    if-nez v5, :cond_2

    move v1, v4

    .line 758
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 759
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    if-eqz v5, :cond_1

    .line 760
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 762
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 763
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 764
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;-><init>()V

    aput-object v6, v5, v1

    .line 765
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 766
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 763
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 757
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    array-length v1, v5

    goto :goto_1

    .line 769
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;-><init>()V

    aput-object v6, v5, v1

    .line 770
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 741
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;

    move-result-object v0

    return-object v0
.end method

.method public setVersion(I)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 637
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->version_:I

    .line 638
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->bitField0_:I

    .line 639
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 704
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 705
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->version_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 707
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    if-eqz v1, :cond_2

    .line 708
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 709
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    if-eqz v0, :cond_1

    .line 710
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 708
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 714
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_2
    return-void
.end method
