.class public Lcom/google/apps/dots/android/newsstand/card/CardIcon;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "CardIcon.java"


# static fields
.field public static final DK_ASPECT_RATIO:I

.field public static final DK_BACKGROUND:I

.field public static final DK_CONTENT_DESCRIPTION:I

.field public static final DK_IMAGE_ID:I

.field public static final DK_ON_CLICK_LISTENER:I

.field public static final DK_TEXT:I

.field public static final DK_TEXT_COLOR:I

.field public static final DK_TRANSITION_NAME:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 14
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardIcon_imageId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_IMAGE_ID:I

    .line 16
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardIcon_text:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_TEXT:I

    .line 18
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardIcon_textColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_TEXT_COLOR:I

    .line 20
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardIcon_background:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_BACKGROUND:I

    .line 22
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardIcon_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_ON_CLICK_LISTENER:I

    .line 24
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardIcon_aspectRatio:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_ASPECT_RATIO:I

    .line 26
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardIcon_contentDescription:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_CONTENT_DESCRIPTION:I

    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardIcon_transitionName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_TRANSITION_NAME:I

    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_icon:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->LAYOUTS:[I

    .line 35
    new-array v0, v2, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method
