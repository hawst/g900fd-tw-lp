.class Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "OnboardQuizMagazineOffersPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->getQuizItemDataList()Lcom/google/android/libraries/bind/data/DataList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private selectedItemIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;
    .param p2, "arg0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 8
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 75
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_OFFER_ID:I

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "itemId":Ljava/lang/String;
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_OFFER_SUMMARY:I

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 78
    .local v3, "offerSummary":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;->selectedItemIds:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 80
    .local v4, "selected":Z
    sget v5, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v6, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->LAYOUT_MAGAZINE:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 81
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_ITEM_ID:I

    invoke-virtual {p1, v5, v0}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 82
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SELECTED:I

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 83
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_DEFAULT_ON:I

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 85
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "magazineTitle":Ljava/lang/String;
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_TITLE:I

    invoke-virtual {p1, v5, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 87
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SOURCE_ICON_ID:I

    .line 88
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v6

    .line 87
    invoke-virtual {p1, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 89
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SOURCE_ASPECT_RATIO:I

    .line 91
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v6

    const/high16 v7, 0x3fc00000    # 1.5f

    .line 90
    invoke-static {v6, v7}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconHeightToWidthRatio(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;F)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    .line 89
    invoke-virtual {p1, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 92
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_CLICK_LISTENER:I

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;

    # invokes: Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->getItemClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    invoke-static {v6, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->access$100(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 93
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SELECTION_COLOR_RES_ID:I

    .line 94
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getOnboardAccentColor(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 93
    invoke-virtual {p1, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 95
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_CONTENT_DESCRIPTION:I

    .line 96
    invoke-static {v1, v4}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->getItemContentDescription(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 95
    invoke-virtual {p1, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 99
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v5

    .line 100
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v6

    .line 99
    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromSummaries(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    .line 101
    .local v2, "offerEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    sget v5, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    new-instance v6, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1$1;

    invoke-direct {v6, p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    invoke-virtual {p1, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 109
    const/4 v5, 0x1

    return v5
.end method

.method public onPreFilter()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;

    # invokes: Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->getSelectedItemIds()Ljava/util/Set;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->access$000(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;->selectedItemIds:Ljava/util/Set;

    .line 71
    return-void
.end method
