.class public Lcom/google/apps/dots/android/newsstand/search/SearchFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final SEARCH_TYPES:[Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;


# instance fields
.field private adapter:Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;

.field private cardList:Lcom/google/android/libraries/bind/data/DataList;

.field private cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

.field private final cardListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private connectivityListener:Ljava/lang/Runnable;

.field private initialEmptyMessageData:Lcom/google/android/libraries/bind/data/Data;

.field private noResultsEmptyMessageData:Lcom/google/android/libraries/bind/data/Data;

.field private final refreshRunnable:Ljava/lang/Runnable;

.field private final searchResultsLists:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    const-class v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 45
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->MAGAZINES:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->NEWS_EDITIONS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->ENTITIES:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->FEEDS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->LIVE_POSTS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->SEARCH_TYPES:[Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 85
    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;-><init>(Ljava/lang/String;)V

    const-string v1, "SearchFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->search_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 52
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->searchResultsLists:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->refreshRunnable:Ljava/lang/Runnable;

    .line 77
    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->adapter:Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->refreshErrorView()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->searchResultsLists:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->refreshRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->initialEmptyMessageData:Lcom/google/android/libraries/bind/data/Data;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->noResultsEmptyMessageData:Lcom/google/android/libraries/bind/data/Data;

    return-object v0
.end method

.method private groupForResultsList(Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 9
    .param p1, "builder"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    .param p2, "list"    # Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    .prologue
    const/4 v8, 0x2

    .line 220
    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->type:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    .line 223
    .local v2, "listType":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->LIVE_POSTS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-eq v2, v5, :cond_0

    sget-object v5, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->SAVED_POSTS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-ne v2, v5, :cond_1

    .line 224
    :cond_0
    new-instance v5, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    invoke-direct {v5, p2}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->setIncludeOversizedLayouts(Z)Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    move-result-object v0

    .line 242
    .local v0, "group":Lcom/google/android/libraries/bind/card/CardGroup;
    :goto_0
    iget-object v5, p2, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->type:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->shelfTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 245
    .local v4, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->state()Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-eqz v5, :cond_2

    .line 247
    invoke-virtual {p1, v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeShelfHeader(Ljava/lang/String;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 268
    .local v1, "header":Lcom/google/android/libraries/bind/data/Data;
    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/card/CardGroup;->addHeader(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 269
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/bind/card/CardGroup;->setHideOnError(Z)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 271
    return-object v0

    .line 226
    .end local v0    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    .end local v1    # "header":Lcom/google/android/libraries/bind/data/Data;
    .end local v4    # "title":Ljava/lang/String;
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 227
    .restart local v0    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$8;->$SwitchMap$com$google$apps$dots$android$newsstand$search$SearchResultsList$Type:[I

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move-object v5, v0

    .line 231
    check-cast v5, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    sget v6, Lcom/google/android/apps/newsstanddev/R$dimen;->extra_large_column_width:I

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setColumnWidthResId(I)Lcom/google/android/libraries/bind/card/GridGroup;

    goto :goto_0

    :pswitch_1
    move-object v5, v0

    .line 234
    check-cast v5, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    .line 235
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/newsstanddev/R$integer;->num_magazine_cols:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    .line 234
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setFixedNumColumns(I)Lcom/google/android/libraries/bind/card/GridGroup;

    goto :goto_0

    .line 250
    .restart local v4    # "title":Ljava/lang/String;
    :cond_2
    new-instance v3, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$7;

    invoke-direct {v3, p0, v2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;)V

    .line 259
    .local v3, "listener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->more_button:I

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 258
    invoke-virtual {p1, v4, v5, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeShelfHeaderWithButton(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 261
    .restart local v1    # "header":Lcom/google/android/libraries/bind/data/Data;
    instance-of v5, v0, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    if-eqz v5, :cond_3

    move-object v5, v0

    .line 262
    check-cast v5, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    invoke-virtual {v5, v8}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setMaxRows(I)Lcom/google/android/libraries/bind/card/GridGroup;

    goto :goto_1

    :cond_3
    move-object v5, v0

    .line 264
    check-cast v5, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    invoke-virtual {v5, v8}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;->setMaxRows(I)Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    goto :goto_1

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private refreshErrorView()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->adapter:Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->adapter:Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;->refreshErrorView()Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 176
    :cond_0
    return-void
.end method

.method private setUpAdapter()V
    .locals 3

    .prologue
    .line 115
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 117
    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$4;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->NONE:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->adapter:Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;

    .line 152
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->adapter:Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$5;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->NONE:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 160
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->adapter:Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$6;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->NONE:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;->setEmptyViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 168
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->adapter:Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 169
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 170
    return-void
.end method

.method private updateAdapter()V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 179
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->state()Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    iget-object v3, v6, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    .line 181
    .local v3, "query":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 183
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->adapter:Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 217
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->searchResultsLists:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 191
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->state()Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-nez v6, :cond_1

    sget-object v5, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->SEARCH_TYPES:[Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    .line 193
    .local v5, "types":[Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    :goto_1
    array-length v8, v5

    move v6, v7

    :goto_2
    if-ge v6, v8, :cond_3

    aget-object v4, v5, v6

    .line 194
    .local v4, "type":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    sget-object v7, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->MAGAZINES:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-ne v4, v7, :cond_2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/play/MarketInfo;->areMagazinesAvailable()Z

    move-result v7

    if-nez v7, :cond_2

    .line 193
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 191
    .end local v4    # "type":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    .end local v5    # "types":[Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    :cond_1
    const/4 v6, 0x1

    new-array v5, v6, [Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->state()Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v6, v5, v7

    goto :goto_1

    .line 198
    .restart local v4    # "type":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    .restart local v5    # "types":[Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    :cond_2
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->searchResultsLists:Ljava/util/ArrayList;

    new-instance v9, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-direct {v9, v10, v4, v3}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;Ljava/lang/String;)V

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 201
    .end local v4    # "type":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    :cond_3
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->removeAll()Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 202
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->searchResultsLists:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    .line 203
    .local v2, "list":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-direct {p0, v6, v2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->groupForResultsList(Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;)Lcom/google/android/libraries/bind/card/CardGroup;

    move-result-object v0

    .line 204
    .local v0, "group":Lcom/google/android/libraries/bind/card/CardGroup;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {v6, v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 205
    .local v1, "groupData":Lcom/google/android/libraries/bind/data/Data;
    sget v8, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    const-string v9, "shelf_"

    iget-object v6, v2, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->type:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v9, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    :goto_5
    invoke-virtual {v1, v8, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 206
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {v6, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 207
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->freshen()V

    .line 208
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    goto :goto_4

    .line 205
    :cond_4
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 211
    .end local v0    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    .end local v1    # "groupData":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "list":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;
    :cond_5
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v6, :cond_6

    .line 212
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v6, v7}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 214
    :cond_6
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/libraries/bind/data/DataList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    .line 215
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v6, v7}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 216
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->adapter:Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    goto/16 :goto_0
.end method


# virtual methods
.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x0

    return v0
.end method

.method protected getSearchActivity()Lcom/google/apps/dots/android/newsstand/search/SearchActivity;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;

    return-object v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 308
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-eqz v0, :cond_0

    .line 309
    new-instance v2, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    invoke-direct {v2, v0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v1}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->changeState(Landroid/os/Parcelable;Z)V

    move v0, v1

    .line 312
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 317
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onDestroyView()V

    .line 318
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->connectivityListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 322
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 324
    :cond_1
    return-void
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 3
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 99
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 100
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_search:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeStandardEmptyCardData(Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->initialEmptyMessageData:Lcom/google/android/libraries/bind/data/Data;

    .line 102
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_search:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->empty_list_caption_search:I

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeStandardEmptyCardData(Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->noResultsEmptyMessageData:Lcom/google/android/libraries/bind/data/Data;

    .line 104
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->setUpAdapter()V

    .line 106
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 112
    return-void
.end method

.method updateSearchView()V
    .locals 3

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->getSearchActivity()Lcom/google/apps/dots/android/newsstand/search/SearchActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->getSearchView()Landroid/support/v7/widget/SearchView;

    move-result-object v0

    .line 287
    .local v0, "searchView":Landroid/support/v7/widget/SearchView;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 289
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 290
    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->requestFocus()Z

    .line 294
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->getSearchActivity()Lcom/google/apps/dots/android/newsstand/search/SearchActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x4

    .line 295
    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 299
    :goto_0
    return-void

    .line 297
    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    goto :goto_0
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 40
    check-cast p1, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;)V
    .locals 0
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->updateSearchView()V

    .line 282
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->updateAdapter()V

    .line 283
    return-void
.end method
