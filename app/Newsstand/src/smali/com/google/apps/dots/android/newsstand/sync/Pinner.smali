.class public Lcom/google/apps/dots/android/newsstand/sync/Pinner;
.super Ljava/lang/Object;
.source "Pinner.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field public static final PINNABLE_EDITION_TYPES:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final accountNameManager:Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

.field private activeAccount:Landroid/accounts/Account;

.field private final curationsObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

.field private final eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

.field private final lock:Ljava/lang/Object;

.field private final magazinesObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

.field private final newsObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field private final serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 55
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->NEWS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->READ_NOW:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->TOPIC:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SAVED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->PINNABLE_EDITION_TYPES:Ljava/util/EnumSet;

    .line 65
    const-class v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/events/EventNotifier;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;Lcom/google/apps/dots/android/newsstand/server/ServerUris;)V
    .locals 2
    .param p1, "eventNotifier"    # Lcom/google/apps/dots/android/newsstand/events/EventNotifier;
    .param p2, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .param p3, "accountNameManager"    # Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;
    .param p4, "serverUris"    # Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->lock:Ljava/lang/Object;

    .line 78
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 79
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->accountNameManager:Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    .line 80
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 81
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    .line 82
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$1;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadExecutor()Lcom/google/common/util/concurrent/ListeningExecutorService;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner$1;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->newsObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    .line 89
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$2;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadExecutor()Lcom/google/common/util/concurrent/ListeningExecutorService;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner$2;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->curationsObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    .line 96
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$3;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadExecutor()Lcom/google/common/util/concurrent/ListeningExecutorService;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner$3;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->magazinesObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    .line 103
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->activeAccount:Landroid/accounts/Account;

    .line 104
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->register()V

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Landroid/accounts/Account;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpinUnsubscribedNews(Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Landroid/accounts/Account;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpinUnsubscribedCurationsAndMigratePinnedEntityTopics(Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Landroid/accounts/Account;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpinUnsubscribedMagazines(Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/sync/Pinner;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/sync/Pinner;)Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .param p2, "x2"    # Landroid/accounts/Account;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->storePinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V

    return-void
.end method

.method private checkPinnable(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 456
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinnable(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 457
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 459
    :cond_0
    return-void
.end method

.method private defaultPinned(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "pinnedAccounts"    # Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    .prologue
    .line 225
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;-><init>()V

    .line 226
    .local v0, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v1, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;->SAVED_EDITION:Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    const/4 v3, 0x0

    .line 227
    invoke-static {v2, v3, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->newItem(Lcom/google/apps/dots/android/newsstand/edition/Edition;ILcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-result-object v2

    .line 226
    invoke-static {v1, v2}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    iput-object v1, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 228
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->accountNameManager:Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getOriginalName(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->setAccount(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    .line 229
    invoke-static {p2, v0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->setPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V

    .line 230
    return-object v0
.end method

.method private getPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .locals 6
    .param p1, "pinnedAccounts"    # Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 195
    iget-object v2, p1, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 196
    .local v0, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->accountNameManager:Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    invoke-virtual {v4, p2}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getOriginalName(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->getAccount()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 204
    :goto_1
    return-object v0

    .line 195
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 202
    .end local v0    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_1
    invoke-direct {p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->defaultPinned(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v0

    .line 203
    .restart local v0    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->storePinnedAccounts(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;)V

    goto :goto_1
.end method

.method private getPinnedItem(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 306
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->lock:Ljava/lang/Object;

    monitor-enter v4

    .line 307
    :try_start_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v2

    .line 308
    .local v2, "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    invoke-direct {p0, v2, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v1

    .line 309
    .local v1, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v5, v1, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v6, v5

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v6, :cond_1

    aget-object v0, v5, v3

    .line 310
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    invoke-direct {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isForEdition(Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 311
    monitor-exit v4

    .line 315
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :goto_1
    return-object v0

    .line 309
    .restart local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 314
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_1
    monitor-exit v4

    .line 315
    const/4 v0, 0x0

    goto :goto_1

    .line 314
    .end local v1    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .end local v2    # "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private isForEdition(Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 1
    .param p1, "item"    # Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 285
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromProto(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isPinnable(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 2
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 452
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->PINNABLE_EDITION_TYPES:Ljava/util/EnumSet;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .locals 4

    .prologue
    .line 179
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v3, "pinnedAccounts"

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 180
    .local v1, "encoded":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 182
    :try_start_0
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;-><init>()V

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->decodeBase64(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    :goto_0
    return-object v2

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    .line 187
    .end local v0    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :cond_0
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;-><init>()V

    goto :goto_0
.end method

.method private static newItem(Lcom/google/apps/dots/android/newsstand/edition/Edition;ILcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .locals 3
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p1, "pinnedVersion"    # I
    .param p2, "pinnedAccounts"    # Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    .prologue
    .line 239
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;-><init>()V

    .line 240
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->toProto()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->setEdition(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 241
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->getHighestPinId()I

    move-result v2

    add-int/lit8 v1, v2, 0x1

    .line 242
    .local v1, "pinId":I
    invoke-virtual {p2, v1}, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->setHighestPinId(I)Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    .line 243
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->setPinId(I)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 244
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->setPinnedVersion(I)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 245
    return-object v0
.end method

.method private register()V
    .locals 4

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->activeAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->activeAccount:Landroid/accounts/Account;

    .line 121
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyNews(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 120
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->newsObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->registerObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 123
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->activeAccount:Landroid/accounts/Account;

    .line 124
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyCurations(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 123
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->curationsObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->registerObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 126
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->activeAccount:Landroid/accounts/Account;

    .line 127
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 126
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->magazinesObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->registerObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 129
    :cond_0
    return-void
.end method

.method private static setPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V
    .locals 4
    .param p0, "pinnedAccounts"    # Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .param p1, "pinned"    # Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    .prologue
    .line 211
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 212
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    aget-object v1, v2, v0

    .line 213
    .local v1, "itPinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->getAccount()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    aput-object p1, v2, v0

    .line 219
    .end local v1    # "itPinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :goto_1
    return-void

    .line 211
    .restart local v1    # "itPinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218
    .end local v1    # "itPinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    invoke-static {v2, p1}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    goto :goto_1
.end method

.method private storePinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V
    .locals 0
    .param p1, "pinnedAccounts"    # Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .param p2, "pinned"    # Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    .prologue
    .line 252
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->setPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V

    .line 253
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->storePinnedAccounts(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;)V

    .line 254
    return-void
.end method

.method private storePinnedAccounts(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;)V
    .locals 3
    .param p1, "pinnedAccounts"    # Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v1, "pinnedAccounts"

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->encodeBase64(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    return-void
.end method

.method private unpinUnsubscribedCurationsAndMigratePinnedEntityTopics(Landroid/accounts/Account;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 527
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v0

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    .line 528
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyCurations(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 527
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getAvailable(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Landroid/accounts/Account;)V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 626
    return-void
.end method

.method private unpinUnsubscribedHelper(Landroid/accounts/Account;Ljava/lang/String;Ljava/util/EnumSet;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "libraryUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 475
    .local p3, "filterTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v0

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getAvailable(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;

    invoke-direct {v1, p0, p1, p3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Landroid/accounts/Account;Ljava/util/EnumSet;)V

    .line 474
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 524
    return-void
.end method

.method private unpinUnsubscribedMagazines(Landroid/accounts/Account;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 468
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 469
    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    .line 468
    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpinUnsubscribedHelper(Landroid/accounts/Account;Ljava/lang/String;Ljava/util/EnumSet;)V

    .line 470
    return-void
.end method

.method private unpinUnsubscribedNews(Landroid/accounts/Account;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 462
    .line 463
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyNews(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->NEWS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    .line 464
    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    .line 462
    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpinUnsubscribedHelper(Landroid/accounts/Account;Ljava/lang/String;Ljava/util/EnumSet;)V

    .line 465
    return-void
.end method

.method private unregister()V
    .locals 4

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->activeAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->activeAccount:Landroid/accounts/Account;

    .line 134
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyNews(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 133
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->newsObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->unregisterObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 136
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->activeAccount:Landroid/accounts/Account;

    .line 137
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyCurations(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 136
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->curationsObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->unregisterObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 139
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->activeAccount:Landroid/accounts/Account;

    .line 140
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 139
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->magazinesObserver:Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->registerObserver(Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/events/EventNotifier$EventObserver;)V

    .line 142
    :cond_0
    return-void
.end method


# virtual methods
.method public getAllPins()Landroid/util/SparseIntArray;
    .locals 13

    .prologue
    const/4 v5, 0x0

    .line 293
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    .line 294
    .local v1, "pinMap":Landroid/util/SparseIntArray;
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v3

    .line 295
    .local v3, "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    iget-object v7, v3, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v8, v7

    move v6, v5

    :goto_0
    if-ge v6, v8, :cond_1

    aget-object v2, v7, v6

    .line 296
    .local v2, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v9, v2, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v10, v9

    move v4, v5

    :goto_1
    if-ge v4, v10, :cond_0

    aget-object v0, v9, v4

    .line 297
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getPinId()I

    move-result v11

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getSnapshotId()I

    move-result v12

    invoke-virtual {v1, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 296
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 295
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_0
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 301
    .end local v2    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_1
    const/4 v4, -0x1

    invoke-virtual {v1, v4, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 302
    return-object v1
.end method

.method public getPinId(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 327
    instance-of v1, p2, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v1, :cond_0

    .line 328
    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .end local p2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinId(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;

    move-result-object v1

    .line 331
    :goto_0
    return-object v1

    .line 330
    .restart local p2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinnedItem(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-result-object v0

    .line 331
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    if-nez v0, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getPinId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method public getPinnedEditionList()Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 271
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v3

    .line 272
    .local v3, "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    iget-object v5, v3, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v5, v5

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 273
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    iget-object v8, v3, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v9, v8

    move v7, v6

    :goto_0
    if-ge v7, v9, :cond_2

    aget-object v2, v8, v7

    .line 274
    .local v2, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v10, v2, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v11, v10

    move v5, v6

    :goto_1
    if-ge v5, v11, :cond_1

    aget-object v1, v10, v5

    .line 275
    .local v1, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->hasEdition()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 276
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v12

    invoke-static {v12}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromProto(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 277
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 273
    .end local v1    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_1
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_0

    .line 281
    .end local v2    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_2
    return-object v4
.end method

.method public getPinnedVersion(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 336
    instance-of v1, p2, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v1, :cond_0

    .line 337
    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .end local p2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinnedVersion(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;

    move-result-object v1

    .line 340
    :goto_0
    return-object v1

    .line 339
    .restart local p2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinnedItem(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-result-object v0

    .line 340
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    if-nez v0, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getPinnedVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method public getSnapshotId(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 345
    instance-of v1, p2, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v1, :cond_0

    .line 346
    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .end local p2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getSnapshotId(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;

    move-result-object v1

    .line 349
    :goto_0
    return-object v1

    .line 348
    .restart local p2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinnedItem(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-result-object v0

    .line 349
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->hasSnapshotId()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getSnapshotId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method public globalUnpin(Z)V
    .locals 6
    .param p1, "ifNotDoneRecently"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v3, "lastGlobalUnpin"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 146
    .local v0, "lastGlobalUnpin":J
    if-eqz p1, :cond_0

    .line 147
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/32 v4, 0x5265c00

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 154
    :goto_0
    return-void

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->prunePinnedAccounts()V

    .line 152
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getAllPins()Landroid/util/SparseIntArray;

    move-result-object v3

    const v4, 0x7fffffff

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->unpinAll(Landroid/util/SparseIntArray;I)V

    .line 153
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v3, "lastGlobalUnpin"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setLong(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 319
    instance-of v0, p2, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v0, :cond_0

    .line 320
    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .end local p2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    .line 322
    :goto_0
    return v0

    .restart local p2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinnedItem(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadPinned(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v0

    return-object v0
.end method

.method public markFailed(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;F)V
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "lastSyncProgress"    # F

    .prologue
    .line 437
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->checkPinnable(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 438
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->lock:Ljava/lang/Object;

    monitor-enter v4

    .line 439
    :try_start_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v2

    .line 440
    .local v2, "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    invoke-direct {p0, v2, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v1

    .line 441
    .local v1, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v5, v1, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v6, v5

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v0, v5, v3

    .line 442
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    invoke-direct {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isForEdition(Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 443
    invoke-virtual {v0, p3}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->setLastSyncProgressAtFailure(F)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 447
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_0
    invoke-direct {p0, v2, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->storePinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V

    .line 448
    monitor-exit v4

    .line 449
    return-void

    .line 441
    .restart local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 448
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .end local v1    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .end local v2    # "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public markSynced(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "snapshotId"    # I

    .prologue
    .line 420
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->checkPinnable(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 421
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->lock:Ljava/lang/Object;

    monitor-enter v4

    .line 422
    :try_start_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v2

    .line 423
    .local v2, "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    invoke-direct {p0, v2, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v1

    .line 424
    .local v1, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v5, v1, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v6, v5

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v0, v5, v3

    .line 425
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    invoke-direct {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isForEdition(Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 426
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->setLastSynced(J)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 427
    invoke-virtual {v0, p3}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->setSnapshotId(I)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 428
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->clearLastSyncProgressAtFailure()Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 432
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_0
    invoke-direct {p0, v2, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->storePinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V

    .line 433
    monitor-exit v4

    .line 434
    return-void

    .line 424
    .restart local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 433
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .end local v1    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .end local v2    # "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public pin(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "pinnedVersion"    # I

    .prologue
    .line 354
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->checkPinnable(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 355
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->lock:Ljava/lang/Object;

    monitor-enter v4

    .line 356
    :try_start_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v2

    .line 357
    .local v2, "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    invoke-direct {p0, v2, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v1

    .line 358
    .local v1, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v5, v1, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    array-length v6, v5

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v6, :cond_1

    aget-object v0, v5, v3

    .line 360
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    invoke-direct {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isForEdition(Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 361
    monitor-exit v4

    .line 368
    :goto_1
    return-void

    .line 358
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 364
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :cond_1
    invoke-static {p2, p3, v2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->newItem(Lcom/google/apps/dots/android/newsstand/edition/Edition;ILcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-result-object v0

    .line 365
    .restart local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    iget-object v3, v1, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-static {v3, v0}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    iput-object v3, v1, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 366
    invoke-direct {p0, v2, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->storePinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V

    .line 367
    monitor-exit v4

    goto :goto_1

    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .end local v1    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .end local v2    # "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public pinnedList()Lcom/google/apps/dots/android/newsstand/sync/PinnedList;
    .locals 1

    .prologue
    .line 264
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner;)V

    return-object v0
.end method

.method protected prunePinnedAccounts()V
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 161
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v4

    .line 162
    .local v4, "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->accountNameManager:Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getOriginalNames()Ljava/util/Set;

    move-result-object v2

    .line 163
    .local v2, "originalNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 164
    .local v1, "kept":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsClient$Pinned;>;"
    iget-object v7, v4, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v8, v7

    move v5, v6

    :goto_0
    if-ge v5, v8, :cond_1

    aget-object v3, v7, v5

    .line 165
    .local v3, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->getAccount()Ljava/lang/String;

    move-result-object v0

    .line 166
    .local v0, "accountName":Ljava/lang/String;
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 167
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 169
    :cond_0
    sget-object v9, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v10, "Pinned account %s no longer valid. Removing."

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    aput-object v0, v11, v6

    invoke-virtual {v9, v10, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->li(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 172
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v3    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    iget-object v6, v4, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v6, v6

    if-eq v5, v6, :cond_2

    .line 173
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    invoke-interface {v1, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    iput-object v5, v4, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    .line 174
    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->storePinnedAccounts(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;)V

    .line 176
    :cond_2
    return-void
.end method

.method public setAccount(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 109
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 110
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->activeAccount:Landroid/accounts/Account;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 113
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unregister()V

    .line 114
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->activeAccount:Landroid/accounts/Account;

    .line 115
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->register()V

    goto :goto_0
.end method

.method public unpin(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 371
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->checkPinnable(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 372
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->lock:Ljava/lang/Object;

    monitor-enter v5

    .line 373
    :try_start_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v2

    .line 374
    .local v2, "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    invoke-direct {p0, v2, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v1

    .line 375
    .local v1, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v4, v1, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-static {v4}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    .line 376
    .local v3, "pinnedItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 377
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 378
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-direct {p0, v4, p2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isForEdition(Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 379
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 383
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    iput-object v4, v1, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 384
    invoke-direct {p0, v2, v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->storePinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V

    .line 385
    monitor-exit v5

    .line 386
    return-void

    .line 385
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    .end local v1    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .end local v2    # "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .end local v3    # "pinnedItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method
