.class public final Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SectionEditionInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;


# instance fields
.field private bitField0_:I

.field private edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

.field private sectionId_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2162
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2163
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2187
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    .line 2163
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;
    .locals 3

    .prologue
    .line 2220
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2224
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-eqz v2, :cond_0

    .line 2225
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 2227
    :cond_0
    return-object v0

    .line 2221
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;
    :catch_0
    move-exception v1

    .line 2222
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2159
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2232
    if-ne p1, p0, :cond_1

    .line 2236
    :cond_0
    :goto_0
    return v1

    .line 2233
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2234
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    .line 2235
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 2236
    goto :goto_0

    .line 2235
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    .line 2236
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1

    .prologue
    .line 2170
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    return-object v0
.end method

.method public getSectionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2189
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2259
    const/4 v0, 0x0

    .line 2260
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-eqz v1, :cond_0

    .line 2261
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 2262
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2264
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 2265
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    .line 2266
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2268
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->cachedSize:I

    .line 2269
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2240
    const/16 v0, 0x11

    .line 2241
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 2242
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 2243
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 2244
    return v0

    .line 2242
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->hashCode()I

    move-result v1

    goto :goto_0

    .line 2243
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2277
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2278
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2282
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2283
    :sswitch_0
    return-object p0

    .line 2288
    :sswitch_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v1, :cond_1

    .line 2289
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 2291
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2295
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    .line 2296
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->bitField0_:I

    goto :goto_0

    .line 2278
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2159
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;

    move-result-object v0

    return-object v0
.end method

.method public setEdition(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 2173
    if-nez p1, :cond_0

    .line 2174
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2176
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 2177
    return-object p0
.end method

.method public setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2192
    if-nez p1, :cond_0

    .line 2193
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2195
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    .line 2196
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->bitField0_:I

    .line 2197
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2249
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-eqz v0, :cond_0

    .line 2250
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2252
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 2253
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$SectionEditionInfo;->sectionId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2255
    :cond_1
    return-void
.end method
