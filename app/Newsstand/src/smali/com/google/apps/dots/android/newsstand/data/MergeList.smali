.class public Lcom/google/apps/dots/android/newsstand/data/MergeList;
.super Lcom/google/android/libraries/bind/data/DataList;
.source "MergeList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter2;,
        Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;
    }
.end annotation


# instance fields
.field private final equalityFields:[I

.field private final mergeFilter:Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;

.field private final queue:Lcom/google/android/libraries/bind/async/Queue;

.field private final refreshObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private final sources:[Lcom/google/android/libraries/bind/data/DataList;


# direct methods
.method public varargs constructor <init>(I[ILcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;Lcom/google/android/libraries/bind/async/Queue;[Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "primaryKey"    # I
    .param p2, "equalityFields"    # [I
    .param p3, "mergeFilter"    # Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;
    .param p4, "queue"    # Lcom/google/android/libraries/bind/async/Queue;
    .param p5, "sources"    # [Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/DataList;-><init>(I)V

    .line 59
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->sources:[Lcom/google/android/libraries/bind/data/DataList;

    .line 60
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->mergeFilter:Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;

    .line 61
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->queue:Lcom/google/android/libraries/bind/async/Queue;

    .line 62
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->equalityFields:[I

    .line 63
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/MergeList$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/data/MergeList$1;-><init>(Lcom/google/apps/dots/android/newsstand/data/MergeList;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->refreshObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 70
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/MergeList;->setDirty(Z)V

    .line 71
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/MergeList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/data/MergeList;)Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/MergeList;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->mergeFilter:Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;

    return-object v0
.end method


# virtual methods
.method protected equalityFields()[I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->equalityFields:[I

    return-object v0
.end method

.method protected makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
    .locals 4

    .prologue
    .line 98
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->sources:[Lcom/google/android/libraries/bind/data/DataList;

    array-length v2, v2

    new-array v1, v2, [Lcom/google/android/libraries/bind/data/Snapshot;

    .line 99
    .local v1, "snapshots":[Lcom/google/android/libraries/bind/data/Snapshot;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 100
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->sources:[Lcom/google/android/libraries/bind/data/DataList;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v2

    aput-object v2, v1, v0

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103
    :cond_0
    new-instance v2, Lcom/google/apps/dots/android/newsstand/data/MergeList$2;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->queue:Lcom/google/android/libraries/bind/async/Queue;

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/google/apps/dots/android/newsstand/data/MergeList$2;-><init>(Lcom/google/apps/dots/android/newsstand/data/MergeList;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;[Lcom/google/android/libraries/bind/data/Snapshot;)V

    return-object v2
.end method

.method protected onRegisterForInvalidation()V
    .locals 5

    .prologue
    .line 81
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataList;->onRegisterForInvalidation()V

    .line 82
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->sources:[Lcom/google/android/libraries/bind/data/DataList;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 83
    .local v0, "list":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->refreshObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    .end local v0    # "list":Lcom/google/android/libraries/bind/data/DataList;
    :cond_0
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 5

    .prologue
    .line 89
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataList;->onUnregisterForInvalidation()V

    .line 90
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->sources:[Lcom/google/android/libraries/bind/data/DataList;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 91
    .local v0, "list":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList;->refreshObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 90
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 93
    .end local v0    # "list":Lcom/google/android/libraries/bind/data/DataList;
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/data/MergeList;->setDirty(Z)V

    .line 94
    return-void
.end method
