.class public Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;
.source "NewsArticlePagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final EQUALITY_FIELDS:[I

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private background:Landroid/view/View;

.field private hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

.field private pager:Landroid/support/v4/view/NSViewPager;

.field private pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

.field private pagerDropShadow:Landroid/view/View;

.field private postReadingList:Lcom/google/android/libraries/bind/data/DataList;

.field private toolbar:Landroid/support/v7/widget/Toolbar;

.field private transitionPlaceholderView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-class v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 80
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 98
    const/4 v0, 0x0

    const-string v1, "NewsArticlePagerFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->news_article_pager_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 99
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method static synthetic access$300()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/support/v4/view/NSViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->transitionPlaceholderView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->background:Landroid/view/View;

    return-object v0
.end method

.method private announceCurrentPostForAccessibility()V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0x12c

    .line 506
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    if-lt v5, v6, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isA11yEnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 509
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->getList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    .line 510
    .local v3, "currentReadingList":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v5}, Landroid/support/v4/view/NSViewPager;->getCurrentLogicalItem()I

    move-result v4

    .line 511
    .local v4, "logicalPosition":I
    if-ltz v4, :cond_0

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 512
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    .line 513
    .local v2, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 514
    .local v0, "articleData":Lcom/google/android/libraries/bind/data/Data;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$11;->$SwitchMap$com$google$apps$dots$android$newsstand$reading$ReadingFragment$ViewType:[I

    sget v5, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_VIEW_TYPE:I

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    .line 571
    .end local v0    # "articleData":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .end local v3    # "currentReadingList":Lcom/google/android/libraries/bind/data/DataList;
    .end local v4    # "logicalPosition":I
    :cond_0
    :goto_0
    return-void

    .line 520
    .restart local v0    # "articleData":Lcom/google/android/libraries/bind/data/Data;
    .restart local v2    # "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .restart local v3    # "currentReadingList":Lcom/google/android/libraries/bind/data/DataList;
    .restart local v4    # "logicalPosition":I
    :pswitch_0
    invoke-direct {p0, v2, v7}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getDelayedFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance v6, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$8;

    invoke-direct {v6, p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$8;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Lcom/google/android/libraries/bind/data/Data;)V

    .line 517
    invoke-virtual {v2, v5, v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 530
    :pswitch_1
    sget v5, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 535
    invoke-direct {p0, v2, v7}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getDelayedFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance v6, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$9;

    invoke-direct {v6, p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$9;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Lcom/google/android/libraries/bind/data/Data;)V

    .line 532
    invoke-virtual {v2, v5, v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 547
    :cond_1
    sget v5, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ID:I

    .line 548
    invoke-virtual {v0, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v5

    .line 547
    invoke-static {v8, v5}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->getArticleLoader(ZLjava/lang/String;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v1

    .line 549
    .local v1, "articleLoader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/common/util/concurrent/ListenableFuture;

    .line 551
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x1

    .line 554
    invoke-direct {p0, v2, v7}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getDelayedFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    aput-object v7, v5, v6

    .line 550
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance v6, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$10;

    invoke-direct {v6, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$10;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)V

    .line 549
    invoke-virtual {v2, v5, v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 514
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getDelayedFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "delay"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 464
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 465
    .local v0, "future":Lcom/google/common/util/concurrent/SettableFuture;, "Lcom/google/common/util/concurrent/SettableFuture<Ljava/lang/Object;>;"
    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$7;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Lcom/google/common/util/concurrent/SettableFuture;)V

    int-to-long v2, p2

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 472
    return-object v0
.end method

.method private onBackToEdition()Z
    .locals 5

    .prologue
    .line 391
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v3, :cond_0

    .line 392
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/NSViewPager;->getCurrentLogicalItem()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 393
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_0

    sget v3, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ID:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 394
    sget v3, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ID:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v2

    .line 397
    .local v2, "postId":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 398
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "EditionPagerFragment_upcoming_post"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 399
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 402
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "postId":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    return v3
.end method

.method private setupActionBarContents()V
    .locals 6

    .prologue
    .line 366
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    .line 367
    .local v1, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v4, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$6;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)V

    invoke-virtual {v1, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 379
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v3

    .line 380
    .local v3, "librarySnapshot":Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v2

    .line 381
    .local v2, "isSubscribed":Z
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->actionBarColor(Landroid/content/Context;Z)I

    move-result v0

    .line 382
    .local v0, "actionBarColor":I
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->setActionBarBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 383
    return-void
.end method

.method private setupAdapter()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 204
    new-instance v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$2;

    .line 205
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    sget-object v6, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->NONE:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v4, p0, v5, v6}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Landroid/support/v4/app/FragmentManager;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 258
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->isLocaleRtl()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->setRtl(Z)V

    .line 262
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    new-instance v5, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$3;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)V

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->setErrorMessageDataProvider(Lcom/google/android/libraries/bind/data/DataProvider;)Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 276
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v4

    if-nez v4, :cond_0

    .line 279
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->DEFAULT_PRIMARY_KEY:I

    .line 280
    .local v2, "primaryKey":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    invoke-interface {v4, v2}, Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;->toDataObject(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 282
    .local v1, "initialPostData":Lcom/google/android/libraries/bind/data/Data;
    new-instance v3, Lcom/google/android/libraries/bind/data/DataList;

    new-array v4, v8, [Lcom/google/android/libraries/bind/data/Data;

    aput-object v1, v4, v7

    .line 283
    invoke-static {v4}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Lcom/google/android/libraries/bind/data/DataList;-><init>(ILjava/util/List;)V

    .line 284
    .local v3, "singlePostList":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v4, v3}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->setList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 290
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 291
    .local v0, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    .line 293
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/DataListUtil;->whenDataListFirstRefreshed(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    aput-object v5, v4, v7

    const/16 v5, 0x4b0

    .line 294
    invoke-direct {p0, v0, v5}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getDelayedFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    aput-object v5, v4, v8

    .line 292
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$4;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)V

    .line 291
    invoke-virtual {v0, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 305
    .end local v0    # "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .end local v1    # "initialPostData":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "primaryKey":I
    .end local v3    # "singlePostList":Lcom/google/android/libraries/bind/data/DataList;
    :goto_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->announceCurrentPostForAccessibility()V

    .line 306
    return-void

    .line 302
    :cond_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->setList(Lcom/google/android/libraries/bind/data/DataList;)V

    goto :goto_0
.end method

.method private setupHidingActionBar(Landroid/view/View;)V
    .locals 2
    .param p1, "toolbarContainer"    # Landroid/view/View;

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/ActionBarActivity;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 116
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->toolbar:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    new-instance v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getNavigationDrawerActivity()Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->attach(Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;)V

    .line 131
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->setToolbarContainer(Landroid/view/View;)V

    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->MODE_HIDE_ON_SCROLL:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->setMode(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;)V

    .line 133
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hide(Z)V

    .line 137
    :cond_0
    return-void
.end method

.method private setupPager()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 310
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Landroid/support/v4/view/NSViewPager;)V

    .line 356
    .local v0, "pageChangeListener":Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerDropShadow:Landroid/view/View;

    invoke-direct {v2, v3, v4, v0}, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;-><init>(Landroid/support/v4/view/NSViewPager;Landroid/view/View;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/NSViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 359
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    if-eqz v1, :cond_0

    .line 360
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/NSViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 362
    :cond_0
    return-void
.end method

.method private setupPostReadingList()V
    .locals 7

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->readingList(Landroid/content/Context;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 182
    .local v1, "editionReadingList":Lcom/google/android/libraries/bind/data/DataList;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    instance-of v3, v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    if-eqz v3, :cond_1

    .line 183
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v2, v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    check-cast v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    .line 185
    .local v2, "newsState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
    iget-object v3, v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    if-nez v3, :cond_0

    const/4 v0, 0x0

    .line 195
    .local v0, "addMissingPostFilter":Lcom/google/android/libraries/bind/data/Filter;
    :goto_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->EQUALITY_FIELDS:[I

    .line 196
    invoke-virtual {v1, v3, v0}, Lcom/google/android/libraries/bind/data/DataList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->autoRefreshOnce()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    .line 200
    .end local v0    # "addMissingPostFilter":Lcom/google/android/libraries/bind/data/Filter;
    .end local v2    # "newsState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
    :goto_1
    return-void

    .line 185
    .restart local v2    # "newsState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 189
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->primaryKey()I

    move-result v5

    iget-object v6, v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    .line 191
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v0, v4, v5, v6, v3}, Lcom/google/apps/dots/android/newsstand/reading/AddMissingPostFilter;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;ILjava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    goto :goto_0

    .line 198
    .end local v2    # "newsState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
    :cond_1
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    goto :goto_1
.end method


# virtual methods
.method protected createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 2

    .prologue
    .line 575
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;)V

    return-object v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 477
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getPrimaryVisibleFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    move-result-object v0

    .line 478
    .local v0, "primaryFragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 479
    :goto_0
    return-object v1

    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v1

    goto :goto_0
.end method

.method protected getIAPConversionEventProvider()Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
    .locals 4

    .prologue
    .line 494
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getPrimaryVisibleFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    move-result-object v0

    .line 495
    .local v0, "visibleFragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    if-eqz v1, :cond_0

    .line 496
    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .end local v0    # "visibleFragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->getIAPConversionEventProvider()Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;

    move-result-object v1

    .line 499
    :goto_0
    return-object v1

    .line 498
    .restart local v0    # "visibleFragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Can not start In-App purchases from outside a Reading Context."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 499
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPrimaryVisibleFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    .locals 4

    .prologue
    .line 483
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v1

    .line 484
    .local v1, "childFragments":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/app/Fragment;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 485
    .local v0, "childFragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 486
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 487
    check-cast v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .line 490
    .end local v0    # "childFragment":Landroid/support/v4/app/Fragment;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleOnBackPressed()Z
    .locals 1

    .prologue
    .line 387
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->handleOnBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->onBackToEdition()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 141
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 142
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->setList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 143
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->destroy()V

    .line 144
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->detach()V

    .line 145
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onDestroyView()V

    .line 146
    return-void
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->background:Landroid/view/View;

    .line 104
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->toolbar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 105
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->news_article_pager:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/NSViewPager;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    .line 106
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->pager_drop_shadow:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerDropShadow:Landroid/view/View;

    .line 107
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->reading_expando_hero:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->transitionPlaceholderView:Landroid/view/View;

    .line 109
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->toolbar_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->setupHidingActionBar(Landroid/view/View;)V

    .line 110
    return-void
.end method

.method protected restoreIntents(Landroid/app/Activity;Landroid/os/Parcelable;)Ljava/util/List;
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Parcelable;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407
    instance-of v5, p2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    if-eqz v5, :cond_2

    move-object v1, p2

    .line 408
    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    .line 409
    .local v1, "newsArticlePagerState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 412
    .local v0, "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    sget-object v5, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$11;->$SwitchMap$com$google$apps$dots$android$newsstand$util$ProtoEnum$EditionType:[I

    iget-object v6, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 428
    new-instance v5, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v5, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 429
    new-instance v5, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-direct {v5, p1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v6, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v5

    .line 430
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v5

    .line 429
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 435
    :goto_0
    iget-object v5, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    instance-of v5, v5, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    if-eqz v5, :cond_1

    .line 436
    iget-object v2, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    check-cast v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    .line 438
    .local v2, "newsArticleState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    invoke-direct {v5, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v6, v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    .line 440
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 441
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setOriginalEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 442
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->optPostIndex:Ljava/lang/Integer;

    .line 443
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostIndex(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v5

    .line 444
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v5

    .line 438
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 460
    .end local v0    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v1    # "newsArticlePagerState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    .end local v2    # "newsArticleState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
    :cond_0
    :goto_1
    return-object v0

    .line 414
    .restart local v0    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .restart local v1    # "newsArticlePagerState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    :pswitch_0
    new-instance v5, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v5, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v6, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 417
    :pswitch_1
    new-instance v5, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v5, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v6, Lcom/google/apps/dots/android/newsstand/home/HomePage;->SAVED_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 420
    :pswitch_2
    iget-object v3, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    .line 422
    .local v3, "searchPostsEdition":Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v5, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424
    new-instance v5, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;

    invoke-direct {v5, p1}, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 425
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;->getQuery()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;->setQuery(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v5

    .line 424
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 445
    .end local v3    # "searchPostsEdition":Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;
    :cond_1
    iget-object v5, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    instance-of v5, v5, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    if-eqz v5, :cond_0

    .line 446
    iget-object v4, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->articleState:Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;

    check-cast v4, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    .line 449
    .local v4, "webArticleState":Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    invoke-direct {v5, p1}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v6, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 451
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v5

    iget-object v6, v4, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    .line 452
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setPostUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v5

    iget-object v6, v4, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    .line 453
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v5

    iget-object v6, v4, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->publisher:Ljava/lang/String;

    .line 454
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setPublisher(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v5

    .line 455
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v5

    .line 449
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 460
    .end local v0    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v1    # "newsArticlePagerState":Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    .end local v4    # "webArticleState":Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 412
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 76
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;)V
    .locals 3
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    if-eqz p2, :cond_2

    .line 167
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 168
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->getList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    if-nez v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 169
    .local v0, "pagerSetupRequired":Z
    :goto_1
    if-eqz v0, :cond_0

    .line 170
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->setupPostReadingList()V

    .line 171
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->setupAdapter()V

    .line 172
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->setupPager()V

    .line 173
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->setupActionBarContents()V

    goto :goto_0

    .line 168
    .end local v0    # "pagerSetupRequired":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
