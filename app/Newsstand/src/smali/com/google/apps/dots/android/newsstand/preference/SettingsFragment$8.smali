.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupAboutAppPreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field lastToast:Landroid/widget/Toast;

.field taps:I

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .prologue
    .line 306
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->taps:I

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 8
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 312
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 313
    .local v0, "context":Landroid/content/Context;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->taps:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->taps:I

    .line 314
    const/4 v1, 0x5

    .line 315
    .local v1, "requiredTaps":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->areDeveloperPreferencesEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 316
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->taps:I

    if-ge v2, v3, :cond_1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->taps:I

    if-ge v2, v1, :cond_1

    .line 317
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .line 318
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->almost_developer_mode_toast:I

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->taps:I

    sub-int v5, v1, v5

    .line 319
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    .line 318
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 317
    invoke-static {v0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->showToast(Landroid/widget/Toast;)V

    .line 328
    :cond_0
    :goto_0
    return v7

    .line 321
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->taps:I

    if-ne v2, v1, :cond_0

    .line 322
    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->developer_mode_toast:I

    .line 323
    invoke-static {v0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 322
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->showToast(Landroid/widget/Toast;)V

    .line 324
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    const-string v3, "developerMode"

    invoke-virtual {v2, v3, v7}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 325
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupDeveloperPreferences()V

    goto :goto_0
.end method

.method showToast(Landroid/widget/Toast;)V
    .locals 1
    .param p1, "toast"    # Landroid/widget/Toast;

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->lastToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->lastToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 335
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;->lastToast:Landroid/widget/Toast;

    .line 336
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 337
    return-void
.end method
