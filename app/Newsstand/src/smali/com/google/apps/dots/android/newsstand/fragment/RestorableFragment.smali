.class public abstract Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "RestorableFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S::",
        "Landroid/os/Parcelable;",
        ">",
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<TS;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Parcelable;Ljava/lang/String;I)V
    .locals 0
    .param p2, "stateExtraKey"    # Ljava/lang/String;
    .param p3, "fragmentLayoutResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment<TS;>;"
    .local p1, "defaultState":Landroid/os/Parcelable;, "TS;"
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 25
    return-void
.end method

.method private isTopRestorableFragment()Z
    .locals 2

    .prologue
    .line 40
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment<TS;>;"
    move-object v0, p0

    .line 41
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 42
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 43
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;

    if-eqz v1, :cond_0

    .line 44
    const/4 v1, 0x0

    .line 47
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static restoreStateIfPossible(Landroid/app/Activity;)Z
    .locals 14
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v12, 0x0

    .line 84
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getRestorableState()Ljava/lang/String;

    move-result-object v8

    .line 85
    .local v8, "parcelString":Ljava/lang/String;
    if-nez v8, :cond_0

    move v11, v12

    .line 115
    :goto_0
    return v11

    .line 89
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v11

    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setRestorableState(Ljava/lang/String;)V

    .line 90
    invoke-static {v8, v12}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v7

    .line 91
    .local v7, "parcelData":[B
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v6

    .line 93
    .local v6, "parcel":Landroid/os/Parcel;
    const/4 v11, 0x0

    :try_start_0
    array-length v13, v7

    invoke-virtual {v6, v7, v11, v13}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 94
    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 95
    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 96
    .local v9, "sdkVersion":I
    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v9, v11, :cond_1

    .line 115
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    move v11, v12

    goto :goto_0

    .line 99
    :cond_1
    :try_start_1
    invoke-virtual {v6}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "className":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 101
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Class;

    invoke-virtual {v1, v11}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 102
    .local v2, "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v2, v11}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;

    .line 103
    .local v4, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment<*>;"
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v11

    invoke-virtual {v6, v11}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v10

    .line 104
    .local v10, "stateParcel":Landroid/os/Parcelable;
    invoke-virtual {v4, p0, v10}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->restoreIntents(Landroid/app/Activity;Landroid/os/Parcelable;)Ljava/util/List;

    move-result-object v5

    .line 105
    .local v5, "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    if-eqz v5, :cond_2

    .line 106
    const/4 v11, 0x0

    invoke-interface {v5, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Intent;

    const/high16 v13, 0x10000000

    invoke-virtual {v11, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 107
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v11

    new-array v11, v11, [Landroid/content/Intent;

    invoke-interface {v5, v11}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Landroid/content/Intent;

    invoke-virtual {p0, v11}, Landroid/app/Activity;->startActivities([Landroid/content/Intent;)V

    .line 108
    const/4 v11, 0x0

    const/4 v13, 0x0

    invoke-virtual {p0, v11, v13}, Landroid/app/Activity;->overridePendingTransition(II)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    const/4 v11, 0x1

    .line 115
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    :cond_2
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    move v11, v12

    goto :goto_0

    .line 112
    .end local v0    # "className":Ljava/lang/String;
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    .end local v4    # "fragment":Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment<*>;"
    .end local v5    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v9    # "sdkVersion":I
    .end local v10    # "stateParcel":Landroid/os/Parcelable;
    :catch_0
    move-exception v3

    .line 115
    .local v3, "e":Ljava/lang/Throwable;
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    move v11, v12

    goto :goto_0

    .end local v3    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v11

    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    throw v11
.end method

.method private saveRestorableState()V
    .locals 6

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment<TS;>;"
    const/4 v5, 0x0

    .line 68
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 71
    .local v1, "parcel":Landroid/os/Parcel;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 72
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->getLowestRestorableFragment()Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;

    move-result-object v0

    .line 73
    .local v0, "lowestFragment":Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment<+Landroid/os/Parcelable;>;"
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    invoke-virtual {v1, v4, v5}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 75
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v2

    .line 76
    .local v2, "parcelData":[B
    invoke-static {v2, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    .line 77
    .local v3, "parcelString":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setRestorableState(Ljava/lang/String;)V

    .line 78
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 79
    return-void
.end method


# virtual methods
.method protected final getLowestRestorableFragment()Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment",
            "<+",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment<TS;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v1

    .line 52
    .local v1, "childFragments":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/app/Fragment;>;"
    if-nez v1, :cond_1

    .line 64
    .end local p0    # "this":Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment<TS;>;"
    :cond_0
    :goto_0
    return-object p0

    .line 55
    .restart local p0    # "this":Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment<TS;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 56
    .local v0, "childFragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_2

    .line 59
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v3

    if-eqz v3, :cond_2

    instance-of v3, v0, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;

    if-eqz v3, :cond_2

    .line 61
    check-cast v0, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;

    .end local v0    # "childFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->getLowestRestorableFragment()Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;

    move-result-object p0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 29
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment<TS;>;"
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onPause()V

    .line 30
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->saveRestorableStateIfAppropriate()V

    .line 31
    return-void
.end method

.method protected abstract restoreIntents(Landroid/app/Activity;Landroid/os/Parcelable;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Parcelable;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end method

.method protected saveRestorableStateIfAppropriate()V
    .locals 1

    .prologue
    .line 34
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment<TS;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->isTopRestorableFragment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->saveRestorableState()V

    .line 37
    :cond_0
    return-void
.end method
