.class Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$1;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "OnboardQuizCurationsPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

.field final synthetic val$freeMagazineOffers:Lcom/google/android/libraries/bind/data/DataList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$1;->val$freeMagazineOffers:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1, "optionalResult"    # Ljava/lang/Object;

    .prologue
    .line 72
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$1;->val$freeMagazineOffers:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    # setter for: Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->showMagazineOffersNext:Ljava/lang/Boolean;
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->access$002(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->access$100(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;)Lcom/google/android/play/onboard/OnboardHostControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/play/onboard/OnboardHostControl;->invalidateControls()V

    .line 74
    return-void

    .line 72
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
