.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Number"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;


# instance fields
.field private bitField0_:I

.field private value_:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12492
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 12493
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 12498
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->value_:J

    .line 12493
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;
    .locals 3

    .prologue
    .line 12527
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 12531
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;
    return-object v0

    .line 12528
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;
    :catch_0
    move-exception v1

    .line 12529
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 12489
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 12536
    if-ne p1, p0, :cond_1

    .line 12539
    :cond_0
    :goto_0
    return v1

    .line 12537
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 12538
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    .line 12539
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->value_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->value_:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 12558
    const/4 v0, 0x0

    .line 12559
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 12560
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->value_:J

    .line 12561
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 12563
    :cond_0
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->cachedSize:I

    .line 12564
    return v0
.end method

.method public getValue()J
    .locals 2

    .prologue
    .line 12500
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->value_:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 12543
    const/16 v0, 0x11

    .line 12544
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 12545
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->value_:J

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->value_:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 12546
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12572
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 12573
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 12577
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 12578
    :sswitch_0
    return-object p0

    .line 12583
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->value_:J

    .line 12584
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->bitField0_:I

    goto :goto_0

    .line 12573
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12489
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12551
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 12552
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->value_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 12554
    :cond_0
    return-void
.end method
