.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;
.super Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;
.source "ScrollPartView.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;


# static fields
.field private static final DEFAULT_MAX_SCALE:F = 4.0f

.field private static final DEFAULT_MIN_SCALE:F = 1.0f

.field private static final GESTURE_EXPIRATION_TIMEOUT:J = 0x1f4L

.field private static final GUTTER_WIDTH_FRACTION:F = 0.15f

.field private static final SCALE_MAX_EXTRA:F = 1.4f

.field private static final SCROLL_ADJUST_THRESHOLD:F = 0.1f

.field private static final SCROLL_WIGGLE_THRESHOLD:F = 2.0f


# instance fields
.field private final bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private final childrenBounds:Landroid/graphics/RectF;

.field private final clearGestureStateRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

.field private didTriggerDoubleTapZoomInGesture:Z

.field private didTriggerPageFlipInGesture:Z

.field private enableGutterTap:Z

.field private final eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

.field private final eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

.field private gestureDetector:Landroid/view/GestureDetector;

.field private hasScrolledInternally:Z

.field private initialOffsetX:F

.field private initialOffsetY:F

.field private isInGesture:Z

.field isLaidOut:Z

.field private final isOnlyChildOfRoot:Z

.field private isScrollingAgainstLeftOrRight:Z

.field private isScrollingAgainstTopOrBottom:Z

.field private lastSnappedPage:I

.field private final leftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private maxScale:F

.field private minScale:F

.field private final originalRect:Landroid/graphics/RectF;

.field private pageFraction:F

.field private final partId:Ljava/lang/String;

.field private final rightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private scaleDetector:Landroid/view/ScaleGestureDetector;

.field private final scaledRect:Landroid/graphics/RectF;

.field private final scrollExtent:Landroid/graphics/RectF;

.field private scroller:Landroid/widget/Scroller;

.field private final simulatedFlingVelocity:F

.field private final snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

.field private final tempMatrix:Landroid/graphics/Matrix;

.field private final tempPoint:[F

.field private final topEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private final touchSlopInterceptor:Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

.field private final transformConstraintCorrection:[F

.field private velocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;
    .param p3, "eventScope"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;
    .param p4, "scrollPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .param p5, "isOnlyChildOfRoot"    # Z

    .prologue
    const/4 v1, 0x2

    .line 140
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V

    .line 73
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->minScale:F

    .line 74
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->maxScale:F

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->lastSnappedPage:I

    .line 112
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->pageFraction:F

    .line 123
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scrollExtent:Landroid/graphics/RectF;

    .line 124
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->childrenBounds:Landroid/graphics/RectF;

    .line 128
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->transformConstraintCorrection:[F

    .line 130
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    .line 133
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    .line 134
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    .line 135
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->originalRect:Landroid/graphics/RectF;

    .line 136
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    .line 141
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$dimen;->simulated_fling_velocity:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->simulatedFlingVelocity:F

    .line 142
    invoke-interface {p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    .line 143
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    .line 144
    invoke-virtual {p4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->partId:Ljava/lang/String;

    .line 145
    iput-boolean p5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isOnlyChildOfRoot:Z

    .line 146
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->touchSlopInterceptor:Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    .line 147
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->initGestureDetectors()V

    .line 149
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 161
    new-instance v0, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->clearGestureStateRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 168
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->topEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 169
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 170
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->leftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 171
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->rightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 172
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->updateChildrenBounds()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->clearGestureState()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForRightBoundary()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isScrollingAgainstTopOrBottom:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isScrollingAgainstTopOrBottom:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForTopBoundary()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForBottomBoundary()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1402(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->hasScrolledInternally:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)[F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->transformConstraintCorrection:[F

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Landroid/support/v4/widget/EdgeEffectCompat;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->leftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Landroid/support/v4/widget/EdgeEffectCompat;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->rightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Landroid/support/v4/widget/EdgeEffectCompat;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->topEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Landroid/support/v4/widget/EdgeEffectCompat;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateScrollToSnapControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->enableGutterTap:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didTriggerPageFlipInGesture:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didTriggerPageFlipInGesture:Z

    return p1
.end method

.method static synthetic access$2202(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didTriggerDoubleTapZoomInGesture:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;FF)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;
    .param p1, "x1"    # F
    .param p2, "x2"    # F

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->handleDoubleTapAt(FF)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isInGesture:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->minScale:F

    return v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getOverZoomScale()F

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->maxScale:F

    return v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Landroid/view/ScaleGestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaleDetector:Landroid/view/ScaleGestureDetector;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isScrollingAgainstLeftOrRight:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isScrollingAgainstLeftOrRight:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForLeftBoundary()Z

    move-result v0

    return v0
.end method

.method private animateScrollToSnapControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)V
    .locals 4
    .param p1, "control"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .prologue
    .line 372
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v0

    .line 373
    .local v0, "contentArea":Landroid/graphics/RectF;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->registerSnappedControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)V

    .line 374
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->getUnzoomedPoint()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getX()I

    move-result v1

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    .line 375
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->getUnzoomedPoint()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getY()I

    move-result v2

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    .line 374
    invoke-virtual {p0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateScrollToPoint(FFF)V

    .line 376
    return-void
.end method

.method private canScrollHorizontally()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 557
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->canScrollHorizontally(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->canScrollHorizontally(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canScrollVertically()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 574
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->canScrollVertically(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->canScrollVertically(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private clearGestureState()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 414
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isInGesture:Z

    .line 415
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didTriggerPageFlipInGesture:Z

    .line 416
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didTriggerDoubleTapZoomInGesture:Z

    .line 417
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isScrollingAgainstLeftOrRight:Z

    .line 418
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isScrollingAgainstTopOrBottom:Z

    .line 419
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->hasScrolledInternally:Z

    .line 420
    return-void
.end method

.method private computeConstrainedOffset(Landroid/graphics/Matrix;FF[F)V
    .locals 7
    .param p1, "transform"    # Landroid/graphics/Matrix;
    .param p2, "targetX"    # F
    .param p3, "targetY"    # F
    .param p4, "output"    # [F

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 648
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isLaidOut:Z

    if-eqz v1, :cond_0

    .line 649
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v0

    .line 652
    .local v0, "contentArea":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->originalRect:Landroid/graphics/RectF;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getScrollBounds()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 653
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->originalRect:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v3, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->offset(FF)V

    .line 654
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->originalRect:Landroid/graphics/RectF;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 657
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    .line 656
    invoke-static {p2, v4, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->translateInterval(FFFFF)F

    move-result v1

    aput v1, p4, v5

    .line 659
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    .line 658
    invoke-static {p3, v4, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->translateInterval(FFFFF)F

    move-result v1

    aput v1, p4, v6

    .line 665
    .end local v0    # "contentArea":Landroid/graphics/RectF;
    :goto_0
    return-void

    .line 662
    :cond_0
    aput p2, p4, v5

    .line 663
    aput p3, p4, v6

    goto :goto_0
.end method

.method private computeHorizontalOffset(I)F
    .locals 4
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x0

    .line 672
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isLaidOut:Z

    if-eqz v0, :cond_1

    .line 673
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransform(Landroid/graphics/Matrix;)V

    .line 677
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    if-ltz p1, :cond_0

    const v0, -0x800001

    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    invoke-direct {p0, v2, v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeConstrainedOffset(Landroid/graphics/Matrix;FF[F)V

    .line 679
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 681
    :goto_1
    return v0

    .line 677
    :cond_0
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_0

    :cond_1
    move v0, v1

    .line 681
    goto :goto_1
.end method

.method private computeVerticalOffset(I)F
    .locals 4
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x0

    .line 692
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isLaidOut:Z

    if-eqz v0, :cond_1

    .line 693
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransform(Landroid/graphics/Matrix;)V

    .line 694
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    if-ltz p1, :cond_0

    const v0, -0x800001

    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    invoke-direct {p0, v2, v1, v0, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeConstrainedOffset(Landroid/graphics/Matrix;FF[F)V

    .line 696
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    .line 698
    :goto_1
    return v0

    .line 694
    :cond_0
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_0

    :cond_1
    move v0, v1

    .line 698
    goto :goto_1
.end method

.method private didAdjustScrollForBottomBoundary()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 735
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->transformConstraintCorrection:[F

    aget v1, v1, v0

    const v2, 0x3dcccccd    # 0.1f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private didAdjustScrollForLeftBoundary()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 723
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->transformConstraintCorrection:[F

    aget v1, v1, v0

    const v2, -0x42333333    # -0.1f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private didAdjustScrollForRightBoundary()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 727
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->transformConstraintCorrection:[F

    aget v1, v1, v0

    const v2, 0x3dcccccd    # 0.1f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private didAdjustScrollForTopBoundary()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 731
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->transformConstraintCorrection:[F

    aget v1, v1, v0

    const v2, -0x42333333    # -0.1f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private drawEdge(Landroid/graphics/Canvas;Landroid/support/v4/widget/EdgeEffectCompat;IIIII)Z
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "edge"    # Landroid/support/v4/widget/EdgeEffectCompat;
    .param p3, "edgeWidth"    # I
    .param p4, "edgeHeight"    # I
    .param p5, "xOffset"    # I
    .param p6, "yOffset"    # I
    .param p7, "rotation"    # I

    .prologue
    .line 1021
    const/4 v0, 0x0

    .line 1022
    .local v0, "invalidate":Z
    invoke-virtual {p2}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1023
    invoke-virtual {p2, p3, p4}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 1025
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1026
    .local v1, "restoreCount":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getScrollX()I

    move-result v2

    add-int/2addr v2, p5

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getScrollY()I

    move-result v3

    add-int/2addr v3, p6

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1027
    int-to-float v2, p7

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1028
    invoke-virtual {p2, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1029
    const/4 v0, 0x1

    .line 1031
    :cond_0
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1033
    .end local v1    # "restoreCount":I
    :cond_1
    return v0
.end method

.method private getHorizontalScrollMax()I
    .locals 2

    .prologue
    .line 987
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeHorizontalOffset(I)F

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeHorizontalOffset(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private getOverZoomScale()F
    .locals 2

    .prologue
    .line 740
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->maxScale:F

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->minScale:F

    sub-float/2addr v0, v1

    const v1, 0x3dcccccd    # 0.1f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->maxScale:F

    const v1, 0x3fb33333    # 1.4f

    mul-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->maxScale:F

    goto :goto_0
.end method

.method private getScrollBounds()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scrollExtent:Landroid/graphics/RectF;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->isEmpty(Landroid/graphics/RectF;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scrollExtent:Landroid/graphics/RectF;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->childrenBounds:Landroid/graphics/RectF;

    goto :goto_0
.end method

.method private getVerticalScrollMax()I
    .locals 2

    .prologue
    .line 991
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeVerticalOffset(I)F

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeVerticalOffset(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private handleDoubleTapAt(FF)V
    .locals 10
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 390
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransformScale()F

    move-result v2

    const/high16 v4, 0x3fc00000    # 1.5f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_1

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_0
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->minScale:F

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->maxScale:F

    invoke-static {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/util/MathUtil;->clamp(FFF)F

    move-result v1

    .line 394
    .local v1, "scale":F
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    aput p1, v2, v8

    .line 395
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    aput p2, v2, v9

    .line 396
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getViewPointInLayoutCoordinates([F)V

    .line 397
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v0

    .line 398
    .local v0, "contentArea":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v4, v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v5, v1

    invoke-virtual {v2, v7, v7, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 399
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    aget v4, v4, v8

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v5, v3

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    aget v5, v5, v9

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    .line 400
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    div-float v3, v6, v3

    sub-float v3, v5, v3

    .line 399
    invoke-virtual {v2, v4, v3}, Landroid/graphics/RectF;->offset(FF)V

    .line 403
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapToNearestControlForScale(FFF)Z

    move-result v2

    if-nez v2, :cond_0

    .line 406
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaledRect:Landroid/graphics/RectF;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeFitRectMatrix(Landroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 407
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    invoke-direct {p0, v2, v7, v7, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeConstrainedOffset(Landroid/graphics/Matrix;FF[F)V

    .line 408
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    aget v3, v3, v8

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    aget v4, v4, v9

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 409
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateScrollToTransform(Landroid/graphics/Matrix;)V

    .line 411
    :cond_0
    return-void

    .end local v0    # "contentArea":Landroid/graphics/RectF;
    .end local v1    # "scale":F
    :cond_1
    move v2, v3

    .line 390
    goto :goto_0
.end method

.method private initGestureDetectors()V
    .locals 4

    .prologue
    .line 744
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 746
    .local v0, "temp":Landroid/graphics/Matrix;
    new-instance v1, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;

    invoke-direct {v3, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Landroid/graphics/Matrix;)V

    invoke-direct {v1, v2, v3}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaleDetector:Landroid/view/ScaleGestureDetector;

    .line 800
    new-instance v1, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;

    invoke-direct {v3, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Landroid/graphics/Matrix;)V

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->gestureDetector:Landroid/view/GestureDetector;

    .line 905
    return-void
.end method

.method private registerSnappedControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)V
    .locals 6
    .param p1, "control"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .prologue
    .line 362
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->getIndexOfSnapControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)I

    move-result v0

    .line 363
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 364
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->getSnapControlCount()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->registerSnappedPage(II)V

    .line 365
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->partId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 366
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    sget-object v2, Lcom/google/apps/dots/shared/EventCode;->SCROLL_ON_SNAP_TO:Lcom/google/apps/dots/shared/EventCode;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->partId:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->dispatch(Ljava/lang/String;)V

    .line 369
    :cond_0
    return-void
.end method

.method private registerSnappedPage(II)V
    .locals 2
    .param p1, "page"    # I
    .param p2, "pageCount"    # I

    .prologue
    .line 352
    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    .line 353
    int-to-float v0, p1

    add-int/lit8 v1, p2, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->pageFraction:F

    .line 354
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->onPageChanged()V

    .line 356
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->lastSnappedPage:I

    if-eq p1, v0, :cond_1

    .line 357
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->lastSnappedPage:I

    .line 359
    :cond_1
    return-void
.end method

.method private updateChildrenBounds()V
    .locals 5

    .prologue
    .line 216
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->childrenBounds:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->setEmpty()V

    .line 217
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 218
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 219
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView$LayoutParams;

    .line 220
    .local v2, "layoutParams":Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView$LayoutParams;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->childrenBounds:Landroid/graphics/RectF;

    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView$LayoutParams;->location:Landroid/graphics/RectF;

    invoke-virtual {v3, v4}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 217
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 222
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "layoutParams":Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView$LayoutParams;
    :cond_0
    return-void
.end method


# virtual methods
.method public animateFlingScroll(FF)V
    .locals 18
    .param p1, "vx"    # F
    .param p2, "vy"    # F

    .prologue
    .line 908
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    if-nez v2, :cond_0

    .line 909
    new-instance v2, Landroid/widget/Scroller;

    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    .line 912
    :cond_0
    move/from16 v0, p1

    float-to-double v2, v0

    move/from16 v0, p2

    float-to-double v4, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    double-to-float v0, v2

    move/from16 v16, v0

    .line 913
    .local v16, "speed":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v2

    int-to-float v13, v2

    .line 914
    .local v13, "maxSpeed":F
    cmpl-float v2, v16, v13

    if-lez v2, :cond_1

    .line 915
    div-float v2, v13, v16

    mul-float p1, p1, v2

    .line 916
    div-float v2, v13, v16

    mul-float p2, p2, v2

    .line 919
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContentUpperLeftInLayoutCoordinates([F)V

    .line 920
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v12

    .line 921
    .local v12, "contentArea":Landroid/graphics/RectF;
    const/16 v11, 0x2710

    .line 922
    .local v11, "big":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransformScale()F

    move-result v14

    .line 923
    .local v14, "scale":F
    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getScrollBounds()Landroid/graphics/RectF;

    move-result-object v15

    .line 924
    .local v15, "scrollBounds":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    float-to-int v4, v4

    div-float v5, p1, v14

    float-to-int v5, v5

    div-float v6, p2, v14

    float-to-int v6, v6

    iget v7, v12, Landroid/graphics/RectF;->left:F

    iget v8, v15, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, v8

    float-to-int v7, v7

    sub-int/2addr v7, v11

    iget v8, v12, Landroid/graphics/RectF;->left:F

    iget v9, v15, Landroid/graphics/RectF;->right:F

    add-float/2addr v8, v9

    float-to-int v8, v8

    add-int/2addr v8, v11

    iget v9, v12, Landroid/graphics/RectF;->top:F

    iget v10, v15, Landroid/graphics/RectF;->top:F

    add-float/2addr v9, v10

    float-to-int v9, v9

    sub-int/2addr v9, v11

    iget v10, v12, Landroid/graphics/RectF;->top:F

    iget v0, v15, Landroid/graphics/RectF;->bottom:F

    move/from16 v17, v0

    add-float v10, v10, v17

    float-to-int v10, v10

    add-int/2addr v10, v11

    invoke-virtual/range {v2 .. v10}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 932
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->invalidate()V

    .line 933
    return-void
.end method

.method public animateScrollToInitialPosition()V
    .locals 3

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->initialOffsetX:F

    neg-float v1, v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->initialOffsetY:F

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 302
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateScrollToTransform(Landroid/graphics/Matrix;)V

    .line 303
    return-void
.end method

.method protected canScroll(Landroid/view/View;ZIIII)Z
    .locals 13
    .param p1, "v"    # Landroid/view/View;
    .param p2, "checkV"    # Z
    .param p3, "dx"    # I
    .param p4, "dy"    # I
    .param p5, "x"    # I
    .param p6, "y"    # I

    .prologue
    .line 497
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    move-object v9, p1

    .line 498
    check-cast v9, Landroid/view/ViewGroup;

    .line 499
    .local v9, "group":Landroid/view/ViewGroup;
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v11

    .line 500
    .local v11, "scrollX":I
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v12

    .line 501
    .local v12, "scrollY":I
    invoke-virtual {v9}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v8

    .line 503
    .local v8, "count":I
    add-int/lit8 v10, v8, -0x1

    .local v10, "i":I
    :goto_0
    if-ltz v10, :cond_1

    .line 506
    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 507
    .local v2, "child":Landroid/view/View;
    add-int v1, p5, v11

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    if-lt v1, v3, :cond_0

    add-int v1, p5, v11

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v3

    if-ge v1, v3, :cond_0

    add-int v1, p6, v12

    .line 508
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    if-lt v1, v3, :cond_0

    add-int v1, p6, v12

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ge v1, v3, :cond_0

    const/4 v3, 0x1

    add-int v1, p5, v11

    .line 509
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int v6, v1, v4

    add-int v1, p6, v12

    .line 510
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int v7, v1, v4

    move-object v1, p0

    move/from16 v4, p3

    move/from16 v5, p4

    .line 509
    invoke-virtual/range {v1 .. v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->canScroll(Landroid/view/View;ZIIII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 511
    const/4 v1, 0x1

    .line 520
    .end local v2    # "child":Landroid/view/View;
    .end local v8    # "count":I
    .end local v9    # "group":Landroid/view/ViewGroup;
    .end local v10    # "i":I
    .end local v11    # "scrollX":I
    .end local v12    # "scrollY":I
    :goto_1
    return v1

    .line 503
    .restart local v2    # "child":Landroid/view/View;
    .restart local v8    # "count":I
    .restart local v9    # "group":Landroid/view/ViewGroup;
    .restart local v10    # "i":I
    .restart local v11    # "scrollX":I
    .restart local v12    # "scrollY":I
    :cond_0
    add-int/lit8 v10, v10, -0x1

    goto :goto_0

    .line 518
    .end local v2    # "child":Landroid/view/View;
    .end local v8    # "count":I
    .end local v9    # "group":Landroid/view/ViewGroup;
    .end local v10    # "i":I
    .end local v11    # "scrollX":I
    .end local v12    # "scrollY":I
    :cond_1
    if-eqz p2, :cond_4

    if-eqz p3, :cond_2

    move/from16 v0, p3

    neg-int v1, v0

    .line 519
    invoke-virtual {p1, v1}, Landroid/view/View;->canScrollHorizontally(I)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    if-eqz p4, :cond_4

    move/from16 v0, p4

    neg-int v1, v0

    .line 520
    invoke-virtual {p1, v1}, Landroid/view/View;->canScrollVertically(I)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const/4 v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public canScrollHorizontally(I)Z
    .locals 3
    .param p1, "direction"    # I

    .prologue
    const/4 v0, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 565
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isOnlyChildOfRoot:Z

    if-nez v1, :cond_1

    neg-int v1, p1

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeHorizontalOffset(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 569
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeHorizontalOffset(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canScrollVertically(I)Z
    .locals 3
    .param p1, "direction"    # I

    .prologue
    const/4 v0, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 582
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isOnlyChildOfRoot:Z

    if-nez v1, :cond_1

    neg-int v1, p1

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeVerticalOffset(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 586
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeVerticalOffset(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelAnimatingScroll()V
    .locals 1

    .prologue
    .line 942
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->cancelAnimatingScroll()V

    .line 943
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    .line 944
    return-void
.end method

.method public computeScroll()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 948
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    if-eqz v6, :cond_a

    .line 949
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 950
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrX()I

    move-result v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->getCurrY()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransformScale()F

    move-result v8

    invoke-virtual {p0, v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scrollToPoint(FFF)V

    .line 953
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForLeftBoundary()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForRightBoundary()Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_0
    move v0, v5

    .line 955
    .local v0, "againstLeftOrRight":Z
    :goto_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForTopBoundary()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForBottomBoundary()Z

    move-result v6

    if-eqz v6, :cond_9

    :cond_1
    move v1, v5

    .line 957
    .local v1, "againstTopOrBottom":Z
    :goto_1
    if-nez v1, :cond_2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->isFinished()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 958
    :cond_2
    const/4 v4, -0x1

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeVerticalOffset(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 959
    .local v2, "topOffset":F
    invoke-direct {p0, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeVerticalOffset(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    add-float v3, v2, v4

    .line 962
    .local v3, "total":F
    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-lez v4, :cond_4

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->hasScrolledInternally:Z

    if-nez v4, :cond_3

    if-eqz v1, :cond_4

    .line 963
    :cond_3
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->onScrolled()V

    .line 967
    .end local v2    # "topOffset":F
    .end local v3    # "total":F
    :cond_4
    if-eqz v0, :cond_5

    if-nez v1, :cond_6

    :cond_5
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    .line 968
    invoke-virtual {v4}, Landroid/widget/Scroller;->isFinished()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 969
    :cond_6
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    .line 974
    .end local v0    # "againstLeftOrRight":Z
    .end local v1    # "againstTopOrBottom":Z
    :cond_7
    :goto_2
    return-void

    :cond_8
    move v0, v4

    .line 953
    goto :goto_0

    .restart local v0    # "againstLeftOrRight":Z
    :cond_9
    move v1, v4

    .line 955
    goto :goto_1

    .line 972
    .end local v0    # "againstLeftOrRight":Z
    :cond_a
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->computeScroll()V

    goto :goto_2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 431
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->clearGestureStateRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->cancel()V

    .line 432
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 433
    .local v0, "action":I
    if-nez v0, :cond_0

    .line 435
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    if-nez v2, :cond_5

    .line 436
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    .line 445
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_1

    .line 446
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 448
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 449
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 450
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->touchSlopInterceptor:Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->onTouchEvent(Landroid/view/MotionEvent;)Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    .line 451
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 454
    .local v1, "result":Z
    if-eq v0, v3, :cond_2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_4

    .line 455
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_3

    .line 456
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->recycle()V

    .line 457
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    .line 461
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->clearGestureStateRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v4, v5, v3}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 469
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->hasScrolledInternally:Z

    .line 471
    :cond_4
    return v1

    .line 438
    .end local v1    # "result":Z
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x0

    .line 1038
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->draw(Landroid/graphics/Canvas;)V

    .line 1041
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getWidth()I

    move-result v3

    .line 1042
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getHeight()I

    move-result v4

    .line 1043
    .local v4, "height":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->topEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    move-object v0, p0

    move-object v1, p1

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->drawEdge(Landroid/graphics/Canvas;Landroid/support/v4/widget/EdgeEffectCompat;IIIII)Z

    move-result v0

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->rightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    const/16 v13, 0x5a

    move-object v6, p0

    move-object v7, p1

    move v9, v4

    move v10, v3

    move v11, v3

    move v12, v5

    .line 1044
    invoke-direct/range {v6 .. v13}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->drawEdge(Landroid/graphics/Canvas;Landroid/support/v4/widget/EdgeEffectCompat;IIIII)Z

    move-result v1

    or-int/2addr v0, v1

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    const/16 v13, 0xb4

    move-object v6, p0

    move-object v7, p1

    move v9, v3

    move v10, v4

    move v11, v3

    move v12, v4

    .line 1045
    invoke-direct/range {v6 .. v13}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->drawEdge(Landroid/graphics/Canvas;Landroid/support/v4/widget/EdgeEffectCompat;IIIII)Z

    move-result v1

    or-int/2addr v0, v1

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->leftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    const/16 v13, -0x5a

    move-object v6, p0

    move-object v7, p1

    move v9, v4

    move v10, v3

    move v11, v5

    move v12, v4

    .line 1046
    invoke-direct/range {v6 .. v13}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->drawEdge(Landroid/graphics/Canvas;Landroid/support/v4/widget/EdgeEffectCompat;IIIII)Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 1047
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->invalidate()V

    .line 1052
    :goto_0
    return-void

    .line 1050
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setWillNotDraw(Z)V

    goto :goto_0
.end method

.method public getCurrentPage()I
    .locals 2

    .prologue
    .line 1010
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getCurrentPageFraction()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getCurrentPageFraction()F
    .locals 1

    .prologue
    .line 978
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->pageFraction:F

    return v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 983
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->getSnapControlCount()I

    move-result v0

    return v0
.end method

.method public getScrollOffset()I
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 996
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getHorizontalScrollMax()I

    move-result v0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getVerticalScrollMax()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 997
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeHorizontalOffset(I)F

    move-result v0

    float-to-int v0, v0

    .line 999
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeVerticalOffset(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public getScrollRange()I
    .locals 2

    .prologue
    .line 1005
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getHorizontalScrollMax()I

    move-result v0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getVerticalScrollMax()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public isAnimatingScroll()Z
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scroller:Landroid/widget/Scroller;

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->isAnimatingScroll()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHorizontallyScrollable()Z
    .locals 4

    .prologue
    .line 713
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getScrollBounds()Landroid/graphics/RectF;

    move-result-object v0

    .line 714
    .local v0, "scrollBounds":Landroid/graphics/RectF;
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransformScale()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isVerticallyScrollable()Z
    .locals 4

    .prologue
    .line 718
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getScrollBounds()Landroid/graphics/RectF;

    move-result-object v0

    .line 719
    .local v0, "scrollBounds":Landroid/graphics/RectF;
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransformScale()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 477
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getPreTransformedMotionEvent()Landroid/view/MotionEvent;

    move-result-object p1

    .line 481
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->touchSlopInterceptor:Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideSlop:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->touchSlopInterceptor:Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideHorizontalSlop:Z

    if-eqz v0, :cond_1

    .line 484
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->touchSlopInterceptor:Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->getReferenceX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    :goto_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->touchSlopInterceptor:Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideVerticalSlop:Z

    if-eqz v0, :cond_2

    .line 486
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->touchSlopInterceptor:Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->getReferenceY()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    :goto_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->touchSlopInterceptor:Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    .line 487
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->getReferenceX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v5

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->touchSlopInterceptor:Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    .line 488
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->getReferenceY()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v6

    move-object v0, p0

    move-object v1, p0

    .line 482
    invoke-virtual/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->canScroll(Landroid/view/View;ZIIII)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 492
    :cond_0
    :goto_2
    return v2

    :cond_1
    move v3, v2

    .line 484
    goto :goto_0

    :cond_2
    move v4, v2

    .line 486
    goto :goto_1

    .line 492
    :cond_3
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isInGesture:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->touchSlopInterceptor:Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideSlop:Z

    if-eqz v0, :cond_0

    :cond_4
    const/4 v2, 0x1

    goto :goto_2
.end method

.method public onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 1015
    invoke-super/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->onLayout(ZIIII)V

    .line 1016
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isLaidOut:Z

    .line 1017
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 526
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getPreTransformedMotionEvent()Landroid/view/MotionEvent;

    move-result-object p1

    .line 528
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isInGesture:Z

    .line 530
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 552
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 534
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didTriggerDoubleTapZoomInGesture:Z

    if-nez v0, :cond_0

    .line 535
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->cancelAnimatingScroll()V

    goto :goto_0

    .line 541
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isAnimatingScroll()Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapToNearestControl()Z

    move-result v0

    if-nez v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 546
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    neg-float v0, v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateFlingScroll(FF)V

    goto :goto_0

    .line 530
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onUnhandledFling(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)V
    .locals 9
    .param p1, "flingDirection"    # Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    .prologue
    const/4 v8, 0x1

    const/4 v7, -0x1

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 591
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransformScale()F

    move-result v0

    .line 592
    .local v0, "scale":F
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->getSnapControlCount()I

    move-result v3

    if-eqz v3, :cond_0

    .line 593
    const/4 v1, 0x0

    .line 594
    .local v1, "vx":F
    const/4 v2, 0x0

    .line 595
    .local v2, "vy":F
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$6;->$SwitchMap$com$google$apps$dots$android$newsstand$util$MotionHelper$FlingDirection:[I

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 601
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransformScale()F

    move-result v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapToNearestControlInDirection(FFF)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 639
    .end local v1    # "vx":F
    .end local v2    # "vy":F
    :goto_1
    return-void

    .line 596
    .restart local v1    # "vx":F
    .restart local v2    # "vy":F
    :pswitch_0
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    .line 597
    :pswitch_1
    const/high16 v1, -0x40800000    # -1.0f

    goto :goto_0

    .line 598
    :pswitch_2
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_0

    .line 599
    :pswitch_3
    const/high16 v2, -0x40800000    # -1.0f

    goto :goto_0

    .line 607
    .end local v1    # "vx":F
    .end local v2    # "vy":F
    :cond_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$6;->$SwitchMap$com$google$apps$dots$android$newsstand$util$MotionHelper$FlingDirection:[I

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 638
    :cond_1
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->onUnhandledFling(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)V

    goto :goto_1

    .line 613
    :pswitch_4
    invoke-direct {p0, v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeHorizontalOffset(I)F

    move-result v3

    neg-float v3, v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_1

    .line 614
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->simulatedFlingVelocity:F

    invoke-virtual {p0, v3, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateFlingScroll(FF)V

    goto :goto_1

    .line 619
    :pswitch_5
    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeHorizontalOffset(I)F

    move-result v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_1

    .line 620
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->simulatedFlingVelocity:F

    neg-float v3, v3

    invoke-virtual {p0, v3, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateFlingScroll(FF)V

    goto :goto_1

    .line 625
    :pswitch_6
    invoke-direct {p0, v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeVerticalOffset(I)F

    move-result v3

    neg-float v3, v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_1

    .line 626
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->simulatedFlingVelocity:F

    invoke-virtual {p0, v5, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateFlingScroll(FF)V

    goto :goto_1

    .line 631
    :pswitch_7
    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeVerticalOffset(I)F

    move-result v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_1

    .line 632
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->simulatedFlingVelocity:F

    neg-float v3, v3

    invoke-virtual {p0, v5, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateFlingScroll(FF)V

    goto :goto_1

    .line 595
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 607
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public scrollToEdge(I)V
    .locals 5
    .param p1, "direction"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 263
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->cancelAnimatingScroll()V

    .line 264
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransform(Landroid/graphics/Matrix;)V

    .line 265
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeHorizontalOffset(I)F

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 266
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setTransform(Landroid/graphics/Matrix;)V

    .line 270
    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeHorizontalOffset(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeHorizontalOffset(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v1, v2

    .line 271
    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeVerticalOffset(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeVerticalOffset(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 272
    if-gez p1, :cond_1

    :goto_0
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->pageFraction:F

    .line 274
    :cond_0
    return-void

    .line 272
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public scrollToInitialPosition()V
    .locals 3

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->initialOffsetX:F

    neg-float v1, v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->initialOffsetY:F

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 297
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setTransform(Landroid/graphics/Matrix;)V

    .line 298
    return-void
.end method

.method public scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
    .locals 11
    .param p1, "pageLocation"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .prologue
    const/4 v10, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    .line 230
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->cancelAnimatingScroll()V

    .line 232
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v0

    .line 233
    .local v0, "contentArea":Landroid/graphics/RectF;
    const/4 v5, 0x0

    .line 234
    .local v5, "snapControl":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    const/4 v2, 0x0

    .line 235
    .local v2, "px":F
    const/4 v3, 0x0

    .line 236
    .local v3, "py":F
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->hasValidPageFraction()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->getSnapControlCount()I

    move-result v6

    if-nez v6, :cond_2

    .line 237
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->getNonNullPageFraction()F

    move-result v1

    .line 238
    .local v1, "pageFraction":F
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getScrollBounds()Landroid/graphics/RectF;

    move-result-object v4

    .line 241
    .local v4, "scrollBounds":Landroid/graphics/RectF;
    iget v6, v4, Landroid/graphics/RectF;->left:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v8

    sub-float/2addr v7, v8

    mul-float/2addr v7, v1

    add-float v2, v6, v7

    .line 242
    iget v6, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v8

    sub-float/2addr v7, v8

    mul-float/2addr v7, v1

    add-float v3, v6, v7

    .line 243
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    invoke-virtual {v6, v2, v3, v9}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->getNearestSnapControlTo(FFF)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    move-result-object v5

    .line 251
    .end local v1    # "pageFraction":F
    .end local v4    # "scrollBounds":Landroid/graphics/RectF;
    :cond_1
    :goto_0
    if-eqz v5, :cond_3

    .line 252
    invoke-direct {p0, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->registerSnappedControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)V

    .line 253
    iget v6, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->getUnzoomedPoint()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getX()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v6, v7

    iget v7, v0, Landroid/graphics/RectF;->top:F

    .line 254
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->getUnzoomedPoint()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getY()I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    .line 253
    invoke-virtual {p0, v6, v7, v9}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scrollToPoint(FFF)V

    .line 259
    :goto_1
    return-void

    .line 244
    :cond_2
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->hasValidPageNumber()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 246
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    .line 247
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->getPageNumber()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->getSnapControlCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    .line 246
    invoke-static {v7, v10, v8}, Lcom/google/apps/dots/android/newsstand/util/MathUtil;->clamp(III)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->getSnapControl(I)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    move-result-object v5

    goto :goto_0

    .line 256
    :cond_3
    const/4 v6, 0x1

    invoke-direct {p0, v10, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->registerSnappedPage(II)V

    .line 257
    iget v6, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v2

    iget v7, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v7, v3

    invoke-virtual {p0, v6, v7, v9}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scrollToPoint(FFF)V

    goto :goto_1
.end method

.method public setEnableGutterTap(Z)V
    .locals 0
    .param p1, "enableGutterTap"    # Z

    .prologue
    .line 200
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->enableGutterTap:Z

    .line 201
    return-void
.end method

.method public setInitialOffset(FF)V
    .locals 0
    .param p1, "initialOffsetX"    # F
    .param p2, "initialOffsetY"    # F

    .prologue
    .line 186
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->initialOffsetX:F

    .line 187
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->initialOffsetY:F

    .line 188
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scrollToInitialPosition()V

    .line 189
    return-void
.end method

.method public setMaxScale(F)V
    .locals 0
    .param p1, "maxScale"    # F

    .prologue
    .line 179
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->maxScale:F

    .line 180
    return-void
.end method

.method public setMinScale(F)V
    .locals 0
    .param p1, "minScale"    # F

    .prologue
    .line 175
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->minScale:F

    .line 176
    return-void
.end method

.method public setScrollExtent(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "scrollExtent"    # Landroid/graphics/RectF;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scrollExtent:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 196
    return-void
.end method

.method public setSnapControls(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 280
    .local p1, "snapControls":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;>;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->set(Ljava/util/List;)V

    .line 281
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->partId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 282
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 283
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .line 284
    .local v0, "control":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    sget-object v3, Lcom/google/apps/dots/shared/EventCode;->SCROLL_DO_SNAP_TO:Lcom/google/apps/dots/shared/EventCode;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->partId:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 285
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->eventScope:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$3;

    invoke-direct {v5, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)V

    .line 284
    invoke-virtual {v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 282
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 293
    .end local v0    # "control":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method public setTransform(Landroid/graphics/Matrix;)V
    .locals 4
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    const/4 v2, 0x0

    .line 706
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 707
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->transformConstraintCorrection:[F

    invoke-direct {p0, v0, v2, v2, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->computeConstrainedOffset(Landroid/graphics/Matrix;FF[F)V

    .line 708
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->transformConstraintCorrection:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->transformConstraintCorrection:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 709
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempMatrix:Landroid/graphics/Matrix;

    invoke-super {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->setTransform(Landroid/graphics/Matrix;)V

    .line 710
    return-void
.end method

.method public snapToNearestControl()Z
    .locals 1

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransformScale()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapToNearestControl(F)Z

    move-result v0

    return v0
.end method

.method public snapToNearestControl(F)Z
    .locals 3
    .param p1, "scale"    # F

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContentUpperLeftInLayoutCoordinates([F)V

    .line 316
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapToNearestControlForScale(FFF)Z

    move-result v0

    return v0
.end method

.method public snapToNearestControlForScale(FFF)Z
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "scale"    # F

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v0

    .line 326
    .local v0, "contentArea":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    sub-float v3, p1, v3

    iget v4, v0, Landroid/graphics/RectF;->top:F

    sub-float v4, p2, v4

    .line 327
    invoke-virtual {v2, v3, v4, p3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->getNearestSnapControlTo(FFF)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    move-result-object v1

    .line 328
    .local v1, "nearestControl":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    if-eqz v1, :cond_0

    .line 329
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateScrollToSnapControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)V

    .line 331
    :cond_0
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public snapToNearestControlInDirection(FFF)Z
    .locals 10
    .param p1, "vx"    # F
    .param p2, "vy"    # F
    .param p3, "scale"    # F

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 341
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v6

    .line 342
    .local v6, "contentArea":Landroid/graphics/RectF;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getContentUpperLeftInLayoutCoordinates([F)V

    .line 343
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapControlUtil:Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    aget v1, v1, v9

    iget v2, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->tempPoint:[F

    aget v2, v2, v8

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    move v3, p3

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->getNearestSnapControlTo(FFFFF)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    move-result-object v7

    .line 345
    .local v7, "nearestControl":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    if-eqz v7, :cond_0

    .line 346
    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->animateScrollToSnapControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)V

    .line 348
    :cond_0
    if-eqz v7, :cond_1

    move v0, v8

    :goto_0
    return v0

    :cond_1
    move v0, v9

    goto :goto_0
.end method

.method public snapToNextPage()Z
    .locals 1

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 380
    invoke-virtual {p0, v0, v0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapToNearestControlInDirection(FFF)Z

    move-result v0

    return v0
.end method

.method public snapToPreviousPage()Z
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 385
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1, v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapToNearestControlInDirection(FFF)Z

    move-result v0

    return v0
.end method
