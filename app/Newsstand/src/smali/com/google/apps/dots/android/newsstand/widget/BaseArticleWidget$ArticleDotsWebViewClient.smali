.class Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$ArticleDotsWebViewClient;
.super Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;
.source "BaseArticleWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ArticleDotsWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p2, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$ArticleDotsWebViewClient;->this$0:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    .line 412
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 413
    return-void
.end method


# virtual methods
.method public onLayoutChange(IZII)V
    .locals 1
    .param p1, "pageCount"    # I
    .param p2, "isDone"    # Z
    .param p3, "pageWidth"    # I
    .param p4, "pageHeight"    # I

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$ArticleDotsWebViewClient;->this$0:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->handleOnLayoutChange(IZII)V

    .line 423
    return-void
.end method

.method public onReady()V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$ArticleDotsWebViewClient;->this$0:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->handleOnWebViewClientReady()V

    .line 418
    return-void
.end method
