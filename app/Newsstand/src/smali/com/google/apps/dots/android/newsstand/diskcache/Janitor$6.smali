.class Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$6;
.super Ljava/lang/Object;
.source "Janitor.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->splitStoreFiles()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 305
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$6;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;)V
    .locals 6
    .param p1, "region"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 308
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 309
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->getStats()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    move-result-object v1

    .line 310
    .local v1, "storeFileStats":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$6;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->shouldSplit(Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;)Z
    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$1200(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 312
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->computeSplitPoint()Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v0

    .line 313
    .local v0, "splitPoint":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    if-nez v0, :cond_1

    .line 314
    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Couldn\'t find split point for %s."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 320
    .end local v0    # "splitPoint":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .end local v1    # "storeFileStats":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    :cond_0
    :goto_0
    return-void

    .line 316
    .restart local v0    # "splitPoint":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .restart local v1    # "storeFileStats":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$6;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$100(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v2

    invoke-virtual {v2, p1, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->split(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V

    goto :goto_0
.end method
