.class Lcom/google/apps/dots/android/newsstand/store/NSStore$4;
.super Ljava/lang/Object;
.source "NSStore.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerAttachmentDownloadWithFallbackToClientTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
        "<",
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/android/newsstand/server/Transform;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 343
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$4;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$4;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "input"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Lcom/google/apps/dots/android/newsstand/server/Transform;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->decayCacheFallbackTimeout()J
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$800(Lcom/google/apps/dots/android/newsstand/store/NSStore;)J

    .line 349
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$4;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$4;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getDefaultTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public fallback(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Lcom/google/apps/dots/android/newsstand/server/Transform;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 354
    invoke-static {p1}, Lcom/google/common/util/concurrent/Futures;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
