.class public Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "MediaDrawerPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private adapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

.field private animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private attribution:Landroid/widget/TextView;

.field private caption:Landroid/widget/TextView;

.field private expandedCaption:Landroid/widget/TextView;

.field private infoView:Landroid/view/View;

.field private pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

.field private pager:Landroid/support/v4/view/NSViewPager;

.field private pagerDropShadow:Landroid/view/View;

.field private pagerList:Lcom/google/android/libraries/bind/data/DataList;

.field private pagerObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private progressDots:Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;

.field private progressInfo:Landroid/view/View;

.field private progressText:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 70
    const/4 v0, 0x0

    const-string v1, "MediaDrawerPagerFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->media_drawer_pager_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 60
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;I)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;
    .param p1, "x1"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->fragment(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->setupProgress()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->updateInfoContainer()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->gotoCurrentMediaItem()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->caption:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandedCaption:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressInfo:Landroid/view/View;

    return-object v0
.end method

.method private changeCaptionState(Z)V
    .locals 6
    .param p1, "expand"    # Z

    .prologue
    .line 361
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    .line 362
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->currentMediaItem()Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-boolean v3, v3, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->restrictToSingleField:Z

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Ljava/lang/String;ZLcom/google/apps/dots/android/newsstand/edition/Edition;Z)V

    .line 363
    .local v0, "newState":Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 364
    return-void
.end method

.method private currentMediaItem()Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .locals 1

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->mediaItem(I)Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    goto :goto_0
.end method

.method private expandCaption(Z)V
    .locals 6
    .param p1, "expand"    # Z

    .prologue
    .line 325
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->caption:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLineCount()I

    move-result v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandedCaption:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLineCount()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 358
    :goto_0
    return-void

    .line 329
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandedCaption:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->caption:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getY()F

    move-result v3

    sub-float v3, v2, v3

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    :goto_1
    int-to-float v2, v2

    mul-float v1, v3, v2

    .line 331
    .local v1, "yDiff":F
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressInfo:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x190

    .line 332
    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 333
    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 334
    .local v0, "animator":Landroid/view/ViewPropertyAnimator;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;

    invoke-direct {v3, p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;Z)V

    invoke-static {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/view/ViewPropertyAnimator;Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 329
    .end local v0    # "animator":Landroid/view/ViewPropertyAnimator;
    .end local v1    # "yDiff":F
    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method private fragment(I)Landroid/support/v4/app/Fragment;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 383
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->mediaItem(I)Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v0

    .line 384
    .local v0, "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;-><init>()V

    .line 385
    .local v1, "mediaItemFragment":Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->setInitialState(Landroid/os/Parcelable;)V

    .line 386
    return-object v1
.end method

.method private gotoCurrentMediaItem()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 270
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v2

    if-nez v2, :cond_0

    .line 281
    :goto_0
    return-void

    .line 274
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->currentMediaItem()Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    .line 275
    .local v0, "logicalPosition":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 276
    sget-object v2, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Unable to find media item"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 279
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-static {v2, v0}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getVisualPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v1

    .line 280
    .local v1, "visualPosition":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v2, v1, v4}, Landroid/support/v4/view/NSViewPager;->setCurrentItem(IZ)V

    goto :goto_0
.end method

.method private mediaItem(I)Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .locals 3
    .param p1, "logicalPosition"    # I

    .prologue
    const/4 v1, 0x0

    .line 375
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v2, :cond_1

    .line 379
    :cond_0
    :goto_0
    return-object v1

    .line 378
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2, p1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 379
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_0

    sget v1, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_MEDIA_ITEM:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    goto :goto_0
.end method

.method private setAccessibility(Z)V
    .locals 2
    .param p1, "isAccessible"    # Z

    .prologue
    .line 231
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 233
    .local v0, "mode":I
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->caption:Landroid/widget/TextView;

    invoke-static {v1, v0}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 234
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandedCaption:Landroid/widget/TextView;

    invoke-static {v1, v0}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 235
    return-void

    .line 231
    .end local v0    # "mode":I
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private setupCaption()V
    .locals 2

    .prologue
    .line 101
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)V

    .line 107
    .local v0, "toggleExpandedCaptionListener":Landroid/view/View$OnClickListener;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->caption:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandedCaption:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    return-void
.end method

.method private setupPager()V
    .locals 5

    .prologue
    .line 112
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$2;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;Landroid/support/v4/view/NSViewPager;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

    .line 125
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerDropShadow:Landroid/view/View;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/HalfSlidePageChangeListener;-><init>(Landroid/support/v4/view/NSViewPager;Landroid/view/View;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 127
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$3;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;Landroid/support/v4/app/FragmentManager;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 133
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 134
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 148
    return-void
.end method

.method private setupProgress()V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 219
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressDots:Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;

    invoke-virtual {v0, v4}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->setPageCountIsFinal(Z)V

    .line 221
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressDots:Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->setPageCount(I)V

    .line 223
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressDots:Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v0

    if-le v0, v4, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v3

    if-le v3, v4, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->updateProgress()V

    .line 226
    return-void

    :cond_1
    move v0, v2

    .line 223
    goto :goto_0

    :cond_2
    move v1, v2

    .line 224
    goto :goto_1
.end method

.method private toggle(Landroid/view/View;Z)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "show"    # Z

    .prologue
    const/4 v1, 0x0

    .line 301
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    if-nez p2, :cond_1

    .line 302
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_2

    if-nez p2, :cond_2

    .line 321
    :cond_1
    :goto_0
    return-void

    .line 305
    :cond_2
    if-eqz p2, :cond_3

    .line 306
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 307
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 309
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0xc8

    .line 310
    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz p2, :cond_4

    const/high16 v1, 0x3f800000    # 1.0f

    .line 311
    :cond_4
    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 312
    .local v0, "animator":Landroid/view/ViewPropertyAnimator;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$5;

    invoke-direct {v2, p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;ZLandroid/view/View;)V

    invoke-static {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/view/ViewPropertyAnimator;Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method private updateCaptionLength()V
    .locals 1

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->expandedCaptionVisible:Z

    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->lightsOn()V

    .line 263
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandCaption(Z)V

    .line 267
    :goto_0
    return-void

    .line 265
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandCaption(Z)V

    goto :goto_0
.end method

.method private updateInfoContainer()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 238
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->currentMediaItem()Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v2

    .line 239
    .local v2, "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    if-nez v2, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    const/4 v0, 0x0

    .line 243
    .local v0, "captionText":Ljava/lang/String;
    iget-object v3, v2, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasImage()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 244
    iget-object v3, v2, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v1

    .line 245
    .local v1, "image":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getCaption()Ljava/lang/String;

    move-result-object v0

    .line 246
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->caption:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandedCaption:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->setAccessibility(Z)V

    .line 250
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->attribution:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttribution()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->attribution:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hasAttribution()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v4

    :goto_1
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    .end local v1    # "image":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_2
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 254
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->caption:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandedCaption:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 256
    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->setAccessibility(Z)V

    goto :goto_0

    .line 251
    .restart local v1    # "image":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_3
    const/4 v3, 0x4

    goto :goto_1
.end method


# virtual methods
.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 391
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->getPrimaryVisibleFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    move-result-object v0

    .line 392
    .local v0, "primaryFragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 393
    :goto_0
    return-object v1

    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v1

    goto :goto_0
.end method

.method public getPrimaryVisibleFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    .locals 4

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v1

    .line 398
    .local v1, "childFragments":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/app/Fragment;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 400
    .local v0, "childFragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    .line 401
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 402
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 403
    check-cast v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .line 406
    .end local v0    # "childFragment":Landroid/support/v4/app/Fragment;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lightsOff()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 292
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->infoView:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->toggle(Landroid/view/View;Z)V

    .line 293
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandCaption(Z)V

    .line 294
    return-void
.end method

.method public lightsOn()V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->infoView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->toggle(Landroid/view/View;Z)V

    .line 298
    return-void
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 158
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 159
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->destroy()V

    .line 160
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onDestroyView()V

    .line 161
    return-void
.end method

.method public onViewCreated(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 81
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->pager:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/NSViewPager;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    .line 82
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->info:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->infoView:Landroid/view/View;

    .line 83
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->caption:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->caption:Landroid/widget/TextView;

    .line 84
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->expanded_caption:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandedCaption:Landroid/widget/TextView;

    .line 85
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->attribution:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->attribution:Landroid/widget/TextView;

    .line 86
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->progress_info:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressInfo:Landroid/view/View;

    .line 87
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->progressDots:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressDots:Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;

    .line 88
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->progressText:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressText:Landroid/widget/TextView;

    .line 89
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->pager_drop_shadow:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerDropShadow:Landroid/view/View;

    .line 90
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->toolbar:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 91
    .local v0, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBarActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 93
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->setupCaption()V

    .line 94
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->setupPager()V

    .line 97
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 98
    return-void
.end method

.method public restartLightsOn()V
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->toggleLightsOnMode()V

    .line 191
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->lightsOn()V

    .line 193
    :cond_0
    return-void
.end method

.method public toggleExpandedCaption()V
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->expandedCaptionVisible:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->changeCaptionState(Z)V

    .line 368
    return-void

    .line 367
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggleLightsOnMode()V
    .locals 1

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->lightsOff()V

    .line 289
    :goto_0
    return-void

    .line 287
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->lightsOn()V

    goto :goto_0
.end method

.method public updateMediaItemsList()V
    .locals 4

    .prologue
    .line 207
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v1, :cond_0

    .line 208
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 210
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->currentMediaItem()Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 212
    .local v0, "fieldId":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-object v3, v1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-boolean v1, v1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->restrictToSingleField:Z

    .line 211
    invoke-static {v2, v3, v0, v1}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->getMediaDrawerList(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    .line 213
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 214
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->setList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 215
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->setupProgress()V

    .line 216
    return-void

    .line 210
    .end local v0    # "fieldId":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->currentMediaItem()Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v1

    iget-object v0, v1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->fieldId:Ljava/lang/String;

    goto :goto_0
.end method

.method public updateProgress()V
    .locals 7

    .prologue
    .line 196
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->currentMediaItem()Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    .line 197
    .local v0, "pageNum":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 198
    const/4 v0, 0x0

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressDots:Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->setPageNumber(I)V

    .line 201
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 202
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->media_progress:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    add-int/lit8 v6, v0, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v6}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 201
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressText:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->announce(Landroid/view/View;)V

    .line 204
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;)V
    .locals 8
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 166
    if-eqz p2, :cond_0

    iget-object v4, p2, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    :cond_0
    move v2, v3

    .line 167
    .local v2, "postChanged":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 168
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v4

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    const/4 v6, 0x0

    iget-object v7, p1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    invoke-static {v4, v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    .line 169
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->updateMediaItemsList()V

    .line 170
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->restartLightsOn()V

    .line 173
    :cond_1
    if-eqz p2, :cond_2

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    iget-object v5, p2, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    :cond_2
    move v1, v3

    .line 174
    .local v1, "itemChanged":Z
    :goto_1
    if-eqz v1, :cond_3

    .line 175
    sget-object v4, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "updateViews: itemChanged"

    new-array v6, v0, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->gotoCurrentMediaItem()V

    .line 177
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->updateInfoContainer()V

    .line 178
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->updateProgress()V

    .line 181
    :cond_3
    if-eqz p2, :cond_4

    iget-boolean v4, p2, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->expandedCaptionVisible:Z

    iget-boolean v5, p1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->expandedCaptionVisible:Z

    if-eq v4, v5, :cond_5

    :cond_4
    move v0, v3

    .line 183
    .local v0, "captionVisibilityChanged":Z
    :cond_5
    if-eqz v0, :cond_6

    .line 184
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->updateCaptionLength()V

    .line 186
    :cond_6
    return-void

    .end local v0    # "captionVisibilityChanged":Z
    .end local v1    # "itemChanged":Z
    .end local v2    # "postChanged":Z
    :cond_7
    move v2, v0

    .line 166
    goto :goto_0

    .restart local v2    # "postChanged":Z
    :cond_8
    move v1, v0

    .line 173
    goto :goto_1
.end method
