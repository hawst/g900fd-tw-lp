.class public interface abstract Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;
.super Ljava/lang/Object;
.source "ArticleEventHandler.java"


# virtual methods
.method public abstract onArticleOverscrolled(Landroid/view/View;Z)V
.end method

.method public abstract onArticlePageChanged(Landroid/view/View;IIZ)V
.end method

.method public abstract onArticleScrolled(Landroid/view/View;IIIZ)V
.end method

.method public abstract onArticleUnhandledClick(Landroid/view/View;)V
.end method
