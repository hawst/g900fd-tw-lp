.class Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$WebArticleRenderDelegate;
.super Ljava/lang/Object;
.source "WebArticleFragment.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WebArticleRenderDelegate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;


# direct methods
.method private constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$WebArticleRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;
    .param p2, "x1"    # Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$1;

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$WebArticleRenderDelegate;-><init>(Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;)V

    return-void
.end method


# virtual methods
.method public onArticleOverscrolled(Landroid/view/View;Z)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "top"    # Z

    .prologue
    .line 142
    return-void
.end method

.method public onArticlePageChanged(Landroid/view/View;IIZ)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "page"    # I
    .param p3, "pageCount"    # I
    .param p4, "userDriven"    # Z

    .prologue
    .line 147
    return-void
.end method

.method public onArticleScrolled(Landroid/view/View;IIIZ)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "scrollOffset"    # I
    .param p3, "scrollRange"    # I
    .param p4, "scrollDelta"    # I
    .param p5, "userDriven"    # Z

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$WebArticleRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->access$100(Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$WebArticleRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->access$100(Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v0

    invoke-virtual {v0, p4, p2}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;->onScroll(II)V

    .line 137
    :cond_0
    return-void
.end method

.method public onArticleUnhandledClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$WebArticleRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->access$100(Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$WebArticleRenderDelegate;->this$0:Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->access$100(Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;->onClick()V

    .line 129
    :cond_0
    return-void
.end method
