.class public final Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PostSummary"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;


# instance fields
.field private abstract__:Ljava/lang/String;

.field public allowedCountry:[Ljava/lang/String;

.field public altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

.field private appFamilyId_:Ljava/lang/String;

.field private appFamilyName_:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field private appName_:Ljava/lang/String;

.field private audioItemCount_:I

.field private author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

.field private availableInScs_:Z

.field private bitField0_:I

.field private bitField1_:I

.field public blockedCountry:[Ljava/lang/String;

.field private canUseImagesAsHeaderBackground_:Z

.field public clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

.field private created_:J

.field private deleted_:Z

.field private externalCreated_:J

.field private externalPostUrl_:Ljava/lang/String;

.field private favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private formId_:Ljava/lang/String;

.field private formTemplateId_:Ljava/lang/String;

.field private invisibleInGotoMenu_:Z

.field private isBookmarkable_:Z

.field private isMetered_:Z

.field public landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private languageCode_:Ljava/lang/String;

.field private meteredPolicyType_:I

.field private nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

.field private numHorizontalLandscapeThumbnails_:I

.field private numHorizontalPortraitThumbnails_:I

.field public postId:Ljava/lang/String;

.field private primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

.field private renderingHintV2_:Ljava/lang/String;

.field private renderingHint_:Ljava/lang/String;

.field public scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

.field public scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field public sectionId:Ljava/lang/String;

.field private sectionType_:I

.field private shareUrl_:Ljava/lang/String;

.field private showTopicTags_:Z

.field private socialAbstract_:Ljava/lang/String;

.field private sortKey_:J

.field private sourceIconId_:Ljava/lang/String;

.field private storeType_:I

.field private subtitle_:Ljava/lang/String;

.field private textDirection_:I

.field private title_:Ljava/lang/String;

.field private translationCode_:Ljava/lang/String;

.field private untranslatedAppFamilyId_:Ljava/lang/String;

.field private untranslatedAppId_:Ljava/lang/String;

.field private untranslatedFormId_:Ljava/lang/String;

.field private untranslatedPostId_:Ljava/lang/String;

.field private untranslatedSectionId_:Ljava/lang/String;

.field private updated_:J

.field private videoItemCount_:I

.field private wordCount_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17381
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 17382
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 17765
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 17768
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedPostId_:Ljava/lang/String;

    .line 17790
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    .line 17793
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedSectionId_:Ljava/lang/String;

    .line 17815
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    .line 17818
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppId_:Ljava/lang/String;

    .line 17840
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyId_:Ljava/lang/String;

    .line 17862
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 17884
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formId_:Ljava/lang/String;

    .line 17906
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedFormId_:Ljava/lang/String;

    .line 17929
    const-string v0, "article_default"

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formTemplateId_:Ljava/lang/String;

    .line 17951
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->title_:Ljava/lang/String;

    .line 17973
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->subtitle_:Ljava/lang/String;

    .line 17995
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->abstract__:Ljava/lang/String;

    .line 18074
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->shareUrl_:Ljava/lang/String;

    .line 18096
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->created_:J

    .line 18115
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->updated_:J

    .line 18134
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sortKey_:J

    .line 18153
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->deleted_:Z

    .line 18172
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalCreated_:J

    .line 18191
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionType_:I

    .line 18229
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->socialAbstract_:Ljava/lang/String;

    .line 18251
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHint_:Ljava/lang/String;

    .line 18273
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->languageCode_:Ljava/lang/String;

    .line 18295
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->translationCode_:Ljava/lang/String;

    .line 18317
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->textDirection_:I

    .line 18336
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->invisibleInGotoMenu_:Z

    .line 18355
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHintV2_:Ljava/lang/String;

    .line 18396
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyName_:Ljava/lang/String;

    .line 18418
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appName_:Ljava/lang/String;

    .line 18440
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sourceIconId_:Ljava/lang/String;

    .line 18462
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->storeType_:I

    .line 18481
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 18484
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->audioItemCount_:I

    .line 18503
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->videoItemCount_:I

    .line 18522
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->wordCount_:I

    .line 18541
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    .line 18544
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    .line 18547
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->availableInScs_:Z

    .line 18566
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isMetered_:Z

    .line 18585
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->meteredPolicyType_:I

    .line 18604
    iput-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->canUseImagesAsHeaderBackground_:Z

    .line 18623
    iput-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isBookmarkable_:Z

    .line 18642
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalPostUrl_:Ljava/lang/String;

    .line 18664
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalPortraitThumbnails_:I

    .line 18683
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalLandscapeThumbnails_:I

    .line 18702
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 18705
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 18708
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 18711
    iput-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->showTopicTags_:Z

    .line 18730
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    .line 17382
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .locals 5

    .prologue
    .line 18799
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18803
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_0

    .line 18804
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 18806
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-eqz v3, :cond_1

    .line 18807
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    .line 18809
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    if-eqz v3, :cond_2

    .line 18810
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    .line 18812
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_3

    .line 18813
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 18815
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    if-eqz v3, :cond_4

    .line 18816
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    .line 18818
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 18819
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 18820
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    if-ge v2, v3, :cond_6

    .line 18821
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v3, v3, v2

    if-eqz v3, :cond_5

    .line 18822
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v4

    aput-object v4, v3, v2

    .line 18820
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 18800
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 18801
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 18826
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 18827
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    invoke-virtual {v3}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    .line 18829
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_8

    .line 18830
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    invoke-virtual {v3}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    .line 18832
    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v3

    if-lez v3, :cond_a

    .line 18833
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 18834
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v3

    if-ge v2, v3, :cond_a

    .line 18835
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v3, v3, v2

    if-eqz v3, :cond_9

    .line 18836
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v4

    aput-object v4, v3, v2

    .line 18834
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 18840
    .end local v2    # "i":I
    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v3

    if-lez v3, :cond_c

    .line 18841
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 18842
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v3

    if-ge v2, v3, :cond_c

    .line 18843
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v3, v3, v2

    if-eqz v3, :cond_b

    .line 18844
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v4

    aput-object v4, v3, v2

    .line 18842
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 18848
    .end local v2    # "i":I
    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v3, v3

    if-lez v3, :cond_e

    .line 18849
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 18850
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v3, v3

    if-ge v2, v3, :cond_e

    .line 18851
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    aget-object v3, v3, v2

    if-eqz v3, :cond_d

    .line 18852
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    move-result-object v4

    aput-object v4, v3, v2

    .line 18850
    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 18856
    .end local v2    # "i":I
    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    array-length v3, v3

    if-lez v3, :cond_10

    .line 18857
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    .line 18858
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    array-length v3, v3

    if-ge v2, v3, :cond_10

    .line 18859
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    aget-object v3, v3, v2

    if-eqz v3, :cond_f

    .line 18860
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    move-result-object v4

    aput-object v4, v3, v2

    .line 18858
    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 18864
    .end local v2    # "i":I
    :cond_10
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 17378
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 18869
    if-ne p1, p0, :cond_1

    .line 18927
    :cond_0
    :goto_0
    return v1

    .line 18870
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 18871
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 18872
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedPostId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedPostId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18873
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18874
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18875
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18876
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18877
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18878
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18879
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formId_:Ljava/lang/String;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18880
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedFormId_:Ljava/lang/String;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedFormId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18881
    :goto_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formTemplateId_:Ljava/lang/String;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formTemplateId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18882
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18883
    :goto_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->subtitle_:Ljava/lang/String;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->subtitle_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18884
    :goto_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->abstract__:Ljava/lang/String;

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->abstract__:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18885
    :goto_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_12

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_3

    .line 18886
    :goto_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-nez v3, :cond_13

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-nez v3, :cond_3

    .line 18887
    :goto_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    if-nez v3, :cond_14

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    if-nez v3, :cond_3

    .line 18888
    :goto_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->shareUrl_:Ljava/lang/String;

    if-nez v3, :cond_15

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->shareUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18889
    :goto_12
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->created_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->created_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->updated_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->updated_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sortKey_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sortKey_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->deleted_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->deleted_:Z

    if-ne v3, v4, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalCreated_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalCreated_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionType_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_16

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_3

    .line 18896
    :goto_13
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->socialAbstract_:Ljava/lang/String;

    if-nez v3, :cond_17

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->socialAbstract_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18897
    :goto_14
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHint_:Ljava/lang/String;

    if-nez v3, :cond_18

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHint_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18898
    :goto_15
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_19

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18899
    :goto_16
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_1a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18900
    :goto_17
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->textDirection_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->textDirection_:I

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->invisibleInGotoMenu_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->invisibleInGotoMenu_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHintV2_:Ljava/lang/String;

    if-nez v3, :cond_1b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHintV2_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18903
    :goto_18
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    if-nez v3, :cond_1c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    if-nez v3, :cond_3

    .line 18904
    :goto_19
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyName_:Ljava/lang/String;

    if-nez v3, :cond_1d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyName_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18905
    :goto_1a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appName_:Ljava/lang/String;

    if-nez v3, :cond_1e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appName_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18906
    :goto_1b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sourceIconId_:Ljava/lang/String;

    if-nez v3, :cond_1f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sourceIconId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18907
    :goto_1c
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->storeType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->storeType_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 18909
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->audioItemCount_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->audioItemCount_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->videoItemCount_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->videoItemCount_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->wordCount_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->wordCount_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    .line 18913
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    .line 18914
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->availableInScs_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->availableInScs_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isMetered_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isMetered_:Z

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->meteredPolicyType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->meteredPolicyType_:I

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->canUseImagesAsHeaderBackground_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->canUseImagesAsHeaderBackground_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isBookmarkable_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isBookmarkable_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalPostUrl_:Ljava/lang/String;

    if-nez v3, :cond_20

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalPostUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 18920
    :goto_1d
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalPortraitThumbnails_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalPortraitThumbnails_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalLandscapeThumbnails_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalLandscapeThumbnails_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 18923
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 18924
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 18925
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->showTopicTags_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->showTopicTags_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    .line 18927
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0

    .line 18872
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedPostId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedPostId_:Ljava/lang/String;

    .line 18873
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    .line 18874
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedSectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedSectionId_:Ljava/lang/String;

    .line 18875
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    .line 18876
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppId_:Ljava/lang/String;

    .line 18877
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyId_:Ljava/lang/String;

    .line 18878
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 18879
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formId_:Ljava/lang/String;

    .line 18880
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedFormId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedFormId_:Ljava/lang/String;

    .line 18881
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formTemplateId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formTemplateId_:Ljava/lang/String;

    .line 18882
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->title_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->title_:Ljava/lang/String;

    .line 18883
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->subtitle_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->subtitle_:Ljava/lang/String;

    .line 18884
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->abstract__:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->abstract__:Ljava/lang/String;

    .line 18885
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 18886
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_f

    :cond_13
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    .line 18887
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_10

    :cond_14
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    .line 18888
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_11

    :cond_15
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->shareUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->shareUrl_:Ljava/lang/String;

    .line 18889
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_12

    :cond_16
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 18896
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_13

    :cond_17
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->socialAbstract_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->socialAbstract_:Ljava/lang/String;

    .line 18897
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_14

    :cond_18
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHint_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHint_:Ljava/lang/String;

    .line 18898
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_15

    :cond_19
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->languageCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->languageCode_:Ljava/lang/String;

    .line 18899
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_16

    :cond_1a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->translationCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->translationCode_:Ljava/lang/String;

    .line 18900
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_17

    :cond_1b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHintV2_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHintV2_:Ljava/lang/String;

    .line 18903
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_18

    :cond_1c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    .line 18904
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_19

    :cond_1d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyName_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyName_:Ljava/lang/String;

    .line 18905
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1a

    :cond_1e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appName_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appName_:Ljava/lang/String;

    .line 18906
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1b

    :cond_1f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sourceIconId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sourceIconId_:Ljava/lang/String;

    .line 18907
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1c

    .line 18914
    :cond_20
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalPostUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalPostUrl_:Ljava/lang/String;

    .line 18920
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1d
.end method

.method public getAbstract()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17997
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->abstract__:Ljava/lang/String;

    return-object v0
.end method

.method public getAppFamilyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18398
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyName_:Ljava/lang/String;

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18420
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appName_:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioItemCount()I
    .locals 1

    .prologue
    .line 18486
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->audioItemCount_:I

    return v0
.end method

.method public getAuthor()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;
    .locals 1

    .prologue
    .line 18057
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    return-object v0
.end method

.method public getCanUseImagesAsHeaderBackground()Z
    .locals 1

    .prologue
    .line 18606
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->canUseImagesAsHeaderBackground_:Z

    return v0
.end method

.method public getExternalCreated()J
    .locals 2

    .prologue
    .line 18174
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalCreated_:J

    return-wide v0
.end method

.method public getExternalPostUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18644
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalPostUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getFavicon()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 18212
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getFormTemplateId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17931
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formTemplateId_:Ljava/lang/String;

    return-object v0
.end method

.method public getIsMetered()Z
    .locals 1

    .prologue
    .line 18568
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isMetered_:Z

    return v0
.end method

.method public getLanguageCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18275
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->languageCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getMeteredPolicyType()I
    .locals 1

    .prologue
    .line 18587
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->meteredPolicyType_:I

    return v0
.end method

.method public getNativeBodySummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;
    .locals 1

    .prologue
    .line 18379
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    return-object v0
.end method

.method public getNumHorizontalLandscapeThumbnails()I
    .locals 1

    .prologue
    .line 18685
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalLandscapeThumbnails_:I

    return v0
.end method

.method public getNumHorizontalPortraitThumbnails()I
    .locals 1

    .prologue
    .line 18666
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalPortraitThumbnails_:I

    return v0
.end method

.method public getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 18019
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getSectionType()I
    .locals 1

    .prologue
    .line 18193
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionType_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 19219
    const/4 v2, 0x0

    .line 19220
    .local v2, "size":I
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 19221
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19222
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    .line 19223
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19224
    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    .line 19225
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19226
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x100

    if-eqz v4, :cond_0

    .line 19227
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->title_:Ljava/lang/String;

    .line 19228
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19230
    :cond_0
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_1

    .line 19231
    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->subtitle_:Ljava/lang/String;

    .line 19232
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19234
    :cond_1
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x400

    if-eqz v4, :cond_2

    .line 19235
    const/4 v4, 0x6

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->abstract__:Ljava/lang/String;

    .line 19236
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19238
    :cond_2
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v4, :cond_3

    .line 19239
    const/4 v4, 0x7

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 19240
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19242
    :cond_3
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    if-eqz v4, :cond_4

    .line 19243
    const/16 v4, 0x9

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    .line 19244
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19246
    :cond_4
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x800

    if-eqz v4, :cond_5

    .line 19247
    const/16 v4, 0xa

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->shareUrl_:Ljava/lang/String;

    .line 19248
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19250
    :cond_5
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x1000

    if-eqz v4, :cond_6

    .line 19251
    const/16 v4, 0xc

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->created_:J

    .line 19252
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 19254
    :cond_6
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x2000

    if-eqz v4, :cond_7

    .line 19255
    const/16 v4, 0xd

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->updated_:J

    .line 19256
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 19258
    :cond_7
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x4000

    if-eqz v4, :cond_8

    .line 19259
    const/16 v4, 0xe

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sortKey_:J

    .line 19260
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 19262
    :cond_8
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const v5, 0x8000

    and-int/2addr v4, v5

    if-eqz v4, :cond_9

    .line 19263
    const/16 v4, 0xf

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->deleted_:Z

    .line 19264
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 19266
    :cond_9
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x10000

    and-int/2addr v4, v5

    if-eqz v4, :cond_a

    .line 19267
    const/16 v4, 0x10

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalCreated_:J

    .line 19268
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 19270
    :cond_a
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x20000

    and-int/2addr v4, v5

    if-eqz v4, :cond_b

    .line 19271
    const/16 v4, 0x12

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionType_:I

    .line 19272
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 19274
    :cond_b
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v4, :cond_c

    .line 19275
    const/16 v4, 0x13

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 19276
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19278
    :cond_c
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x40000

    and-int/2addr v4, v5

    if-eqz v4, :cond_d

    .line 19279
    const/16 v4, 0x14

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->socialAbstract_:Ljava/lang/String;

    .line 19280
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19282
    :cond_d
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x80000

    and-int/2addr v4, v5

    if-eqz v4, :cond_e

    .line 19283
    const/16 v4, 0x15

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHint_:Ljava/lang/String;

    .line 19284
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19286
    :cond_e
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x100000

    and-int/2addr v4, v5

    if-eqz v4, :cond_f

    .line 19287
    const/16 v4, 0x16

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->languageCode_:Ljava/lang/String;

    .line 19288
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19290
    :cond_f
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x200000

    and-int/2addr v4, v5

    if-eqz v4, :cond_10

    .line 19291
    const/16 v4, 0x17

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->translationCode_:Ljava/lang/String;

    .line 19292
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19294
    :cond_10
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x800000

    and-int/2addr v4, v5

    if-eqz v4, :cond_11

    .line 19295
    const/16 v4, 0x18

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->invisibleInGotoMenu_:Z

    .line 19296
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 19298
    :cond_11
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x1000000

    and-int/2addr v4, v5

    if-eqz v4, :cond_12

    .line 19299
    const/16 v4, 0x19

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHintV2_:Ljava/lang/String;

    .line 19300
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19302
    :cond_12
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    if-eqz v4, :cond_13

    .line 19303
    const/16 v4, 0x1a

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    .line 19304
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19306
    :cond_13
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x2000000

    and-int/2addr v4, v5

    if-eqz v4, :cond_14

    .line 19307
    const/16 v4, 0x1c

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyName_:Ljava/lang/String;

    .line 19308
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19310
    :cond_14
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x4000000

    and-int/2addr v4, v5

    if-eqz v4, :cond_15

    .line 19311
    const/16 v4, 0x1d

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appName_:Ljava/lang/String;

    .line 19312
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19314
    :cond_15
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_16

    .line 19315
    const/16 v4, 0x1e

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyId_:Ljava/lang/String;

    .line 19316
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19318
    :cond_16
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-eqz v4, :cond_17

    .line 19319
    const/16 v4, 0x20

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    .line 19320
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19322
    :cond_17
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v4, :cond_19

    .line 19323
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_19

    aget-object v1, v5, v4

    .line 19324
    .local v1, "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    if-eqz v1, :cond_18

    .line 19325
    const/16 v7, 0x21

    .line 19326
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 19323
    :cond_18
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 19330
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_19
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x8000000

    and-int/2addr v4, v5

    if-eqz v4, :cond_1a

    .line 19331
    const/16 v4, 0x22

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sourceIconId_:Ljava/lang/String;

    .line 19332
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19334
    :cond_1a
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x20000000

    and-int/2addr v4, v5

    if-eqz v4, :cond_1b

    .line 19335
    const/16 v4, 0x23

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->audioItemCount_:I

    .line 19336
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 19338
    :cond_1b
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x40000000    # 2.0f

    and-int/2addr v4, v5

    if-eqz v4, :cond_1c

    .line 19339
    const/16 v4, 0x24

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->videoItemCount_:I

    .line 19340
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 19342
    :cond_1c
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1d

    .line 19343
    const/16 v4, 0x25

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->availableInScs_:Z

    .line 19344
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 19346
    :cond_1d
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, -0x80000000

    and-int/2addr v4, v5

    if-eqz v4, :cond_1e

    .line 19347
    const/16 v4, 0x26

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->wordCount_:I

    .line 19348
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 19350
    :cond_1e
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_1f

    .line 19351
    const/16 v4, 0x27

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formId_:Ljava/lang/String;

    .line 19352
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19354
    :cond_1f
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_20

    .line 19355
    const/16 v4, 0x28

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isMetered_:Z

    .line 19356
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 19358
    :cond_20
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_21

    .line 19359
    const/16 v4, 0x29

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalPostUrl_:Ljava/lang/String;

    .line 19360
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19362
    :cond_21
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_22

    .line 19363
    const/16 v4, 0x2a

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalPortraitThumbnails_:I

    .line 19364
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 19366
    :cond_22
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_23

    .line 19367
    const/16 v4, 0x2b

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalLandscapeThumbnails_:I

    .line 19368
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 19370
    :cond_23
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x10000000

    and-int/2addr v4, v5

    if-eqz v4, :cond_24

    .line 19371
    const/16 v4, 0x2c

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->storeType_:I

    .line 19372
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 19374
    :cond_24
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_25

    .line 19375
    const/16 v4, 0x2d

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formTemplateId_:Ljava/lang/String;

    .line 19376
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19378
    :cond_25
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    if-eqz v4, :cond_27

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_27

    .line 19379
    const/4 v0, 0x0

    .line 19380
    .local v0, "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_26

    aget-object v1, v5, v4

    .line 19382
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 19380
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 19384
    .end local v1    # "element":Ljava/lang/String;
    :cond_26
    add-int/2addr v2, v0

    .line 19385
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    .line 19387
    .end local v0    # "dataSize":I
    :cond_27
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    if-eqz v4, :cond_29

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_29

    .line 19388
    const/4 v0, 0x0

    .line 19389
    .restart local v0    # "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_28

    aget-object v1, v5, v4

    .line 19391
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 19389
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 19393
    .end local v1    # "element":Ljava/lang/String;
    :cond_28
    add-int/2addr v2, v0

    .line 19394
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    .line 19396
    .end local v0    # "dataSize":I
    :cond_29
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v4, :cond_2b

    .line 19397
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v6, v5

    move v4, v3

    :goto_3
    if-ge v4, v6, :cond_2b

    aget-object v1, v5, v4

    .line 19398
    .local v1, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    if-eqz v1, :cond_2a

    .line 19399
    const/16 v7, 0x30

    .line 19400
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 19397
    :cond_2a
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 19404
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_2b
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_2c

    .line 19405
    const/16 v4, 0x31

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->meteredPolicyType_:I

    .line 19406
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 19408
    :cond_2c
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2d

    .line 19409
    const/16 v4, 0x32

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedPostId_:Ljava/lang/String;

    .line 19410
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19412
    :cond_2d
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_2e

    .line 19413
    const/16 v4, 0x33

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedSectionId_:Ljava/lang/String;

    .line 19414
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19416
    :cond_2e
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_2f

    .line 19417
    const/16 v4, 0x34

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppId_:Ljava/lang/String;

    .line 19418
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19420
    :cond_2f
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_30

    .line 19421
    const/16 v4, 0x35

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 19422
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19424
    :cond_30
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_31

    .line 19425
    const/16 v4, 0x36

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedFormId_:Ljava/lang/String;

    .line 19426
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 19428
    :cond_31
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v4, :cond_33

    .line 19429
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v6, v5

    move v4, v3

    :goto_4
    if-ge v4, v6, :cond_33

    aget-object v1, v5, v4

    .line 19430
    .restart local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    if-eqz v1, :cond_32

    .line 19431
    const/16 v7, 0x37

    .line 19432
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 19429
    :cond_32
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 19436
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_33
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit16 v4, v4, 0x100

    if-eqz v4, :cond_34

    .line 19437
    const/16 v4, 0x38

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->showTopicTags_:Z

    .line 19438
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 19440
    :cond_34
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v5, 0x400000

    and-int/2addr v4, v5

    if-eqz v4, :cond_35

    .line 19441
    const/16 v4, 0x39

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->textDirection_:I

    .line 19442
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 19444
    :cond_35
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    if-eqz v4, :cond_37

    .line 19445
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    array-length v6, v5

    move v4, v3

    :goto_5
    if-ge v4, v6, :cond_37

    aget-object v1, v5, v4

    .line 19446
    .local v1, "element":Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    if-eqz v1, :cond_36

    .line 19447
    const/16 v7, 0x3a

    .line 19448
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 19445
    :cond_36
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 19452
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    :cond_37
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-eqz v4, :cond_39

    .line 19453
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v5, v4

    :goto_6
    if-ge v3, v5, :cond_39

    aget-object v1, v4, v3

    .line 19454
    .local v1, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    if-eqz v1, :cond_38

    .line 19455
    const/16 v6, 0x3b

    .line 19456
    invoke-static {v6, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v2, v6

    .line 19453
    :cond_38
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 19460
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    :cond_39
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_3a

    .line 19461
    const/16 v3, 0x3c

    iget-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->canUseImagesAsHeaderBackground_:Z

    .line 19462
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 19464
    :cond_3a
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v3, v3, 0x10

    if-eqz v3, :cond_3b

    .line 19465
    const/16 v3, 0x3d

    iget-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isBookmarkable_:Z

    .line 19466
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 19468
    :cond_3b
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->cachedSize:I

    .line 19469
    return v2
.end method

.method public getShareUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18076
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->shareUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getShowTopicTags()Z
    .locals 1

    .prologue
    .line 18713
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->showTopicTags_:Z

    return v0
.end method

.method public getSourceIconId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18442
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sourceIconId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17975
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->subtitle_:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17953
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdated()J
    .locals 2

    .prologue
    .line 18117
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->updated_:J

    return-wide v0
.end method

.method public hasAppName()Z
    .locals 2

    .prologue
    .line 18431
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v1, 0x4000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAuthor()Z
    .locals 1

    .prologue
    .line 18067
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCanUseImagesAsHeaderBackground()Z
    .locals 1

    .prologue
    .line 18614
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExternalPostUrl()Z
    .locals 1

    .prologue
    .line 18655
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFavicon()Z
    .locals 1

    .prologue
    .line 18222
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFormTemplateId()Z
    .locals 1

    .prologue
    .line 17942
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMeteredPolicyType()Z
    .locals 1

    .prologue
    .line 18595
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNativeBodySummary()Z
    .locals 1

    .prologue
    .line 18389
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrimaryImage()Z
    .locals 1

    .prologue
    .line 18029
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShareUrl()Z
    .locals 1

    .prologue
    .line 18087
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSourceIconId()Z
    .locals 2

    .prologue
    .line 18453
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v1, 0x8000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTitle()Z
    .locals 1

    .prologue
    .line 17964
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const/16 v10, 0x20

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18931
    const/16 v1, 0x11

    .line 18932
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 18933
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 18934
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedPostId_:Ljava/lang/String;

    if-nez v2, :cond_8

    move v2, v3

    :goto_1
    add-int v1, v6, v2

    .line 18935
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    if-nez v2, :cond_9

    move v2, v3

    :goto_2
    add-int v1, v6, v2

    .line 18936
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v2, :cond_a

    move v2, v3

    :goto_3
    add-int v1, v6, v2

    .line 18937
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    if-nez v2, :cond_b

    move v2, v3

    :goto_4
    add-int v1, v6, v2

    .line 18938
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppId_:Ljava/lang/String;

    if-nez v2, :cond_c

    move v2, v3

    :goto_5
    add-int v1, v6, v2

    .line 18939
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyId_:Ljava/lang/String;

    if-nez v2, :cond_d

    move v2, v3

    :goto_6
    add-int v1, v6, v2

    .line 18940
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v2, :cond_e

    move v2, v3

    :goto_7
    add-int v1, v6, v2

    .line 18941
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formId_:Ljava/lang/String;

    if-nez v2, :cond_f

    move v2, v3

    :goto_8
    add-int v1, v6, v2

    .line 18942
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedFormId_:Ljava/lang/String;

    if-nez v2, :cond_10

    move v2, v3

    :goto_9
    add-int v1, v6, v2

    .line 18943
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formTemplateId_:Ljava/lang/String;

    if-nez v2, :cond_11

    move v2, v3

    :goto_a
    add-int v1, v6, v2

    .line 18944
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->title_:Ljava/lang/String;

    if-nez v2, :cond_12

    move v2, v3

    :goto_b
    add-int v1, v6, v2

    .line 18945
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->subtitle_:Ljava/lang/String;

    if-nez v2, :cond_13

    move v2, v3

    :goto_c
    add-int v1, v6, v2

    .line 18946
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->abstract__:Ljava/lang/String;

    if-nez v2, :cond_14

    move v2, v3

    :goto_d
    add-int v1, v6, v2

    .line 18947
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_15

    move v2, v3

    :goto_e
    add-int v1, v6, v2

    .line 18948
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-nez v2, :cond_16

    move v2, v3

    :goto_f
    add-int v1, v6, v2

    .line 18949
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    if-nez v2, :cond_17

    move v2, v3

    :goto_10
    add-int v1, v6, v2

    .line 18950
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->shareUrl_:Ljava/lang/String;

    if-nez v2, :cond_18

    move v2, v3

    :goto_11
    add-int v1, v6, v2

    .line 18951
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->created_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->created_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 18952
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->updated_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->updated_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 18953
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sortKey_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sortKey_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 18954
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->deleted_:Z

    if-eqz v2, :cond_19

    move v2, v4

    :goto_12
    add-int v1, v6, v2

    .line 18955
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalCreated_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalCreated_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 18956
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionType_:I

    add-int v1, v2, v6

    .line 18957
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_1a

    move v2, v3

    :goto_13
    add-int v1, v6, v2

    .line 18958
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->socialAbstract_:Ljava/lang/String;

    if-nez v2, :cond_1b

    move v2, v3

    :goto_14
    add-int v1, v6, v2

    .line 18959
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHint_:Ljava/lang/String;

    if-nez v2, :cond_1c

    move v2, v3

    :goto_15
    add-int v1, v6, v2

    .line 18960
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->languageCode_:Ljava/lang/String;

    if-nez v2, :cond_1d

    move v2, v3

    :goto_16
    add-int v1, v6, v2

    .line 18961
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->translationCode_:Ljava/lang/String;

    if-nez v2, :cond_1e

    move v2, v3

    :goto_17
    add-int v1, v6, v2

    .line 18962
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->textDirection_:I

    add-int v1, v2, v6

    .line 18963
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->invisibleInGotoMenu_:Z

    if-eqz v2, :cond_1f

    move v2, v4

    :goto_18
    add-int v1, v6, v2

    .line 18964
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHintV2_:Ljava/lang/String;

    if-nez v2, :cond_20

    move v2, v3

    :goto_19
    add-int v1, v6, v2

    .line 18965
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    if-nez v2, :cond_21

    move v2, v3

    :goto_1a
    add-int v1, v6, v2

    .line 18966
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyName_:Ljava/lang/String;

    if-nez v2, :cond_22

    move v2, v3

    :goto_1b
    add-int v1, v6, v2

    .line 18967
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appName_:Ljava/lang/String;

    if-nez v2, :cond_23

    move v2, v3

    :goto_1c
    add-int v1, v6, v2

    .line 18968
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sourceIconId_:Ljava/lang/String;

    if-nez v2, :cond_24

    move v2, v3

    :goto_1d
    add-int v1, v6, v2

    .line 18969
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->storeType_:I

    add-int v1, v2, v6

    .line 18970
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v2, :cond_25

    mul-int/lit8 v1, v1, 0x1f

    .line 18976
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->audioItemCount_:I

    add-int v1, v2, v6

    .line 18977
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->videoItemCount_:I

    add-int v1, v2, v6

    .line 18978
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->wordCount_:I

    add-int v1, v2, v6

    .line 18979
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    if-nez v2, :cond_27

    mul-int/lit8 v1, v1, 0x1f

    .line 18985
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    if-nez v2, :cond_29

    mul-int/lit8 v1, v1, 0x1f

    .line 18991
    :cond_2
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->availableInScs_:Z

    if-eqz v2, :cond_2b

    move v2, v4

    :goto_1e
    add-int v1, v6, v2

    .line 18992
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isMetered_:Z

    if-eqz v2, :cond_2c

    move v2, v4

    :goto_1f
    add-int v1, v6, v2

    .line 18993
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->meteredPolicyType_:I

    add-int v1, v2, v6

    .line 18994
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->canUseImagesAsHeaderBackground_:Z

    if-eqz v2, :cond_2d

    move v2, v4

    :goto_20
    add-int v1, v6, v2

    .line 18995
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isBookmarkable_:Z

    if-eqz v2, :cond_2e

    move v2, v4

    :goto_21
    add-int v1, v6, v2

    .line 18996
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalPostUrl_:Ljava/lang/String;

    if-nez v2, :cond_2f

    move v2, v3

    :goto_22
    add-int v1, v6, v2

    .line 18997
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalPortraitThumbnails_:I

    add-int v1, v2, v6

    .line 18998
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalLandscapeThumbnails_:I

    add-int v1, v2, v6

    .line 18999
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_30

    mul-int/lit8 v1, v1, 0x1f

    .line 19005
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_32

    mul-int/lit8 v1, v1, 0x1f

    .line 19011
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-nez v2, :cond_34

    mul-int/lit8 v1, v1, 0x1f

    .line 19017
    :cond_5
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->showTopicTags_:Z

    if-eqz v6, :cond_36

    :goto_23
    add-int v1, v2, v4

    .line 19018
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    if-nez v2, :cond_37

    mul-int/lit8 v1, v1, 0x1f

    .line 19024
    :cond_6
    return v1

    .line 18933
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 18934
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedPostId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 18935
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 18936
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedSectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 18937
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 18938
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 18939
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 18940
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 18941
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 18942
    :cond_10
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedFormId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_9

    .line 18943
    :cond_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formTemplateId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_a

    .line 18944
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->title_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_b

    .line 18945
    :cond_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->subtitle_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_c

    .line 18946
    :cond_14
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->abstract__:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_d

    .line 18947
    :cond_15
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto/16 :goto_e

    .line 18948
    :cond_16
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->hashCode()I

    move-result v2

    goto/16 :goto_f

    .line 18949
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;->hashCode()I

    move-result v2

    goto/16 :goto_10

    .line 18950
    :cond_18
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->shareUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_11

    :cond_19
    move v2, v5

    .line 18954
    goto/16 :goto_12

    .line 18957
    :cond_1a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto/16 :goto_13

    .line 18958
    :cond_1b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->socialAbstract_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_14

    .line 18959
    :cond_1c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHint_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_15

    .line 18960
    :cond_1d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->languageCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_16

    .line 18961
    :cond_1e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->translationCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_17

    :cond_1f
    move v2, v5

    .line 18963
    goto/16 :goto_18

    .line 18964
    :cond_20
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHintV2_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_19

    .line 18965
    :cond_21
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->hashCode()I

    move-result v2

    goto/16 :goto_1a

    .line 18966
    :cond_22
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyName_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1b

    .line 18967
    :cond_23
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appName_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1c

    .line 18968
    :cond_24
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sourceIconId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1d

    .line 18972
    :cond_25
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_24
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 18973
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v2, v2, v0

    if-nez v2, :cond_26

    move v2, v3

    :goto_25
    add-int v1, v6, v2

    .line 18972
    add-int/lit8 v0, v0, 0x1

    goto :goto_24

    .line 18973
    :cond_26
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->hashCode()I

    move-result v2

    goto :goto_25

    .line 18981
    .end local v0    # "i":I
    :cond_27
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_26
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 18982
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_28

    move v2, v3

    :goto_27
    add-int v1, v6, v2

    .line 18981
    add-int/lit8 v0, v0, 0x1

    goto :goto_26

    .line 18982
    :cond_28
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_27

    .line 18987
    .end local v0    # "i":I
    :cond_29
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_28
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 18988
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_2a

    move v2, v3

    :goto_29
    add-int v1, v6, v2

    .line 18987
    add-int/lit8 v0, v0, 0x1

    goto :goto_28

    .line 18988
    :cond_2a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_29

    .end local v0    # "i":I
    :cond_2b
    move v2, v5

    .line 18991
    goto/16 :goto_1e

    :cond_2c
    move v2, v5

    .line 18992
    goto/16 :goto_1f

    :cond_2d
    move v2, v5

    .line 18994
    goto/16 :goto_20

    :cond_2e
    move v2, v5

    .line 18995
    goto/16 :goto_21

    .line 18996
    :cond_2f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalPostUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_22

    .line 19001
    :cond_30
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 19002
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v2, v2, v0

    if-nez v2, :cond_31

    move v2, v3

    :goto_2b
    add-int v1, v6, v2

    .line 19001
    add-int/lit8 v0, v0, 0x1

    goto :goto_2a

    .line 19002
    :cond_31
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto :goto_2b

    .line 19007
    .end local v0    # "i":I
    :cond_32
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 19008
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v2, v2, v0

    if-nez v2, :cond_33

    move v2, v3

    :goto_2d
    add-int v1, v6, v2

    .line 19007
    add-int/lit8 v0, v0, 0x1

    goto :goto_2c

    .line 19008
    :cond_33
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto :goto_2d

    .line 19013
    .end local v0    # "i":I
    :cond_34
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 19014
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    aget-object v2, v2, v0

    if-nez v2, :cond_35

    move v2, v3

    :goto_2f
    add-int v1, v6, v2

    .line 19013
    add-int/lit8 v0, v0, 0x1

    goto :goto_2e

    .line 19014
    :cond_35
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->hashCode()I

    move-result v2

    goto :goto_2f

    .end local v0    # "i":I
    :cond_36
    move v4, v5

    .line 19017
    goto/16 :goto_23

    .line 19020
    :cond_37
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_30
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 19021
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    aget-object v2, v2, v0

    if-nez v2, :cond_38

    move v2, v3

    :goto_31
    add-int v1, v4, v2

    .line 19020
    add-int/lit8 v0, v0, 0x1

    goto :goto_30

    .line 19021
    :cond_38
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;->hashCode()I

    move-result v2

    goto :goto_31
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .locals 10
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 19477
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 19478
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 19482
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 19483
    :sswitch_0
    return-object p0

    .line 19488
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    goto :goto_0

    .line 19492
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    goto :goto_0

    .line 19496
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    goto :goto_0

    .line 19500
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->title_:Ljava/lang/String;

    .line 19501
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit16 v6, v6, 0x100

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto :goto_0

    .line 19505
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->subtitle_:Ljava/lang/String;

    .line 19506
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit16 v6, v6, 0x200

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto :goto_0

    .line 19510
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->abstract__:Ljava/lang/String;

    .line 19511
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit16 v6, v6, 0x400

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto :goto_0

    .line 19515
    :sswitch_7
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v6, :cond_1

    .line 19516
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 19518
    :cond_1
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 19522
    :sswitch_8
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    if-nez v6, :cond_2

    .line 19523
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    .line 19525
    :cond_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 19529
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->shareUrl_:Ljava/lang/String;

    .line 19530
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit16 v6, v6, 0x800

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto :goto_0

    .line 19534
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->created_:J

    .line 19535
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit16 v6, v6, 0x1000

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19539
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->updated_:J

    .line 19540
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit16 v6, v6, 0x2000

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19544
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sortKey_:J

    .line 19545
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit16 v6, v6, 0x4000

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19549
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->deleted_:Z

    .line 19550
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const v7, 0x8000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19554
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalCreated_:J

    .line 19555
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x10000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19559
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 19560
    .local v4, "temp":I
    if-eqz v4, :cond_3

    if-eq v4, v8, :cond_3

    if-eq v4, v9, :cond_3

    const/4 v6, 0x5

    if-eq v4, v6, :cond_3

    const/4 v6, 0x6

    if-eq v4, v6, :cond_3

    const/4 v6, 0x7

    if-eq v4, v6, :cond_3

    const/16 v6, 0x8

    if-eq v4, v6, :cond_3

    const/16 v6, 0x9

    if-eq v4, v6, :cond_3

    const/16 v6, 0xa

    if-eq v4, v6, :cond_3

    const/16 v6, 0xb

    if-eq v4, v6, :cond_3

    const/16 v6, 0xc

    if-ne v4, v6, :cond_4

    .line 19571
    :cond_3
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionType_:I

    .line 19572
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x20000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19574
    :cond_4
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionType_:I

    goto/16 :goto_0

    .line 19579
    .end local v4    # "temp":I
    :sswitch_10
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v6, :cond_5

    .line 19580
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 19582
    :cond_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 19586
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->socialAbstract_:Ljava/lang/String;

    .line 19587
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x40000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19591
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHint_:Ljava/lang/String;

    .line 19592
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x80000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19596
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->languageCode_:Ljava/lang/String;

    .line 19597
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x100000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19601
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->translationCode_:Ljava/lang/String;

    .line 19602
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x200000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19606
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->invisibleInGotoMenu_:Z

    .line 19607
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x800000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19611
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHintV2_:Ljava/lang/String;

    .line 19612
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x1000000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19616
    :sswitch_17
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    if-nez v6, :cond_6

    .line 19617
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    .line 19619
    :cond_6
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 19623
    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyName_:Ljava/lang/String;

    .line 19624
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x2000000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19628
    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appName_:Ljava/lang/String;

    .line 19629
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x4000000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19633
    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyId_:Ljava/lang/String;

    .line 19634
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19638
    :sswitch_1b
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-nez v6, :cond_7

    .line 19639
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    .line 19641
    :cond_7
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 19645
    :sswitch_1c
    const/16 v6, 0x10a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 19646
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v6, :cond_9

    move v1, v5

    .line 19647
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 19648
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v6, :cond_8

    .line 19649
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 19651
    :cond_8
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 19652
    :goto_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_a

    .line 19653
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    aput-object v7, v6, v1

    .line 19654
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 19655
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 19652
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 19646
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_9
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v1, v6

    goto :goto_1

    .line 19658
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_a
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    aput-object v7, v6, v1

    .line 19659
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 19663
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sourceIconId_:Ljava/lang/String;

    .line 19664
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x8000000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19668
    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->audioItemCount_:I

    .line 19669
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x20000000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19673
    :sswitch_1f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->videoItemCount_:I

    .line 19674
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x40000000    # 2.0f

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19678
    :sswitch_20
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->availableInScs_:Z

    .line 19679
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    goto/16 :goto_0

    .line 19683
    :sswitch_21
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->wordCount_:I

    .line 19684
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, -0x80000000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19688
    :sswitch_22
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formId_:Ljava/lang/String;

    .line 19689
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19693
    :sswitch_23
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isMetered_:Z

    .line 19694
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    goto/16 :goto_0

    .line 19698
    :sswitch_24
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalPostUrl_:Ljava/lang/String;

    .line 19699
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    goto/16 :goto_0

    .line 19703
    :sswitch_25
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalPortraitThumbnails_:I

    .line 19704
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    goto/16 :goto_0

    .line 19708
    :sswitch_26
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalLandscapeThumbnails_:I

    .line 19709
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    goto/16 :goto_0

    .line 19713
    :sswitch_27
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 19714
    .restart local v4    # "temp":I
    if-eqz v4, :cond_b

    if-eq v4, v8, :cond_b

    if-ne v4, v9, :cond_c

    .line 19717
    :cond_b
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->storeType_:I

    .line 19718
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x10000000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19720
    :cond_c
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->storeType_:I

    goto/16 :goto_0

    .line 19725
    .end local v4    # "temp":I
    :sswitch_28
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formTemplateId_:Ljava/lang/String;

    .line 19726
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19730
    :sswitch_29
    const/16 v6, 0x172

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 19731
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    array-length v1, v6

    .line 19732
    .restart local v1    # "i":I
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 19733
    .local v2, "newArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 19734
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    .line 19735
    :goto_3
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_d

    .line 19736
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 19737
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 19735
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 19740
    :cond_d
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    goto/16 :goto_0

    .line 19744
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_2a
    const/16 v6, 0x17a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 19745
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    array-length v1, v6

    .line 19746
    .restart local v1    # "i":I
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 19747
    .restart local v2    # "newArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 19748
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    .line 19749
    :goto_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_e

    .line 19750
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 19751
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 19749
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 19754
    :cond_e
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    goto/16 :goto_0

    .line 19758
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_2b
    const/16 v6, 0x182

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 19759
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v6, :cond_10

    move v1, v5

    .line 19760
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 19761
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v6, :cond_f

    .line 19762
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 19764
    :cond_f
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 19765
    :goto_6
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_11

    .line 19766
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    aput-object v7, v6, v1

    .line 19767
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 19768
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 19765
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 19759
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_10
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v1, v6

    goto :goto_5

    .line 19771
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_11
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    aput-object v7, v6, v1

    .line 19772
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 19776
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :sswitch_2c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 19777
    .restart local v4    # "temp":I
    if-eqz v4, :cond_12

    if-ne v4, v8, :cond_13

    .line 19779
    :cond_12
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->meteredPolicyType_:I

    .line 19780
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    goto/16 :goto_0

    .line 19782
    :cond_13
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->meteredPolicyType_:I

    goto/16 :goto_0

    .line 19787
    .end local v4    # "temp":I
    :sswitch_2d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedPostId_:Ljava/lang/String;

    .line 19788
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19792
    :sswitch_2e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedSectionId_:Ljava/lang/String;

    .line 19793
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19797
    :sswitch_2f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppId_:Ljava/lang/String;

    .line 19798
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19802
    :sswitch_30
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 19803
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19807
    :sswitch_31
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedFormId_:Ljava/lang/String;

    .line 19808
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19812
    :sswitch_32
    const/16 v6, 0x1ba

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 19813
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v6, :cond_15

    move v1, v5

    .line 19814
    .restart local v1    # "i":I
    :goto_7
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 19815
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v6, :cond_14

    .line 19816
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 19818
    :cond_14
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 19819
    :goto_8
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_16

    .line 19820
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    aput-object v7, v6, v1

    .line 19821
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 19822
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 19819
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 19813
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_15
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v1, v6

    goto :goto_7

    .line 19825
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_16
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    aput-object v7, v6, v1

    .line 19826
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 19830
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :sswitch_33
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->showTopicTags_:Z

    .line 19831
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    or-int/lit16 v6, v6, 0x100

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    goto/16 :goto_0

    .line 19835
    :sswitch_34
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 19836
    .restart local v4    # "temp":I
    if-eqz v4, :cond_17

    if-eq v4, v8, :cond_17

    if-ne v4, v9, :cond_18

    .line 19839
    :cond_17
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->textDirection_:I

    .line 19840
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v7, 0x400000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    goto/16 :goto_0

    .line 19842
    :cond_18
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->textDirection_:I

    goto/16 :goto_0

    .line 19847
    .end local v4    # "temp":I
    :sswitch_35
    const/16 v6, 0x1d2

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 19848
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    if-nez v6, :cond_1a

    move v1, v5

    .line 19849
    .restart local v1    # "i":I
    :goto_9
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    .line 19850
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    if-eqz v6, :cond_19

    .line 19851
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 19853
    :cond_19
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    .line 19854
    :goto_a
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_1b

    .line 19855
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;-><init>()V

    aput-object v7, v6, v1

    .line 19856
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 19857
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 19854
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 19848
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    :cond_1a
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    array-length v1, v6

    goto :goto_9

    .line 19860
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    :cond_1b
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;-><init>()V

    aput-object v7, v6, v1

    .line 19861
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 19865
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    :sswitch_36
    const/16 v6, 0x1da

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 19866
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-nez v6, :cond_1d

    move v1, v5

    .line 19867
    .restart local v1    # "i":I
    :goto_b
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 19868
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-eqz v6, :cond_1c

    .line 19869
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 19871
    :cond_1c
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 19872
    :goto_c
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_1e

    .line 19873
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;-><init>()V

    aput-object v7, v6, v1

    .line 19874
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 19875
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 19872
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 19866
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    :cond_1d
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v1, v6

    goto :goto_b

    .line 19878
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    :cond_1e
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;-><init>()V

    aput-object v7, v6, v1

    .line 19879
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 19883
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    :sswitch_37
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->canUseImagesAsHeaderBackground_:Z

    .line 19884
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    goto/16 :goto_0

    .line 19888
    :sswitch_38
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isBookmarkable_:Z

    .line 19889
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    goto/16 :goto_0

    .line 19478
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x60 -> :sswitch_a
        0x68 -> :sswitch_b
        0x70 -> :sswitch_c
        0x78 -> :sswitch_d
        0x80 -> :sswitch_e
        0x90 -> :sswitch_f
        0x9a -> :sswitch_10
        0xa2 -> :sswitch_11
        0xaa -> :sswitch_12
        0xb2 -> :sswitch_13
        0xba -> :sswitch_14
        0xc0 -> :sswitch_15
        0xca -> :sswitch_16
        0xd2 -> :sswitch_17
        0xe2 -> :sswitch_18
        0xea -> :sswitch_19
        0xf2 -> :sswitch_1a
        0x102 -> :sswitch_1b
        0x10a -> :sswitch_1c
        0x112 -> :sswitch_1d
        0x118 -> :sswitch_1e
        0x120 -> :sswitch_1f
        0x128 -> :sswitch_20
        0x130 -> :sswitch_21
        0x13a -> :sswitch_22
        0x140 -> :sswitch_23
        0x14a -> :sswitch_24
        0x150 -> :sswitch_25
        0x158 -> :sswitch_26
        0x160 -> :sswitch_27
        0x16a -> :sswitch_28
        0x172 -> :sswitch_29
        0x17a -> :sswitch_2a
        0x182 -> :sswitch_2b
        0x188 -> :sswitch_2c
        0x192 -> :sswitch_2d
        0x19a -> :sswitch_2e
        0x1a2 -> :sswitch_2f
        0x1aa -> :sswitch_30
        0x1b2 -> :sswitch_31
        0x1ba -> :sswitch_32
        0x1c0 -> :sswitch_33
        0x1c8 -> :sswitch_34
        0x1d2 -> :sswitch_35
        0x1da -> :sswitch_36
        0x1e0 -> :sswitch_37
        0x1e8 -> :sswitch_38
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17378
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 19029
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19030
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19031
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19032
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_0

    .line 19033
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->title_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19035
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_1

    .line 19036
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->subtitle_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19038
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_2

    .line 19039
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->abstract__:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19041
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_3

    .line 19042
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 19044
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    if-eqz v2, :cond_4

    .line 19045
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->author_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 19047
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x800

    if-eqz v2, :cond_5

    .line 19048
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->shareUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19050
    :cond_5
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x1000

    if-eqz v2, :cond_6

    .line 19051
    const/16 v2, 0xc

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->created_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 19053
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x2000

    if-eqz v2, :cond_7

    .line 19054
    const/16 v2, 0xd

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->updated_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 19056
    :cond_7
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x4000

    if-eqz v2, :cond_8

    .line 19057
    const/16 v2, 0xe

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sortKey_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 19059
    :cond_8
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const v3, 0x8000

    and-int/2addr v2, v3

    if-eqz v2, :cond_9

    .line 19060
    const/16 v2, 0xf

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->deleted_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 19062
    :cond_9
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    if-eqz v2, :cond_a

    .line 19063
    const/16 v2, 0x10

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalCreated_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 19065
    :cond_a
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    if-eqz v2, :cond_b

    .line 19066
    const/16 v2, 0x12

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionType_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 19068
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_c

    .line 19069
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->favicon_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 19071
    :cond_c
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    if-eqz v2, :cond_d

    .line 19072
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->socialAbstract_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19074
    :cond_d
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    if-eqz v2, :cond_e

    .line 19075
    const/16 v2, 0x15

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHint_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19077
    :cond_e
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    if-eqz v2, :cond_f

    .line 19078
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->languageCode_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19080
    :cond_f
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x200000

    and-int/2addr v2, v3

    if-eqz v2, :cond_10

    .line 19081
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->translationCode_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19083
    :cond_10
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x800000

    and-int/2addr v2, v3

    if-eqz v2, :cond_11

    .line 19084
    const/16 v2, 0x18

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->invisibleInGotoMenu_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 19086
    :cond_11
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x1000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_12

    .line 19087
    const/16 v2, 0x19

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->renderingHintV2_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19089
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    if-eqz v2, :cond_13

    .line 19090
    const/16 v2, 0x1a

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->nativeBodySummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 19092
    :cond_13
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x2000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_14

    .line 19093
    const/16 v2, 0x1c

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyName_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19095
    :cond_14
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x4000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_15

    .line 19096
    const/16 v2, 0x1d

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appName_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19098
    :cond_15
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_16

    .line 19099
    const/16 v2, 0x1e

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19101
    :cond_16
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-eqz v2, :cond_17

    .line 19102
    const/16 v2, 0x20

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->primaryVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 19104
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v2, :cond_19

    .line 19105
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_19

    aget-object v0, v3, v2

    .line 19106
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    if-eqz v0, :cond_18

    .line 19107
    const/16 v5, 0x21

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 19105
    :cond_18
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 19111
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_19
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x8000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_1a

    .line 19112
    const/16 v2, 0x22

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sourceIconId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19114
    :cond_1a
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x20000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_1b

    .line 19115
    const/16 v2, 0x23

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->audioItemCount_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 19117
    :cond_1b
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x40000000    # 2.0f

    and-int/2addr v2, v3

    if-eqz v2, :cond_1c

    .line 19118
    const/16 v2, 0x24

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->videoItemCount_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 19120
    :cond_1c
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1d

    .line 19121
    const/16 v2, 0x25

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->availableInScs_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 19123
    :cond_1d
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, -0x80000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_1e

    .line 19124
    const/16 v2, 0x26

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->wordCount_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 19126
    :cond_1e
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_1f

    .line 19127
    const/16 v2, 0x27

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19129
    :cond_1f
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_20

    .line 19130
    const/16 v2, 0x28

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isMetered_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 19132
    :cond_20
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_21

    .line 19133
    const/16 v2, 0x29

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->externalPostUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19135
    :cond_21
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_22

    .line 19136
    const/16 v2, 0x2a

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalPortraitThumbnails_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 19138
    :cond_22
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_23

    .line 19139
    const/16 v2, 0x2b

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->numHorizontalLandscapeThumbnails_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 19141
    :cond_23
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x10000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_24

    .line 19142
    const/16 v2, 0x2c

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->storeType_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 19144
    :cond_24
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_25

    .line 19145
    const/16 v2, 0x2d

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->formTemplateId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19147
    :cond_25
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    if-eqz v2, :cond_26

    .line 19148
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->allowedCountry:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_26

    aget-object v0, v3, v2

    .line 19149
    .local v0, "element":Ljava/lang/String;
    const/16 v5, 0x2e

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19148
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 19152
    .end local v0    # "element":Ljava/lang/String;
    :cond_26
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    if-eqz v2, :cond_27

    .line 19153
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->blockedCountry:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_27

    aget-object v0, v3, v2

    .line 19154
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v5, 0x2f

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19153
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 19157
    .end local v0    # "element":Ljava/lang/String;
    :cond_27
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_29

    .line 19158
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_29

    aget-object v0, v3, v2

    .line 19159
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    if-eqz v0, :cond_28

    .line 19160
    const/16 v5, 0x30

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 19158
    :cond_28
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 19164
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_29
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2a

    .line 19165
    const/16 v2, 0x31

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->meteredPolicyType_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 19167
    :cond_2a
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2b

    .line 19168
    const/16 v2, 0x32

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedPostId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19170
    :cond_2b
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_2c

    .line 19171
    const/16 v2, 0x33

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedSectionId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19173
    :cond_2c
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2d

    .line 19174
    const/16 v2, 0x34

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19176
    :cond_2d
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_2e

    .line 19177
    const/16 v2, 0x35

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19179
    :cond_2e
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_2f

    .line 19180
    const/16 v2, 0x36

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->untranslatedFormId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 19182
    :cond_2f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_31

    .line 19183
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_31

    aget-object v0, v3, v2

    .line 19184
    .restart local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    if-eqz v0, :cond_30

    .line 19185
    const/16 v5, 0x37

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 19183
    :cond_30
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 19189
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_31
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_32

    .line 19190
    const/16 v2, 0x38

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->showTopicTags_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 19192
    :cond_32
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField0_:I

    const/high16 v3, 0x400000

    and-int/2addr v2, v3

    if-eqz v2, :cond_33

    .line 19193
    const/16 v2, 0x39

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->textDirection_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 19195
    :cond_33
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    if-eqz v2, :cond_35

    .line 19196
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scoredCountry:[Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;

    array-length v4, v3

    move v2, v1

    :goto_5
    if-ge v2, v4, :cond_35

    aget-object v0, v3, v2

    .line 19197
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    if-eqz v0, :cond_34

    .line 19198
    const/16 v5, 0x3a

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 19196
    :cond_34
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 19202
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;
    :cond_35
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-eqz v2, :cond_37

    .line 19203
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v3, v2

    :goto_6
    if-ge v1, v3, :cond_37

    aget-object v0, v2, v1

    .line 19204
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    if-eqz v0, :cond_36

    .line 19205
    const/16 v4, 0x3b

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 19203
    :cond_36
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 19209
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    :cond_37
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_38

    .line 19210
    const/16 v1, 0x3c

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->canUseImagesAsHeaderBackground_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 19212
    :cond_38
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->bitField1_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_39

    .line 19213
    const/16 v1, 0x3d

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->isBookmarkable_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 19215
    :cond_39
    return-void
.end method
