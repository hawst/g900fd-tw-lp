.class public final Lcom/google/apps/dots/proto/client/DotsShared$Form;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Form"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Form;


# instance fields
.field public appId:Ljava/lang/String;

.field private bitField0_:I

.field private created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

.field private defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

.field public field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

.field public formId:Ljava/lang/String;

.field private languageCode_:Ljava/lang/String;

.field private postTemplateCss_:Ljava/lang/String;

.field private postTemplateId_:Ljava/lang/String;

.field private postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

.field private sectionId_:Ljava/lang/String;

.field private storedInScs_:Z

.field private translationCode_:Ljava/lang/String;

.field private untranslatedAppId_:Ljava/lang/String;

.field private untranslatedFormId_:Ljava/lang/String;

.field private untranslatedSectionId_:Ljava/lang/String;

.field private updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9703
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Form;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Form;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9704
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 10152
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    .line 10155
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedFormId_:Ljava/lang/String;

    .line 10177
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->appId:Ljava/lang/String;

    .line 10180
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedAppId_:Ljava/lang/String;

    .line 10202
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->sectionId_:Ljava/lang/String;

    .line 10224
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedSectionId_:Ljava/lang/String;

    .line 10284
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    .line 10326
    const-string v0, "article_default"

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateId_:Ljava/lang/String;

    .line 10348
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateCss_:Ljava/lang/String;

    .line 10370
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->languageCode_:Ljava/lang/String;

    .line 10392
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->translationCode_:Ljava/lang/String;

    .line 10414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->storedInScs_:Z

    .line 9704
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Form;
    .locals 5

    .prologue
    .line 10458
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10462
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Form;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v3, :cond_0

    .line 10463
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->clone()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 10465
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v3, :cond_1

    .line 10466
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->clone()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 10468
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 10469
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    .line 10470
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    array-length v3, v3

    if-ge v2, v3, :cond_3

    .line 10471
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    aget-object v3, v3, v2

    if-eqz v3, :cond_2

    .line 10472
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    move-result-object v4

    aput-object v4, v3, v2

    .line 10470
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 10459
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Form;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 10460
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 10476
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Form;
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v3, :cond_4

    .line 10477
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10479
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v3, :cond_5

    .line 10480
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10482
    :cond_5
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 9700
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Form;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 10487
    if-ne p1, p0, :cond_1

    .line 10504
    :cond_0
    :goto_0
    return v1

    .line 10488
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 10489
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .line 10490
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Form;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedFormId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedFormId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 10491
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->appId:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->appId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 10492
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 10493
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->sectionId_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->sectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 10494
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 10495
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_3

    .line 10496
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_3

    .line 10497
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    .line 10498
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_3

    .line 10499
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_3

    .line 10500
    :goto_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateId_:Ljava/lang/String;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 10501
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateCss_:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateCss_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 10502
    :goto_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 10503
    :goto_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 10504
    :goto_e
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->storedInScs_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->storedInScs_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0

    .line 10490
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedFormId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedFormId_:Ljava/lang/String;

    .line 10491
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->appId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->appId:Ljava/lang/String;

    .line 10492
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedAppId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedAppId_:Ljava/lang/String;

    .line 10493
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->sectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->sectionId_:Ljava/lang/String;

    .line 10494
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedSectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedSectionId_:Ljava/lang/String;

    .line 10495
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 10496
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 10497
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    .line 10498
    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10499
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10500
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateId_:Ljava/lang/String;

    .line 10501
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateCss_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateCss_:Ljava/lang/String;

    .line 10502
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->languageCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->languageCode_:Ljava/lang/String;

    .line 10503
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->translationCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->translationCode_:Ljava/lang/String;

    .line 10504
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_e
.end method

.method public getPostTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
    .locals 1

    .prologue
    .line 10289
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    return-object v0
.end method

.method public getPostTemplateCss()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10350
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateCss_:Ljava/lang/String;

    return-object v0
.end method

.method public getPostTemplateId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10328
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 10589
    const/4 v1, 0x0

    .line 10590
    .local v1, "size":I
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    .line 10591
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10592
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->appId:Ljava/lang/String;

    .line 10593
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10594
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_0

    .line 10595
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 10596
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10598
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_1

    .line 10599
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 10600
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10602
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    if-eqz v2, :cond_3

    .line 10603
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 10604
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    if-eqz v0, :cond_2

    .line 10605
    const/16 v5, 0x9

    .line 10606
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 10603
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 10610
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_4

    .line 10611
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->sectionId_:Ljava/lang/String;

    .line 10612
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10614
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v2, :cond_5

    .line 10615
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10616
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10618
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v2, :cond_6

    .line 10619
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10620
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10622
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_7

    .line 10623
    const/16 v2, 0x15

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->languageCode_:Ljava/lang/String;

    .line 10624
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10626
    :cond_7
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_8

    .line 10627
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->translationCode_:Ljava/lang/String;

    .line 10628
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10630
    :cond_8
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_9

    .line 10631
    const/16 v2, 0x17

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->storedInScs_:Z

    .line 10632
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 10634
    :cond_9
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_a

    .line 10635
    const/16 v2, 0x18

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateId_:Ljava/lang/String;

    .line 10636
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10638
    :cond_a
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_b

    .line 10639
    const/16 v2, 0x19

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateCss_:Ljava/lang/String;

    .line 10640
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10642
    :cond_b
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_c

    .line 10643
    const/16 v2, 0x1a

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedFormId_:Ljava/lang/String;

    .line 10644
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10646
    :cond_c
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_d

    .line 10647
    const/16 v2, 0x1b

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedAppId_:Ljava/lang/String;

    .line 10648
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10650
    :cond_d
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_e

    .line 10651
    const/16 v2, 0x1c

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedSectionId_:Ljava/lang/String;

    .line 10652
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10654
    :cond_e
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->cachedSize:I

    .line 10655
    return v1
.end method

.method public getUpdated()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;
    .locals 1

    .prologue
    .line 10267
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    return-object v0
.end method

.method public hasPostTemplate()Z
    .locals 1

    .prologue
    .line 10299
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPostTemplateId()Z
    .locals 1

    .prologue
    .line 10339
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 10509
    const/16 v1, 0x11

    .line 10510
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 10511
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 10512
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedFormId_:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 10513
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->appId:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 10514
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedAppId_:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 10515
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->sectionId_:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 10516
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 10517
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v2, :cond_7

    move v2, v3

    :goto_6
    add-int v1, v4, v2

    .line 10518
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v2, :cond_8

    move v2, v3

    :goto_7
    add-int v1, v4, v2

    .line 10519
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    if-nez v2, :cond_9

    mul-int/lit8 v1, v1, 0x1f

    .line 10525
    :cond_0
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v2, :cond_b

    move v2, v3

    :goto_8
    add-int v1, v4, v2

    .line 10526
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v2, :cond_c

    move v2, v3

    :goto_9
    add-int v1, v4, v2

    .line 10527
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateId_:Ljava/lang/String;

    if-nez v2, :cond_d

    move v2, v3

    :goto_a
    add-int v1, v4, v2

    .line 10528
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateCss_:Ljava/lang/String;

    if-nez v2, :cond_e

    move v2, v3

    :goto_b
    add-int v1, v4, v2

    .line 10529
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->languageCode_:Ljava/lang/String;

    if-nez v2, :cond_f

    move v2, v3

    :goto_c
    add-int v1, v4, v2

    .line 10530
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->translationCode_:Ljava/lang/String;

    if-nez v4, :cond_10

    :goto_d
    add-int v1, v2, v3

    .line 10531
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->storedInScs_:Z

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    :goto_e
    add-int v1, v3, v2

    .line 10532
    return v1

    .line 10511
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 10512
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedFormId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 10513
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->appId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 10514
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 10515
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->sectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 10516
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedSectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 10517
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 10518
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 10521
    :cond_9
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 10522
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    aget-object v2, v2, v0

    if-nez v2, :cond_a

    move v2, v3

    :goto_10
    add-int v1, v4, v2

    .line 10521
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 10522
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->hashCode()I

    move-result v2

    goto :goto_10

    .line 10525
    .end local v0    # "i":I
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 10526
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->hashCode()I

    move-result v2

    goto/16 :goto_9

    .line 10527
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_a

    .line 10528
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateCss_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_b

    .line 10529
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->languageCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_c

    .line 10530
    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->translationCode_:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto/16 :goto_d

    .line 10531
    :cond_11
    const/4 v2, 0x2

    goto/16 :goto_e
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Form;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 10663
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 10664
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 10668
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 10669
    :sswitch_0
    return-object p0

    .line 10674
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    goto :goto_0

    .line 10678
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->appId:Ljava/lang/String;

    goto :goto_0

    .line 10682
    :sswitch_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v5, :cond_1

    .line 10683
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 10685
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 10689
    :sswitch_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v5, :cond_2

    .line 10690
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 10692
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 10696
    :sswitch_5
    const/16 v5, 0x4a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 10697
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    if-nez v5, :cond_4

    move v1, v4

    .line 10698
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    .line 10699
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    if-eqz v5, :cond_3

    .line 10700
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 10702
    :cond_3
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    .line 10703
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 10704
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;-><init>()V

    aput-object v6, v5, v1

    .line 10705
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 10706
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 10703
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 10697
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    array-length v1, v5

    goto :goto_1

    .line 10709
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    :cond_5
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;-><init>()V

    aput-object v6, v5, v1

    .line 10710
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 10714
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->sectionId_:Ljava/lang/String;

    .line 10715
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    goto/16 :goto_0

    .line 10719
    :sswitch_7
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v5, :cond_6

    .line 10720
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10722
    :cond_6
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 10726
    :sswitch_8
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v5, :cond_7

    .line 10727
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10729
    :cond_7
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 10733
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->languageCode_:Ljava/lang/String;

    .line 10734
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    goto/16 :goto_0

    .line 10738
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->translationCode_:Ljava/lang/String;

    .line 10739
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    goto/16 :goto_0

    .line 10743
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->storedInScs_:Z

    .line 10744
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    goto/16 :goto_0

    .line 10748
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateId_:Ljava/lang/String;

    .line 10749
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    goto/16 :goto_0

    .line 10753
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateCss_:Ljava/lang/String;

    .line 10754
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    goto/16 :goto_0

    .line 10758
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedFormId_:Ljava/lang/String;

    .line 10759
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    goto/16 :goto_0

    .line 10763
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedAppId_:Ljava/lang/String;

    .line 10764
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    goto/16 :goto_0

    .line 10768
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedSectionId_:Ljava/lang/String;

    .line 10769
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    goto/16 :goto_0

    .line 10664
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x4a -> :sswitch_5
        0x7a -> :sswitch_6
        0x92 -> :sswitch_7
        0xa2 -> :sswitch_8
        0xaa -> :sswitch_9
        0xb2 -> :sswitch_a
        0xb8 -> :sswitch_b
        0xc2 -> :sswitch_c
        0xca -> :sswitch_d
        0xd2 -> :sswitch_e
        0xda -> :sswitch_f
        0xe2 -> :sswitch_10
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9700
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Form;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 10537
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 10538
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->appId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 10539
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v1, :cond_0

    .line 10540
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 10542
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v1, :cond_1

    .line 10543
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 10545
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    if-eqz v1, :cond_3

    .line 10546
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v0, v2, v1

    .line 10547
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    if-eqz v0, :cond_2

    .line 10548
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 10546
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 10552
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_4

    .line 10553
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->sectionId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 10555
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v1, :cond_5

    .line 10556
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 10558
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v1, :cond_6

    .line 10559
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->defaultPostTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 10561
    :cond_6
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_7

    .line 10562
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->languageCode_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 10564
    :cond_7
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_8

    .line 10565
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->translationCode_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 10567
    :cond_8
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_9

    .line 10568
    const/16 v1, 0x17

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->storedInScs_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 10570
    :cond_9
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_a

    .line 10571
    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 10573
    :cond_a
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_b

    .line 10574
    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->postTemplateCss_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 10576
    :cond_b
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_c

    .line 10577
    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedFormId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 10579
    :cond_c
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_d

    .line 10580
    const/16 v1, 0x1b

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 10582
    :cond_d
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_e

    .line 10583
    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Form;->untranslatedSectionId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 10585
    :cond_e
    return-void
.end method
