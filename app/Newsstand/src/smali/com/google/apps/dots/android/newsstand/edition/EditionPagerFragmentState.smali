.class public abstract Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;
.super Ljava/lang/Object;
.source "EditionPagerFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "pageEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 18
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 27
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 28
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;

    .line 29
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 31
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 36
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 22
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "{EditionPagerFragmentState: %s}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
