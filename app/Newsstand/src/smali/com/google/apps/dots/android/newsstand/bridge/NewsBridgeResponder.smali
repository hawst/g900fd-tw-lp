.class public Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;
.super Ljava/lang/Object;
.source "NewsBridgeResponder.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private final isTouchNavigationAllowedProvider:Lcom/google/android/libraries/bind/util/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/bind/util/Provider",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

.field private renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/android/libraries/bind/util/Provider;)V
    .locals 0
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/android/libraries/bind/util/Provider",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p2, "isTouchNavigationAllowedProvider":Lcom/google/android/libraries/bind/util/Provider;, "Lcom/google/android/libraries/bind/util/Provider<Ljava/lang/Boolean;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 56
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->isTouchNavigationAllowedProvider:Lcom/google/android/libraries/bind/util/Provider;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;)Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method


# virtual methods
.method public getPostsForSectionAsync(Ljava/lang/String;IZLjava/lang/String;)V
    .locals 3
    .param p1, "sectionId"    # Ljava/lang/String;
    .param p2, "max"    # I
    .param p3, "requireImages"    # Z
    .param p4, "callbackKey"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 409
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 410
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "getPostsForSectionAsync - unspecified sectionId"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 467
    :cond_0
    :goto_0
    return-void

    .line 413
    :cond_1
    if-gez p2, :cond_2

    .line 414
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "getPostsForSectionAsync - invalid max"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 417
    :cond_2
    invoke-static {p4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "getPostsForSectionAsync - unspecified callbackKey"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected isTouchNavigationAllowed()Z
    .locals 2

    .prologue
    .line 470
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->isTouchNavigationAllowedProvider:Lcom/google/android/libraries/bind/util/Provider;

    invoke-interface {v1}, Lcom/google/android/libraries/bind/util/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 471
    .local v0, "allowed":Z
    if-nez v0, :cond_0

    .line 472
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->onTouchNavigationNotAllowed()V

    .line 474
    :cond_0
    return v0
.end method

.method protected onTouchNavigationNotAllowed()V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;->onArticleUnhandledTouchEvent()V

    .line 481
    :cond_0
    return-void
.end method

.method public openAudio(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V
    .locals 3
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "audioUri"    # Ljava/lang/String;

    .prologue
    .line 229
    if-nez p1, :cond_1

    .line 230
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "openAudio - WebView not attached to an activity"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    invoke-static {p2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->isTouchNavigationAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$3;

    invoke-direct {v2, p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$3;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public openDrawer(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;IZ)V
    .locals 8
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "fieldId"    # Ljava/lang/String;
    .param p3, "offset"    # I
    .param p4, "restrictToSingleField"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 132
    if-nez p1, :cond_1

    .line 133
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "openDrawer - WebView not attached to an activity"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    invoke-static {p2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "openDrawer - unspecified fieldId: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 140
    :cond_2
    if-gez p3, :cond_3

    .line 141
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "openDrawer - invalid offset: %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 145
    :cond_3
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->isTouchNavigationAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p1

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$2;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;Ljava/lang/String;ILcom/google/apps/dots/android/newsstand/activity/NSActivity;Z)V

    invoke-virtual {v6, v7, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public openOriginalUrl(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V
    .locals 3
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "postId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 109
    if-nez p1, :cond_1

    .line 110
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "openOriginalUrl - WebView not attached to an activity"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-static {p2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "openOriginalUrl - unspecified postId"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 117
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->isTouchNavigationAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$1;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$1;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public openPlayStoreDoc(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "docTypeValue"    # I
    .param p3, "backendDocId"    # Ljava/lang/String;
    .param p4, "parentBackendDocId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 325
    if-nez p1, :cond_1

    .line 326
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "openPlayStoreDoc - WebView not attached to an activity"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/DocType;->forProtoValue(I)Lcom/google/apps/dots/android/newsstand/util/DocType;

    move-result-object v0

    .line 330
    .local v0, "docType":Lcom/google/apps/dots/android/newsstand/util/DocType;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/DocType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/DocType;

    if-eq v0, v1, :cond_2

    move v1, v2

    :goto_1
    const-string v4, "Unknown doc type value %s"

    new-array v2, v2, [Ljava/lang/Object;

    .line 331
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    .line 330
    invoke-static {v1, v4, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 332
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->isTouchNavigationAllowed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 333
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->forDocType(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v1

    .line 334
    invoke-virtual {v1, p3}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v1

    .line 335
    invoke-virtual {v1, p4}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->setParentBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v1

    .line 336
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->start()V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 330
    goto :goto_1
.end method

.method public pauseAudio(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 3
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 254
    if-nez p1, :cond_1

    .line 255
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "pauseAudio - WebView not attached to an activity"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->isTouchNavigationAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->pauseAudio(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public requestPurchase(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V
    .locals 4
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 279
    if-nez p1, :cond_1

    .line 280
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "requestPurchase - WebView not attached to an activity"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->isTouchNavigationAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "requesting purchase: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 285
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 286
    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setPath(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v0

    .line 287
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->start()V

    goto :goto_0
.end method

.method public requestPurchaseInContext(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;IILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "backendId"    # I
    .param p3, "docTypeValue"    # I
    .param p4, "backendDocId"    # Ljava/lang/String;
    .param p5, "fullDocId"    # Ljava/lang/String;
    .param p6, "offerType"    # I
    .param p7, "parentBackendDocId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 299
    if-nez p1, :cond_0

    .line 300
    sget-object v2, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "requestPurchaseInContext - WebView not attached to an activity"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 320
    :goto_0
    return-void

    .line 303
    :cond_0
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/util/DocType;->forProtoValue(I)Lcom/google/apps/dots/android/newsstand/util/DocType;

    move-result-object v0

    .line 304
    .local v0, "docType":Lcom/google/apps/dots/android/newsstand/util/DocType;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/DocType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/DocType;

    if-eq v0, v2, :cond_2

    move v2, v3

    :goto_1
    const-string v5, "Unknown doc type value %s"

    new-array v6, v3, [Ljava/lang/Object;

    .line 305
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    .line 304
    invoke-static {v2, v5, v6}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 307
    const/4 v1, 0x0

    .line 308
    .local v1, "provider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;

    if-eqz v2, :cond_1

    move-object v2, p1

    .line 309
    check-cast v2, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->getIAPConversionEventProvider()Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;

    move-result-object v1

    .line 312
    :cond_1
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    invoke-direct {v2, p1, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;)V

    .line 313
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setShowPurchaseToast(Z)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v2

    .line 314
    invoke-virtual {v2, p2}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendId(I)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v2

    .line 315
    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v2

    .line 316
    invoke-virtual {v2, p4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v2

    .line 317
    invoke-virtual {v2, p5}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setFullDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v2

    .line 318
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setOfferType(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v2

    .line 319
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->start()V

    goto :goto_0

    .end local v1    # "provider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
    :cond_2
    move v2, v4

    .line 304
    goto :goto_1
.end method

.method public sendAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "pageIndex"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 342
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "sendAnalyticsEvent(%s,%s,%s,%s,%s)"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    aput-object p2, v2, v8

    const/4 v3, 0x2

    aput-object p3, v2, v3

    const/4 v3, 0x3

    aput-object p4, v2, v3

    const/4 v3, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 345
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 346
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Can not send analytics ads event: Null parameters found."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 357
    :goto_0
    return-void

    .line 350
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getReadingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 351
    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    move-object v3, p1

    move v4, p5

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    invoke-virtual {v0, v8}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->track(Z)V

    goto :goto_0
.end method

.method public sendCustomAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 16
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "pageIndex"    # I
    .param p6, "dimensionsMetricsJson"    # Ljava/lang/String;

    .prologue
    .line 362
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "sendCustomAnalyticsEvent(%s,%s,%s,%s,%s,%s)"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p3, v3, v4

    const/4 v4, 0x3

    aput-object p4, v3, v4

    const/4 v4, 0x4

    .line 363
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    aput-object p6, v3, v4

    .line 362
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 367
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Can not send analytics event: Null parameters found."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 401
    :goto_0
    return-void

    .line 371
    :cond_1
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v9

    .line 372
    .local v9, "dimensions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v10

    .line 373
    .local v10, "metrics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    if-eqz p6, :cond_3

    .line 375
    :try_start_0
    new-instance v11, Lorg/json/JSONObject;

    move-object/from16 v0, p6

    invoke-direct {v11, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 376
    .local v11, "dimensionsMetrics":Lorg/json/JSONObject;
    invoke-virtual {v11}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 377
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 378
    .local v13, "key":Ljava/lang/String;
    invoke-virtual {v11, v13}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    .line 379
    .local v15, "value":Ljava/lang/Object;
    instance-of v1, v15, Ljava/lang/Number;

    if-eqz v1, :cond_4

    .line 380
    move-object v0, v15

    check-cast v0, Ljava/lang/Number;

    move-object v1, v0

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v10, v13, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "\tadded custom metric %s = %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v13, v3, v4

    const/4 v4, 0x1

    aput-object v15, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 387
    .end local v11    # "dimensionsMetrics":Lorg/json/JSONObject;
    .end local v13    # "key":Ljava/lang/String;
    .end local v14    # "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v15    # "value":Ljava/lang/Object;
    :catch_0
    move-exception v12

    .line 388
    .local v12, "e":Lorg/json/JSONException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Couldn\'t parse additional dimensions and metrics from JSON: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p6, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 392
    .end local v12    # "e":Lorg/json/JSONException;
    :cond_3
    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getReadingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 393
    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    move-object/from16 v4, p1

    move/from16 v5, p5

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    invoke-direct/range {v1 .. v10}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    const/4 v2, 0x1

    .line 400
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->track(Z)V

    goto :goto_0

    .line 382
    .restart local v11    # "dimensionsMetrics":Lorg/json/JSONObject;
    .restart local v13    # "key":Ljava/lang/String;
    .restart local v14    # "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v15    # "value":Ljava/lang/Object;
    :cond_4
    :try_start_1
    instance-of v1, v15, Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 383
    move-object v0, v15

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    invoke-interface {v9, v13, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "\tadded custom dimension %s = %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v13, v3, v4

    const/4 v4, 0x1

    aput-object v15, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public setRenderDelegate(Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;)V
    .locals 0
    .param p1, "renderDelegate"    # Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    .line 65
    return-void
.end method

.method public setRenderSource(Lcom/google/apps/dots/android/newsstand/reading/RenderSource;)V
    .locals 0
    .param p1, "renderSource"    # Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 61
    return-void
.end method

.method public switchToApp(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V
    .locals 5
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 84
    if-nez p1, :cond_1

    .line 85
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "switchToApp - WebView not attached to an activity"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    invoke-static {p2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "undefined"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 90
    :cond_2
    sget-object v1, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "switchToApp() with bad appId: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 93
    :cond_3
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->isTouchNavigationAllowed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v0

    .line 95
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-direct {v1, p1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    goto :goto_0
.end method

.method public switchToArticle(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V
    .locals 3
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "postId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 68
    if-nez p1, :cond_0

    .line 69
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "switchToArticle - WebView not attached to an activity"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-static {p2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "switchToArticle - unspecified postId"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 77
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 78
    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->start()V

    goto :goto_0
.end method

.method public switchToSection(Ljava/lang/String;)V
    .locals 3
    .param p1, "sectionId"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "switchToSection - unspecified sectionId"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    :cond_0
    return-void
.end method

.method public toggleFullScreen()V
    .locals 0

    .prologue
    .line 275
    return-void
.end method

.method public watchVideo(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V
    .locals 3
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 217
    if-nez p1, :cond_1

    .line 218
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "watchVideo - WebView not attached to an activity"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->isTouchNavigationAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 223
    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->setUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;

    move-result-object v0

    .line 224
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->start()V

    goto :goto_0
.end method
