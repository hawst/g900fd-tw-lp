.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;
.super Lcom/google/android/libraries/bind/data/DataObserver;
.source "MagazineEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->setupObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 418
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 6
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 421
    iget-boolean v2, p1, Lcom/google/android/libraries/bind/data/DataChange;->affectsPrimaryKey:Z

    if-nez v2, :cond_0

    .line 459
    :goto_0
    return-void

    .line 425
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    const/4 v3, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->tocType:I
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1202(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;I)I

    .line 426
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 427
    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v2

    if-lez v2, :cond_4

    sget-object v2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;->DOES_NOT_HAVE_LITE_MODE:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    .line 426
    :goto_1
    # setter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->hasLiteMode:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;
    invoke-static {v3, v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1302(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    .line 428
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 429
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    sget v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_SECTION_SUMMARY:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 432
    .local v1, "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->getTocType()I

    move-result v3

    # setter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->tocType:I
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1202(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;I)I

    .line 433
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->hasCorrespondingImageSectionId()Z

    move-result v2

    if-nez v2, :cond_1

    .line 434
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->hasCorrespondingTextSectionId()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 435
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;->DOES_HAVE_LITE_MODE:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    # setter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->hasLiteMode:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1302(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    .line 441
    .end local v1    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->invalidateOptionsMenu()V

    .line 444
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->cardGroup:Lcom/google/android/libraries/bind/card/GridGroup;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1600(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNumColumns()I
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1500(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/card/GridGroup;->setFixedNumColumns(I)Lcom/google/android/libraries/bind/card/GridGroup;

    .line 448
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateMagazineAccessibilityWarning()V
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1700(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    .line 452
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->postIdToCenterOn:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1800(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 455
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->postIdToCenterOn:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1800(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->centerOnPostId(Ljava/lang/String;Ljava/lang/Integer;Z)V
    invoke-static {v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$1900(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Ljava/lang/String;Ljava/lang/Integer;Z)V

    .line 458
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->fetchMostRecentlyReadPost()V
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2000(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V

    goto/16 :goto_0

    .line 427
    .end local v0    # "i":I
    :cond_4
    sget-object v2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$HasLiteMode;

    goto :goto_1

    .line 428
    .restart local v0    # "i":I
    .restart local v1    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method
