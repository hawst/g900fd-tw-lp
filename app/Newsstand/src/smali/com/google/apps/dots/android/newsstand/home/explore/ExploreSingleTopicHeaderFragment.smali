.class public Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "ExploreSingleTopicHeaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private connectivityListener:Ljava/lang/Runnable;

.field private headerImage:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

.field private headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private topicObserver:Landroid/database/DataSetObserver;

.field private transitionDummyImage:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 90
    const/4 v0, 0x0

    const-string v1, "ExploreSingleTopicHeaderFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->edition_pager_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->transitionDummyImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->transitionDummyImage:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerImage:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerImage:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->updateErrorView()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->updateHeader(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getOnClickListenerForShelfType(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "shelfType"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .prologue
    .line 229
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$6;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V

    return-object v0
.end method

.method private refreshIfNeeded()V
    .locals 4

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 311
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 310
    invoke-static {v2, v3, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->getServerUriForExploreTopic(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;)Ljava/lang/String;

    move-result-object v0

    .line 312
    .local v0, "singleTopicUri":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->refreshIfNeeded(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    .line 313
    return-void
.end method

.method private sendAnalyticsEvent(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;)V
    .locals 2
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    .prologue
    .line 277
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->NONE:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    if-ne v0, v1, :cond_0

    .line 279
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ExploreSingleTopicScreen;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ExploreSingleTopicScreen;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ExploreSingleTopicScreen;->track(Z)V

    .line 281
    :cond_0
    return-void
.end method

.method private setupAdapter()V
    .locals 3

    .prologue
    .line 160
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    .line 161
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$2;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 174
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 180
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 181
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 183
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->topicObserver:Landroid/database/DataSetObserver;

    .line 192
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->topicObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 193
    return-void
.end method

.method private setupHeaderListLayout()V
    .locals 4

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 148
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 149
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 148
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 150
    return-void
.end method

.method private updateAdapter()V
    .locals 7

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .line 198
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    iget-object v6, v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    invoke-direct {v1, v5, v6, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)V

    .line 199
    .local v1, "cardList":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 200
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->cardListBuilder()Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 201
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->useSpacerTopPadding(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    move-result-object v4

    .line 202
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 204
    .local v0, "builtCardList":Lcom/google/android/libraries/bind/data/DataList;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->TOPICS:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .line 205
    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->getOnClickListenerForShelfType(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 206
    .local v3, "expandTopicsShelfListener":Landroid/view/View$OnClickListener;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->NEWS:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    .line 207
    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->getOnClickListenerForShelfType(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 209
    .local v2, "expandNewsShelfListener":Landroid/view/View$OnClickListener;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$5;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    invoke-direct {v4, p0, v5, v3, v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Ljava/util/concurrent/Executor;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 225
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 226
    return-void
.end method

.method private updateErrorView()V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->refreshErrorView()Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 255
    return-void
.end method

.method private updateHeader(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    iget-object v0, v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 155
    .local v0, "topic":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerImage:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setRunWhenImageSet(Ljava/lang/Runnable;)V

    .line 156
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerImage:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->imageAttachmentId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentId(Ljava/lang/String;)V

    .line 157
    return-void
.end method


# virtual methods
.method protected createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 2

    .prologue
    .line 317
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;)V

    return-object v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 246
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 247
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_explore_category"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const-string v2, "exploreTopicInfo"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    return-object v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 285
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onDestroyView()V

    .line 286
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->connectivityListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 287
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 288
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->topicObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 291
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 295
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onResume()V

    .line 296
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->getMsSincePause()J

    move-result-wide v0

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 297
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->refreshIfNeeded()V

    .line 299
    :cond_0
    return-void
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 100
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 101
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->setupHeaderListLayout()V

    .line 102
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 103
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->setupAdapter()V

    .line 104
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->updateErrorView()V

    .line 105
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 0
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 303
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->setUserVisibleHint(Z)V

    .line 304
    if-eqz p1, :cond_0

    .line 305
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->refreshIfNeeded()V

    .line 307
    :cond_0
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 73
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;)V
    .locals 3
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    .prologue
    .line 265
    if-eqz p2, :cond_0

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 266
    .local v0, "topicChanged":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 267
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->updateAdapter()V

    .line 268
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->updateErrorView()V

    .line 269
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->updateHeader(Ljava/lang/Runnable;)V

    .line 271
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->sendAnalyticsEvent(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;)V

    .line 272
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->refreshIfNeeded()V

    .line 274
    :cond_1
    return-void

    .line 265
    .end local v0    # "topicChanged":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
