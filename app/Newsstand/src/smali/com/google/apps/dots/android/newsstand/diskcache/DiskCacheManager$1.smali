.class Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$1;
.super Ljava/lang/Object;
.source "DiskCacheManager.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$1;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "preferenceKey"    # Ljava/lang/String;

    .prologue
    .line 43
    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "On external storage pref change"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$1;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$1;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->getRootDirs()Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    move-result-object v1

    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->requestSetRootDir(Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->access$100(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;)V

    .line 45
    return-void
.end method
