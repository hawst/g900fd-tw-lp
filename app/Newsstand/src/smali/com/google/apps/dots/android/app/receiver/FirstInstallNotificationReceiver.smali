.class public Lcom/google/apps/dots/android/app/receiver/FirstInstallNotificationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "FirstInstallNotificationReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 25
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getWasMagazinesUser()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 26
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getSeenUpgradeNotification()Z

    move-result v4

    if-nez v4, :cond_0

    .line 27
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setSeenUpgradeNotification(Z)V

    .line 28
    new-instance v4, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v4, p1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    sget v5, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_notification:I

    .line 29
    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    .line 31
    invoke-virtual {v4, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const-string v5, "promo"

    .line 32
    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->upgrade_status_bar_notification_title:I

    .line 33
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->upgrade_status_bar_notification_body:I

    .line 34
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 37
    .local v0, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/apps/dots/android/app/activity/CurrentsStartActivity;

    invoke-direct {v3, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 38
    .local v3, "resultIntent":Landroid/content/Intent;
    invoke-static {p1, v7, v3, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 39
    const-string v4, "notification"

    .line 40
    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 41
    .local v2, "notificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 42
    .local v1, "notification":Landroid/app/Notification;
    iget v4, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v4, v4, 0x4

    or-int/lit8 v4, v4, 0x10

    iput v4, v1, Landroid/app/Notification;->flags:I

    .line 44
    invoke-virtual {v2, v7, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 46
    .end local v0    # "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    .end local v1    # "notification":Landroid/app/Notification;
    .end local v2    # "notificationManager":Landroid/app/NotificationManager;
    .end local v3    # "resultIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
