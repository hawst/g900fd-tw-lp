.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;


# instance fields
.field private attachmentId_:Ljava/lang/String;

.field private attribution_:Ljava/lang/String;

.field private bitField0_:I

.field private caption_:Ljava/lang/String;

.field private fairUse_:Z

.field private height_:I

.field private originalUri_:Ljava/lang/String;

.field private preferAsPrimaryImage_:Z

.field private shoeboxPermanentUrl_:Ljava/lang/String;

.field private width_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11490
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11491
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 11496
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attachmentId_:Ljava/lang/String;

    .line 11518
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->originalUri_:Ljava/lang/String;

    .line 11540
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->shoeboxPermanentUrl_:Ljava/lang/String;

    .line 11562
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->width_:I

    .line 11581
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->height_:I

    .line 11600
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->caption_:Ljava/lang/String;

    .line 11622
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->fairUse_:Z

    .line 11641
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->preferAsPrimaryImage_:Z

    .line 11660
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attribution_:Ljava/lang/String;

    .line 11491
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 3

    .prologue
    .line 11700
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11704
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    return-object v0

    .line 11701
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :catch_0
    move-exception v1

    .line 11702
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11487
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 11709
    if-ne p1, p0, :cond_1

    .line 11720
    :cond_0
    :goto_0
    return v1

    .line 11710
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 11711
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 11712
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attachmentId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attachmentId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->originalUri_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->originalUri_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 11713
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->shoeboxPermanentUrl_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->shoeboxPermanentUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 11714
    :goto_3
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->width_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->width_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->height_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->height_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->caption_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->caption_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 11717
    :goto_4
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->fairUse_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->fairUse_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->preferAsPrimaryImage_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->preferAsPrimaryImage_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attribution_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attribution_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 11720
    goto :goto_0

    .line 11712
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attachmentId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->originalUri_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->originalUri_:Ljava/lang/String;

    .line 11713
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->shoeboxPermanentUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->shoeboxPermanentUrl_:Ljava/lang/String;

    .line 11714
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->caption_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->caption_:Ljava/lang/String;

    .line 11717
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attribution_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attribution_:Ljava/lang/String;

    .line 11720
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0
.end method

.method public getAttachmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11498
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attachmentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getAttribution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11662
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attribution_:Ljava/lang/String;

    return-object v0
.end method

.method public getCaption()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11602
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->caption_:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 11583
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->height_:I

    return v0
.end method

.method public getOriginalUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11520
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->originalUri_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 11771
    const/4 v0, 0x0

    .line 11772
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 11773
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attachmentId_:Ljava/lang/String;

    .line 11774
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11776
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 11777
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->originalUri_:Ljava/lang/String;

    .line 11778
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11780
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 11781
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->width_:I

    .line 11782
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11784
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    .line 11785
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->height_:I

    .line 11786
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11788
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    .line 11789
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->caption_:Ljava/lang/String;

    .line 11790
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11792
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_5

    .line 11793
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->fairUse_:Z

    .line 11794
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 11796
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_6

    .line 11797
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->preferAsPrimaryImage_:Z

    .line 11798
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 11800
    :cond_6
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_7

    .line 11801
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attribution_:Ljava/lang/String;

    .line 11802
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11804
    :cond_7
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_8

    .line 11805
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->shoeboxPermanentUrl_:Ljava/lang/String;

    .line 11806
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11808
    :cond_8
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->cachedSize:I

    .line 11809
    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 11564
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->width_:I

    return v0
.end method

.method public hasAttribution()Z
    .locals 1

    .prologue
    .line 11673
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11724
    const/16 v0, 0x11

    .line 11725
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 11726
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attachmentId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v5, v1

    .line 11727
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->originalUri_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v5, v1

    .line 11728
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->shoeboxPermanentUrl_:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v5, v1

    .line 11729
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->width_:I

    add-int v0, v1, v5

    .line 11730
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->height_:I

    add-int v0, v1, v5

    .line 11731
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->caption_:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v5, v1

    .line 11732
    mul-int/lit8 v5, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->fairUse_:Z

    if-eqz v1, :cond_4

    move v1, v3

    :goto_4
    add-int v0, v5, v1

    .line 11733
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->preferAsPrimaryImage_:Z

    if-eqz v5, :cond_5

    :goto_5
    add-int v0, v1, v3

    .line 11734
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attribution_:Ljava/lang/String;

    if-nez v3, :cond_6

    :goto_6
    add-int v0, v1, v2

    .line 11735
    return v0

    .line 11726
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attachmentId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 11727
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->originalUri_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 11728
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->shoeboxPermanentUrl_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 11731
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->caption_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    move v1, v4

    .line 11732
    goto :goto_4

    :cond_5
    move v3, v4

    .line 11733
    goto :goto_5

    .line 11734
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attribution_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11817
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 11818
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 11822
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 11823
    :sswitch_0
    return-object p0

    .line 11828
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attachmentId_:Ljava/lang/String;

    .line 11829
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    goto :goto_0

    .line 11833
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->originalUri_:Ljava/lang/String;

    .line 11834
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    goto :goto_0

    .line 11838
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->width_:I

    .line 11839
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    goto :goto_0

    .line 11843
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->height_:I

    .line 11844
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    goto :goto_0

    .line 11848
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->caption_:Ljava/lang/String;

    .line 11849
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    goto :goto_0

    .line 11853
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->fairUse_:Z

    .line 11854
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    goto :goto_0

    .line 11858
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->preferAsPrimaryImage_:Z

    .line 11859
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    goto :goto_0

    .line 11863
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attribution_:Ljava/lang/String;

    .line 11864
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    goto :goto_0

    .line 11868
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->shoeboxPermanentUrl_:Ljava/lang/String;

    .line 11869
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    goto/16 :goto_0

    .line 11818
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11487
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11740
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 11741
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 11743
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 11744
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->originalUri_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 11746
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 11747
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->width_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 11749
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    .line 11750
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->height_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 11752
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    .line 11753
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->caption_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 11755
    :cond_4
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_5

    .line 11756
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->fairUse_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 11758
    :cond_5
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    .line 11759
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->preferAsPrimaryImage_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 11761
    :cond_6
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_7

    .line 11762
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->attribution_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 11764
    :cond_7
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    .line 11765
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->shoeboxPermanentUrl_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 11767
    :cond_8
    return-void
.end method
