.class public Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil;
.super Ljava/lang/Object;
.source "GservicesUtil.java"


# direct methods
.method static synthetic access$000()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil;->contentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method private static contentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public static getBoolean(Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defValue"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/gcm/GservicesUtil$1;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->submit(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
