.class public final enum Lcom/google/apps/dots/android/newsstand/util/DocType;
.super Ljava/lang/Enum;
.source "DocType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/DocType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum ANDROID_APP:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum APK:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum MAGAZINE_ISSUE:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum MUSIC_ALBUM:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum MUSIC_SONG:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum NEWS_EDITION:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum OCEAN_BOOK:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum SUBSCRIPTION:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum TV_EPISODE:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum TV_SEASON:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum TV_SHOW:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field public static final enum YOUTUBE_MOVIE:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field private static final protoEnumValueToFinskyDocType:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/apps/dots/android/newsstand/util/DocType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final protoValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 11
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 12
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "ANDROID_APP"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->ANDROID_APP:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 13
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "MUSIC_ALBUM"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->MUSIC_ALBUM:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 14
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "MUSIC_SONG"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v6}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->MUSIC_SONG:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 15
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "OCEAN_BOOK"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->OCEAN_BOOK:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 16
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "YOUTUBE_MOVIE"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->YOUTUBE_MOVIE:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 17
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "APK"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v8, v2}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->APK:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 18
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "SUBSCRIPTION"

    const/4 v2, 0x7

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->SUBSCRIPTION:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 19
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "MAGAZINE"

    const/16 v2, 0x8

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 20
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "MAGAZINE_ISSUE"

    const/16 v2, 0x9

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->MAGAZINE_ISSUE:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 21
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "NEWS_EDITION"

    const/16 v2, 0xa

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->NEWS_EDITION:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 22
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "TV_SHOW"

    const/16 v2, 0xb

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->TV_SHOW:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "TV_SEASON"

    const/16 v2, 0xc

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->TV_SEASON:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v1, "TV_EPISODE"

    const/16 v2, 0xd

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/DocType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->TV_EPISODE:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 10
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/DocType;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/DocType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/DocType;->ANDROID_APP:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/DocType;->MUSIC_ALBUM:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v1, v0, v5

    const/4 v1, 0x3

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/DocType;->MUSIC_SONG:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/DocType;->OCEAN_BOOK:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/DocType;->YOUTUBE_MOVIE:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/DocType;->APK:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v1, v0, v8

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/DocType;->SUBSCRIPTION:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/DocType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/DocType;->MAGAZINE_ISSUE:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/DocType;->NEWS_EDITION:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/DocType;->TV_SHOW:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/DocType;->TV_SEASON:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/DocType;->TV_EPISODE:Lcom/google/apps/dots/android/newsstand/util/DocType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 26
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->protoEnumValueToFinskyDocType:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "protoValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/DocType;->protoValue:I

    .line 31
    return-void
.end method

.method public static forProtoValue(I)Lcom/google/apps/dots/android/newsstand/util/DocType;
    .locals 6
    .param p0, "protoValue"    # I

    .prologue
    .line 35
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/DocType;->protoEnumValueToFinskyDocType:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/DocType;->values()[Lcom/google/apps/dots/android/newsstand/util/DocType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 37
    .local v0, "docType":Lcom/google/apps/dots/android/newsstand/util/DocType;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/DocType;->protoEnumValueToFinskyDocType:Ljava/util/Map;

    iget v5, v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->protoValue:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    .end local v0    # "docType":Lcom/google/apps/dots/android/newsstand/util/DocType;
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/DocType;->protoEnumValueToFinskyDocType:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 41
    .restart local v0    # "docType":Lcom/google/apps/dots/android/newsstand/util/DocType;
    if-nez v0, :cond_1

    .line 42
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 44
    .end local v0    # "docType":Lcom/google/apps/dots/android/newsstand/util/DocType;
    :cond_1
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/DocType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/DocType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/DocType;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/DocType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/DocType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/DocType;

    return-object v0
.end method
