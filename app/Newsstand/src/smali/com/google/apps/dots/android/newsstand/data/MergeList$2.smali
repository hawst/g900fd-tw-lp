.class Lcom/google/apps/dots/android/newsstand/data/MergeList$2;
.super Lcom/google/android/libraries/bind/data/RefreshTask;
.source "MergeList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/data/MergeList;->makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/data/MergeList;

.field final synthetic val$snapshots:[Lcom/google/android/libraries/bind/data/Snapshot;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/data/MergeList;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;[Lcom/google/android/libraries/bind/data/Snapshot;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/data/MergeList;
    .param p2, "arg0"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p3, "arg1"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList$2;->this$0:Lcom/google/apps/dots/android/newsstand/data/MergeList;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList$2;->val$snapshots:[Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-direct {p0, p2, p3}, Lcom/google/android/libraries/bind/data/RefreshTask;-><init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method protected getFreshData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/libraries/bind/data/DataException;
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList$2;->this$0:Lcom/google/apps/dots/android/newsstand/data/MergeList;

    # getter for: Lcom/google/apps/dots/android/newsstand/data/MergeList;->mergeFilter:Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/data/MergeList;->access$000(Lcom/google/apps/dots/android/newsstand/data/MergeList;)Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/MergeList$2;->val$snapshots:[Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-interface {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;->transform([Lcom/google/android/libraries/bind/data/Snapshot;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
