.class public final Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;
.super Ljava/lang/Object;
.source "DiskCacheManager.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final appContext:Landroid/content/Context;

.field private final diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

.field private final externalStorageReceiver:Landroid/content/BroadcastReceiver;

.field private final listenerHandle:Lcom/google/apps/dots/android/newsstand/util/Disposable;

.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field private final upgrade:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .param p3, "upgrade"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->appContext:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 38
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->upgrade:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .line 39
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->makeDiskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$1;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "externalStorageDir"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->listenerHandle:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 47
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$2;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->externalStorageReceiver:Landroid/content/BroadcastReceiver;

    .line 53
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->startWatchingExternalStorage()V

    .line 54
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->requestCheckAvailability()V

    .line 55
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->requestSetRootDir(Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->checkAvailability()V

    return-void
.end method

.method private checkAvailability()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 123
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->open(Z)V
    :try_end_0
    .catch Lcom/google/apps/dots/android/newsstand/diskcache/UnmountedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 135
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "ue":Lcom/google/apps/dots/android/newsstand/diskcache/UnmountedException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Unavailable. Routing through start flow."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ii(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->appContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;->start()V

    goto :goto_0

    .line 130
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Not visible"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 132
    .end local v0    # "ue":Lcom/google/apps/dots/android/newsstand/diskcache/UnmountedException;
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private makeDiskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    .locals 3

    .prologue
    .line 142
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->getDefault()Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    move-result-object v1

    .line 143
    .local v1, "options":Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->upgrade:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->whenUpgradeFinished()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 146
    .local v0, "diskCache":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->getRootDirs()Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->setRootDirs(Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;)V

    .line 147
    return-object v0
.end method

.method private requestSetRootDir(Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;)V
    .locals 4
    .param p1, "rootDirs"    # Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    .prologue
    .line 101
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Request set root dirs: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSSTORE_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$3;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$3;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->execute(Ljava/lang/Runnable;)V

    .line 109
    return-void
.end method

.method private startWatchingExternalStorage()V
    .locals 3

    .prologue
    .line 151
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 152
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 153
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 154
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 155
    const-string v1, "android.intent.action.MEDIA_NOFS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156
    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 157
    const-string v1, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 158
    const-string v1, "android.intent.action.MEDIA_UNMOUNTABLE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->appContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->externalStorageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 161
    return-void
.end method

.method public static storeDirName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->storeDirName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static storeDirName(I)Ljava/lang/String;
    .locals 5
    .param p0, "version"    # I

    .prologue
    .line 68
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "store.%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDiskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    return-object v0
.end method

.method public getRootDirs()Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 76
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->storeDirName()Ljava/lang/String;

    move-result-object v2

    .line 77
    .local v2, "storeDirName":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v8, "externalStorageDir"

    invoke-virtual {v7, v8}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    move v3, v5

    .line 81
    .local v3, "useExternalStorage":Z
    :goto_0
    if-eqz v3, :cond_2

    .line 82
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->appContext:Landroid/content/Context;

    invoke-virtual {v7, v4}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 83
    .local v1, "permDir":Ljava/io/File;
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->appContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    .line 84
    .local v0, "cacheDir":Ljava/io/File;
    if-eqz v1, :cond_0

    if-nez v0, :cond_3

    .line 85
    :cond_0
    sget-object v5, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Root dirs not available"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v5, v7, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    :goto_1
    return-object v4

    .end local v0    # "cacheDir":Ljava/io/File;
    .end local v1    # "permDir":Ljava/io/File;
    .end local v3    # "useExternalStorage":Z
    :cond_1
    move v3, v6

    .line 77
    goto :goto_0

    .line 89
    .restart local v3    # "useExternalStorage":Z
    :cond_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->appContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 90
    .restart local v1    # "permDir":Ljava/io/File;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->appContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 93
    .restart local v0    # "cacheDir":Ljava/io/File;
    :cond_3
    sget-object v4, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Root dirs %s %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v1, v8, v6

    aput-object v0, v8, v5

    invoke-virtual {v4, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    new-instance v4, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;-><init>(Ljava/io/File;Ljava/io/File;)V

    goto :goto_1
.end method

.method public requestCheckAvailability()V
    .locals 2

    .prologue
    .line 112
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSSTORE_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager$4;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->execute(Ljava/lang/Runnable;)V

    .line 119
    return-void
.end method
