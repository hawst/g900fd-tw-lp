.class final Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState$1;
.super Ljava/lang/Object;
.source "YearMonthIssuesFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;
    .locals 5
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 68
    .local v3, "year":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 69
    .local v2, "month":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 70
    .local v0, "endMonth":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 71
    .local v1, "hideArchive":Z
    new-instance v4, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    invoke-direct {v4, v3, v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;-><init>(IIIZ)V

    return-object v4
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 76
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragmentState;

    move-result-object v0

    return-object v0
.end method
