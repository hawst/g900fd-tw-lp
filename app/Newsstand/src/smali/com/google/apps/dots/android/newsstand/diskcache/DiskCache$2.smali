.class Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;
.super Ljava/lang/Object;
.source "DiskCache.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->visit(Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

.field final synthetic val$blobVisitor:Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    .prologue
    .line 648
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;->val$blobVisitor:Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;)V
    .locals 4
    .param p1, "region"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 651
    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->touch()V

    .line 652
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 654
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->lb()Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v0

    .line 655
    .local v0, "lb":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getUpper(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    invoke-static {v2, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->access$000(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v1

    .line 656
    .local v1, "ub":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;

    invoke-direct {v3, p0, v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->visit(Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;)V

    .line 672
    return-void
.end method
