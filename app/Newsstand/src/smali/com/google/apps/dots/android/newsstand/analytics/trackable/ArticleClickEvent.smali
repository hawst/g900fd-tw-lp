.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;
.source "ArticleClickEvent.java"


# instance fields
.field private final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private section:Lcom/google/apps/dots/proto/client/DotsShared$Section;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 0
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 29
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 31
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 35
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 36
    return-void
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 3
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 93
    .local v0, "originalEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    const-string v1, "Article Click"

    invoke-virtual {p1, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 94
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 95
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    .line 96
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 97
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setSectionName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    .line 98
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 99
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 100
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 101
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppFamilyName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 102
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setUserSubscriptionType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 103
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 105
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->addCuratedDimensionIfNecessary(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 106
    return-object p1
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 40
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->getSection(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Section;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 43
    sget-object v3, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent$1;->$SwitchMap$com$google$apps$dots$android$newsstand$util$ProtoEnum$EditionType:[I

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 82
    const/4 v2, 0x0

    .line 85
    .local v2, "screen":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 45
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_0
    const-string v2, "ReadNow"

    .line 46
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 48
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_1
    const-string v4, "%s - %s"

    new-array v5, v8, [Ljava/lang/Object;

    const-string v3, "[Topic]"

    aput-object v3, v5, v6

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    .line 49
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getDescription()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    .line 48
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 50
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 52
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_2
    const-string v4, "%s - %s"

    new-array v5, v8, [Ljava/lang/Object;

    const-string v3, "[Topic]"

    aput-object v3, v5, v6

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .line 53
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    .line 52
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 54
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 56
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_3
    const-string v3, "%s %s - %s"

    new-array v4, v9, [Ljava/lang/Object;

    const-string v5, "[Section]"

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 57
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 56
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 58
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 60
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_4
    const-string v2, "Bookmarks"

    .line 61
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 63
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_5
    const-string v2, "Search"

    .line 64
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 66
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_6
    const-string v3, "%s %s - %s"

    new-array v4, v9, [Ljava/lang/Object;

    const-string v5, "[Magazine - TOC]"

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 67
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppFamilyName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 66
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 68
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 72
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_7
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;->getPostId()Ljava/lang/String;

    move-result-object v1

    .line 74
    .local v1, "postId":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    invoke-virtual {v3, v4, v1, v5}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 73
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 75
    .local v0, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    if-nez v0, :cond_1

    .line 76
    new-instance v4, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v5, "Could not find post for postId = "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-direct {v4, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 78
    :cond_1
    const-string v3, "%s %s - %s"

    new-array v4, v9, [Ljava/lang/Object;

    const-string v5, "[Related]"

    aput-object v5, v4, v6

    .line 79
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 78
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 80
    .restart local v2    # "screen":Ljava/lang/String;
    goto/16 :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
