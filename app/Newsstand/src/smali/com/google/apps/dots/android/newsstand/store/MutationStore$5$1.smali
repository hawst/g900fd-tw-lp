.class Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;
.super Ljava/lang/Object;
.source "MutationStore.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->call()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$batchUri:Ljava/lang/String;

.field final synthetic val$mutationLog:Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

.field final synthetic val$storeRequest:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;

    .prologue
    .line 309
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$batchUri:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$mutationLog:Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$account:Landroid/accounts/Account;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$storeRequest:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private handleHttpStatusCode(I)V
    .locals 10
    .param p1, "httpStatusCode"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v7, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 342
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->retryPolicy:Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$1000(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$mutationLog:Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    invoke-virtual {v3, v4, p1}, Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;->shouldRetryOnFailureResponse(Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 344
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;-><init>()V

    .line 345
    .local v2, "mutationLogDelta":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$mutationLog:Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getNumTries()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->setNumTries(I)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    .line 346
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->setLastHttpFailureTime(J)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    .line 349
    :try_start_0
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "%s: received HTTP %d, bumping num tries to %d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$batchUri:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    .line 350
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getNumTries()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 349
    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 351
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->val$mutLogFile:Ljava/io/File;

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    .local v1, "mutLogOut":Ljava/io/OutputStream;
    :try_start_1
    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->writeToStream(Lcom/google/protobuf/nano/MessageNano;Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 355
    :try_start_2
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 369
    .end local v1    # "mutLogOut":Ljava/io/OutputStream;
    .end local v2    # "mutationLogDelta":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    :goto_0
    return-void

    .line 355
    .restart local v1    # "mutLogOut":Ljava/io/OutputStream;
    .restart local v2    # "mutationLogDelta":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 357
    .end local v1    # "mutLogOut":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "%s: trouble updating mutation log, deleting."

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$batchUri:Ljava/lang/String;

    aput-object v6, v5, v9

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 359
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$account:Landroid/accounts/Account;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$storeRequest:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->clear(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    .line 360
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->val$mutLogFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 364
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "mutationLogDelta":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    :cond_0
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "%s: Giving up with HTTP %s on attempt %d."

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$batchUri:Ljava/lang/String;

    aput-object v6, v5, v9

    .line 365
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$mutationLog:Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getNumTries()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 364
    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->val$mutLogFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 367
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$account:Landroid/accounts/Account;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$storeRequest:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->clear(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    goto :goto_0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1, "input"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 316
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: upload successful"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->val$batchUri:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->val$mutLogFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 318
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public fallback(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 323
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    .line 324
    const/4 v1, 0x0

    .line 325
    .local v1, "httpStatusCode":Ljava/lang/Integer;
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    if-eqz v2, :cond_1

    move-object v0, p1

    .line 326
    check-cast v0, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    .line 327
    .local v0, "httpException":Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;->getResponseStatus()Ljava/lang/Integer;

    move-result-object v1

    .line 328
    if-nez v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v2, v2, Lcom/google/apps/dots/android/newsstand/auth/AuthException;

    if-eqz v2, :cond_0

    .line 329
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Auth exception: %s. Treating as HTTP 401 Unauthorized."

    new-array v4, v7, [Ljava/lang/Object;

    .line 330
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 329
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 331
    const/16 v2, 0x191

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 333
    :cond_0
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Status %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335
    .end local v0    # "httpException":Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;
    :cond_1
    if-eqz v1, :cond_2

    .line 336
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;->handleHttpStatusCode(I)V

    .line 338
    :cond_2
    throw p1
.end method
