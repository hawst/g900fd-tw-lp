.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;
.super Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;
.source "MyMagazinesGridGroup.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private appFamilyId:Ljava/lang/String;

.field private areIssuesArchived:Z

.field private final cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

.field private endMonth:I

.field private month:I

.field private rowId:Ljava/lang/String;

.field private year:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;)V
    .locals 1
    .param p1, "magazineList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "magazineGroup"    # Lcom/google/android/libraries/bind/data/Data;
    .param p3, "cardListBuilder"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 38
    const-string v0, "DEFAULT_MAGAZINE_GROUP_ID"

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->rowId:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 45
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->updateDateAppInfo(Lcom/google/android/libraries/bind/data/Data;)V

    .line 46
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->updateArchiveState(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 47
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->setRowDimensions(Lcom/google/android/libraries/bind/data/Data;)V

    .line 48
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->createRowId()V

    .line 49
    invoke-virtual {p1}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 50
    invoke-direct {p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->createHeader(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->addHeader(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 52
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    .prologue
    .line 27
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->year:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    .prologue
    .line 27
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->month:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    .prologue
    .line 27
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->endMonth:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->areIssuesArchived:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/view/View;IIIZ)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # I
    .param p6, "x6"    # Z

    .prologue
    .line 27
    invoke-direct/range {p0 .. p6}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->createIntentBuilder(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/view/View;IIIZ)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->appFamilyId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/view/View;Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Z

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->createIntentBuilder(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/view/View;Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v0

    return-object v0
.end method

.method private createHeader(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/Data;
    .locals 7
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "issues"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 85
    invoke-virtual {p2}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v1

    .line 87
    .local v1, "numIssues":I
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_NAME:I

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_NAME:I

    .line 88
    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "name":Ljava/lang/String;
    :goto_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "The number of issues: %s The number remaining %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 93
    invoke-virtual {p2}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 94
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 92
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->fixedNumColumns:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->maxRows:I

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->createShelfHeader(Ljava/lang/String;III)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    return-object v2

    .line 89
    .end local v0    # "name":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->year:I

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->month:I

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->endMonth:I

    invoke-static {v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/util/DateFormatUtil;->getLocalizedYearAndOptionalMonthRange(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private createIntentBuilder(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/view/View;IIIZ)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    .locals 4
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "year"    # I
    .param p4, "month"    # I
    .param p5, "endMonth"    # I
    .param p6, "hideArchived"    # Z

    .prologue
    .line 160
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;

    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 161
    invoke-virtual {v2, p3}, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->setYear(I)Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;

    move-result-object v1

    .line 162
    .local v1, "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;
    if-lez p4, :cond_0

    .line 163
    invoke-virtual {v1, p4}, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->setMonth(I)Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;

    .line 164
    if-lez p5, :cond_0

    .line 165
    invoke-virtual {v1, p5}, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->setEndMonth(I)Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;

    .line 168
    :cond_0
    instance-of v2, p2, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;

    if-eqz v2, :cond_1

    .line 169
    check-cast p2, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;

    .end local p2    # "view":Landroid/view/View;
    const-class v2, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;

    invoke-virtual {v1, p2, v2}, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->enableReloadoTransition(Landroid/view/ViewGroup;Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;

    .line 170
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    .line 171
    invoke-virtual {p1, v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 172
    .local v0, "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    if-eqz v0, :cond_1

    .line 173
    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarView()Landroid/view/View;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->reloado_actionbar:I

    .line 174
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getStringResource(I)Ljava/lang/String;

    move-result-object v3

    .line 173
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->addSharedElement(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;

    .line 177
    .end local v0    # "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    :cond_1
    invoke-virtual {v1, p6}, Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;->setHideArchive(Z)Lcom/google/apps/dots/android/newsstand/navigation/YearMonthIssuesIntentBuilder;

    .line 178
    return-object v1
.end method

.method private createIntentBuilder(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/view/View;Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    .locals 4
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "appFamilyId"    # Ljava/lang/String;
    .param p4, "hideArchived"    # Z

    .prologue
    .line 139
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;

    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 140
    invoke-virtual {v2, p3}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;

    move-result-object v2

    .line 141
    invoke-virtual {v2, p4}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->setHideArchive(Z)Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;

    move-result-object v1

    .line 142
    .local v1, "titleIssuesIntentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;
    instance-of v2, p2, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;

    if-eqz v2, :cond_0

    .line 143
    check-cast p2, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;

    .end local p2    # "view":Landroid/view/View;
    const-class v2, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;

    invoke-virtual {v1, p2, v2}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->enableReloadoTransition(Landroid/view/ViewGroup;Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;

    .line 144
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    .line 145
    invoke-virtual {p1, v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 146
    .local v0, "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarView()Landroid/view/View;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->reloado_actionbar:I

    .line 148
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getStringResource(I)Ljava/lang/String;

    move-result-object v3

    .line 147
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->addSharedElement(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;

    .line 151
    .end local v0    # "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    :cond_0
    return-object v1
.end method

.method private createRowId()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 103
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->areIssuesArchived:Z

    if-eqz v1, :cond_0

    const-string v0, "archived"

    .line 104
    .local v0, "archiveAppend":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->appFamilyId:Ljava/lang/String;

    if-eqz v1, :cond_1

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s-%s_%s"

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "Magazine_Group_"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->appFamilyId:Ljava/lang/String;

    aput-object v4, v3, v6

    aput-object v0, v3, v7

    .line 105
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 106
    :goto_1
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->rowId:Ljava/lang/String;

    .line 108
    return-void

    .line 103
    .end local v0    # "archiveAppend":Ljava/lang/String;
    :cond_0
    const-string v0, "unarchived"

    goto :goto_0

    .line 105
    .restart local v0    # "archiveAppend":Ljava/lang/String;
    :cond_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s-%d-%d_%s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Magazine_Group_"

    aput-object v4, v3, v5

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->year:I

    .line 107
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->month:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    aput-object v0, v3, v8

    .line 106
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private createShelfHeader(Ljava/lang/String;III)Lcom/google/android/libraries/bind/data/Data;
    .locals 8
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "numColumns"    # I
    .param p3, "numRows"    # I
    .param p4, "actualCount"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 111
    mul-int v2, p2, p3

    invoke-static {v2, p4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 112
    .local v1, "numIssuesShown":I
    sub-int v0, p4, v1

    .line 113
    .local v0, "numIssuesNotShown":I
    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Number of issues not shown %s"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    if-lez v0, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 115
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$plurals;->magazines_see_more:I

    new-array v5, v6, [Ljava/lang/Object;

    .line 116
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 115
    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;)V

    .line 114
    invoke-virtual {v2, p1, v3, v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeShelfHeaderWithButton(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    .line 130
    :goto_0
    return-object v2

    .line 114
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 130
    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeShelfHeader(Ljava/lang/String;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    goto :goto_0
.end method

.method private setRowDimensions(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 5
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v2, 0x1

    .line 55
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_APP_FAMILY_ID:I

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->appFamilyId:Ljava/lang/String;

    .line 56
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$integer;->num_magazine_cols:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 61
    .local v1, "numCols":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getMyMagazinesSortByField()I

    move-result v3

    if-ne v3, v2, :cond_1

    move v0, v2

    .line 62
    .local v0, "groupByTitle":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 63
    .local v2, "numRows":I
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->setFixedNumColumns(I)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/bind/card/GridGroup;->setMaxRows(I)Lcom/google/android/libraries/bind/card/GridGroup;

    .line 64
    return-void

    .line 61
    .end local v0    # "groupByTitle":Z
    .end local v2    # "numRows":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateArchiveState(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 3
    .param p1, "issues"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    const/4 v2, 0x0

    .line 80
    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_ARCHIVED:I

    .line 81
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->areIssuesArchived:Z

    .line 82
    return-void
.end method

.method private updateDateAppInfo(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v1, 0x0

    .line 67
    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_APP_FAMILY_ID:I

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_APP_FAMILY_ID:I

    .line 68
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->appFamilyId:Ljava/lang/String;

    .line 69
    sget v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_YEAR:I

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_YEAR:I

    .line 70
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->year:I

    .line 71
    sget v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_MONTH:I

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_MONTH:I

    .line 72
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_2
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->month:I

    .line 74
    sget v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_END_MONTH:I

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_END_MONTH:I

    .line 75
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_0
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->endMonth:I

    .line 76
    return-void

    .line 68
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 70
    goto :goto_1

    :cond_3
    move v0, v1

    .line 72
    goto :goto_2
.end method


# virtual methods
.method public getRowId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->rowId:Ljava/lang/String;

    return-object v0
.end method
