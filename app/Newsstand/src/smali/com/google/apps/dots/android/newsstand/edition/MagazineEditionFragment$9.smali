.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$9;
.super Ljava/lang/Object;
.source "MagazineEditionFragment.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/android/libraries/bind/data/Data;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 559
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/libraries/bind/data/Data;)Z
    .locals 4
    .param p1, "input"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v1, 0x0

    .line 562
    sget v2, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 563
    .local v0, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v2, v2

    if-lez v2, :cond_0

    iget-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    aget-object v2, v2, v1

    .line 564
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 559
    check-cast p1, Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$9;->apply(Lcom/google/android/libraries/bind/data/Data;)Z

    move-result v0

    return v0
.end method
