.class public final Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AdFormatSettings"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;


# instance fields
.field private adSettings_:Ljava/lang/String;

.field private adSystem_:I

.field private bitField0_:I

.field private googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

.field private phoneTemplate_:Ljava/lang/String;

.field private pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

.field private tabletTemplate_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2492
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2493
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2507
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSystem_:I

    .line 2526
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSettings_:Ljava/lang/String;

    .line 2548
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->phoneTemplate_:Ljava/lang/String;

    .line 2570
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->tabletTemplate_:Ljava/lang/String;

    .line 2493
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    .locals 3

    .prologue
    .line 2645
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2649
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-eqz v2, :cond_0

    .line 2650
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    .line 2652
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-eqz v2, :cond_1

    .line 2653
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    .line 2655
    :cond_1
    return-object v0

    .line 2646
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    :catch_0
    move-exception v1

    .line 2647
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2489
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2660
    if-ne p1, p0, :cond_1

    .line 2668
    :cond_0
    :goto_0
    return v1

    .line 2661
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2662
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 2663
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSystem_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSystem_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSettings_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSettings_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2664
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->phoneTemplate_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->phoneTemplate_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2665
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->tabletTemplate_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->tabletTemplate_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2666
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-nez v3, :cond_3

    .line 2667
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 2668
    goto :goto_0

    .line 2663
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSettings_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSettings_:Ljava/lang/String;

    .line 2664
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->phoneTemplate_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->phoneTemplate_:Ljava/lang/String;

    .line 2665
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->tabletTemplate_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->tabletTemplate_:Ljava/lang/String;

    .line 2666
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    .line 2667
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    .line 2668
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getGoogleSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    .locals 1

    .prologue
    .line 2613
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    return-object v0
.end method

.method public getPubSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    .locals 1

    .prologue
    .line 2594
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2707
    const/4 v0, 0x0

    .line 2708
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2709
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSystem_:I

    .line 2710
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2712
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2713
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSettings_:Ljava/lang/String;

    .line 2714
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2716
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2717
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->phoneTemplate_:Ljava/lang/String;

    .line 2718
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2720
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 2721
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->tabletTemplate_:Ljava/lang/String;

    .line 2722
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2724
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-eqz v1, :cond_4

    .line 2725
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    .line 2726
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2728
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-eqz v1, :cond_5

    .line 2729
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    .line 2730
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2732
    :cond_5
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->cachedSize:I

    .line 2733
    return v0
.end method

.method public hasGoogleSold()Z
    .locals 1

    .prologue
    .line 2623
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPubSold()Z
    .locals 1

    .prologue
    .line 2604
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2672
    const/16 v0, 0x11

    .line 2673
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 2674
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSystem_:I

    add-int v0, v1, v3

    .line 2675
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSettings_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 2676
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->phoneTemplate_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 2677
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->tabletTemplate_:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 2678
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 2679
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-nez v3, :cond_4

    :goto_4
    add-int v0, v1, v2

    .line 2680
    return v0

    .line 2675
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSettings_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 2676
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->phoneTemplate_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 2677
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->tabletTemplate_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 2678
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->hashCode()I

    move-result v1

    goto :goto_3

    .line 2679
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->hashCode()I

    move-result v2

    goto :goto_4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2741
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2742
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2746
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2747
    :sswitch_0
    return-object p0

    .line 2752
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2753
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 2759
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSystem_:I

    .line 2760
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    goto :goto_0

    .line 2762
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSystem_:I

    goto :goto_0

    .line 2767
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSettings_:Ljava/lang/String;

    .line 2768
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    goto :goto_0

    .line 2772
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->phoneTemplate_:Ljava/lang/String;

    .line 2773
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    goto :goto_0

    .line 2777
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->tabletTemplate_:Ljava/lang/String;

    .line 2778
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    goto :goto_0

    .line 2782
    :sswitch_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-nez v2, :cond_3

    .line 2783
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    .line 2785
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2789
    :sswitch_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-nez v2, :cond_4

    .line 2790
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    .line 2792
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2742
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x42 -> :sswitch_5
        0x4a -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2489
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2685
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2686
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSystem_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2688
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2689
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->adSettings_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2691
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2692
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->phoneTemplate_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2694
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 2695
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->tabletTemplate_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2697
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-eqz v0, :cond_4

    .line 2698
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->pubSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2700
    :cond_4
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    if-eqz v0, :cond_5

    .line 2701
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->googleSold_:Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2703
    :cond_5
    return-void
.end method
