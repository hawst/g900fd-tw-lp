.class final Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$3;
.super Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.source "MediaItemsSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->restrictToSingleFieldFilter(Ljava/lang/String;)Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$fieldId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Ljava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 133
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$3;->val$fieldId:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 3
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 136
    sget v1, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_MEDIA_ITEM:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 137
    .local v0, "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->fieldId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$3;->val$fieldId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method
