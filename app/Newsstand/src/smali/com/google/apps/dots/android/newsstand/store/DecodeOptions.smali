.class public Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;
.super Ljava/lang/Object;
.source "DecodeOptions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;


# instance fields
.field public final bitmapConfig:Landroid/graphics/Bitmap$Config;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->newBuilder()Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;->build()Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->DEFAULT:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    return-void
.end method

.method private constructor <init>(Landroid/graphics/Bitmap$Config;)V
    .locals 0
    .param p1, "bitmapConfig"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    .line 21
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/Bitmap$Config;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/graphics/Bitmap$Config;
    .param p2, "x1"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$1;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;-><init>(Landroid/graphics/Bitmap$Config;)V

    return-void
.end method

.method public static newBuilder()Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 25
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 26
    check-cast v0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    .line 27
    .local v0, "decodeOptions":Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 29
    .end local v0    # "decodeOptions":Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 34
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 39
    const-string v0, "bitmapConfig: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
