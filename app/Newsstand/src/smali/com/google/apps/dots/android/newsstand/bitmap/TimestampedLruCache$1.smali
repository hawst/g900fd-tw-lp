.class Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;
.super Landroid/support/v4/util/LruCache;
.source "TimestampedLruCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;-><init>(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<TK;",
        "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;
    .param p2, "x0"    # I

    .prologue
    .line 39
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;->this$0:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    invoke-direct {p0, p2}, Landroid/support/v4/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected entryRemoved(ZLjava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)V
    .locals 3
    .param p1, "evicted"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTK;",
            "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper",
            "<TV;>;",
            "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;"
    .local p2, "key":Ljava/lang/Object;, "TK;"
    .local p3, "oldWrapper":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<TV;>;"
    .local p4, "newWrapper":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<TV;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;->this$0:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;->this$0:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    # invokes: Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->unwrap(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;
    invoke-static {v1, p3}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->access$000(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;->this$0:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    # invokes: Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->unwrap(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;
    invoke-static {v2, p4}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->access$000(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 44
    return-void
.end method

.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 39
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;"
    check-cast p3, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;

    check-cast p4, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;->entryRemoved(ZLjava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)V

    return-void
.end method

.method protected sizeOf(Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper",
            "<TV;>;)I"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "wrapper":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<TV;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;->this$0:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;->this$0:Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;

    # invokes: Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->unwrap(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;
    invoke-static {v1, p2}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->access$000(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 39
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;"
    check-cast p2, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;->sizeOf(Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)I

    move-result v0

    return v0
.end method
