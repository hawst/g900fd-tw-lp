.class public Lcom/google/apps/dots/android/newsstand/analytics/InternalGATracker;
.super Lcom/google/apps/dots/android/newsstand/analytics/GATracker;
.source "InternalGATracker.java"


# instance fields
.field private final systemAnalyticsId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/analytics/GoogleAnalytics;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "googleAnalytics"    # Lcom/google/android/gms/analytics/GoogleAnalytics;
    .param p3, "util"    # Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;-><init>(Landroid/content/Context;Lcom/google/android/gms/analytics/GoogleAnalytics;)V

    .line 18
    invoke-virtual {p3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getSystemAnalyticsId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/InternalGATracker;->systemAnalyticsId:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public trackEvent(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "trackingId"    # Ljava/lang/String;
    .param p3, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/InternalGATracker;->systemAnalyticsId:Ljava/lang/String;

    invoke-super {p0, p1, v0, p3}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->trackEvent(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V

    .line 24
    return-void
.end method
