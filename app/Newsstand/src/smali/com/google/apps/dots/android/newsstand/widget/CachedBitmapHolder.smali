.class public Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;
.super Ljava/lang/Object;
.source "CachedBitmapHolder.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;


# instance fields
.field private acquired:Z

.field private final attachmentId:Ljava/lang/String;

.field private cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

.field private final transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

.field private final views:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)V
    .locals 1
    .param p1, "attachmentId"    # Ljava/lang/String;
    .param p2, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->acquired:Z

    .line 27
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->views:Ljava/util/Set;

    .line 32
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->attachmentId:Ljava/lang/String;

    .line 33
    if-eqz p2, :cond_0

    .end local p2    # "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    :goto_0
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 34
    return-void

    .line 33
    .restart local p2    # "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDefaultTransform()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object p2

    goto :goto_0
.end method


# virtual methods
.method public acquire()V
    .locals 4

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->acquired:Z

    if-nez v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->attachmentId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentViewCache()Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->attachmentId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->DEFAULT:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->getBitmap(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .line 75
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->acquired:Z

    .line 77
    :cond_1
    return-void
.end method

.method public addView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->views:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    return-void
.end method

.method public getAttachmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->attachmentId:Ljava/lang/String;

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->acquired:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAcquired()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->acquired:Z

    return v0
.end method

.method public onCachedBitmapMissing()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public onCachedBitmapReady(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)V
    .locals 3
    .param p1, "cachedBitmap"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .prologue
    .line 55
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->views:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 56
    .local v0, "view":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 58
    .end local v0    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->acquired:Z

    if-eqz v0, :cond_0

    .line 82
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentViewCache()Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    invoke-virtual {v0, v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->releaseBitmap(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->acquired:Z

    .line 88
    :cond_0
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/CachedBitmapHolder;->views:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method
