.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncFailedEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;
.source "SyncFailedEvent.java"


# instance fields
.field private final failedCount:I

.field private final lengthOfEventInMillis:J

.field private final successCount:I


# direct methods
.method public constructor <init>(IILcom/google/apps/dots/android/newsstand/async/AsyncToken;J)V
    .locals 0
    .param p1, "failedCount"    # I
    .param p2, "successCount"    # I
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "lengthOfEventInMillis"    # J

    .prologue
    .line 22
    invoke-direct {p0, p3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 23
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncFailedEvent;->failedCount:I

    .line 24
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncFailedEvent;->successCount:I

    .line 25
    iput-wide p4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncFailedEvent;->lengthOfEventInMillis:J

    .line 26
    return-void
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 6
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 31
    const-string v0, "SyncMetadata"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "failedSyncCount = %d, successSyncCount = %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncFailedEvent;->failedCount:I

    .line 33
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncFailedEvent;->successCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 32
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 31
    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v0, "Sync Failed"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncFailedEvent;->lengthOfEventInMillis:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 36
    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setValue(J)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 38
    return-object p1
.end method
