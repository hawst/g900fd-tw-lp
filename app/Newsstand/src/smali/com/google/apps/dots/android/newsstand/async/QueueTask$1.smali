.class Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;
.super Ljava/lang/Object;
.source "QueueTask.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/QueueTask;->execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/async/QueueTask;

.field final synthetic val$optAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$runOnPostExecute:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/QueueTask;ZLcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/async/QueueTask;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/QueueTask;

    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;->val$runOnPostExecute:Z

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;->val$optAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/QueueTask;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->doInBackground()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;->val$runOnPostExecute:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;->val$optAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/QueueTask;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;->val$optAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 70
    :cond_0
    const/4 v1, 0x0

    return-object v1

    .line 57
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/google/apps/dots/android/newsstand/async/QueueTask;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Caught an exception on queue: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/async/QueueTask$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/QueueTask;

    # getter for: Lcom/google/apps/dots/android/newsstand/async/QueueTask;->queue:Lcom/google/apps/dots/android/newsstand/async/Queue;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->access$000(Lcom/google/apps/dots/android/newsstand/async/QueueTask;)Lcom/google/apps/dots/android/newsstand/async/Queue;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 60
    throw v0
.end method
