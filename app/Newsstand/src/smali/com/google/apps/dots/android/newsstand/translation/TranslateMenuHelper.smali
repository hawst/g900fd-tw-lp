.class public Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;
.super Ljava/lang/Object;
.source "TranslateMenuHelper.java"


# instance fields
.field private fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V
    .locals 0
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .line 27
    return-void
.end method


# virtual methods
.method public onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p3, "optPostId"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 32
    .local v0, "account":Landroid/accounts/Account;
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->isTranslated()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 33
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_undo_translate:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 34
    .local v2, "undoTranslateItem":Landroid/view/MenuItem;
    if-eqz v2, :cond_0

    .line 35
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$1;

    invoke-direct {v4, p0, v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V

    .line 36
    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 57
    .end local v2    # "undoTranslateItem":Landroid/view/MenuItem;
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_translate:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 46
    .local v1, "translateItem":Landroid/view/MenuItem;
    if-eqz v1, :cond_0

    .line 47
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->supportsTranslation()Z

    move-result v3

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$2;

    invoke-direct {v4, p0, v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$2;-><init>(Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V

    .line 48
    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method translate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p3, "optPostId"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;->show(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method untranslate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p3, "optPostId"    # Ljava/lang/String;

    .prologue
    .line 67
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    .line 68
    .local v0, "activity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    invoke-static {v0, p1, p2, v3}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->translateNewsSubscriptionIfSubscribed(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V

    .line 70
    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    .line 71
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getTranslatedEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 72
    .local v1, "untranslatedEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-nez p3, :cond_0

    .line 73
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-direct {v3, v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 74
    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v3

    .line 75
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    .line 86
    :goto_0
    return-void

    .line 77
    :cond_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    invoke-static {p3, v3}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->addOrReplaceTargetTranslationLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 81
    .local v2, "untranslatedPostId":Ljava/lang/String;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    invoke-direct {v3, v0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 82
    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v3

    .line 83
    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v3

    .line 84
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->start()V

    goto :goto_0
.end method
