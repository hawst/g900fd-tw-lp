.class Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient$TempWebViewClient;
.super Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;
.source "DotsWebChromeClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TempWebViewClient"
.end annotation


# instance fields
.field private final parentView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/webkit/WebView;)V
    .locals 1
    .param p1, "parentView"    # Landroid/webkit/WebView;

    .prologue
    .line 128
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 129
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient$TempWebViewClient;->parentView:Landroid/webkit/WebView;

    .line 130
    return-void
.end method


# virtual methods
.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 135
    const/4 v0, 0x0

    return-object v0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 140
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->access$000()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "TempWebViewClient.shouldOverrideUrlLoading %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient$TempWebViewClient$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient$TempWebViewClient$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient$TempWebViewClient;Landroid/webkit/WebView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 151
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient$TempWebViewClient;->parentView:Landroid/webkit/WebView;

    invoke-super {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
