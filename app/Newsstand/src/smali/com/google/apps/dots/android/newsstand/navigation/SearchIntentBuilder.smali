.class public Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "SearchIntentBuilder.java"


# instance fields
.field private query:Ljava/lang/String;

.field private searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 20
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 34
    const-class v2, Lcom/google/apps/dots/android/newsstand/search/SearchActivity;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 35
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;->query:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    invoke-direct {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;)V

    .line 36
    .local v1, "state":Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;
    const-string v2, "SearchFragmentState_state"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 37
    iget-object v2, v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 38
    const-string v2, "query"

    iget-object v3, v1, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    :cond_0
    return-object v0
.end method

.method public setQuery(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;->query:Ljava/lang/String;

    .line 24
    return-object p0
.end method

.method public setSearchType(Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;)Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;
    .locals 0
    .param p1, "searchType"    # Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;->searchType:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    .line 29
    return-object p0
.end method
