.class Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager$1;
.super Landroid/content/BroadcastReceiver;
.source "NSConnectivityManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager$1;->this$0:Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 50
    const-string v3, "noConnectivity"

    .line 51
    invoke-virtual {p2, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x1

    .line 52
    .local v1, "newHasConnectivity":Z
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager$1;->this$0:Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    # getter for: Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->hasConnectivity:Z
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->access$000(Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;)Z

    move-result v3

    if-eq v1, v3, :cond_2

    .line 53
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager$1;->this$0:Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    # setter for: Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->hasConnectivity:Z
    invoke-static {v3, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->access$002(Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;Z)Z

    .line 55
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager$1;->this$0:Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    # getter for: Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->hasConnectivity:Z
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->access$000(Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 63
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager$1;->this$0:Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    # getter for: Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->connectivityListeners:Ljava/util/LinkedHashSet;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->access$100(Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;)Ljava/util/LinkedHashSet;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/collect/Sets;->newLinkedHashSet(Ljava/lang/Iterable;)Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 64
    .local v0, "listeners":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Runnable;>;"
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    .line 65
    .local v2, "runnable":Ljava/lang/Runnable;
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 68
    .end local v0    # "listeners":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Runnable;>;"
    .end local v2    # "runnable":Ljava/lang/Runnable;
    :cond_2
    return-void
.end method
