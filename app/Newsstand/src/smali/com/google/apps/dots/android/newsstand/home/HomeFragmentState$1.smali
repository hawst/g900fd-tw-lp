.class final Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState$1;
.super Ljava/lang/Object;
.source "HomeFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 67
    const-class v2, Landroid/accounts/Account;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 68
    .local v0, "account":Landroid/accounts/Account;
    const-class v2, Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 69
    .local v1, "homePage":Lcom/google/apps/dots/android/newsstand/home/HomePage;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    invoke-direct {v2, v1, v0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/home/HomePage;Landroid/accounts/Account;)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 74
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    move-result-object v0

    return-object v0
.end method
