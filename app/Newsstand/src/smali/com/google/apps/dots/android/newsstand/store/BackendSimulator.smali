.class public Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;
.super Ljava/lang/Object;
.source "BackendSimulator.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method

.method private getArchiveAppNode(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 9
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p3, "library"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .prologue
    .line 156
    iget-object v2, p3, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 157
    .local v2, "familyNodes":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v7, v2

    if-ge v3, v7, :cond_2

    .line 158
    aget-object v7, v2, v3

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v7

    const/4 v8, 0x4

    if-ne v7, v8, :cond_1

    .line 159
    aget-object v7, v2, v3

    iget-object v6, v7, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 160
    .local v6, "toAddNodes":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    iget-object v0, p2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    .line 161
    .local v0, "appFamilyId":Ljava/lang/String;
    aget-object v7, v2, v3

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v7

    iget-object v1, v7, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 162
    .local v1, "currNodeFamilyId":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 163
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    array-length v7, v6

    if-ge v4, v7, :cond_1

    .line 164
    aget-object v5, v6, v4

    .line 165
    .local v5, "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppSummary()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 166
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_0

    .line 167
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v7

    iget-object v7, v7, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 174
    .end local v0    # "appFamilyId":Ljava/lang/String;
    .end local v1    # "currNodeFamilyId":Ljava/lang/String;
    .end local v4    # "j":I
    .end local v5    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .end local v6    # "toAddNodes":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :goto_2
    return-object v5

    .line 163
    .restart local v0    # "appFamilyId":Ljava/lang/String;
    .restart local v1    # "currNodeFamilyId":Ljava/lang/String;
    .restart local v4    # "j":I
    .restart local v5    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .restart local v6    # "toAddNodes":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 157
    .end local v0    # "appFamilyId":Ljava/lang/String;
    .end local v1    # "currNodeFamilyId":Ljava/lang/String;
    .end local v4    # "j":I
    .end local v5    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .end local v6    # "toAddNodes":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 174
    :cond_2
    const/4 v5, 0x0

    goto :goto_2
.end method

.method private handleArchiveMagazine(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V
    .locals 11
    .param p1, "magazineLibrary"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .param p2, "action"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v10, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 178
    invoke-virtual {p3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/collect/Iterables;->getLast(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 179
    .local v0, "appId":Ljava/lang/String;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "handleArchiveInBS %s/%s, appId: %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->methodString(Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v5

    aput-object p3, v8, v4

    aput-object v0, v8, v10

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 180
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getMethod()I

    move-result v6

    if-nez v6, :cond_1

    move v3, v4

    .line 181
    .local v3, "toArchive":Z
    :goto_0
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getSimulationHint()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v2

    .line 182
    .local v2, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    invoke-direct {p0, v0, v2, p1}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->getArchiveAppNode(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v1

    .line 183
    .local v1, "appNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    if-eqz v1, :cond_0

    .line 184
    sget-object v6, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "handleArchiveInBS setting app %s to archived %s"

    new-array v8, v10, [Ljava/lang/Object;

    .line 185
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v8, v4

    .line 184
    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 186
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getPurchaseSummary()Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->setIsArchived(Z)Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    .line 188
    :cond_0
    return-void

    .end local v1    # "appNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .end local v2    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .end local v3    # "toArchive":Z
    :cond_1
    move v3, v5

    .line 180
    goto :goto_0
.end method

.method private handleNewsSubscription(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V
    .locals 11
    .param p1, "newsLibrary"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .param p2, "action"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 191
    invoke-virtual {p3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/collect/Iterables;->getLast(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 192
    .local v1, "appId":Ljava/lang/String;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "handleNewsSubscription %s/%s, appId: %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->methodString(Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v5

    aput-object p3, v8, v10

    const/4 v9, 0x2

    aput-object v1, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getMethod()I

    move-result v6

    if-ne v6, v10, :cond_3

    .line 194
    new-instance v5, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    invoke-direct {v5, p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v6, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$1;

    invoke-direct {v6, p0, v1}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 206
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v5, v5

    if-ge v2, v5, :cond_1

    .line 207
    iget-object v5, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v3, v5, v2

    .line 208
    .local v3, "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppFamilySummary()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v3, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v5, v5

    if-nez v5, :cond_0

    .line 209
    iget-object v5, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    const/4 v6, 0x0

    aput-object v6, v5, v2

    .line 206
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 212
    .end local v3    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_1
    const-class v5, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->filterNonNull(Ljava/lang/Class;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v5, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 236
    .end local v2    # "i":I
    :cond_2
    :goto_1
    return-void

    .line 214
    :cond_3
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getMethod()I

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->hasSimulationHint()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 215
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getSimulationHint()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v4

    .line 218
    .local v4, "simulationHint":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    const/4 v0, 0x0

    .line 219
    .local v0, "appFamilyExists":Z
    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v7, v6

    :goto_2
    if-ge v5, v7, :cond_4

    aget-object v3, v6, v5

    .line 220
    .restart local v3    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppFamilySummary()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppFamilySummary()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 221
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v8

    iget-object v8, v8, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 222
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v9

    iget-object v9, v9, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 221
    invoke-static {v8, v9}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 223
    iget-object v5, v3, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iget-object v6, v4, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    const-class v7, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-static {v5, v6, v7}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v5, v3, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 224
    const/4 v0, 0x1

    .line 231
    .end local v3    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_4
    if-nez v0, :cond_2

    .line 232
    iget-object v5, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 233
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getSimulationHint()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v6

    .line 232
    invoke-static {v5, v6}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v5, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    goto :goto_1

    .line 219
    .restart local v3    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method private handleNewsSubscriptionReorder(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V
    .locals 12
    .param p1, "newsLibrary"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .param p2, "action"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v11, 0x0

    .line 239
    invoke-virtual {p3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/collect/Iterables;->getLast(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 240
    .local v1, "moveId":Ljava/lang/String;
    const-string v6, "pivotId"

    invoke-virtual {p3, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 242
    .local v5, "pivotId":Ljava/lang/String;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "handleNewsSubscriptionReorder %s/%s, move: %s, pivot: %s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    .line 243
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->methodString(Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    const/4 v9, 0x1

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getUri()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    aput-object v1, v8, v9

    const/4 v9, 0x3

    aput-object v5, v8, v9

    .line 242
    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 246
    const/4 v2, 0x0

    .line 247
    .local v2, "moveNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-static {v6}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 248
    .local v4, "nodes":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 249
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 250
    .local v3, "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppFamilySummary()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 251
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v6

    iget-object v6, v6, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-static {v6, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 252
    invoke-interface {v4, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "moveNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    check-cast v2, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 256
    .end local v3    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .restart local v2    # "moveNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_0
    if-eqz v2, :cond_1

    .line 257
    if-nez v5, :cond_3

    .line 259
    invoke-interface {v4, v11, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 272
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-interface {v4, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v6, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 273
    return-void

    .line 248
    .restart local v3    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    .end local v3    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_3
    const/4 v0, 0x0

    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 263
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 264
    .restart local v3    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppFamilySummary()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 265
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v6

    iget-object v6, v6, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-static {v6, v5}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 266
    add-int/lit8 v6, v0, 0x1

    invoke-interface {v4, v6, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 262
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private handleNewsSubscriptionTranslate(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V
    .locals 12
    .param p1, "newsLibrary"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .param p2, "action"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x0

    .line 276
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getMethod()I

    move-result v7

    if-eqz v7, :cond_0

    .line 300
    :goto_0
    return-void

    .line 280
    :cond_0
    invoke-virtual {p3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    invoke-static {v7}, Lcom/google/common/collect/Iterables;->getLast(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 281
    .local v0, "appId":Ljava/lang/String;
    const-string v7, "translate"

    invoke-virtual {p3, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 282
    .local v3, "languageCode":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getSimulationHint()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v6

    .line 283
    .local v6, "simulationHint":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    sget-object v7, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v9, "handleNewsSubscriptionTranslate %s/%s, languageCode: %s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->methodString(Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    const/4 v11, 0x1

    aput-object p3, v10, v11

    const/4 v11, 0x2

    aput-object v3, v10, v11

    invoke-virtual {v7, v9, v10}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    iget-object v7, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-static {v7}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v5

    .line 288
    .local v5, "nodes":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 289
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 290
    .local v4, "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v7

    const/4 v9, 0x4

    if-ne v7, v9, :cond_1

    .line 291
    iget-object v9, v4, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v10, v9

    move v7, v8

    :goto_2
    if-ge v7, v10, :cond_1

    aget-object v1, v9, v7

    .line 292
    .local v1, "childNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppSummary()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v11

    iget-object v11, v11, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 293
    invoke-interface {v5, v2, v6}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 288
    .end local v1    # "childNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 291
    .restart local v1    # "childNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 299
    .end local v1    # "childNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .end local v4    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    new-array v7, v7, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-interface {v5, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v7, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    goto :goto_0
.end method

.method private handleOffer(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V
    .locals 10
    .param p1, "offerRoot"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .param p2, "action"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 376
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getSimulationHint()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v3

    .line 377
    .local v3, "simulationHint":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getOfferSummary()Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    move-result-object v4

    .line 378
    .local v4, "simulationOfferSummary":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "handleOffer %s/%s, offerId: %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->methodString(Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object p3, v7, v8

    const/4 v8, 0x2

    .line 379
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 378
    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 382
    iget-object v5, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    .line 383
    .local v2, "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 384
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 385
    .local v1, "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v1, v5}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->isOfferNodeForOfferId(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 386
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 389
    .end local v1    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-interface {v2, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v5, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 390
    return-void
.end method

.method private handlePostRead(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V
    .locals 12
    .param p1, "readStatesRoot"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .param p2, "action"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 90
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getSimulationHint()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v4

    .line 91
    .local v4, "simulationHint":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getPostReadState()Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v5

    .line 92
    .local v5, "simulationReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    sget-object v7, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "handlePostRead %s/%s, postId: %s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->methodString(Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object p3, v9, v10

    const/4 v10, 0x2

    .line 93
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getPostId()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 92
    invoke-virtual {v7, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    iget-object v7, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-static {v7}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    .line 96
    .local v3, "readStates":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 97
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 98
    .local v1, "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getPostId()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v1, v7}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->isReadNodeForPostId(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 99
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getMethod()I

    move-result v7

    if-nez v7, :cond_3

    .line 100
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getPostReadState()Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v2

    .line 103
    .local v2, "oldPostReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getIsPostInMeteredSection()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 104
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getWasEditionOwnedWhenRead()Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v6, 0x1

    .line 105
    .local v6, "wasPostReadStateOriginallyMetered":Z
    :goto_1
    if-nez v6, :cond_1

    .line 106
    invoke-virtual {v1, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setPostReadState(Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 121
    .end local v1    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .end local v2    # "oldPostReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .end local v6    # "wasPostReadStateOriginallyMetered":Z
    :cond_1
    :goto_2
    return-void

    .line 104
    .restart local v1    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .restart local v2    # "oldPostReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 109
    .end local v2    # "oldPostReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    :cond_3
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getMethod()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 110
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 114
    .end local v1    # "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    new-array v7, v7, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-interface {v3, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v7, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 116
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getMethod()I

    move-result v7

    if-nez v7, :cond_1

    .line 118
    iget-object v7, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 119
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getSimulationHint()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v8

    .line 118
    invoke-static {v7, v8}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v7, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    goto :goto_2
.end method

.method private handleSavedPost(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V
    .locals 8
    .param p1, "savedRoot"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .param p2, "action"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 327
    invoke-virtual {p3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/Iterables;->getLast(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 328
    .local v1, "postId":Ljava/lang/String;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "handleSavedPost %s/%s, postId: %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->methodString(Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object p3, v4, v6

    const/4 v5, 0x2

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 329
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getMethod()I

    move-result v2

    if-ne v2, v6, :cond_1

    .line 330
    new-instance v2, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v3, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$2;

    invoke-direct {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$2;-><init>(Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getMethod()I

    move-result v2

    if-nez v2, :cond_0

    .line 339
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->hasSimulationHint()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 340
    new-array v0, v6, [Z

    .line 341
    .local v0, "alreadySaved":[Z
    new-instance v2, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v3, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$3;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator$3;-><init>(Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;Ljava/lang/String;[Z)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 350
    aget-boolean v2, v0, v7

    if-nez v2, :cond_0

    .line 352
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getSimulationHint()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v2

    iget-object v3, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 351
    invoke-static {v2, v3}, Lcom/google/common/collect/ObjectArrays;->concat(Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v2, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    goto :goto_0
.end method

.method private isOfferNodeForOfferId(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;Ljava/lang/String;)Z
    .locals 2
    .param p1, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .param p2, "offerId"    # Ljava/lang/String;

    .prologue
    .line 371
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 372
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getOfferSummary()Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isReadNodeForPostId(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;Ljava/lang/String;)Z
    .locals 2
    .param p1, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .param p2, "postId"    # Ljava/lang/String;

    .prologue
    .line 85
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 86
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getPostReadState()Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getPostId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeAddSubscriptionHint(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 5
    .param p0, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 142
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    const/4 v2, 0x4

    .line 143
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setType(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v1

    .line 144
    invoke-virtual {v1, p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setAppFamilySummary(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v0

    .line 145
    .local v0, "node":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    const/4 v4, 0x3

    .line 147
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setType(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v3

    .line 148
    invoke-virtual {v3, p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setAppSummary(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v1, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 150
    return-object v0
.end method

.method public static makeArchiveHint(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 2
    .param p0, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 358
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    const/4 v1, 0x3

    .line 359
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setType(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v0

    .line 360
    invoke-virtual {v0, p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setAppSummary(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v0

    return-object v0
.end method

.method public static makeOfferHint(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 2
    .param p0, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 365
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    const/16 v1, 0x8

    .line 366
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setType(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v0

    .line 367
    invoke-virtual {v0, p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setOfferSummary(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v0

    return-object v0
.end method

.method public static makePostReadStateHint(Ljava/lang/String;Ljava/lang/Float;Z)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 6
    .param p0, "postId"    # Ljava/lang/String;
    .param p1, "pageFraction"    # Ljava/lang/Float;
    .param p2, "isMetered"    # Z

    .prologue
    const/4 v1, 0x1

    .line 124
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;-><init>()V

    .line 125
    invoke-virtual {v2, p0}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v2

    .line 126
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->setUpdateTimestamp(J)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v2

    .line 127
    invoke-virtual {v2, v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->setState(I)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v2

    .line 128
    invoke-virtual {v2, p2}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->setIsPostInMeteredSection(Z)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v2

    if-nez p2, :cond_1

    .line 129
    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->setWasEditionOwnedWhenRead(Z)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v0

    .line 130
    .local v0, "postReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    if-eqz p1, :cond_0

    .line 131
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->setPageFraction(F)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .line 133
    :cond_0
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    const/4 v2, 0x7

    .line 134
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setType(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v1

    .line 135
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setPostReadState(Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v1

    return-object v1

    .line 128
    .end local v0    # "postReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static makeSavePostHint(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 2
    .param p0, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 321
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    const/4 v1, 0x1

    .line 322
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setType(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v0

    .line 323
    invoke-virtual {v0, p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setPostSummary(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v0

    return-object v0
.end method

.method public static makeSubscriptionTranslationHint(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 6
    .param p0, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 305
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->getTranslatedEditionSummary(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v3

    .line 308
    .local v3, "translatedEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    new-instance v4, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setType(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v4

    iget-object v5, v3, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 309
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setAppFamilySummary(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v1

    .line 311
    .local v1, "translatedAppFamilySummaryNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    new-instance v4, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setType(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v4

    iget-object v5, v3, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 312
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->setAppSummary(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v2

    .line 313
    .local v2, "translatedAppSummaryNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    const/4 v4, 0x1

    new-array v0, v4, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    const/4 v4, 0x0

    aput-object v2, v0, v4

    .line 314
    .local v0, "appSummarychildren":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    iput-object v0, v1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 316
    return-object v1
.end method

.method private methodString(Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)Ljava/lang/String;
    .locals 1
    .param p1, "action"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .prologue
    .line 46
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getMethod()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 52
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 48
    :pswitch_0
    const-string v0, "POST"

    goto :goto_0

    .line 50
    :pswitch_1
    const-string v0, "DELETE"

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public applyActionTo(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V
    .locals 6
    .param p1, "root"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .param p2, "action"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .prologue
    .line 58
    sget-object v1, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Applying %s/%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->methodString(Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getUri()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 60
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "people/me/read-states"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->handlePostRead(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "people/me/news_v2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 63
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "people/me/curations"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 64
    :cond_2
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "reorder"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 65
    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->handleNewsSubscriptionReorder(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V

    goto :goto_0

    .line 66
    :cond_3
    invoke-virtual {v0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v1

    const-string v2, "translate"

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 67
    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->handleNewsSubscriptionTranslate(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V

    goto :goto_0

    .line 69
    :cond_4
    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->handleNewsSubscription(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V

    goto :goto_0

    .line 71
    :cond_5
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/people/me/saved"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 72
    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->handleSavedPost(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V

    goto :goto_0

    .line 73
    :cond_6
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "people/me/magazines"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 74
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "archive"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->handleArchiveMagazine(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V

    goto :goto_0

    .line 77
    :cond_7
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/people/me/offers/promo/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->handleOffer(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public applyActionsTo(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;)V
    .locals 4
    .param p1, "root"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .param p2, "mutationLog"    # Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    .prologue
    .line 40
    iget-object v2, p2, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 41
    .local v0, "action":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->applyActionTo(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    .line 40
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    .end local v0    # "action":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    :cond_0
    return-void
.end method
