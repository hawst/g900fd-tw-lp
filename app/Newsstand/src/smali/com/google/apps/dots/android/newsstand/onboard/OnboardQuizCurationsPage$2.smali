.class Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "OnboardQuizCurationsPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->getQuizItemDataList()Lcom/google/android/libraries/bind/data/DataList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private selectedItemIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;
    .param p2, "arg0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 6
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 110
    sget v4, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_ITEM_ID:I

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    .line 111
    .local v1, "itemId":Ljava/lang/String;
    sget v4, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_TITLE:I

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v3

    .line 112
    .local v3, "title":Ljava/lang/String;
    sget v4, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_DEFAULT_ON:I

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v0

    .line 114
    .local v0, "defaultOn":Z
    const/4 v2, 0x0

    .line 116
    .local v2, "selected":Z
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;->selectedItemIds:Ljava/util/Set;

    if-eqz v4, :cond_0

    .line 119
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->mModifiedByUser:Z
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->access$300(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;)Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v0, :cond_1

    .line 122
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;->selectedItemIds:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 123
    const/4 v2, 0x1

    .line 129
    :cond_0
    :goto_0
    sget v4, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SELECTED:I

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 130
    sget v4, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_CLICK_LISTENER:I

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    # invokes: Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->getItemClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    invoke-static {v5, v1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->access$500(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 131
    sget v4, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_CONTENT_DESCRIPTION:I

    .line 132
    invoke-static {v3, v2}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->getItemContentDescription(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 131
    invoke-virtual {p1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 134
    const/4 v4, 0x1

    return v4

    .line 124
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->mModifiedByUser:Z
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->access$400(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 125
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;->selectedItemIds:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method public onPreFilter()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    # invokes: Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->getSelectedItemIds()Ljava/util/Set;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->access$200(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;->selectedItemIds:Ljava/util/Set;

    .line 104
    return-void
.end method
