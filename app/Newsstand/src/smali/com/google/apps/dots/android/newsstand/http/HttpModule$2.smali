.class final Lcom/google/apps/dots/android/newsstand/http/HttpModule$2;
.super Ljava/lang/Object;
.source "HttpModule.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/http/HttpModule;->provideHttpClient(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field final synthetic val$util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$2;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$2;->val$util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$2;->val$prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 90
    # invokes: Lcom/google/apps/dots/android/newsstand/http/HttpModule;->createGoogleHttpClient()Lcom/google/android/gms/http/GoogleHttpClient;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/HttpModule;->access$000()Lcom/google/android/gms/http/GoogleHttpClient;

    move-result-object v4

    .line 91
    .local v4, "httpClient":Lorg/apache/http/client/HttpClient;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$2;->val$util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$2;->val$prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$2;->val$context:Landroid/content/Context;

    # invokes: Lcom/google/apps/dots/android/newsstand/http/HttpModule;->getUserAgent(Landroid/content/Context;)Ljava/lang/String;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/http/HttpModule;->access$100(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lorg/apache/http/client/HttpClient;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/http/HttpModule$2;->call()Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;

    move-result-object v0

    return-object v0
.end method
