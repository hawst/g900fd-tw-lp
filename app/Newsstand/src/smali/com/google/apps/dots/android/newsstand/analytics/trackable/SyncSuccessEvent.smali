.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSuccessEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;
.source "SyncSuccessEvent.java"


# instance fields
.field private final lengthOfEventInMillis:J

.field private final successCount:I


# direct methods
.method public constructor <init>(ILcom/google/apps/dots/android/newsstand/async/AsyncToken;J)V
    .locals 1
    .param p1, "successCount"    # I
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "lengthOfEventInMillis"    # J

    .prologue
    .line 18
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/DebugAnalyticsBase;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 19
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSuccessEvent;->successCount:I

    .line 20
    iput-wide p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSuccessEvent;->lengthOfEventInMillis:J

    .line 21
    return-void
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 6
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 26
    const-string v0, "SyncMetadata"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "successSyncCount = %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSuccessEvent;->successCount:I

    .line 27
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 26
    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v0, "Sync Success"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 29
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncSuccessEvent;->lengthOfEventInMillis:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setValue(J)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 30
    return-object p1
.end method
