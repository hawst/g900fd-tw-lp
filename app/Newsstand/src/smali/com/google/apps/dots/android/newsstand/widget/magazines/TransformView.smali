.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;
.super Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;
.source "TransformView.java"


# static fields
.field private static final SCROLLER_FAKE_DISTANCE:I = 0x100


# instance fields
.field private interpolator:Lcom/google/apps/dots/android/newsstand/widget/magazines/MatrixInterpolator;

.field private final matrix:Landroid/graphics/Matrix;

.field private final pauseQueuesScrollDistance:F

.field private final points:[F

.field private preTransformedMotionEvent:Landroid/view/MotionEvent;

.field private scroller:Landroid/widget/Scroller;

.field private final tempRect:Landroid/graphics/RectF;

.field private transformedMotionEvent:Landroid/view/MotionEvent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V

    .line 30
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->matrix:Landroid/graphics/Matrix;

    .line 44
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->tempRect:Landroid/graphics/RectF;

    .line 45
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->points:[F

    .line 49
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$dimen;->pause_queues_scroll_distance:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->pauseQueuesScrollDistance:F

    .line 51
    return-void
.end method


# virtual methods
.method public animateScrollToPoint(FFF)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "scale"    # F

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v0

    .line 205
    .local v0, "contentArea":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->tempRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, p3

    add-float/2addr v2, p1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v3, p3

    add-float/2addr v3, p2

    invoke-virtual {v1, p1, p2, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 206
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->tempRect:Landroid/graphics/RectF;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->animateScrollToRect(Landroid/graphics/RectF;)V

    .line 207
    return-void
.end method

.method public animateScrollToRect(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "targetRect"    # Landroid/graphics/RectF;

    .prologue
    .line 226
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->computeFitRectMatrix(Landroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->animateScrollToTransform(Landroid/graphics/Matrix;)V

    .line 227
    return-void
.end method

.method public animateScrollToTransform(Landroid/graphics/Matrix;)V
    .locals 4
    .param p1, "target"    # Landroid/graphics/Matrix;

    .prologue
    const/4 v3, 0x0

    .line 231
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->cancelAnimatingScroll()V

    .line 232
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 233
    .local v0, "tempMatrix":Landroid/graphics/Matrix;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getTransform(Landroid/graphics/Matrix;)V

    .line 234
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/MatrixInterpolator;

    invoke-direct {v1, v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MatrixInterpolator;-><init>(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->interpolator:Lcom/google/apps/dots/android/newsstand/widget/magazines/MatrixInterpolator;

    .line 235
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/Matrix;)V

    .line 236
    new-instance v1, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->scroller:Landroid/widget/Scroller;

    .line 237
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->scroller:Landroid/widget/Scroller;

    const/16 v2, 0x100

    invoke-virtual {v1, v3, v3, v2, v3}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 238
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->invalidate()V

    .line 239
    return-void
.end method

.method public applyLayoutToViewCoordsTransform(Landroid/graphics/Matrix;)V
    .locals 2
    .param p1, "output"    # Landroid/graphics/Matrix;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 116
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollX()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollY()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 117
    return-void
.end method

.method public cancelAnimatingScroll()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 246
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->interpolator:Lcom/google/apps/dots/android/newsstand/widget/magazines/MatrixInterpolator;

    .line 247
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->scroller:Landroid/widget/Scroller;

    .line 248
    return-void
.end method

.method protected computeFitRectMatrix(Landroid/graphics/RectF;)Landroid/graphics/Matrix;
    .locals 4
    .param p1, "targetRect"    # Landroid/graphics/RectF;

    .prologue
    .line 211
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 212
    .local v1, "tempMatrix":Landroid/graphics/Matrix;
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 213
    .local v0, "output":Landroid/graphics/Matrix;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v2

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->START:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v2, p1, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 214
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 215
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/Matrix;)V

    .line 216
    return-object v0
.end method

.method public computeScroll()V
    .locals 4

    .prologue
    .line 252
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->scroller:Landroid/widget/Scroller;

    if-eqz v2, :cond_1

    .line 253
    const/high16 v0, 0x3f800000    # 1.0f

    .line 254
    .local v0, "t":F
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 255
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x43800000    # 256.0f

    div-float v0, v2, v3

    .line 257
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 258
    .local v1, "tempMatrix":Landroid/graphics/Matrix;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->interpolator:Lcom/google/apps/dots/android/newsstand/widget/magazines/MatrixInterpolator;

    invoke-virtual {v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MatrixInterpolator;->getInterpolation(FLandroid/graphics/Matrix;)V

    .line 259
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->setTransform(Landroid/graphics/Matrix;)V

    .line 260
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/Matrix;)V

    .line 261
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_1

    .line 262
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->cancelAnimatingScroll()V

    .line 265
    .end local v0    # "t":F
    .end local v1    # "tempMatrix":Landroid/graphics/Matrix;
    :cond_1
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 162
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->save(I)I

    move-result v0

    .line 163
    .local v0, "save":I
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 164
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 165
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 166
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 171
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->preTransformedMotionEvent:Landroid/view/MotionEvent;

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->preTransformedMotionEvent:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 173
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->transformedMotionEvent:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 175
    :cond_0
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->preTransformedMotionEvent:Landroid/view/MotionEvent;

    .line 176
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 177
    .local v0, "tempInverse":Landroid/graphics/Matrix;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getInverseTransform(Landroid/graphics/Matrix;)V

    .line 182
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollX()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollY()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 183
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/util/MotionEventUtil;->transform(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->transformedMotionEvent:Landroid/view/MotionEvent;

    .line 184
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/Matrix;)V

    .line 185
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->transformedMotionEvent:Landroid/view/MotionEvent;

    invoke-super {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1
.end method

.method public getChildVisibleRect(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z
    .locals 5
    .param p1, "child"    # Landroid/view/View;
    .param p2, "r"    # Landroid/graphics/Rect;
    .param p3, "offset"    # Landroid/graphics/Point;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p2, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 122
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->tempRect:Landroid/graphics/RectF;

    invoke-virtual {v3, p2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 123
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->matrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->tempRect:Landroid/graphics/RectF;

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 124
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->tempRect:Landroid/graphics/RectF;

    invoke-virtual {v3, p2}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 125
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollX()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollY()I

    move-result v4

    neg-int v4, v4

    invoke-virtual {p2, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 127
    if-eqz p3, :cond_0

    .line 128
    iget v3, p3, Landroid/graphics/Point;->x:I

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p3, Landroid/graphics/Point;->x:I

    .line 129
    iget v3, p3, Landroid/graphics/Point;->y:I

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p3, Landroid/graphics/Point;->y:I

    .line 130
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->points:[F

    iget v4, p3, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    aput v4, v3, v2

    .line 131
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->points:[F

    iget v4, p3, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    aput v4, v3, v1

    .line 132
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->matrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->points:[F

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 133
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->points:[F

    aget v3, v3, v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollX()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, p3, Landroid/graphics/Point;->x:I

    .line 134
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->points:[F

    aget v3, v3, v1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollY()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, p3, Landroid/graphics/Point;->y:I

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 138
    .local v0, "parent":Landroid/view/ViewParent;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getHeight()I

    move-result v4

    invoke-virtual {p2, v2, v2, v3, v4}, Landroid/graphics/Rect;->intersect(IIII)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_1

    .line 139
    invoke-interface {v0, p0, p2, p3}, Landroid/view/ViewParent;->getChildVisibleRect(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    :goto_0
    return v1

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method protected getContentUpperLeftInLayoutCoordinates([F)V
    .locals 3
    .param p1, "point"    # [F

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v0

    .line 108
    .local v0, "contentArea":Landroid/graphics/RectF;
    const/4 v1, 0x0

    iget v2, v0, Landroid/graphics/RectF;->left:F

    aput v2, p1, v1

    .line 109
    const/4 v1, 0x1

    iget v2, v0, Landroid/graphics/RectF;->top:F

    aput v2, p1, v1

    .line 110
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getViewPointInLayoutCoordinates([F)V

    .line 111
    return-void
.end method

.method public getInverseTransform(Landroid/graphics/Matrix;)V
    .locals 1
    .param p1, "inverse"    # Landroid/graphics/Matrix;

    .prologue
    .line 89
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 90
    .local v0, "tempMatrix":Landroid/graphics/Matrix;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getTransform(Landroid/graphics/Matrix;)V

    .line 91
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 92
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/Matrix;)V

    .line 93
    return-void
.end method

.method protected getPreTransformedMotionEvent()Landroid/view/MotionEvent;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->preTransformedMotionEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method public getTransform(Landroid/graphics/Matrix;)V
    .locals 2
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 84
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollX()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollY()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 85
    return-void
.end method

.method public getTransformScale()F
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->matrix:Landroid/graphics/Matrix;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->getMatrixScale(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method protected getViewPointInLayoutCoordinates([F)V
    .locals 1
    .param p1, "point"    # [F

    .prologue
    .line 100
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 101
    .local v0, "tempMatrix":Landroid/graphics/Matrix;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getInverseTransform(Landroid/graphics/Matrix;)V

    .line 102
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 103
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/Matrix;)V

    .line 104
    return-void
.end method

.method public invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;
    .locals 3
    .param p1, "location"    # [I
    .param p2, "r"    # Landroid/graphics/Rect;

    .prologue
    const/4 v2, 0x0

    .line 145
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollX()I

    move-result v0

    aput v0, p1, v2

    .line 146
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getScrollY()I

    move-result v1

    aput v1, p1, v0

    .line 147
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getHeight()I

    move-result v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 148
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;->invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;

    move-result-object v0

    return-object v0
.end method

.method public isAnimatingScroll()Z
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->scroller:Landroid/widget/Scroller;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 153
    invoke-super/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;->onLayout(ZIIII)V

    .line 154
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->onTransformChanged()V

    .line 155
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 4
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 72
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/FramePartView;->onScrollChanged(IIII)V

    .line 73
    sub-int v0, p1, p3

    int-to-double v0, v0

    sub-int v2, p2, p4

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->pauseQueuesScrollDistance:F

    float-to-double v2, v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 74
    sget-object v0, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/async/JankLock;->pauseTemporarily()V

    .line 76
    :cond_0
    return-void
.end method

.method public scrollToPoint(FFF)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "scale"    # F

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v0

    .line 197
    .local v0, "contentArea":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->tempRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, p3

    add-float/2addr v2, p1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v3, p3

    add-float/2addr v3, p2

    invoke-virtual {v1, p1, p2, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 198
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->tempRect:Landroid/graphics/RectF;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->scrollToRect(Landroid/graphics/RectF;)V

    .line 199
    return-void
.end method

.method public scrollToRect(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "targetRect"    # Landroid/graphics/RectF;

    .prologue
    .line 221
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->computeFitRectMatrix(Landroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->setTransform(Landroid/graphics/Matrix;)V

    .line 222
    return-void
.end method

.method public setTransform(Landroid/graphics/Matrix;)V
    .locals 5
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 55
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 58
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->points:[F

    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->getMatrixTranslation(Landroid/graphics/Matrix;[F)V

    .line 59
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->points:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 60
    .local v0, "scrollX":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->points:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 61
    .local v1, "scrollY":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->matrix:Landroid/graphics/Matrix;

    neg-int v3, v0

    int-to-float v3, v3

    neg-int v4, v1

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 64
    neg-int v2, v0

    neg-int v3, v1

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->scrollTo(II)V

    .line 66
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->onTransformChanged()V

    .line 67
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TransformView;->invalidate()V

    .line 68
    return-void
.end method
