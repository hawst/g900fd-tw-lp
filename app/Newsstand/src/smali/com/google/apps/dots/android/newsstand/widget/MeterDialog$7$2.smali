.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7$2;
.super Ljava/lang/Object;
.source "MeterDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->doInBackground()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;

    .prologue
    .line 348
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7$2;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 351
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7$2;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup:Z
    invoke-static {v2, v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$402(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Z)Z

    .line 352
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7$2;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7$2;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredArticlesRemaining:I
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$1000(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)I

    move-result v3

    if-lez v3, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->reset(Z)V

    .line 353
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7$2;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setVisibility(I)V

    .line 354
    return-void

    :cond_0
    move v0, v1

    .line 352
    goto :goto_0
.end method
