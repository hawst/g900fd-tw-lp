.class public interface abstract Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;
.super Ljava/lang/Object;
.source "ReadingFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;
    }
.end annotation


# static fields
.field public static final DK_BACKGROUND_COLOR:I

.field public static final DK_LINK_VIEW_POST_TITLE:I

.field public static final DK_LINK_VIEW_POST_URL:I

.field public static final DK_LINK_VIEW_PUBLISHER:I

.field public static final DK_POST_ID:I

.field public static final DK_POST_INDEX:I

.field public static final DK_POST_ORIGINAL_EDITION:I

.field public static final DK_POST_SUMMARY:I

.field public static final DK_POST_UPDATED:I

.field public static final DK_SHARE_PARAMS:I

.field public static final DK_VIEW_TYPE:I

.field public static final EQUALITY_FIELDS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_postId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ID:I

    .line 17
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_postSummary:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    .line 18
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_postIndex:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_INDEX:I

    .line 19
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_viewType:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_VIEW_TYPE:I

    .line 20
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_shareParams:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_SHARE_PARAMS:I

    .line 23
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_postOriginalEdition:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ORIGINAL_EDITION:I

    .line 24
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_postUpdated:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_UPDATED:I

    .line 25
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_backgroundColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_BACKGROUND_COLOR:I

    .line 27
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_linkViewPostTitle:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_POST_TITLE:I

    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_linkViewPostUrl:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_POST_URL:I

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_linkViewPublisher:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_PUBLISHER:I

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_IS_METERED:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_BACKGROUND_COLOR:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->EQUALITY_FIELDS:[I

    return-void
.end method
