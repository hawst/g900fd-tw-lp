.class public Lcom/google/apps/dots/android/newsstand/drawer/SettingsEntry;
.super Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;
.source "SettingsEntry.java"


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->SETTINGS:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;-><init>(Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;)V

    .line 13
    return-void
.end method


# virtual methods
.method public getLabelResId()I
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->settings:I

    return v0
.end method

.method public onClick(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 22
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/SettingsIntentBuilder;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/SettingsIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/SettingsIntentBuilder;->start()V

    .line 23
    return-void
.end method
