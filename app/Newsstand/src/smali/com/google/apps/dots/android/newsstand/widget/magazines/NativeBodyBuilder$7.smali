.class Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;
.super Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;
.source "NativeBodyBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field initialView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

.field final synthetic val$isOnlyChildOfRoot:Z

.field final synthetic val$part:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

.field final synthetic val$partContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;ZLcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .prologue
    .line 404
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->val$isOnlyChildOfRoot:Z

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->val$partContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->val$part:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V

    .line 408
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->val$isOnlyChildOfRoot:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->makeWebPartView()Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->initialView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeWebPartView()Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    .locals 4

    .prologue
    .line 411
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->val$partContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->forChild()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    move-result-object v0

    .line 412
    .local v0, "supplierChildPartContext":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->val$part:Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->val$isOnlyChildOfRoot:Z

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildWebPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V
    invoke-static {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->access$400(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Z)V

    .line 413
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view:Landroid/view/View;

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    return-object v1
.end method


# virtual methods
.method public createChildView()Landroid/view/View;
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->initialView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .line 421
    .local v0, "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->initialView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .line 422
    if-eqz v0, :cond_0

    .end local v0    # "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    :goto_0
    return-object v0

    .restart local v0    # "result":Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->makeWebPartView()Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 427
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->onDetachedFromWindow()V

    .line 429
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->initialView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->initialView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->destroy()V

    .line 431
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$7;->initialView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .line 433
    :cond_0
    return-void
.end method
