.class final Lcom/google/apps/dots/android/newsstand/async/AsyncScope$1;
.super Ljava/lang/Object;
.source "AsyncScope.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->resetAccountScope()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 32
    # getter for: Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->accountScopes:Ljava/util/Map;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->access$000()Ljava/util/Map;

    move-result-object v2

    monitor-enter v2

    .line 34
    const/4 v1, 0x0

    :try_start_0
    # setter for: Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->access$102(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;)Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 35
    # invokes: Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->access$200()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 37
    # getter for: Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->accountScopes:Ljava/util/Map;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->access$000()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 38
    .local v0, "asyncScope":Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    # getter for: Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->access$100()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v3

    if-eq v0, v3, :cond_0

    .line 39
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    goto :goto_0

    .line 44
    .end local v0    # "asyncScope":Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 41
    .restart local v0    # "asyncScope":Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    goto :goto_0

    .line 44
    .end local v0    # "asyncScope":Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45
    return-void
.end method
