.class Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$14;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardListVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeMagazineOfferCard(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

.field final synthetic val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$titleDocId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .prologue
    .line 973
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$14;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$14;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$14;->val$titleDocId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 976
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineClickEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$14;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->getAnalyticsScreenName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$14;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineClickEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineClickEvent;->track(Z)V

    .line 977
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$14;->val$titleDocId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setMagazineDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v0

    .line 978
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->start()V

    .line 979
    return-void
.end method
