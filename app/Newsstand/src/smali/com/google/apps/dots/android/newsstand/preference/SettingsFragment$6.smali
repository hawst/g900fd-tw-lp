.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$6;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupUseExternalStoragePreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

.field final synthetic val$externalStorageDir:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Ljava/io/File;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$6;->val$externalStorageDir:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$6;->val$externalStorageDir:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    const-string v2, "externalStorageDir"

    check-cast p2, Ljava/lang/Boolean;

    .line 242
    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$6;->val$externalStorageDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 241
    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const/4 v0, 0x1

    .line 246
    :goto_1
    return v0

    .line 242
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 246
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
