.class public Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "CardHeaderSpacer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;
    }
.end annotation


# static fields
.field public static final DK_HEIGHT:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUT:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardHeaderSpacer_height:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;->DK_HEIGHT:I

    .line 21
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_header_spacer:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;->LAYOUT:I

    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;->DK_HEIGHT:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;->EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 93
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 94
    return-void
.end method

.method public static makeHeaderCard(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 77
    .local v0, "headerRow":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;->LAYOUT:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 78
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;->EQUALITY_FIELDS:[I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 79
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_A11Y_COUNT:I

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 80
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;->DK_HEIGHT:I

    invoke-virtual {p1, p0}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 81
    return-object v0
.end method
