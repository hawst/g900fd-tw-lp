.class public Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil;
.super Ljava/lang/Object;
.source "SavedPostUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static postSavedState(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/saved/SavedList;)Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;
    .locals 2
    .param p0, "owningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "savedList"    # Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    .prologue
    .line 83
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->supportsBookmarking()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    invoke-virtual {p2, p1}, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;->NOT_SAVED:Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;

    .line 87
    :goto_0
    return-object v0

    .line 84
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;->SAVED:Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;

    goto :goto_0

    .line 87
    :cond_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;->NOT_SUPPORTED:Lcom/google/apps/dots/android/newsstand/saved/PostSavedState;

    goto :goto_0
.end method

.method public static savePost(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 33
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v2, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Saving post: %s"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    .line 37
    invoke-virtual {v2, v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v2

    .line 38
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v3

    iget-object v4, p2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    invoke-virtual {v3, p1, v4}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getSavedPost(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v2

    .line 39
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v2

    .line 40
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->makeSavePostHint(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setSimulationHint(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    .line 42
    .local v0, "clientAction":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getSaved(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 43
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    move-result-object v1

    .line 45
    .local v1, "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil$1;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil$1;-><init>(Landroid/content/Context;)V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_MAIN:Lcom/google/android/libraries/bind/async/Queue;

    .line 44
    invoke-static {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 56
    new-instance v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleBookmarkEvent;

    iget-object v3, p2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleBookmarkEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    .line 57
    invoke-virtual {v2, v7}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleBookmarkEvent;->track(Z)V

    .line 58
    return-void
.end method

.method protected static syncSavedEdition()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 91
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;->SAVED_EDITION:Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    .line 92
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->syncEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v0

    .line 93
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setAnyFreshness(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v0

    .line 94
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    .line 96
    return-void
.end method

.method public static unsavePost(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    const/4 v7, 0x1

    .line 62
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v2, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Unsaving post: %s"

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    invoke-virtual {v2, v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v2

    .line 65
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v3

    iget-object v4, p2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    invoke-virtual {v3, p1, v4}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getSavedPost(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v2

    .line 66
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    .line 68
    .local v0, "clientAction":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getSaved(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 69
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    move-result-object v1

    .line 71
    .local v1, "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil$2;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil$2;-><init>(Landroid/content/Context;)V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_MAIN:Lcom/google/android/libraries/bind/async/Queue;

    .line 70
    invoke-static {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 79
    return-void
.end method
