.class public Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;
.super Landroid/content/ContentProvider;
.source "ExportedContentProvider.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private attachmentsProvidelet:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

.field private tempFileDeleter:Ljava/util/Timer;

.field private tempFileDeletionRunnable:Ljava/lang/Runnable;

.field private tempFileLastRequestedTimestamp:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/io/File;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private tmpDir:Ljava/io/File;

.field private uriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tempFileDeletionRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tempFileLastRequestedTimestamp:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;)Ljava/util/TimerTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->makeTempFileDeletionTask()Ljava/util/TimerTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tempFileDeleter:Ljava/util/Timer;

    return-object v0
.end method

.method private capTransformAt(Lcom/google/apps/dots/android/newsstand/server/Transform;II)Lcom/google/apps/dots/android/newsstand/server/Transform;
    .locals 1
    .param p1, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 330
    if-eqz p1, :cond_0

    .line 331
    iget v0, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 332
    iget v0, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 334
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    return-object v0
.end method

.method private getMatch(Landroid/net/Uri;)I
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 338
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 340
    .local v0, "result":I
    if-gez v0, :cond_0

    .line 341
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->newIllegalArgumentException(Landroid/net/Uri;)Ljava/lang/IllegalArgumentException;

    move-result-object v1

    throw v1

    .line 343
    :cond_0
    return v0
.end method

.method private makeTempFileDeletionTask()Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 267
    new-instance v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider$1;-><init>(Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;)V

    return-object v0
.end method

.method private static newIllegalArgumentException(Landroid/net/Uri;)Ljava/lang/IllegalArgumentException;
    .locals 4
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unsupported uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static newUnsupportedOperationException(Ljava/lang/String;)Ljava/lang/UnsupportedOperationException;
    .locals 3
    .param p0, "operation"    # Ljava/lang/String;

    .prologue
    .line 164
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "() is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private openLimitedAttachmentFile(ILandroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 20
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 178
    const/4 v8, 0x0

    .line 179
    .local v8, "attachmentId":Ljava/lang/String;
    const/16 v16, 0x0

    .line 180
    .local v16, "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    const/4 v13, 0x0

    .line 182
    .local v13, "path":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 200
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->parseDimensionParameters(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v17

    .line 201
    .local v17, "transformFromParams":Lcom/google/apps/dots/android/newsstand/server/Transform;
    if-eqz v17, :cond_1

    .line 202
    move-object/from16 v16, v17

    .line 205
    :cond_1
    const/16 v2, 0x258

    const/16 v4, 0x258

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2, v4}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->capTransformAt(Lcom/google/apps/dots/android/newsstand/server/Transform;II)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v16

    .line 206
    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    if-eqz v2, :cond_2

    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    if-nez v2, :cond_4

    .line 207
    :cond_2
    new-instance v4, Ljava/io/FileNotFoundException;

    const-string v5, "Requesting image of size 0: "

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-direct {v4, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 184
    .end local v17    # "transformFromParams":Lcom/google/apps/dots/android/newsstand/server/Transform;
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    .line 185
    goto :goto_0

    .line 187
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v14

    .line 188
    .local v14, "segments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-interface {v14, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "attachmentId":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 189
    .restart local v8    # "attachmentId":Ljava/lang/String;
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v14, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 190
    .local v18, "transformString":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/google/apps/dots/android/newsstand/server/Transform;->parse(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v16

    .line 191
    if-nez v16, :cond_0

    .line 192
    sget-object v2, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Failed to parse transform: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v18, v5, v6

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 207
    .end local v14    # "segments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v18    # "transformString":Ljava/lang/String;
    .restart local v17    # "transformFromParams":Lcom/google/apps/dots/android/newsstand/server/Transform;
    :cond_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 210
    :cond_4
    const/4 v15, 0x0

    .line 211
    .local v15, "tmpFile":Ljava/io/File;
    invoke-static {v8}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 212
    new-instance v15, Ljava/io/File;

    .end local v15    # "tmpFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tmpDir:Ljava/io/File;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/google/apps/dots/android/newsstand/server/Transform;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v19

    add-int v7, v7, v19

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v15, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 213
    .restart local v15    # "tmpFile":Ljava/io/File;
    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 215
    invoke-virtual {v15}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    .line 241
    :cond_5
    :goto_2
    if-nez v13, :cond_8

    .line 242
    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 218
    :cond_6
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v2

    .line 219
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v4

    .line 218
    move-object/from16 v0, v16

    invoke-virtual {v2, v4, v8, v0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 217
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/Bitmap;

    .line 220
    .local v9, "bitmap":Landroid/graphics/Bitmap;
    if-nez v9, :cond_7

    .line 221
    sget-object v2, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Unable to fetch attachment %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v8, v5, v6

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 222
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->attachmentsProvidelet:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->getNotFoundImagePath()Ljava/lang/String;

    move-result-object v13

    goto :goto_2

    .line 225
    :cond_7
    :try_start_0
    new-instance v12, Ljava/io/FileOutputStream;

    invoke-direct {v12, v15}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    .local v12, "outStream":Ljava/io/FileOutputStream;
    const/4 v11, 0x1

    .line 228
    .local v11, "failed":Z
    :try_start_1
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v9, v2, v4, v12}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 229
    invoke-virtual {v15}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v13

    .line 230
    const/4 v11, 0x0

    .line 232
    :try_start_2
    invoke-static {v12, v11}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 234
    .end local v11    # "failed":Z
    .end local v12    # "outStream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v10

    .line 235
    .local v10, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->attachmentsProvidelet:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->getNotFoundImagePath()Ljava/lang/String;

    move-result-object v13

    goto :goto_2

    .line 232
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v11    # "failed":Z
    .restart local v12    # "outStream":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-static {v12, v11}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    throw v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 244
    .end local v9    # "bitmap":Landroid/graphics/Bitmap;
    .end local v11    # "failed":Z
    .end local v12    # "outStream":Ljava/io/FileOutputStream;
    :cond_8
    sget-object v2, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Temporary file saved at: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v13, v5, v6

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 246
    if-eqz v15, :cond_a

    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 247
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tempFileLastRequestedTimestamp:Ljava/util/Map;

    monitor-enter v4

    .line 250
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tempFileLastRequestedTimestamp:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 251
    sget-object v2, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Scheduling a deletion of temporary files."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 252
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tempFileDeleter:Ljava/util/Timer;

    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->makeTempFileDeletionTask()Ljava/util/TimerTask;

    move-result-object v5

    const-wide/32 v6, 0x495d4

    invoke-virtual {v2, v5, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 255
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tempFileLastRequestedTimestamp:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v2, v15, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 259
    :cond_a
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/high16 v4, 0x10000000

    .line 260
    invoke-static {v2, v4}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    .line 263
    .local v3, "parcelFileDescriptor":Landroid/os/ParcelFileDescriptor;
    new-instance v2, Landroid/content/res/AssetFileDescriptor;

    const-wide/16 v4, 0x0

    const-wide/16 v6, -0x1

    invoke-direct/range {v2 .. v7}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V

    return-object v2

    .line 256
    .end local v3    # "parcelFileDescriptor":Landroid/os/ParcelFileDescriptor;
    :catchall_1
    move-exception v2

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v2

    .line 182
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private parseDimensionParameters(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/server/Transform;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x0

    .line 308
    const/4 v3, 0x0

    .line 309
    .local v3, "w":I
    const/4 v1, 0x0

    .line 310
    .local v1, "h":I
    const-string v6, "w"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 311
    .local v4, "wString":Ljava/lang/String;
    const-string v6, "h"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 312
    .local v2, "hString":Ljava/lang/String;
    if-nez v4, :cond_2

    if-eqz v2, :cond_2

    .line 313
    move-object v4, v2

    .line 317
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    .line 319
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 320
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 324
    new-instance v5, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    invoke-virtual {v5, v3}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v5

    .line 326
    :cond_1
    :goto_1
    return-object v5

    .line 314
    :cond_2
    if-eqz v4, :cond_0

    if-nez v2, :cond_0

    .line 315
    move-object v2, v4

    goto :goto_0

    .line 321
    :catch_0
    move-exception v0

    .line 322
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_1
.end method

.method private setUpTempFileDeletionRunnable()V
    .locals 1

    .prologue
    .line 276
    new-instance v0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider$2;-><init>(Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tempFileDeletionRunnable:Ljava/lang/Runnable;

    .line 305
    return-void
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 120
    const-string v0, "bulkInsert"

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->newUnsupportedOperationException(Ljava/lang/String;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "arg"    # Ljava/lang/String;
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 112
    const-string v0, "call"

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->newUnsupportedOperationException(Ljava/lang/String;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 124
    const-string v0, "delete"

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->newUnsupportedOperationException(Ljava/lang/String;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 132
    const-string v0, "getType"

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->newUnsupportedOperationException(Ljava/lang/String;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 116
    const-string v0, "insert"

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->newUnsupportedOperationException(Ljava/lang/String;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public onCreate()Z
    .locals 8

    .prologue
    const/4 v6, 0x7

    const/4 v7, 0x3

    .line 66
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 68
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/NSDepend;->setup(Landroid/content/Context;)V

    .line 70
    new-instance v4, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    invoke-direct {v4, v1}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->attachmentsProvidelet:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    .line 72
    new-instance v4, Landroid/content/UriMatcher;

    const/4 v5, -0x1

    invoke-direct {v4, v5}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    .line 73
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->exportedContentAuthority()Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "authority":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v5, "attachment/"

    invoke-virtual {v4, v0, v5, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 76
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v5, "attachment/*"

    invoke-virtual {v4, v0, v5, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 77
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v5, "attachment/*/*"

    const/16 v6, 0x8

    invoke-virtual {v4, v0, v5, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 80
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v5, "att/"

    invoke-virtual {v4, v0, v5, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 81
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v5, "att/*"

    invoke-virtual {v4, v0, v5, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 82
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v5, "att/*/*"

    const/4 v6, 0x4

    invoke-virtual {v4, v0, v5, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 84
    new-instance v4, Ljava/io/File;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    const-string v6, "exported_content_tmpdir"

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tmpDir:Ljava/io/File;

    .line 85
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tmpDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 89
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tmpDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 90
    .local v3, "tmpDirFiles":[Ljava/io/File;
    if-eqz v3, :cond_2

    .line 91
    array-length v5, v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v2, v3, v4

    .line 92
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 93
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 91
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 98
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "tmpDirFiles":[Ljava/io/File;
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tmpDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 101
    :cond_2
    new-instance v4, Ljava/util/Timer;

    invoke-direct {v4}, Ljava/util/Timer;-><init>()V

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tempFileDeleter:Ljava/util/Timer;

    .line 102
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->tempFileLastRequestedTimestamp:Ljava/util/Map;

    .line 103
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->setUpTempFileDeletionRunnable()V

    .line 105
    const/4 v4, 0x1

    return v4
.end method

.method public openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->getMatch(Landroid/net/Uri;)I

    move-result v0

    .line 151
    .local v0, "match":I
    packed-switch v0, :pswitch_data_0

    .line 159
    :pswitch_0
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->newIllegalArgumentException(Landroid/net/Uri;)Ljava/lang/IllegalArgumentException;

    move-result-object v1

    throw v1

    .line 154
    :pswitch_1
    invoke-direct {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->openLimitedAttachmentFile(ILandroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    .line 157
    :goto_0
    return-object v1

    :pswitch_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->attachmentsProvidelet:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    invoke-virtual {v1, v0, p1, p2, p0}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->openAssetFile(ILandroid/net/Uri;Ljava/lang/String;Landroid/content/ContentProvider;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 139
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->getMatch(Landroid/net/Uri;)I

    move-result v0

    .line 142
    .local v0, "match":I
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->newIllegalArgumentException(Landroid/net/Uri;)Ljava/lang/IllegalArgumentException;

    move-result-object v1

    throw v1
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 128
    const-string v0, "update"

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/ExportedContentProvider;->newUnsupportedOperationException(Ljava/lang/String;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method
