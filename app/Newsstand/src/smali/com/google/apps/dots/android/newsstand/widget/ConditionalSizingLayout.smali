.class public abstract Lcom/google/apps/dots/android/newsstand/widget/ConditionalSizingLayout;
.super Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;
.source "ConditionalSizingLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ConditionalSizingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/ConditionalSizingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method


# virtual methods
.method protected abstract checkCondition()Z
.end method

.method protected computeMeasureSpec(ZII)I
    .locals 1
    .param p1, "isWidth"    # Z
    .param p2, "widthMeasureSpec"    # I
    .param p3, "heightMeasureSpec"    # I

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ConditionalSizingLayout;->checkCondition()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/SizingLayout;->computeMeasureSpec(ZII)I

    move-result p2

    .line 28
    .end local p2    # "widthMeasureSpec":I
    :cond_0
    :goto_0
    return p2

    .restart local p2    # "widthMeasureSpec":I
    :cond_1
    if-nez p1, :cond_0

    move p2, p3

    goto :goto_0
.end method
