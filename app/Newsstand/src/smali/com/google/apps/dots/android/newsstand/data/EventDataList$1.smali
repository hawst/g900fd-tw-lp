.class Lcom/google/apps/dots/android/newsstand/data/EventDataList$1;
.super Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver;
.source "EventDataList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/data/EventDataList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/data/EventDataList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/data/EventDataList;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/data/EventDataList;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList$1;->this$0:Lcom/google/apps/dots/android/newsstand/data/EventDataList;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method protected onEventAsync(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p2, "extras":Ljava/util/Map;, "Ljava/util/Map<**>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList$1;->this$0:Lcom/google/apps/dots/android/newsstand/data/EventDataList;

    # invokes: Lcom/google/apps/dots/android/newsstand/data/EventDataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->access$000(Lcom/google/apps/dots/android/newsstand/data/EventDataList;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "onEvent %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/EventDataList$1;->this$0:Lcom/google/apps/dots/android/newsstand/data/EventDataList;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->onEvent(Landroid/net/Uri;Ljava/util/Map;)V

    .line 28
    return-void
.end method
