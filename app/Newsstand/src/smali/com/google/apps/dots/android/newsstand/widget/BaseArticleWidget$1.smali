.class Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1;
.super Lcom/google/apps/dots/android/newsstand/async/QueueTask;
.source "BaseArticleWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->loadPostData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/Queue;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;
    .param p2, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    return-void
.end method


# virtual methods
.method protected doInBackground()V
    .locals 5

    .prologue
    .line 196
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-nez v2, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->itemJsonSerializer()Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->form:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->getEncodedPostData(Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/proto/client/DotsShared$Form;)Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, "postDataJson":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 203
    const-string v2, "dots.store.setupPost(%s);"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "loadClause":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1$1;

    invoke-direct {v3, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
