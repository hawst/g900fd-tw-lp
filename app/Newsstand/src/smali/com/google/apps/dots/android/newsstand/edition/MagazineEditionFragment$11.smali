.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$11;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "MagazineEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateHeroView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 631
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$11;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 12
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 634
    if-eqz p1, :cond_0

    .line 635
    iget-object v6, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v4

    .line 636
    .local v4, "iconImage":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    if-eqz v4, :cond_0

    .line 637
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    .line 638
    .local v0, "coverAttachmentId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 639
    new-instance v3, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    invoke-direct {v3, v0}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(Ljava/lang/String;)V

    .line 640
    .local v3, "iconId":Lcom/google/apps/dots/android/newsstand/icon/IconId;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$11;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/newsstanddev/R$dimen;->magazine_edition_header_cover_height:I

    .line 641
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 643
    .local v1, "headerLogoHeight":I
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getWidth()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float v5, v6, v7

    .line 644
    .local v5, "widthToHeightRatio":F
    int-to-float v6, v1

    mul-float/2addr v6, v5

    float-to-int v2, v6

    .line 645
    .local v2, "headerLogoWidth":I
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$11;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v6

    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v8, 0x11

    invoke-direct {v7, v2, v1, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 648
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$11;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$11;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/apps/newsstanddev/R$string;->magazine_issue_name_format:I

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 650
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 651
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 648
    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 652
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$11;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v6

    invoke-virtual {v3, v6, v2, v1}, Lcom/google/apps/dots/android/newsstand/icon/IconId;->apply(Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;II)V

    .line 656
    .end local v0    # "coverAttachmentId":Ljava/lang/String;
    .end local v1    # "headerLogoHeight":I
    .end local v2    # "headerLogoWidth":I
    .end local v3    # "iconId":Lcom/google/apps/dots/android/newsstand/icon/IconId;
    .end local v4    # "iconImage":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .end local v5    # "widthToHeightRatio":F
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 631
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$11;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
