.class public final Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayTarget"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;


# instance fields
.field private bitField0_:I

.field private height_:I

.field private scaling_:I

.field private shouldDisplay_:Z

.field private width_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8885
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8886
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 8898
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->shouldDisplay_:Z

    .line 8917
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->width_:I

    .line 8936
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->height_:I

    .line 8955
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->scaling_:I

    .line 8886
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;
    .locals 3

    .prologue
    .line 8987
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8991
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;
    return-object v0

    .line 8988
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;
    :catch_0
    move-exception v1

    .line 8989
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 8882
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 8996
    if-ne p1, p0, :cond_1

    .line 8999
    :cond_0
    :goto_0
    return v1

    .line 8997
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 8998
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    .line 8999
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->shouldDisplay_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->shouldDisplay_:Z

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->width_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->width_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->height_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->height_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->scaling_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->scaling_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 8938
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->height_:I

    return v0
.end method

.method public getScaling()I
    .locals 1

    .prologue
    .line 8957
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->scaling_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 9033
    const/4 v0, 0x0

    .line 9034
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9035
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->shouldDisplay_:Z

    .line 9036
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9038
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 9039
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->width_:I

    .line 9040
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9042
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 9043
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->height_:I

    .line 9044
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9046
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 9047
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->scaling_:I

    .line 9048
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9050
    :cond_3
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->cachedSize:I

    .line 9051
    return v0
.end method

.method public getShouldDisplay()Z
    .locals 1

    .prologue
    .line 8900
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->shouldDisplay_:Z

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 8919
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->width_:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 9006
    const/16 v0, 0x11

    .line 9007
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 9008
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->shouldDisplay_:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    add-int v0, v2, v1

    .line 9009
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->width_:I

    add-int v0, v1, v2

    .line 9010
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->height_:I

    add-int v0, v1, v2

    .line 9011
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->scaling_:I

    add-int v0, v1, v2

    .line 9012
    return v0

    .line 9008
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9059
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 9060
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 9064
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 9065
    :sswitch_0
    return-object p0

    .line 9070
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->shouldDisplay_:Z

    .line 9071
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    goto :goto_0

    .line 9075
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->width_:I

    .line 9076
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    goto :goto_0

    .line 9080
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->height_:I

    .line 9081
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    goto :goto_0

    .line 9085
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 9086
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 9090
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->scaling_:I

    .line 9091
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    goto :goto_0

    .line 9093
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->scaling_:I

    goto :goto_0

    .line 9060
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8882
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9017
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9018
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->shouldDisplay_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 9020
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 9021
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->width_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 9023
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 9024
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->height_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 9026
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 9027
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->scaling_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 9029
    :cond_3
    return-void
.end method
