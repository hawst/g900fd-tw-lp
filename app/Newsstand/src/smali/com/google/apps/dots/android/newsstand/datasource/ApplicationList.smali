.class public interface abstract Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;
.super Ljava/lang/Object;
.source "ApplicationList.java"


# static fields
.field public static final DK_APP_FAMILY_ID:I

.field public static final DK_APP_FAMILY_SUMMARY:I

.field public static final DK_APP_ID:I

.field public static final DK_APP_SUMMARY:I

.field public static final DK_CATEGORY:I

.field public static final DK_CONTENT_DESCRIPTION:I

.field public static final DK_DESCRIPTION:I

.field public static final DK_EDITION:I

.field public static final DK_ICON_ID:I

.field public static final DK_IS_ARCHIVED:I

.field public static final DK_IS_PURCHASED:I

.field public static final DK_PUBLICATION_DATE:I

.field public static final DK_TITLE:I

.field public static final DK_UPDATED:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_edition:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    .line 11
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_appSummary:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_SUMMARY:I

    .line 12
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_appFamilySummary:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_FAMILY_SUMMARY:I

    .line 13
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_appId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_ID:I

    .line 14
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_appFamilyId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_FAMILY_ID:I

    .line 15
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_TITLE:I

    .line 16
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_iconId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_ICON_ID:I

    .line 17
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_description:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_DESCRIPTION:I

    .line 18
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_publicationDate:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_PUBLICATION_DATE:I

    .line 19
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_isPurchased:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_PURCHASED:I

    .line 20
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_isArchived:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_ARCHIVED:I

    .line 21
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_updated:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_UPDATED:I

    .line 22
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_contentDescription:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_CONTENT_DESCRIPTION:I

    .line 23
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ApplicationList_category:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_CATEGORY:I

    return-void
.end method
