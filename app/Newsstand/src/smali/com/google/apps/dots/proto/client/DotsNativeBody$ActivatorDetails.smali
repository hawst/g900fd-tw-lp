.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActivatorDetails"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;


# instance fields
.field private bitField0_:I

.field private respectBoundaries_:Z

.field private respectVisibility_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3457
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 3458
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3463
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectBoundaries_:Z

    .line 3482
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectVisibility_:Z

    .line 3458
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;
    .locals 3

    .prologue
    .line 3512
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3516
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;
    return-object v0

    .line 3513
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;
    :catch_0
    move-exception v1

    .line 3514
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3454
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3521
    if-ne p1, p0, :cond_1

    .line 3524
    :cond_0
    :goto_0
    return v1

    .line 3522
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 3523
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    .line 3524
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectBoundaries_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectBoundaries_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectVisibility_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectVisibility_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getRespectBoundaries()Z
    .locals 1

    .prologue
    .line 3465
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectBoundaries_:Z

    return v0
.end method

.method public getRespectVisibility()Z
    .locals 1

    .prologue
    .line 3484
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectVisibility_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3548
    const/4 v0, 0x0

    .line 3549
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3550
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectBoundaries_:Z

    .line 3551
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3553
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3554
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectVisibility_:Z

    .line 3555
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3557
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->cachedSize:I

    .line 3558
    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3529
    const/16 v0, 0x11

    .line 3530
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 3531
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectBoundaries_:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v4, v1

    .line 3532
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectVisibility_:Z

    if-eqz v4, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 3533
    return v0

    :cond_0
    move v1, v3

    .line 3531
    goto :goto_0

    :cond_1
    move v2, v3

    .line 3532
    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3566
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3567
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3571
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3572
    :sswitch_0
    return-object p0

    .line 3577
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectBoundaries_:Z

    .line 3578
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->bitField0_:I

    goto :goto_0

    .line 3582
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectVisibility_:Z

    .line 3583
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->bitField0_:I

    goto :goto_0

    .line 3567
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3454
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3538
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3539
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectBoundaries_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3541
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3542
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ActivatorDetails;->respectVisibility_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3544
    :cond_1
    return-void
.end method
