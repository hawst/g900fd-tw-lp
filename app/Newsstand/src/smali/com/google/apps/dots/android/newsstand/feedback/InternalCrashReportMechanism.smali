.class public Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;
.super Ljava/lang/Object;
.source "InternalCrashReportMechanism.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field private final serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/server/ServerUris;)V
    .locals 0
    .param p1, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .param p2, "serverUris"    # Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 39
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 40
    return-void
.end method

.method private static crashInfoToReport(Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;Landroid/app/ApplicationErrorReport;)V
    .locals 2
    .param p0, "source"    # Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;
    .param p1, "applicationErrorReport"    # Landroid/app/ApplicationErrorReport;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 108
    iget-object v1, p1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    if-nez v1, :cond_0

    .line 109
    new-instance v1, Landroid/app/ApplicationErrorReport$CrashInfo;

    invoke-direct {v1}, Landroid/app/ApplicationErrorReport$CrashInfo;-><init>()V

    iput-object v1, p1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    .line 111
    :cond_0
    iget-object v0, p1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    .line 112
    .local v0, "destination":Landroid/app/ApplicationErrorReport$CrashInfo;
    if-eqz p0, :cond_1

    .line 113
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;->exceptionClassName:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionClassName:Ljava/lang/String;

    .line 114
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;->throwFileName:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    .line 115
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;->throwLineNumber:I

    iput v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwLineNumber:I

    .line 116
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;->throwClassName:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwClassName:Ljava/lang/String;

    .line 117
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;->throwMethodName:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwMethodName:Ljava/lang/String;

    .line 118
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;->stackTrace:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    .line 119
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;->exceptionMessage:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    .line 121
    :cond_1
    return-void
.end method

.method private fillErrorReport(Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;)Landroid/app/ApplicationErrorReport;
    .locals 5
    .param p1, "feedbackInfo"    # Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 44
    new-instance v1, Landroid/app/ApplicationErrorReport;

    invoke-direct {v1}, Landroid/app/ApplicationErrorReport;-><init>()V

    .line 47
    .local v1, "report":Landroid/app/ApplicationErrorReport;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "newsstandBase":Ljava/lang/String;
    iput-object v0, v1, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    .line 49
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v2, ".CRASH_REPORT"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    iput-object v2, v1, Landroid/app/ApplicationErrorReport;->processName:Ljava/lang/String;

    .line 50
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Landroid/app/ApplicationErrorReport;->time:J

    .line 51
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/app/ApplicationErrorReport;->systemApp:Z

    .line 52
    const/4 v2, 0x1

    iput v2, v1, Landroid/app/ApplicationErrorReport;->type:I

    .line 53
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->readCrashInfo()Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;->crashInfoToReport(Lcom/google/apps/dots/android/newsstand/feedback/CrashInfo;Landroid/app/ApplicationErrorReport;)V

    .line 55
    return-object v1

    .line 49
    :cond_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public launchCrashReportIntent(Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;)V
    .locals 8
    .param p1, "feedbackInfo"    # Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 81
    :try_start_0
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 83
    .local v2, "lastActivity":Landroid/app/Activity;
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.APP_ERROR"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 85
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "com.google.android.feedback"

    const-string v5, "com.google.android.feedback.FeedbackActivity"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    const v4, 0x10008000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 87
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;->fillErrorReport(Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;)Landroid/app/ApplicationErrorReport;

    move-result-object v3

    .line 88
    .local v3, "report":Landroid/app/ApplicationErrorReport;
    const-string v4, "android.intent.extra.BUG_REPORT"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 98
    invoke-virtual {v2, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "lastActivity":Landroid/app/Activity;
    .end local v3    # "report":Landroid/app/ApplicationErrorReport;
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Crash report intent encountered error %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
