.class Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;
.super Ljava/lang/Object;
.source "Janitor.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->evictOldBlobs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field now:J

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

.field final synthetic val$deletedCount:[I

.field final synthetic val$deletedSize:[J

.field final synthetic val$threshold:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;I[J[I)V
    .locals 2
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    iput p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;->val$threshold:I

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;->val$deletedSize:[J

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;->val$deletedCount:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 263
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;->now:J

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;JJJZ)V
    .locals 8
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "readTime"    # J
    .param p4, "writeTime"    # J
    .param p6, "size"    # J
    .param p8, "pinned"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 268
    invoke-static {p2, p3, p4, p5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 269
    .local v0, "accessTime":J
    if-nez p8, :cond_0

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;->now:J

    sub-long/2addr v2, v0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .line 270
    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$1100(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    move-result-object v4

    iget-wide v4, v4, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->lruMinAgeMs:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    long-to-int v2, v2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;->val$threshold:I

    if-gt v2, v3, :cond_0

    .line 272
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$100(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->delete(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 273
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;->val$deletedSize:[J

    aget-wide v4, v2, v6

    add-long/2addr v4, p6

    aput-wide v4, v2, v6

    .line 274
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;->val$deletedCount:[I

    aget v3, v2, v6

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v6

    .line 277
    :cond_0
    return-void
.end method
