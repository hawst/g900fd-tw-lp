.class Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;
.super Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;
.source "MagazineReadingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->setupPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/support/v4/view/NSViewPager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p2, "viewPager"    # Landroid/support/v4/view/NSViewPager;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;-><init>(Landroid/support/v4/view/NSViewPager;)V

    return-void
.end method


# virtual methods
.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 341
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->onPageScrolled(IFI)V

    .line 342
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    const/4 v2, 0x1

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$700(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;I)Landroid/view/View;

    move-result-object v0

    .line 343
    .local v0, "widget":Landroid/view/View;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    if-eqz v1, :cond_0

    .line 345
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->pauseBackgroundProcessingTemporarily()V

    .line 346
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .end local v0    # "widget":Landroid/view/View;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->onTransformChanged()V

    .line 348
    :cond_0
    return-void
.end method

.method public onPageSelected(IZ)V
    .locals 16
    .param p1, "visualPosition"    # I
    .param p2, "userDriven"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 275
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1600()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v10

    const-string v11, "onPageSelected: %d, userDriven: %b"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1700(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 337
    :goto_0
    return-void

    .line 281
    :cond_0
    if-eqz p2, :cond_2

    .line 282
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1800(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    move-result-object v10

    move/from16 v0, p1

    invoke-static {v10, v0}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v5

    .line 283
    .local v5, "logicalPosition":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1700(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v10

    invoke-virtual {v10, v5}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    .line 284
    .local v3, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v10, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_ID:I

    invoke-virtual {v3, v10}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v6

    .line 285
    .local v6, "newPostId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {v10}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 286
    new-instance v7, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .line 287
    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1900(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Z

    move-result v11

    invoke-direct {v7, v10, v6, v11}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Z)V

    .line 288
    .local v7, "newState":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    const/4 v11, 0x1

    invoke-virtual {v10, v7, v11}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 289
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x10

    if-lt v10, v11, :cond_1

    .line 290
    sget v10, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_SECTION_SUMMARY:I

    invoke-virtual {v3, v10}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 291
    .local v9, "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->hasCorrespondingImageSectionId()Z

    move-result v10

    if-eqz v10, :cond_8

    .line 293
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$900(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/support/v4/view/NSViewPager;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 301
    .end local v7    # "newState":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    .end local v9    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_1
    :goto_1
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x15

    if-lt v10, v11, :cond_2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 304
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    const/4 v11, 0x0

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getMagazineImageAttachmentId(I)Ljava/lang/String;
    invoke-static {v10, v11}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2100(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;I)Ljava/lang/String;

    move-result-object v1

    .line 305
    .local v1, "attachmentId":Ljava/lang/String;
    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .line 306
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getAttachmentId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 307
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .line 308
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getTransform()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v11

    .line 307
    invoke-virtual {v10, v1, v11}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentIdPx(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    .line 315
    .end local v1    # "attachmentId":Ljava/lang/String;
    .end local v3    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v5    # "logicalPosition":I
    .end local v6    # "newPostId":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    const/4 v11, 0x0

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;
    invoke-static {v10, v11}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$700(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;I)Landroid/view/View;

    move-result-object v4

    .line 316
    .local v4, "leftWidget":Landroid/view/View;
    instance-of v10, v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    if-eqz v10, :cond_3

    move-object v10, v4

    .line 317
    check-cast v10, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrollToEdge(I)V

    .line 319
    :cond_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    const/4 v11, 0x2

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;
    invoke-static {v10, v11}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$700(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;I)Landroid/view/View;

    move-result-object v8

    .line 320
    .local v8, "rightWidget":Landroid/view/View;
    instance-of v10, v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    if-eqz v10, :cond_4

    move-object v10, v8

    .line 321
    check-cast v10, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    const/4 v11, -0x1

    invoke-virtual {v10, v11}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrollToEdge(I)V

    .line 325
    :cond_4
    instance-of v10, v4, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;

    if-eqz v10, :cond_5

    .line 326
    check-cast v4, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;

    .end local v4    # "leftWidget":Landroid/view/View;
    const/4 v10, 0x0

    invoke-interface {v4, v10}, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;->setUserVisibleHint(Z)V

    .line 328
    :cond_5
    instance-of v10, v8, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;

    if-eqz v10, :cond_6

    .line 329
    check-cast v8, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;

    .end local v8    # "rightWidget":Landroid/view/View;
    const/4 v10, 0x0

    invoke-interface {v8, v10}, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;->setUserVisibleHint(Z)V

    .line 331
    :cond_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    const/4 v11, 0x1

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;
    invoke-static {v10, v11}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$700(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;I)Landroid/view/View;

    move-result-object v2

    .line 332
    .local v2, "centerWidget":Landroid/view/View;
    instance-of v10, v2, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;

    if-eqz v10, :cond_7

    .line 333
    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;

    .end local v2    # "centerWidget":Landroid/view/View;
    const/4 v10, 0x1

    invoke-interface {v2, v10}, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;->setUserVisibleHint(Z)V

    .line 336
    :cond_7
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    move/from16 v0, p1

    # setter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lastVisualPageSelected:I
    invoke-static {v10, v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$002(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;I)I

    goto/16 :goto_0

    .line 296
    .restart local v3    # "data":Lcom/google/android/libraries/bind/data/Data;
    .restart local v5    # "logicalPosition":I
    .restart local v6    # "newPostId":Ljava/lang/String;
    .restart local v7    # "newState":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    .restart local v9    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_8
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    sget v12, Lcom/google/android/apps/newsstanddev/R$string;->magazine_article_accessibility_warning:I

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .line 297
    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    invoke-static {v15}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$900(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    .line 296
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/support/v4/view/NSViewPager;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method
