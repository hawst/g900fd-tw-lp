.class public final Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;
.super Ljava/lang/Object;
.source "RootDirs.java"


# instance fields
.field public final cache:Ljava/io/File;

.field public final perm:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/io/File;)V
    .locals 1
    .param p1, "permRootDir"    # Ljava/io/File;
    .param p2, "cacheRootDir"    # Ljava/io/File;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    .line 17
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->cache:Ljava/io/File;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 22
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    if-nez v2, :cond_1

    .line 26
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 25
    check-cast v0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    .line 26
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;
    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->cache:Ljava/io/File;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->cache:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->cache:Ljava/io/File;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 36
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "perm"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    .line 37
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "cache"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->cache:Ljava/io/File;

    .line 38
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
