.class public Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;
.super Ljava/lang/Object;
.source "TraceCompat.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static asyncTraceBegin:Ljava/lang/reflect/Method;

.field private static asyncTraceEnd:Ljava/lang/reflect/Method;

.field private static enabled:Z

.field private static nestingLevel:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[I>;"
        }
    .end annotation
.end field

.field private static traceTagApp:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    const-class v0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 51
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->whenSetup()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat$1;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat$1;-><init>()V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 76
    new-instance v0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat$2;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat$2;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->nestingLevel:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 40
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->enabled:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 40
    sput-boolean p0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->enabled:Z

    return p0
.end method

.method static synthetic access$102(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    .locals 0
    .param p0, "x0"    # Ljava/lang/reflect/Method;

    .prologue
    .line 40
    sput-object p0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->asyncTraceBegin:Ljava/lang/reflect/Method;

    return-object p0
.end method

.method static synthetic access$202(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    .locals 0
    .param p0, "x0"    # Ljava/lang/reflect/Method;

    .prologue
    .line 40
    sput-object p0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->asyncTraceEnd:Ljava/lang/reflect/Method;

    return-object p0
.end method

.method static synthetic access$302(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 40
    sput-wide p0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->traceTagApp:J

    return-wide p0
.end method

.method static synthetic access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public static asyncTraceBegin(Ljava/lang/String;)V
    .locals 9
    .param p0, "methodName"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 152
    sget-boolean v4, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->enabled:Z

    if-eqz v4, :cond_0

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_0

    .line 153
    sget-wide v2, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->traceTagApp:J

    .line 154
    .local v2, "tag":J
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 156
    .local v0, "cookie":I
    :try_start_0
    sget-object v4, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->asyncTraceBegin:Ljava/lang/reflect/Method;

    const/4 v5, 0x0

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object p0, v6, v7

    const/4 v7, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    .end local v0    # "cookie":I
    .end local v2    # "tag":J
    :cond_0
    :goto_0
    return-void

    .line 157
    .restart local v0    # "cookie":I
    .restart local v2    # "tag":J
    :catch_0
    move-exception v1

    .line 158
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v4, v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static asyncTraceEnd(Ljava/lang/String;)V
    .locals 9
    .param p0, "methodName"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 165
    sget-boolean v4, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->enabled:Z

    if-eqz v4, :cond_0

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_0

    .line 166
    sget-wide v2, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->traceTagApp:J

    .line 167
    .local v2, "tag":J
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 169
    .local v0, "cookie":I
    :try_start_0
    sget-object v4, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->asyncTraceEnd:Ljava/lang/reflect/Method;

    const/4 v5, 0x0

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object p0, v6, v7

    const/4 v7, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    .end local v0    # "cookie":I
    .end local v2    # "tag":J
    :cond_0
    :goto_0
    return-void

    .line 170
    .restart local v0    # "cookie":I
    .restart local v2    # "tag":J
    :catch_0
    move-exception v1

    .line 171
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v4, v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static beginSection(Ljava/lang/String;)I
    .locals 3
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 91
    sget-boolean v1, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->enabled:Z

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 92
    const/4 v1, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v0

    .line 94
    :cond_0
    return v0
.end method

.method public static varargs beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    .locals 11
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/16 v10, 0x7d

    const/4 v4, 0x0

    .line 104
    sget-boolean v5, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->enabled:Z

    if-eqz v5, :cond_3

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x12

    if-lt v5, v6, :cond_3

    .line 105
    sget-object v5, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->nestingLevel:Ljava/lang/ThreadLocal;

    invoke-virtual {v5}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 106
    .local v1, "level":[I
    aget v2, v1, v4

    .line 108
    .local v2, "restorePoint":I
    move-object v0, p0

    .line 109
    .local v0, "formatted":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 110
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    array-length v6, p2

    if-nez v6, :cond_1

    .end local p1    # "format":Ljava/lang/String;
    :goto_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-gt v5, v10, :cond_2

    .line 115
    :goto_1
    :try_start_0
    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    .line 116
    const/4 v5, 0x0

    aget v6, v1, v5

    add-int/lit8 v6, v6, 0x1

    aput v6, v1, v5
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    .end local v0    # "formatted":Ljava/lang/String;
    .end local v1    # "level":[I
    .end local v2    # "restorePoint":I
    :goto_2
    return v2

    .line 110
    .restart local v0    # "formatted":Ljava/lang/String;
    .restart local v1    # "level":[I
    .restart local v2    # "restorePoint":I
    .restart local p1    # "format":Ljava/lang/String;
    :cond_1
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v6, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 112
    .end local p1    # "format":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0, v4, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 117
    :catch_0
    move-exception v3

    .line 118
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Trouble starting systrace section"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v5, v3, v6, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .end local v0    # "formatted":Ljava/lang/String;
    .end local v1    # "level":[I
    .end local v2    # "restorePoint":I
    .end local v3    # "t":Ljava/lang/Throwable;
    .restart local p1    # "format":Ljava/lang/String;
    :cond_3
    move v2, v4

    .line 123
    goto :goto_2
.end method

.method public static currentRestorePoint()I
    .locals 2

    .prologue
    .line 84
    sget-object v0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->nestingLevel:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public static endSection()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 132
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->enabled:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 133
    invoke-static {}, Landroid/os/Trace;->endSection()V

    .line 134
    sget-object v0, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->nestingLevel:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    const/4 v1, 0x0

    aget v2, v0, v1

    add-int/lit8 v2, v2, -0x1

    aput v2, v0, v1

    .line 136
    :cond_0
    return-void
.end method

.method public static endSection(I)V
    .locals 4
    .param p0, "traceRestorePoint"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 143
    sget-boolean v1, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->enabled:Z

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 144
    sget-object v1, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->nestingLevel:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .local v0, "level":[I
    :goto_0
    aget v1, v0, v3

    if-le v1, p0, :cond_0

    .line 145
    invoke-static {}, Landroid/os/Trace;->endSection()V

    .line 144
    aget v1, v0, v3

    add-int/lit8 v1, v1, -0x1

    aput v1, v0, v3

    goto :goto_0

    .line 148
    .end local v0    # "level":[I
    :cond_0
    return-void
.end method
