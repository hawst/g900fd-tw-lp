.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LayoutDetails"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;


# instance fields
.field private bitField0_:I

.field private location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

.field private matchParentExtent_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1286
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1287
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1311
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->matchParentExtent_:Z

    .line 1287
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;
    .locals 3

    .prologue
    .line 1341
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1345
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-eqz v2, :cond_0

    .line 1346
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    .line 1348
    :cond_0
    return-object v0

    .line 1342
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;
    :catch_0
    move-exception v1

    .line 1343
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1283
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1353
    if-ne p1, p0, :cond_1

    .line 1356
    :cond_0
    :goto_0
    return v1

    .line 1354
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1355
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    .line 1356
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-nez v3, :cond_3

    :goto_1
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->matchParentExtent_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->matchParentExtent_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getLocation()Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;
    .locals 1

    .prologue
    .line 1294
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    return-object v0
.end method

.method public getMatchParentExtent()Z
    .locals 1

    .prologue
    .line 1313
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->matchParentExtent_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1380
    const/4 v0, 0x0

    .line 1381
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-eqz v1, :cond_0

    .line 1382
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    .line 1383
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1385
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 1386
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->matchParentExtent_:Z

    .line 1387
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1389
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->cachedSize:I

    .line 1390
    return v0
.end method

.method public hasLocation()Z
    .locals 1

    .prologue
    .line 1304
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 1361
    const/16 v0, 0x11

    .line 1362
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 1363
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 1364
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->matchParentExtent_:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    add-int v0, v2, v1

    .line 1365
    return v0

    .line 1363
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->hashCode()I

    move-result v1

    goto :goto_0

    .line 1364
    :cond_1
    const/4 v1, 0x2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1398
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1399
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1403
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1404
    :sswitch_0
    return-object p0

    .line 1409
    :sswitch_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-nez v1, :cond_1

    .line 1410
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    .line 1412
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1416
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->matchParentExtent_:Z

    .line 1417
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->bitField0_:I

    goto :goto_0

    .line 1399
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1283
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    move-result-object v0

    return-object v0
.end method

.method public setLocation(Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;)Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    .prologue
    .line 1297
    if-nez p1, :cond_0

    .line 1298
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1300
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    .line 1301
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1370
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-eqz v0, :cond_0

    .line 1371
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->location_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1373
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 1374
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->matchParentExtent_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1376
    :cond_1
    return-void
.end method
