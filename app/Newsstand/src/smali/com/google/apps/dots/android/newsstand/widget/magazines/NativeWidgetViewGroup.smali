.class abstract Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;
.super Landroid/view/ViewGroup;
.source "NativeWidgetViewGroup.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetParent;


# instance fields
.field protected final nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

.field protected nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 22
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .line 23
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    .line 24
    return-void
.end method


# virtual methods
.method public applyLayoutToViewCoordsTransform(Landroid/graphics/Matrix;)V
    .locals 1
    .param p1, "output"    # Landroid/graphics/Matrix;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->applyDefaultLayoutToViewCoordsTransform(Landroid/graphics/Matrix;)V

    .line 40
    return-void
.end method

.method protected getContentArea()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->getContentArea()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public isLoadComplete()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->isLoadComplete()Z

    move-result v0

    return v0
.end method

.method public varargs notify(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "optionalArgs"    # [Ljava/lang/Object;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->notify(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 96
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    invoke-interface {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->onDestroyed(Landroid/view/View;)V

    .line 99
    :cond_0
    return-void
.end method

.method public onLoadComplete()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->onLoadComplete()V

    .line 76
    return-void
.end method

.method public onPageChanged()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->onPageChanged()V

    .line 86
    return-void
.end method

.method public onScrolled()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->onScrolled()V

    .line 81
    return-void
.end method

.method public onTransformChanged()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->onTransformChanged()V

    .line 35
    return-void
.end method

.method public onUnhandledClick()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->onUnhandledClick()V

    .line 61
    return-void
.end method

.method public onUnhandledFling(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)V
    .locals 1
    .param p1, "flingDirection"    # Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->onUnhandledFling(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)V

    .line 66
    return-void
.end method

.method public onZoomAttempt()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->onZoomAttempt()V

    .line 71
    return-void
.end method

.method public setContentArea(FFFF)V
    .locals 1
    .param p1, "l"    # F
    .param p2, "t"    # F
    .param p3, "r"    # F
    .param p4, "b"    # F

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->setContentArea(FFFF)V

    .line 50
    return-void
.end method

.method public setNBContext(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V
    .locals 0
    .param p1, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .line 28
    return-void
.end method
