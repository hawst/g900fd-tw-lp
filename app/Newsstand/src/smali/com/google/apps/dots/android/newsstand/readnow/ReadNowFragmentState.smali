.class public Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;
.super Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;
.source "ReadNowFragmentState.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "pageEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 23
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 32
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 33
    check-cast v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    .line 34
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 36
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 27
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "{ReadNowFragmentState: %s}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 52
    return-void
.end method
