.class public Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;
.super Ljava/lang/Object;
.source "ProtoUtil.java"


# direct methods
.method public static decodeBase64(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2
    .param p0, "base64"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "output":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    if-eqz p0, :cond_0

    .line 43
    const/16 v1, 0xb

    invoke-static {p0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 44
    .local v0, "bytes":[B
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    .line 46
    .end local v0    # "bytes":[B
    :cond_0
    return-object p1
.end method

.method public static encodeBase64(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;
    .locals 2
    .param p0, "proto"    # Lcom/google/protobuf/nano/MessageNano;

    .prologue
    .line 55
    if-eqz p0, :cond_0

    .line 56
    invoke-static {p0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    const/16 v1, 0xb

    .line 55
    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static readFromStream(Lcom/google/protobuf/nano/MessageNano;Ljava/io/InputStream;I)Lcom/google/protobuf/nano/MessageNano;
    .locals 8
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "sizeGuess"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(TT;",
            "Ljava/io/InputStream;",
            "I)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    .local p0, "proto":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    const-string v5, "ProtoUtil-readFromStream"

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;)I

    .line 76
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bytePool()Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v0

    .line 77
    .local v0, "bytePool":Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->acquire(I)[B

    move-result-object v1

    .line 79
    .local v1, "bytes":[B
    const/4 v4, 0x0

    .line 81
    .local v4, "readSoFar":I
    :goto_0
    :try_start_0
    array-length v5, v1

    sub-int/2addr v5, v4

    invoke-static {p1, v1, v4, v5}, Lcom/google/common/io/ByteStreams;->read(Ljava/io/InputStream;[BII)I

    move-result v3

    .line 82
    .local v3, "read":I
    add-int/2addr v4, v3

    .line 83
    array-length v5, v1

    if-ge v4, v5, :cond_0

    .line 94
    const/4 v5, 0x0

    invoke-static {v1, v5, v4}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->newInstance([BII)Lcom/google/protobuf/nano/CodedInputByteBufferNano;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 97
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 99
    return-object p0

    .line 87
    :cond_0
    move-object v2, v1

    .line 88
    .local v2, "oldBytes":[B
    :try_start_1
    array-length v5, v1

    mul-int/lit8 v5, v5, 0x2

    invoke-virtual {v0, v5}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->acquire(I)[B

    move-result-object v1

    .line 89
    const/4 v5, 0x0

    const/4 v6, 0x0

    array-length v7, v2

    invoke-static {v2, v5, v1, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 90
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 96
    .end local v2    # "oldBytes":[B
    .end local v3    # "read":I
    :catchall_0
    move-exception v5

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 97
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    throw v5
.end method

.method public static toString(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;
    .locals 1
    .param p0, "message"    # Lcom/google/protobuf/nano/MessageNano;

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/google/protobuf/nano/MessageNano;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toStringer(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/Object;
    .locals 1
    .param p0, "message"    # Lcom/google/protobuf/nano/MessageNano;

    .prologue
    .line 107
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil$1;-><init>(Lcom/google/protobuf/nano/MessageNano;)V

    return-object v0
.end method

.method public static writeToStream(Lcom/google/protobuf/nano/MessageNano;Ljava/io/OutputStream;)V
    .locals 4
    .param p0, "proto"    # Lcom/google/protobuf/nano/MessageNano;
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    const-string v3, "ProtoUtil-writeToStream"

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;)I

    .line 61
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bytePool()Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v0

    .line 62
    .local v0, "bytePool":Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-virtual {p0}, Lcom/google/protobuf/nano/MessageNano;->getSerializedSize()I

    move-result v2

    .line 63
    .local v2, "protoSize":I
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->acquire(I)[B

    move-result-object v1

    .line 65
    .local v1, "bytes":[B
    :try_start_0
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/CodedOutputByteBufferNano;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 66
    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 69
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 71
    return-void

    .line 68
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 69
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    throw v3
.end method
