.class public Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;
.super Ljava/lang/Object;
.source "AdSettingsJsonSerializer.java"


# instance fields
.field private final includeTemplates:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1, "includeTemplates"    # Z

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->includeTemplates:Z

    .line 22
    return-void
.end method


# virtual methods
.method public encode([Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;)Lorg/codehaus/jackson/node/ArrayNode;
    .locals 5
    .param p1, "units"    # [Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .prologue
    .line 78
    sget-object v2, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v2}, Lorg/codehaus/jackson/node/JsonNodeFactory;->arrayNode()Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v0

    .line 79
    .local v0, "array":Lorg/codehaus/jackson/node/ArrayNode;
    if-nez p1, :cond_1

    .line 85
    :cond_0
    return-object v0

    .line 82
    :cond_1
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, p1, v2

    .line 83
    .local v1, "unit":Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/codehaus/jackson/node/ArrayNode;->add(Lorg/codehaus/jackson/JsonNode;)V

    .line 82
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public encode([Lcom/google/apps/dots/proto/client/DotsShared$Size;)Lorg/codehaus/jackson/node/ArrayNode;
    .locals 5
    .param p1, "sizes"    # [Lcom/google/apps/dots/proto/client/DotsShared$Size;

    .prologue
    .line 103
    sget-object v2, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v2}, Lorg/codehaus/jackson/node/JsonNodeFactory;->arrayNode()Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v0

    .line 104
    .local v0, "array":Lorg/codehaus/jackson/node/ArrayNode;
    if-nez p1, :cond_1

    .line 110
    :cond_0
    return-object v0

    .line 107
    :cond_1
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, p1, v2

    .line 108
    .local v1, "size":Lcom/google/apps/dots/proto/client/DotsShared$Size;
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$Size;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/codehaus/jackson/node/ArrayNode;->add(Lorg/codehaus/jackson/JsonNode;)V

    .line 107
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public encode(Lcom/google/apps/dots/proto/client/DotsShared$AdContent;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "content"    # Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    .prologue
    .line 114
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 115
    .local v0, "object":Lorg/codehaus/jackson/node/ObjectNode;
    if-nez p1, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-object v0

    .line 118
    :cond_1
    const-string v1, "adSystem"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdSystem()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 119
    const-string v1, "phoneUnits"

    iget-object v2, p1, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->phoneUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode([Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;)Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 120
    const-string v1, "tabletUnits"

    iget-object v2, p1, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->tabletUnits:[Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode([Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;)Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 121
    const-string v1, "isReady"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getIsReady()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Z)V

    .line 122
    const-string v1, "houseAdsEnabled"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getHouseAdsEnabled()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Z)V

    .line 125
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->hasPublisherId()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    const-string v1, "publisherId"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getPublisherId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_2
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->includeTemplates:Z

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->hasPhoneTemplate()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 129
    const-string v1, "phoneTemplate"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getPhoneTemplate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_3
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->includeTemplates:Z

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->hasTabletTemplate()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 132
    const-string v1, "tabletTemplate"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getTabletTemplate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_4
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->hasAdTemplateId()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    const-string v1, "adTemplateId"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdTemplateId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public encode(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "settings"    # Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .prologue
    .line 39
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 40
    .local v0, "object":Lorg/codehaus/jackson/node/ObjectNode;
    if-nez p1, :cond_0

    .line 45
    :goto_0
    return-object v0

    .line 43
    :cond_0
    const-string v1, "googleSold"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getGoogleSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$AdContent;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 44
    const-string v1, "pubSold"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getPubSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$AdContent;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    goto :goto_0
.end method

.method public encode(Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "settings"    # Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    .prologue
    .line 25
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 26
    .local v0, "object":Lorg/codehaus/jackson/node/ObjectNode;
    if-nez p1, :cond_0

    .line 35
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "min"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->getMin()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 31
    const-string v1, "max"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->getMax()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 32
    const-string v1, "every"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->getEvery()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 33
    const-string v1, "page"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->getPage()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 34
    const-string v1, "articleFrequency"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->getArticleFrequency()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public encode(Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "unit"    # Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;

    .prologue
    .line 49
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 50
    .local v0, "object":Lorg/codehaus/jackson/node/ObjectNode;
    if-nez p1, :cond_1

    .line 74
    :cond_0
    :goto_0
    return-object v0

    .line 53
    :cond_1
    const-string v1, "location"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->getLocation()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 56
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->hasOrientationRestrict()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 57
    const-string v1, "orientationRestrict"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->getOrientationRestrict()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 59
    :cond_2
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->hasName()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 60
    const-string v1, "name"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_3
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->hasCode()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 63
    const-string v1, "code"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->getCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_4
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->hasWidth()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 66
    const-string v1, "width"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->getWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 68
    :cond_5
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->hasHeight()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 69
    const-string v1, "height"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 71
    :cond_6
    iget-object v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 72
    const-string v1, "size"

    iget-object v2, p1, Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;->size:[Lcom/google/apps/dots/proto/client/DotsShared$Size;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode([Lcom/google/apps/dots/proto/client/DotsShared$Size;)Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    goto :goto_0
.end method

.method public encode(Lcom/google/apps/dots/proto/client/DotsShared$Size;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "size"    # Lcom/google/apps/dots/proto/client/DotsShared$Size;

    .prologue
    .line 89
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 90
    .local v0, "object":Lorg/codehaus/jackson/node/ObjectNode;
    if-nez p1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-object v0

    .line 93
    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Size;->hasWidth()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 94
    const-string v1, "width"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Size;->getWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 96
    :cond_2
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Size;->hasHeight()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    const-string v1, "height"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Size;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    goto :goto_0
.end method
