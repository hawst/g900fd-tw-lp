.class public abstract Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;
.super Ljava/lang/Object;
.source "NSBaseErrorViewProvider.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/ViewProvider;


# instance fields
.field protected headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 23
    return-void
.end method


# virtual methods
.method public abstract getErrorMessageData()Lcom/google/android/libraries/bind/data/Data;
.end method

.method public getView(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;
    .locals 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    const/4 v3, 0x0

    .line 35
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->LAYOUT:I

    .line 36
    invoke-static {p1}, Lcom/google/android/libraries/bind/data/DataAdapter;->getFullScreenLayoutParams(Landroid/view/ViewGroup;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 35
    invoke-virtual {p2, v1, v3, v2}, Lcom/google/android/libraries/bind/view/ViewHeap;->get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    .line 38
    .local v0, "errorView":Lcom/google/apps/dots/android/newsstand/card/ActionMessage;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;->getErrorMessageData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v6

    .line 39
    .local v6, "errorMessageData":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v6, :cond_0

    .line 40
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-static {v6, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->addHeaderPadding(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/android/libraries/bind/data/Data;

    .line 41
    invoke-virtual {v0, v6}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    .line 46
    :goto_0
    return-object v0

    .line 44
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_error:I

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->configure(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;ILjava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setHeaderType(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;
    .locals 0
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 30
    return-object p0
.end method
