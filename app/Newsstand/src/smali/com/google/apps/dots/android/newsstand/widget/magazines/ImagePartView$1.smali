.class Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;
.super Ljava/lang/Object;
.source "ImagePartView.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->downloadAndInitBounds()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->setImageInfo(Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;)V

    .line 78
    return-void
.end method

.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V
    .locals 3
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    .prologue
    .line 66
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->access$002(Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    .line 68
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeAssetFileDescriptor()Landroid/content/res/AssetFileDescriptor;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/bitmap/BitmapUtil;->getBitmapInfo(Landroid/content/res/AssetFileDescriptor;)Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    move-result-object v2

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->access$102(Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;)Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    .line 69
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->access$100(Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;)Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->setImageInfo(Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "ioe":Ljava/io/IOException;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V

    return-void
.end method
