.class Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$3;
.super Ljava/lang/Object;
.source "ExternalStorageDialog.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field destroyToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$3;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$3;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->destroyAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$3;->destroyToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    return-void
.end method


# virtual methods
.method public onAvailable()V
    .locals 3

    .prologue
    .line 109
    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "DiskCache available, dismissing"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$3;->destroyToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$3$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$3$1;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$3;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 117
    return-void
.end method

.method public onClosed()V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public onUnmounted()V
    .locals 0

    .prologue
    .line 97
    return-void
.end method
