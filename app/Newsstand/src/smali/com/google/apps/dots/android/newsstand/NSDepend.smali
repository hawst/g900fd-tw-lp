.class public Lcom/google/apps/dots/android/newsstand/NSDepend;
.super Ljava/lang/Object;
.source "NSDepend.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field protected static appContext:Landroid/content/Context;

.field protected static classLoaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ClassLoader;",
            ">;"
        }
    .end annotation
.end field

.field protected static impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

.field private static lock:Ljava/lang/Object;

.field private static setupIsDone:Z

.field private static final whenSetup:Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field protected accountManager:Landroid/accounts/AccountManager;

.field protected accountNameManager:Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

.field protected adShieldClient:Lcom/google/android/gms/ads/adshield/AdShieldClient;

.field protected appFamilySummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

.field protected appStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

.field protected appSummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

.field protected attachmentStore:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

.field protected attachmentViewCache:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

.field protected authHelper:Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

.field protected bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

.field protected bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;

.field protected colorHelper:Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

.field protected configUtil:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

.field protected connectivityManager:Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

.field protected cookiePolicy:Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

.field protected defaultCookieManager:Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;

.field protected diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

.field protected diskCacheManager:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

.field protected eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

.field protected formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

.field protected formTemplateStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

.field protected globalManifest:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

.field protected httpContentService:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;

.field protected itemJsonSerializer:Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

.field protected layoutStore:Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;

.field protected mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

.field protected nsClient:Lcom/google/apps/dots/android/newsstand/http/NSClient;

.field protected nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field protected pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

.field protected playCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

.field protected postStore:Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

.field protected prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field protected readingActivityTracker:Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

.field protected recentlyReadHelper:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

.field protected sectionStore:Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

.field protected serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

.field protected storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

.field protected tracker:Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;

.field protected upgrade:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

.field protected util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

.field protected viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

.field protected webViewTracker:Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

.field protected webViewUriWhitelist:Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

.field protected webviewHttpClientPool:Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const-class v0, Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 112
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->classLoaders:Ljava/util/Map;

    .line 114
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->lock:Ljava/lang/Object;

    .line 116
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;->create(Z)Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->whenSetup:Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public static accountManager()Landroid/accounts/AccountManager;
    .locals 1

    .prologue
    .line 369
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getAccountManager()Landroid/accounts/AccountManager;

    move-result-object v0

    return-object v0
.end method

.method public static accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;
    .locals 1

    .prologue
    .line 394
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getAccountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v0

    return-object v0
.end method

.method public static adShieldClient()Lcom/google/android/gms/ads/adshield/AdShieldClient;
    .locals 1

    .prologue
    .line 852
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getAdShieldClient()Lcom/google/android/gms/ads/adshield/AdShieldClient;

    move-result-object v0

    return-object v0
.end method

.method public static appContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 257
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    return-object v0
.end method

.method public static appFamilySummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;
    .locals 1

    .prologue
    .line 674
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getAppFamilySummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    move-result-object v0

    return-object v0
.end method

.method public static appStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;
    .locals 1

    .prologue
    .line 596
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getAppStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

    move-result-object v0

    return-object v0
.end method

.method public static appSummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;
    .locals 1

    .prologue
    .line 661
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getAppSummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    move-result-object v0

    return-object v0
.end method

.method public static attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;
    .locals 1

    .prologue
    .line 687
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getAttachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v0

    return-object v0
.end method

.method public static attachmentViewCache()Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;
    .locals 1

    .prologue
    .line 918
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getAttachmentViewCache()Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    move-result-object v0

    return-object v0
.end method

.method public static authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;
    .locals 1

    .prologue
    .line 382
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getAuthHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    move-result-object v0

    return-object v0
.end method

.method public static bitmapPool()Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    .locals 1

    .prologue
    .line 544
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getBitmapPool()Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v0

    return-object v0
.end method

.method public static bytePool()Lcom/google/apps/dots/android/newsstand/util/BytePool;
    .locals 1

    .prologue
    .line 557
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getBytePool()Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v0

    return-object v0
.end method

.method private static checkApplication()V
    .locals 2

    .prologue
    .line 1064
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    const-string v1, "NSDepend.setup was never called"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1065
    return-void
.end method

.method public static colorHelper()Lcom/google/apps/dots/android/newsstand/util/ColorHelper;
    .locals 1

    .prologue
    .line 518
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getColorHelper()Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

    move-result-object v0

    return-object v0
.end method

.method public static configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;
    .locals 1

    .prologue
    .line 826
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getConfigUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v0

    return-object v0
.end method

.method public static connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;
    .locals 1

    .prologue
    .line 505
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getConnectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    return-object v0
.end method

.method public static cookiePolicy()Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;
    .locals 1

    .prologue
    .line 878
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getCookiePolicy()Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    move-result-object v0

    return-object v0
.end method

.method public static defaultCookieManager()Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;
    .locals 1

    .prologue
    .line 891
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getDefaultCookieManager()Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;

    move-result-object v0

    return-object v0
.end method

.method public static diskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    .locals 1

    .prologue
    .line 433
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getDiskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v0

    return-object v0
.end method

.method public static diskCacheManager()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;
    .locals 1

    .prologue
    .line 420
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getDiskCacheManager()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    move-result-object v0

    return-object v0
.end method

.method private static dynamicallyLoadLibrary(Ljava/lang/String;)Ljava/lang/ClassLoader;
    .locals 12
    .param p0, "jar"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 1095
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1097
    .local v4, "res":Landroid/content/res/Resources;
    new-instance v2, Ljava/io/File;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->dex_storage_dir:I

    .line 1098
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v11}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    invoke-direct {v2, v5, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1100
    .local v2, "dexInternalStoragePath":Ljava/io/File;
    const/4 v0, 0x0

    .line 1102
    .local v0, "bis":Ljava/io/BufferedInputStream;
    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    .line 1103
    invoke-virtual {v5}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->asset_libs_dir:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1104
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .local v1, "bis":Ljava/io/BufferedInputStream;
    const/4 v5, 0x0

    :try_start_1
    new-array v5, v5, [Lcom/google/common/io/FileWriteMode;

    invoke-static {v2, v5}, Lcom/google/common/io/Files;->asByteSink(Ljava/io/File;[Lcom/google/common/io/FileWriteMode;)Lcom/google/common/io/ByteSink;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/common/io/ByteSink;->writeFrom(Ljava/io/InputStream;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1106
    if-eqz v1, :cond_0

    .line 1107
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 1112
    :cond_0
    sget-object v5, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->dex_output_prefix:I

    .line 1113
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v11}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    .line 1114
    .local v3, "optimizedDexOutputPath":Ljava/io/File;
    new-instance v5, Ldalvik/system/DexClassLoader;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    .line 1115
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    sget-object v9, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    invoke-direct {v5, v6, v7, v8, v9}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    return-object v5

    .line 1106
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v3    # "optimizedDexOutputPath":Ljava/io/File;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    :catchall_0
    move-exception v5

    :goto_0
    if-eqz v0, :cond_1

    .line 1107
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    :cond_1
    throw v5

    .line 1106
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_0
.end method

.method public static eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;
    .locals 1

    .prologue
    .line 839
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getUriNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v0

    return-object v0
.end method

.method public static feedbackMechanism()Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;
    .locals 1

    .prologue
    .line 262
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->makeFeedbackMechanism()Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;

    move-result-object v0

    return-object v0
.end method

.method public static formStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;
    .locals 1

    .prologue
    .line 648
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFormStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    move-result-object v0

    return-object v0
.end method

.method public static formTemplateStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;
    .locals 1

    .prologue
    .line 609
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFormTemplateStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

    move-result-object v0

    return-object v0
.end method

.method public static getBooleanResource(I)Z
    .locals 1
    .param p0, "resId"    # I

    .prologue
    .line 979
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->checkApplication()V

    .line 980
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public static getClassLoaderForJar(Ljava/lang/String;)Ljava/lang/ClassLoader;
    .locals 8
    .param p0, "jar"    # Ljava/lang/String;

    .prologue
    .line 1043
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->checkApplication()V

    .line 1044
    const/4 v1, 0x0

    .line 1046
    .local v1, "classLoader":Ljava/lang/ClassLoader;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/NSDepend;->classLoaders:Ljava/util/Map;

    monitor-enter v4

    .line 1047
    :try_start_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSDepend;->classLoaders:Ljava/util/Map;

    invoke-interface {v3, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1048
    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSDepend;->classLoaders:Ljava/util/Map;

    invoke-interface {v3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/ClassLoader;

    move-object v1, v0

    .line 1059
    :goto_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1060
    return-object v1

    .line 1051
    :cond_0
    :try_start_1
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->dynamicallyLoadLibrary(Ljava/lang/String;)Ljava/lang/ClassLoader;

    move-result-object v1

    .line 1052
    sget-object v5, Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Extra library loaded: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v5, v3, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1057
    :goto_2
    :try_start_2
    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSDepend;->classLoaders:Ljava/util/Map;

    invoke-interface {v3, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1059
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 1052
    :cond_1
    :try_start_3
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1053
    :catch_0
    move-exception v2

    .line 1054
    .local v2, "e":Ljava/lang/Exception;
    :try_start_4
    sget-object v5, Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Could not load library: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_3
    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v5, v2, v3, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3
.end method

.method public static getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;
    .locals 1
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "resId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/apps/dots/android/newsstand/activity/NSActivity;",
            "I)TT;"
        }
    .end annotation

    .prologue
    .line 968
    const-string v0, "Must supply an activity to get a fragment"

    invoke-static {p0, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 969
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public static getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 989
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->checkApplication()V

    .line 990
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public static getStringResource(I)Ljava/lang/String;
    .locals 1
    .param p0, "resId"    # I

    .prologue
    .line 984
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->checkApplication()V

    .line 985
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static globalManifest()Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;
    .locals 1

    .prologue
    .line 446
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getManifestBlobResolver()Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    move-result-object v0

    return-object v0
.end method

.method public static httpContentService()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;
    .locals 1

    .prologue
    .line 714
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getHttpContentService()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;

    move-result-object v0

    return-object v0
.end method

.method protected static initClassLoaders()V
    .locals 2

    .prologue
    .line 1079
    new-instance v0, Lcom/google/apps/dots/android/newsstand/NSDepend$4;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/NSDepend$4;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    const/4 v1, 0x0

    .line 1087
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/NSDepend$4;->execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1088
    return-void
.end method

.method public static isSetupDone()Z
    .locals 1

    .prologue
    .line 951
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->setupIsDone:Z

    return v0
.end method

.method public static itemJsonSerializer()Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;
    .locals 1

    .prologue
    .line 570
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getItemJsonSerializer()Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    move-result-object v0

    return-object v0
.end method

.method public static layoutStore()Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;
    .locals 1

    .prologue
    .line 701
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getLayoutStore()Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;

    move-result-object v0

    return-object v0
.end method

.method public static mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;
    .locals 1

    .prologue
    .line 492
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getMutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v0

    return-object v0
.end method

.method public static nsClient()Lcom/google/apps/dots/android/newsstand/http/NSClient;
    .locals 1

    .prologue
    .line 407
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getNSClient()Lcom/google/apps/dots/android/newsstand/http/NSClient;

    move-result-object v0

    return-object v0
.end method

.method public static nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .locals 1

    .prologue
    .line 472
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getNSStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v0

    return-object v0
.end method

.method public static pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;
    .locals 1

    .prologue
    .line 800
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getPinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v0

    return-object v0
.end method

.method public static playCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;
    .locals 1

    .prologue
    .line 741
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getPlayCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;

    move-result-object v0

    return-object v0
.end method

.method public static postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;
    .locals 1

    .prologue
    .line 635
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getPostStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v0

    return-object v0
.end method

.method public static prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .locals 1

    .prologue
    .line 298
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getPrefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    return-object v0
.end method

.method public static readingActivityTracker()Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;
    .locals 1

    .prologue
    .line 531
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getReadingActivityTracker()Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    move-result-object v0

    return-object v0
.end method

.method public static recentlyReadHelper()Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;
    .locals 1

    .prologue
    .line 813
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getRecentlyReadHelper()Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    move-result-object v0

    return-object v0
.end method

.method public static sectionStore()Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;
    .locals 1

    .prologue
    .line 622
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getSectionStore()Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    move-result-object v0

    return-object v0
.end method

.method public static serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .locals 1

    .prologue
    .line 356
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getServerUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    return-object v0
.end method

.method public static setup(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 127
    const-string v0, "context cannot be null"

    invoke-static {p0, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 130
    :try_start_0
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->setupIsDone:Z

    if-eqz v0, :cond_0

    .line 131
    monitor-exit v1

    .line 146
    :goto_0
    return-void

    .line 133
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Setup"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    .line 135
    new-instance v0, Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    .line 136
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isRunningInFeedbackProcess(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->setupInternalFeedback()V

    .line 141
    :goto_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->setupIsDone:Z

    .line 142
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Setup complete."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->whenSetup:Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;->set(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->setupInternal()V

    goto :goto_1

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected static setupContentAuthority()V
    .locals 3

    .prologue
    .line 121
    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->content_authority:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "contentAuthority":Ljava/lang/String;
    const-string v1, "MUST_OVERRIDE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 123
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->init(Ljava/lang/String;)V

    .line 124
    return-void

    .line 122
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static setupSystemSettingListeners()V
    .locals 4

    .prologue
    .line 726
    new-instance v1, Lcom/google/apps/dots/android/newsstand/NSDepend$1;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/NSDepend$1;-><init>(Landroid/os/Handler;)V

    .line 734
    .local v1, "fontSizeObserver":Landroid/database/ContentObserver;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 735
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string v2, "font_scale"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 737
    return-void
.end method

.method public static storeCache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    .locals 1

    .prologue
    .line 459
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getStoreCache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v0

    return-object v0
.end method

.method public static tracker()Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;
    .locals 1

    .prologue
    .line 324
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getTracker()Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;

    move-result-object v0

    return-object v0
.end method

.method public static trimCaches(F)V
    .locals 3
    .param p0, "fraction"    # F

    .prologue
    .line 933
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "clearCaches"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 935
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->setupIsDone:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    if-eqz v0, :cond_0

    .line 936
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->storeCache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->trim(F)V

    .line 937
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->trimNativeBodyMemory(F)V

    .line 938
    invoke-static {}, Lcom/google/android/libraries/bind/card/CardListView;->clearScrapBitmap()V

    .line 940
    :cond_0
    return-void
.end method

.method public static trimNativeBodyMemory(F)V
    .locals 1
    .param p0, "fraction"    # F

    .prologue
    .line 946
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bytePool()Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->trim(F)V

    .line 947
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bitmapPool()Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->trim(F)V

    .line 948
    return-void
.end method

.method public static upgrade()Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;
    .locals 1

    .prologue
    .line 311
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getUpgrade()Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    move-result-object v0

    return-object v0
.end method

.method public static util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
    .locals 1

    .prologue
    .line 285
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getUtil()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    return-object v0
.end method

.method public static viewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;
    .locals 1

    .prologue
    .line 787
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getViewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;

    move-result-object v0

    return-object v0
.end method

.method public static webViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;
    .locals 1

    .prologue
    .line 583
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getWebViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    move-result-object v0

    return-object v0
.end method

.method public static webViewUriWhitelist()Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;
    .locals 1

    .prologue
    .line 865
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getWebViewUriWhitelist()Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    move-result-object v0

    return-object v0
.end method

.method public static webviewHttpClientPool()Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;
    .locals 1

    .prologue
    .line 905
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->impl:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getWebviewHttpClientPool()Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;

    move-result-object v0

    return-object v0
.end method

.method public static whenSetup()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 152
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->whenSetup:Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;

    return-object v0
.end method


# virtual methods
.method public getAccountManager()Landroid/accounts/AccountManager;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountManager:Landroid/accounts/AccountManager;

    if-nez v0, :cond_0

    .line 374
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountManager:Landroid/accounts/AccountManager;

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountManager:Landroid/accounts/AccountManager;

    return-object v0
.end method

.method public getAccountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager:Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    if-nez v0, :cond_0

    .line 399
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountManager()Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;->create(Landroid/accounts/AccountManager;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)Lcom/google/apps/dots/android/newsstand/auth/NSAccountNameManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager:Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager:Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    return-object v0
.end method

.method public getAdShieldClient()Lcom/google/android/gms/ads/adshield/AdShieldClient;
    .locals 3

    .prologue
    .line 856
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->adShieldClient:Lcom/google/android/gms/ads/adshield/AdShieldClient;

    if-nez v0, :cond_0

    .line 857
    new-instance v0, Lcom/google/android/gms/ads/adshield/AdShieldClient;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->getVersionLabel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/ads/adshield/AdShieldClient;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->adShieldClient:Lcom/google/android/gms/ads/adshield/AdShieldClient;

    .line 859
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->adShieldClient:Lcom/google/android/gms/ads/adshield/AdShieldClient;

    return-object v0
.end method

.method public getAppFamilySummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;
    .locals 3

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appFamilySummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    if-nez v0, :cond_0

    .line 679
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->globalManifest()Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appFamilySummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    .line 681
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appFamilySummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    return-object v0
.end method

.method public getAppStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;
    .locals 2

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

    if-nez v0, :cond_0

    .line 601
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

    .line 603
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

    return-object v0
.end method

.method public getAppSummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;
    .locals 3

    .prologue
    .line 665
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appSummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    if-nez v0, :cond_0

    .line 666
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->globalManifest()Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appSummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    .line 668
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->appSummaryStore:Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    return-object v0
.end method

.method public getAttachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;
    .locals 7

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    if-nez v0, :cond_0

    .line 692
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->sectionStore()Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    move-result-object v2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->formStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    move-result-object v3

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v4

    .line 693
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bytePool()Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v5

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bitmapPool()Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;Lcom/google/apps/dots/android/newsstand/util/BytePool;Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .line 695
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    return-object v0
.end method

.method public getAttachmentViewCache()Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;
    .locals 2

    .prologue
    .line 922
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentViewCache:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    if-nez v0, :cond_0

    .line 923
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bitmapPool()Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;-><init>(Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentViewCache:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    .line 925
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentViewCache:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    return-object v0
.end method

.method public getAuthHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;
    .locals 4

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper:Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    if-nez v0, :cond_0

    .line 387
    new-instance v0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountManager()Landroid/accounts/AccountManager;

    move-result-object v2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;-><init>(Landroid/content/Context;Landroid/accounts/AccountManager;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper:Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper:Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    return-object v0
.end method

.method public getBitmapPool()Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    if-nez v0, :cond_0

    .line 549
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;-><init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    .line 551
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    return-object v0
.end method

.method public getBytePool()Lcom/google/apps/dots/android/newsstand/util/BytePool;
    .locals 2

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;

    if-nez v0, :cond_0

    .line 562
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/BytePool;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;-><init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;

    .line 564
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->bytePool:Lcom/google/apps/dots/android/newsstand/util/BytePool;

    return-object v0
.end method

.method public getColorHelper()Lcom/google/apps/dots/android/newsstand/util/ColorHelper;
    .locals 2

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->colorHelper:Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

    if-nez v0, :cond_0

    .line 523
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;-><init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->colorHelper:Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

    .line 525
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->colorHelper:Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

    return-object v0
.end method

.method public getConfigUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;
    .locals 5

    .prologue
    .line 830
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    if-nez v0, :cond_0

    .line 831
    new-instance v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v3

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsClient()Lcom/google/apps/dots/android/newsstand/http/NSClient;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/http/NSClient;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    .line 833
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    return-object v0
.end method

.method public getConnectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;
    .locals 3

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager:Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    if-nez v0, :cond_0

    .line 510
    new-instance v0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager:Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager:Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    return-object v0
.end method

.method public getCookiePolicy()Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;
    .locals 1

    .prologue
    .line 882
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->cookiePolicy:Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    if-nez v0, :cond_0

    .line 883
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->cookiePolicy:Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    .line 885
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->cookiePolicy:Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    return-object v0
.end method

.method public getDefaultCookieManager()Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;
    .locals 2

    .prologue
    .line 895
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->defaultCookieManager:Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;

    if-nez v0, :cond_0

    .line 896
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;

    invoke-static {}, Ljava/net/CookieHandler;->getDefault()Ljava/net/CookieHandler;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;-><init>(Ljava/net/CookieHandler;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->defaultCookieManager:Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;

    .line 897
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->defaultCookieManager:Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;

    invoke-static {v0}, Ljava/net/CookieHandler;->setDefault(Ljava/net/CookieHandler;)V

    .line 899
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->defaultCookieManager:Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;

    return-object v0
.end method

.method public getDiskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    if-nez v0, :cond_0

    .line 438
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCacheManager()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->getDiskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    return-object v0
.end method

.method public getDiskCacheManager()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;
    .locals 4

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCacheManager:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    if-nez v0, :cond_0

    .line 425
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->upgrade()Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCacheManager:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCacheManager:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;

    return-object v0
.end method

.method public getFormStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;
    .locals 2

    .prologue
    .line 652
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    if-nez v0, :cond_0

    .line 653
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    .line 655
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    return-object v0
.end method

.method public getFormTemplateStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;
    .locals 2

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->formTemplateStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

    if-nez v0, :cond_0

    .line 614
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->formTemplateStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

    .line 616
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->formTemplateStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

    return-object v0
.end method

.method public getHttpContentService()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;
    .locals 2

    .prologue
    .line 718
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->httpContentService:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;

    if-nez v0, :cond_0

    .line 719
    new-instance v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->httpContentService:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;

    .line 721
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->httpContentService:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;

    return-object v0
.end method

.method public getItemJsonSerializer()Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->itemJsonSerializer:Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    if-nez v0, :cond_0

    .line 575
    new-instance v0, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->itemJsonSerializer:Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->itemJsonSerializer:Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    return-object v0
.end method

.method public getLayoutStore()Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;
    .locals 2

    .prologue
    .line 705
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->layoutStore:Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;

    if-nez v0, :cond_0

    .line 706
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->layoutStore:Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;

    .line 708
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->layoutStore:Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;

    return-object v0
.end method

.method public getManifestBlobResolver()Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->globalManifest:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    if-nez v0, :cond_0

    .line 451
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->globalManifest:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->globalManifest:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    return-object v0
.end method

.method public getMutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;
    .locals 5

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    if-nez v0, :cond_0

    .line 497
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->storeCache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v3

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;Lcom/google/apps/dots/android/newsstand/events/EventNotifier;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    return-object v0
.end method

.method public getNSClient()Lcom/google/apps/dots/android/newsstand/http/NSClient;
    .locals 3

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsClient:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    if-nez v0, :cond_0

    .line 412
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSClient;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/http/NSClient;-><init>(Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsClient:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsClient:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    return-object v0
.end method

.method public getNSStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .locals 8

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    if-nez v0, :cond_0

    .line 477
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 478
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v1

    .line 479
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->storeCache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    move-result-object v2

    .line 480
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->globalManifest()Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    move-result-object v3

    .line 481
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsClient()Lcom/google/apps/dots/android/newsstand/http/NSClient;

    move-result-object v4

    .line 482
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v5

    .line 483
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v6

    .line 484
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/store/NSStore;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;Lcom/google/apps/dots/android/newsstand/http/NSClient;Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/events/EventNotifier;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    return-object v0
.end method

.method public getPinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;
    .locals 5

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    if-nez v0, :cond_0

    .line 805
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v3

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;-><init>(Lcom/google/apps/dots/android/newsstand/events/EventNotifier;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;Lcom/google/apps/dots/android/newsstand/server/ServerUris;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    .line 807
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    return-object v0
.end method

.method public getPlayCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;
    .locals 3

    .prologue
    .line 745
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->playCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

    if-nez v0, :cond_0

    .line 750
    new-instance v0, Lcom/google/apps/dots/android/newsstand/NSDepend$3;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/NSDepend$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/NSDepend$2;-><init>(Lcom/google/apps/dots/android/newsstand/NSDepend;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/NSDepend$3;-><init>(Lcom/google/apps/dots/android/newsstand/NSDepend;Landroid/content/Context;Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->playCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

    .line 768
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->playCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

    return-object v0
.end method

.method public getPostStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;
    .locals 2

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore:Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    if-nez v0, :cond_0

    .line 640
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore:Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    .line 642
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore:Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    return-object v0
.end method

.method public getPrefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    if-nez v0, :cond_0

    .line 303
    new-instance v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    return-object v0
.end method

.method public getReadingActivityTracker()Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->readingActivityTracker:Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    if-nez v0, :cond_0

    .line 536
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->readingActivityTracker:Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->readingActivityTracker:Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    return-object v0
.end method

.method public getRecentlyReadHelper()Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;
    .locals 2

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->recentlyReadHelper:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    if-nez v0, :cond_0

    .line 818
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;-><init>(Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->recentlyReadHelper:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    .line 820
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->recentlyReadHelper:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    return-object v0
.end method

.method public getSectionStore()Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;
    .locals 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->sectionStore:Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    if-nez v0, :cond_0

    .line 627
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->sectionStore:Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    .line 629
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->sectionStore:Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    return-object v0
.end method

.method public getServerUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .locals 3

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    if-nez v0, :cond_0

    .line 361
    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    return-object v0
.end method

.method public getStoreCache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    .locals 2

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    if-nez v0, :cond_0

    .line 464
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;-><init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    return-object v0
.end method

.method public getTracker()Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;
    .locals 6

    .prologue
    .line 328
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->tracker:Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;

    if-nez v3, :cond_3

    .line 329
    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/analytics/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object v0

    .line 330
    .local v0, "gaTrackerManager":Lcom/google/android/gms/analytics/GoogleAnalytics;
    const/16 v3, 0x3c

    invoke-virtual {v0, v3}, Lcom/google/android/gms/analytics/GoogleAnalytics;->setLocalDispatchPeriod(I)V

    .line 331
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 332
    .local v1, "internalTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 333
    .local v2, "publisherTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;>;"
    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$bool;->enable_internal_analytics_proto_reporting:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 334
    new-instance v3, Lcom/google/apps/dots/android/newsstand/analytics/InternalProtoTracker;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/InternalProtoTracker;-><init>(Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/store/MutationStore;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->canCallGooglePlayServicesApi(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 339
    new-instance v3, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/analytics/PlayLogTracker;-><init>(Landroid/content/Context;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 342
    :cond_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$bool;->enable_internal_analytics_reporting:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 343
    new-instance v3, Lcom/google/apps/dots/android/newsstand/analytics/InternalGATracker;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v5

    invoke-direct {v3, v4, v0, v5}, Lcom/google/apps/dots/android/newsstand/analytics/InternalGATracker;-><init>(Landroid/content/Context;Lcom/google/android/gms/analytics/GoogleAnalytics;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    :cond_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$bool;->enable_publisher_analytics_reporting:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 346
    new-instance v3, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-direct {v3, v4, v0}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;-><init>(Landroid/content/Context;Lcom/google/android/gms/analytics/GoogleAnalytics;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    :cond_2
    new-instance v3, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;

    invoke-direct {v3, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;-><init>(Ljava/util/List;Ljava/util/List;)V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->tracker:Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;

    .line 350
    .end local v0    # "gaTrackerManager":Lcom/google/android/gms/analytics/GoogleAnalytics;
    .end local v1    # "internalTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;>;"
    .end local v2    # "publisherTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;>;"
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->tracker:Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;

    return-object v3
.end method

.method public getUpgrade()Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;
    .locals 3

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->upgrade:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    if-nez v0, :cond_0

    .line 316
    new-instance v0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->upgrade:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->upgrade:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    return-object v0
.end method

.method public getUriNotifier()Lcom/google/apps/dots/android/newsstand/events/EventNotifier;
    .locals 1

    .prologue
    .line 843
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    if-nez v0, :cond_0

    .line 844
    new-instance v0, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    .line 846
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    return-object v0
.end method

.method public getUtil()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    if-nez v0, :cond_0

    .line 290
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    return-object v0
.end method

.method public getViewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;
    .locals 2

    .prologue
    .line 791
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    if-nez v0, :cond_0

    .line 792
    new-instance v0, Lcom/google/android/libraries/bind/view/ViewHeap;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/bind/view/ViewHeap;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    .line 794
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    return-object v0
.end method

.method public getWebViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;
    .locals 2

    .prologue
    .line 587
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker:Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    if-nez v0, :cond_0

    .line 588
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker:Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    .line 590
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker:Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    return-object v0
.end method

.method public getWebViewUriWhitelist()Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;
    .locals 1

    .prologue
    .line 869
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewUriWhitelist:Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    if-nez v0, :cond_0

    .line 870
    new-instance v0, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewUriWhitelist:Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    .line 872
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewUriWhitelist:Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    return-object v0
.end method

.method public getWebviewHttpClientPool()Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;
    .locals 1

    .prologue
    .line 909
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->webviewHttpClientPool:Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;

    if-nez v0, :cond_0

    .line 910
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->webviewHttpClientPool:Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;

    .line 912
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/NSDepend;->webviewHttpClientPool:Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;

    return-object v0
.end method

.method public makeFeedbackMechanism()Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;
    .locals 3

    .prologue
    .line 266
    new-instance v0, Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/feedback/InternalCrashReportMechanism;-><init>(Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/server/ServerUris;)V

    return-object v0
.end method

.method protected setupInternal()V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 168
    :try_start_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    .line 169
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$bool;->enable_custom_crash_report:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 177
    .local v0, "enableCustomCrashReporting":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 180
    sget-object v2, Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Setting custom uncaught exception handler..."

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 181
    new-instance v2, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/NSInternalUncaughtExceptionHandler;-><init>(Landroid/content/Context;)V

    invoke-static {v2}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 184
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->setupContentAuthority()V

    .line 187
    sget-object v2, Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Starting async queues ..."

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->init()V

    .line 191
    sget-object v2, Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Setting dependencies..."

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    sget-object v2, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->initialize(Landroid/content/Context;)V

    .line 196
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .line 199
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/NSDepend;->verifyFileSystemAccess(I)V

    .line 201
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 202
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountManager()Landroid/accounts/AccountManager;

    .line 203
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    .line 206
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->upgrade()Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->runUpgradeFlow()V

    .line 208
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->tracker()Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;

    .line 209
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 211
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    .line 212
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsClient()Lcom/google/apps/dots/android/newsstand/http/NSClient;

    .line 213
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    .line 214
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .line 215
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->colorHelper()Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

    .line 216
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->readingActivityTracker()Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    .line 217
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bitmapPool()Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    .line 218
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bytePool()Lcom/google/apps/dots/android/newsstand/util/BytePool;

    .line 219
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->itemJsonSerializer()Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    .line 220
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    .line 221
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->httpContentService()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;

    .line 222
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->setupSystemSettingListeners()V

    .line 223
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->playCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;

    .line 226
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    .line 227
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->globalManifest()Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    .line 228
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->storeCache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    .line 229
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 230
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .line 231
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    .line 232
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->sectionStore()Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    .line 233
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->formStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    .line 234
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

    .line 235
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->formTemplateStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

    .line 236
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appSummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    .line 237
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appFamilySummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    .line 239
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    .line 240
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->recentlyReadHelper()Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    .line 241
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewUriWhitelist()Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    .line 242
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->cookiePolicy()Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    .line 243
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->defaultCookieManager()Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;

    .line 244
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webviewHttpClientPool()Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;

    .line 247
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->initClassLoaders()V

    .line 250
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_1

    .line 251
    sget v2, Lcom/google/android/apps/newsstanddev/R$bool;->enable_webview_debugging:I

    .line 252
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getBooleanResource(I)Z

    move-result v2

    .line 251
    invoke-static {v2}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V

    .line 254
    :cond_1
    return-void

    .line 170
    .end local v0    # "enableCustomCrashReporting":Z
    :catch_0
    move-exception v1

    .line 173
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/NSDepend;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Exception while trying to retrieve \'enable_custom_crash_report\' boolean: %s"

    new-array v4, v7, [Ljava/lang/Object;

    .line 174
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 173
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    const/4 v0, 0x0

    .restart local v0    # "enableCustomCrashReporting":Z
    goto/16 :goto_0
.end method

.method protected setupInternalFeedback()V
    .locals 0

    .prologue
    .line 158
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->setupContentAuthority()V

    .line 159
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .line 160
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 161
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 162
    return-void
.end method

.method protected verifyFileSystemAccess(I)V
    .locals 4
    .param p1, "tryCount"    # I

    .prologue
    .line 1119
    :goto_0
    if-lez p1, :cond_0

    .line 1120
    add-int/lit8 p1, p1, -0x1

    .line 1121
    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 1122
    .local v0, "file":Ljava/io/File;
    if-nez v0, :cond_1

    .line 1126
    const-wide/16 v2, 0x1

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1127
    :catch_0
    move-exception v1

    goto :goto_0

    .line 1135
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    const/4 v1, 0x0

    const-string v2, "Failed to get access to the file system"

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 1136
    :cond_1
    return-void
.end method
