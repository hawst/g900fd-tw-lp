.class public Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;
.super Ljava/lang/Object;
.source "OverlayViewHelper.java"


# static fields
.field private static final bugFixingDrawable:Landroid/graphics/drawable/Drawable;


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final interceptTouches:Z

.field private overlayViewContainer:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->bugFixingDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;-><init>(Landroid/app/Activity;Z)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "interceptTouches"    # Z

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->activity:Landroid/app/Activity;

    .line 65
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->interceptTouches:Z

    .line 66
    return-void
.end method

.method private initializeOverlayViewContainer()V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v1, -0x1

    const/4 v6, 0x0

    .line 81
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    if-nez v2, :cond_1

    .line 82
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->interceptTouches:Z

    if-eqz v2, :cond_2

    move v2, v6

    :goto_0
    or-int/lit8 v4, v2, 0x8

    .line 84
    .local v4, "flags":I
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x3e8

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 87
    .local v0, "containerParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v1, 0x30

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 88
    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 89
    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 90
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->activity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    .line 91
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 94
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setLayoutDirection(I)V

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setClipChildren(Z)V

    .line 100
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->bugFixingDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 101
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 103
    .end local v0    # "containerParams":Landroid/view/WindowManager$LayoutParams;
    .end local v4    # "flags":I
    :cond_1
    return-void

    .line 82
    :cond_2
    const/16 v2, 0x10

    goto :goto_0
.end method

.method private removeWindowOverlayViews()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 125
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    .line 128
    :cond_0
    return-void
.end method


# virtual methods
.method public addWindowOverlayView(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "layoutParams"    # Landroid/widget/RelativeLayout$LayoutParams;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->initializeOverlayViewContainer()V

    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1, p2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 74
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getLocaleLayoutDirection()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutDirection(I)V

    .line 76
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->removeWindowOverlayViews()V

    .line 120
    return-void
.end method

.method public removeWindowOverlayView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 108
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->overlayViewContainer:Landroid/widget/RelativeLayout;

    .line 113
    :cond_0
    return-void
.end method
