.class public Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter$HeaderAwareLoadingFragment;
.super Lcom/google/android/libraries/bind/fragment/LoadingFragment;
.source "NSFragmentDataStatePagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HeaderAwareLoadingFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/google/android/libraries/bind/fragment/LoadingFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 130
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter$HeaderAwareLoadingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "headerHeightPx"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 131
    .local v0, "headerHeightPx":I
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/fragment/LoadingFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 132
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1, v4, v0, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 133
    return-object v1
.end method
