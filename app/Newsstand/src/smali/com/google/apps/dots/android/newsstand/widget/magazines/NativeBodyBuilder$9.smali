.class Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$9;
.super Ljava/lang/Object;
.source "NativeBodyBuilder.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildMediaPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

.field final synthetic val$fullScreenIntent:Landroid/content/Intent;

.field final synthetic val$partId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    .prologue
    .line 758
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$9;->val$partId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$9;->val$fullScreenIntent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Landroid/net/Uri;)V
    .locals 5
    .param p1, "event"    # Landroid/net/Uri;

    .prologue
    .line 764
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    sget-object v1, Lcom/google/apps/dots/shared/EventCode;->MEDIA_ON_START:Lcom/google/apps/dots/shared/EventCode;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$9;->val$partId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->dispatch(Ljava/lang/String;)V

    .line 765
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$9;->val$fullScreenIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->startActivity(Landroid/content/Intent;)V

    .line 766
    return-void
.end method
