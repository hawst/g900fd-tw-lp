.class public Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
.source "VideosAppIntentBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder",
        "<",
        "Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;",
        ">;"
    }
.end annotation


# static fields
.field private static final BASE_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "http://play.google.com/movies"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->BASE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected buildViewCollectionIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 36
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.google.android.videos"

    .line 37
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 38
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 39
    const-string v1, "authAccount"

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->addAccountExtra(Landroid/content/Intent;Ljava/lang/String;)V

    .line 40
    return-object v0
.end method

.method protected buildViewItemIntent()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 46
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder$1;->$SwitchMap$com$google$apps$dots$android$newsstand$util$DocType:[I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/DocType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 73
    const/4 v0, 0x0

    .line 79
    :goto_0
    return-object v0

    .line 48
    :pswitch_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "movies"

    .line 49
    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "v"

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->backendDocId:Ljava/lang/String;

    .line 50
    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 51
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 76
    .local v1, "uri":Landroid/net/Uri;
    :goto_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.videos.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 77
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.google.android.videos"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const-string v2, "authAccount"

    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->addAccountExtra(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0

    .line 54
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "uri":Landroid/net/Uri;
    :pswitch_1
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "shows"

    .line 55
    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "sh"

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->backendDocId:Ljava/lang/String;

    .line 56
    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 57
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 58
    .restart local v1    # "uri":Landroid/net/Uri;
    goto :goto_1

    .line 60
    .end local v1    # "uri":Landroid/net/Uri;
    :pswitch_2
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "shows"

    .line 61
    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "se"

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->backendDocId:Ljava/lang/String;

    .line 62
    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 63
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 64
    .restart local v1    # "uri":Landroid/net/Uri;
    goto :goto_1

    .line 66
    .end local v1    # "uri":Landroid/net/Uri;
    :pswitch_3
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "shows"

    .line 67
    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "se"

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->parentBackendDocId:Ljava/lang/String;

    .line 68
    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "v"

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;->backendDocId:Ljava/lang/String;

    .line 69
    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 70
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 71
    .restart local v1    # "uri":Landroid/net/Uri;
    goto :goto_1

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
