.class final Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$2;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardWarmWelcome.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->makeHidingOnClickListener(Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private clicked:Z

.field final synthetic val$optAfter:Ljava/lang/Runnable;

.field final synthetic val$optBefore:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$2;->val$optBefore:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$2;->val$optAfter:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 90
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$2;->clicked:Z

    if-nez v1, :cond_1

    .line 91
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$2;->clicked:Z

    .line 92
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$2;->val$optBefore:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$2;->val$optBefore:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 95
    :cond_0
    const-class v1, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;

    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->getTypedAncestor(Ljava/lang/Class;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;

    .line 96
    .local v0, "card":Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome$2;->val$optAfter:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->fadeOutCard(Ljava/lang/Runnable;)V

    .line 98
    .end local v0    # "card":Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;
    :cond_1
    return-void
.end method
