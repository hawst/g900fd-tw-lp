.class Lcom/google/apps/dots/android/newsstand/card/NSCardListView$2;
.super Ljava/lang/Object;
.source "NSCardListView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/NSCardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$2;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$2;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    # getter for: Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->audioFragment:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->access$200(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$2;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    # getter for: Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->audioFragment:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->access$200(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->spyOnTouchEvent(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 82
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$2;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$2;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    if-eqz v0, :cond_0

    .line 79
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$2;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$2;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->audio_fragment:I

    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->audioFragment:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->access$202(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    goto :goto_0
.end method
