.class final Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$3;
.super Ljava/lang/Object;
.source "AsyncUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->warnOnError(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/logging/Logd;[Ljava/lang/Class;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$future:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$ignoreTypes:[Ljava/lang/Class;

.field final synthetic val$logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;[Ljava/lang/Class;Lcom/google/apps/dots/android/newsstand/logging/Logd;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$3;->val$future:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$3;->val$ignoreTypes:[Ljava/lang/Class;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$3;->val$logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 242
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$3;->val$future:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v3}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 244
    :try_start_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$3;->val$future:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v3}, Lcom/google/common/util/concurrent/Uninterruptibles;->getUninterruptibly(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 245
    :catch_0
    move-exception v1

    .line 246
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 247
    .local v0, "cause":Ljava/lang/Throwable;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$3;->val$ignoreTypes:[Ljava/lang/Class;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_1

    aget-object v2, v4, v3

    .line 248
    .local v2, "exceptionType":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Throwable;>;"
    invoke-virtual {v2, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 247
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 252
    .end local v2    # "exceptionType":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Throwable;>;"
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$3;->val$logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 253
    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v1    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_1
    move-exception v3

    goto :goto_0
.end method
