.class Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardListVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShelfIconClickListener"
.end annotation


# instance fields
.field private final analyticsScreenName:Ljava/lang/String;

.field private final appId:Ljava/lang/String;

.field private final isMagazineShelf:Z

.field private sectionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "isMagazineShelf"    # Z
    .param p2, "appId"    # Ljava/lang/String;
    .param p3, "analyticsScreenName"    # Ljava/lang/String;

    .prologue
    .line 357
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    .line 358
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->isMagazineShelf:Z

    .line 359
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->appId:Ljava/lang/String;

    .line 360
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->analyticsScreenName:Ljava/lang/String;

    .line 361
    return-void
.end method


# virtual methods
.method public getSectionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->sectionId:Ljava/lang/String;

    return-object v0
.end method

.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 375
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->isMagazineShelf:Z

    if-eqz v3, :cond_2

    .line 376
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->appId:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->magazineEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v0

    .line 385
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->isMagazineShelf:Z

    if-eqz v3, :cond_3

    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineClickEvent;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->analyticsScreenName:Ljava/lang/String;

    invoke-direct {v1, v3, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineClickEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 388
    .local v1, "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
    :goto_1
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->track(Z)V

    .line 390
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-direct {v3, p2}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 391
    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v2

    .line 393
    .local v2, "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_1

    .line 394
    const-class v3, Lcom/google/apps/dots/android/newsstand/card/CardIcon;

    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->icon:I

    .line 395
    invoke-static {p1, v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewInParent(Landroid/view/View;Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v3

    .line 394
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    .line 397
    :cond_1
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    .line 398
    return-void

    .line 378
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v1    # "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
    .end local v2    # "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->appId:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v0

    .line 379
    .restart local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->getSectionId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 380
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->getSectionId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->sectionEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    move-result-object v0

    goto :goto_0

    .line 385
    :cond_3
    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->analyticsScreenName:Ljava/lang/String;

    invoke-direct {v1, v3, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    goto :goto_1
.end method

.method public setSectionId(Ljava/lang/String;)V
    .locals 0
    .param p1, "sectionId"    # Ljava/lang/String;

    .prologue
    .line 364
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->sectionId:Ljava/lang/String;

    .line 365
    return-void
.end method
