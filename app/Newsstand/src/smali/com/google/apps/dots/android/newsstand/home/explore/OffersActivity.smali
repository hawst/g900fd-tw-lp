.class public Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "OffersActivity.java"


# instance fields
.field private offersFragment:Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>()V

    return-void
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 71
    if-nez p1, :cond_0

    .line 75
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->offersFragment:Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->handleExtras(Landroid/os/Bundle;)Z

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 37
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->offersFragment:Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->offersFragment:Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->offersFragment:Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->handleOnBackPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->offers_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->setContentView(I)V

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->offers_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->offersFragment:Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;

    .line 30
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->offers_title:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->setActionBarTitle(Ljava/lang/CharSequence;)V

    .line 32
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 33
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 44
    .local v0, "menuInflater":Landroid/view/MenuInflater;
    sget v1, Lcom/google/android/apps/newsstanddev/R$menu;->explore_single_topic_activity_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 45
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 67
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 68
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 50
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->onOptionsItemSelectedCloseDrawerIfNeeded(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    :goto_0
    return v0

    .line 52
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_2

    .line 53
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->isTaskRoot()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/HomePage;->EXPLORE_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->start()V

    .line 57
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;->finish()V

    goto :goto_0

    .line 60
    :cond_2
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
