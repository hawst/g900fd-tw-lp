.class final Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$1;
.super Ljava/lang/Object;
.source "LibraryPage.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 78
    .local v1, "typeInt":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;->values()[Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;

    move-result-object v2

    aget-object v0, v2, v1

    .line 79
    .local v0, "type":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$2;->$SwitchMap$com$google$apps$dots$android$newsstand$home$library$LibraryPage$Type:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 87
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    .line 81
    :pswitch_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_NEWS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 85
    :goto_0
    return-object v2

    .line 83
    :pswitch_1
    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_TOPICS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    goto :goto_0

    .line 85
    :pswitch_2
    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 92
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    move-result-object v0

    return-object v0
.end method
