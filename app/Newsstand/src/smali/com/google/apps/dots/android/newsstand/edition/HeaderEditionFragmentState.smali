.class public Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;
.super Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;
.source "HeaderEditionFragmentState.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 2
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "pageEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 24
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 25
    if-eqz p2, :cond_0

    .line 26
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SECTION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 28
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 29
    return-void

    .line 26
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 38
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 39
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    .line 40
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 41
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 43
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 33
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "{HeaderEditionFragmentState: %s/%s}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 60
    return-void
.end method
