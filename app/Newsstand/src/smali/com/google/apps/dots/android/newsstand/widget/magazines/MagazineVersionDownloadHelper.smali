.class public abstract Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper;
.super Ljava/lang/Object;
.source "MagazineVersionDownloadHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/bind/data/DataList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 20
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper;->hasPrintAndTextSections(Lcom/google/android/libraries/bind/data/DataList;)Z

    move-result v0

    return v0
.end method

.method public static handleMagazineKeepOnDevice(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;Z)V
    .locals 5
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "magazineEdition"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;
    .param p2, "isArchived"    # Z

    .prologue
    .line 44
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 45
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->maybeShow(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 85
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    .line 51
    .local v0, "manager":Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->canForegroundSyncEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 57
    const/4 v2, 0x1

    invoke-static {p0, p1, p2, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->show(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/edition/Edition;ZZ)V

    goto :goto_0

    .line 61
    :cond_1
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->sectionList(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/datasource/SectionList;

    move-result-object v1

    .line 62
    .local v1, "sectionList":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    .line 63
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/DataListUtil;->whenDataListFirstRefreshed(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;

    invoke-direct {v4, v1, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;-><init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;Z)V

    .line 62
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private static hasPrintAndTextSections(Lcom/google/android/libraries/bind/data/DataList;)Z
    .locals 6
    .param p0, "sectionList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 23
    const/4 v0, 0x0

    .line 24
    .local v0, "hasPrintSection":Z
    const/4 v1, 0x0

    .line 25
    .local v1, "hasTextSection":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 27
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v4

    sget v5, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_SUMMARY:I

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 28
    .local v3, "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->hasCorrespondingTextSectionId()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 29
    const/4 v0, 0x1

    .line 31
    :cond_0
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->hasCorrespondingImageSectionId()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 32
    const/4 v1, 0x1

    .line 25
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 35
    .end local v3    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_2
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method
