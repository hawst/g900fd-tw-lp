.class public Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;
.super Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;
.source "CardTopicItem.java"


# static fields
.field public static final DK_ARTICLE_ARRAY:I

.field public static final DK_BACKGROUND_COLOR:I

.field public static final DK_ENTITY_IMAGE_ID:I

.field public static final DK_ENTITY_ON_CLICK_LISTENER:I

.field public static final DK_INITIALS:I

.field public static final DK_READ_STATE_ARRAY:I

.field public static final DK_TITLE:I

.field public static final DK_TOPIC_ICON_ID:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I

.field public static final LAYOUTS_COMPACT:[I

.field public static final LAYOUTS_HORIZONTAL:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicItem_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_TITLE:I

    .line 22
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicItem_initials:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_INITIALS:I

    .line 24
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicItem_topicIconId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_TOPIC_ICON_ID:I

    .line 26
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicItem_entityImageId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_ENTITY_IMAGE_ID:I

    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicItem_entityOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_ENTITY_ON_CLICK_LISTENER:I

    .line 30
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicItem_articleArray:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_ARTICLE_ARRAY:I

    .line 32
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicItem_readStateArray:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_READ_STATE_ARRAY:I

    .line 34
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicItem_backgroundColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_BACKGROUND_COLOR:I

    .line 36
    new-array v0, v3, [I

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_READ_STATE_ARRAY:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->EQUALITY_FIELDS:[I

    .line 40
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_item:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->LAYOUTS:[I

    .line 44
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_item_horizontal:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->LAYOUTS_HORIZONTAL:[I

    .line 48
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_item_compact:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->LAYOUTS_COMPACT:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method


# virtual methods
.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 13
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v8, 0x0

    const/4 v12, 0x0

    .line 66
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 67
    sget v7, Lcom/google/android/apps/newsstanddev/R$id;->article_container:I

    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 69
    .local v1, "articleContainer":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    add-int/lit8 v5, v7, -0x1

    .local v5, "i":I
    :goto_0
    if-ltz v5, :cond_0

    .line 70
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 71
    .local v4, "childView":Landroid/view/View;
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 72
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->viewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/google/android/libraries/bind/view/ViewHeap;->recycle(Landroid/view/View;)V

    .line 69
    add-int/lit8 v5, v5, -0x1

    goto :goto_0

    .line 75
    .end local v4    # "childView":Landroid/view/View;
    :cond_0
    if-nez p1, :cond_1

    move-object v3, v8

    .line 76
    .local v3, "articles":[Lcom/google/android/libraries/bind/data/Data;
    :goto_1
    if-eqz v3, :cond_2

    .line 78
    array-length v7, v3

    add-int/lit8 v5, v7, -0x1

    :goto_2
    if-ltz v5, :cond_2

    .line 79
    aget-object v0, v3, v5

    .line 80
    .local v0, "article":Lcom/google/android/libraries/bind/data/Data;
    sget v7, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 81
    .local v6, "viewResId":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->viewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;

    move-result-object v7

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v9, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v6, v8, v9}, Lcom/google/android/libraries/bind/view/ViewHeap;->get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;

    .line 84
    .local v2, "articleView":Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;
    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    .line 86
    invoke-virtual {v2, v12}, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->setOwnedByParent(Z)V

    .line 87
    invoke-virtual {v1, v2, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 78
    add-int/lit8 v5, v5, -0x1

    goto :goto_2

    .line 75
    .end local v0    # "article":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "articleView":Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;
    .end local v3    # "articles":[Lcom/google/android/libraries/bind/data/Data;
    .end local v6    # "viewResId":I
    :cond_1
    sget v7, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_ARTICLE_ARRAY:I

    invoke-virtual {p1, v7}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/google/android/libraries/bind/data/Data;

    check-cast v7, [Lcom/google/android/libraries/bind/data/Data;

    move-object v3, v7

    goto :goto_1

    .line 90
    .restart local v3    # "articles":[Lcom/google/android/libraries/bind/data/Data;
    :cond_2
    return-void
.end method
