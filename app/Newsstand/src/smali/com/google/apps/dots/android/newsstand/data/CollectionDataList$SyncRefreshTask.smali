.class public Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;
.super Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;
.source "CollectionDataList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "SyncRefreshTask"
.end annotation


# instance fields
.field final getFresh:Z

.field final refreshApiUri:Ljava/lang/String;

.field final showStaleOnTimeout:Z

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

.field final timeoutMs:J


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)V
    .locals 6
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 170
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    .line 171
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NETWORK_API:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;-><init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V

    .line 165
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    # invokes: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getApiUri()Ljava/lang/String;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$100(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->refreshApiUri:Ljava/lang/String;

    .line 166
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    # getter for: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getFreshOnNextRequest:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$200(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->getFresh:Z

    .line 167
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    # getter for: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getFreshTimeoutMs:J
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$300(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->timeoutMs:J

    .line 168
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    # getter for: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->showStaleOnGetFreshTimeout:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$400(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->showStaleOnTimeout:Z

    .line 173
    const-string v0, ""

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->refreshApiUri:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 174
    # invokes: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$500(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v3, "Creating refresh task for collectionUri: %s, getFresh: %b"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->refreshApiUri:Ljava/lang/String;

    aput-object v5, v4, v2

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->getFresh:Z

    .line 175
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v4, v1

    .line 174
    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    return-void

    :cond_0
    move v0, v2

    .line 173
    goto :goto_0
.end method


# virtual methods
.method protected getFreshData()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/libraries/bind/data/DataException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 180
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->refreshApiUri:Ljava/lang/String;

    if-nez v8, :cond_0

    .line 181
    new-instance v8, Lcom/google/android/libraries/bind/data/DataException;

    const-string v9, "No collection URI"

    invoke-direct {v8, v9}, Lcom/google/android/libraries/bind/data/DataException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 183
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->currentRestorePoint()I

    move-result v7

    .line 186
    .local v7, "traceRestorePoint":I
    :try_start_0
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    # getter for: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionLock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$600(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Ljava/util/concurrent/locks/Lock;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 188
    :try_start_1
    const-string v8, "CDL-load"

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->refreshApiUri:Ljava/lang/String;

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v8, v10, v11}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 189
    new-instance v8, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->refreshApiUri:Ljava/lang/String;

    sget-object v11, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v8, v10, v11}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 190
    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->freshVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v8

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    invoke-virtual {v10}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->shouldStorePermanently()Z

    move-result v10

    invoke-virtual {v8, v10}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->makePermanent(Z)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v2

    .line 191
    .local v2, "freshRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    new-instance v8, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->refreshApiUri:Ljava/lang/String;

    sget-object v11, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v8, v10, v11}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 192
    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->anyVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v8

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    invoke-virtual {v10}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->shouldStorePermanently()Z

    move-result v10

    invoke-virtual {v8, v10}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->makePermanent(Z)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    .line 193
    .local v0, "anyRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->getFresh:Z

    if-eqz v8, :cond_2

    move-object v6, v2

    .line 195
    .local v6, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v8

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v8, v10, v6}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 197
    .local v4, "mutationResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->getFresh:Z

    if-eqz v8, :cond_1

    .line 198
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    # getter for: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getFreshOnNextRequest:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$200(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v8

    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 201
    :try_start_2
    iget-wide v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->timeoutMs:J

    const-wide/16 v12, 0x0

    cmp-long v8, v10, v12

    if-lez v8, :cond_3

    iget-wide v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->timeoutMs:J

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 202
    invoke-interface {v4, v10, v11, v8}, Lcom/google/common/util/concurrent/ListenableFuture;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    move-object v3, v8

    .line 204
    .local v3, "mutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    :goto_1
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 216
    .end local v3    # "mutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    :cond_1
    :goto_2
    :try_start_3
    invoke-interface {v4}, Lcom/google/common/util/concurrent/ListenableFuture;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .line 217
    .restart local v3    # "mutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 222
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->isCancelled()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v8

    if-eqz v8, :cond_5

    .line 230
    :try_start_4
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    # getter for: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionLock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$600(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Ljava/util/concurrent/locks/Lock;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 248
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    move-object v5, v9

    .end local v0    # "anyRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .end local v2    # "freshRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .end local v3    # "mutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    .end local v4    # "mutationResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    .end local v6    # "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    :goto_3
    return-object v5

    .restart local v0    # "anyRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .restart local v2    # "freshRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    :cond_2
    move-object v6, v0

    .line 193
    goto :goto_0

    .line 203
    .restart local v4    # "mutationResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    .restart local v6    # "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    :cond_3
    :try_start_5
    invoke-interface {v4}, Lcom/google/common/util/concurrent/ListenableFuture;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    :try_end_5
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v3, v8

    goto :goto_1

    .line 205
    :catch_0
    move-exception v1

    .line 207
    .local v1, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_6
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    # getter for: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->showStaleOnGetFreshTimeout:Z
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$400(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 209
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v8

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v8, v10, v0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    goto :goto_2

    .line 212
    :cond_4
    new-instance v8, Lcom/google/android/libraries/bind/data/DataException;

    invoke-direct {v8, v1}, Lcom/google/android/libraries/bind/data/DataException;-><init>(Ljava/lang/Throwable;)V

    throw v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 230
    .end local v0    # "anyRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .end local v1    # "e":Ljava/util/concurrent/TimeoutException;
    .end local v2    # "freshRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .end local v4    # "mutationResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    .end local v6    # "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    :catchall_0
    move-exception v8

    :try_start_7
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    # getter for: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionLock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$600(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Ljava/util/concurrent/locks/Lock;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v8
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 239
    :catch_1
    move-exception v1

    .line 240
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_8
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->interrupt()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 248
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    move-object v5, v9

    goto :goto_3

    .line 227
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "anyRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .restart local v2    # "freshRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .restart local v3    # "mutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    .restart local v4    # "mutationResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    .restart local v6    # "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    :cond_5
    :try_start_9
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    iget-object v10, v3, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    # setter for: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersion:Lcom/google/apps/dots/android/newsstand/store/Version;
    invoke-static {v8, v10}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$702(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;Lcom/google/apps/dots/android/newsstand/store/Version;)Lcom/google/apps/dots/android/newsstand/store/Version;

    .line 228
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->refreshApiUri:Ljava/lang/String;

    # setter for: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionUri:Ljava/lang/String;
    invoke-static {v8, v10}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$802(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;Ljava/lang/String;)Ljava/lang/String;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 230
    :try_start_a
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    # getter for: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionLock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$600(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Ljava/util/concurrent/locks/Lock;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 233
    const-string v8, "CDL processing %s"

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->refreshApiUri:Ljava/lang/String;

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v8, v10, v11}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 234
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v11, v3, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-virtual {v8, p0, v10, v11}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;

    move-result-object v5

    .line 235
    .local v5, "response":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 248
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    goto :goto_3

    .line 242
    .end local v0    # "anyRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .end local v2    # "freshRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .end local v3    # "mutationResponse":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    .end local v4    # "mutationResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/MutationResponse;>;"
    .end local v5    # "response":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .end local v6    # "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    :catch_2
    move-exception v1

    .line 243
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_b
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    # invokes: Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->access$900(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v8

    const-string v9, "Error requesting data: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v1, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 244
    new-instance v8, Lcom/google/android/libraries/bind/data/DataException;

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/libraries/bind/data/DataException;-><init>(Ljava/lang/Throwable;)V

    throw v8
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 248
    .end local v1    # "e":Ljava/util/concurrent/ExecutionException;
    :catchall_1
    move-exception v8

    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    throw v8

    .line 245
    :catch_3
    move-exception v1

    .line 248
    .local v1, "e":Ljava/util/concurrent/CancellationException;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    move-object v5, v9

    goto/16 :goto_3
.end method
