.class public abstract Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;
.super Ljava/lang/Object;
.source "PageViewOnScrollListener.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# static fields
.field private static final MIN_NUMBER_OF_ROWS_PER_PAGE:I = 0x2


# instance fields
.field private lastPageIndexSent:I

.field private lastSentPageFirstViewIndex:I

.field private numViewsPerPageEstimate:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private maybeSendAnalyticsEvent(I)V
    .locals 3
    .param p1, "firstVisible"    # I

    .prologue
    .line 50
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->lastSentPageFirstViewIndex:I

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->numViewsPerPageEstimate:I

    sub-int/2addr v1, v2

    if-gt p1, v1, :cond_1

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->lastPageIndexSent:I

    if-lez v1, :cond_1

    .line 53
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->lastPageIndexSent:I

    add-int/lit8 v0, v1, -0x1

    .line 54
    .local v0, "pageToSend":I
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->lastSentPageFirstViewIndex:I

    .line 62
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->onPageView(I)V

    .line 63
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->lastPageIndexSent:I

    .line 64
    .end local v0    # "pageToSend":I
    :cond_0
    return-void

    .line 55
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->lastSentPageFirstViewIndex:I

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->numViewsPerPageEstimate:I

    add-int/2addr v1, v2

    if-lt p1, v1, :cond_0

    .line 57
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->lastPageIndexSent:I

    add-int/lit8 v0, v1, 0x1

    .line 58
    .restart local v0    # "pageToSend":I
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->lastSentPageFirstViewIndex:I

    goto :goto_0
.end method

.method private updateViewsPerPageEstimate(II)V
    .locals 3
    .param p1, "firstVisible"    # I
    .param p2, "lastVisible"    # I

    .prologue
    .line 38
    sub-int v1, p2, p1

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x2

    .line 39
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 40
    .local v0, "tempNumViewsPerPageEstimate":I
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->numViewsPerPageEstimate:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->numViewsPerPageEstimate:I

    .line 41
    return-void
.end method


# virtual methods
.method public abstract onPageView(I)V
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 3
    .param p1, "listView"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 24
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    .line 25
    .local v0, "firstVisible":I
    add-int v2, v0, p3

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2, p4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 27
    .local v1, "lastVisible":I
    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->updateViewsPerPageEstimate(II)V

    .line 28
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->maybeSendAnalyticsEvent(I)V

    .line 29
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 19
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->numViewsPerPageEstimate:I

    .line 33
    return-void
.end method
