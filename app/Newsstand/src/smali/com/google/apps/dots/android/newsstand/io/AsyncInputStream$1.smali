.class Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream$1;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "AsyncInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/io/InputStream;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream$1;->this$0:Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "result"    # Ljava/io/InputStream;

    .prologue
    .line 27
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream$1;->this$0:Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;

    monitor-enter v1

    .line 28
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream$1;->this$0:Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;

    # getter for: Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->closed:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;->access$000(Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    monitor-exit v1

    .line 37
    :goto_0
    return-void

    .line 31
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 34
    :catch_0
    move-exception v0

    goto :goto_0

    .line 31
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Ljava/io/InputStream;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream$1;->onSuccess(Ljava/io/InputStream;)V

    return-void
.end method
