.class public Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "TransitionBackgroundDrawable.java"


# instance fields
.field protected alphaMultiplier:F

.field protected final context:Landroid/content/Context;

.field protected currentDrawable:Landroid/graphics/drawable/Drawable;

.field protected final endDrawable:Landroid/graphics/drawable/Drawable;

.field protected ratio:F

.field protected final startDrawable:Landroid/graphics/drawable/Drawable;

.field protected transitionFraction:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "startDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p3, "endDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 25
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->transitionFraction:F

    .line 21
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->alphaMultiplier:F

    .line 22
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->ratio:F

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->context:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->startDrawable:Landroid/graphics/drawable/Drawable;

    .line 28
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->endDrawable:Landroid/graphics/drawable/Drawable;

    .line 29
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->currentDrawable:Landroid/graphics/drawable/Drawable;

    .line 30
    return-void
.end method

.method private updateBounds()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->startDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 58
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->endDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->endDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->invalidateSelf()V

    .line 62
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 48
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->endDrawable:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->alphaMultiplier:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 49
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->endDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 50
    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->ratio:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->transitionFraction:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->alphaMultiplier:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/16 v2, 0xff

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 52
    .local v0, "alpha":I
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->startDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 53
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->startDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 54
    return-void
.end method

.method public getAlpha()I
    .locals 2

    .prologue
    .line 80
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->alphaMultiplier:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 66
    const/4 v0, -0x3

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 91
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->updateBounds()V

    .line 92
    return-void
.end method

.method public setAlpha(I)V
    .locals 2
    .param p1, "alpha"    # I

    .prologue
    .line 71
    div-int/lit16 v1, p1, 0xff

    int-to-float v0, v1

    .line 72
    .local v0, "newAlphaMultiplier":F
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->alphaMultiplier:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 73
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->alphaMultiplier:F

    .line 74
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->invalidateSelf()V

    .line 76
    :cond_0
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 85
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setTransitionFraction(F)V
    .locals 0
    .param p1, "transitionFraction"    # F

    .prologue
    .line 42
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->transitionFraction:F

    .line 43
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->invalidateSelf()V

    .line 44
    return-void
.end method

.method public setTransitionRatio(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 102
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/transition/TransitionBackgroundDrawable;->ratio:F

    .line 103
    return-void
.end method
