.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;
.super Ljava/lang/Object;
.source "JavascriptInterfaceInjector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Bridge"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;


# direct methods
.method private constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
    .param p2, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$1;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)V

    return-void
.end method


# virtual methods
.method public onAppeared()V
    .locals 0
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 97
    return-void
.end method

.method public onScriptInjected()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    const/4 v1, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->scriptReady:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$602(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;Z)Z

    .line 86
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->scriptInjectedRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$700(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 87
    return-void
.end method

.method public onTouchEventDefaultPrevented(Ljava/lang/String;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->touchEventDefaultPreventedRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 92
    return-void
.end method

.method public onUnhandledClick()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->unhandledClickRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$400(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 81
    return-void
.end method

.method public updateSnapPoints(Ljava/lang/String;)V
    .locals 2
    .param p1, "stringSnapPoints"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 117
    return-void
.end method
