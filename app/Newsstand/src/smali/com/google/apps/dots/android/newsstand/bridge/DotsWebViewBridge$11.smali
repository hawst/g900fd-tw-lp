.class Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;
.super Ljava/lang/Object;
.source "DotsWebViewBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->sendCustomAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

.field final synthetic val$action:Ljava/lang/String;

.field final synthetic val$category:Ljava/lang/String;

.field final synthetic val$dimensionsMetricsJson:Ljava/lang/String;

.field final synthetic val$label:Ljava/lang/String;

.field final synthetic val$pageIndex:I

.field final synthetic val$postId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    .prologue
    .line 383
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$dimensionsMetricsJson:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$postId:Ljava/lang/String;

    iput p4, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$pageIndex:I

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$category:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$action:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$label:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 16

    .prologue
    .line 386
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v9

    .line 387
    .local v9, "dimensions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v10

    .line 388
    .local v10, "metrics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$dimensionsMetricsJson:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 390
    :try_start_0
    new-instance v11, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$dimensionsMetricsJson:Ljava/lang/String;

    invoke-direct {v11, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 391
    .local v11, "dimensionsMetrics":Lorg/json/JSONObject;
    invoke-virtual {v11}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 392
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 393
    .local v13, "key":Ljava/lang/String;
    invoke-virtual {v11, v13}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    .line 394
    .local v15, "value":Ljava/lang/Object;
    instance-of v1, v15, Ljava/lang/Number;

    if-eqz v1, :cond_2

    .line 395
    move-object v0, v15

    check-cast v0, Ljava/lang/Number;

    move-object v1, v0

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v10, v13, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    # getter for: Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "\tadded custom metric %s = %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v13, v3, v4

    const/4 v4, 0x1

    aput-object v15, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 402
    .end local v11    # "dimensionsMetrics":Lorg/json/JSONObject;
    .end local v13    # "key":Ljava/lang/String;
    .end local v14    # "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v15    # "value":Ljava/lang/Object;
    :catch_0
    move-exception v12

    .line 403
    .local v12, "e":Lorg/json/JSONException;
    # getter for: Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Couldn\'t parse additional dimensions and metrics from JSON: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$dimensionsMetricsJson:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 407
    .end local v12    # "e":Lorg/json/JSONException;
    :cond_1
    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$postId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$pageIndex:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$category:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$action:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$11;->val$label:Ljava/lang/String;

    invoke-direct/range {v1 .. v10}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    const/4 v2, 0x1

    .line 408
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/GenericBridgeEvent;->track(Z)V

    .line 409
    return-void

    .line 397
    .restart local v11    # "dimensionsMetrics":Lorg/json/JSONObject;
    .restart local v13    # "key":Ljava/lang/String;
    .restart local v14    # "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v15    # "value":Ljava/lang/Object;
    :cond_2
    :try_start_1
    instance-of v1, v15, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 398
    move-object v0, v15

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    invoke-interface {v9, v13, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    # getter for: Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "\tadded custom dimension %s = %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v13, v3, v4

    const/4 v4, 0x1

    aput-object v15, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
