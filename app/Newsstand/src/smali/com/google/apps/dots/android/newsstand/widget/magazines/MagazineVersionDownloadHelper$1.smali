.class final Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "MagazineVersionDownloadHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper;->handleMagazineKeepOnDevice(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field final synthetic val$isArchived:Z

.field final synthetic val$magazineEdition:Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

.field final synthetic val$sectionList:Lcom/google/android/libraries/bind/data/DataList;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;Z)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$sectionList:Lcom/google/android/libraries/bind/data/DataList;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$magazineEdition:Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$isArchived:Z

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 67
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$sectionList:Lcom/google/android/libraries/bind/data/DataList;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper;->hasPrintAndTextSections(Lcom/google/android/libraries/bind/data/DataList;)Z
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper;->access$000(Lcom/google/android/libraries/bind/data/DataList;)Z

    move-result v0

    .line 68
    .local v0, "hasPrintAndText":Z
    if-eqz v0, :cond_1

    .line 70
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$magazineEdition:Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$isArchived:Z

    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->show(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$magazineEdition:Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    .line 74
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->pinEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v1

    const/4 v2, 0x1

    .line 75
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v1

    .line 76
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    .line 77
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$isArchived:Z

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadHelper$1;->val$magazineEdition:Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    .line 79
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    .line 78
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->removeEditionFromArchive(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    goto :goto_0
.end method
