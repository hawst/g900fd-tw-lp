.class Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;
.super Ljava/lang/Object;
.source "BaseArticleWidgetBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->openDrawer(Ljava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;

.field final synthetic val$mediaItemToShow:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

.field final synthetic val$restrictToSingleField:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;Lcom/google/apps/dots/android/newsstand/media/MediaItem;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;->val$mediaItemToShow:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;->val$restrictToSingleField:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 113
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;->val$mediaItemToShow:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 114
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->setMediaItem(Lcom/google/apps/dots/android/newsstand/media/MediaItem;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v1, v1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    .line 115
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;->val$restrictToSingleField:Z

    .line 116
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->restrictToSingleField(Z)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 117
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge$1;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    .line 118
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->setOwningEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;

    move-result-object v0

    .line 119
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/MediaItemIntentBuilder;->startIfAvailable()V

    .line 120
    return-void
.end method
