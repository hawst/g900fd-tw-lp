.class Lcom/google/apps/dots/android/newsstand/preference/Preferences$3;
.super Ljava/lang/Object;
.source "Preferences.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$actualKeys:Ljava/util/Set;

.field final synthetic val$listener:Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/Preferences;Ljava/util/Set;Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 588
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$3;->this$0:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$3;->val$actualKeys:Ljava/util/Set;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$3;->val$listener:Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$3;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$3;->val$actualKeys:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$3;->val$listener:Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$3;->this$0:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$3;->val$account:Landroid/accounts/Account;

    invoke-virtual {v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getOriginalKey(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;->onPreferenceChanged(Ljava/lang/String;)V

    .line 594
    :cond_0
    return-void
.end method
