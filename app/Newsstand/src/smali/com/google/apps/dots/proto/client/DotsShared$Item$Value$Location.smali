.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Location"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;


# instance fields
.field private address_:Ljava/lang/String;

.field private bitField0_:I

.field private latitude_:D

.field private longitude_:D

.field private radius_:D

.field private thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private unit_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12188
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 12189
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 12199
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->latitude_:D

    .line 12218
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->longitude_:D

    .line 12237
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->address_:Ljava/lang/String;

    .line 12259
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->radius_:D

    .line 12278
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->unit_:I

    .line 12189
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;
    .locals 3

    .prologue
    .line 12331
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 12335
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_0

    .line 12336
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 12338
    :cond_0
    return-object v0

    .line 12332
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;
    :catch_0
    move-exception v1

    .line 12333
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 12185
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 12343
    if-ne p1, p0, :cond_1

    .line 12351
    :cond_0
    :goto_0
    return v1

    .line 12344
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 12345
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .line 12346
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->latitude_:D

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->latitude_:D

    cmpl-double v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->longitude_:D

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->longitude_:D

    cmpl-double v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->address_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->address_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 12348
    :goto_1
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->radius_:D

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->radius_:D

    cmpl-double v3, v4, v6

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->unit_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->unit_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 12351
    goto :goto_0

    .line 12346
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->address_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->address_:Ljava/lang/String;

    .line 12348
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 12351
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12239
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->address_:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 12201
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->latitude_:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 12220
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->longitude_:D

    return-wide v0
.end method

.method public getRadius()D
    .locals 2

    .prologue
    .line 12261
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->radius_:D

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 12390
    const/4 v0, 0x0

    .line 12391
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 12392
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->latitude_:D

    .line 12393
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 12395
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 12396
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->longitude_:D

    .line 12397
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 12399
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 12400
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->address_:Ljava/lang/String;

    .line 12401
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12403
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 12404
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->radius_:D

    .line 12405
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 12407
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 12408
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->unit_:I

    .line 12409
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12411
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v1, :cond_5

    .line 12412
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 12413
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12415
    :cond_5
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->cachedSize:I

    .line 12416
    return v0
.end method

.method public getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 12299
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getUnit()I
    .locals 1

    .prologue
    .line 12280
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->unit_:I

    return v0
.end method

.method public hasThumbnail()Z
    .locals 1

    .prologue
    .line 12309
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/16 v8, 0x20

    .line 12355
    const/16 v0, 0x11

    .line 12356
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 12357
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->latitude_:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->latitude_:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 12358
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->longitude_:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->longitude_:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 12359
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->address_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 12360
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->radius_:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->radius_:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 12361
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->unit_:I

    add-int v0, v1, v3

    .line 12362
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 12363
    return v0

    .line 12359
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->address_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 12362
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 12424
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 12425
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 12429
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 12430
    :sswitch_0
    return-object p0

    .line 12435
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->latitude_:D

    .line 12436
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    goto :goto_0

    .line 12440
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->longitude_:D

    .line 12441
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    goto :goto_0

    .line 12445
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->address_:Ljava/lang/String;

    .line 12446
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    goto :goto_0

    .line 12450
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->radius_:D

    .line 12451
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    goto :goto_0

    .line 12455
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 12456
    .local v1, "temp":I
    if-eq v1, v4, :cond_1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 12458
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->unit_:I

    .line 12459
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    goto :goto_0

    .line 12461
    :cond_2
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->unit_:I

    goto :goto_0

    .line 12466
    .end local v1    # "temp":I
    :sswitch_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_3

    .line 12467
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 12469
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 12425
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x21 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12185
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12368
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 12369
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->latitude_:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 12371
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 12372
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->longitude_:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 12374
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 12375
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->address_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 12377
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 12378
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->radius_:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 12380
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 12381
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->unit_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 12383
    :cond_4
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_5

    .line 12384
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 12386
    :cond_5
    return-void
.end method
