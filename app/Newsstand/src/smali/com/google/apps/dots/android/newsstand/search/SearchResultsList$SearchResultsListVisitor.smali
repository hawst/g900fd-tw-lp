.class public Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;
.super Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
.source "SearchResultsList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "SearchResultsListVisitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V
    .locals 6
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p5, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    .line 96
    sget v2, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    move-object v0, p0

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;-><init>(Landroid/content/Context;ILcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    .line 101
    return-void
.end method


# virtual methods
.method protected getAnalyticsScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    const-string v0, "Search"

    return-object v0
.end method

.method protected readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->type:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->LIVE_POSTS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-ne v0, v1, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->query:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->searchPostsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    move-result-object v0

    return-object v0

    .line 108
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 2
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->type:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->NEWS_EDITIONS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->type:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->FEEDS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->type:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->ENTITIES:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-ne v0, v1, :cond_1

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->currentAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->makeCardSourceListItem(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 117
    :cond_1
    return-void
.end method

.method public visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 2
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;->type:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->MAGAZINES:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 122
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->makeMagazineOfferCard(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 123
    return-void

    .line 121
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 0

    .prologue
    .line 92
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V

    return-void
.end method

.method public bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 0

    .prologue
    .line 92
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$SearchResultsListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    return-void
.end method
