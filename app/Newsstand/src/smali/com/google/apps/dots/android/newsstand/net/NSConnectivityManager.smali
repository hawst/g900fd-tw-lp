.class public Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;
.super Ljava/lang/Object;
.source "NSConnectivityManager.java"


# instance fields
.field private connectivityListeners:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private hasConnectivity:Z

.field private final manager:Landroid/net/ConnectivityManager;

.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {}, Lcom/google/common/collect/Sets;->newLinkedHashSet()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->connectivityListeners:Ljava/util/LinkedHashSet;

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->context:Landroid/content/Context;

    .line 43
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->manager:Landroid/net/ConnectivityManager;

    .line 44
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 45
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->hasConnectivity:Z

    .line 47
    new-instance v0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager$1;-><init>(Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;)V

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->hasConnectivity:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->hasConnectivity:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;)Ljava/util/LinkedHashSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->connectivityListeners:Ljava/util/LinkedHashSet;

    return-object v0
.end method


# virtual methods
.method public addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->connectivityListeners:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 77
    return-object p1
.end method

.method public canForegroundSyncEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)Z
    .locals 4
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "wifiOnlyDownloadOverride"    # Z

    .prologue
    const/4 v1, 0x0

    .line 158
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v1

    .line 161
    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->getCurrentForegroundSyncPolicy(Z)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    move-result-object v0

    .line 162
    .local v0, "policy":Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v2, v3, :cond_2

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->allowsMagazines()Z

    move-result v2

    if-nez v2, :cond_3

    .line 163
    :cond_2
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->allowsNewsAnything()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public cantSync(Z)Z
    .locals 1
    .param p1, "userRequested"    # Z

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->getApplicableSyncPolicy(Z)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->allowsAnything()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cantSyncReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    const-string v0, "not connected"

    .line 187
    :goto_0
    return-object v0

    .line 182
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isMetered()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDownloadViaWifiOnlyPreference()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    const-string v0, "metered connection"

    goto :goto_0

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDownloadWhileChargingOnlyPreference()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isCharging()Z

    move-result v0

    if-nez v0, :cond_2

    .line 185
    const-string v0, "not charging"

    goto :goto_0

    .line 187
    :cond_2
    const-string v0, "unknown reason"

    goto :goto_0
.end method

.method public getApplicableSyncPolicy(Z)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;
    .locals 1
    .param p1, "userRequested"    # Z

    .prologue
    .line 127
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->getApplicableSyncPolicy(ZZ)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    move-result-object v0

    return-object v0
.end method

.method public getApplicableSyncPolicy(ZZ)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;
    .locals 1
    .param p1, "userRequested"    # Z
    .param p2, "wifiOnlyDownloadOverride"    # Z

    .prologue
    .line 140
    if-eqz p1, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->getCurrentForegroundSyncPolicy(Z)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    move-result-object v0

    .line 141
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->getCurrentBackgroundSyncPolicy()Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrentBackgroundSyncPolicy()Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;
    .locals 3

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getImageSyncType()Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnectionRestricted()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->getPolicy(ZZ)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentForegroundSyncPolicy()Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->getCurrentForegroundSyncPolicy(Z)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentForegroundSyncPolicy(Z)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;
    .locals 3
    .param p1, "wifiOnlyDownloadOverride"    # Z

    .prologue
    .line 149
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnectionRestricted(Z)Z

    move-result v0

    .line 150
    .local v0, "restricted":Z
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getImageSyncType()Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->getPolicy(ZZ)Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    move-result-object v1

    return-object v1
.end method

.method public getNetworkInfo()Landroid/net/NetworkInfo;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->manager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    return-object v0
.end method

.method public isCharging()Z
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 105
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->context:Landroid/content/Context;

    const/4 v4, 0x0

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 106
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "plugged"

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 107
    .local v2, "plugged":I
    if-eq v2, v0, :cond_0

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 110
    .local v0, "charging":Z
    :cond_0
    :goto_0
    return v0

    .line 107
    .end local v0    # "charging":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 2

    .prologue
    .line 90
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->manager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 91
    .local v0, "info":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isConnectionRestricted()Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnectionRestricted(Z)Z

    move-result v0

    return v0
.end method

.method public isConnectionRestricted(Z)Z
    .locals 1
    .param p1, "wifiOnlyDownloadOverride"    # Z

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isMetered()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDownloadViaWifiOnlyPreference()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 122
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDownloadWhileChargingOnlyPreference()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isCharging()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMetered()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->manager:Landroid/net/ConnectivityManager;

    invoke-static {v0}, Landroid/support/v4/net/ConnectivityManagerCompat;->isActiveNetworkMetered(Landroid/net/ConnectivityManager;)Z

    move-result v0

    return v0
.end method

.method public removeConnectivityListener(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->connectivityListeners:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    .line 82
    return-void
.end method
