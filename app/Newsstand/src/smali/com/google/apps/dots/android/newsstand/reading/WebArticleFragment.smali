.class public Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "WebArticleFragment.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$WebArticleRenderDelegate;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;",
        ">;",
        "Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

.field private loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

.field private loadingView:Landroid/view/View;

.field private webView:Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const-class v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 34
    const-class v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->setStrictModeLimits(Ljava/lang/Class;I)V

    .line 35
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x0

    const-string v1, "WebArticleFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->web_article_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 29
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 39
    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    return-object v0
.end method

.method private sendAnalyticsEventIfNeeded()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne v0, v1, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->publisher:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 107
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->publisher:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    .line 108
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V

    invoke-virtual {v0, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->track(Z)V

    .line 111
    :cond_0
    return-void
.end method


# virtual methods
.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 115
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 116
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_news_object"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v2, "article_title"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v2, "shareUrl"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    return-object v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->setHidingActionBarArticleEventDelegate(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;)V

    .line 65
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onDestroyView()V

    .line 66
    return-void
.end method

.method public onLoadStateChanged(Landroid/view/View;Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "loadState"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 84
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 85
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne p2, v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->loadingView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 88
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->sendAnalyticsEventIfNeeded()V

    .line 90
    :cond_0
    return-void
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 58
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->link_widget:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;

    .line 59
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->loading:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->loadingView:Landroid/view/View;

    .line 60
    return-void
.end method

.method public setHidingActionBarArticleEventDelegate(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;)V
    .locals 0
    .param p1, "hidingActionBarArticleEventDelegate"    # Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    .line 54
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 0
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->setUserVisibleHint(Z)V

    .line 95
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->sendAnalyticsEventIfNeeded()V

    .line 96
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;)V
    .locals 5
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    .prologue
    const/4 v4, 0x0

    .line 70
    if-nez p2, :cond_0

    if-eqz p1, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v0, v2, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    .line 72
    .local v0, "postTitle":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    iget-object v1, v2, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    .line 73
    .local v1, "postUrl":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;

    invoke-virtual {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V

    .line 74
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$WebArticleRenderDelegate;

    invoke-direct {v3, p0, v4}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$WebArticleRenderDelegate;-><init>(Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment$1;)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->setArticleEventHandler(Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;)V

    .line 77
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;

    invoke-virtual {v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->setup(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;

    invoke-virtual {v2, v4}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->loadDelayedContents(Ljava/lang/Runnable;)V

    .line 80
    .end local v0    # "postTitle":Ljava/lang/String;
    .end local v1    # "postUrl":Ljava/lang/String;
    :cond_0
    return-void
.end method
