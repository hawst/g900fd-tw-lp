.class public Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;
.super Lcom/google/android/libraries/bind/data/DataList;
.source "RecentlyReadList.java"


# static fields
.field public static final DK_EDITION:I

.field public static final DK_LAST_READ:I


# instance fields
.field private final account:Landroid/accounts/Account;

.field private prefsListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

.field private final recentlyReadHelper:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->RecentlyReadList_edition:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->DK_EDITION:I

    .line 30
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->RecentlyReadList_lastRead:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->DK_LAST_READ:I

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;)V
    .locals 1
    .param p1, "recentlyReadHelper"    # Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    .prologue
    .line 36
    sget v0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->DK_EDITION:I

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/data/DataList;-><init>(I)V

    .line 37
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->recentlyReadHelper:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    .line 38
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->account:Landroid/accounts/Account;

    .line 39
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->setDirty(Z)V

    .line 40
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->account:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;)Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->recentlyReadHelper:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    return-object v0
.end method


# virtual methods
.method protected makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList$2;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, p0, p0, v1}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList$2;-><init>(Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method protected onRegisterForInvalidation()V
    .locals 5

    .prologue
    .line 47
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataList;->onRegisterForInvalidation()V

    .line 48
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList$1;-><init>(Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "recentlyReadEditions"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->prefsListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 54
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataList;->onUnregisterForInvalidation()V

    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->prefsListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 60
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->setDirty(Z)V

    .line 61
    return-void
.end method
