.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;
.super Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;
.source "NewsArticleFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setupMeterDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 316
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 7
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 319
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getIsMetered()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 320
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v6, 0x1

    .line 321
    .local v6, "showMeterDialog":Z
    :goto_0
    if-eqz v6, :cond_0

    .line 322
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$400(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setMeterDialog(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V

    .line 324
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .line 325
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->overlayView:Landroid/view/View;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$400(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    move-result-object v5

    .line 324
    invoke-virtual/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Landroid/view/View;Landroid/webkit/WebView;)V

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v0, v6}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setEnabledInArticle(Z)V

    .line 328
    return-void

    .line 320
    .end local v6    # "showMeterDialog":Z
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 316
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
