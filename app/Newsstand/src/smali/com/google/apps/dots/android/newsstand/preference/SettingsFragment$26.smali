.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$26;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setUpInternalSyncPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .prologue
    .line 678
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$26;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 681
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 682
    .local v0, "account":Landroid/accounts/Account;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$26$1;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$26$1;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$26;Lcom/google/apps/dots/android/newsstand/async/Queue;Landroid/accounts/Account;)V

    .line 688
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$26$1;->execute()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 689
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$26;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "Attempting to start background sync. See logs for outcome."

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 691
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 692
    const/4 v1, 0x1

    return v1
.end method
