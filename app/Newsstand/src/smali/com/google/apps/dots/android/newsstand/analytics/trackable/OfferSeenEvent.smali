.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;
.source "OfferSeenEvent.java"


# instance fields
.field private offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

.field private final readingScreen:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 0
    .param p1, "readingScreen"    # Ljava/lang/String;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 22
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 23
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->readingScreen:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 28
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 68
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 69
    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;

    .line 70
    .local v0, "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->readingScreen:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->readingScreen:Ljava/lang/String;

    .line 71
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 72
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 74
    .end local v0    # "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;
    :cond_0
    return v1
.end method

.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 4
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v1

    .line 40
    .local v1, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v0

    .line 42
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    const-string v2, "Offer Seen"

    invoke-virtual {p1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 43
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    .line 44
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 45
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    .line 46
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    .line 47
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 48
    return-object p1
.end method

.method protected getDedupeExpiryTime()J
    .locals 2

    .prologue
    .line 58
    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->readingScreen:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->readingScreen:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isDedupable()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method
