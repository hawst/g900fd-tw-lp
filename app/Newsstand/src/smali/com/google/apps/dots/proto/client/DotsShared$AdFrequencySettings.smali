.class public final Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AdFrequencySettings"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;


# instance fields
.field private articleFrequency_:I

.field private bitField0_:I

.field private every_:I

.field private max_:I

.field private min_:I

.field private page_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2815
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x0

    .line 2816
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2821
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->min_:I

    .line 2840
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->max_:I

    .line 2859
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->every_:I

    .line 2878
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->page_:I

    .line 2897
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->articleFrequency_:I

    .line 2816
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;
    .locals 3

    .prologue
    .line 2930
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2934
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;
    return-object v0

    .line 2931
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;
    :catch_0
    move-exception v1

    .line 2932
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2812
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2939
    if-ne p1, p0, :cond_1

    .line 2942
    :cond_0
    :goto_0
    return v1

    .line 2940
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2941
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    .line 2942
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->min_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->min_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->max_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->max_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->every_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->every_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->page_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->page_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->articleFrequency_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->articleFrequency_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getArticleFrequency()I
    .locals 1

    .prologue
    .line 2899
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->articleFrequency_:I

    return v0
.end method

.method public getEvery()I
    .locals 1

    .prologue
    .line 2861
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->every_:I

    return v0
.end method

.method public getMax()I
    .locals 1

    .prologue
    .line 2842
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->max_:I

    return v0
.end method

.method public getMin()I
    .locals 1

    .prologue
    .line 2823
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->min_:I

    return v0
.end method

.method public getPage()I
    .locals 1

    .prologue
    .line 2880
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->page_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2981
    const/4 v0, 0x0

    .line 2982
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2983
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->min_:I

    .line 2984
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2986
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2987
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->max_:I

    .line 2988
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2990
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2991
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->every_:I

    .line 2992
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2994
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 2995
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->page_:I

    .line 2996
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2998
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 2999
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->articleFrequency_:I

    .line 3000
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3002
    :cond_4
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->cachedSize:I

    .line 3003
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 2950
    const/16 v0, 0x11

    .line 2951
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 2952
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->min_:I

    add-int v0, v1, v2

    .line 2953
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->max_:I

    add-int v0, v1, v2

    .line 2954
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->every_:I

    add-int v0, v1, v2

    .line 2955
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->page_:I

    add-int v0, v1, v2

    .line 2956
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->articleFrequency_:I

    add-int v0, v1, v2

    .line 2957
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3011
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3012
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3016
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3017
    :sswitch_0
    return-object p0

    .line 3022
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->min_:I

    .line 3023
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    goto :goto_0

    .line 3027
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->max_:I

    .line 3028
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    goto :goto_0

    .line 3032
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->every_:I

    .line 3033
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    goto :goto_0

    .line 3037
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->page_:I

    .line 3038
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    goto :goto_0

    .line 3042
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->articleFrequency_:I

    .line 3043
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    goto :goto_0

    .line 3012
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2812
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2962
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2963
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->min_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2965
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2966
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->max_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2968
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2969
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->every_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2971
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 2972
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->page_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2974
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 2975
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->articleFrequency_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2977
    :cond_4
    return-void
.end method
