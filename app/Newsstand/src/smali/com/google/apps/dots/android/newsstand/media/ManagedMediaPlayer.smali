.class public Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;
.super Landroid/media/MediaPlayer;
.source "ManagedMediaPlayer.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;
    }
.end annotation


# static fields
.field private static currentInstance:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;


# instance fields
.field audioFocused:Z

.field private final audioManager:Landroid/media/AudioManager;

.field private bufferPercent:I

.field private onBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private onErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private onPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private onReleaseListener:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;

.field private state:I


# direct methods
.method private constructor <init>(Landroid/media/AudioManager;)V
    .locals 2
    .param p1, "audioManager"    # Landroid/media/AudioManager;

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 57
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    .line 58
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->bufferPercent:I

    .line 67
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioManager:Landroid/media/AudioManager;

    .line 68
    invoke-super {p0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 69
    invoke-super {p0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 70
    invoke-super {p0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 71
    invoke-super {p0, p0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 72
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    sget-object v1, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->currentInstance:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eqz v1, :cond_0

    .line 34
    sget-object v1, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->currentInstance:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->release()V

    .line 35
    const/4 v1, 0x0

    sput-object v1, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->currentInstance:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    .line 37
    :cond_0
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 38
    .local v0, "audioManager":Landroid/media/AudioManager;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;-><init>(Landroid/media/AudioManager;)V

    sput-object v1, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->currentInstance:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    .line 39
    sget-object v1, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->currentInstance:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    return-object v1
.end method


# virtual methods
.method public getBufferPercent()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->bufferPercent:I

    return v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    return v0
.end method

.method public onAudioFocusChange(I)V
    .locals 0
    .param p1, "focusChange"    # I

    .prologue
    .line 92
    packed-switch p1, :pswitch_data_0

    .line 106
    :goto_0
    :pswitch_0
    return-void

    .line 103
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->pause()V

    goto :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "percent"    # I

    .prologue
    .line 258
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->bufferPercent:I

    .line 259
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-interface {v0, p1, p2}, Landroid/media/MediaPlayer$OnBufferingUpdateListener;->onBufferingUpdate(Landroid/media/MediaPlayer;I)V

    .line 262
    :cond_0
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 266
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 267
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-interface {v0, p1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    .line 270
    :cond_0
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v0, 0x0

    .line 274
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    if-eqz v1, :cond_0

    .line 275
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 276
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    .line 278
    :cond_0
    const/16 v1, 0x9

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 279
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    if-eqz v1, :cond_1

    .line 280
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v0

    .line 282
    :cond_1
    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 250
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 251
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-interface {v0, p1}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 254
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    .line 181
    :cond_0
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 182
    invoke-super {p0}, Landroid/media/MediaPlayer;->pause()V

    .line 183
    return-void
.end method

.method public prepare()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 142
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setAudioStreamType(I)V

    .line 143
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 144
    invoke-super {p0}, Landroid/media/MediaPlayer;->prepare()V

    .line 145
    return-void
.end method

.method public prepareAsync()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x3

    .line 149
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setAudioStreamType(I)V

    .line 150
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 151
    invoke-super {p0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 152
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 200
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 202
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    .line 204
    :cond_0
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 205
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onReleaseListener:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onReleaseListener:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;->onRelease()V

    .line 208
    :cond_1
    invoke-super {p0}, Landroid/media/MediaPlayer;->release()V

    .line 211
    sget-object v0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->currentInstance:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-ne p0, v0, :cond_2

    .line 212
    const/4 v0, 0x0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->currentInstance:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    .line 214
    :cond_2
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    .line 222
    :cond_0
    invoke-super {p0}, Landroid/media/MediaPlayer;->reset()V

    .line 223
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    if-eqz v0, :cond_1

    .line 224
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 226
    :cond_1
    return-void
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 114
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 115
    invoke-super {p0, p1, p2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 116
    return-void
.end method

.method public setDataSource(Ljava/io/FileDescriptor;)V
    .locals 1
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 122
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 123
    invoke-super {p0, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 124
    return-void
.end method

.method public setDataSource(Ljava/io/FileDescriptor;JJ)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "offset"    # J
    .param p4, "length"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 130
    invoke-super/range {p0 .. p5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 131
    return-void
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 136
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 137
    invoke-super {p0, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .prologue
    .line 235
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 236
    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnCompletionListener;

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 241
    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnErrorListener;

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 246
    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnPreparedListener;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 231
    return-void
.end method

.method public setOnReleaseListener(Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;)V
    .locals 0
    .param p1, "onReleaseListener"    # Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->onReleaseListener:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;

    .line 76
    return-void
.end method

.method public start()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 160
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    if-nez v1, :cond_0

    .line 161
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    .line 162
    invoke-virtual {v1, p0, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 163
    .local v0, "audioFocus":I
    if-ne v0, v3, :cond_1

    .line 164
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    .line 171
    .end local v0    # "audioFocus":I
    :cond_0
    const/4 v1, 0x5

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 172
    invoke-super {p0}, Landroid/media/MediaPlayer;->start()V

    .line 173
    :goto_0
    return-void

    .line 166
    .restart local v0    # "audioFocus":I
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 167
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->stop()V

    goto :goto_0
.end method

.method public stop()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 187
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 189
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->audioFocused:Z

    .line 191
    :cond_0
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->state:I

    .line 192
    invoke-super {p0}, Landroid/media/MediaPlayer;->stop()V

    .line 193
    return-void
.end method
