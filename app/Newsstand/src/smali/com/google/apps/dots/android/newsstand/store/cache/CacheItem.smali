.class public Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;
.super Ljava/lang/Object;
.source "CacheItem.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final item:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final sizeKb:I

.field public final storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/lang/Object;I)V
    .locals 0
    .param p1, "storeResponse"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    .param p3, "sizeKb"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            "TT;I)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;, "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem<TT;>;"
    .local p2, "item":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    .line 17
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->item:Ljava/lang/Object;

    .line 19
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->sizeKb:I

    .line 20
    return-void
.end method
