.class public abstract Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
.source "NavigationDrawerActivity.java"


# instance fields
.field private activeNavDrawerEntry:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

.field private drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

.field private isAccountListExpanded:Z

.field private optionsMenu:Landroid/view/Menu;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;-><init>(Z)V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->isAccountListExpanded:Z

    .line 29
    return-void
.end method

.method protected constructor <init>(Z)V
    .locals 1
    .param p1, "isUserActivity"    # Z

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;-><init>(Z)V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->isAccountListExpanded:Z

    .line 33
    return-void
.end method


# virtual methods
.method protected closeDrawer()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    .line 93
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->closeDrawer()V

    .line 94
    return-void

    .line 92
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActiveNavDrawerEntry()Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->activeNavDrawerEntry:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    return-object v0
.end method

.method protected getDrawerIndicatorEnabled()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public abstract getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
.end method

.method public isDrawerVisible()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->isDrawerVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->isDrawerOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->closeDrawer()V

    .line 137
    :goto_0
    return-void

    .line 135
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 70
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 73
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x1

    .line 103
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelectedCloseDrawerIfNeeded(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 107
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    .line 108
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->isDrawerIndicatorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 117
    :goto_0
    return v0

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->isDrawerVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->closeDrawer()V

    .line 113
    const/4 v0, 0x1

    goto :goto_0

    .line 117
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 40
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 41
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->getContentView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->drawer:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    .line 45
    if-nez p1, :cond_1

    const/4 v1, 0x0

    .line 46
    :goto_0
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->isAccountListExpanded:Z

    .line 47
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->isAccountListExpanded:Z

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->activeNavDrawerEntry:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->initializeDrawer(ZLcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;)V

    .line 49
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->getDrawerIndicatorEnabled()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->setDrawerIndicatorEnabled(Z)V

    .line 50
    return-void

    .line 45
    :cond_1
    const-string v1, "key_account_list_expanded"

    .line 46
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->optionsMenu:Landroid/view/Menu;

    .line 123
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 59
    const-string v1, "key_account_list_expanded"

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->isAccountListExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 60
    return-void

    .line 59
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setActiveHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)V
    .locals 3
    .param p1, "homePage"    # Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .prologue
    .line 140
    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    .line 141
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->getNavDrawerEntryFromHomePageType(Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;)Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    move-result-object v0

    .line 142
    .local v0, "homePageNavDrawerEntry":Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->activeNavDrawerEntry:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    .line 144
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    if-eqz v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->activeNavDrawerEntry:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->updateDrawerEntries(Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;)V

    .line 147
    :cond_0
    return-void
.end method

.method public setDrawerIndicatorEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->setDrawerIndicatorEnabled(Z)V

    .line 65
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V
    .locals 0
    .param p1, "toolbar"    # Landroid/support/v7/widget/Toolbar;

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 78
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->syncDrawerIndicator()V

    .line 79
    return-void
.end method

.method public syncDrawerIndicator()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->drawer:Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NSPlayDrawerLayout;->syncDrawerIndicator()V

    .line 85
    :cond_0
    return-void
.end method
