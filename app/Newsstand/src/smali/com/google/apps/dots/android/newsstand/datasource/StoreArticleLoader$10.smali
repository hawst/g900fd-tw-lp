.class Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$10;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getBaseHtmlFuture(Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

.field final synthetic val$widget:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 346
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$10;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$10;->val$widget:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 346
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$10;->apply(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public apply(Ljava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 349
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v5, 0x0

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 350
    .local v0, "articleTemplate":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    const/4 v5, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 351
    .local v1, "htmlFromFile":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$10;->val$widget:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    .line 352
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getZoomable()Z

    move-result v6

    invoke-direct {v4, v5, v1, v6}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;-><init>(Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;Ljava/lang/String;Z)V

    .line 353
    .local v4, "processor":Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getLandscapeDisplay()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    move-result-object v2

    .line 354
    .local v2, "landscapeTarget":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getPortraitDisplay()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    move-result-object v3

    .line 356
    .local v3, "portraitTarget":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$10;->val$widget:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->landscape()Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz v2, :cond_0

    .line 357
    invoke-virtual {v4, v2}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtmlForTarget(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;)Ljava/lang/String;

    move-result-object v5

    .line 361
    :goto_0
    return-object v5

    .line 358
    :cond_0
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$10;->val$widget:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->landscape()Z

    move-result v5

    if-nez v5, :cond_1

    if-eqz v3, :cond_1

    .line 359
    invoke-virtual {v4, v3}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtmlForTarget(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 361
    :cond_1
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtml()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method
