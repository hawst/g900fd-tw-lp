.class Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView$1;
.super Ljava/lang/Object;
.source "ChangeTextView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView;->createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView;

.field final synthetic val$endText:Ljava/lang/CharSequence;

.field final synthetic val$startText:Ljava/lang/CharSequence;

.field final synthetic val$textView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView;Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView$1;->this$0:Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView$1;->val$textView:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView$1;->val$endText:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView$1;->val$startText:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 85
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView$1;->val$textView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    const/high16 v2, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView$1;->val$endText:Ljava/lang/CharSequence;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView$1;->val$startText:Ljava/lang/CharSequence;

    goto :goto_0
.end method
