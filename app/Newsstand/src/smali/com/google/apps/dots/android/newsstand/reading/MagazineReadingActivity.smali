.class public Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "MagazineReadingActivity.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private readingFragment:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>()V

    return-void
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 44
    if-nez p1, :cond_0

    .line 48
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->readingFragment:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->handleExtras(Landroid/os/Bundle;)Z

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 52
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->readingFragment:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->readingFragment:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->readingFragment:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->handleOnBackPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->magazine_reading_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->setContentView(I)V

    .line 30
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->magazine_reading_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->readingFragment:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .line 33
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 34
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 59
    .local v0, "menuInflater":Landroid/view/MenuInflater;
    sget v1, Lcom/google/android/apps/newsstanddev/R$menu;->reading_activity_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 60
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 38
    sget-object v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onNewIntent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 40
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 41
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingActivity;->onOptionsItemSelectedCloseDrawerIfNeeded(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const/4 v0, 0x1

    .line 68
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
