.class public Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
.super Ljava/lang/Object;
.source "AsyncScope.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final accountScopes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncScope;",
            ">;"
        }
    .end annotation
.end field

.field private static currentUserScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;


# instance fields
.field private accountOverride:Landroid/accounts/Account;

.field private asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private final children:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncScope;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final parent:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private final scopeThread:Ljava/lang/Thread;

.field private stoppedOverride:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 25
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->accountScopes:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    check-cast v0, Landroid/accounts/Account;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;-><init>(Landroid/accounts/Account;)V

    .line 112
    return-void
.end method

.method private constructor <init>(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->children:Ljava/util/WeakHashMap;

    .line 123
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->parent:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 124
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->accountOverride:Landroid/accounts/Account;

    .line 125
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->scopeThread:Ljava/lang/Thread;

    .line 126
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 127
    return-void
.end method

.method private constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;)V
    .locals 1
    .param p1, "parent"    # Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->children:Ljava/util/WeakHashMap;

    .line 115
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->parent:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->accountOverride:Landroid/accounts/Account;

    .line 117
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->scopeThread:Ljava/lang/Thread;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->scopeThread:Ljava/lang/Thread;

    .line 118
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 119
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->updateOnStateChange()V

    .line 120
    return-void
.end method

.method static synthetic access$000()Ljava/util/Map;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->accountScopes:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;)Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .prologue
    .line 23
    sput-object p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    return-object p0
.end method

.method static synthetic access$200()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    return-object v0
.end method

.method private checkScopeThread()V
    .locals 2

    .prologue
    .line 242
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->scopeThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Accessing scope off creating thread."

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 244
    return-void

    .line 242
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static currentUserScope()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 4

    .prologue
    .line 50
    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->accountScopes:Ljava/util/Map;

    monitor-enter v2

    .line 51
    :try_start_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    if-nez v1, :cond_0

    .line 52
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 53
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    new-instance v1, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;-><init>(Landroid/accounts/Account;)V

    sput-object v1, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 55
    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->accountScopes:Ljava/util/Map;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    monitor-exit v2

    return-object v1

    .line 58
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private destroyed()Z
    .locals 2

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stopped()Ljava/lang/Boolean;

    move-result-object v0

    .line 168
    .local v0, "stopped":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static resetAccountScope()V
    .locals 2

    .prologue
    .line 29
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/async/AsyncScope$1;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope$1;-><init>()V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->tryRunElsePost(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    .line 47
    return-void
.end method

.method private stopped(Ljava/lang/Boolean;)Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 1
    .param p1, "stopped"    # Ljava/lang/Boolean;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stoppedOverride:Ljava/lang/Boolean;

    if-eq v0, p1, :cond_0

    .line 200
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stoppedOverride:Ljava/lang/Boolean;

    .line 201
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->updateOnStateChange()V

    .line 203
    :cond_0
    return-object p0
.end method

.method private stopped()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->parent:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stoppedOverride:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stoppedOverride:Ljava/lang/Boolean;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->parent:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stopped()Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method private updateOnStateChange()V
    .locals 4

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->destroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroy()V

    .line 158
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->children:Ljava/util/WeakHashMap;

    monitor-enter v2

    .line 159
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->children:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 160
    .local v0, "child":Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->updateOnStateChange()V

    goto :goto_1

    .line 162
    .end local v0    # "child":Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    goto :goto_0

    .line 162
    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->destroyed()Z

    move-result v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->isDestroyed()Z

    move-result v2

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 164
    return-void

    .line 163
    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public static user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 1

    .prologue
    .line 66
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    return-object v0
.end method

.method public static userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1

    .prologue
    .line 74
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->currentUserScope()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    return-object v0
.end method

.method public static userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 2

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    return-object v0
.end method

.method public static userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    return-object v0
.end method

.method public static userless()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;-><init>()V

    return-object v0
.end method


# virtual methods
.method public account()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->parent:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->accountOverride:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->accountOverride:Landroid/accounts/Account;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->parent:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0
.end method

.method public inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 4

    .prologue
    .line 133
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncScope;)V

    .line 134
    .local v0, "child":Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->children:Ljava/util/WeakHashMap;

    monitor-enter v2

    .line 135
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->children:Ljava/util/WeakHashMap;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    monitor-exit v2

    .line 137
    return-object v0

    .line 136
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->checkScopeThread()V

    .line 230
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->destroyed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 1

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->checkScopeThread()V

    .line 238
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    return-object v0
.end method

.method public start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->checkScopeThread()V

    .line 211
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stopped(Ljava/lang/Boolean;)Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    return-object v0
.end method

.method public stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 1

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->checkScopeThread()V

    .line 222
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stopped(Ljava/lang/Boolean;)Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    return-object v0
.end method

.method public token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 5

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->checkScopeThread()V

    .line 146
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->destroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isStrictModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Requesting expired async token: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getStackTrace(Ljava/lang/Thread;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    return-object v0
.end method
