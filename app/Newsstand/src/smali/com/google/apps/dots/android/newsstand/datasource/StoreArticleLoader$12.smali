.class Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$12;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getHtmlFromFileFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 382
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$12;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$12;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1, "useLegacyLayout"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 385
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "webview_legacy.html"

    .line 389
    .local v1, "id":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->layoutStore()Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$12;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v2, v3, v1}, Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 390
    .local v0, "htmlFromFileFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$12;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$12;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$1;)V

    invoke-virtual {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 391
    return-object v0

    .line 385
    .end local v0    # "htmlFromFileFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/String;>;"
    .end local v1    # "id":Ljava/lang/String;
    :cond_0
    const-string v1, "webview.html"

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 382
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$12;->apply(Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
