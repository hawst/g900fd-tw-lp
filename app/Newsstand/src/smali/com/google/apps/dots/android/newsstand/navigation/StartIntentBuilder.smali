.class public Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "StartIntentBuilder.java"


# instance fields
.field private final intent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "originalIntent"    # Landroid/content/Intent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/content/Context;)V

    .line 25
    const-class v0, Lcom/google/apps/dots/android/app/activity/CurrentsStartActivity;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;->intent:Landroid/content/Intent;

    .line 26
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;->intent:Landroid/content/Intent;

    const v1, 0x14008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 28
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;->intent:Landroid/content/Intent;

    invoke-virtual {p0, p2, v0}, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;->copyExtrasAndDataIfPossible(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 29
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;->intent:Landroid/content/Intent;

    return-object v0
.end method
