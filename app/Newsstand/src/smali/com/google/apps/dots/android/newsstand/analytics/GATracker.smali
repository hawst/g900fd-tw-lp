.class public Lcom/google/apps/dots/android/newsstand/analytics/GATracker;
.super Ljava/lang/Object;
.source "GATracker.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final NAME_TO_CUSTOM_DIMENSION_INDEX:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final NAME_TO_CUSTOM_METRIC_INDEX:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final appName:Ljava/lang/String;

.field private final appVersion:Ljava/lang/String;

.field private final gaTrackerManager:Lcom/google/android/gms/analytics/GoogleAnalytics;

.field private final lastScreenNameTrackedMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const-class v0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 54
    new-instance v0, Lcom/google/common/collect/ImmutableMap$Builder;

    invoke-direct {v0}, Lcom/google/common/collect/ImmutableMap$Builder;-><init>()V

    const-string v1, "ReadFrom"

    const/16 v2, 0xf

    .line 56
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "SyncMetadata"

    const/16 v2, 0x10

    .line 57
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "TranslatedLanguage"

    const/16 v2, 0x11

    .line 58
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "LiteMode"

    const/16 v2, 0x12

    .line 59
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "MiniMode"

    const/16 v2, 0x1a

    .line 60
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "SettingsWiFiOnlyDownload"

    const/16 v2, 0x1b

    .line 61
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "SettingsKeepReadNow"

    const/16 v2, 0x1c

    .line 62
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "SettingsWidgetInstalled"

    const/16 v2, 0x1d

    .line 63
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "SettingsEditionsPinned"

    const/16 v2, 0x1e

    .line 64
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "SettingsMagazinesPinned"

    const/16 v2, 0x1f

    .line 65
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "CuratedTopic"

    const/16 v2, 0x20

    .line 66
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "DocType"

    const/16 v2, 0x21

    .line 67
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "OfferType"

    const/16 v2, 0x22

    .line 68
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "OfferCurrencyCode"

    const/16 v2, 0x23

    .line 69
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "IsGoogleSoldAd"

    const/16 v2, 0x24

    .line 70
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "AdFormat"

    const/16 v2, 0x25

    .line 71
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "AdCreativeId"

    const/16 v2, 0x26

    .line 72
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "ItemsAccepted"

    const/16 v2, 0x27

    .line 73
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "ItemsRejected"

    const/16 v2, 0x28

    .line 74
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "ItemsTotal"

    const/16 v2, 0x29

    .line 75
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "ReadFromAppId"

    const/16 v2, 0x2a

    .line 76
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "WebArticleEvent"

    const/16 v2, 0x2b

    .line 77
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "ErrorDescription"

    const/16 v2, 0x2c

    .line 78
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "ErrorCode"

    const/16 v2, 0x2d

    .line 79
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->NAME_TO_CUSTOM_DIMENSION_INDEX:Lcom/google/common/collect/ImmutableMap;

    .line 82
    new-instance v0, Lcom/google/common/collect/ImmutableMap$Builder;

    invoke-direct {v0}, Lcom/google/common/collect/ImmutableMap$Builder;-><init>()V

    const-string v1, "OfferPrice"

    const/4 v2, 0x1

    .line 84
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->NAME_TO_CUSTOM_METRIC_INDEX:Lcom/google/common/collect/ImmutableMap;

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/analytics/GoogleAnalytics;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "googleAnalytics"    # Lcom/google/android/gms/analytics/GoogleAnalytics;

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->lastScreenNameTrackedMap:Ljava/util/Map;

    .line 97
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->gaTrackerManager:Lcom/google/android/gms/analytics/GoogleAnalytics;

    .line 98
    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->app_name:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->appName:Ljava/lang/String;

    .line 100
    const-string v1, "unknown"

    .line 102
    .local v1, "version":Ljava/lang/String;
    :try_start_0
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->getVersionName(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 108
    :goto_0
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->appVersion:Ljava/lang/String;

    .line 109
    return-void

    .line 103
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Couldn\'t get info for package %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private addCustomDimensions(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;)V
    .locals 9
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "hitBuilderWrapper"    # Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .prologue
    const/4 v3, 0x0

    .line 192
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasAppFamilyId()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 193
    const/4 v4, 0x1

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getAppFamilyId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 194
    const/4 v4, 0x5

    .line 195
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getAppFamilyName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 197
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasAppId()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 198
    const/4 v4, 0x2

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getAppId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 199
    const/4 v4, 0x6

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getAppName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 201
    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasSectionId()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 202
    const/4 v4, 0x3

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getSectionId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 203
    const/4 v4, 0x7

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getSectionName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 205
    :cond_2
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasPostId()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 206
    const/4 v4, 0x4

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getPostId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 207
    const/16 v4, 0x8

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getPostTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 209
    :cond_3
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasPage()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 210
    const/16 v4, 0x9

    .line 211
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getPage()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 210
    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 213
    :cond_4
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasUserSubscriptionType()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 214
    const/16 v4, 0xa

    .line 215
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getUserSubscriptionType()I

    move-result v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;

    move-result-object v5

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SubscriptionType;->subscriptionType:Ljava/lang/String;

    .line 214
    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 217
    :cond_5
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasIsOffline()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 218
    const/16 v4, 0xb

    .line 219
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getIsOffline()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    .line 218
    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 221
    :cond_6
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasTestId()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 222
    const/16 v4, 0xc

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getTestId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 224
    :cond_7
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasStoreType()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 225
    const/16 v4, 0xd

    .line 226
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getStoreType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 225
    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 228
    :cond_8
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasExperiments()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 229
    const/16 v4, 0xe

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getExperiments()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 232
    :cond_9
    iget-object v5, p1, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_b

    aget-object v2, v5, v4

    .line 233
    .local v2, "pair":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    sget-object v7, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->NAME_TO_CUSTOM_DIMENSION_INDEX:Lcom/google/common/collect/ImmutableMap;

    iget-object v8, v2, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 234
    .local v0, "index":Ljava/lang/Integer;
    if-eqz v0, :cond_a

    .line 235
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, v2, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    invoke-virtual {p2, v7, v8}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomDimension(ILjava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 232
    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 239
    .end local v0    # "index":Ljava/lang/Integer;
    .end local v2    # "pair":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    :cond_b
    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    array-length v5, v4

    :goto_1
    if-ge v3, v5, :cond_d

    aget-object v1, v4, v3

    .line 240
    .local v1, "metric":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->NAME_TO_CUSTOM_METRIC_INDEX:Lcom/google/common/collect/ImmutableMap;

    iget-object v7, v1, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 241
    .restart local v0    # "index":Ljava/lang/Integer;
    if-eqz v0, :cond_c

    .line 242
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget v7, v1, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;->value:F

    invoke-virtual {p2, v6, v7}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->setCustomMetric(IF)Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    .line 239
    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 245
    .end local v0    # "index":Ljava/lang/Integer;
    .end local v1    # "metric":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    :cond_d
    return-void
.end method

.method public static flushAnalyticsEvents()V
    .locals 1

    .prologue
    .line 175
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->dispatchLocalHits()V

    .line 176
    return-void
.end method

.method private sendAppScreenView(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/android/gms/analytics/Tracker;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "tracker"    # Lcom/google/android/gms/analytics/Tracker;
    .param p3, "trackingId"    # Ljava/lang/String;
    .param p4, "appScreen"    # Ljava/lang/String;
    .param p5, "eventDriven"    # Z

    .prologue
    .line 180
    sget-object v2, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    if-eqz p5, :cond_0

    const-string v1, "VIEW FROM EVENT to %s, %s"

    :goto_0
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p3, v3, v4

    const/4 v4, 0x1

    aput-object p4, v3, v4

    invoke-virtual {v2, v1, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->di(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->lastScreenNameTrackedMap:Ljava/util/Map;

    invoke-interface {v1, p3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    new-instance v1, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    invoke-direct {v1}, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;-><init>(Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;)V

    .line 185
    .local v0, "hitBuilderWrapper":Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->addCustomDimensions(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;)V

    .line 188
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->build()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 189
    return-void

    .line 180
    .end local v0    # "hitBuilderWrapper":Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;
    :cond_0
    const-string v1, "VIEW to %s, %s"

    goto :goto_0
.end method


# virtual methods
.method public supportsAccountlessEvents()Z
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x1

    return v0
.end method

.method public trackEvent(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V
    .locals 11
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "trackingId"    # Ljava/lang/String;
    .param p3, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .prologue
    .line 118
    invoke-static {p2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "trackEvent() - no account id"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->gaTrackerManager:Lcom/google/android/gms/analytics/GoogleAnalytics;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/analytics/GoogleAnalytics;->newTracker(Ljava/lang/String;)Lcom/google/android/gms/analytics/Tracker;

    move-result-object v2

    .line 124
    .local v2, "tracker":Lcom/google/android/gms/analytics/Tracker;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->appName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/analytics/Tracker;->setAppName(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->appVersion:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/analytics/Tracker;->setAppVersion(Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/translation/Translation;->getPreferred()Lcom/google/apps/dots/android/newsstand/translation/Translation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/translation/Translation;->toLanguageCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/analytics/Tracker;->setLanguage(Ljava/lang/String;)V

    .line 128
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasScreen()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getScreen()Ljava/lang/String;

    move-result-object v4

    .line 129
    .local v4, "appScreen":Ljava/lang/String;
    :goto_1
    invoke-static {v4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 130
    sget v0, Lcom/google/android/apps/newsstanddev/R$bool;->fail_on_analytics_errors:I

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getBooleanResource(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 131
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot track event without a screen name."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    .end local v4    # "appScreen":Ljava/lang/String;
    :cond_2
    const-string v4, ""

    goto :goto_1

    .line 133
    .restart local v4    # "appScreen":Ljava/lang/String;
    :cond_3
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "trackEvent() - no event screen"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 136
    :cond_4
    invoke-virtual {v2, v4}, Lcom/google/android/gms/analytics/Tracker;->setScreenName(Ljava/lang/String;)V

    .line 138
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getCategory()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 140
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p3

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->sendAppScreenView(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/android/gms/analytics/Tracker;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 142
    :cond_5
    const-wide/16 v8, 0x0

    .line 143
    .local v8, "value":J
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 144
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getValue()J

    move-result-wide v8

    .line 148
    :cond_6
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->lastScreenNameTrackedMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 149
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p3

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->sendAppScreenView(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/android/gms/analytics/Tracker;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 152
    :cond_7
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "EVENT to %s: [appScreen: \'%s\', action: %s, AppName: %s, postName: %s]"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v3, v5

    const/4 v5, 0x1

    aput-object v4, v3, v5

    const/4 v5, 0x2

    .line 155
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getAction()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v3, v5

    const/4 v5, 0x3

    .line 156
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getAppName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v3, v5

    const/4 v5, 0x4

    .line 157
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getPostTitle()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v3, v5

    .line 152
    invoke-virtual {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->di(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    new-instance v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    .line 160
    .local v6, "eventBuilder":Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getCategory()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCategory(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setAction(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    .line 161
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setLabel(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setValue(J)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    .line 164
    new-instance v7, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;

    invoke-direct {v7, v6}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;-><init>(Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    .line 165
    .local v7, "hitBuilderWrapper":Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;
    invoke-direct {p0, p3, v7}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->addCustomDimensions(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;)V

    .line 166
    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker$NSHitBuilderWrapper;->build()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    goto/16 :goto_0
.end method
