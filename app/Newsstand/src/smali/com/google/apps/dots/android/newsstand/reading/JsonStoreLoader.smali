.class public Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;
.super Ljava/lang/Object;
.source "JsonStoreLoader.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/util/Loader;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/android/newsstand/util/Loader",
        "<",
        "Lorg/codehaus/jackson/node/ObjectNode;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final androidUtil:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

.field private final articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

.field private final configUtil:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

.field private final context:Landroid/content/Context;

.field private final delegate:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;

.field private final isA11yTouchExplorationEnabled:Z

.field private final originalEditionSubscriptionType:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

.field private final postIndex:Ljava/lang/Integer;

.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field private final serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    const-class v0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;Ljava/lang/Integer;Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "androidUtil"    # Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
    .param p3, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .param p4, "configUtil"    # Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;
    .param p5, "serverUris"    # Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .param p6, "articleLoader"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p7, "originalEditionSubscriptionType"    # Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;
    .param p8, "optPostIndex"    # Ljava/lang/Integer;
    .param p9, "delegate"    # Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;
    .param p10, "isA11yTouchExplorationEnabled"    # Z

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->context:Landroid/content/Context;

    .line 146
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->androidUtil:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .line 147
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 148
    invoke-static {p4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->configUtil:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    .line 149
    invoke-static {p5}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 150
    invoke-static {p6}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .line 152
    invoke-static {p7}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->originalEditionSubscriptionType:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    .line 153
    invoke-static {p9}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->delegate:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;

    .line 154
    iput-boolean p10, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->isA11yTouchExplorationEnabled:Z

    .line 155
    iput-object p8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->postIndex:Ljava/lang/Integer;

    .line 156
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Landroid/accounts/Account;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;
    .param p1, "x1"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "x2"    # Landroid/accounts/Account;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addExecutionContextData(Lorg/codehaus/jackson/node/ObjectNode;Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;
    .param p1, "x1"    # Lorg/codehaus/jackson/node/ObjectNode;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addLabsData(Lorg/codehaus/jackson/node/ObjectNode;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/util/Map;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;
    .param p1, "x1"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .param p3, "x3"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p4, "x4"    # Ljava/util/Map;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addAdsData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;
    .param p1, "x1"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addSectionHeaderData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$RoleList;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;
    .param p1, "x1"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addUserRolesData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$RoleList;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;
    .param p1, "x1"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .param p3, "x3"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addTextNameData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;ZLcom/google/apps/dots/proto/client/DotsShared$Form;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;
    .param p1, "x1"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addContentData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;ZLcom/google/apps/dots/proto/client/DotsShared$Form;)V

    return-void
.end method

.method private addAdsData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/util/Map;)V
    .locals 2
    .param p1, "jsonStore"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "app"    # Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .param p3, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/codehaus/jackson/node/ObjectNode;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Application;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 278
    .local p4, "idToAdTemplates":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->postIndex:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 279
    const-string v0, "postIndex"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->postIndex:Ljava/lang/Integer;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 282
    :cond_0
    const-string v0, "adBlockData"

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 283
    invoke-virtual {p0, p2, p3, p4, v1}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->buildAdBlockData(Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/util/Map;Ljava/util/Random;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v1

    .line 282
    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 284
    return-void
.end method

.method private addContentData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;ZLcom/google/apps/dots/proto/client/DotsShared$Form;)V
    .locals 3
    .param p1, "jsonStore"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "articleTemplate"    # Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .param p3, "useLegacyLayout"    # Z
    .param p4, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .prologue
    const/4 v2, 0x1

    .line 337
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getUsesColumns()Z

    move-result v1

    if-nez v1, :cond_0

    .line 338
    const-string v1, "skipColumnLayout"

    invoke-virtual {p1, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Z)V

    .line 341
    :cond_0
    if-eqz p3, :cond_1

    .line 344
    const-string v1, "useVerticalLayout"

    invoke-virtual {p1, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Z)V

    .line 348
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->delegate:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;

    invoke-interface {v1, p2, p4, p3}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;->getJsonDataContent(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;Lcom/google/apps/dots/proto/client/DotsShared$Form;Z)Ljava/lang/String;

    move-result-object v0

    .line 349
    .local v0, "content":Ljava/lang/String;
    const-string v1, "content"

    invoke-virtual {p1, v1, v0}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    return-void
.end method

.method private addExecutionContextData(Lorg/codehaus/jackson/node/ObjectNode;Landroid/accounts/Account;)V
    .locals 3
    .param p1, "jsonStore"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 229
    const-string v0, "serverBaseUrl"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    invoke-virtual {v1, p2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getBaseUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string v0, "attachmentBaseUrl"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->delegate:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;->getDataAttachmentBaseUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v1, "isPhone"

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->androidUtil:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/shared/DeviceCategory;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Z)V

    .line 234
    const-string v0, "deviceCategory"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->androidUtil:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .line 235
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/shared/DeviceCategory;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 234
    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v0, "clientPlatform"

    const-string v1, "android"

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v0, "clientPlatformVersion"

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 239
    const-string v0, "clientProduct"

    const-string v1, "ns"

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const-string v0, "clientVersion"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->getVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const-string v0, "layoutEngineVersion"

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 244
    const-string v0, "appId"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const-string v0, "outerMargin"

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ArticleLoadingUtil;->getDefaultArticleMarginOuter()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v0, "topMargin"

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ArticleLoadingUtil;->getDefaultArticleMarginTop()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const-string v0, "innerMargin"

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ArticleLoadingUtil;->getDefaultArticleMarginInner()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v0, "bottomMargin"

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ArticleLoadingUtil;->getDefaultArticleMarginBottom()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    return-void

    .line 233
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private addLabsData(Lorg/codehaus/jackson/node/ObjectNode;)V
    .locals 4
    .param p1, "jsonStore"    # Lorg/codehaus/jackson/node/ObjectNode;

    .prologue
    .line 259
    sget-object v2, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v2}, Lorg/codehaus/jackson/node/JsonNodeFactory;->arrayNode()Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v1

    .line 260
    .local v1, "node":Lorg/codehaus/jackson/node/ArrayNode;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getEnabledLabIds()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 261
    .local v0, "entry":Ljava/lang/String;
    invoke-virtual {v1, v0}, Lorg/codehaus/jackson/node/ArrayNode;->add(Ljava/lang/String;)V

    goto :goto_0

    .line 263
    .end local v0    # "entry":Ljava/lang/String;
    :cond_0
    const-string v2, "enabledLabIds"

    invoke-virtual {p1, v2, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 264
    return-void
.end method

.method private addSectionHeaderData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V
    .locals 2
    .param p1, "jsonStore"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .prologue
    .line 293
    const-string v0, "sectionHeaderTemplate"

    .line 294
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getDisplayOptions()Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->getHeaderTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->getMainTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getTemplate()Ljava/lang/String;

    move-result-object v1

    .line 293
    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    return-void
.end method

.method private addTextNameData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V
    .locals 3
    .param p1, "jsonStore"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "app"    # Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .param p3, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .prologue
    .line 316
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 317
    .local v0, "textData":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "editionName"

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const-string v1, "sectionName"

    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const-string v1, "text"

    invoke-virtual {p1, v1, v0}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 321
    iget-object v1, p3, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 322
    const-string v1, "sectionId"

    iget-object v2, p3, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    :cond_0
    return-void
.end method

.method private addUserRolesData(Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/proto/client/DotsShared$RoleList;)V
    .locals 2
    .param p1, "jsonStore"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "userRoles"    # Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .prologue
    .line 304
    if-eqz p2, :cond_0

    .line 305
    const-string v0, "userRoles"

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->buildRolesBlockData(Lcom/google/apps/dots/proto/client/DotsShared$RoleList;)Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 307
    :cond_0
    return-void
.end method

.method private buildAdTemplateString(Ljava/util/Map;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;ZZ)Ljava/lang/String;
    .locals 1
    .param p2, "settings"    # Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    .param p3, "isPhone"    # Z
    .param p4, "showGoogleSold"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;",
            "Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;",
            "ZZ)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 518
    .local p1, "idToAdTemplate":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;"
    if-eqz p2, :cond_1

    .line 519
    if-eqz p4, :cond_0

    .line 520
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getGoogleSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    .line 519
    :goto_0
    invoke-virtual {p0, p1, v0, p3}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->getAdTemplate(Ljava/util/Map;Lcom/google/apps/dots/proto/client/DotsShared$AdContent;Z)Ljava/lang/String;

    move-result-object v0

    .line 522
    :goto_1
    return-object v0

    .line 520
    :cond_0
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getPubSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    goto :goto_0

    .line 522
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private buildRolesBlockData(Lcom/google/apps/dots/proto/client/DotsShared$RoleList;)Lorg/codehaus/jackson/node/ArrayNode;
    .locals 5
    .param p1, "roles"    # Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .prologue
    .line 451
    sget-object v2, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v2}, Lorg/codehaus/jackson/node/JsonNodeFactory;->arrayNode()Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v0

    .line 452
    .local v0, "roleBlockData":Lorg/codehaus/jackson/node/ArrayNode;
    iget-object v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 453
    .local v1, "roleId":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lorg/codehaus/jackson/node/ArrayNode;->add(Ljava/lang/String;)V

    .line 452
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 455
    .end local v1    # "roleId":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "objectNode"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 552
    if-eqz p3, :cond_0

    .line 553
    invoke-virtual {p1, p2, p3}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    :cond_0
    return-void
.end method


# virtual methods
.method addDemographicInfo(Lorg/codehaus/jackson/node/ObjectNode;)V
    .locals 9
    .param p1, "adBlockData"    # Lorg/codehaus/jackson/node/ObjectNode;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 491
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 492
    .local v0, "account":Landroid/accounts/Account;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "account is %s"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v0, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 493
    if-eqz v0, :cond_0

    .line 494
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->configUtil:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v1

    .line 495
    .local v1, "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->hasDemographics()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 496
    sget-object v3, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "config is %s"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v1, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 497
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getDemographics()Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    move-result-object v2

    .line 498
    .local v2, "demographics":Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
    if-eqz v2, :cond_0

    .line 499
    sget-object v3, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "demographics is %s %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->getAgeRange()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->getGender()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 500
    const-string v3, "ageRange"

    .line 501
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->getAgeRange()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 500
    invoke-direct {p0, p1, v3, v4}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    const-string v3, "gender"

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->getGender()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p1, v3, v4}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    .end local v1    # "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .end local v2    # "demographics":Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
    :cond_0
    return-void
.end method

.method buildAdBlockData(Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/util/Map;Ljava/util/Random;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 13
    .param p1, "app"    # Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .param p2, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p4, "random"    # Ljava/util/Random;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Application;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;",
            "Ljava/util/Random;",
            ")",
            "Lorg/codehaus/jackson/node/ObjectNode;"
        }
    .end annotation

    .prologue
    .line 366
    .local p3, "idToAdTemplates":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;"
    sget-object v8, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v8}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v1

    .line 368
    .local v1, "adBlockData":Lorg/codehaus/jackson/node/ObjectNode;
    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->isA11yTouchExplorationEnabled:Z

    if-eqz v8, :cond_1

    .line 442
    :cond_0
    :goto_0
    return-object v1

    .line 372
    :cond_1
    new-instance v6, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;

    const/4 v8, 0x0

    invoke-direct {v6, v8}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;-><init>(Z)V

    .line 373
    .local v6, "serializer":Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    .line 375
    .local v4, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->androidUtil:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v8

    sget-object v9, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v8, v9, :cond_6

    const/4 v2, 0x1

    .line 376
    .local v2, "isPhone":Z
    :goto_1
    move-object/from16 v0, p4

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->showGoogleSoldAds(Lcom/google/apps/dots/proto/client/DotsShared$Application;Ljava/util/Random;)Z

    move-result v7

    .line 380
    .local v7, "showGoogleSoldAds":Z
    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Integer;

    const/4 v9, 0x0

    .line 381
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getInterstitialAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->getPubAdSystem(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    .line 382
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getLeaderboardAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->getPubAdSystem(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    .line 383
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getMrectAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->getPubAdSystem(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    .line 381
    invoke-static {v8}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v5

    .line 384
    .local v5, "pubAdSystems":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 385
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_7

    const/4 v8, 0x5

    .line 386
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    const/4 v3, 0x1

    .line 388
    .local v3, "onlyGoogleManaged":Z
    :goto_2
    sget-object v8, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v9, "getAdBlockData(): isPhone? %b showGoogleSold? %b onlyGoogleManaged? %b"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    .line 389
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    .line 388
    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 392
    const-string v8, "adFrequencySettings"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getAdFrequency()Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 394
    const-string v8, "interstitialAdSettings"

    .line 395
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getInterstitialAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v9

    .line 394
    invoke-virtual {v1, v8, v9}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 396
    const-string v8, "interstitialAdTemplate"

    .line 397
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getInterstitialAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v9

    .line 396
    move-object/from16 v0, p3

    invoke-direct {p0, v0, v9, v2, v7}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->buildAdTemplateString(Ljava/util/Map;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;ZZ)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    const-string v8, "leaderboardAdSettings"

    .line 400
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getLeaderboardAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v9

    .line 399
    invoke-virtual {v1, v8, v9}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 401
    const-string v8, "leaderboardAdTemplate"

    .line 402
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getLeaderboardAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v9

    .line 401
    move-object/from16 v0, p3

    invoke-direct {p0, v0, v9, v2, v7}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->buildAdTemplateString(Ljava/util/Map;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;ZZ)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const-string v8, "mrectAdSettings"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getMrectAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 405
    const-string v8, "mrectAdTemplate"

    .line 406
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getMrectAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v9

    .line 405
    move-object/from16 v0, p3

    invoke-direct {p0, v0, v9, v2, v7}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->buildAdTemplateString(Ljava/util/Map;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;ZZ)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const-string v8, "showEmptyAds"

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDesignerMode()Z

    move-result v9

    invoke-virtual {v1, v8, v9}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Z)V

    .line 411
    const-string v8, "postId"

    iget-object v9, v4, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    invoke-virtual {v1, v8, v9}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    const-string v8, "appId"

    iget-object v9, v4, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    invoke-virtual {v1, v8, v9}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const-string v8, "appFamilyId"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getAppFamilyId()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const-string v8, "languagePref"

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 416
    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getPreferredLanguage()Ljava/lang/String;

    move-result-object v9

    .line 415
    invoke-direct {p0, v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    const-string v8, "contentLanguage"

    .line 418
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getLanguageCode()Ljava/lang/String;

    move-result-object v9

    .line 417
    invoke-direct {p0, v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    const-string v9, "deviceType"

    if-eqz v2, :cond_8

    const-string v8, "phone"

    :goto_3
    invoke-direct {p0, v1, v9, v8}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const-string v8, "platform"

    const-string v9, "android"

    invoke-direct {p0, v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    const-string v8, "subscriptionType"

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->originalEditionSubscriptionType:Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    .line 423
    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->toString()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    .line 422
    invoke-direct {p0, v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    if-eqz v7, :cond_2

    .line 429
    const-string v8, "showGoogleSold"

    invoke-virtual {v1, v8, v7}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Z)V

    .line 433
    :cond_2
    if-nez v7, :cond_3

    const/4 v8, 0x5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 434
    :cond_3
    const-string v8, "topic"

    const-string v9, ","

    .line 435
    invoke-static {v9}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v9

    iget-object v10, p2, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    invoke-static {v10}, Lcom/google/common/primitives/Ints;->asList([I)Ljava/util/List;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v9

    .line 434
    invoke-direct {p0, v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->putIfNotNull(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    :cond_4
    if-nez v7, :cond_5

    if-eqz v3, :cond_0

    .line 440
    :cond_5
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->addDemographicInfo(Lorg/codehaus/jackson/node/ObjectNode;)V

    goto/16 :goto_0

    .line 375
    .end local v2    # "isPhone":Z
    .end local v3    # "onlyGoogleManaged":Z
    .end local v5    # "pubAdSystems":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v7    # "showGoogleSoldAds":Z
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 386
    .restart local v2    # "isPhone":Z
    .restart local v5    # "pubAdSystems":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v7    # "showGoogleSoldAds":Z
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 419
    .restart local v3    # "onlyGoogleManaged":Z
    :cond_8
    const-string v8, "tablet"

    goto :goto_3
.end method

.method getAdTemplate(Ljava/util/Map;Lcom/google/apps/dots/proto/client/DotsShared$AdContent;Z)Ljava/lang/String;
    .locals 3
    .param p2, "content"    # Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    .param p3, "isPhone"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;",
            "Lcom/google/apps/dots/proto/client/DotsShared$AdContent;",
            "Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "idToAdTemplate":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;"
    const/4 v1, 0x0

    .line 533
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdSystem()I

    move-result v2

    if-nez v2, :cond_1

    .line 539
    :cond_0
    :goto_0
    return-object v1

    .line 535
    :cond_1
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdTemplateId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 536
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdTemplateId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 537
    .local v0, "template":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getTemplate()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 539
    .end local v0    # "template":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getPhoneTemplate()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getTabletTemplate()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method getPubAdSystem(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/lang/Integer;
    .locals 1
    .param p1, "settings"    # Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .prologue
    .line 482
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getPubSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 483
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 485
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getPubSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdSystem()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public load(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lorg/codehaus/jackson/node/ObjectNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    sget-object v0, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v0}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v10

    .line 169
    .local v10, "jsonStore":Lorg/codehaus/jackson/node/ObjectNode;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .line 170
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getApplicationFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 171
    .local v2, "applicationFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Application;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getFormFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 172
    .local v3, "formFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Form;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getSectionFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 173
    .local v4, "sectionFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Section;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 174
    .local v5, "postFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Post;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .line 176
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getIdToAdTemplateMapFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 177
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    .line 175
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 178
    .local v6, "idToAdTemplatesFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getUserRolesFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 179
    .local v7, "userRolesFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$RoleList;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .line 180
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 181
    .local v8, "articleTemplateFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .line 182
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getUseLegacyLayoutFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 186
    .local v9, "useLegacyLayoutFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/Boolean;>;"
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object v3, v0, v1

    const/4 v1, 0x2

    aput-object v4, v0, v1

    const/4 v1, 0x3

    aput-object v5, v0, v1

    const/4 v1, 0x4

    aput-object v6, v0, v1

    const/4 v1, 0x5

    aput-object v7, v0, v1

    const/4 v1, 0x6

    aput-object v8, v0, v1

    const/4 v1, 0x7

    aput-object v9, v0, v1

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->whenAllDone([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v12

    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;

    move-object v1, p0

    move-object v11, p1

    invoke-direct/range {v0 .. v11}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader$1;-><init>(Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lorg/codehaus/jackson/node/ObjectNode;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-static {v12, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method showGoogleSoldAds(Lcom/google/apps/dots/proto/client/DotsShared$Application;Ljava/util/Random;)Z
    .locals 2
    .param p1, "app"    # Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .param p2, "random"    # Ljava/util/Random;

    .prologue
    .line 466
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->hasGoogleSoldAds()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 467
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAlwaysShowGoogleSoldAds()Z

    move-result v0

    if-nez v0, :cond_0

    .line 468
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getGoogleSoldAds()Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->getPercent()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-le v0, v1, :cond_1

    .line 472
    invoke-virtual {p2}, Ljava/util/Random;->nextFloat()F

    move-result v0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getGoogleSoldAds()Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->getPercent()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
