.class Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$1;
.super Ljava/lang/Object;
.source "EditableLibraryFragment.java"

# interfaces
.implements Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public commitEditOperation(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
    .locals 1
    .param p1, "groupList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "operation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->commitNewsEdit(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    .line 73
    return-void
.end method

.method public onOperationDisallowed(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
    .locals 1
    .param p1, "groupList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "operation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->onNewsOperationDisallowed(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    .line 78
    return-void
.end method
