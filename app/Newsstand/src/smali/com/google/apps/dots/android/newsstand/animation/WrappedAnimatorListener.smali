.class public Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;
.super Ljava/lang/Object;
.source "WrappedAnimatorListener.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field private final wrappedListener:Landroid/animation/Animator$AnimatorListener;


# direct methods
.method public constructor <init>(Landroid/animation/Animator$AnimatorListener;)V
    .locals 0
    .param p1, "wrappedListener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;->wrappedListener:Landroid/animation/Animator$AnimatorListener;

    .line 14
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;->wrappedListener:Landroid/animation/Animator$AnimatorListener;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;->wrappedListener:Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v0, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 35
    :cond_0
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;->wrappedListener:Landroid/animation/Animator$AnimatorListener;

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;->wrappedListener:Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v0, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 28
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;->wrappedListener:Landroid/animation/Animator$AnimatorListener;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;->wrappedListener:Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v0, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationRepeat(Landroid/animation/Animator;)V

    .line 42
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;->wrappedListener:Landroid/animation/Animator$AnimatorListener;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/animation/WrappedAnimatorListener;->wrappedListener:Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v0, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    .line 21
    :cond_0
    return-void
.end method
