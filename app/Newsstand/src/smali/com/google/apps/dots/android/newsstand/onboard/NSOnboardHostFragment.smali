.class public Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;
.super Lcom/google/android/play/onboard/OnboardHostFragment;
.source "NSOnboardHostFragment.java"


# static fields
.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;

.field public static final STATE_CURATION_SUBS:Ljava/lang/String;

.field public static final STATE_MAGAZINE_OFFERS:Ljava/lang/String;

.field private static final STATE_PREFIX:Ljava/lang/String;

.field public static final STATE_SEQUENCE_TYPE:Ljava/lang/String;

.field private static pageList:Lcom/google/android/libraries/bind/data/DataList;


# instance fields
.field private final lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    const-class v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    .line 52
    const-class v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_PREFIX:Ljava/lang/String;

    .line 56
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_PREFIX:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "sequenceType"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_SEQUENCE_TYPE:Ljava/lang/String;

    .line 57
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_PREFIX:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "curationSubs"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_CURATION_SUBS:Ljava/lang/String;

    .line 58
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_PREFIX:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "magazineOffers"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_MAGAZINE_OFFERS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;-><init>()V

    .line 62
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userless()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPager;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    return-object v0
.end method

.method private addAuthPage(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p1, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 214
    .local v0, "page":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    const-string v2, "auth"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 215
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->onboard_auth_page:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 216
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    return-void
.end method

.method private addTutorialPages(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v3, 0x2

    .line 195
    new-instance v0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;-><init>(Landroid/content/Context;II)V

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->onboard_tutorial_page:I

    .line 196
    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setViewResId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    .line 197
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getAppColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBackgroundColor(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->onboard_tutorial_page_0_title:I

    .line 198
    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setTitleText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->onboard_tutorial_page_0_body:I

    .line 199
    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBodyText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_on_boarding_1_scaleable:I

    .line 200
    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setIconDrawableId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->build()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 195
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    new-instance v0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;-><init>(Landroid/content/Context;II)V

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->onboard_tutorial_page:I

    .line 204
    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setViewResId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    .line 205
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getAppColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBackgroundColor(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->onboard_tutorial_page_1_title:I

    .line 206
    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setTitleText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->onboard_tutorial_page_1_body:I

    .line 207
    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBodyText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_on_boarding_2_scaleable:I

    .line 208
    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setIconDrawableId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    .line 209
    invoke-virtual {v0}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->build()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 203
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    return-void
.end method

.method private showedQuizzes()Z
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_CURATION_SUBS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_MAGAZINE_OFFERS:Ljava/lang/String;

    .line 221
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected exitOnboardFlow()V
    .locals 2

    .prologue
    .line 235
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setShowedTutorial(Z)V

    .line 236
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getHostActivity()Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->onFinishedOnboardFlow()V

    .line 238
    const/4 v0, 0x0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    .line 239
    return-void
.end method

.method public finishOnboardFlow()V
    .locals 1

    .prologue
    .line 226
    invoke-super {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->finishOnboardFlow()V

    .line 227
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->showedQuizzes()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->submitQuizResultsAndCompleteOnboardFlow()V

    .line 232
    :goto_0
    return-void

    .line 230
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->exitOnboardFlow()V

    goto :goto_0
.end method

.method protected getCurationSubscriptions()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_CURATION_SUBS:Ljava/lang/String;

    .line 243
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 244
    .local v0, "subs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;>;"
    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "subs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    return-object v0
.end method

.method protected getHostActivity()Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;

    return-object v0
.end method

.method public getPageList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 3

    .prologue
    .line 177
    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v1, :cond_0

    .line 178
    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    .line 191
    :goto_0
    return-object v1

    .line 181
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getSequenceType()I

    move-result v1

    if-nez v1, :cond_1

    .line 182
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 183
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->addTutorialPages(Ljava/util/List;)V

    .line 184
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->addAuthPage(Ljava/util/List;)V

    .line 185
    new-instance v1, Lcom/google/android/libraries/bind/data/DataList;

    sget v2, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    invoke-direct {v1, v2, v0}, Lcom/google/android/libraries/bind/data/DataList;-><init>(ILjava/util/List;)V

    sput-object v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    .line 191
    .end local v0    # "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :goto_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    goto :goto_0

    .line 188
    :cond_1
    new-instance v1, Lcom/google/android/libraries/bind/data/DataList;

    sget v2, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    invoke-direct {v1, v2}, Lcom/google/android/libraries/bind/data/DataList;-><init>(I)V

    sput-object v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    .line 189
    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->addQuizPagesIfAbsent(Lcom/google/android/libraries/bind/data/DataList;)V

    goto :goto_1
.end method

.method protected getSelectedMagazineOffers()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_MAGAZINE_OFFERS:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 249
    .local v1, "encodedOffers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/play/utils/collections/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 250
    .local v2, "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;>;"
    if-eqz v1, :cond_0

    .line 251
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 253
    .local v0, "encodedOffer":Ljava/lang/String;
    :try_start_0
    new-instance v4, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-direct {v4}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;-><init>()V

    invoke-static {v0, v4}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->decodeBase64(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 254
    :catch_0
    move-exception v4

    goto :goto_0

    .line 259
    .end local v0    # "encodedOffer":Ljava/lang/String;
    :cond_0
    return-object v2
.end method

.method public getSequenceType()I
    .locals 4

    .prologue
    .line 108
    const/4 v1, 0x0

    .line 109
    .local v1, "sequenceType":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 110
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 111
    const-string v2, "arg_sequenceType"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 113
    :cond_0
    return v1
.end method

.method protected getViewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;
    .locals 1

    .prologue
    .line 122
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->viewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 92
    invoke-super {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->onDestroyView()V

    .line 93
    return-void
.end method

.method protected onPageSelected(IZ)V
    .locals 2
    .param p1, "logicalPosition"    # I
    .param p2, "byUser"    # Z

    .prologue
    .line 170
    invoke-super {p0, p1, p2}, Lcom/google/android/play/onboard/OnboardHostFragment;->onPageSelected(IZ)V

    .line 172
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Lcom/google/android/play/onboard/OnboardPager;->restrictFocusToCurrentItem(Ljava/lang/Integer;Z)V

    .line 173
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Lcom/google/android/play/onboard/OnboardHostFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 74
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_SEQUENCE_TYPE:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getSequenceType()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 76
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->onboard_tutorial_background:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 75
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->setBackgroundView(Landroid/view/View;)V

    .line 77
    return-void
.end method

.method protected restoreOnboardState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/google/android/play/onboard/OnboardHostFragment;->restoreOnboardState(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getSequenceType()I

    move-result v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_SEQUENCE_TYPE:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 85
    const/4 v0, 0x0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    .line 87
    :cond_0
    return-void
.end method

.method public setSequenceType(I)V
    .locals 2
    .param p1, "sequenceType"    # I

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getOrCreateArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 103
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "arg_sequenceType"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 104
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->setArguments(Landroid/os/Bundle;)V

    .line 105
    return-void
.end method

.method protected setUpPager()V
    .locals 3

    .prologue
    .line 127
    invoke-super {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->setUpPager()V

    .line 129
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/onboard/OnboardPager;->setPageTransformer(ZLandroid/support/v4/view/ViewPager$PageTransformer;)V

    .line 166
    return-void
.end method

.method protected submitCurationsQuiz()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getCurationSubscriptions()Ljava/util/List;

    move-result-object v1

    .line 325
    .local v1, "curationSubs":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    .line 326
    .local v4, "itemsTotal":I
    const/4 v2, 0x0

    .line 327
    .local v2, "itemsAccepted":I
    const/4 v3, 0x0

    .line 329
    .local v3, "itemsRejected":I
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 330
    .local v6, "subsFutures":Ljava/util/List;, "Ljava/util/List<Lcom/google/common/util/concurrent/ListenableFuture<*>;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 332
    .local v0, "account":Landroid/accounts/Account;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;

    .line 333
    .local v5, "sub":Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
    iget-boolean v8, v5, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->subscribe:Z

    if-eqz v8, :cond_0

    .line 340
    iget-object v8, v5, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-static {v0, v8}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->addSubscriptionNoUi(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 347
    .end local v5    # "sub":Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
    :cond_1
    if-lez v2, :cond_3

    .line 348
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;

    .line 350
    .restart local v5    # "sub":Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
    iget-boolean v8, v5, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->defaultSubscribed:Z

    if-eqz v8, :cond_2

    iget-boolean v8, v5, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->subscribe:Z

    if-nez v8, :cond_2

    .line 351
    iget-object v8, v5, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-static {v0, v8}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->removeSubscriptionNoUi(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 358
    .end local v5    # "sub":Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
    :cond_3
    new-instance v7, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;

    const-string v8, "Onboard-Curations-Quiz"

    invoke-direct {v7, v8, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;-><init>(Ljava/lang/String;III)V

    const/4 v8, 0x0

    .line 359
    invoke-virtual {v7, v8}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->track(Z)V

    .line 361
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList(Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    return-object v7
.end method

.method protected submitMagazineOffersQuiz()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 371
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 372
    .local v0, "account":Landroid/accounts/Account;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getSelectedMagazineOffers()Ljava/util/List;

    move-result-object v5

    .line 373
    .local v5, "selectedOffers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/play/utils/collections/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 374
    .local v2, "futures":Ljava/util/List;, "Ljava/util/List<Lcom/google/common/util/concurrent/ListenableFuture<*>;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 375
    .local v3, "offerSummary":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    const-string v7, "Onboard-Magazines-Offers"

    invoke-static {v0, v3, v7}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->acceptOfferNoUi(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 381
    .end local v3    # "offerSummary":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->rawOffersList()Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->freeMagazineOffersList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 382
    .local v1, "freeOffersList":Lcom/google/android/libraries/bind/data/DataList;
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v4

    .line 385
    .local v4, "offersCount":I
    :goto_1
    new-instance v6, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;

    const-string v7, "Onboard-Magazines-Offers"

    .line 386
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    invoke-direct {v6, v7, v8, v9, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;-><init>(Ljava/lang/String;III)V

    invoke-virtual {v6, v9}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->track(Z)V

    .line 388
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList(Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    return-object v6

    .line 382
    .end local v4    # "offersCount":I
    :cond_1
    const/4 v4, -0x1

    goto :goto_1
.end method

.method protected submitQuizResultsAndCompleteOnboardFlow()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 267
    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->onboard_quiz_interstitial_caption:I

    sget-object v7, Lcom/google/android/play/onboard/InterstitialOverlay;->DEFAULT_ACCENT_COLORS_RES_IDS:[I

    invoke-virtual {p0, v6, v7}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->showPulsatingDotOverlay(I[I)V

    .line 271
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    .line 272
    .local v5, "writeToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v4

    .line 274
    .local v4, "uiToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    const-wide/16 v6, 0x9c4

    .line 275
    invoke-static {v6, v7, v4, v8}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->makeTimerFuture(JLjava/util/concurrent/Executor;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 278
    .local v0, "minimumTimeFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/Object;>;"
    new-array v6, v10, [Lcom/google/common/util/concurrent/ListenableFuture;

    .line 279
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->submitCurationsQuiz()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->submitMagazineOffersQuiz()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 282
    .local v2, "submitFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/util/List<Ljava/lang/Object;>;>;"
    new-instance v6, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$2;

    invoke-direct {v6, p0, v5}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 283
    invoke-static {v2, v6}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 290
    .local v1, "quizCompletionFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;>;"
    invoke-virtual {v5, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->track(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 294
    new-array v6, v10, [Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v1, v6, v8

    aput-object v0, v6, v9

    .line 295
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 297
    .local v3, "uiFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/util/List<Ljava/lang/Object;>;>;"
    new-instance v6, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$3;

    invoke-direct {v6, p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)V

    invoke-virtual {v4, v3, v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 314
    return-void
.end method
