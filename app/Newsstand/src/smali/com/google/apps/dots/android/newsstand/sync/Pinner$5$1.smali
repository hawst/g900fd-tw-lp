.class Lcom/google/apps/dots/android/newsstand/sync/Pinner$5$1;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/SimpleNodeVisitor;
.source "Pinner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;->getCuratedTopicEditions(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;

.field final synthetic val$curatedTopicEditions:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;Ljava/util/Map;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;

    .prologue
    .line 538
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Pinner$5;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5$1;->val$curatedTopicEditions:Ljava/util/Map;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/SimpleNodeVisitor;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 7
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    const/4 v6, 0x0

    .line 541
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_0

    iget-object v4, p2, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v4, v4

    if-lez v4, :cond_0

    .line 543
    iget-object v4, p2, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v0, v4, v6

    .line 544
    .local v0, "appNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 546
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromSummaries(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 547
    .local v1, "curatedTopicEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-eqz v1, :cond_0

    .line 549
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getLeadCurationClientEntityId()Ljava/lang/String;

    move-result-object v2

    .line 550
    .local v2, "leadCurationClientEntityId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5$1;->val$curatedTopicEditions:Ljava/util/Map;

    .line 551
    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 552
    .local v3, "matchingCurations":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    if-nez v3, :cond_1

    .line 553
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$5$1;->val$curatedTopicEditions:Ljava/util/Map;

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v1, v5, v6

    .line 554
    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v5

    .line 553
    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "matchingCurations":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    check-cast v3, Ljava/util/List;

    .line 561
    .end local v0    # "appNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .end local v1    # "curatedTopicEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v2    # "leadCurationClientEntityId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 556
    .restart local v0    # "appNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .restart local v1    # "curatedTopicEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .restart local v2    # "leadCurationClientEntityId":Ljava/lang/String;
    .restart local v3    # "matchingCurations":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    :cond_1
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
