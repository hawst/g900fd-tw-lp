.class Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;
.super Ljava/lang/Object;
.source "NewsArticleParentLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 365
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 368
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$400(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isLoadComplete()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 369
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$400(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculateVerticalScrollRange()I

    move-result v2

    .line 370
    .local v2, "scrollRange":I
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->lastKnownScrollRange:I
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$700(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)I

    move-result v5

    if-eq v2, v5, :cond_3

    .line 371
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->lastKnownScrollRange:I
    invoke-static {v5, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$702(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;I)I

    .line 372
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->lastScrollRangeUpdateEpoch:J
    invoke-static {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$802(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;J)J

    .line 375
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailShown:Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$900(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 376
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->updateArticleTailPosition(Z)Z
    invoke-static {v5, v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$1000(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;Z)Z

    .line 391
    .end local v2    # "scrollRange":I
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailShown:Z
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$900(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->lastScrollRangeUpdateEpoch:J
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$800(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    .line 392
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->lastScrollRangeUpdateEpoch:J
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$800(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 393
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->SCROLL_RANGE_TOTAL_POLLING_TIME_MS:J
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$1300()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    .line 394
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$1500(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->pollScrollRangeRunnable:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$1400(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Ljava/lang/Runnable;

    move-result-object v5

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 396
    :cond_2
    return-void

    .line 378
    .restart local v2    # "scrollRange":I
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailShown:Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$900(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 381
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 382
    .local v0, "now":J
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .line 383
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->lastScrollRangeUpdateEpoch:J
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$800(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)J

    move-result-wide v6

    sub-long v6, v0, v6

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->SCROLL_RANGE_UPDATE_THRESHOLD_MS:J
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$1100()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-lez v5, :cond_4

    const/4 v3, 0x1

    .line 384
    .local v3, "scrollRangeIsSettled":Z
    :goto_1
    if-eqz v3, :cond_0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->updateArticleTailPosition(Z)Z
    invoke-static {v5, v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$1000(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 385
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->showArticleTail()V
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V

    goto :goto_0

    .end local v3    # "scrollRangeIsSettled":Z
    :cond_4
    move v3, v4

    .line 383
    goto :goto_1
.end method
