.class public final enum Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;
.super Ljava/lang/Enum;
.source "OfferActionEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OfferActionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

.field public static final enum ACCEPTED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

.field public static final enum DECLINED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

.field public static final enum EDITION_CLICKED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    const-string v1, "ACCEPTED"

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->ACCEPTED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    const-string v1, "DECLINED"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->DECLINED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    const-string v1, "EDITION_CLICKED"

    invoke-direct {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->EDITION_CLICKED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->ACCEPTED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->DECLINED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->EDITION_CLICKED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    return-object v0
.end method
