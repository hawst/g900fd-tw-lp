.class public final Lcom/google/apps/dots/android/newsstand/util/BytePool;
.super Ljava/lang/Object;
.source "BytePool.java"


# instance fields
.field private final lock:Ljava/lang/Object;

.field public final maxSize:I

.field private final minPooledSize:I

.field private final pool:Lcom/google/android/libraries/bind/collections/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/bind/collections/RingBuffer",
            "<[B>;"
        }
    .end annotation
.end field

.field private totalSize:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxSize"    # I

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-static {}, Lcom/google/android/libraries/bind/collections/RingBuffer;->create()Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 15
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->lock:Ljava/lang/Object;

    .line 18
    const/16 v0, 0x20

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->minPooledSize:I

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->totalSize:I

    .line 26
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->maxSize:I

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;)V
    .locals 2
    .param p1, "util"    # Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .prologue
    .line 22
    const/16 v0, 0x400

    const/16 v1, 0xa

    invoke-virtual {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->scaleForMemoryClass(II)I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/util/BytePool;-><init>(I)V

    .line 23
    return-void
.end method

.method private static arraySizeFor(I)I
    .locals 2
    .param p0, "size"    # I

    .prologue
    .line 45
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->nextPowerOf2(I)I

    move-result v0

    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->nextMultipleOf256k(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method private static nextMultipleOf256k(I)I
    .locals 2
    .param p0, "size"    # I

    .prologue
    .line 40
    add-int/lit8 v0, p0, -0x1

    const v1, 0x3ffff

    or-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private static nextPowerOf2(I)I
    .locals 1
    .param p0, "size"    # I

    .prologue
    .line 30
    add-int/lit8 p0, p0, -0x1

    .line 31
    ushr-int/lit8 v0, p0, 0x1

    or-int/2addr p0, v0

    .line 32
    ushr-int/lit8 v0, p0, 0x2

    or-int/2addr p0, v0

    .line 33
    ushr-int/lit8 v0, p0, 0x4

    or-int/2addr p0, v0

    .line 34
    ushr-int/lit8 v0, p0, 0x8

    or-int/2addr p0, v0

    .line 35
    ushr-int/lit8 v0, p0, 0x10

    or-int/2addr p0, v0

    .line 36
    add-int/lit8 v0, p0, 0x1

    return v0
.end method

.method private trimToSize(I)V
    .locals 2
    .param p1, "targetSize"    # I

    .prologue
    .line 99
    :goto_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->totalSize:I

    if-le v0, p1, :cond_0

    .line 100
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->totalSize:I

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v0, v0

    sub-int v0, v1, v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->totalSize:I

    goto :goto_0

    .line 102
    :cond_0
    return-void
.end method


# virtual methods
.method public acquire(I)[B
    .locals 8
    .param p1, "size"    # I

    .prologue
    .line 52
    const/16 v5, 0x20

    if-ge p1, v5, :cond_0

    .line 53
    new-array v3, p1, [B

    .line 75
    :goto_0
    return-object v3

    .line 55
    :cond_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->lock:Ljava/lang/Object;

    monitor-enter v6

    .line 56
    const/4 v3, 0x0

    .line 57
    .local v3, "result":[B
    const/4 v4, 0x0

    .line 58
    .local v4, "resultPosition":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    :try_start_0
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v5}, Lcom/google/android/libraries/bind/collections/RingBuffer;->size()I

    move-result v5

    if-ge v2, v5, :cond_3

    .line 59
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v5, v2}, Lcom/google/android/libraries/bind/collections/RingBuffer;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 60
    .local v1, "buf":[B
    array-length v5, v1

    if-lt v5, p1, :cond_2

    if-eqz v3, :cond_1

    array-length v5, v1

    array-length v7, v3

    if-ge v5, v7, :cond_2

    .line 61
    :cond_1
    move-object v3, v1

    .line 62
    move v4, v2

    .line 58
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 65
    .end local v1    # "buf":[B
    :cond_3
    if-eqz v3, :cond_4

    .line 66
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v5, v4}, Lcom/google/android/libraries/bind/collections/RingBuffer;->remove(I)Ljava/lang/Object;

    .line 67
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->totalSize:I

    array-length v7, v3

    sub-int/2addr v5, v7

    iput v5, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->totalSize:I

    .line 68
    monitor-exit v6

    goto :goto_0

    .line 70
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_4
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->arraySizeFor(I)I

    move-result v0

    .line 72
    .local v0, "allocSize":I
    const v5, 0x8000

    if-lt v0, v5, :cond_5

    .line 73
    sget-object v5, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    invoke-virtual {v5}, Lcom/google/android/libraries/bind/async/JankLock;->blockUntilJankPermitted()V

    .line 75
    :cond_5
    new-array v3, v0, [B

    goto :goto_0
.end method

.method public release([B)V
    .locals 3
    .param p1, "bytes"    # [B

    .prologue
    .line 82
    array-length v0, p1

    const/16 v1, 0x20

    if-ge v0, v1, :cond_0

    .line 90
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->pool:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->addFirst(Ljava/lang/Object;)V

    .line 87
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->totalSize:I

    array-length v2, p1

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->totalSize:I

    .line 88
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->maxSize:I

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->trimToSize(I)V

    .line 89
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public trim(F)V
    .locals 2
    .param p1, "fraction"    # F

    .prologue
    .line 93
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 94
    :try_start_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/util/BytePool;->totalSize:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->trimToSize(I)V

    .line 95
    monitor-exit v1

    .line 96
    return-void

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
