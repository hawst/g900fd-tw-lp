.class public abstract Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;
.super Ljava/lang/Object;
.source "RetryWithGC.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TResult:",
        "Ljava/lang/Object;",
        "TException:",
        "Ljava/lang/Throwable;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;, "Lcom/google/apps/dots/android/newsstand/util/RetryWithGC<TTResult;TTException;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected fail()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTResult;"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;, "Lcom/google/apps/dots/android/newsstand/util/RetryWithGC<TTResult;TTException;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onOom(Ljava/lang/OutOfMemoryError;Z)V
    .locals 0
    .param p1, "error"    # Ljava/lang/OutOfMemoryError;
    .param p2, "isPostGC"    # Z

    .prologue
    .line 41
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;, "Lcom/google/apps/dots/android/newsstand/util/RetryWithGC<TTResult;TTException;>;"
    return-void
.end method

.method public run()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTResult;^TTException;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;, "Lcom/google/apps/dots/android/newsstand/util/RetryWithGC<TTResult;TTException;>;"
    const/4 v0, 0x0

    .line 19
    .local v0, "attemptedGC":Z
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;->work()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 25
    :goto_1
    return-object v2

    .line 20
    :catch_0
    move-exception v1

    .line 21
    .local v1, "error":Ljava/lang/OutOfMemoryError;
    invoke-virtual {p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;->onOom(Ljava/lang/OutOfMemoryError;Z)V

    .line 22
    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/NSDepend;->trimCaches(F)V

    .line 23
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 24
    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;->fail()Ljava/lang/Object;

    move-result-object v2

    goto :goto_1

    .line 27
    :cond_0
    const/4 v0, 0x1

    .line 29
    goto :goto_0
.end method

.method protected abstract work()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTResult;^TTException;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation
.end method
