.class Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;
.super Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;
.source "MyLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->setupPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;Landroid/support/v4/view/NSViewPager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;
    .param p2, "viewPager"    # Landroid/support/v4/view/NSViewPager;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;-><init>(Landroid/support/v4/view/NSViewPager;)V

    return-void
.end method


# virtual methods
.method public onPageSelected(IZ)V
    .locals 4
    .param p1, "visualPosition"    # I
    .param p2, "userDriven"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 187
    if-eqz p2, :cond_1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->isChangingState()Z
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$500(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 188
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-static {v2, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v1

    .line 189
    .local v1, "logicalPosition":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .line 190
    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    sget v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DK_LIBRARY_PAGE:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 191
    .local v0, "libraryPage":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    invoke-direct {v3, v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)V

    invoke-virtual {v2, v3, p2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 192
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    .line 193
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$000(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/view/NSViewPager;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 196
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->access$400(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 198
    .end local v0    # "libraryPage":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    .end local v1    # "logicalPosition":I
    :cond_1
    return-void
.end method
