.class Lcom/google/apps/dots/android/newsstand/async/futures/Async$12$1;
.super Ljava/lang/Object;
.source "Async.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;

    .prologue
    .line 394
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 397
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;->val$returnVal:Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

    new-instance v1, Ljava/util/concurrent/TimeoutException;

    const-string v2, "Timeout of %d ms exceeded."

    new-array v3, v8, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;

    iget-wide v6, v5, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;->val$timeoutMs:J

    .line 398
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    .line 397
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->setException(Ljava/lang/Throwable;)Z

    .line 399
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;->val$cancelOnTimeout:Z

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;->val$inputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v8}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 402
    :cond_0
    return-void
.end method
