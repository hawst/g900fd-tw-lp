.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
.super Landroid/webkit/WebView;
.source "WebPartView.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/EventSupport;
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;


# static fields
.field private static final DOTS_NATIVEBODY_REQUEST_COLLECT_SNAP_POINTS:Ljava/lang/String; = "javascript:dots.nativeBody.requestCollectSnapPoints()"

.field private static final INTERACTION_TIMEOUT_MILLIS:J = 0x1f4L

.field private static final JAVASCRIPT_INTERFACE_INJECT:I = 0x2

.field private static final JAVASCRIPT_INTERFACE_RESET:I = 0x1

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final NATURAL_SNAP_POINTS:[I

.field private static final NAV_TO_SCHEME:Ljava/lang/String; = "navto"

.field private static final ONLOADCOMPLETE:I = 0x4

.field private static didSetRenderPriority:Z

.field private static userAgent:Ljava/lang/String;


# instance fields
.field private final activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

.field private final appId:Ljava/lang/String;

.field private currentScale:F

.field private currentScrollY:I

.field private final doubleTapDetector:Landroid/view/GestureDetector;

.field private final eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

.field private final fieldId:Ljava/lang/String;

.field private final handleLocalUrls:Z

.field private final handler:Landroid/os/Handler;

.field private hasScrolledInternally:Z

.field private httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

.field private final interactionTimer:Lcom/google/android/libraries/bind/async/DelayedRunnable;

.field private isDestroyed:Z

.field private isInTouchTrace:Z

.field private isLoadComplete:Z

.field private isLoadStarted:Z

.field private final isOnlyChildOfRoot:Z

.field private javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

.field private lastOverscrollCompatTouchX:F

.field private lastOverscrollCompatTouchY:F

.field private final localBaseUri:Landroid/net/Uri;

.field private motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

.field private final nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

.field private final nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

.field private final postId:Ljava/lang/String;

.field private final qualifiedMainResourceUri:Landroid/net/Uri;

.field private recentInteraction:Z

.field private reloadedWithoutWideViewportCount:I

.field private reportedSnapPoints:[I

.field private final scale:F

.field private scroller:Landroid/widget/Scroller;

.field private final sectionId:Ljava/lang/String;

.field private touchEventDefaultPrevented:Z

.field private final unscaledHeight:I

.field private final unscaledWidth:I

.field private wasDoubleTap:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 59
    const/16 v0, 0x12

    new-array v0, v0, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->NATURAL_SNAP_POINTS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;ZLcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;Z)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;
    .param p3, "appId"    # Ljava/lang/String;
    .param p4, "sectionId"    # Ljava/lang/String;
    .param p5, "postId"    # Ljava/lang/String;
    .param p6, "fieldId"    # Ljava/lang/String;
    .param p7, "webPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .param p8, "isOnlyChildOfRoot"    # Z
    .param p9, "inlineFrame"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    .param p10, "handleLocalUrls"    # Z

    .prologue
    .line 115
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 68
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    .line 70
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->handler:Landroid/os/Handler;

    .line 116
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .line 117
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->appId:Ljava/lang/String;

    .line 118
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->sectionId:Ljava/lang/String;

    .line 119
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->postId:Ljava/lang/String;

    .line 120
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->fieldId:Ljava/lang/String;

    .line 121
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->handleLocalUrls:Z

    .line 122
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$1;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;Landroid/view/View;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;ZZ)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

    .line 137
    invoke-interface {p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getLetterboxScale()F

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scale:F

    .line 138
    invoke-virtual/range {p7 .. p7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getLayoutDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->getLocation()Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->unscaledWidth:I

    .line 139
    invoke-virtual/range {p7 .. p7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getLayoutDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$LayoutDetails;->getLocation()Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->unscaledHeight:I

    .line 140
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->currentScale:F

    .line 141
    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isOnlyChildOfRoot:Z

    .line 142
    invoke-interface {p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    .line 145
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->httpContentService()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;

    move-result-object v9

    .line 146
    .local v9, "httpContentService":Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;
    invoke-interface {v9}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;->bind()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    .line 148
    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    move-object v1, p1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    .line 151
    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;->getContentBaseUri()Landroid/net/Uri;

    move-result-object v2

    :goto_0
    invoke-direct {v3, v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;Landroid/net/Uri;)V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .line 153
    invoke-virtual/range {p9 .. p9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->getExternalResourceUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 155
    invoke-static/range {p5 .. p5}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 156
    const-string v1, "%s/%s/%s/%s/%s/"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "internal"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    aput-object p5, v2, v3

    const/4 v3, 0x4

    .line 161
    invoke-static/range {p6 .. p6}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 156
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 167
    .local v10, "innerPath":Ljava/lang/String;
    :goto_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    .line 168
    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;->getWebDataBaseUri()Landroid/net/Uri;

    move-result-object v7

    .line 170
    .local v7, "baseUri":Landroid/net/Uri;
    :goto_2
    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 171
    invoke-virtual {v1, v10}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    .line 172
    .local v8, "baseUriBuilder":Landroid/net/Uri$Builder;
    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->localBaseUri:Landroid/net/Uri;

    .line 173
    invoke-virtual/range {p9 .. p9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->getMainResourceUri()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->qualifyWithAuthority(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->qualifiedMainResourceUri:Landroid/net/Uri;

    .line 179
    .end local v7    # "baseUri":Landroid/net/Uri;
    .end local v8    # "baseUriBuilder":Landroid/net/Uri$Builder;
    .end local v10    # "innerPath":Ljava/lang/String;
    :goto_3
    new-instance v1, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-direct {v1, p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    .line 180
    invoke-virtual/range {p7 .. p7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getWebDetails()Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$WebDetails;->getScrollType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 182
    new-instance v1, Landroid/widget/Scroller;

    invoke-direct {v1, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    .line 185
    :cond_0
    new-instance v1, Landroid/view/GestureDetector;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)V

    invoke-direct {v1, p1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->doubleTapDetector:Landroid/view/GestureDetector;

    .line 193
    new-instance v1, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    invoke-interface {p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getAsyncToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->handler:Landroid/os/Handler;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$3;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->interactionTimer:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 199
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->initWebSettings()V

    .line 200
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->initWebViewClient()V

    .line 201
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->initWebChromeClient()V

    .line 202
    return-void

    .line 151
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentUri()Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_0

    .line 163
    :cond_2
    const-string v1, "%s/%s/%s/"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "external"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    .line 164
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "innerPath":Ljava/lang/String;
    goto :goto_1

    .line 168
    :cond_3
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/WebDataContentProvider;->contentUri()Landroid/net/Uri;

    move-result-object v7

    goto :goto_2

    .line 175
    .end local v10    # "innerPath":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->localBaseUri:Landroid/net/Uri;

    .line 176
    invoke-virtual/range {p9 .. p9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->getExternalResourceUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->qualifiedMainResourceUri:Landroid/net/Uri;

    goto :goto_3
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->postMethod(I)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->wasDoubleTap:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->qualifiedMainResourceUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->localBaseUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->handleLocalUrls:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->fieldId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->postId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->sectionId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->appId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->recentInteraction:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->recentInteraction:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reloadedWithoutWideViewportCount:I

    return v0
.end method

.method static synthetic access$308(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reloadedWithoutWideViewportCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reloadedWithoutWideViewportCount:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->currentScale:F

    return v0
.end method

.method static synthetic access$402(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    .param p1, "x1"    # F

    .prologue
    .line 51
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->currentScale:F

    return p1
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeHorizontalScrollRange()I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->loadBaseHtml()V

    return-void
.end method

.method static synthetic access$702(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isLoadComplete:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    .param p1, "x1"    # [I

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reportedSnapPoints:[I

    return-object p1
.end method

.method private getHorizontalScrollMax()I
    .locals 2

    .prologue
    .line 811
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeHorizontalScrollRange()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeHorizontalScrollExtent()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private getHorizontalScrollOffset()I
    .locals 1

    .prologue
    .line 819
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getScrollX()I

    move-result v0

    goto :goto_0
.end method

.method private getVerticalScrollMax()I
    .locals 2

    .prologue
    .line 815
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeVerticalScrollRange()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeVerticalScrollExtent()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private getVerticalScrollOffset()I
    .locals 1

    .prologue
    .line 823
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getScrollY()I

    move-result v0

    goto :goto_0
.end method

.method private initWebChromeClient()V
    .locals 2

    .prologue
    .line 376
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$6;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$6;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 383
    return-void
.end method

.method private initWebViewClient()V
    .locals 1

    .prologue
    .line 294
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$5;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 373
    return-void
.end method

.method private loadBaseHtml()V
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->qualifiedMainResourceUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isDestroyed:Z

    if-nez v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->qualifiedMainResourceUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->loadUrl(Ljava/lang/String;)V

    .line 389
    :cond_0
    return-void
.end method

.method private postMethod(I)V
    .locals 2
    .param p1, "methods"    # I

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getAsyncToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;I)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 265
    return-void
.end method

.method private qualifyWithAuthority(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p1, "unqualified"    # Ljava/lang/String;

    .prologue
    .line 220
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 221
    .local v0, "unqualifiedUri":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->isRelative()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 222
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->localBaseUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 223
    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 224
    invoke-virtual {v0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 225
    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 226
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 232
    .end local v0    # "unqualifiedUri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-object v0

    .line 230
    :catch_0
    move-exception v1

    .line 232
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private scrollToX(I)V
    .locals 1
    .param p1, "x"    # I

    .prologue
    .line 740
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    if-eqz v0, :cond_0

    .line 741
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v0, p1}, Landroid/widget/Scroller;->setFinalX(I)V

    .line 745
    :goto_0
    return-void

    .line 743
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getScrollY()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scrollTo(II)V

    goto :goto_0
.end method

.method private scrollToY(I)V
    .locals 1
    .param p1, "y"    # I

    .prologue
    .line 748
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    if-eqz v0, :cond_0

    .line 749
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v0, p1}, Landroid/widget/Scroller;->setFinalY(I)V

    .line 753
    :goto_0
    return-void

    .line 751
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getScrollX()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scrollTo(II)V

    goto :goto_0
.end method

.method private setUserAgent(Landroid/webkit/WebSettings;)V
    .locals 5
    .param p1, "settings"    # Landroid/webkit/WebSettings;

    .prologue
    .line 211
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->userAgent:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 212
    invoke-virtual {p1}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 213
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->getVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " GooglePlayMagazines/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->userAgent:Ljava/lang/String;

    .line 215
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->userAgent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 216
    return-void
.end method

.method private snapToPageInDirection(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)Z
    .locals 38
    .param p1, "flingDirection"    # Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    .prologue
    .line 468
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeHorizontalScrollRange()I

    move-result v13

    .line 469
    .local v13, "contentWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeVerticalScrollRange()I

    move-result v12

    .line 470
    .local v12, "contentHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeHorizontalScrollExtent()I

    move-result v34

    .line 471
    .local v34, "windowWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeVerticalScrollExtent()I

    move-result v33

    .line 472
    .local v33, "windowHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getScrollX()I

    move-result v23

    .line 473
    .local v23, "scrollX":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getScrollY()I

    move-result v24

    .line 479
    .local v24, "scrollY":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isOnlyChildOfRoot:Z

    move/from16 v35, v0

    if-eqz v35, :cond_6

    .line 480
    sget-object v35, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->UP:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_0

    sget-object v35, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->DOWN:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->hasScrolledInternally:Z

    move/from16 v35, v0

    if-eqz v35, :cond_5

    :cond_0
    const/16 v17, 0x1

    .line 490
    .local v17, "forbidOverscroll":Z
    :goto_0
    if-nez v17, :cond_a

    sget-object v35, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->LEFT:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_1

    add-int v35, v23, v34

    move/from16 v0, v35

    if-ge v0, v13, :cond_4

    :cond_1
    sget-object v35, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->RIGHT:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_2

    if-lez v23, :cond_4

    :cond_2
    sget-object v35, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->UP:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_3

    add-int v35, v24, v33

    move/from16 v0, v35

    if-ge v0, v12, :cond_4

    :cond_3
    sget-object v35, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->DOWN:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_a

    if-gtz v24, :cond_a

    .line 496
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->onUnhandledFling(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)V

    .line 497
    const/16 v35, 0x1

    .line 585
    :goto_1
    return v35

    .line 480
    .end local v17    # "forbidOverscroll":Z
    :cond_5
    const/16 v17, 0x0

    goto :goto_0

    .line 484
    :cond_6
    move/from16 v0, v34

    if-le v13, v0, :cond_7

    sget-object v35, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->LEFT:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_8

    sget-object v35, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->RIGHT:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_8

    :cond_7
    move/from16 v0, v33

    if-le v12, v0, :cond_9

    sget-object v35, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->UP:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_8

    sget-object v35, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->DOWN:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_9

    :cond_8
    const/16 v17, 0x1

    .restart local v17    # "forbidOverscroll":Z
    :goto_2
    goto :goto_0

    .end local v17    # "forbidOverscroll":Z
    :cond_9
    const/16 v17, 0x0

    goto :goto_2

    .line 499
    .restart local v17    # "forbidOverscroll":Z
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    move-object/from16 v35, v0

    if-nez v35, :cond_b

    .line 501
    const/16 v35, 0x0

    goto :goto_1

    .line 506
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    move-object/from16 v35, v0

    const/16 v36, 0x1

    invoke-virtual/range {v35 .. v36}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 509
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->unscaledWidth:I

    move/from16 v35, v0

    if-eqz v35, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->unscaledHeight:I

    move/from16 v35, v0

    if-eqz v35, :cond_f

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v35, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->unscaledWidth:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    div-float v35, v35, v36

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->unscaledHeight:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    mul-float v5, v35, v36

    .line 513
    .local v5, "adjustedWindowHeight":F
    :goto_3
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v36, v0

    div-float v35, v35, v36

    invoke-static/range {v35 .. v35}, Ljava/lang/Math;->round(F)I

    move-result v20

    .line 514
    .local v20, "pageX":I
    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v35, v0

    div-float v35, v35, v5

    invoke-static/range {v35 .. v35}, Ljava/lang/Math;->round(F)I

    move-result v21

    .line 517
    .local v21, "pageY":I
    const/16 v31, 0x0

    .line 518
    .local v31, "vx":F
    const/16 v32, 0x0

    .line 519
    .local v32, "vy":F
    sget-object v35, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$9;->$SwitchMap$com$google$apps$dots$android$newsstand$util$MotionHelper$FlingDirection:[I

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->ordinal()I

    move-result v36

    aget v35, v35, v36

    packed-switch v35, :pswitch_data_0

    .line 527
    :goto_4
    const v6, 0x7f7fffff    # Float.MAX_VALUE

    .line 528
    .local v6, "bestProjectedDistance":F
    const/4 v7, 0x0

    .line 529
    .local v7, "bestX":I
    const/4 v8, 0x0

    .line 531
    .local v8, "bestY":I
    const v9, 0x7f7fffff    # Float.MAX_VALUE

    .line 532
    .local v9, "closestDistance":F
    const/4 v10, 0x0

    .line 533
    .local v10, "closestX":I
    const/4 v11, 0x0

    .line 536
    .local v11, "closestY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reportedSnapPoints:[I

    move-object/from16 v35, v0

    if-eqz v35, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reportedSnapPoints:[I

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    array-length v0, v0

    move/from16 v35, v0

    const/16 v36, 0x2

    move/from16 v0, v35

    move/from16 v1, v36

    if-lt v0, v1, :cond_10

    .line 537
    sget-object v35, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v36, "Using reported snap points"

    const/16 v37, 0x0

    move/from16 v0, v37

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reportedSnapPoints:[I

    move-object/from16 v26, v0

    .line 539
    .local v26, "snapPoints":[I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->currentScale:F

    move/from16 v25, v0

    .line 554
    .local v25, "snapPointScale":F
    :cond_c
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_5
    add-int/lit8 v35, v18, 0x1

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v36, v0

    move/from16 v0, v35

    move/from16 v1, v36

    if-ge v0, v1, :cond_12

    .line 555
    aget v35, v26, v18

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    mul-float v35, v35, v25

    move/from16 v0, v35

    float-to-int v0, v0

    move/from16 v35, v0

    const/16 v36, 0x0

    sub-int v37, v13, v34

    .line 556
    invoke-static/range {v35 .. v37}, Lcom/google/apps/dots/android/newsstand/util/MathUtil;->clamp(III)I

    move-result v29

    .line 557
    .local v29, "targetX":I
    add-int/lit8 v35, v18, 0x1

    aget v35, v26, v35

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    mul-float v35, v35, v25

    move/from16 v0, v35

    float-to-int v0, v0

    move/from16 v35, v0

    const/16 v36, 0x0

    sub-int v37, v12, v33

    invoke-static/range {v35 .. v37}, Lcom/google/apps/dots/android/newsstand/util/MathUtil;->clamp(III)I

    move-result v30

    .line 559
    .local v30, "targetY":I
    sub-int v35, v29, v23

    move/from16 v0, v35

    int-to-float v15, v0

    .line 560
    .local v15, "dx":F
    sub-int v35, v30, v24

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v16, v0

    .line 562
    .local v16, "dy":F
    move/from16 v0, v16

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-static {v15, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->nearAngleDotProduct(FFFF)F

    move-result v22

    .line 563
    .local v22, "projectedDistance":F
    const/16 v35, 0x0

    cmpg-float v35, v35, v22

    if-gez v35, :cond_d

    cmpg-float v35, v22, v6

    if-gez v35, :cond_d

    .line 564
    move/from16 v6, v22

    .line 565
    move/from16 v7, v29

    .line 566
    move/from16 v8, v30

    .line 569
    :cond_d
    mul-float v35, v15, v15

    mul-float v36, v16, v16

    add-float v14, v35, v36

    .line 570
    .local v14, "distance":F
    cmpg-float v35, v14, v9

    if-gez v35, :cond_e

    .line 571
    move v9, v14

    .line 572
    move/from16 v10, v29

    .line 573
    move/from16 v11, v30

    .line 554
    :cond_e
    add-int/lit8 v18, v18, 0x2

    goto :goto_5

    .line 511
    .end local v5    # "adjustedWindowHeight":F
    .end local v6    # "bestProjectedDistance":F
    .end local v7    # "bestX":I
    .end local v8    # "bestY":I
    .end local v9    # "closestDistance":F
    .end local v10    # "closestX":I
    .end local v11    # "closestY":I
    .end local v14    # "distance":F
    .end local v15    # "dx":F
    .end local v16    # "dy":F
    .end local v18    # "i":I
    .end local v20    # "pageX":I
    .end local v21    # "pageY":I
    .end local v22    # "projectedDistance":F
    .end local v25    # "snapPointScale":F
    .end local v26    # "snapPoints":[I
    .end local v29    # "targetX":I
    .end local v30    # "targetY":I
    .end local v31    # "vx":F
    .end local v32    # "vy":F
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeVerticalScrollExtent()I

    move-result v35

    move/from16 v0, v35

    int-to-float v5, v0

    goto/16 :goto_3

    .line 520
    .restart local v5    # "adjustedWindowHeight":F
    .restart local v20    # "pageX":I
    .restart local v21    # "pageY":I
    .restart local v31    # "vx":F
    .restart local v32    # "vy":F
    :pswitch_0
    const/high16 v31, 0x3f800000    # 1.0f

    goto/16 :goto_4

    .line 521
    :pswitch_1
    const/high16 v31, -0x40800000    # -1.0f

    goto/16 :goto_4

    .line 522
    :pswitch_2
    const/high16 v32, 0x3f800000    # 1.0f

    goto/16 :goto_4

    .line 523
    :pswitch_3
    const/high16 v32, -0x40800000    # -1.0f

    goto/16 :goto_4

    .line 541
    .restart local v6    # "bestProjectedDistance":F
    .restart local v7    # "bestX":I
    .restart local v8    # "bestY":I
    .restart local v9    # "closestDistance":F
    .restart local v10    # "closestX":I
    .restart local v11    # "closestY":I
    :cond_10
    sget-object v35, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v36, "Using natural snap points"

    const/16 v37, 0x0

    move/from16 v0, v37

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 542
    sget-object v26, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->NATURAL_SNAP_POINTS:[I

    .line 543
    .restart local v26    # "snapPoints":[I
    const/high16 v25, 0x3f800000    # 1.0f

    .line 544
    .restart local v25    # "snapPointScale":F
    const/16 v27, 0x0

    .line 545
    .local v27, "snapPointsIndex":I
    const/16 v18, -0x1

    .restart local v18    # "i":I
    :goto_6
    const/16 v35, 0x1

    move/from16 v0, v18

    move/from16 v1, v35

    if-gt v0, v1, :cond_c

    .line 546
    add-int v35, v20, v18

    mul-int v35, v35, v34

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    invoke-static/range {v35 .. v35}, Ljava/lang/Math;->round(F)I

    move-result v29

    .line 547
    .restart local v29    # "targetX":I
    const/16 v19, -0x1

    .local v19, "j":I
    move/from16 v28, v27

    .end local v27    # "snapPointsIndex":I
    .local v28, "snapPointsIndex":I
    :goto_7
    const/16 v35, 0x1

    move/from16 v0, v19

    move/from16 v1, v35

    if-gt v0, v1, :cond_11

    .line 548
    add-int v35, v21, v19

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    mul-float v35, v35, v5

    invoke-static/range {v35 .. v35}, Ljava/lang/Math;->round(F)I

    move-result v30

    .line 549
    .restart local v30    # "targetY":I
    add-int/lit8 v27, v28, 0x1

    .end local v28    # "snapPointsIndex":I
    .restart local v27    # "snapPointsIndex":I
    aput v29, v26, v28

    .line 550
    add-int/lit8 v28, v27, 0x1

    .end local v27    # "snapPointsIndex":I
    .restart local v28    # "snapPointsIndex":I
    aput v30, v26, v27

    .line 547
    add-int/lit8 v19, v19, 0x1

    goto :goto_7

    .line 545
    .end local v30    # "targetY":I
    :cond_11
    add-int/lit8 v18, v18, 0x1

    move/from16 v27, v28

    .end local v28    # "snapPointsIndex":I
    .restart local v27    # "snapPointsIndex":I
    goto :goto_6

    .line 577
    .end local v19    # "j":I
    .end local v27    # "snapPointsIndex":I
    .end local v29    # "targetX":I
    :cond_12
    const v35, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v35, v6, v35

    if-nez v35, :cond_13

    .line 578
    move v7, v10

    .line 579
    move v8, v11

    .line 582
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    move-object/from16 v35, v0

    sub-int v36, v7, v23

    sub-int v37, v8, v24

    move-object/from16 v0, v35

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v36

    move/from16 v4, v37

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 584
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->invalidate()V

    .line 585
    sget-object v35, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->NONE:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_14

    const/16 v35, 0x1

    goto/16 :goto_1

    :cond_14
    const/16 v35, 0x0

    goto/16 :goto_1

    .line 519
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public canScrollHorizontally(I)Z
    .locals 4
    .param p1, "direction"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 774
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollMax()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_1

    move v0, v1

    .line 786
    :cond_0
    :goto_0
    return v0

    .line 780
    :cond_1
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isOnlyChildOfRoot:Z

    if-nez v2, :cond_2

    .line 781
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollOffset()I

    move-result v2

    if-gtz v2, :cond_0

    .line 782
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollOffset()I

    move-result v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollMax()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 785
    :cond_2
    if-gez p1, :cond_3

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollOffset()I

    move-result v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 786
    :cond_3
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollOffset()I

    move-result v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollMax()I

    move-result v3

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public canScrollVertically(I)Z
    .locals 4
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 794
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isOnlyChildOfRoot:Z

    if-nez v2, :cond_1

    .line 795
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollOffset()I

    move-result v2

    if-gtz v2, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollOffset()I

    move-result v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollMax()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 799
    :cond_0
    :goto_0
    return v0

    .line 798
    :cond_1
    if-gez p1, :cond_2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollOffset()I

    move-result v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 799
    :cond_2
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollOffset()I

    move-result v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollMax()I

    move-result v3

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 11

    .prologue
    .line 437
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getScrollX()I

    move-result v1

    .line 438
    .local v1, "oldX":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getScrollY()I

    move-result v2

    .line 440
    .local v2, "oldY":I
    invoke-super {p0}, Landroid/webkit/WebView;->computeScroll()V

    .line 441
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    if-eqz v6, :cond_0

    .line 442
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 444
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrX()I

    move-result v4

    .line 445
    .local v4, "x":I
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    move-result v5

    .line 446
    .local v5, "y":I
    invoke-virtual {p0, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scrollTo(II)V

    .line 459
    .end local v4    # "x":I
    .end local v5    # "y":I
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getScrollY()I

    move-result v3

    .line 460
    .local v3, "scrollY":I
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollMax()I

    move-result v0

    .line 461
    .local v0, "maxScrollY":I
    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->currentScrollY:I

    if-eq v3, v6, :cond_1

    if-lez v0, :cond_1

    .line 462
    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->currentScrollY:I

    .line 463
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->onScrolled()V

    .line 465
    :cond_1
    return-void

    .line 448
    .end local v0    # "maxScrollY":I
    .end local v3    # "scrollY":I
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getScrollX()I

    move-result v4

    .line 449
    .restart local v4    # "x":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getScrollY()I

    move-result v5

    .line 450
    .restart local v5    # "y":I
    if-ne v1, v4, :cond_3

    if-eq v2, v5, :cond_0

    .line 453
    :cond_3
    sget-object v6, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Undoing scroll from (%d, %d) to (%d, %d)"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 454
    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scrollTo(II)V

    goto :goto_0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 713
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    .line 714
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    .line 715
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isDestroyed:Z

    .line 716
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    if-eqz v0, :cond_0

    .line 717
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;->unbind()V

    .line 718
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    .line 720
    :cond_0
    invoke-super {p0}, Landroid/webkit/WebView;->destroy()V

    .line 721
    return-void
.end method

.method public executeJavascript(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 724
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isDestroyed:Z

    if-nez v0, :cond_0

    .line 725
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->loadUrl(Ljava/lang/String;)V

    .line 727
    :cond_0
    return-void
.end method

.method public getCurrentPage()I
    .locals 2

    .prologue
    .line 861
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getCurrentPageFraction()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getCurrentPageFraction()F
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 828
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollMax()I

    move-result v0

    .line 829
    .local v0, "horizontalMax":I
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollMax()I

    move-result v1

    .line 830
    .local v1, "verticalMax":I
    if-ltz v0, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Got negative horizontal scroll max"

    invoke-static {v2, v5}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 831
    if-ltz v1, :cond_1

    :goto_1
    const-string v2, "Got negative vertical scroll max"

    invoke-static {v3, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 832
    if-le v0, v1, :cond_2

    .line 833
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollOffset()I

    move-result v2

    int-to-float v2, v2

    int-to-float v3, v0

    div-float/2addr v2, v3

    .line 837
    :goto_2
    return v2

    :cond_0
    move v2, v4

    .line 830
    goto :goto_0

    :cond_1
    move v3, v4

    .line 831
    goto :goto_1

    .line 834
    :cond_2
    if-lez v1, :cond_3

    .line 835
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollOffset()I

    move-result v2

    int-to-float v2, v2

    int-to-float v3, v1

    div-float/2addr v2, v3

    goto :goto_2

    .line 837
    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public getPageCount()I
    .locals 4

    .prologue
    .line 842
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    if-eqz v0, :cond_1

    .line 844
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeHorizontalScrollRange()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeVerticalScrollRange()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 845
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeHorizontalScrollExtent()I

    move-result v0

    if-lez v0, :cond_1

    .line 847
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeHorizontalScrollRange()I

    move-result v0

    int-to-double v0, v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeHorizontalScrollExtent()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 846
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 856
    :goto_0
    return v0

    .line 850
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeVerticalScrollExtent()I

    move-result v0

    if-lez v0, :cond_1

    .line 852
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeVerticalScrollRange()I

    move-result v0

    int-to-double v0, v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeVerticalScrollExtent()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 851
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0

    .line 856
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getScrollOffset()I
    .locals 2

    .prologue
    .line 866
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollOffset()I

    move-result v0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollOffset()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getScrollRange()I
    .locals 2

    .prologue
    .line 871
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollMax()I

    move-result v0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollMax()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected initWebSettings()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 269
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->startWebView()V

    .line 270
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 271
    .local v0, "settings":Landroid/webkit/WebSettings;
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 272
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 273
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 274
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setGeolocationEnabled(Z)V

    .line 275
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->setUserAgent(Landroid/webkit/WebSettings;)V

    .line 279
    const/high16 v1, 0x42c80000    # 100.0f

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->setInitialScale(I)V

    .line 281
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 282
    sget-boolean v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->didSetRenderPriority:Z

    if-nez v1, :cond_0

    .line 284
    sput-boolean v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->didSetRenderPriority:Z

    .line 285
    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    .line 287
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-lt v1, v2, :cond_1

    .line 288
    const-string v1, "use_minimal_memory"

    const-string v2, "false"

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setWebSettingsProperty(Landroid/webkit/WebSettings;Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_1
    return-void
.end method

.method public isLoadComplete()Z
    .locals 1

    .prologue
    .line 398
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isLoadComplete:Z

    return v0
.end method

.method public makeBackgroundTransparent()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 206
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 207
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->setBackgroundColor(I)V

    .line 208
    return-void
.end method

.method public varargs notify(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "optionalArgs"    # [Ljava/lang/Object;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 732
    const-string v0, "pause"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 733
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->onPause()V

    .line 737
    :cond_0
    :goto_0
    return-void

    .line 734
    :cond_1
    const-string v0, "resume"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 735
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->onResume()V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 690
    invoke-super {p0}, Landroid/webkit/WebView;->onAttachedToWindow()V

    .line 691
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->enterWebView()V

    .line 692
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 696
    invoke-super {p0}, Landroid/webkit/WebView;->onDetachedFromWindow()V

    .line 697
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    if-eqz v0, :cond_0

    .line 698
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->requestDetachment()V

    .line 699
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->javascriptInterfaceInjector:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .line 701
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$8;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$8;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 707
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->exitWebView()V

    .line 708
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    invoke-interface {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->onDestroyed(Landroid/view/View;)V

    .line 709
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 590
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->goBack()V

    .line 592
    const/4 v0, 0x1

    .line 594
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 405
    invoke-super/range {p0 .. p5}, Landroid/webkit/WebView;->onLayout(ZIIII)V

    .line 406
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isLoadStarted:Z

    if-nez v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getAsyncToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$7;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView$7;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 413
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isLoadStarted:Z

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;->onLayout()V

    .line 416
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 613
    const/4 v1, 0x0

    .line 614
    .local v1, "eventCanceled":Z
    move-object v3, p1

    .line 616
    .local v3, "superEvent":Landroid/view/MotionEvent;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 617
    .local v0, "action":I
    if-eq v0, v5, :cond_6

    if-eq v0, v7, :cond_6

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->isInTouchTrace:Z

    .line 618
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->recentInteraction:Z

    .line 619
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->interactionTimer:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    const-wide/16 v8, 0x1f4

    invoke-virtual {v4, v8, v9, v5}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 621
    if-nez v0, :cond_0

    .line 622
    iput-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->touchEventDefaultPrevented:Z

    .line 623
    iput-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->wasDoubleTap:Z

    .line 624
    iput-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->hasScrolledInternally:Z

    .line 630
    :cond_0
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->wasDoubleTap:Z

    if-nez v4, :cond_5

    .line 631
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->doubleTapDetector:Landroid/view/GestureDetector;

    invoke-virtual {v4, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 632
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->wasDoubleTap:Z

    if-eqz v4, :cond_1

    .line 633
    const/4 v1, 0x1

    .line 634
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object p1

    .line 635
    const/4 v0, 0x3

    .line 636
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 637
    move-object v3, p1

    .line 640
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->doubleTapDetector:Landroid/view/GestureDetector;

    invoke-virtual {v4, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 649
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    if-eqz v4, :cond_2

    .line 650
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {v4, p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->onStartTouchEvent(Landroid/view/MotionEvent;)V

    .line 652
    :cond_2
    packed-switch v0, :pswitch_data_0

    .line 675
    :cond_3
    :goto_1
    :pswitch_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    if-eqz v4, :cond_4

    .line 676
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {v4, p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->onEndTouchEvent(Landroid/view/MotionEvent;)V

    .line 679
    :cond_4
    invoke-super {p0, v3}, Landroid/webkit/WebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 681
    if-eqz v1, :cond_5

    .line 682
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    .line 685
    :cond_5
    return v5

    :cond_6
    move v4, v6

    .line 617
    goto :goto_0

    .line 654
    :pswitch_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    if-eqz v4, :cond_3

    .line 656
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v4, v5}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 657
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getFinalX()I

    move-result v4

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getFinalY()I

    move-result v6

    invoke-virtual {p0, v4, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scrollTo(II)V

    goto :goto_1

    .line 661
    :pswitch_2
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->touchEventDefaultPrevented:Z

    .line 664
    :pswitch_3
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    if-eqz v4, :cond_3

    .line 665
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->touchEventDefaultPrevented:Z

    if-eqz v4, :cond_7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->NONE:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    .line 667
    .local v2, "flingDirection":Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;
    :goto_2
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->snapToPageInDirection(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 668
    const/4 v1, 0x1

    .line 669
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    .line 670
    invoke-virtual {v3, v7}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_1

    .line 665
    .end local v2    # "flingDirection":Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;
    :cond_7
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    .line 666
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->getFlingDirection()Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-result-object v2

    goto :goto_2

    .line 652
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onTransformChanged()V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;->onTransformChanged()V

    .line 394
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 420
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 421
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;->onVisibilityChanged()V

    .line 422
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 426
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onWindowVisibilityChanged(I)V

    .line 427
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;->onWindowVisibilityChanged()V

    .line 428
    return-void
.end method

.method public reportScriptInjected()V
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scroller:Landroid/widget/Scroller;

    if-eqz v0, :cond_0

    .line 603
    const-string v0, "javascript:dots.nativeBody.requestCollectSnapPoints()"

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->executeJavascript(Ljava/lang/String;)V

    .line 605
    :cond_0
    return-void
.end method

.method public reportTouchEventDefaultPrevented()V
    .locals 1

    .prologue
    .line 598
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->touchEventDefaultPrevented:Z

    .line 599
    return-void
.end method

.method public reportUpdatedSnapPoints([I)V
    .locals 0
    .param p1, "snapPoints"    # [I

    .prologue
    .line 608
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->reportedSnapPoints:[I

    .line 609
    return-void
.end method

.method public scrollToEdge(I)V
    .locals 1
    .param p1, "direction"    # I

    .prologue
    .line 804
    if-gez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scrollToX(I)V

    .line 805
    return-void

    .line 804
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getHorizontalScrollMax()I

    move-result v0

    goto :goto_0
.end method

.method public scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
    .locals 4
    .param p1, "pageLocation"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .prologue
    .line 757
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->getPageFraction()Ljava/lang/Float;

    move-result-object v0

    .line 758
    .local v0, "pageFraction":Ljava/lang/Float;
    if-eqz v0, :cond_1

    .line 759
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->getVerticalScrollMax()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scrollToY(I)V

    .line 767
    :cond_0
    :goto_0
    return-void

    .line 761
    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->getPageNumber()Ljava/lang/Integer;

    move-result-object v1

    .line 762
    .local v1, "pageNumber":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 763
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->computeVerticalScrollExtent()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    mul-int/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->scrollToY(I)V

    goto :goto_0
.end method

.method public setContentArea(FFFF)V
    .locals 1
    .param p1, "l"    # F
    .param p2, "t"    # F
    .param p3, "r"    # F
    .param p4, "b"    # F

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->setContentArea(FFFF)V

    .line 433
    return-void
.end method

.method public setEnableGutterTap(Z)V
    .locals 0
    .param p1, "enableGutterTap"    # Z

    .prologue
    .line 877
    return-void
.end method
