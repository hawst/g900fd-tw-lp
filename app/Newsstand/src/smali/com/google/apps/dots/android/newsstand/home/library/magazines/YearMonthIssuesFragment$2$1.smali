.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2$1;
.super Ljava/lang/Object;
.source "YearMonthIssuesFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;->getComparatorForSortingClusters()Ljava/util/Comparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 172
    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2$1;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public compare(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "lhs"    # Ljava/lang/String;
    .param p2, "rhs"    # Ljava/lang/String;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;->val$yearMonthStringMapping:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$2;->val$yearMonthStringMapping:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method
