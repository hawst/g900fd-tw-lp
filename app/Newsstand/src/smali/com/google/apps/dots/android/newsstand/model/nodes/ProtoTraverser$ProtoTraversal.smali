.class public final Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/BaseTraversal;
.source "ProtoTraverser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProtoTraversal"
.end annotation


# instance fields
.field requestedDeletion:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/BaseTraversal;-><init>()V

    return-void
.end method


# virtual methods
.method clearRequest()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;->requestedDeletion:Z

    .line 41
    return-void
.end method

.method public delete()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser$ProtoTraversal;->requestedDeletion:Z

    .line 33
    return-void
.end method
