.class public Lcom/google/apps/dots/android/newsstand/media/VideoItem;
.super Lcom/google/apps/dots/android/newsstand/media/MediaItem;
.source "VideoItem.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V
    .locals 1
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .param p3, "fieldId"    # Ljava/lang/String;
    .param p4, "value"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .prologue
    .line 11
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/media/MediaItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .line 12
    invoke-virtual {p4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 13
    return-void

    .line 12
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
