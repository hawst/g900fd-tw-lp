.class public Lcom/google/apps/dots/android/newsstand/share/ShareUtil;
.super Ljava/lang/Object;
.source "ShareUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/share/ShareUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getShareIntentBuilder(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/share/ShareParams;)Landroid/support/v4/app/ShareCompat$IntentBuilder;
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "params"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    .prologue
    .line 68
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->forParams(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/share/ShareParams;)Lcom/google/apps/dots/android/newsstand/share/ShareMessage;

    move-result-object v0

    .line 69
    .local v0, "shareMessage":Lcom/google/apps/dots/android/newsstand/share/ShareMessage;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->toIntentBuilder()Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v1

    return-object v1
.end method

.method public static getShareParamsForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    const/4 v2, 0x0

    .line 21
    if-eqz p1, :cond_0

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-nez v3, :cond_2

    .line 23
    :cond_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/ShareUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Not ready to build ShareParams yet"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    :cond_1
    :goto_0
    return-object v2

    .line 27
    :cond_2
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 30
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasShortShareUrl()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 31
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getShortShareUrl()Ljava/lang/String;

    move-result-object v1

    .line 36
    .local v1, "url":Ljava/lang/String;
    :goto_1
    if-eqz v1, :cond_1

    .line 37
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->forEdition(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    move-result-object v2

    .line 38
    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->setShortUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    move-result-object v2

    .line 39
    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->setLongUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    move-result-object v2

    .line 40
    invoke-virtual {p1, p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->title(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->setEditionName(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    move-result-object v2

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 41
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->setEditionDescription(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    move-result-object v2

    goto :goto_0

    .line 32
    .end local v1    # "url":Ljava/lang/String;
    :cond_3
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasLongShareUrl()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 33
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getLongShareUrl()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    move-object v1, v2

    goto :goto_1
.end method

.method public static getShareParamsForPost(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p2, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 51
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasShareUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->forPost(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    move-result-object v0

    .line 53
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getShareUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->setShortUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    move-result-object v0

    .line 54
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getShareUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->setLongUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    move-result-object v0

    .line 55
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->setEditionName(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    move-result-object v0

    .line 56
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->setEditionDescription(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    move-result-object v0

    .line 57
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    move-result-object v0

    .line 58
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAbstract()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->setSnippet(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
