.class Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$1;
.super Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;
.source "NewsBridgeResponder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->openOriginalUrl(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

.field final synthetic val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$1;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 2
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 122
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 123
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getExternalPostUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;->start()V

    .line 125
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 119
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder$1;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
