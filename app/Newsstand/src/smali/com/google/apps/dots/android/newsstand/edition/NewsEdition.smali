.class public Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;
.super Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
.source "NewsEdition.java"


# instance fields
.field private isFeed:Ljava/lang/Boolean;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V
    .locals 0
    .param p1, "editionProto"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 25
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 28
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    const/4 v1, 0x3

    .line 29
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setType(I)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;-><init>()V

    .line 30
    invoke-virtual {v1, p1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setNews(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    .line 28
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 31
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getNews()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$NewsEditionInfo;->getAppId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTranslatedEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 2
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 79
    .line 80
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->getAppId()Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->addOrReplaceTargetTranslationLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "translatedAppId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v1

    return-object v1
.end method

.method public isFeed()Z
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    .line 88
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->isFeed:Ljava/lang/Boolean;

    if-nez v4, :cond_2

    .line 89
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->isFeed:Ljava/lang/Boolean;

    .line 90
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->getAppId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->tryParseObjectId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v3

    .line 91
    .local v3, "objectId":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->hasParentId()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->getParentId()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->hasExternalId()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 92
    const/4 v0, 0x0

    .line 95
    .local v0, "externalId":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->getParentId()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->getExternalId()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v2

    .line 96
    .local v2, "externalIdBytes":[B
    if-eqz v2, :cond_0

    .line 97
    new-instance v1, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-direct {v1, v2, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "externalId":Ljava/lang/String;
    .local v1, "externalId":Ljava/lang/String;
    move-object v0, v1

    .line 104
    .end local v1    # "externalId":Ljava/lang/String;
    .end local v2    # "externalIdBytes":[B
    .restart local v0    # "externalId":Ljava/lang/String;
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    const-string v4, "http://"

    .line 105
    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "https://"

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 106
    :cond_1
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->isFeed:Ljava/lang/Boolean;

    .line 110
    .end local v0    # "externalId":Ljava/lang/String;
    .end local v3    # "objectId":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :cond_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->isFeed:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    return v4

    .line 101
    .restart local v0    # "externalId":Ljava/lang/String;
    .restart local v3    # "objectId":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :catch_0
    move-exception v4

    goto :goto_0

    .line 99
    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 42
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public showKeepOnDeviceUi()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public showOnDeviceOnlyUi()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public supportsBookmarking()Z
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->isFeed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public supportsReadStates()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    return v0
.end method

.method public supportsSubscription()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method

.method public syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    .line 48
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->sectionCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;->readStatesUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    .line 47
    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method
