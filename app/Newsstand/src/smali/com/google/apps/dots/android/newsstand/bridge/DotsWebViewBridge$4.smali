.class Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$4;
.super Ljava/lang/Object;
.source "DotsWebViewBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->switchToArticle(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

.field final synthetic val$postId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$4;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$4;->val$postId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$4;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v0, v1, :cond_0

    .line 147
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$4;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$4;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$4;->val$postId:Ljava/lang/String;

    .line 148
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v0

    const/4 v1, 0x1

    .line 149
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->start(Z)V

    .line 156
    :goto_0
    return-void

    .line 152
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$4;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge$4;->val$postId:Ljava/lang/String;

    .line 153
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v0

    .line 154
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->start()V

    goto :goto_0
.end method
