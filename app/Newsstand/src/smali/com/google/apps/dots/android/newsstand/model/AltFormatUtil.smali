.class public Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;
.super Ljava/lang/Object;
.source "AltFormatUtil.java"


# direct methods
.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "x2"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .prologue
    .line 30
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->jumpToAltFormat(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;)V

    return-void
.end method

.method public static getAltFormatToggleA11yString(Z)Ljava/lang/String;
    .locals 1
    .param p0, "forToggleToPrint"    # Z

    .prologue
    .line 196
    if-eqz p0, :cond_0

    .line 197
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->magazines_toggle_to_print_mode:I

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getStringResource(I)Ljava/lang/String;

    move-result-object v0

    .line 199
    :goto_0
    return-object v0

    :cond_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->magazines_toggle_to_lite_mode:I

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getStringResource(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static hasValidAltFormats(Ljava/util/List;Lcom/google/android/libraries/bind/data/DataList;)Z
    .locals 4
    .param p1, "allPostsList"    # Lcom/google/android/libraries/bind/data/DataList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;",
            ">;",
            "Lcom/google/android/libraries/bind/data/DataList;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p0, "altFormats":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;>;"
    const/4 v2, 0x0

    .line 116
    if-nez p0, :cond_0

    move v1, v2

    .line 126
    :goto_0
    return v1

    .line 119
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 121
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getObjectId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    .line 123
    const/4 v1, 0x1

    goto :goto_0

    .line 119
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 126
    goto :goto_0
.end method

.method private static jumpToAltFormat(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;)V
    .locals 6
    .param p0, "magazineReadingFragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "altFormat"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 172
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getType()I

    move-result v0

    if-ne v0, v5, :cond_1

    .line 175
    new-instance v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .line 178
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getObjectId()Ljava/lang/String;

    move-result-object v4

    .line 180
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getFormat()I

    move-result v0

    if-ne v0, v5, :cond_2

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getIndex()I

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 179
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->fromNumber(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v0

    .line 183
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getFormat()I

    move-result v5

    if-ne v5, v2, :cond_0

    move v1, v2

    :cond_0
    invoke-direct {v3, p1, v4, v0, v1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V

    .line 175
    invoke-virtual {p0, v3, v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 186
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 180
    goto :goto_0
.end method

.method public static jumpToAltFormats(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/List;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "postsList"    # Lcom/google/android/libraries/bind/data/DataList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            "Lcom/google/android/libraries/bind/data/DataList;",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "altFormats":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;>;"
    const/4 v10, 0x2

    const/4 v9, 0x0

    .line 72
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    .line 77
    invoke-interface {p4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    invoke-static {p1, p2, v7}, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->jumpToAltFormat(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;)V

    goto :goto_0

    .line 79
    :cond_2
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 80
    .local v3, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 81
    .local v4, "jumpToAltFormats":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_5

    .line 82
    invoke-interface {p4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 83
    .local v0, "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getFormat()I

    move-result v7

    if-ne v7, v10, :cond_3

    .line 85
    invoke-static {p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->jumpToAltFormat(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;)V

    goto :goto_0

    .line 88
    :cond_3
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getType()I

    move-result v7

    if-ne v7, v10, :cond_4

    .line 89
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getObjectId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v5

    .line 90
    .local v5, "position":I
    if-ltz v5, :cond_4

    invoke-virtual {p3}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v7

    if-ge v5, v7, :cond_4

    .line 91
    invoke-virtual {p3, v5}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v7

    sget v8, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {v7, v8}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 92
    .local v6, "ps":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    if-eqz v6, :cond_4

    .line 93
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    .end local v5    # "position":I
    .end local v6    # "ps":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 99
    .end local v0    # "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    :cond_5
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 100
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 101
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    sget v7, Lcom/google/android/apps/newsstanddev/R$string;->magazines_select_text_article_dialog_title:I

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 102
    new-array v7, v9, [Ljava/lang/CharSequence;

    invoke-interface {v3, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/CharSequence;

    new-instance v8, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil$1;

    invoke-direct {v8, v4, p1, p2}, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil$1;-><init>(Ljava/util/List;Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {v1, v7, v8}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 110
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0
.end method
