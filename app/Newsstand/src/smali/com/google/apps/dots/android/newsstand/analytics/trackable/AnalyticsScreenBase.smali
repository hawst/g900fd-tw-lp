.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
.source "AnalyticsScreenBase.java"


# instance fields
.field private final publisherTrackingId:Ljava/lang/String;

.field private final screenName:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "publisherTrackingId"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;-><init>()V

    .line 15
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 16
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;->screenName:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;->publisherTrackingId:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 0
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 24
    return-object p1
.end method

.method protected fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 0
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "publisherEvent"    # Z

    .prologue
    .line 31
    return-object p1
.end method

.method protected getPublisherTrackingId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;->publisherTrackingId:Ljava/lang/String;

    return-object v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;->screenName:Ljava/lang/String;

    return-object v0
.end method
