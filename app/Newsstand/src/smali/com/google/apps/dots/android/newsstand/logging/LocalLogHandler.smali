.class public Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;
.super Ljava/lang/Object;
.source "LocalLogHandler.java"

# interfaces
.implements Lcom/google/android/libraries/bind/logging/LogHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$CustomFormatter;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static logger:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method static synthetic access$000(I)Ljava/util/logging/Level;
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->javaLevel(I)Ljava/util/logging/Level;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Landroid/content/Context;)Ljava/util/logging/Logger;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->getLogger(Landroid/content/Context;)Ljava/util/logging/Logger;

    move-result-object v0

    return-object v0
.end method

.method private static getLogger(Landroid/content/Context;)Ljava/util/logging/Logger;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 119
    sget-object v4, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->logger:Ljava/util/logging/Logger;

    if-nez v4, :cond_0

    .line 120
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->logDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    .line 121
    .local v2, "logDir":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    const-string v4, "local"

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 122
    .local v3, "pattern":Ljava/io/File;
    const-string v4, "local"

    invoke-static {v4}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v4

    sput-object v4, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->logger:Ljava/util/logging/Logger;

    .line 124
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 125
    new-instance v1, Ljava/util/logging/FileHandler;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const/high16 v5, 0x20000

    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-direct {v1, v4, v5, v6, v7}, Ljava/util/logging/FileHandler;-><init>(Ljava/lang/String;IIZ)V

    .line 126
    .local v1, "handler":Ljava/util/logging/Handler;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$CustomFormatter;

    invoke-direct {v4}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$CustomFormatter;-><init>()V

    invoke-virtual {v1, v4}, Ljava/util/logging/Handler;->setFormatter(Ljava/util/logging/Formatter;)V

    .line 127
    sget-object v4, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->logger:Ljava/util/logging/Logger;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->setUseParentHandlers(Z)V

    .line 128
    sget-object v4, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->logger:Ljava/util/logging/Logger;

    invoke-virtual {v4, v1}, Ljava/util/logging/Logger;->addHandler(Ljava/util/logging/Handler;)V

    .line 129
    sget-object v4, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->ALL:Ljava/util/logging/Level;

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    .end local v1    # "handler":Ljava/util/logging/Handler;
    .end local v2    # "logDir":Ljava/io/File;
    .end local v3    # "pattern":Ljava/io/File;
    :cond_0
    :goto_0
    sget-object v4, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->logger:Ljava/util/logging/Logger;

    return-object v4

    .line 130
    .restart local v2    # "logDir":Ljava/io/File;
    .restart local v3    # "pattern":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Ljava/io/IOException;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    .line 134
    sget-object v4, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Failed to create local log handler: %s"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static isEnabled(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method private static javaLevel(I)Ljava/util/logging/Level;
    .locals 1
    .param p0, "logLevel"    # I

    .prologue
    .line 47
    packed-switch p0, :pswitch_data_0

    .line 61
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 49
    :pswitch_0
    sget-object v0, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    .line 59
    :goto_0
    return-object v0

    .line 51
    :pswitch_1
    sget-object v0, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    goto :goto_0

    .line 53
    :pswitch_2
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    goto :goto_0

    .line 55
    :pswitch_3
    sget-object v0, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    goto :goto_0

    .line 57
    :pswitch_4
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    goto :goto_0

    .line 59
    :pswitch_5
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static logDir(Landroid/content/Context;)Ljava/io/File;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "logs"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static logFile(Landroid/content/Context;I)Ljava/io/File;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "index"    # I

    .prologue
    .line 92
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->logDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    const-string v2, "local."

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xb

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static logFiles(Landroid/content/Context;)Ljava/util/List;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 100
    .local v0, "files":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->isEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 101
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v3, 0x2

    if-ge v1, v3, :cond_1

    .line 102
    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->logFile(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v2

    .line 103
    .local v2, "logFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_0

    .line 104
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 107
    .end local v2    # "logFile":Ljava/io/File;
    :cond_1
    new-instance v3, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$2;

    invoke-direct {v3}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$2;-><init>()V

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 114
    .end local v1    # "i":I
    :cond_2
    return-object v0
.end method


# virtual methods
.method public log(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "logLevel"    # I
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 72
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->LOGL:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$1;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$1;-><init>(Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->execute(Ljava/lang/Runnable;)V

    .line 85
    return-void
.end method
