.class Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter$1;
.super Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;
.source "CollectionDataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter$1;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-void
.end method


# virtual methods
.method public getErrorMessageData()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    .line 22
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter$1;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter$1;->this$0:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->getRetryRunnable()Ljava/lang/Runnable;

    move-result-object v3

    .line 21
    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method
