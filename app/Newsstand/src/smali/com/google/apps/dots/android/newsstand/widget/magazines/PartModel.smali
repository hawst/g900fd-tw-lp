.class public abstract Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;
.super Ljava/lang/Object;
.source "PartModel.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private configureAnalyticsEvents(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 6
    .param p1, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;
    .param p2, "eventScope"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;
    .param p3, "part"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 51
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "partId":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 53
    invoke-interface {p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getAppId()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "appId":Ljava/lang/String;
    invoke-interface {p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/shared/EventCode;->ANALYTICS_DO_TRACK_EVENT:Lcom/google/apps/dots/shared/EventCode;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    .line 55
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel$1;

    invoke-direct {v4, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;Ljava/lang/String;)V

    .line 54
    invoke-virtual {v2, v3, p2, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 70
    .end local v0    # "appId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private configureEventHandlers(Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 11
    .param p1, "eventDispatcher"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;
    .param p2, "eventScope"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;
    .param p3, "part"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    const/4 v4, 0x0

    .line 39
    iget-object v6, p3, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->eventHandler:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    array-length v7, v6

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_1

    aget-object v1, v6, v5

    .line 40
    .local v1, "eventHandler":Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->hasEventFilter()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 41
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->getEventFilter()Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;->getUriFilter()Ljava/lang/String;

    move-result-object v2

    .line 42
    .local v2, "eventUri":Ljava/lang/String;
    iget-object v8, v1, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    array-length v9, v8

    move v3, v4

    :goto_1
    if-ge v3, v9, :cond_0

    aget-object v0, v8, v3

    .line 43
    .local v0, "dispatchEvent":Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;->getUri()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v2, p2, v10}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addHandler(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Ljava/lang/String;)V

    .line 42
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 39
    .end local v0    # "dispatchEvent":Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;
    .end local v2    # "eventUri":Ljava/lang/String;
    :cond_0
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 47
    .end local v1    # "eventHandler":Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    :cond_1
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;)V
    .locals 0
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    .prologue
    .line 32
    return-void
.end method

.method public configureEvents(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    .locals 2
    .param p1, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;
    .param p2, "eventScope"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;
    .param p3, "part"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .prologue
    .line 20
    invoke-virtual {p3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v0

    .line 21
    .local v0, "partId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 22
    invoke-interface {p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;->configureEventHandlers(Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;->configureAnalyticsEvents(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 24
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;->onConfigureEvents(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 26
    :cond_0
    return-void
.end method

.method protected abstract onConfigureEvents(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
.end method

.method public unbindFrom(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;)V
    .locals 0
    .param p1, "partContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    .prologue
    .line 35
    return-void
.end method
