.class final Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState$1;
.super Ljava/lang/Object;
.source "ReadNowFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 59
    const-class v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 60
    .local v0, "pageEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 65
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    move-result-object v0

    return-object v0
.end method
