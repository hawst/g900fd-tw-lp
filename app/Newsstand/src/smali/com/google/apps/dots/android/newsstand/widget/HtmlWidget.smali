.class public abstract Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;
.super Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;
.source "HtmlWidget.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;
.implements Lcom/google/apps/dots/android/newsstand/widget/DotsWidget;
.implements Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;
    }
.end annotation


# static fields
.field private static final DEFAULT_HEADER_HEIGHT_VIEWPORT_PX:I = 0x3c

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field public static final MAX_USER_SCALE:I = 0x3

.field private static final POST_LAYOUT_SCROLL_DELAY:I = 0x1f4

.field public static final WEBVIEW_HTML_FILENAME:Ljava/lang/String; = "webview.html"

.field public static final WEBVIEW_HTML_LEGACY_FILENAME:Ljava/lang/String; = "webview_legacy.html"


# instance fields
.field protected applyFontSizePreference:Z

.field public applyPageFractionAfterLayout:Z

.field protected final articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

.field protected colorHelper:Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

.field protected currentViewportDpi:I

.field protected currentViewportHeight:I

.field protected currentViewportScale:F

.field protected currentViewportWidth:I

.field protected isVisibleToUser:Z

.field private lastTouchDownX:F

.field private lastTouchDownY:F

.field protected loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

.field private navigationTouchpointsEnabled:Z

.field private outerMarginViewportPx:I

.field private postTemplate:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

.field protected statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

.field protected userDrivenScroll:Z

.field protected whenLayoutCompletes:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V
    .locals 3
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "postTemplate"    # Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
    .param p5, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p6, "useVerticalLayout"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 133
    invoke-direct {p0, p1, p4, p5, p6}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V

    .line 56
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->whenLayoutCompletes:Lcom/google/common/util/concurrent/SettableFuture;

    .line 69
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->applyFontSizePreference:Z

    .line 70
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->userDrivenScroll:Z

    .line 71
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isVisibleToUser:Z

    .line 73
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    .line 120
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;-><init>(Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    .line 121
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->applyPageFractionAfterLayout:Z

    .line 124
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->navigationTouchpointsEnabled:Z

    .line 134
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->dotsDepend()V

    .line 135
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->postTemplate:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 136
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->setBackgroundColorFromTemplate(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;)V

    .line 137
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->setHapticFeedbackEnabled(Z)V

    .line 138
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->onUserVisibleHintChanged()V

    .line 139
    return-void
.end method

.method private delayedNotifyOnLoadComplete()V
    .locals 3

    .prologue
    .line 202
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->isLoaded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 203
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->stealLoadedRunnable()Ljava/lang/Runnable;

    move-result-object v0

    .line 204
    .local v0, "loadedAction":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;)V

    .line 205
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 213
    .end local v0    # "loadedAction":Ljava/lang/Runnable;
    :cond_0
    :goto_0
    return-void

    .line 208
    .restart local v0    # "loadedAction":Ljava/lang/Runnable;
    :cond_1
    if-eqz v0, :cond_2

    .line 209
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 211
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->notifyOnLoadComplete()V

    goto :goto_0
.end method

.method private dotsDepend()V
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->colorHelper()Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->colorHelper:Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

    .line 62
    return-void
.end method

.method private isBelowDefaultHeader(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 520
    const/high16 v2, 0x42700000    # 60.0f

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->currentViewportWidth:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 521
    .local v0, "marginPctY":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 522
    .local v1, "touchPctY":F
    cmpl-float v2, v1, v0

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private onUserVisibleHintChanged()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 534
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isVisibleToUser:Z

    if-eqz v0, :cond_0

    .line 535
    const-string v0, "pageview"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->notify(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 537
    :cond_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isVisibleToUser:Z

    if-eqz v0, :cond_1

    const-string v0, "focus"

    :goto_0
    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->notify(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538
    return-void

    .line 537
    :cond_1
    const-string v0, "blur"

    goto :goto_0
.end method


# virtual methods
.method protected computePageFromPageFraction()I
    .locals 4

    .prologue
    .line 444
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageFraction:F

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 445
    .local v0, "pageIndex":I
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1
.end method

.method public convertDipToViewportPx(F)I
    .locals 1
    .param p1, "dipValue"    # F

    .prologue
    .line 293
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->currentViewportScale:F

    div-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public convertPxToViewportPx(F)I
    .locals 2
    .param p1, "pxValue"    # F

    .prologue
    .line 297
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float v0, p1, v1

    .line 298
    .local v0, "dipValue":F
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->convertDipToViewportPx(F)I

    move-result v1

    return v1
.end method

.method public convertViewportPxToDip(FI)F
    .locals 2
    .param p1, "viewportPx"    # F
    .param p2, "viewportDpi"    # I

    .prologue
    .line 307
    const/high16 v0, 0x43200000    # 160.0f

    mul-float/2addr v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    return v0
.end method

.method public convertViewportPxToPx(FI)I
    .locals 2
    .param p1, "viewportPx"    # F
    .param p2, "viewportDpi"    # I

    .prologue
    .line 302
    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->convertViewportPxToDip(FI)F

    move-result v0

    .line 303
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 279
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    :goto_0
    return-void

    .line 283
    :cond_0
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    .line 284
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->postTemplate:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 285
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->destroy()V

    goto :goto_0
.end method

.method public getArticleState()Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->clone()Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPage()I
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    return v0
.end method

.method protected getDefaultArticleMarginBottom()Ljava/lang/String;
    .locals 2

    .prologue
    .line 329
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v0, v1, :cond_0

    const-string v0, "40px"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "60px"

    goto :goto_0
.end method

.method protected getDefaultArticleMarginInner()Ljava/lang/String;
    .locals 2

    .prologue
    .line 317
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v0, v1, :cond_0

    const-string v0, "30px"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "45px"

    goto :goto_0
.end method

.method protected getDefaultArticleMarginOuter()Ljava/lang/String;
    .locals 2

    .prologue
    .line 323
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v0, v1, :cond_0

    const-string v0, "22px"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "60px"

    goto :goto_0
.end method

.method protected getDefaultArticleMarginTop()Ljava/lang/String;
    .locals 2

    .prologue
    .line 311
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v0, v1, :cond_0

    const-string v0, "40px"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "60px"

    goto :goto_0
.end method

.method public getDefaultViewportDpi()F
    .locals 5

    .prologue
    .line 160
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v3, v4, :cond_0

    sget-object v0, Lcom/google/apps/dots/shared/ArticleRenderSettings;->FONT_DPI_MAP_PHONE:Ljava/util/Map;

    .line 162
    .local v0, "dpiMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;Ljava/lang/Integer;>;"
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getArticleFontSize()Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    move-result-object v2

    .line 163
    .local v2, "prefsFontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->applyFontSizePreference:Z

    if-eqz v3, :cond_1

    move-object v1, v2

    .line 164
    .local v1, "fontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    :goto_1
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-float v3, v3

    return v3

    .line 160
    .end local v0    # "dpiMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;Ljava/lang/Integer;>;"
    .end local v1    # "fontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    .end local v2    # "prefsFontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    :cond_0
    sget-object v0, Lcom/google/apps/dots/shared/ArticleRenderSettings;->FONT_DPI_MAP_TABLET:Ljava/util/Map;

    goto :goto_0

    .line 163
    .restart local v0    # "dpiMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;Ljava/lang/Integer;>;"
    .restart local v2    # "prefsFontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    :cond_1
    sget-object v1, Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;->MEDIUM:Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    goto :goto_1
.end method

.method public getHeightInInches()F
    .locals 2

    .prologue
    .line 550
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getXDpi()F

    move-result v1

    div-float/2addr v0, v1

    return v0
.end method

.method public getHttpHandle()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    return-object v0
.end method

.method public getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationTouchpointsEnabled()Z
    .locals 1

    .prologue
    .line 463
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->navigationTouchpointsEnabled:Z

    return v0
.end method

.method public getPageCount()I
    .locals 2

    .prologue
    .line 387
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getWidthInInches()F
    .locals 2

    .prologue
    .line 545
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getXDpi()F

    move-result v1

    div-float/2addr v0, v1

    return v0
.end method

.method protected handleLoadContent(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;Lcom/google/apps/dots/proto/client/DotsShared$Form;)V
    .locals 7
    .param p1, "template"    # Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .param p2, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 177
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getUsesColumns()Z

    move-result v1

    if-nez v1, :cond_0

    .line 178
    const-string v1, "skipColumnLayout"

    const-string v2, "true"

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->storeLayoutParameters()V

    .line 184
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->useLegacyLayout()Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_1

    .line 185
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "<style>body{background-color:%s}</style>%s"

    new-array v3, v4, [Ljava/lang/Object;

    .line 186
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getBackgroundColor()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 187
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getTemplate()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 185
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "content":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->setContent(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->loadContent()V

    .line 194
    return-void

    .line 189
    .end local v0    # "content":Ljava/lang/String;
    :cond_1
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s<style>%s</style>"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getTemplate()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    if-nez p2, :cond_2

    const-string v1, ""

    .line 190
    :goto_1
    aput-object v1, v4, v6

    .line 189
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "content":Ljava/lang/String;
    goto :goto_0

    .line 190
    .end local v0    # "content":Ljava/lang/String;
    :cond_2
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->getPostTemplateCss()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method protected handleOnLayoutChange(IZII)V
    .locals 8
    .param p1, "pageCount"    # I
    .param p2, "isDone"    # Z
    .param p3, "pageWidth"    # I
    .param p4, "pageHeight"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 217
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iput p1, v4, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    .line 222
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iput-boolean p2, v4, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    .line 224
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v4, v4, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageFraction:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_4

    move v0, v2

    .line 229
    .local v0, "canShowPageEarly":Z
    :goto_1
    if-nez v0, :cond_2

    if-eqz p2, :cond_3

    .line 230
    :cond_2
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->delayedNotifyOnLoadComplete()V

    .line 234
    :cond_3
    if-eqz p2, :cond_7

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->applyPageFractionAfterLayout:Z

    if-eqz v4, :cond_7

    .line 237
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->computePageFromPageFraction()I

    move-result v1

    .line 238
    .local v1, "currentPage":I
    invoke-virtual {p0, v1, p1, v3}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->updateCurrentPage(IIZ)V

    .line 244
    if-lez v1, :cond_5

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-ge v4, v5, :cond_5

    .line 245
    .local v2, "delayScrolling":Z
    :goto_2
    if-eqz v2, :cond_6

    .line 246
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$1;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;)V

    const-wide/16 v6, 0x1f4

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .end local v0    # "canShowPageEarly":Z
    .end local v1    # "currentPage":I
    .end local v2    # "delayScrolling":Z
    :cond_4
    move v0, v3

    .line 224
    goto :goto_1

    .restart local v0    # "canShowPageEarly":Z
    .restart local v1    # "currentPage":I
    :cond_5
    move v2, v3

    .line 244
    goto :goto_2

    .line 256
    .restart local v2    # "delayScrolling":Z
    :cond_6
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->scrollToCurrentPage()V

    goto :goto_0

    .line 258
    .end local v1    # "currentPage":I
    .end local v2    # "delayScrolling":Z
    :cond_7
    if-eqz v0, :cond_0

    .line 260
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v4, v4, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    invoke-virtual {p0, v4, p1, v3}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->updateCurrentPage(IIZ)V

    goto :goto_0
.end method

.method protected notifyNavigateBackward()V
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;->navigateBackward()V

    .line 367
    :cond_0
    return-void
.end method

.method protected notifyNavigateForward()V
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;->navigateForward()V

    .line 361
    :cond_0
    return-void
.end method

.method protected notifyOnLoadComplete()V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;->onLoadComplete()V

    .line 355
    :cond_0
    return-void
.end method

.method protected notifyOnLoadStart()V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;->onLoadStart(Ljava/lang/String;)V

    .line 349
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 266
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->onAttachedToWindow()V

    .line 267
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->notifyOnLoadStart()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 170
    invoke-super/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->onLayout(ZIIII)V

    .line 173
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->whenLayoutCompletes:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    .line 174
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 472
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->navigationTouchpointsEnabled:Z

    if-eqz v4, :cond_0

    .line 473
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_1

    .line 474
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isTouchDown:Z

    .line 475
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->lastTouchDownX:F

    .line 476
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->lastTouchDownY:F

    .line 513
    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    :goto_0
    return v3

    .line 477
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-ne v4, v3, :cond_0

    .line 478
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isTouchDown:Z

    .line 481
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->lastTouchDownX:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->lastTouchDownY:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    add-float v1, v4, v5

    .line 482
    .local v1, "touchDiff":F
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->getTouchSlop()F

    move-result v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    .line 483
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isBelowDefaultHeader(Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->useVerticalLayout:Z

    if-nez v4, :cond_5

    .line 484
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->outerMarginViewportPx:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->currentViewportWidth:I

    int-to-float v5, v5

    div-float v0, v4, v5

    .line 485
    .local v0, "marginPct":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v5, v5

    div-float v2, v4, v5

    .line 486
    .local v2, "touchPct":F
    cmpg-float v4, v2, v0

    if-gez v4, :cond_3

    .line 488
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getCurrentPage()I

    move-result v4

    if-lez v4, :cond_2

    .line 489
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getCurrentPage()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->snapToPage(I)Z

    goto :goto_0

    .line 491
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->notifyNavigateBackward()V

    goto :goto_0

    .line 494
    :cond_3
    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, v0

    cmpl-float v4, v2, v4

    if-lez v4, :cond_0

    .line 496
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getCurrentPage()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getPageCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v4, v5, :cond_4

    .line 497
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getCurrentPage()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->snapToPage(I)Z

    goto/16 :goto_0

    .line 499
    :cond_4
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->notifyNavigateForward()V

    goto/16 :goto_0

    .line 503
    .end local v0    # "marginPct":F
    .end local v2    # "touchPct":F
    :cond_5
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->useVerticalLayout:Z

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    .line 504
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$dimen;->action_bar_default_height:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    .line 507
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->toggleActionBar()V

    goto/16 :goto_0
.end method

.method protected scrollToCurrentPage()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 370
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->isLoaded()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 371
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    .line 372
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    .line 374
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getScrollOffset(I)I

    move-result v0

    .line 375
    .local v0, "target":I
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->useVerticalLayout:Z

    if-eqz v1, :cond_2

    .line 376
    invoke-virtual {p0, v3, v0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->scrollTo(II)V

    .line 377
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1, v0}, Landroid/widget/Scroller;->setFinalY(I)V

    .line 383
    .end local v0    # "target":I
    :cond_1
    :goto_0
    return-void

    .line 379
    .restart local v0    # "target":I
    :cond_2
    invoke-virtual {p0, v0, v3}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->scrollTo(II)V

    .line 380
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1, v0}, Landroid/widget/Scroller;->setFinalX(I)V

    goto :goto_0
.end method

.method public scrollToEdge(I)V
    .locals 1
    .param p1, "direction"    # I

    .prologue
    .line 440
    if-ltz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->fromFraction(Ljava/lang/Float;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    .line 441
    return-void

    .line 440
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
    .locals 5
    .param p1, "pageLocation"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 426
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->hasValidPageFraction()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 427
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->getNonNullPageFraction()F

    move-result v1

    .line 428
    .local v1, "pageFraction":F
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_1

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 429
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iput v1, v2, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageFraction:F

    .line 430
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->computePageFromPageFraction()I

    move-result v0

    .line 431
    .local v0, "currentPage":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    invoke-virtual {p0, v0, v2, v4}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->updateCurrentPage(IIZ)V

    .line 432
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->userDrivenScroll:Z

    .line 433
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->scrollToCurrentPage()V

    .line 434
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->userDrivenScroll:Z

    .line 436
    .end local v0    # "currentPage":I
    .end local v1    # "pageFraction":F
    :cond_0
    return-void

    .restart local v1    # "pageFraction":F
    :cond_1
    move v2, v4

    .line 428
    goto :goto_0
.end method

.method public setBackgroundColorFromTemplate(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;)V
    .locals 2
    .param p1, "template"    # Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .prologue
    .line 142
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->colorHelper:Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getTemplateBackgroundColor(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;)Ljava/lang/Integer;

    move-result-object v0

    .line 143
    .local v0, "bgColor":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->setBackgroundColor(I)V

    .line 146
    :cond_0
    return-void
.end method

.method public setCurrentViewportData(IIIF)V
    .locals 0
    .param p1, "currentViewportDpi"    # I
    .param p2, "currentViewportWidth"    # I
    .param p3, "currentViewportHeight"    # I
    .param p4, "currentViewportScale"    # F

    .prologue
    .line 555
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->currentViewportDpi:I

    .line 556
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->currentViewportWidth:I

    .line 557
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->currentViewportHeight:I

    .line 558
    iput p4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->currentViewportScale:F

    .line 559
    return-void
.end method

.method public setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V
    .locals 1
    .param p1, "eventHandler"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadStateEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V

    .line 156
    return-void
.end method

.method public setNavigationTouchpointsEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 467
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->navigationTouchpointsEnabled:Z

    .line 468
    return-void
.end method

.method public setStatusListener(Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;)V
    .locals 0
    .param p1, "statusListener"    # Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    .prologue
    .line 421
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    .line 422
    return-void
.end method

.method public setTemplateProperties(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "sectionId"    # Ljava/lang/String;
    .param p3, "sectionName"    # Ljava/lang/String;

    .prologue
    .line 452
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 453
    .local v0, "textData":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "editionName"

    if-eqz p1, :cond_1

    .end local p1    # "appName":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0, v1, p1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    const-string v1, "sectionName"

    if-eqz p3, :cond_2

    .end local p3    # "sectionName":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0, v1, p3}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    const-string v1, "text"

    invoke-virtual {p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->putIntoJsonStore(Ljava/lang/String;Lorg/codehaus/jackson/node/ObjectNode;)V

    .line 457
    if-eqz p2, :cond_0

    .line 458
    const-string v1, "sectionId"

    invoke-virtual {p0, v1, p2}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    :cond_0
    return-void

    .line 453
    .restart local p1    # "appName":Ljava/lang/String;
    .restart local p3    # "sectionName":Ljava/lang/String;
    :cond_1
    const-string p1, ""

    goto :goto_0

    .line 454
    .end local p1    # "appName":Ljava/lang/String;
    :cond_2
    const-string p3, ""

    goto :goto_1
.end method

.method public setUserVisibleHint(Z)V
    .locals 1
    .param p1, "isUserVisible"    # Z

    .prologue
    .line 527
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isVisibleToUser:Z

    if-eq v0, p1, :cond_0

    .line 528
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isVisibleToUser:Z

    .line 529
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->onUserVisibleHintChanged()V

    .line 531
    :cond_0
    return-void
.end method

.method protected storeLayoutParameters()V
    .locals 3

    .prologue
    .line 335
    const-string v1, "topMargin"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getDefaultArticleMarginTop()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    const-string v1, "innerMargin"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getDefaultArticleMarginInner()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getDefaultArticleMarginOuter()Ljava/lang/String;

    move-result-object v0

    .line 338
    .local v0, "outer":Ljava/lang/String;
    const-string v1, "outerMargin"

    invoke-virtual {p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v1, "bottomMargin"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->getDefaultArticleMarginBottom()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    const-string v1, "px"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->outerMarginViewportPx:I

    .line 343
    return-void
.end method

.method protected updateCurrentPage(IIZ)V
    .locals 5
    .param p1, "page"    # I
    .param p2, "pageCount"    # I
    .param p3, "userDriven"    # Z

    .prologue
    const/4 v4, 0x0

    .line 401
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->updateCurrentPage(IIZ)V

    .line 402
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iput p1, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    .line 403
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iput p2, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    .line 404
    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    if-nez v0, :cond_0

    .line 408
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->applyPageFractionAfterLayout:Z

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    if-eqz v0, :cond_1

    .line 411
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget-boolean v3, v3, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    invoke-interface {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;->updatePageNumber(IIZ)V

    .line 414
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->isLaidOut:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->isVisibleToUser:Z

    if-eqz v0, :cond_2

    .line 415
    const-string v0, "pageview"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->notify(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 417
    :cond_2
    return-void
.end method
