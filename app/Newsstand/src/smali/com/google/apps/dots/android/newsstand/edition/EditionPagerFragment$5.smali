.class Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;
.super Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
.source "EditionPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->setupPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Landroid/support/v4/app/FragmentManager;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;
    .param p2, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;
    .param p3, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 257
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-void
.end method


# virtual methods
.method public createFragment(ILcom/google/android/libraries/bind/data/Data;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p1, "logicalPosition"    # I
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 260
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageFragment(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 5
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 265
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;"
    instance-of v3, p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    if-eqz v3, :cond_1

    .line 266
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    iget-object v0, v3, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 267
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->dataListId(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v1

    .line 268
    .local v1, "logicalPosition":I
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    .line 269
    const/4 v2, -0x2

    .line 274
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v1    # "logicalPosition":I
    :goto_0
    return v2

    .line 271
    .restart local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .restart local v1    # "logicalPosition":I
    :cond_0
    invoke-static {p0, v1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getVisualPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v2

    goto :goto_0

    .line 273
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v1    # "logicalPosition":I
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_1
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    move-result v2

    .line 274
    .local v2, "superResult":I
    goto :goto_0
.end method
