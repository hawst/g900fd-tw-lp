.class Lcom/google/apps/dots/android/newsstand/async/TaskQueue$1;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "TaskQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->addTask(Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/TaskQueue;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/async/TaskQueue;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    # getter for: Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->tasks:Ljava/util/concurrent/PriorityBlockingQueue;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->access$000(Lcom/google/apps/dots/android/newsstand/async/TaskQueue;)Ljava/util/concurrent/PriorityBlockingQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/async/Task;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/Task;->execute()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/TaskQueue$1;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
