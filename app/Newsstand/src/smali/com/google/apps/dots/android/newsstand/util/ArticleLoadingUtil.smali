.class public Lcom/google/apps/dots/android/newsstand/util/ArticleLoadingUtil;
.super Ljava/lang/Object;
.source "ArticleLoadingUtil.java"


# direct methods
.method public static convertDipToViewportPx(F)I
    .locals 7
    .param p0, "dipValue"    # F

    .prologue
    const/high16 v6, 0x42c80000    # 100.0f

    .line 48
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 50
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getXDpi()F

    move-result v3

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDefaultViewportDpi()F

    move-result v4

    div-float/2addr v3, v4

    iget v4, v1, Landroid/util/DisplayMetrics;->density:F

    div-float v0, v3, v4

    .line 51
    .local v0, "defaultViewportScale":F
    mul-float v3, v6, v0

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-float v3, v4

    div-float v2, v3, v6

    .line 52
    .local v2, "quantizedDefaultViewportScale":F
    div-float v3, p0, v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    return v3
.end method

.method public static convertPxToViewportPx(F)I
    .locals 2
    .param p0, "pxValue"    # F

    .prologue
    .line 39
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float v0, p0, v1

    .line 40
    .local v0, "dipValue":F
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/ArticleLoadingUtil;->convertDipToViewportPx(F)I

    move-result v1

    return v1
.end method

.method public static getDefaultArticleMarginBottom()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v0, v1, :cond_0

    const-string v0, "40px"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "60px"

    goto :goto_0
.end method

.method public static getDefaultArticleMarginInner()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v0, v1, :cond_0

    const-string v0, "30px"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "45px"

    goto :goto_0
.end method

.method public static getDefaultArticleMarginOuter()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v0, v1, :cond_0

    const-string v0, "22px"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "60px"

    goto :goto_0
.end method

.method public static getDefaultArticleMarginTop()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v0, v1, :cond_0

    const-string v0, "40px"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "60px"

    goto :goto_0
.end method
