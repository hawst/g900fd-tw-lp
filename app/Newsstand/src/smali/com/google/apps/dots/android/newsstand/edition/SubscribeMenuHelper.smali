.class public Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;
.super Ljava/lang/Object;
.source "SubscribeMenuHelper.java"


# instance fields
.field private activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field private allowRemoveToFade:Z

.field private fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

.field private subscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

.field private subscriptionObserver:Lcom/google/android/libraries/bind/data/DataObserver;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->allowRemoveToFade:Z

    .line 41
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .line 42
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->init()V

    .line 43
    return-void
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->allowRemoveToFade:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;)Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    return-object v0
.end method

.method public static getRemoveStringResId(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I
    .locals 2
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->TOPIC:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v0, v1, :cond_1

    .line 176
    :cond_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->remove_topic:I

    .line 179
    :goto_0
    return v0

    :cond_1
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->remove_source:I

    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 46
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->subscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    .line 47
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->subscriptionObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 55
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->subscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->subscriptionObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 56
    return-void
.end method


# virtual methods
.method public isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 2
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 138
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v0

    .line 139
    .local v0, "librarySnapshot":Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v1

    return v1
.end method

.method public isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 2
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 133
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v0

    .line 134
    .local v0, "librarySnapshot":Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v1

    return v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->subscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->subscriptionObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 60
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 12
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 63
    if-nez p2, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v8

    sget-object v9, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SECTION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v8, v9, :cond_2

    .line 67
    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .end local p2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object p2

    .line 69
    .restart local p2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_2
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v1

    .line 70
    .local v1, "isSubscribed":Z
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->supportsSubscription()Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->subscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->hasRefreshedOnce()Z

    move-result v8

    if-eqz v8, :cond_4

    if-eqz v1, :cond_4

    move v4, v6

    .line 73
    .local v4, "showRemove":Z
    :goto_1
    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->menu_remove_edition:I

    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 74
    .local v2, "removeEditionMenuItem":Landroid/view/MenuItem;
    if-eqz v2, :cond_3

    .line 76
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v8

    if-eqz v8, :cond_5

    sget v8, Lcom/google/android/apps/newsstanddev/R$string;->unsubscribe:I

    .line 75
    :goto_2
    invoke-interface {v2, v8}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 77
    if-eqz v4, :cond_8

    .line 78
    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->allowRemoveToFade:Z

    if-eqz v8, :cond_7

    .line 79
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    if-nez v8, :cond_6

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v8, v8, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    .line 81
    .local v5, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    :goto_3
    new-instance v8, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$2;

    invoke-direct {v8, p0}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$2;-><init>(Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;)V

    const-wide/16 v10, 0x7d0

    invoke-virtual {v5, v8, v10, v11}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 95
    .end local v5    # "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    :goto_4
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 104
    :cond_3
    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->menu_add_edition:I

    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 105
    .local v0, "addEditionMenuItem":Landroid/view/MenuItem;
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->supportsSubscription()Z

    move-result v8

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->subscriptionList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->hasRefreshedOnce()Z

    move-result v8

    if-eqz v8, :cond_9

    if-nez v1, :cond_9

    move v3, v6

    .line 107
    .local v3, "showAdd":Z
    :goto_5
    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 108
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    if-nez v6, :cond_a

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 109
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    .line 110
    .restart local v5    # "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    :goto_6
    invoke-virtual {p2, v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance v7, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$3;

    invoke-direct {v7, p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$3;-><init>(Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;Landroid/view/MenuItem;)V

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addInlineCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_0

    .end local v0    # "addEditionMenuItem":Landroid/view/MenuItem;
    .end local v2    # "removeEditionMenuItem":Landroid/view/MenuItem;
    .end local v3    # "showAdd":Z
    .end local v4    # "showRemove":Z
    .end local v5    # "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    :cond_4
    move v4, v7

    .line 70
    goto :goto_1

    .line 76
    .restart local v2    # "removeEditionMenuItem":Landroid/view/MenuItem;
    .restart local v4    # "showRemove":Z
    :cond_5
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->getRemoveStringResId(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I

    move-result v8

    goto :goto_2

    .line 79
    :cond_6
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    iget-object v8, v8, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 80
    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    goto :goto_3

    .line 90
    :cond_7
    invoke-static {v2, v7}, Landroid/support/v4/view/MenuItemCompat;->setShowAsAction(Landroid/view/MenuItem;I)V

    goto :goto_4

    .line 93
    :cond_8
    iput-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->allowRemoveToFade:Z

    goto :goto_4

    .restart local v0    # "addEditionMenuItem":Landroid/view/MenuItem;
    :cond_9
    move v3, v7

    .line 105
    goto :goto_5

    .line 109
    .restart local v3    # "showAdd":Z
    :cond_a
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    goto :goto_6
.end method

.method protected onSubscriptionsChanged()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->invalidateOptionsMenu()V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->supportInvalidateOptionsMenu()V

    .line 130
    :cond_1
    return-void
.end method

.method public subscribe(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 144
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    .line 146
    .local v0, "subscribeActivity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    .line 148
    .local v1, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-virtual {p2, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$4;

    invoke-direct {v3, p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$4;-><init>(Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;)V

    .line 147
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 158
    return-void

    .line 144
    .end local v0    # "subscribeActivity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .end local v1    # "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    goto :goto_0
.end method

.method public unsubscribe(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 161
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 162
    .local v1, "unsubscribeActivity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 164
    .local v0, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-virtual {p2, v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;

    invoke-direct {v3, p0, v1, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper$5;-><init>(Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 163
    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 172
    return-void

    .line 161
    .end local v0    # "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .end local v1    # "unsubscribeActivity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    goto :goto_0
.end method
