.class public Lcom/google/apps/dots/android/newsstand/util/OffersUtil;
.super Ljava/lang/Object;
.source "OffersUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;
    }
.end annotation


# static fields
.field private static final localOfferStatus:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/accounts/Account;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->localOfferStatus:Ljava/util/HashMap;

    return-void
.end method

.method public static acceptOffer(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;ZLjava/lang/String;)V
    .locals 6
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p3, "longToastDisplayTime"    # Z
    .param p4, "analyticsReadingScreen"    # Ljava/lang/String;

    .prologue
    .line 122
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->modifyOffer(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;ZZLjava/lang/String;)V

    .line 128
    return-void
.end method

.method public static acceptOfferNoUi(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p2, "analyticsReadingScreen"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 143
    const/4 v0, 0x1

    invoke-static {p0, p1, v0, p2}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->modifyOfferNoUi(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$000([II)Z
    .locals 1
    .param p0, "x0"    # [I
    .param p1, "x1"    # I

    .prologue
    .line 43
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->containsResId([II)Z

    move-result v0

    return v0
.end method

.method public static clearOffersLocallyAcceptedForEdition(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 5
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 93
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 94
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->getOfferStatusForAccount(Landroid/accounts/Account;)Ljava/util/HashMap;

    move-result-object v2

    .line 95
    .local v2, "localOfferStatusForAccount":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 100
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 101
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 102
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 103
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 106
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    :cond_1
    return-void
.end method

.method private static containsResId([II)Z
    .locals 2
    .param p0, "resIds"    # [I
    .param p1, "resId"    # I

    .prologue
    .line 266
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 267
    aget v1, p0, v0

    if-ne v1, p1, :cond_0

    .line 268
    const/4 v1, 0x1

    .line 271
    :goto_1
    return v1

    .line 266
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 271
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static declineOffer(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;)V
    .locals 6
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p3, "analyticsReadingScreen"    # Ljava/lang/String;

    .prologue
    .line 158
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->modifyOffer(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;ZZLjava/lang/String;)V

    .line 159
    return-void
.end method

.method public static getEditionSummaryForOffer(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 3
    .param p0, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 225
    .line 226
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromSummaries(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 227
    .local v0, "offerEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v1

    .line 228
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v2

    .line 227
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->editionSummary(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v1

    return-object v1
.end method

.method private static getOfferStatusForAccount(Landroid/accounts/Account;)Ljava/util/HashMap;
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->localOfferStatus:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 59
    .local v0, "localOfferStatusForAccount":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "localOfferStatusForAccount":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 61
    .restart local v0    # "localOfferStatusForAccount":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->localOfferStatus:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    :cond_0
    return-object v0
.end method

.method public static getOffersStatusSnapshot(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 88
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 89
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;

    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->getOfferStatusForAccount(Landroid/accounts/Account;)Ljava/util/HashMap;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;-><init>(Ljava/util/HashMap;)V

    return-object v0
.end method

.method public static makeEnsureNoWarmWelcomeCardIfOfferCardFilter()Lcom/google/android/libraries/bind/data/Filter;
    .locals 2

    .prologue
    .line 232
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$2;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$2;-><init>(Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method private static modifyOffer(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;ZZLjava/lang/String;)V
    .locals 4
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p3, "accept"    # Z
    .param p4, "longToastDisplayTime"    # Z
    .param p5, "analyticsReadingScreen"    # Ljava/lang/String;

    .prologue
    .line 180
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setLastNewFeatureCardDismissTime(J)V

    .line 183
    invoke-static {p1, p2, p3, p5}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->modifyOfferNoUi(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 184
    .local v0, "mutationFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$1;

    invoke-direct {v2, p3, p2, p0}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$1;-><init>(ZLcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 195
    return-void
.end method

.method private static modifyOfferNoUi(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p2, "accept"    # Z
    .param p3, "analyticsReadingScreen"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 202
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 204
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->getEditionSummaryForOffer(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v6

    iget-object v4, v6, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 205
    .local v4, "offeredEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->getOfferStatusForAccount(Landroid/accounts/Account;)Ljava/util/HashMap;

    move-result-object v1

    .line 206
    .local v1, "localOfferStatusForAccount":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v6

    if-eqz p2, :cond_1

    .end local v4    # "offeredEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :goto_0
    invoke-virtual {v1, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, p0, v8}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getOfferForOfferId(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 208
    .local v3, "offerUrl":Ljava/lang/String;
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    invoke-virtual {v6, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v8

    if-eqz p2, :cond_2

    move v6, v7

    .line 209
    :goto_1
    invoke-virtual {v8, v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v6

    .line 210
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v6

    .line 211
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->makeOfferHint(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setSimulationHint(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    .line 212
    .local v0, "action":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    new-instance v6, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v8

    invoke-virtual {v8, p0}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyOffers(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8, v0}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    sget-object v8, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 213
    invoke-virtual {v6, v8}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    move-result-object v5

    .line 214
    .local v5, "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v6

    invoke-virtual {v6, p0, v5}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 216
    .local v2, "mutationFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    if-eqz p3, :cond_0

    .line 217
    new-instance v8, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v9

    .line 218
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v10

    if-eqz p2, :cond_3

    sget-object v6, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->ACCEPTED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    :goto_2
    invoke-direct {v8, p3, v9, v10, v6}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;)V

    .line 219
    invoke-virtual {v8, v7}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->track(Z)V

    .line 221
    :cond_0
    return-object v2

    .line 206
    .end local v0    # "action":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .end local v2    # "mutationFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    .end local v3    # "offerUrl":Ljava/lang/String;
    .end local v5    # "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    .restart local v4    # "offeredEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 208
    .end local v4    # "offeredEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .restart local v3    # "offerUrl":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x1

    goto :goto_1

    .line 218
    .restart local v0    # "action":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .restart local v2    # "mutationFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    .restart local v5    # "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    :cond_3
    sget-object v6, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->DECLINED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    goto :goto_2
.end method
