.class public final Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;
.super Ljava/lang/Object;
.source "PrefixTree.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<KeySegmentT:",
        "Ljava/lang/Object;",
        "ValueT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final root:Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node",
            "<TKeySegmentT;TValueT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree<TKeySegmentT;TValueT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;-><init>(Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$1;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;->root:Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;

    return-void
.end method

.method public static create()Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<KeySegmentT:",
            "Ljava/lang/Object;",
            "ValueT:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree",
            "<TKeySegmentT;TValueT;>;"
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getPrefixes(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TKeySegmentT;>;)",
            "Ljava/util/List",
            "<TValueT;>;"
        }
    .end annotation

    .prologue
    .line 86
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree<TKeySegmentT;TValueT;>;"
    .local p1, "key":Ljava/util/List;, "Ljava/util/List<TKeySegmentT;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 87
    .local v0, "results":Ljava/util/List;, "Ljava/util/List<TValueT;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;->root:Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->get(Ljava/util/Collection;Ljava/util/Iterator;)V

    .line 88
    return-object v0
.end method

.method public put(Ljava/util/List;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TKeySegmentT;>;TValueT;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree<TKeySegmentT;TValueT;>;"
    .local p1, "key":Ljava/util/List;, "Ljava/util/List<TKeySegmentT;>;"
    .local p2, "value":Ljava/lang/Object;, "TValueT;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;->root:Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->put(Ljava/util/Iterator;Ljava/lang/Object;)V

    .line 76
    return-void
.end method

.method public remove(Ljava/util/List;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TKeySegmentT;>;TValueT;)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;, "Lcom/google/apps/dots/android/newsstand/collections/PrefixTree<TKeySegmentT;TValueT;>;"
    .local p1, "key":Ljava/util/List;, "Ljava/util/List<TKeySegmentT;>;"
    .local p2, "value":Ljava/lang/Object;, "TValueT;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree;->root:Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/apps/dots/android/newsstand/collections/PrefixTree$Node;->remove(Ljava/util/Iterator;Ljava/lang/Object;)Z

    .line 80
    return-void
.end method
