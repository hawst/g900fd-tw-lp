.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;
.super Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

.field final synthetic val$collection:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p2, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .param p3, "debugId"    # Ljava/lang/String;
    .param p4, "debugOwner"    # Ljava/lang/Object;

    .prologue
    .line 560
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->val$collection:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public callInternal()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 563
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "%s: syncing deps"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 565
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v2

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->skipNodePredicate:Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;

    if-eqz v2, :cond_0

    .line 566
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v2

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->skipNodePredicate:Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->val$collection:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;->onPreTraverseCollection(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    .line 569
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 570
    .local v0, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    new-instance v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;Ljava/util/List;)V

    .line 625
    .local v1, "visitor":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;>;"
    new-instance v2, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->val$collection:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 626
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->startTasks(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v2, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    return-object v2
.end method
