.class Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "ReadNowFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateHero()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 4
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 252
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentHero:Landroid/view/View;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->access$300(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 253
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 255
    .local v0, "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    :goto_0
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;

    if-eqz v1, :cond_2

    .line 256
    check-cast v0, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;

    .end local v0    # "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->access$400(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;->apply(Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;)V

    .line 257
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->access$400(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    move-result-object v2

    # setter for: Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentHero:Landroid/view/View;
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->access$302(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;Landroid/view/View;)Landroid/view/View;

    .line 263
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->startRippleAnimation()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->access$600(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)V

    .line 264
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->access$700(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshNow()V

    .line 265
    return-void

    .line 254
    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->iconSource()Lcom/google/apps/dots/android/newsstand/icon/IconSource;

    move-result-object v0

    goto :goto_0

    .line 258
    .restart local v0    # "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    :cond_2
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    if-eqz v1, :cond_0

    .line 259
    check-cast v0, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    .end local v0    # "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->access$500(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    .line 260
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$dimen;->edition_header_logo_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 259
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/icon/IconId;->apply(Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;I)V

    .line 261
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->access$500(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v2

    # setter for: Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentHero:Landroid/view/View;
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->access$302(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;Landroid/view/View;)Landroid/view/View;

    goto :goto_1
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 249
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
