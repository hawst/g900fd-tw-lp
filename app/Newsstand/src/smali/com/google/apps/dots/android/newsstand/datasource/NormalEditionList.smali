.class public Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;
.super Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
.source "NormalEditionList.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V

    .line 26
    return-void
.end method

.method private logResponseNodeStats(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V
    .locals 11
    .param p1, "rootNode"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .prologue
    const/4 v5, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 37
    new-array v0, v8, [Ljava/lang/String;

    .line 38
    .local v0, "appTitle":[Ljava/lang/String;
    new-array v2, v8, [I

    .line 39
    .local v2, "sections":[I
    new-array v1, v8, [I

    .line 40
    .local v1, "posts":[I
    new-instance v3, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    invoke-direct {v3, p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v4, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList$1;

    invoke-direct {v4, p0, v0, v2, v1}, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList$1;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;[Ljava/lang/String;[I[I)V

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 54
    aget v3, v2, v7

    if-nez v3, :cond_0

    aget v3, v1, v7

    if-nez v3, :cond_0

    .line 55
    sget-object v3, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Warning! %s for %s returned %d sections and %d posts"

    new-array v5, v5, [Ljava/lang/Object;

    .line 56
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->getSelf()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getUri()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    aget-object v6, v0, v7

    aput-object v6, v5, v8

    aget v6, v2, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    aget v6, v1, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    .line 55
    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    :goto_0
    return-void

    .line 58
    :cond_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "%s for %s returned %d sections and %d posts"

    new-array v5, v5, [Ljava/lang/Object;

    .line 59
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->getSelf()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getUri()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    aget-object v6, v0, v7

    aput-object v6, v5, v8

    aget v6, v2, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    aget v6, v1, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    .line 58
    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
    .locals 1
    .param p1, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "rootNode"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p3}, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;->logResponseNodeStats(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
