.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Appearance"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;


# instance fields
.field private backgroundColor_:Ljava/lang/String;

.field private bitField0_:I

.field private visible_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1440
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1441
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1446
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->visible_:Z

    .line 1465
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->backgroundColor_:Ljava/lang/String;

    .line 1441
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;
    .locals 3

    .prologue
    .line 1498
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1502
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;
    return-object v0

    .line 1499
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;
    :catch_0
    move-exception v1

    .line 1500
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1437
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1507
    if-ne p1, p0, :cond_1

    .line 1511
    :cond_0
    :goto_0
    return v1

    .line 1508
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1509
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    .line 1510
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->visible_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->visible_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->backgroundColor_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->backgroundColor_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 1511
    goto :goto_0

    .line 1510
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->backgroundColor_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->backgroundColor_:Ljava/lang/String;

    .line 1511
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getBackgroundColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1467
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->backgroundColor_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1534
    const/4 v0, 0x0

    .line 1535
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1536
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->visible_:Z

    .line 1537
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1539
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1540
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->backgroundColor_:Ljava/lang/String;

    .line 1541
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1543
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->cachedSize:I

    .line 1544
    return v0
.end method

.method public getVisible()Z
    .locals 1

    .prologue
    .line 1448
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->visible_:Z

    return v0
.end method

.method public hasBackgroundColor()Z
    .locals 1

    .prologue
    .line 1478
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVisible()Z
    .locals 1

    .prologue
    .line 1456
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 1515
    const/16 v0, 0x11

    .line 1516
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 1517
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->visible_:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    add-int v0, v2, v1

    .line 1518
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->backgroundColor_:Ljava/lang/String;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    add-int v0, v2, v1

    .line 1519
    return v0

    .line 1517
    :cond_0
    const/4 v1, 0x2

    goto :goto_0

    .line 1518
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->backgroundColor_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1552
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1553
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1557
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1558
    :sswitch_0
    return-object p0

    .line 1563
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->visible_:Z

    .line 1564
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->bitField0_:I

    goto :goto_0

    .line 1568
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->backgroundColor_:Ljava/lang/String;

    .line 1569
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->bitField0_:I

    goto :goto_0

    .line 1553
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1437
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1524
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1525
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->visible_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1527
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1528
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Appearance;->backgroundColor_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1530
    :cond_1
    return-void
.end method
