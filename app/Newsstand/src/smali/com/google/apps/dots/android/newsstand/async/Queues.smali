.class public Lcom/google/apps/dots/android/newsstand/async/Queues;
.super Lcom/google/android/libraries/bind/async/Queues;
.source "Queues.java"


# static fields
.field public static final ANALYTICS:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final CACHE_WARMUP:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final DECODE_BITMAP:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final HTTP_CONTENT_SERVICE:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final LOGL:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final NETWORK_API:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final NSCLIENT_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final NSSTORE_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final PRIORITY_UNINTERRUPTED:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final STORE_MUTATION:Lcom/google/apps/dots/android/newsstand/async/Queue;

.field public static final SYNC:Lcom/google/apps/dots/android/newsstand/async/Queue;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 8
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "CPU"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 10
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "DISK"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 12
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "DECODE_BITMAP"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DECODE_BITMAP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 15
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "NETWORK_API"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NETWORK_API:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 17
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "CACHE_WARMUP"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->CACHE_WARMUP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 19
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "SYNC"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->SYNC:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 21
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "STORE_MUTATION"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->STORE_MUTATION:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 25
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "NSSTORE_PRIVATE"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSSTORE_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 26
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "NSCLIENT_PRIVATE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v4}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSCLIENT_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 28
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "PRIORITY_UNINTERRUPTED"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->PRIORITY_UNINTERRUPTED:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 29
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "HTTP_CONTENT_SERVICE"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->HTTP_CONTENT_SERVICE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 35
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "ANALYTICS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->ANALYTICS:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 36
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/Queue;

    const-string v1, "LOGL"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/dots/android/newsstand/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->LOGL:Lcom/google/apps/dots/android/newsstand/async/Queue;

    return-void
.end method
