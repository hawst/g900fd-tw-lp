.class public interface abstract Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;
.super Ljava/lang/Object;
.source "ScrollablePartView.java"


# virtual methods
.method public abstract canScrollHorizontally(I)Z
.end method

.method public abstract getCurrentPage()I
.end method

.method public abstract getCurrentPageFraction()F
.end method

.method public abstract getPageCount()I
.end method

.method public abstract getScrollOffset()I
.end method

.method public abstract getScrollRange()I
.end method

.method public abstract scrollToEdge(I)V
.end method

.method public abstract scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
.end method

.method public abstract setEnableGutterTap(Z)V
.end method
