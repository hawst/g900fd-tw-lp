.class public Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "SettingsFragment.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field protected prefListenerHandles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/util/Disposable;",
            ">;"
        }
    .end annotation
.end field

.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field private processRestartRequired:Z

.field private showingDeveloperPreferences:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 59
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 61
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefListenerHandles:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    .prologue
    .line 55
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getFontSizeTitleResId(Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;)I

    move-result v0

    return v0
.end method

.method static synthetic access$302(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->processRestartRequired:Z

    return p1
.end method

.method private static getFontSizeTitleResId(Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;)I
    .locals 2
    .param p0, "fontSize"    # Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    .prologue
    .line 102
    sget-object v0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$27;->$SwitchMap$com$google$apps$dots$shared$ArticleRenderSettings$FontSize:[I

    invoke-virtual {p0}, Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 110
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 104
    :pswitch_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->article_text_size_small:I

    goto :goto_0

    .line 106
    :pswitch_1
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->article_text_size_normal:I

    goto :goto_0

    .line 108
    :pswitch_2
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->article_text_size_large:I

    goto :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setUpImageSyncTypePreference()V
    .locals 3

    .prologue
    .line 214
    sget-object v2, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->IMAGE_SYNC_TYPE:Ljava/lang/String;

    .line 215
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    .line 216
    .local v1, "preference":Landroid/preference/ListPreference;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getImageSyncType()Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;

    move-result-object v0

    .line 217
    .local v0, "initialType":Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 218
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType;->getOptionStringResId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    .line 219
    new-instance v2, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$5;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 229
    return-void
.end method

.method private setUpInternalSyncPreferences()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 638
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setUpImageSyncTypePreference()V

    .line 640
    sget-object v5, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SYNC_MINIMUM_STORAGE_MAGS:Ljava/lang/String;

    .line 641
    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/EditTextPreference;

    .line 642
    .local v2, "minSyncSpaceMagsPreference":Landroid/preference/EditTextPreference;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getMinSyncSpaceMagazinesMb()I

    move-result v0

    .line 643
    .local v0, "initialMinMags":I
    const-string v5, "%d MB"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 644
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 645
    new-instance v5, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$24;

    invoke-direct {v5, p0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$24;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/EditTextPreference;)V

    invoke-virtual {v2, v5}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 658
    sget-object v5, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SYNC_MINIMUM_STORAGE_NEWS:Ljava/lang/String;

    .line 659
    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/EditTextPreference;

    .line 660
    .local v3, "minSyncSpaceNewsPreference":Landroid/preference/EditTextPreference;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getMinSyncSpaceNewsMb()I

    move-result v1

    .line 661
    .local v1, "initialMinNews":I
    const-string v5, "%d MB"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 662
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 663
    new-instance v5, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$25;

    invoke-direct {v5, p0, v3}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$25;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/EditTextPreference;)V

    invoke-virtual {v3, v5}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 676
    sget-object v5, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->START_BACKGROUND_SYNC:Ljava/lang/String;

    .line 677
    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 678
    .local v4, "startBackgroundSyncPreference":Landroid/preference/Preference;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$26;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$26;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 696
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->updateSyncInfo()V

    .line 697
    return-void
.end method

.method private setupAboutAppPreference()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ShowToast"
        }
    .end annotation

    .prologue
    .line 300
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 301
    .local v0, "context":Landroid/content/Context;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->ABOUT:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 302
    .local v1, "preference":Landroid/preference/Preference;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getAppName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 303
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getAppSubtitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 305
    sget-object v2, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->ABOUT:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$8;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 339
    return-void
.end method

.method private setupAboutPreferences()V
    .locals 0

    .prologue
    .line 264
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupOpenSourcePreference()V

    .line 265
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupTermsOfServicePreference()V

    .line 266
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupPrivacyPolicyPreference()V

    .line 268
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupAboutAppPreference()V

    .line 269
    return-void
.end method

.method private setupDownloadPreferences()V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupDownloadViaWifiOnlyPreference()V

    .line 174
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupDownloadWhileChargingOnlyPreference()V

    .line 175
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupUseExternalStoragePreference()V

    .line 176
    return-void
.end method

.method private setupDownloadViaWifiOnlyPreference()V
    .locals 3

    .prologue
    .line 180
    sget-object v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->DOWNLOAD_VIA_WIFI_ONLY:Ljava/lang/String;

    .line 181
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 182
    .local v0, "wifiSyncPreference":Landroid/preference/CheckBoxPreference;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$3;

    const-string v2, "readingPositionSync"

    invoke-direct {v1, p0, v0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    .line 193
    return-void
.end method

.method private setupDownloadWhileChargingOnlyPreference()V
    .locals 3

    .prologue
    .line 196
    sget-object v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->DOWNLOAD_VIA_CHARGING_ONLY:Ljava/lang/String;

    .line 197
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 198
    .local v0, "wifiSyncPreference":Landroid/preference/CheckBoxPreference;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$4;

    const-string v2, "syncOnlyIfCharging"

    invoke-direct {v1, p0, v0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    .line 210
    return-void
.end method

.method private setupOnlineLinkPreference(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 2
    .param p1, "preferenceKey"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 343
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$9;

    invoke-direct {v1, p0, p2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$9;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/net/Uri;)V

    .line 344
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 353
    return-void
.end method

.method private setupOpenSourcePreference()V
    .locals 2

    .prologue
    .line 273
    sget-object v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->OPEN_SOURCE_LICENSES:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$7;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V

    .line 274
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 283
    return-void
.end method

.method private setupPrivacyPolicyPreference()V
    .locals 4

    .prologue
    .line 292
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 293
    .local v0, "account":Landroid/accounts/Account;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->PRIVACY_POLICY:Ljava/lang/String;

    .line 294
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getGooglePrivacyPolicyUri(Landroid/accounts/Account;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    .line 293
    invoke-direct {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupOnlineLinkPreference(Ljava/lang/String;Landroid/net/Uri;)V

    .line 295
    return-void
.end method

.method private setupShowNotificationsPreference()V
    .locals 4

    .prologue
    .line 131
    sget-object v2, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SHOW_NOTIFICATIONS:Ljava/lang/String;

    .line 132
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 135
    .local v1, "notificationsPreference":Landroid/preference/CheckBoxPreference;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/play/MarketInfo;->areMagazinesAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 136
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;->NOTIFICATIONS_DISABLED:Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setNotificationMode(Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;)V

    .line 137
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 138
    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->general_preference_category_key:I

    .line 139
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 140
    .local v0, "generalPreferenceCategory":Landroid/preference/PreferenceCategory;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 157
    .end local v0    # "generalPreferenceCategory":Landroid/preference/PreferenceCategory;
    :goto_0
    return-void

    .line 145
    :cond_0
    new-instance v2, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$2;

    const-string v3, "notificationMode"

    invoke-direct {v2, p0, v1, v3}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setupTermsOfServicePreference()V
    .locals 4

    .prologue
    .line 286
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 287
    .local v0, "account":Landroid/accounts/Account;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->TERMS_OF_SERVICE:Ljava/lang/String;

    .line 288
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getTermsOfServiceUri(Landroid/accounts/Account;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    .line 287
    invoke-direct {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupOnlineLinkPreference(Ljava/lang/String;Landroid/net/Uri;)V

    .line 289
    return-void
.end method

.method private setupUseExternalStoragePreference()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 233
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v1

    .line 235
    .local v1, "externalStorageDir":Ljava/io/File;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->USE_EXTERNAL_STORAGE:Ljava/lang/String;

    .line 236
    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    .line 237
    .local v2, "useExternalStoragePreference":Landroid/preference/CheckBoxPreference;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$6;

    invoke-direct {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Ljava/io/File;)V

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 250
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v5, "externalStorageDir"

    .line 251
    invoke-virtual {v3, v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    .line 250
    :goto_0
    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 253
    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 254
    sget-object v3, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->DOWNLOADING_CATEGORY:Ljava/lang/String;

    .line 255
    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 256
    .local v0, "downloadCategory":Landroid/preference/PreferenceCategory;
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 261
    .end local v0    # "downloadCategory":Landroid/preference/PreferenceCategory;
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v3, v4

    .line 251
    goto :goto_0

    .line 257
    :cond_2
    if-nez v1, :cond_0

    .line 259
    invoke-virtual {v2, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_1
.end method

.method private updateSyncInfo()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 700
    const/4 v1, 0x0

    .line 702
    .local v1, "intervalSeconds":Ljava/lang/Float;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 704
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->isPeriodicSyncEnabled(Landroid/accounts/Account;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 705
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentAuthority()Ljava/lang/String;

    move-result-object v6

    .line 704
    invoke-static {v5, v6}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 708
    .local v3, "periodicSyncs":Ljava/util/List;, "Ljava/util/List<Landroid/content/PeriodicSync;>;"
    :goto_0
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 709
    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/PeriodicSync;

    iget-wide v6, v5, Landroid/content/PeriodicSync;->period:J

    long-to-float v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 712
    :cond_0
    sget-object v5, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SYNC_INFO:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 713
    .local v4, "syncInfoPreference":Landroid/preference/Preference;
    if-nez v1, :cond_2

    const-string v2, "never"

    .line 715
    .local v2, "intervalStr":Ljava/lang/String;
    :goto_1
    const-string v5, "Interval: %s"

    new-array v6, v11, [Ljava/lang/Object;

    aput-object v2, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 716
    return-void

    .line 704
    .end local v2    # "intervalStr":Ljava/lang/String;
    .end local v3    # "periodicSyncs":Ljava/util/List;, "Ljava/util/List<Landroid/content/PeriodicSync;>;"
    .end local v4    # "syncInfoPreference":Landroid/preference/Preference;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 713
    .restart local v3    # "periodicSyncs":Ljava/util/List;, "Ljava/util/List<Landroid/content/PeriodicSync;>;"
    .restart local v4    # "syncInfoPreference":Landroid/preference/Preference;
    :cond_2
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%.1f h"

    new-array v7, v11, [Ljava/lang/Object;

    .line 714
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v8

    const/high16 v9, 0x45610000    # 3600.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method protected areDeveloperPreferencesEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 164
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v2, "developerMode"

    invoke-virtual {v1, v2, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->areInternalPreferencesEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected areInternalPreferencesEnabled()Z
    .locals 1

    .prologue
    .line 169
    sget v0, Lcom/google/android/apps/newsstanddev/R$bool;->enable_developer_options:I

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getBooleanResource(I)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 70
    .local v0, "policy":Landroid/os/StrictMode$ThreadPolicy;
    sget v1, Lcom/google/android/apps/newsstanddev/R$xml;->settings:I

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->addPreferencesFromResource(I)V

    .line 71
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupGeneralPreferences()V

    .line 72
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupDownloadPreferences()V

    .line 73
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupAboutPreferences()V

    .line 74
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupDeveloperPreferences()V

    .line 75
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupInternalPreferences()V

    .line 76
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 77
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefListenerHandles:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 90
    .local v0, "handle":Lcom/google/apps/dots/android/newsstand/util/Disposable;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    goto :goto_0

    .line 92
    .end local v0    # "handle":Lcom/google/apps/dots/android/newsstand/util/Disposable;
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefListenerHandles:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 93
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 94
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onStop()V

    .line 82
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->processRestartRequired:Z

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 85
    :cond_0
    return-void
.end method

.method protected setupArticleTextSizePreference()V
    .locals 2

    .prologue
    .line 115
    sget-object v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->ARTICLE_TEXT_SIZE:Ljava/lang/String;

    .line 116
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 117
    .local v0, "preference":Landroid/preference/ListPreference;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getArticleFontSize()Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 118
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getArticleFontSize()Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getFontSizeTitleResId(Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    .line 119
    new-instance v1, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 128
    return-void
.end method

.method protected setupDeveloperPreferences()V
    .locals 3

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->areDeveloperPreferencesEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->showingDeveloperPreferences:Z

    if-eqz v1, :cond_1

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 360
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->showingDeveloperPreferences:Z

    .line 361
    sget v1, Lcom/google/android/apps/newsstanddev/R$xml;->settings_developer:I

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->addPreferencesFromResource(I)V

    .line 363
    sget-object v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->DESIGNER_MODE:Ljava/lang/String;

    .line 364
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 365
    .local v0, "designerModeEnabledPreference":Landroid/preference/CheckBoxPreference;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$10;

    const-string v2, "designerModeTimestamp"

    invoke-direct {v1, p0, v0, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$10;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setupGeneralPreferences()V
    .locals 0

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupArticleTextSizePreference()V

    .line 98
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupShowNotificationsPreference()V

    .line 99
    return-void
.end method

.method protected setupInternalPreferences()V
    .locals 33

    .prologue
    .line 385
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getServerType()Ljava/lang/String;

    move-result-object v25

    .line 386
    .local v25, "serverType":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->areInternalPreferencesEnabled()Z

    move-result v30

    if-eqz v30, :cond_2

    .line 388
    sget v30, Lcom/google/android/apps/newsstanddev/R$xml;->settings_internal:I

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->addPreferencesFromResource(I)V

    .line 389
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SERVER_TYPE:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 390
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/ListPreference;

    .line 391
    .local v26, "serverTypePref":Landroid/preference/ListPreference;
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 392
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 393
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$11;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$11;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/ListPreference;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getCustomBaseUrl()Ljava/lang/String;

    move-result-object v8

    .line 406
    .local v8, "customBaseUrl":Ljava/lang/String;
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->CUSTOM_BASE_URL:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 407
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Landroid/preference/EditTextPreference;

    .line 408
    .local v9, "customBaseUrlPref":Landroid/preference/EditTextPreference;
    invoke-virtual {v9, v8}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 409
    invoke-static {v8}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_0

    .line 410
    invoke-virtual {v9, v8}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 412
    :cond_0
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$12;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v9}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$12;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/EditTextPreference;)V

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getCustomGucUrl()Ljava/lang/String;

    move-result-object v10

    .line 426
    .local v10, "customGucUrl":Ljava/lang/String;
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->CUSTOM_GUC_URL:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 427
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    check-cast v11, Landroid/preference/EditTextPreference;

    .line 428
    .local v11, "customGucUrlPref":Landroid/preference/EditTextPreference;
    invoke-virtual {v11, v10}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 429
    invoke-static {v10}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_1

    .line 430
    invoke-virtual {v11, v10}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 432
    :cond_1
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$13;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v11}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$13;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/EditTextPreference;)V

    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 445
    .end local v8    # "customBaseUrl":Ljava/lang/String;
    .end local v9    # "customBaseUrlPref":Landroid/preference/EditTextPreference;
    .end local v10    # "customGucUrl":Ljava/lang/String;
    .end local v11    # "customGucUrlPref":Landroid/preference/EditTextPreference;
    .end local v26    # "serverTypePref":Landroid/preference/ListPreference;
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->areInternalPreferencesEnabled()Z

    move-result v30

    if-eqz v30, :cond_6

    .line 447
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->READING_POSITION_SYNC:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 448
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v23

    check-cast v23, Landroid/preference/CheckBoxPreference;

    .line 449
    .local v23, "readingPositionSyncPreference":Landroid/preference/CheckBoxPreference;
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$14;

    const-string v31, "readingPositionSync"

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, v23

    move-object/from16 v3, v31

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$14;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    .line 464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getCountryOverride()Ljava/lang/String;

    move-result-object v6

    .line 465
    .local v6, "countryOverrideUrl":Ljava/lang/String;
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->COUNTRY_OVERRIDE:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 466
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/EditTextPreference;

    .line 467
    .local v7, "countryOverrideUrlPref":Landroid/preference/EditTextPreference;
    invoke-virtual {v7, v6}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 468
    invoke-static {v6}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_3

    .line 469
    invoke-virtual {v7, v6}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 471
    :cond_3
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$15;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v7}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$15;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/EditTextPreference;)V

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLoadExtraJs()Ljava/lang/String;

    move-result-object v20

    .line 485
    .local v20, "loadExtraJs":Ljava/lang/String;
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->LOAD_EXTRA_JS:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 486
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v21

    check-cast v21, Landroid/preference/EditTextPreference;

    .line 487
    .local v21, "loadExtraJsPref":Landroid/preference/EditTextPreference;
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 488
    invoke-static/range {v20 .. v20}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_4

    .line 489
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 491
    :cond_4
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$16;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$16;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/EditTextPreference;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 501
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SHOW_ONBOARD_TUTORIAL:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 502
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v28

    check-cast v28, Landroid/preference/CheckBoxPreference;

    .line 503
    .local v28, "showOnboardTutorialPref":Landroid/preference/CheckBoxPreference;
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$17;

    const-string v31, "showedOnboardTutorial"

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, v28

    move-object/from16 v3, v31

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$17;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    .line 517
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SHOW_ONBOARD_QUIZ:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 518
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v27

    check-cast v27, Landroid/preference/ListPreference;

    .line 519
    .local v27, "showOnboardQuizPref":Landroid/preference/ListPreference;
    invoke-virtual/range {v27 .. v27}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v14

    .line 520
    .local v14, "entries":[Ljava/lang/CharSequence;
    invoke-virtual/range {v27 .. v27}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v29

    .line 521
    .local v29, "values":[Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getShowOnboardQuizPreference()I

    move-result v30

    aget-object v30, v29, v30

    check-cast v30, Ljava/lang/String;

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 522
    invoke-virtual/range {v27 .. v27}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 523
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$18;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2, v14}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$18;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;[Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)V

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 536
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SIMULATE_CRASH_EVENT:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 537
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 538
    .local v5, "causeCrashPreference":Landroid/preference/Preference;
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$19;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$19;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 547
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->RESET_WARM_WELCOME_CARDS:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 548
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    .line 549
    .local v24, "resetWarmWelcomePreference":Landroid/preference/Preference;
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$20;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$20;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 563
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getEnabledLabIds()Ljava/util/Set;

    move-result-object v13

    .line 564
    .local v13, "enabledLabIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget v30, Lcom/google/android/apps/newsstanddev/R$string;->internal_labs_preference_key:I

    .line 565
    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v19

    check-cast v19, Landroid/preference/PreferenceCategory;

    .line 568
    .local v19, "labsPreferenceCategory":Landroid/preference/PreferenceCategory;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v18

    .line 570
    .local v18, "labs":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$Lab;>;"
    new-instance v22, Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    invoke-direct/range {v22 .. v22}, Lcom/google/apps/dots/proto/client/DotsShared$Lab;-><init>()V

    .line 571
    .local v22, "pullquoteLab":Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    sget-object v30, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->ARTICLE_AUTO_PULLQUOTES:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    invoke-virtual/range {v30 .. v30}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->ordinal()I

    move-result v30

    move-object/from16 v0, v22

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->setLabId(I)Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    .line 572
    const-string v30, "Enable automatic pullquotes within articles."

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->setLabName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    .line 573
    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 575
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v30

    if-nez v30, :cond_7

    .line 576
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 602
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setUpInternalSyncPreferences()V

    .line 605
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->ALWAYS_SHOW_GOOGLE_SOLD_ADS:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 606
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    .line 607
    .local v4, "alwaysShowGoogleSoldAdsPreference":Landroid/preference/CheckBoxPreference;
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$22;

    const-string v31, "alwaysShowGoogleSoldAds"

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v4, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$22;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    .line 622
    sget-object v30, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->ENABLE_ALL_DEBUG_LOGGERS:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 623
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v12

    check-cast v12, Landroid/preference/CheckBoxPreference;

    .line 624
    .local v12, "enableAllDebugLoggersPreference":Landroid/preference/CheckBoxPreference;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->isEnableAll()Z

    move-result v30

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 625
    new-instance v30, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$23;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$23;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V

    move-object/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 635
    .end local v4    # "alwaysShowGoogleSoldAdsPreference":Landroid/preference/CheckBoxPreference;
    .end local v5    # "causeCrashPreference":Landroid/preference/Preference;
    .end local v6    # "countryOverrideUrl":Ljava/lang/String;
    .end local v7    # "countryOverrideUrlPref":Landroid/preference/EditTextPreference;
    .end local v12    # "enableAllDebugLoggersPreference":Landroid/preference/CheckBoxPreference;
    .end local v13    # "enabledLabIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v14    # "entries":[Ljava/lang/CharSequence;
    .end local v18    # "labs":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$Lab;>;"
    .end local v19    # "labsPreferenceCategory":Landroid/preference/PreferenceCategory;
    .end local v20    # "loadExtraJs":Ljava/lang/String;
    .end local v21    # "loadExtraJsPref":Landroid/preference/EditTextPreference;
    .end local v22    # "pullquoteLab":Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    .end local v23    # "readingPositionSyncPreference":Landroid/preference/CheckBoxPreference;
    .end local v24    # "resetWarmWelcomePreference":Landroid/preference/Preference;
    .end local v27    # "showOnboardQuizPref":Landroid/preference/ListPreference;
    .end local v28    # "showOnboardTutorialPref":Landroid/preference/CheckBoxPreference;
    .end local v29    # "values":[Ljava/lang/CharSequence;
    :cond_6
    return-void

    .line 578
    .restart local v5    # "causeCrashPreference":Landroid/preference/Preference;
    .restart local v6    # "countryOverrideUrl":Ljava/lang/String;
    .restart local v7    # "countryOverrideUrlPref":Landroid/preference/EditTextPreference;
    .restart local v13    # "enabledLabIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v14    # "entries":[Ljava/lang/CharSequence;
    .restart local v18    # "labs":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$Lab;>;"
    .restart local v19    # "labsPreferenceCategory":Landroid/preference/PreferenceCategory;
    .restart local v20    # "loadExtraJs":Ljava/lang/String;
    .restart local v21    # "loadExtraJsPref":Landroid/preference/EditTextPreference;
    .restart local v22    # "pullquoteLab":Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    .restart local v23    # "readingPositionSyncPreference":Landroid/preference/CheckBoxPreference;
    .restart local v24    # "resetWarmWelcomePreference":Landroid/preference/Preference;
    .restart local v27    # "showOnboardQuizPref":Landroid/preference/ListPreference;
    .restart local v28    # "showOnboardTutorialPref":Landroid/preference/CheckBoxPreference;
    .restart local v29    # "values":[Ljava/lang/CharSequence;
    :cond_7
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v30

    :goto_0
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v31

    if-eqz v31, :cond_5

    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    .line 579
    .local v15, "lab":Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    new-instance v17, Landroid/preference/CheckBoxPreference;

    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v31

    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 580
    .local v17, "labPreference":Landroid/preference/CheckBoxPreference;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    move-result-object v31

    invoke-virtual {v15}, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->getLabId()I

    move-result v32

    aget-object v16, v31, v32

    .line 581
    .local v16, "labId":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;
    invoke-virtual {v15}, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->getLabName()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 582
    invoke-virtual/range {v16 .. v16}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->name()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-interface {v13, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v31

    move-object/from16 v0, v17

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 583
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 584
    new-instance v31, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v13, v3}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$21;-><init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;Ljava/util/Set;Landroid/preference/CheckBoxPreference;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0
.end method
