.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EventHandler"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;


# instance fields
.field public dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

.field private eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3842
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3843
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3865
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    .line 3843
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    .locals 5

    .prologue
    .line 3878
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3882
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    if-eqz v3, :cond_0

    .line 3883
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    .line 3885
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 3886
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    .line 3887
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 3888
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 3889
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    move-result-object v4

    aput-object v4, v3, v2

    .line 3887
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3879
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 3880
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 3893
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    :cond_2
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3839
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3898
    if-ne p1, p0, :cond_1

    .line 3902
    :cond_0
    :goto_0
    return v1

    .line 3899
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 3900
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    .line 3901
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    .line 3902
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 3901
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getEventFilter()Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;
    .locals 1

    .prologue
    .line 3848
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 3934
    const/4 v1, 0x0

    .line 3935
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    if-eqz v2, :cond_0

    .line 3936
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    .line 3937
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3939
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    if-eqz v2, :cond_2

    .line 3940
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 3941
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;
    if-eqz v0, :cond_1

    .line 3942
    const/4 v5, 0x2

    .line 3943
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3940
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3947
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;
    :cond_2
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->cachedSize:I

    .line 3948
    return v1
.end method

.method public hasEventFilter()Z
    .locals 1

    .prologue
    .line 3858
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 3906
    const/16 v1, 0x11

    .line 3907
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 3908
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 3909
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    if-nez v2, :cond_2

    mul-int/lit8 v1, v1, 0x1f

    .line 3915
    :cond_0
    return v1

    .line 3908
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;->hashCode()I

    move-result v2

    goto :goto_0

    .line 3911
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 3912
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 3911
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3912
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3956
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3957
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3961
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3962
    :sswitch_0
    return-object p0

    .line 3967
    :sswitch_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    if-nez v5, :cond_1

    .line 3968
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    .line 3970
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3974
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3975
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    if-nez v5, :cond_3

    move v1, v4

    .line 3976
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    .line 3977
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    if-eqz v5, :cond_2

    .line 3978
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3980
    :cond_2
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    .line 3981
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 3982
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;-><init>()V

    aput-object v6, v5, v1

    .line 3983
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3984
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3981
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3975
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    array-length v1, v5

    goto :goto_1

    .line 3987
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;
    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;-><init>()V

    aput-object v6, v5, v1

    .line 3988
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3957
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3839
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3920
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    if-eqz v1, :cond_0

    .line 3921
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->eventFilter_:Lcom/google/apps/dots/proto/client/DotsNativeBody$EventFilter;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3923
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    if-eqz v1, :cond_2

    .line 3924
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$EventHandler;->dispatchEvent:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 3925
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;
    if-eqz v0, :cond_1

    .line 3926
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3924
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3930
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$Event;
    :cond_2
    return-void
.end method
