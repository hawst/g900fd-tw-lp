.class Lcom/google/apps/dots/android/newsstand/store/NSStore$1;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "NSStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 190
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v8, v8, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 191
    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->getManifestUri(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    invoke-static {v0, v1, v2, v3, v8}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$000(Lcom/google/apps/dots/android/newsstand/store/NSStore;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v4

    .line 193
    .local v4, "manifestUri":Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v8, v8, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 194
    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->getCanonicalUri(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    invoke-static {v0, v1, v2, v3, v8}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$100(Lcom/google/apps/dots/android/newsstand/store/NSStore;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v5

    .line 196
    .local v5, "canonicalUri":Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "%s: resolved to %s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    aput-object v0, v3, v9

    if-nez v4, :cond_0

    move-object v0, v5

    :goto_0
    aput-object v0, v3, v10

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->getBlobFile(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/NSStore;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    move-result-object v6

    .line 198
    .local v6, "blobFile":Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->getBlobMetadata(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$400(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    move-result-object v7

    .line 200
    .local v7, "blobMetadata":Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->cachePolicy:Lcom/google/apps/dots/android/newsstand/store/CachePolicy;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$500(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/store/CachePolicy;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-virtual {v0, v7, v1}, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;->mayUseCachedVersion(Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 247
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .end local v6    # "blobFile":Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .end local v7    # "blobMetadata":Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    :cond_0
    move-object v0, v4

    .line 196
    goto :goto_0

    .line 203
    .restart local v6    # "blobFile":Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .restart local v7    # "blobMetadata":Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    :pswitch_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: MAY_USE. Returning cached version"

    new-array v2, v10, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 205
    invoke-virtual {v1, v7}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getBlobFileVersion(Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/apps/dots/android/newsstand/store/Version;

    move-result-object v1

    invoke-direct {v0, v6, v7, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;-><init>(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;Lcom/google/apps/dots/android/newsstand/store/Version;)V

    .line 204
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 237
    :goto_1
    return-object v0

    .line 209
    :pswitch_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: MAY_USE_OFFLINE. Triggering download"

    new-array v2, v10, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .line 216
    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->recoverCacheFallbackTimeoutMs()J
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$600(Lcom/google/apps/dots/android/newsstand/store/NSStore;)J

    move-result-wide v8

    .line 210
    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerDownloadWithTimeout(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;J)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static/range {v1 .. v9}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$700(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;J)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;

    invoke-direct {v1, p0, v6, v7}, Lcom/google/apps/dots/android/newsstand/store/NSStore$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore$1;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    .line 227
    :pswitch_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->mayDownload()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 228
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: DONT_USE. Triggering download"

    new-array v2, v10, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    if-ne v0, v1, :cond_1

    .line 230
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerAttachmentDownloadWithFallbackToClientTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static/range {v1 .. v7}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$900(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    .line 237
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerDownload(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static/range {v1 .. v7}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$1000(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    .line 241
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: DONT_USE. Download not permitted"

    new-array v2, v10, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/NSStore$NotAvailableException;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/NSStore$NotAvailableException;-><init>(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    throw v0

    .line 200
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
