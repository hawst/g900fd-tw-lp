.class public abstract Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;
.super Lcom/google/apps/dots/android/newsstand/widget/StateTrackingOnScrollListener;
.source "ScrollDeltaOnScrollListener.java"


# instance fields
.field private previousCount:I

.field private previousFirstVisibleItem:I

.field private previousFirstVisibleItemBottom:I

.field private previousFirstVisibleItemTop:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/StateTrackingOnScrollListener;-><init>()V

    return-void
.end method


# virtual methods
.method public notScrolled(II)V
    .locals 0
    .param p1, "firstVisibleItem"    # I
    .param p2, "firstVisibleTop"    # I

    .prologue
    .line 68
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 6
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 23
    const/4 v0, 0x0

    .line 24
    .local v0, "deltaY":I
    if-nez p4, :cond_3

    .line 25
    const/4 v2, 0x0

    .line 26
    .local v2, "firstVisibleItemTop":I
    const/4 v1, 0x0

    .line 32
    .local v1, "firstVisibleItemBottom":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->isScrolling()Z

    move-result v5

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousCount:I

    if-ne v5, p4, :cond_1

    .line 33
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousFirstVisibleItem:I

    if-ne p2, v5, :cond_4

    .line 35
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousFirstVisibleItemTop:I

    sub-int v0, v5, v2

    .line 47
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 49
    invoke-virtual {p0, p2, v2, v0}, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->scrolledBy(III)V

    .line 52
    :cond_1
    if-nez v0, :cond_2

    .line 54
    invoke-virtual {p0, p2, v2}, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->notScrolled(II)V

    .line 56
    :cond_2
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousFirstVisibleItem:I

    .line 57
    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousFirstVisibleItemTop:I

    .line 58
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousFirstVisibleItemBottom:I

    .line 59
    iput p4, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousCount:I

    .line 60
    return-void

    .line 28
    .end local v1    # "firstVisibleItemBottom":I
    .end local v2    # "firstVisibleItemTop":I
    :cond_3
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 29
    .local v4, "topView":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v2

    .line 30
    .restart local v2    # "firstVisibleItemTop":I
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v1

    .restart local v1    # "firstVisibleItemBottom":I
    goto :goto_0

    .line 36
    .end local v4    # "topView":Landroid/view/View;
    :cond_4
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousFirstVisibleItem:I

    if-le p2, v5, :cond_5

    .line 38
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousFirstVisibleItemBottom:I

    sub-int v0, v5, v2

    goto :goto_1

    .line 39
    :cond_5
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousFirstVisibleItem:I

    if-ge p2, v5, :cond_0

    .line 41
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousFirstVisibleItemTop:I

    sub-int v0, v5, v1

    .line 42
    add-int/lit8 v3, p2, 0x1

    .local v3, "i":I
    :goto_2
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ScrollDeltaOnScrollListener;->previousFirstVisibleItem:I

    if-ge v3, v5, :cond_0

    if-ge v3, p3, :cond_0

    .line 44
    sub-int v5, v3, p2

    invoke-virtual {p1, v5}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    sub-int/2addr v0, v5

    .line 43
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public abstract scrolledBy(III)V
.end method
