.class public Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
.super Lcom/google/android/libraries/bind/card/CardListView;
.source "NSCardListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/card/NSCardListView$NSCardShuffleEvaluator;
    }
.end annotation


# instance fields
.field private audioFragment:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

.field private audioTouchListener:Landroid/view/View$OnTouchListener;

.field private final firstShuffleObserver:Landroid/database/DataSetObserver;

.field private shuffleAnimationOnNextInvalidation:Z

.field private shuffleOnFirstLoad:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/card/CardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->shuffleOnFirstLoad:Z

    .line 37
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$1;-><init>(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->firstShuffleObserver:Landroid/database/DataSetObserver;

    .line 68
    sget-object v1, Lcom/google/android/apps/newsstanddev/R$styleable;->NSCardListView:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 69
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->NSCardListView_shuffleOnFirstLoad:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->shuffleOnFirstLoad:Z

    .line 70
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 71
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setClipChildren(Z)V

    .line 72
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$2;-><init>(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->audioTouchListener:Landroid/view/View$OnTouchListener;

    .line 85
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->audioTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)Lcom/google/android/libraries/bind/data/DataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getDataAdapter()Lcom/google/android/libraries/bind/data/DataAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)Landroid/database/DataSetObserver;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->firstShuffleObserver:Landroid/database/DataSetObserver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->audioFragment:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->audioFragment:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    return-object p1
.end method

.method private getDataAdapter()Lcom/google/android/libraries/bind/data/DataAdapter;
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 165
    .local v0, "listAdapter":Landroid/widget/ListAdapter;
    instance-of v1, v0, Landroid/widget/WrapperListAdapter;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/widget/WrapperListAdapter;

    .line 166
    .end local v0    # "listAdapter":Landroid/widget/ListAdapter;
    invoke-interface {v0}, Landroid/widget/WrapperListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    :goto_0
    check-cast v1, Lcom/google/android/libraries/bind/data/DataAdapter;

    check-cast v1, Lcom/google/android/libraries/bind/data/DataAdapter;

    return-object v1

    .restart local v0    # "listAdapter":Landroid/widget/ListAdapter;
    :cond_0
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public addOnChildrenAddedListener(ILjava/lang/Runnable;)V
    .locals 1
    .param p1, "threshold"    # I
    .param p2, "onChildrenAdded"    # Ljava/lang/Runnable;

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getChildCount()I

    move-result v0

    if-lt v0, p1, :cond_0

    .line 124
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    .line 137
    :goto_0
    return-void

    .line 126
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$3;-><init>(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;ILjava/lang/Runnable;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto :goto_0
.end method

.method protected animateIfNeeded()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 172
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getAnimateChanges()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->shuffleAnimationOnNextInvalidation:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getCount()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 173
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$NSCardShuffleEvaluator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$NSCardShuffleEvaluator;-><init>(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;Lcom/google/apps/dots/android/newsstand/card/NSCardListView$1;)V

    invoke-static {p0, v2, v0}, Lcom/google/android/play/animation/ShuffleAnimation;->shuffle(Landroid/view/ViewGroup;ZLcom/google/android/play/animation/AnimationGroupEvaluator;)V

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->shuffleAnimationOnNextInvalidation:Z

    .line 176
    sget-object v0, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/async/JankLock;->pauseTemporarily(J)V

    .line 180
    :goto_0
    return-void

    .line 179
    :cond_0
    invoke-super {p0}, Lcom/google/android/libraries/bind/card/CardListView;->animateIfNeeded()V

    goto :goto_0
.end method

.method protected prepareInvalidationAnimation()V
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->shuffleAnimationOnNextInvalidation:Z

    if-eqz v0, :cond_0

    .line 146
    :goto_0
    return-void

    .line 145
    :cond_0
    invoke-super {p0}, Lcom/google/android/libraries/bind/card/CardListView;->prepareInvalidationAnimation()V

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 153
    .local v0, "oldAdapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->shuffleOnFirstLoad:Z

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->firstShuffleObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 156
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/card/CardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 157
    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->shuffleOnFirstLoad:Z

    if-eqz v1, :cond_1

    .line 158
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->firstShuffleObserver:Landroid/database/DataSetObserver;

    invoke-interface {p1, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 160
    :cond_1
    return-void
.end method

.method public setShuffleOnFirstLoad(Z)V
    .locals 3
    .param p1, "shuffleOnFirstLoad"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->shuffleOnFirstLoad:Z

    .line 93
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 94
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_0

    .line 95
    if-nez p1, :cond_1

    .line 96
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->firstShuffleObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getDataAdapter()Lcom/google/android/libraries/bind/data/DataAdapter;

    move-result-object v1

    .line 99
    .local v1, "dataAdapter":Lcom/google/android/libraries/bind/data/DataAdapter;
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataAdapter;->hasRefreshedOnce()Z

    move-result v2

    if-nez v2, :cond_0

    .line 100
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->firstShuffleObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public shuffleAnimationOnNextInvalidation()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->shuffleAnimationOnNextInvalidation:Z

    .line 108
    return-void
.end method
