.class public final enum Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;
.super Ljava/lang/Enum;
.source "ProtoEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/util/ProtoEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ItemOrigin"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

.field public static final enum CONNECTOR:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

.field public static final enum CONNECTOR_EXTRACTOR:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

.field public static final enum CONNECTOR_EXTRACTOR_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

.field public static final enum CONNECTOR_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

.field public static final enum CONNECTOR_PENDING_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

.field public static final enum GENERATED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

.field public static final enum IMPORT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

.field public static final enum IMPORT_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

.field public static final enum USER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

.field public static final enum USER_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;


# instance fields
.field private itemOrigin:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 58
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    const-string v1, "GENERATED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->GENERATED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    .line 59
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    const-string v1, "CONNECTOR"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->CONNECTOR:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    .line 60
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    const-string v1, "CONNECTOR_HTML"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->CONNECTOR_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    .line 61
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    const-string v1, "CONNECTOR_EXTRACTOR"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->CONNECTOR_EXTRACTOR:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    .line 62
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    const-string v1, "CONNECTOR_EXTRACTOR_HTML"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->CONNECTOR_EXTRACTOR_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    .line 63
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    const-string v1, "CONNECTOR_PENDING_ATTACHMENT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v8, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->CONNECTOR_PENDING_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    .line 64
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    const-string v1, "IMPORT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->IMPORT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    .line 65
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    const-string v1, "IMPORT_HTML"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->IMPORT_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    .line 66
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    const-string v1, "USER"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->USER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    .line 67
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    const-string v1, "USER_HTML"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->USER_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    .line 57
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->GENERATED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->CONNECTOR:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->CONNECTOR_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->CONNECTOR_EXTRACTOR:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->CONNECTOR_EXTRACTOR_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->CONNECTOR_PENDING_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->IMPORT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->IMPORT_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->USER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->USER_HTML:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "itemOrigin"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 81
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->itemOrigin:I

    .line 82
    return-void
.end method

.method public static fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;
    .locals 5
    .param p0, "itemOrigin"    # I

    .prologue
    .line 72
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 73
    .local v0, "origin":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;
    iget v4, v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->itemOrigin:I

    if-ne v4, p0, :cond_0

    .line 77
    .end local v0    # "origin":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;
    :goto_1
    return-object v0

    .line 72
    .restart local v0    # "origin":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    .end local v0    # "origin":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 57
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    return-object v0
.end method
