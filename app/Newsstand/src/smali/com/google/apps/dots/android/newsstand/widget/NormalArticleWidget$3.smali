.class Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "NormalArticleWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->setupWebView(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

.field final synthetic val$articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    .prologue
    .line 376
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;->val$articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;->val$articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 380
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getBaseHtmlFuture(Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$3;)V

    .line 379
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 396
    return-void
.end method
