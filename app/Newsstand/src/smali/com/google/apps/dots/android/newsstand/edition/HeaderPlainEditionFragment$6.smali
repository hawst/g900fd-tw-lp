.class Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$6;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "HeaderPlainEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateImageRotator()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 312
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 3
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->headerImageList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->setupImageRotator(Lcom/google/android/libraries/bind/data/DataList;)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$1000(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;Lcom/google/android/libraries/bind/data/DataList;)V

    .line 316
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 312
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$6;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
