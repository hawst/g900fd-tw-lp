.class public final Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReadNowEditionShelfSummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;


# instance fields
.field private bitField0_:I

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25500
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25501
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 25513
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->type_:I

    .line 25501
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;
    .locals 3

    .prologue
    .line 25542
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25546
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;
    return-object v0

    .line 25543
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;
    :catch_0
    move-exception v1

    .line 25544
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25497
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 25551
    if-ne p1, p0, :cond_1

    .line 25554
    :cond_0
    :goto_0
    return v1

    .line 25552
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 25553
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    .line 25554
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->type_:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 25573
    const/4 v0, 0x0

    .line 25574
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 25575
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->type_:I

    .line 25576
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 25578
    :cond_0
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->cachedSize:I

    .line 25579
    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 25515
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->type_:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 25558
    const/16 v0, 0x11

    .line 25559
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 25560
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->type_:I

    add-int v0, v1, v2

    .line 25561
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25587
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 25588
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 25592
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 25593
    :sswitch_0
    return-object p0

    .line 25598
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 25599
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 25603
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->type_:I

    .line 25604
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->bitField0_:I

    goto :goto_0

    .line 25606
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->type_:I

    goto :goto_0

    .line 25588
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25497
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25566
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 25567
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 25569
    :cond_0
    return-void
.end method
