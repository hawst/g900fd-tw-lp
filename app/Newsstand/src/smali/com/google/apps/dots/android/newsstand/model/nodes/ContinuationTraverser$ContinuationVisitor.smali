.class Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;
.super Ljava/lang/Object;
.source "ContinuationTraverser.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ContinuationVisitor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor",
        "<",
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;",
        ">;"
    }
.end annotation


# instance fields
.field private final clientVisitor:Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor",
            "<-",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;",
            ">;"
        }
    .end annotation
.end field

.field private final continuationTraversal:Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

.field private final mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

.field private final token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V
    .locals 1
    .param p1, "mutationStore"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;
    .param p2, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/MutationStore;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor",
            "<-",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p3, "clientVisitor":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor<-Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->continuationTraversal:Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    .line 96
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .line 97
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 98
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->clientVisitor:Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;

    .line 99
    return-void
.end method

.method private proxyClientFinishedCall(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;)V
    .locals 1
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->continuationTraversal:Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;->requestedFinish()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    invoke-interface {p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;->finish()V

    .line 140
    :cond_0
    return-void
.end method


# virtual methods
.method public exit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 2
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->continuationTraversal:Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;->clearRequest()V

    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->clientVisitor:Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->continuationTraversal:Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-interface {v0, v1, p2}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;->exit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V

    .line 133
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->proxyClientFinishedCall(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;)V

    .line 134
    return-void
.end method

.method public visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 8
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 103
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->continuationTraversal:Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;->clearRequest()V

    .line 104
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->clientVisitor:Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->continuationTraversal:Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-interface {v3, v4, p2}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V

    .line 105
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->proxyClientFinishedCall(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;)V

    .line 106
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->continuationTraversal:Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;->requestedFinish()Z

    move-result v3

    if-nez v3, :cond_0

    .line 107
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v3

    const/16 v4, 0xe

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->continuationTraversal:Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    iget-boolean v3, v3, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;->requestedTraverseContinuation:Z

    if-eqz v3, :cond_0

    .line 110
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getSelf()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    move-result-object v1

    .line 112
    .local v1, "link":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/base/Strings;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 113
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/base/Strings;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 111
    invoke-static {v3, v4}, Lcom/google/apps/dots/android/newsstand/util/ObjectUtil;->coalesce(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 114
    .local v0, "continuationUri":Ljava/lang/String;
    # getter for: Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "Continuation found: %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 116
    invoke-virtual {v3, v4, v0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getAvailable(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 115
    invoke-static {v3, v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;Z)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .line 118
    .local v2, "response":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    if-nez v2, :cond_1

    .line 127
    .end local v0    # "continuationUri":Ljava/lang/String;
    .end local v1    # "link":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    .end local v2    # "response":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    :cond_0
    :goto_0
    return-void

    .line 122
    .restart local v0    # "continuationUri":Ljava/lang/String;
    .restart local v1    # "link":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    .restart local v2    # "response":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    :cond_1
    # getter for: Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "Continuation available: %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    new-instance v3, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    invoke-virtual {v3, p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 125
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;->proxyClientFinishedCall(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;)V

    goto :goto_0
.end method
