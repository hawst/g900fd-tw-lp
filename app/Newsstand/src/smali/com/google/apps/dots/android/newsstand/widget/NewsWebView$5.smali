.class Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$5;
.super Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;
.source "NewsWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setupAudioStatusHelper()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .prologue
    .line 370
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 3
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-direct {v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->audioStatusHelper:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->access$402(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;)Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    .line 374
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->audioStatusHelper:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->access$400(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->register(Landroid/content/Context;)V

    .line 375
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 370
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$5;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
