.class Lcom/google/apps/dots/android/newsstand/auth/AuthHelper$1;
.super Ljava/lang/Object;
.source "AuthHelper.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->getAuthTokenFuture(Landroid/accounts/Account;Z)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$getPromptIntent:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;Landroid/accounts/Account;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper$1;->val$account:Landroid/accounts/Account;

    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper$1;->val$getPromptIntent:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper$1;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper$1;->val$account:Landroid/accounts/Account;

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper$1;->val$getPromptIntent:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->getAuthToken(Landroid/accounts/Account;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
