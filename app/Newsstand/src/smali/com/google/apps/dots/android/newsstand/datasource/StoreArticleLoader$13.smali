.class Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$13;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getNewsBaseHtmlFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 407
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$13;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 407
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$13;->apply(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public apply(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "htmlFromFile"    # Ljava/lang/String;

    .prologue
    .line 410
    new-instance v0, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/article/NewsWebViewHtmlProcessor;->processBaseHtml()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
